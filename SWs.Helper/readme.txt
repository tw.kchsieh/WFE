﻿SWs.Helper (Solidworks API Helper)

官方2016版本API(VB.NET)說明找到獲取面積程式語法資訊與範例
資訊一：http://help.solidworks.com/2016/english/api/sldworksapi/solidworks.interop.sldworks~solidworks.interop.sldworks.iface2~getarea.html?verRedirect=1
資訊二：http://help.solidworks.com/2016/english/api/sldworksapi/Get_Areas_of_MidSurface_FAces_Example_VBNET.htm

官方討論區於2012年討論資訊
資訊一：https://forum.solidworks.com/thread/60518

官方討論區於2014年討論資訊(最後一位評論者，有提供獲取最大面積的語法，但我不確定它獲取方式。)
資訊一：https://forum.solidworks.com/message/457586#comment-457586

## SurfaceArea
http://help.solidworks.com/2016/english/api/sldworksapi/Get_Mass_Properties_Using_IMassProperty_Example_VBNET.htm
