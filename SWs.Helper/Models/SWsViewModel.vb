﻿Public Class SWsViewModel
	Public Sub New()
		'設定預設值
		IsElementCatched = False
		IsFaceCatched = False
		ErrMsg = ""
		GetAreaErrMsg = ""
	End Sub

	''' <summary>
	''' 元件名稱
	''' </summary>
	''' <returns></returns>
	Public Property Name As String

	''' <summary>
	''' 體積 mm^3
	''' </summary>
	''' <returns></returns>
	Public Property Volumn As String

	''' <summary>
	''' 是否取得 Solidworks 元件
	''' </summary>
	''' <returns></returns>
	Public Property IsElementCatched As Boolean

	''' <summary>
	''' 最大面積(mm^2)
	''' </summary>
	''' <returns></returns>
	Public Property Area As Double

	''' <summary>
	''' 表面積(mm^2)
	''' </summary>
	''' <returns></returns>
	Public Property SurfaceArea As Double

	''' <summary>
	''' 是否取得 Solidworks 元件之 Face 部位
	''' </summary>
	''' <returns></returns>
	Public Property IsFaceCatched As Boolean

	''' <summary>
	''' 錯誤訊息
	''' </summary>
	''' <returns></returns>
	Public Property ErrMsg As String
	Public Property GetAreaErrMsg As String

	Public Property CreateMassPropertyErrMsg As String
End Class
