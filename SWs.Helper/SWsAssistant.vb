﻿Imports SolidWorks.Interop.sldworks
Imports SWorks = SolidWorks.Interop.sldworks
Public Class SWsAssistant
	Private _mm2Val As Double
	Private _mm3Val As Double
	Public Sub New()
		_mm2Val = Math.Pow(1000, 2) 'mm^2
		_mm3Val = Math.Pow(1000, 3) 'mm^3
	End Sub
	''' <summary>
	''' 取得元件的名稱和體積
	''' </summary>
	''' <returns></returns>
	Public Function GetComponentInfo() As SWsViewModel
		'Result model
		Dim model = New SWsViewModel()

		'Solidworks variables
		Dim swApp As SWorks.SldWorks
		Dim swModel As SWorks.ModelDoc2
		Dim swSelMgr As SWorks.SelectionMgr
		Dim swComp As SWorks.Component2
		Dim swCompModel As SWorks.ModelDoc2
		'Dim swCompBody As SWorks.Body2
		'Dim vMassProps As Object
		'Dim nDensity As Double
		Dim swFace As Face2

		Try
			swApp = CreateObject("SldWorks.Application") '必要
			swModel = swApp.ActiveDoc '必要
			swSelMgr = swModel.SelectionManager '必要

			'========== Get Component Name and Volumn =============
			swComp = swSelMgr.GetSelectedObjectsComponent2(1)
			If swComp IsNot Nothing Then
				'swCompBody = swComp.GetBody
				swCompModel = swComp.GetModelDoc

				'獲取零件名稱
				model.Name = swComp.Name2
				Try
					Dim massProperty = swCompModel.Extension.CreateMassProperty

					'獲取表面積, square millimeters			
					model.SurfaceArea = Format(massProperty.SurfaceArea * _mm2Val, "0.00")

					'cubic millimeters
					model.Volumn = Format(massProperty.Volume * _mm3Val, "0.00")
				Catch ex As Exception
					model.CreateMassPropertyErrMsg = ex.Message
				End Try

				'The return value Is a 0-based array of doubles as follows:
				'[ CenterOfMassX, CenterOfMassY, CenterOfMassZ, Volume, Area, Mass, MomXX, MomYY, MomZZ, MomXY, MomZX, MomYZ, Accuracy ]
				'vMassProps = swCompBody.GetMassProperties(nDensity)
				'獲取體積換算成 mm^3 並取到小數第二位
				'model.Volumn = Format(vMassProps(3) * _mm3Val, "#.00")
				model.IsElementCatched = True
			End If

			'============= Get Area =======================
			Try
				swFace = swSelMgr.GetSelectedObject5(1)
				If swFace IsNot Nothing Then
					'面積，換算成 mm^2 並取到小數第二位
					model.Area = Format(swFace.GetArea() * _mm2Val, "#.00")
					model.IsFaceCatched = True
				End If
			Catch ex As Exception
				model.GetAreaErrMsg = ex.Message
			End Try

		Catch ex As Exception
			model.ErrMsg = ex.Message
		Finally
			swApp = Nothing
		End Try

		'回傳 model
		Return model
	End Function
End Class
