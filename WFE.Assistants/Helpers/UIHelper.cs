﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WFE.Assistants.Helpers
{
	/// <summary>
	/// 介面小幫手
	/// </summary>
	public class UIHelper
	{
		/// <summary>
		/// 靜態建構子
		/// </summary>
		static UIHelper()
		{
			//default value
			_PrecisionNumber = 6;
			_NumberDisplayType = "E";
			_NumberTextFormatPattern = String.Format("{{0:{0}{1}}}", NumberDisplayType, PrecisionNumber);
		}


		private static int _PrecisionNumber;
		public static void SetPrceisionNumber(int value)
		{
			_PrecisionNumber = value;
		}
		private static string _NumberDisplayType;
		public static void SetNumberDisplayType(string value)
		{
			_NumberDisplayType = value;
		}
		private static string _NumberTextFormatPattern;
		public static void SetNumberTextFormatPattern(string pattern)
		{
			_NumberTextFormatPattern = pattern;
		}
		public static int PrecisionNumber { get { return _PrecisionNumber; } }
		public static string NumberDisplayType { get { return _NumberDisplayType; } }

		/// <summary>
		/// 轉換成百分比，用浮點數表示(F2)
		/// </summary>
		/// <param name="value"></param>
		/// <returns></returns>
		public static string ToPercentFixedPointText(int value)
		{
			return String.Format("{0:f2}", value / 100.0);
		}

		/// <summary>
		/// 文字轉成百分比，整數表示
		/// </summary>
		/// <param name="text"></param>
		/// <returns></returns>
		public static int ToPercentValue(string text)
		{
			double value;
			int result = 0;
			if (Double.TryParse(text, out value))
			{
				result = Convert.ToInt32(Math.Round(value, 2) * 100);
			}
			return result;
		}

		/// <summary>
		/// 設定預設的 DataGridView 屬性設定
		/// </summary>
		/// <param name="dv"></param>
		public static void SetDefaultDataGridViewProperties(DataGridView dv)
		{
			dv.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;//欄位自動填滿 GridView
			dv.SelectionMode = DataGridViewSelectionMode.FullRowSelect;//整列選取
			dv.MultiSelect = false;//禁止多選

			dv.AllowUserToAddRows = false;//禁止新增 Row
			dv.AllowUserToDeleteRows = false;//禁止刪除
			dv.ReadOnly = true;//禁止編輯
			dv.AllowUserToResizeRows = false; //禁止調整列高
			dv.RowHeadersVisible = false;//左側列不顯示
		}

		/// <summary>
		/// 數值格式化
		/// </summary>
		/// <param name="pattern"></param>
		/// <param name="val"></param>
		/// <returns></returns>
		public static string NumberTextFormat(double val)
		{
			return String.Format(_NumberTextFormatPattern, val);
		}

		/// <summary>
		/// 數值任精度作 Math.Round
		/// </summary>
		/// <param name="v"></param>
		/// <returns></returns>
		internal static double PrecisionNumberFormat(double v)
		{
			return Math.Round(v, _PrecisionNumber);
		}
	}
}
