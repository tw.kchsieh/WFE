﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFE.Assistants.Helpers
{
	public class SQLStatementHelper
	{
		private string _tableName;
		private string _pKeyName;
		public SQLStatementHelper(string tablename, string pKeyName)
		{
			_tableName = tablename;
			_pKeyName = pKeyName;
		}

		/// <summary>
		/// Insert Statement
		/// </summary>
		/// <param name="dict"></param>
		/// <returns></returns>
		[Obsolete("Not Used")]
		public string Generate_Insert(Dictionary<string, object> dict)
		{
			var colNames = dict.Select(d => d.Key).ToArray();
			var cols = String.Join(",", colNames);
			var colValues = String.Join(",", colNames.Select(n => "@" + n));
			return String.Format("INSERT INTO {0} ({1}) Values ({2})",
								_tableName, cols, colValues);
		}

		/// <summary>
		/// Update Statement
		/// </summary>
		/// <param name="dict"></param>
		/// <param name="pKeyValue"></param>
		/// <returns></returns>
		public string Generate_Update(Dictionary<string, object> dict, string pKeyValue)
		{
			var condition = String.Format("Where {0} = {1}", _pKeyName, pKeyValue);
			var setValues = get_set_statement(dict);
			var queryPattern = @"
				UPDATE {0}
				SET {1}
				{2}";
			return String.Format(queryPattern, _tableName, setValues, condition);
		}

		/// <summary>
		/// Delete Statement
		/// </summary>
		/// <param name="pKeyValue"></param>
		/// <returns></returns>
		public string Generate_Delete(string pKeyValue)
		{
			return String.Format("Delete From {0} Where {1} = {2}", _tableName, _pKeyName, pKeyValue);
		}

		private string get_set_statement(Dictionary<string, object> columnValueDict)
		{
			var colNames = columnValueDict.Select(d => String.Format("{0}=@{1}\r\n",
													getNormalizedColumnName(d.Key), d.Key)).ToArray();
			return String.Join(",", colNames);
		}
		private string getNormalizedColumnName(string name)
		{
			var cName = name;
			if (name == "WFP")
			{
				cName = "WFP(WFN)";
			}
			else if (name == "ISO")
			{
				cName = "WFP(ISO14046)";
			}
			return String.Format("[{0}]", cName);
		}
	}
}
