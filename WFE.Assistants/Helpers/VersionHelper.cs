﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace WFE.Assistants.Helpers
{
	public static class VersionHelper
	{
		public static string Version
		{
			get
			{
				Assembly asm = Assembly.GetExecutingAssembly();

				return asm.GetName().Version.ToString();
			}
		}
	}
}
