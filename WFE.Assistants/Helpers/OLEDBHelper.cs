﻿using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFE.Assistants.Models.SQL;
using WFE.Assistants.Repos;

namespace WFE.Assistants.Helpers
{
	/// <summary>
	/// OLE DB SQL Helerp
	/// <para>產生 SQL 句</para>
	/// </summary>
	public class OLEDBHelper : RepoBase
	{
		/// <summary>
		/// Execute Insert Statment and return KeySN
		/// <para>Insert Statement</para>
		/// </summary>
		/// <param name="olesql"></param>
		/// <param name="keySN"></param>
		/// <returns></returns>
		public bool Execute(OLESQL olesql, out int keySN)
		{
			var success = true;
			keySN = 0;
			using (var connection = new OleDbConnection(base.Conn))
			{
				var cmd = new OleDbCommand();
				OleDbTransaction transaction = null;

				// Set the Connection to the new OleDbConnection.
				cmd.Connection = connection;

				try
				{
					connection.Open();
					// Start a local transaction with ReadCommitted isolation level.
					transaction = connection.BeginTransaction(System.Data.IsolationLevel.ReadCommitted);

					// Assign transaction object for a pending local transaction.
					cmd.Connection = connection;
					cmd.Transaction = transaction;

					// Execute the commands.
					cmd.Parameters.AddRange(olesql.Parameters.ToArray());
					cmd.CommandText = olesql.SQL;
					cmd.ExecuteNonQuery();
					cmd.Parameters.Clear();

					//Get Insert Statment Primary Key
					cmd.CommandText = @"SELECT @@Identity;";
					var reader = cmd.ExecuteReader();
					while (reader.Read())
					{
						keySN = Convert.ToInt32(reader[0]);
					}
					// Commit the transaction.
					transaction.Commit();

				}
				catch (OleDbException ex)
				{
					success = false;
					transaction.Rollback();
					base.ErrMsg = ex.Message;
				}
			}
			return success;
		}

		/// <summary>
		/// Excute SQL Statment without Query
		/// <para>Update, Delete statment</para>
		/// </summary>
		/// <param name="olesql"></param>
		/// <returns></returns>
		public bool Execute(OLESQL olesql)
		{
			var success = true;
			using (var connection = new OleDbConnection(base.Conn))
			{
				var cmd = new OleDbCommand();
				OleDbTransaction transaction = null;

				// Set the Connection to the new OleDbConnection.
				cmd.Connection = connection;

				try
				{
					connection.Open();
					// Start a local transaction with ReadCommitted isolation level.
					transaction = connection.BeginTransaction(System.Data.IsolationLevel.ReadCommitted);

					// Assign transaction object for a pending local transaction.
					cmd.Connection = connection;
					cmd.Transaction = transaction;

					// Execute the commands.
					cmd.Parameters.AddRange(olesql.Parameters.ToArray());
					cmd.CommandText = olesql.SQL;
					cmd.ExecuteNonQuery();
					cmd.Parameters.Clear();

					// Commit the transaction.
					transaction.Commit();
				}
				catch (Exception ex)
				{
					success = false;
					transaction.Rollback();
					base.ErrMsg = ex.Message;
				}
			}
			return success;
		}

		/// <summary>
		/// 批次執行
		/// </summary>
		/// <param name="olesqls"></param>
		/// <returns></returns>
		public bool BatchExecute(List<OLESQL> olesqls)
		{
			var success = true;
			using (var connection = new OleDbConnection(base.Conn))
			{
				var cmd = new OleDbCommand();
				OleDbTransaction transaction = null;

				// Set the Connection to the new OleDbConnection.
				cmd.Connection = connection;
				try
				{
					connection.Open();
					// Start a local transaction with ReadCommitted isolation level.
					transaction = connection.BeginTransaction(System.Data.IsolationLevel.ReadCommitted);

					// Assign transaction object for a pending local transaction.
					cmd.Connection = connection;
					cmd.Transaction = transaction;

					// Execute the commands.
					foreach (var olesql in olesqls)
					{
						cmd.Parameters.AddRange(olesql.Parameters.ToArray());
						cmd.CommandText = olesql.SQL;
						cmd.ExecuteNonQuery();
						cmd.Parameters.Clear();
					}
					// Commit the transaction.
					transaction.Commit();
				}
				catch (Exception ex)
				{
					success = false;
					transaction.Rollback();
					base.ErrMsg = ex.Message;
				}
			}
			return success;
		}
	}
}
