﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFE.Assistants.Helpers
{
	/// <summary>
	/// 單位轉換
	/// </summary>
	public static class UnitHelper
	{
		public static string VolumnNormalize(string component_Volumn)
		{
			string result = component_Volumn;
			double volumn;
			var v1 = Double.TryParse(component_Volumn, out volumn);
			if (v1)
			{
				//單位轉換, 體積取得為 mm3 轉成 m3, 
				result = (volumn / Math.Pow(1000, 3)).ToString();
			}
			return result;
		}
	}
}
