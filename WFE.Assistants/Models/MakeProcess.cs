﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFE.Assistants.Models
{
	/// <summary>
	/// 加工方式
	/// </summary>
	public class MakeProcess
	{
		/// <summary>
		/// 識別碼
		/// </summary>
		[DisplayName("序號")]
		public int SN { get; set; }

		/// <summary>
		/// 方式
		/// </summary>
		[DisplayName("加工方式")]
		public string MakeProcessName { get; set; }

		/// <summary>
		/// 水足跡計算分類
		/// </summary>
		public string CalculateCode { get; set; }

		/// <summary>
		/// 水足跡 (m3)
		/// </summary>
		[DisplayName("水足跡(WFN)")]
		public string WFP { get; set; }

		/// <summary>
		/// 水足跡 ISO 14046
		/// </summary>
		[DisplayName("水足跡(ISO 14046)")]
		public string ISO { get; set; }

		/// <summary>
		/// BOD
		/// </summary>
		[DisplayName("BOD")]
		public string BOD { get; set; }

		/// <summary>
		/// 藍水(m3)
		/// </summary>
		[DisplayName("藍水")]
		public string BlueWater { get; set; }

		/// <summary>
		/// 綠水(m3)
		/// </summary>
		[DisplayName("綠水")]
		public string GreenWater { get; set; }

		/// <summary>
		/// 灰水(m3)
		/// </summary>
		[DisplayName("灰水")]
		public string GrayWater { get; set; }

		/// <summary>
		/// 機具名稱
		/// </summary>
		public string MachineName { get; set; }

		/// <summary>
		/// 能源/單位
		/// </summary>
		public string EnergyPerUnit { get; set; }

		/// <summary>
		/// 電力消耗量
		/// </summary>
		[DisplayName("電力消耗量 Kw")]
		public string PowerConsumption { get; set; }

		[DisplayName("電力系數 m^3/Kw．hr")]
		public string PowerCOEFF { get; set; }

		/// <summary>
		/// 資料來源
		/// </summary>
		public string DataSource { get; set; }
	}
}
