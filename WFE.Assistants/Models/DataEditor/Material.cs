﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFE.Assistants.Models.DataEditor
{
	/// <summary>
	/// 材料 - 欄位與資料庫一致
	/// </summary>
	public class Material
	{
		/// <summary>
		/// 識別碼
		/// </summary>
		[DisplayName("序號")]
		public string SN { get; set; }

		/// <summary>
		/// 材料
		/// </summary>
		[DisplayName("名稱")]
		public string MaterialName { get; set; }

		/// <summary>
		/// 密度kg/m3
		/// </summary>
		[DisplayName("密度 kg/m3")]
		public string Density { get; set; }

		/// <summary>
		/// 藍水(m3)
		/// </summary>
		[DisplayName("藍水 m^3/Kg")]
		public string BlueWater { get; set; }

		/// <summary>
		/// 綠水(m3)
		/// </summary>
		[DisplayName("綠水 m^3/Kg")]
		public string GreenWater { get; set; }

		/// <summary>
		/// 灰水(m3)
		/// </summary>
		[DisplayName("灰水 m^3/Kg")]
		public string GrayWater { get; set; }

		[DisplayName("BOD mg/Kg")]
		public string BOD { get; set; }

		/// <summary>
		/// 總水足跡 (m3)
		/// </summary>
		[DisplayName("總水足跡(WFN) m^3/Kg")]
		public string WFP { get; set; }

		[DisplayName("總水足跡(ISO) m^3/Kg")]
		public string ISO { get; set; }

		[DisplayName("資料來源")]
		public string DataSource { get; set; }

		[DisplayName("建立日期")]
		public string CreateDate { get; set; }

		[DisplayName("修改日期")]
		public string ModifiedDate { get; set; }
	}
}
