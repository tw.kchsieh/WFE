﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFE.Assistants.Models.DataEditor
{
	public class AssemblyMethod
	{
		/// <summary>
		/// 序號
		/// </summary>
		public string SN { get; set; }

		[DisplayName("名稱")]
		public string AssemblyMethodName { get; set; }

		[DisplayName("單位")]
		public string CalculateUnit { get; set; }

		[DisplayName("電力消耗量 Kw")]
		public string PowerConsumption { get; set; }

		[DisplayName("電力係數 m^3/Kg．hr")]
		public string PowerCoefficient { get; set; }

		[DisplayName("備註")]
		public string Remark { get; set; }

		[DisplayName("資料來源")]
		public string DataSource { get; set; }

		[DisplayName("建立日期")]
		public string CreateDate { get; set; }

		[DisplayName("修改日期")]
		public string ModifiedDate { get; set; }
	}
}
