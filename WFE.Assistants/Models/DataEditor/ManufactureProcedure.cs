﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFE.Assistants.Models.DataEditor
{
	/// <summary>
	/// 特殊零件製程 - 欄位與資料庫一致
	/// </summary>
	public class ManufactureProcedure
	{
		[DisplayName("序號")]
		public string SN { get; set; }

		[DisplayName("名稱")]
		public string ProcessName { get; set; }

		[DisplayName("製程分類代碼")]
		public string ProcessTypeCode { get; set; }

		[DisplayName("單位")]
		public string UnitCode { get; set; }

		[DisplayName("密度 kg/m3")]
		public string Density { get; set; }

		[DisplayName("藍水 m^3/Kg")]
		public string BlueWater { get; set; }

		[DisplayName("綠水 m^3/Kg")]
		public string GreenWater { get; set; }

		[DisplayName("灰水 m^3/Kg")]
		public string GrayWater { get; set; }

		[DisplayName("BOD mg/Kg")]
		public string BOD { get; set; }

		[DisplayName("總水足跡(WFN) m^3/Kg")]
		public string WFP { get; set; }

		[DisplayName("總水足跡(ISO) m^3/Kg")]
		public string ISO { get; set; }

		[DisplayName("資料來源")]
		public string DataSource { get; set; }

		[DisplayName("建立日期")]
		public string CreateDate { get; set; }

		[DisplayName("修改日期")]
		public string ModifiedDate { get; set; }
	}
}
