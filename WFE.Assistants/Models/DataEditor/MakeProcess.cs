﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFE.Assistants.Models.DataEditor
{
	/// <summary>
	/// 加工方式 - 欄位與資料庫一致
	/// </summary>
	public class MakeProcess
	{
		[DisplayName("序號")]
		public string SN { get; set; }

		[DisplayName("名稱")]
		public string MakeProcessName { get; set; }

		[DisplayName("計算公式分類")]
		public string CalculateCode { get; set; }

		[DisplayName("計算單位")]
		public string CalculateUnit { get; set; }

		[DisplayName("藍水 m^3/Kg")]
		public string BlueWater { get; set; }

		[DisplayName("綠水 m^3/Kg")]
		public string GreenWater { get; set; }

		[DisplayName("灰水 m^3/Kg")]
		public string GrayWater { get; set; }

		[DisplayName("BOD mg/Kg")]
		public string BOD { get; set; }

		[DisplayName("總水足跡(WFN) m^3/Kg")]
		public string WFP { get; set; }

		[DisplayName("總水足跡(ISO) m^3/Kg")]
		public string ISO { get; set; }

		[DisplayName("機具名稱")]
		public string MachineName { get; set; }

		[DisplayName("能源/單位")]
		public string EnergyPerUnit { get; set; }

		[DisplayName("電力消耗量 Kw")]
		public string PowerConsumption { get; set; }

		[DisplayName("電力系數 m^3/Kw．hr")]
		public string PowerCoefficient { get; set; }

		[DisplayName("資料來源")]
		public string DataSource { get; set; }

		[DisplayName("建立日期")]
		public string CreateDate { get; set; }

		[DisplayName("修改日期")]
		public string ModifiedDate { get; set; }

	}
}
