﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WFE.Assistants.Models.AssemblyPageViewModels
{
	/// <summary>
	/// 組裝清單
	/// </summary>
	public class Component_AssemblyItem
	{
		public int SN { get; set; }
		public int ProductSN { get; set; }
		public string ComponentID { get; set; }
		public string ComponentName { get; set; }
		public string ParentID { get; set; }
		public DateTime CreateDate { get; set; }

		public TreeNode ToTreeNode()
		{
			return new TreeNode()
			{
				Name = this.ComponentID,
				Text = this.ComponentName,
				Checked = false,
				Tag = ParentID,
			};
		}
	}
}
