﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFE.Assistants.Models.AssemblyPageViewModels
{
	/// <summary>
	/// 產品組裝記錄
	/// <para>For View</para>
	/// </summary>
	public class AssemblyRecord
	{
		/// <summary>
		/// 零件ID
		/// </summary>
		public string ComponentID { get; set; }

		/// <summary>
		/// 組裝後的零件名稱
		/// </summary>
		[DisplayName("零件")]
		public string ComponentName { get; set; }

		[DisplayName("組裝方式")]
		public string AssemblyMethodName { get; set; }

		[DisplayName("組裝水足跡 (WFN)")]
		public string WFN { get; set; }

		[DisplayName("組裝水足跡 (ISO)")]
		public string ISO { get; set; }

		[DisplayName("組裝運輸水足跡 (WFN)")]
		public string TransportWFN { get; set; }

		[DisplayName("組裝運輸水足跡 (ISO)")]
		public string TransportISO { get; set; }
	}
}
