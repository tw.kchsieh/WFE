﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFE.Assistants.Models.AssemblyPageViewModels
{
	/// <summary>
	/// Page Assembly View Record
	/// <para>Table Join View</para>
	/// </summary>
	public class AssemblyTransportRecord
	{
		[DisplayName("序號")]
		public int SN { get; set; }

		/// <summary>
		/// 產品序號
		/// </summary>
		public int ProductSN { get; set; }

		/// <summary>
		/// 產品建立時間
		/// </summary>
		public DateTime ProductCreateDate { get; set; }

		[DisplayName("零件名稱")]
		public string ComponentName { get; set; }

		/// <summary>
		/// 零件ID
		/// </summary>
		public string ComponentID
		{
			get
			{
				//TODO: Use Item CreateDate
				return this.ProductCreateDate.ToString("yyyyMMddHHmmss");
			}
		}

		/// <summary>
		/// 一般;後製;特殊
		/// </summary>
		[DisplayName("零件類型")]
		public string ComponentType { get; set; }

		/// <summary>
		/// 運輸SN
		/// </summary>
		public int TransportSN { get; set; }

		[DisplayName("Stage A, B 總水足跡 WFN")]
		public string WFN { get; set; }

		[DisplayName("Stage A, B 總水足跡 ISO 14046")]
		public string ISO { get; set; }

		[DisplayName("組裝廠運輸總水足跡 WFN")]
		public string AssemblyTransportWFN { get; set; }

		[DisplayName("組裝廠運輸總水足跡 ISO 14046")]
		public string AssemblyTransportISO { get; set; }

	}
}
