﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFE.Assistants.Helpers;

namespace WFE.Assistants.Models.AssemblyPageViewModels
{
	public class Component_AssemblyDetail
	{
		public int SN { get; set; }
		public string ComponentName { get; set; }
		public string ComponentID { get; set; }
		public int AssemblyMethodSN { get; set; }
		public string AssemblyMethodName { get; set; }
		public string PowerConsumption { get; set; }
		public string PowerCoefficient { get; set; }
		public string WorkingHour { get; set; }

		/// <summary>
		/// 工作時數 * 組裝 WFN 水足跡
		/// </summary>
		public string CalcWFP
		{
			get
			{
				var result = "0";
				double w, h, coeff;
				var v1 = Double.TryParse(this.PowerConsumption, out w);
				var v2 = Double.TryParse(this.WorkingHour, out h);
				var v3 = Double.TryParse(this.PowerCoefficient, out coeff);
				if (v1 && v2 && v3)
				{
					result = UIHelper.NumberTextFormat(w * h * coeff);
				}
				return result;
			}
		}
		/// <summary>
		/// 工作時數 * 組裝 ISO 水足跡
		/// </summary>
		public string CalcISO
		{
			get
			{
				return this.CalcWFP;
				/*
				var result = "0";
				double w, h;
				var v1 = Double.TryParse(this.PowerCoefficient, out w);
				var v2 = Double.TryParse(this.WorkingHour, out h);
				if (v1 && v2)
				{
					result = UIHelper.NumberTextFormat(w * h);
				}
				return result;
				*/
			}
		}

		public Component_AssemblyDetail()
		{
			ComponentID = DateTime.Now.ToString("yyyyMMddHHmmss");
		}

		public bool Valid()
		{
			return !String.IsNullOrEmpty(ComponentName)
				&& ComponentName.Length <= 255
				&& !String.IsNullOrEmpty(ComponentID)
				&& AssemblyMethodSN > 0
				&& !String.IsNullOrEmpty(AssemblyMethodName)
				&& !String.IsNullOrEmpty(PowerConsumption)
				&& !String.IsNullOrEmpty(PowerCoefficient)
				&& !String.IsNullOrEmpty(WorkingHour)
				;
		}
	}
}
