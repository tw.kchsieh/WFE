﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFE.Assistants.Models.AssemblyPageViewModels
{
	public class ComponentTransportToAssemblyDTO
	{
		[DisplayName("零件")]
		public string ComponentName { get; set; }
		[DisplayName("來源")]
		public string ComponentType { get; set; }

		public string ComponentTypeCode { get; set; }

		/// <summary>
		/// 質量
		/// </summary>
		public string ComponentMass { get; set; }

		[DisplayName("前階段總水足跡 (WFN)")]
		public string PreviousStageWFN { get; set; }
		[DisplayName("前階段總水足跡 (ISO)")]
		public string PreviousStageISO { get; set; }

		[DisplayName("組裝運輸水足跡 (WFN)")]
		public string TransportWFN { get; set; }
		[DisplayName("組裝運輸水足跡 (ISO)")]
		public string TransportISO { get; set; }

		[DisplayName("零件ID")]
		public string ComponentID
		{
			get
			{
				var id = "";
				if (ComponentCreateDate != null)
				{
					id = ComponentCreateDate.ToString("yyyyMMddHHmmss");
				}
				return id;
			}
		}
		public DateTime ComponentCreateDate { get; set; }

	}
}
