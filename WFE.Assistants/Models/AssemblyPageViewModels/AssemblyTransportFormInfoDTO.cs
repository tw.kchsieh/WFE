﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFE.Assistants.Models.AssemblyPageViewModels
{
	public class AssemblyTransportFormInfoDTO
	{
		#region properties
		public string FormText { get; set; }
		public string ProductText { get; set; }
		public string C_Name { get; set; }
		public string C_Mass { get; set; }
		public string ProductName { get; set; }
		#endregion
		public AssemblyTransportFormInfoDTO(string productName)
		{
			FormText = "選擇運輸工具 (組裝)";
			ProductText = "產品";
			C_Name = "零件名稱";
			C_Mass = "零件質量";
			ProductName = productName;
		}
	}
}
