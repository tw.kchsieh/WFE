﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFE.Assistants.Models
{
	public class AssemblyMethod
	{
		/// <summary>
		/// 識別碼
		/// </summary>
		[DisplayName("序號")]
		public int SN { get; set; }

		/// <summary>
		/// 材料
		/// </summary>
		[DisplayName("組裝方式")]
		public string AssemblyMethodName { get; set; }

		/// <summary>
		/// 單位
		/// </summary>
		[DisplayName("單位")]
		public string CalculateUnit { get; set; }

		/// <summary>
		/// 總水足跡 (m3)
		/// <para>藍水 + 綠水 + 灰水</para>
		/// </summary>
		[DisplayName("電力消耗量 Kw")]
		public string PowerConsumption { get; set; }

		///// <summary>
		///// BOD
		///// <para>水質污染量</para>
		///// </summary>
		//[DisplayName("BOD")]
		//public string BOD { get; set; }

		/// <summary>
		/// ISO-14046
		///<para>水量 (藍水)</para>
		/// </summary>
		[DisplayName("電力係數 m^3/Kw．hr")]
		public string PowerCoefficient { get; set; }

		/// <summary>
		/// 備註
		/// </summary>
		[DisplayName("備註")]
		public string Remark { get; set; }

		/// <summary>
		/// 資料來源
		/// </summary>
		[DisplayName("資料來源")]
		public string DataSource { get; set; }
	}
}
