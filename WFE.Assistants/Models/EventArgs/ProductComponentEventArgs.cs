﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFE.Assistants.Models.ORDs;

namespace WFE.Assistants.Models.EventArgs
{
	public class ProductComponentEventArgs
	{
		public ProductComponent Data;
	}

}
