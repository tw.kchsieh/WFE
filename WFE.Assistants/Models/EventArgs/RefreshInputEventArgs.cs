﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFE.Assistants.Models.EventArgs
{
	public class RefreshInputEventArgs
	{
		public Dictionary<string, string> RequiredInputDict { get; set; }
		public RefreshInputEventArgs()
		{
			RequiredInputDict = new Dictionary<string, string>();
		}
	}
}
