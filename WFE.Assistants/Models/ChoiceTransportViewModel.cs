﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFE.Assistants.Helpers;

namespace WFE.Assistants.Models
{
	/// <summary>
	/// 選擇的運輸工具 ViewModel
	/// </summary>
	public class ChoiceTransportViewModel
	{
		private int _C_SN;
		/// <summary>
		/// 原料/零件 SN
		/// </summary>
		public int C_SN
		{
			get { return _C_SN; }
			set
			{
				if (value == _C_SN)
					return;

				_C_SN = value;
				OnPropertyChanged("C_SN");
			}
		}

		private string _C_Name;
		/// <summary>
		/// 原料/零件 C_Name
		/// </summary>
		public string C_Name
		{
			get { return _C_Name; }
			set
			{
				if (value == _C_Name)
					return;

				_C_Name = value;
				OnPropertyChanged("C_Name");
			}
		}


		private string _Mass;
		/// <summary>
		/// 原料/零件 Mass
		/// </summary>
		public string Mass
		{
			get
			{
				double mass;
				string result = _Mass;
				var v1 = double.TryParse(_Mass, out mass);
				if (v1)
				{
					result = UIHelper.NumberTextFormat(mass);
				}
				return result;
			}
			set
			{
				if (value == _Mass)
					return;

				_Mass = value;
				OnPropertyChanged("Mass");
			}
		}

		private int _SN;
		/// <summary>
		/// 運輸工具 SN
		/// </summary>
		public int SN
		{
			get { return _SN; }
			set
			{
				if (value == _SN)
					return;

				_SN = value;
				OnPropertyChanged("SN");
			}
		}

		private string _Name;
		/// <summary>
		/// 運輸工具 Name
		/// </summary>
		public string Name
		{
			get { return _Name; }
			set
			{
				if (value == _Name)
					return;

				_Name = value;
				OnPropertyChanged("Name");
			}
		}

		private string _Distance;
		/// <summary>
		/// Distance
		/// </summary>
		public string Distance
		{
			get
			{
				double distance;
				string result = _Distance;
				var v1 = double.TryParse(_Distance, out distance);
				if (v1)
				{
					result = UIHelper.NumberTextFormat(distance);
				}
				return result;
			}
			set
			{
				if (value == _Distance)
					return;

				_Distance = value;
				OnPropertyChanged("Distance");
			}
		}

		private string _TransportWFP;
		/// <summary>
		/// TransportWFP (運輸水足跡 WFN)
		/// </summary>
		public string TransportWFP
		{
			get
			{
				double wfp;
				string result = _TransportWFP;
				if (double.TryParse(_TransportWFP, out wfp))
				{
					result = UIHelper.NumberTextFormat(wfp);
				}
				return result;
			}
			set
			{
				if (value == _TransportWFP)
					return;

				_TransportWFP = value;
				OnPropertyChanged("TransportWFP");
			}
		}

		private string _TransportISO;
		/// <summary>
		/// TransportISO (運輸水足跡 ISO 14046)
		/// </summary>
		public string TransportISO
		{
			get
			{
				double wfp;
				string result = _TransportISO;
				if (double.TryParse(_TransportISO, out wfp))
				{
					result = UIHelper.NumberTextFormat(wfp);
				}
				return result;
			}
			set
			{
				if (value == _TransportISO)
					return;

				_TransportISO = value;
				OnPropertyChanged("TransportISO");
			}
		}

		private string _WFP;
		/// <summary>
		/// 水足跡 (WFN)
		/// </summary>
		public string WFP
		{
			get
			{
				double mass, transportWFP, distance;
				var v1 = double.TryParse(Mass, out mass);
				var v2 = double.TryParse(TransportWFP, out transportWFP);
				var v3 = double.TryParse(Distance, out distance);
				if (v1 && v2 && v3)
				{
					_WFP = UIHelper.NumberTextFormat((mass) * transportWFP * distance);
				}
				else
				{
					_WFP = "N/A";
				}
				return _WFP;
			}
		}

		private string _ISO;
		/// <summary>
		/// 水足跡 (ISO 14046)
		/// </summary>
		public string ISO
		{
			get
			{
				double mass, transportISO, distance;
				var v1 = double.TryParse(Mass, out mass);
				var v2 = double.TryParse(TransportISO, out transportISO);
				var v3 = double.TryParse(Distance, out distance);
				if (v1 && v2 && v3)
				{
					_ISO = UIHelper.NumberTextFormat((mass) * transportISO * distance);
				}
				else
				{
					_ISO = "N/A";
				}
				return _ISO;
			}
		}

		#region INotifyPropertyChanged

		/// <summary>
		/// The PropertyChanged event is used by consuming code
		/// (like WPF's binding infrastructure) to detect when
		/// a value has changed.
		/// </summary>
		public event PropertyChangedEventHandler PropertyChanged;

		/// <summary>
		/// Raise the PropertyChanged event for the 
		/// specified property.
		/// </summary>
		/// <param name="propertyName">
		/// A string representing the name of 
		/// the property that changed.</param>
		/// <remarks>
		/// Only raise the event if the value of the property 
		/// has changed from its previous value</remarks>
		protected void OnPropertyChanged(string propertyName)
		{
			{
				// Validate the property name in debug builds
				VerifyProperty(propertyName);

				if (null != PropertyChanged)
				{
					{
						PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
					}
				}
			}
		}

		/// <summary>
		/// Verifies whether the current class provides a property with a given
		/// name. This method is only invoked in debug builds, and results in
		/// a runtime exception if the <see cref="OnPropertyChanged"/> method
		/// is being invoked with an invalid property name. This may happen if
		/// a property's name was changed but not the parameter of the property's
		/// invocation of <see cref="OnPropertyChanged"/>.
		/// </summary>
		/// <param name="propertyName">The name of the changed property.</param>
		[System.Diagnostics.Conditional("DEBUG")]
		private void VerifyProperty(string propertyName)
		{
			{
				Type type = this.GetType();

				// Look for a *public* property with the specified name
				System.Reflection.PropertyInfo pi = type.GetProperty(propertyName);
				if (pi == null)
				{
					{
						// There is no matching property - notify the developer
						string msg = "OnPropertyChanged was invoked with invalid " +
								"property name {{0}}. {{0}} is not a public " +
								"property of {{1}}.";
						msg = String.Format(msg, propertyName, type.FullName);
						System.Diagnostics.Debug.Fail(msg);
					}
				}
			}
		}

		#endregion
	}
}
