﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFE.Assistants.Models.ORDs;

namespace WFE.Assistants.Models
{
	/// <summary>
	/// 選擇加工方式資料模型
	/// </summary>
	public class MakeProcessFormInfoDTO
	{
		public string FormText { get; set; }
		public string ProductText { get; set; }
		public string C_Text { get; set; }
		public string CalcWFP_Text { get; set; }
		public string Mass_Text { get; set; }

		public string ProductName;
		public ProductComponent Component;
		public MakeProcessFormInfoDTO(string code)
		{
			switch (code.ToLower())
			{
				case "stagea":
					FormText = "選擇加工方式 (一般零件)";
					ProductText = "專案";
					C_Text = "零件名稱";
					Mass_Text = "零件質量";
					CalcWFP_Text = "零件加工水足跡";
					break;
				case "stageb":
					FormText = "選擇加工方式 (後製加工)";
					ProductText = "專案";
					C_Text = "零件名稱";
					Mass_Text = "零件質量";
					CalcWFP_Text = "零件加工水足跡";
					break;
			}
		}

		public void UseModel(string productName, ProductComponent curComponent)
		{
			this.ProductName = productName;
			this.Component = curComponent;
		}
	}
}
