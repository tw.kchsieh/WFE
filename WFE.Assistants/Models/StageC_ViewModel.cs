﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFE.Assistants.Helpers;
using WFE.Assistants.Models.ORDs;

namespace WFE.Assistants.Models
{
	public class StageC_ViewModel
	{
		internal bool Clear()
		{
			//特殊元件
			this.Component_Name = "";
			this.Component_Volumn = "";
			this.Component_Area = "";

			//製程
			this.Manufacture_SN = "0";
			this.Manufacture_Name = "";
			this.Manufacture_ProcessTypeCode = "";
			this.Manufacture_Density = "";
			this.Manufacture_WFP = "";
			this.Manufacture_ISO = "";

			this.SelectedSpeialComponentType = "";

			CreateDate = DateTime.Now;

			return true;
		}
		public DateTime CreateDate { get; set; }
		public StageC_ViewModel()
		{
			//CreateDate = DateTime.Now;
		}
		public ProductSpecialComponent ToProductSpecialComponent()
		{
			var model = new ProductSpecialComponent()
			{
				//特殊元件
				Component_Name = this.Component_Name,
				Component_Volumn = this.Component_Volumn,
				Component_Area = this.Component_Area,

				//
				SelectedSpeialComponentType = this.SelectedSpeialComponentType,

				//製程
				ManufactureProcedureSN = this.Manufacture_SN,
				ManufactureProcedure_Name = this.Manufacture_Name,
				ManufactureProcedure_ProcessTypeCode = this.Manufacture_ProcessTypeCode,
				ManufactureProcedure_Density = this.Manufacture_Density,
				ManufactureProcedure_WFP = this.Manufacture_WFP,
				ManufactureProcedure_ISO = this.Manufacture_ISO,

				CreateDate = DateTime.Now
			};
			return model;
		}
		#region properties
		private string _Component_Name;
		/// <summary>
		/// Component_Name
		/// </summary>
		public string Component_Name
		{
			get { return _Component_Name; }
			set
			{
				if (value == _Component_Name)
					return;

				_Component_Name = value;
				OnPropertyChanged("Component_Name");
			}
		}


		private string _Component_Type;
		/// <summary>
		/// Component_Type (電路版(軟、硬); LCD; 電子元件)
		/// </summary>
		public string Component_Type
		{
			get { return _Component_Type; }
			set
			{
				if (value == _Component_Type)
					return;

				_Component_Type = value;
				OnPropertyChanged("Component_Type");
			}
		}



		private string _Component_Area;
		/// <summary>
		/// Component_Area
		/// </summary>
		public string Component_Area
		{
			get
			{
				double area;
				var result = _Component_Area;
				if (double.TryParse(_Component_Area, out area))
				{
					result = UIHelper.NumberTextFormat(area);
				}
				return result;
			}
			set
			{
				if (value == _Component_Area)
					return;

				_Component_Area = value;
				OnPropertyChanged("Component_Area");
			}
		}

		/// <summary>
		/// Component_Mass (readOnly)
		/// <para>計算欄位</para>
		/// </summary>
		public string Component_Mass
		{
			get
			{
				//質量 = 密度 * 體積
				var result = "0";
				double density, volumn;
				var v1 = Double.TryParse(Manufacture_Density, out density);
				var v2 = Double.TryParse(Component_Volumn, out volumn);
				if (v1 && v2)
				{
					result = UIHelper.NumberTextFormat(density * volumn);
				}
				return result;
			}
		}

		private string _Manufacture_Name;
		/// <summary>
		/// Manufacture_Name
		/// </summary>
		public string Manufacture_Name
		{
			get { return _Manufacture_Name; }
			set
			{
				if (value == _Manufacture_Name)
					return;

				_Manufacture_Name = value;
				OnPropertyChanged("Manufacture_Name");
			}
		}

		private string _Manufacture_SN;
		/// <summary>
		/// Manufacture_SN
		/// </summary>
		public string Manufacture_SN
		{
			get { return _Manufacture_SN; }
			set
			{
				if (value == _Manufacture_SN)
					return;

				_Manufacture_SN = value;
				OnPropertyChanged("Manufacture_SN");
			}
		}

		private string _Manufacture_Density;
		/// <summary>
		/// Manufacture_Density
		/// </summary>
		public string Manufacture_Density
		{
			get
			{
				double density;
				var result = _Manufacture_Density;
				if (Double.TryParse(_Manufacture_Density, out density))
				{
					result = UIHelper.NumberTextFormat(density);
				}
				return result;
			}
			set
			{
				if (value == _Manufacture_Density)
					return;

				_Manufacture_Density = value;
				OnPropertyChanged("Manufacture_Density");
			}
		}

		private string _Component_Volumn;
		/// <summary>
		/// Component_Volumn
		/// </summary>
		public string Component_Volumn
		{
			get
			{
				double volumn;
				var result = _Component_Volumn;
				if (Double.TryParse(_Component_Volumn, out volumn))
				{
					result = UIHelper.NumberTextFormat((volumn / Math.Pow(1000, 3)));

				}
				return result;
			}
			set
			{
				if (value == _Component_Volumn)
					return;

				_Component_Volumn = value;
				OnPropertyChanged("Component_Volumn");
			}
		}

		private string _Manufacture_WFP;
		/// <summary>
		/// Manufacture_WFP
		/// </summary>
		public string Manufacture_WFP
		{
			get
			{
				var result = _Manufacture_WFP;
				double wfp;
				if (Double.TryParse(_Manufacture_WFP, out wfp))
				{
					result = UIHelper.NumberTextFormat(wfp);
				}
				return result;
			}
			set
			{
				if (value == _Manufacture_WFP)
					return;

				_Manufacture_WFP = value;
				OnPropertyChanged("Manufacture_WFP");
			}
		}
		private string _Manufacture_ISO;
		/// <summary>
		/// Manufacture_ISO 14046
		/// </summary>
		public string Manufacture_ISO
		{
			get
			{
				var result = _Manufacture_ISO;
				double wfp;
				if (Double.TryParse(_Manufacture_ISO, out wfp))
				{
					result = UIHelper.NumberTextFormat(wfp);
				}
				return result;
			}
			set
			{
				if (value == _Manufacture_ISO)
					return;

				_Manufacture_ISO = value;
				OnPropertyChanged("Manufacture_ISO");
			}
		}

		private string _Manufacture_ProcessTypeCode;
		/// <summary>
		/// Manufacture_ProcessTypeCode
		/// </summary>
		public string Manufacture_ProcessTypeCode
		{
			get { return _Manufacture_ProcessTypeCode; }
			set
			{
				if (value == _Manufacture_ProcessTypeCode)
					return;

				_Manufacture_ProcessTypeCode = value;
				OnPropertyChanged("Manufacture_ProcessTypeCode");
			}
		}


		private string _WFP;
		/// <summary>
		/// WFP
		/// </summary>
		public string WFP
		{
			get
			{
				var result = "N/A";
				double mass, m_wfp, area;
				var v1 = Double.TryParse(Component_Mass, out mass);
				var v2 = Double.TryParse(Manufacture_WFP, out m_wfp);
				var v3 = Double.TryParse(Component_Area, out area);
				switch (this._Manufacture_ProcessTypeCode)
				{
					case "E":
						//電子元件 : WFP = 質量 * 製程水足跡
						if (v1 && v2)
						{
							result = UIHelper.NumberTextFormat(mass * m_wfp);
						}
						break;
					case "LCD":
					case "M-Hard":
					case "M-Mixed":
						//硬板, LCD
						if (v2 && v3)
						{
							result = UIHelper.NumberTextFormat(m_wfp * area);
						}
						break;
					case "M-Soft":
						//軟板
						if (v2)
						{
							result = UIHelper.NumberTextFormat(m_wfp);
						}
						break;
				}
				return result;
			}
		}
		//
		private string _ISO;
		/// <summary>
		/// ISO
		/// </summary>
		public string ISO
		{
			get
			{
				var result = "N/A";
				double mass, m_ISO, area;
				var v1 = Double.TryParse(Component_Mass, out mass);
				var v2 = Double.TryParse(Manufacture_ISO, out m_ISO);
				var v3 = Double.TryParse(Component_Area, out area);
				switch (this._Manufacture_ProcessTypeCode)
				{
					case "E":
						//電子元件 : ISO = 質量 * 製程水足跡
						if (v1 && v2)
						{
							result = UIHelper.NumberTextFormat(mass * m_ISO);
						}
						break;
					case "LCD":
					case "M-Hard":
					case "M-Mixed":
						//硬板, LCD
						if (v2 && v3)
						{
							result = UIHelper.NumberTextFormat(m_ISO * area);
						}
						break;
					case "M-Soft":
						//軟板
						if (v2)
						{
							result = UIHelper.NumberTextFormat(m_ISO);
						}
						break;
				}
				return result;
			}
		}

		/// <summary>
		/// 選擇的特殊零件類型
		/// </summary>
		public string SelectedSpeialComponentType { get; set; }

		/// <summary>
		/// 表面積
		/// </summary>
		public double Component_SurfaceArea { get; set; }

		public string[] GetCurrentComponentProcessCode()
		{
			string[] codes = null;
			switch (this.SelectedSpeialComponentType)
			{
				case "EBoard":
					codes = new string[] { "M-Hard", "M-Soft", "M-Mixed" };
					break;
				case "EComponent":
					codes = new string[] { "E" };
					break;
				case "LCD":
					codes = new string[] { "LCD" };
					break;
				default:
					codes = new string[] { "" };
					break;
			}
			return codes;
		}
		#endregion

		#region INotifyPropertyChanged

		/// <summary>
		/// The PropertyChanged event is used by consuming code
		/// (like WPF's binding infrastructure) to detect when
		/// a value has changed.
		/// </summary>
		public event PropertyChangedEventHandler PropertyChanged;



		/// <summary>
		/// Raise the PropertyChanged event for the 
		/// specified property.
		/// </summary>
		/// <param name="propertyName">
		/// A string representing the name of 
		/// the property that changed.</param>
		/// <remarks>
		/// Only raise the event if the value of the property 
		/// has changed from its previous value</remarks>
		protected void OnPropertyChanged(string propertyName)
		{
			{
				// Validate the property name in debug builds
				VerifyProperty(propertyName);

				if (null != PropertyChanged)
				{
					{
						PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
					}
				}
			}
		}

		/// <summary>
		/// Verifies whether the current class provides a property with a given
		/// name. This method is only invoked in debug builds, and results in
		/// a runtime exception if the <see cref="OnPropertyChanged"/> method
		/// is being invoked with an invalid property name. This may happen if
		/// a property's name was changed but not the parameter of the property's
		/// invocation of <see cref="OnPropertyChanged"/>.
		/// </summary>
		/// <param name="propertyName">The name of the changed property.</param>
		[System.Diagnostics.Conditional("DEBUG")]
		private void VerifyProperty(string propertyName)
		{
			{
				Type type = this.GetType();

				// Look for a *public* property with the specified name
				System.Reflection.PropertyInfo pi = type.GetProperty(propertyName);
				if (pi == null)
				{
					{
						// There is no matching property - notify the developer
						string msg = "OnPropertyChanged was invoked with invalid " +
								"property name {{0}}. {{0}} is not a public " +
								"property of {{1}}.";
						msg = String.Format(msg, propertyName, type.FullName);
						System.Diagnostics.Debug.Fail(msg);
					}
				}
			}
		}

		public void ClearManufactureProcedureInfo()
		{
			Manufacture_SN = "0";
			Manufacture_Name = "";
			Manufacture_Density = "";
			Manufacture_WFP = "";
			Manufacture_ISO = "";
			Manufacture_ProcessTypeCode = "";
		}

		#endregion


	}
}
