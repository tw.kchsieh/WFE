﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFE.Assistants.Models
{
	/// <summary>
	/// 運輸工具
	/// </summary>
	public class Transport
	{
		[DisplayName("序號")]
		public int SN { get; set; }

		[DisplayName("運輸工具名稱")]
		public string TransportName { get; set; }

		[DisplayName("最大載貨重量")]
		public string MaxLoad { get; set; }

		/// <summary>
		/// 藍水(m3)
		/// </summary>
		[DisplayName("藍水 m^3/Kg")]
		public string BlueWater { get; set; }

		/// <summary>
		/// 綠水(m3)
		/// </summary>
		[DisplayName("綠水 m^3/Kg")]
		public string GreenWater { get; set; }

		/// <summary>
		/// 灰水(m3)
		/// </summary>
		[DisplayName("灰水 m^3/Kg")]
		public string GrayWater { get; set; }

		/// <summary>
		/// BOD
		/// </summary>
		[DisplayName("BOD mg/Kg")]
		public string BOD { get; set; }

		/// <summary>
		/// 總水足跡 (m3)
		/// </summary>
		[DisplayName("總水足跡 (WFN) m^3/Kg")]
		public string WFP { get; set; }

		/// <summary>
		/// 總水足跡 ISO 14046
		/// </summary>
		[DisplayName("總水足跡 (ISO) m^3/Kg")]
		public string ISO { get; set; }


		[DisplayName("計算單位")]
		public string CalcUnit { get; set; }

		[DisplayName("單位")]
		public string UnitOne { get; set; }

		[DisplayName("能源/單位")]
		public string EnergyPerUnit { get; set; }
	}
}
