﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFE.Assistants.Models.ProductCompare
{
	public class ProductComparison
	{
		public ProductComparison()
		{
			this.CreateDate = DateTime.Now.ToString();
			this.ModifiedDate = DateTime.Now.ToString();
		}
		public int SN { get; set; }
		public string ComparisonName { get; set; }
		public string ComparisonDesc { get; set; }
		public string CreateDate { get; set; }
		public string ModifiedDate { get; set; }
	}
}
