﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFE.Assistants.Helpers;
using WFE.Assistants.Models.ProductCompare.Enums;

namespace WFE.Assistants.Models.ProductCompare
{
	/// <summary>
	/// 產品比較 Detail
	/// </summary>
	public class ProductComparisonDetail
	{
		public ProductComparisonDetail()
		{
			/*
			this.SN = 1;
			this.WFP = 1;
			this.ISO = 2;
			this.Importance = 4;
			this.P1 = 1;
			this.P2 = 2;
			this.P3 = 4;
			this.W1 = 2.5;
			this.W2 = 3.5;
			this.W3 = 4.55;
			this.PerformanceType = Performance_Type.LCD.ToString();
			*/
		}

		public ProductComparisonDetail(ProductViewModel model)
		{
			//default value
			this.Importance = 1;
			this.P1 = 1;
			this.P2 = 1;
			this.P3 = 1;
			this.W1 = 1.0;
			this.W2 = 1.0;
			this.W3 = 1.0;
			this.ISO = 0.0;
			this.WFP = 0.0;
			this.PerformanceType = Performance_Type.Camera.ToString();

			//assign
			this.ProductSN = model.SN;
			this.ProductName = model.ProductName;
			this.ProductDesc = model.ProductDesc;
			double wfp, iso;
			if (Double.TryParse(model.Total_ISO, out iso))
			{
				this.ISO = iso;
			}
			if (Double.TryParse(model.Total_WFP, out wfp))
			{
				this.WFP = wfp;
			}
		}

		/// <summary>
		/// P1 + P2 + P3
		/// </summary>
		/// <returns></returns>
		public double GetPerformance()
		{
			return this.P1 + this.P2 + this.P3;
		}

		private int _SN;
		/// <summary>
		/// ProductComparisonDetail.SN
		/// </summary>
		public int SN
		{
			get { return _SN; }
			set
			{
				if (value == _SN)
					return;

				_SN = value;
				OnPropertyChanged("SN");
			}
		}
		private int _ProductComparisonSN;
		/// <summary>
		/// ProductComparison.SN
		/// </summary>
		public int ProductComparisonSN
		{
			get { return _ProductComparisonSN; }
			set
			{
				if (value == _ProductComparisonSN)
					return;

				_ProductComparisonSN = value;
				OnPropertyChanged("ProductComparisonSN");
			}
		}

		private int _ProductSN;
		/// <summary>
		/// ProductSN
		/// </summary>
		public int ProductSN
		{
			get { return _ProductSN; }
			set
			{
				if (value == _ProductSN)
					return;

				_ProductSN = value;
				OnPropertyChanged("ProductSN");
			}
		}

		private string _ProductName;
		/// <summary>
		/// ProductName
		/// </summary>
		[DisplayName("產品名稱")]
		public string ProductName
		{
			get { return _ProductName; }
			set
			{
				if (value == _ProductName)
					return;

				_ProductName = value;
				OnPropertyChanged("ProductName");
			}
		}

		private string _ProductDesc;
		/// <summary>
		/// ProductDesc
		/// </summary>
		[DisplayName("產品描述")]
		public string ProductDesc
		{
			get { return _ProductDesc; }
			set
			{
				if (value == _ProductDesc)
					return;

				_ProductDesc = value;
				OnPropertyChanged("ProductDesc");
			}
		}

		private double _WFP;
		/// <summary>
		/// Total_WFP
		/// </summary>
		[DisplayName("總水足跡(WFN)")]
		public double WFP
		{
			get { return _WFP; }
			set
			{
				if (value == _WFP)
					return;

				_WFP = value;
				OnPropertyChanged("WFP");
			}
		}

		private double _ISO;
		/// <summary>
		/// Total_ISO 14046
		/// </summary>
		[DisplayName("總水足跡(ISO 14046)")]
		public double ISO
		{
			get { return _ISO; }
			set
			{
				if (value == _ISO)
					return;

				_ISO = value;
				OnPropertyChanged("ISO");
			}
		}

		private string _PerformanceType;
		/// <summary>
		/// PerformanceType (性能表現類型 ; 攝影模組 or 液晶螢幕總成)
		/// </summary>
		public string PerformanceType
		{
			get { return _PerformanceType; }
			set
			{
				if (value == _PerformanceType)
					return;

				_PerformanceType = value;
				OnPropertyChanged("PerformanceType");
			}
		}

		private double _P1;
		/// <summary>
		/// P1 (Performance 1)
		/// </summary>
		public double P1
		{
			get { return _P1; }
			set
			{
				if (value == _P1)
					return;

				_P1 = value;
				OnPropertyChanged("P1");
			}
		}

		private double _P2;
		/// <summary>
		/// P2
		/// </summary>
		public double P2
		{
			get { return _P2; }
			set
			{
				if (value == _P2)
					return;

				_P2 = value;
				OnPropertyChanged("P2");
			}
		}

		private double _P3;
		/// <summary>
		/// P3
		/// </summary>
		public double P3
		{
			get { return _P3; }
			set
			{
				if (value == _P3)
					return;

				_P3 = value;
				OnPropertyChanged("P3");
			}
		}

		private double _Importance;
		/// <summary>
		/// Importance
		/// </summary>
		public double Importance
		{
			get { return _Importance; }
			set
			{
				if (value == _Importance)
					return;

				_Importance = value;
				OnPropertyChanged("Importance");
			}
		}

		private double _W1;
		/// <summary>
		/// W1
		/// </summary>
		public double W1
		{
			get { return _W1; }
			set
			{
				if (value == _W1)
					return;

				_W1 = value;
				OnPropertyChanged("W1");
			}
		}

		private double _W2;
		/// <summary>
		/// W2
		/// </summary>
		public double W2
		{
			get { return _W2; }
			set
			{
				if (value == _W2)
					return;

				_W2 = value;
				OnPropertyChanged("W2");
			}
		}

		private double _W3;

		/// <summary>
		/// W3
		/// </summary>
		public double W3
		{
			get { return _W3; }
			set
			{
				if (value == _W3)
					return;

				_W3 = value;
				OnPropertyChanged("W3");
			}
		}
		#region INotifyPropertyChanged

		/// <summary>
		/// The PropertyChanged event is used by consuming code
		/// (like WPF's binding infrastructure) to detect when
		/// a value has changed.
		/// </summary>
		public event PropertyChangedEventHandler PropertyChanged;

		/// <summary>
		/// Raise the PropertyChanged event for the 
		/// specified property.
		/// </summary>
		/// <param name="propertyName">
		/// A string representing the name of 
		/// the property that changed.</param>
		/// <remarks>
		/// Only raise the event if the value of the property 
		/// has changed from its previous value</remarks>
		protected void OnPropertyChanged(string propertyName)
		{
			{
				// Validate the property name in debug builds
				VerifyProperty(propertyName);

				if (null != PropertyChanged)
				{
					{
						PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
					}
				}
			}
		}

		/// <summary>
		/// Verifies whether the current class provides a property with a given
		/// name. This method is only invoked in debug builds, and results in
		/// a runtime exception if the <see cref="OnPropertyChanged"/> method
		/// is being invoked with an invalid property name. This may happen if
		/// a property's name was changed but not the parameter of the property's
		/// invocation of <see cref="OnPropertyChanged"/>.
		/// </summary>
		/// <param name="propertyName">The name of the changed property.</param>
		[System.Diagnostics.Conditional("DEBUG")]
		private void VerifyProperty(string propertyName)
		{
			{
				Type type = this.GetType();

				// Look for a *public* property with the specified name
				System.Reflection.PropertyInfo pi = type.GetProperty(propertyName);
				if (pi == null)
				{
					{
						// There is no matching property - notify the developer
						string msg = "OnPropertyChanged was invoked with invalid " +
								"property name {{0}}. {{0}} is not a public " +
								"property of {{1}}.";
						msg = String.Format(msg, propertyName, type.FullName);
						System.Diagnostics.Debug.Fail(msg);
					}
				}
			}
		}

		#endregion
	}
}
