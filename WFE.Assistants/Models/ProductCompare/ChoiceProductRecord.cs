﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFE.Assistants.Models.ProductCompare
{
	public class ChoiceProductRecord
	{
		public ChoiceProductRecord(ProductViewModel model, string createDate, string modifedDate)
		{
			this.SN = model.SN;
			this.ProductName = model.ProductName;
			this.ProductDesc = model.ProductDesc;
			this.WFP = model.Total_WFP;
			this.ISO = model.Total_ISO;
			this.CreateDate = createDate;
			this.ModifiedDate = modifedDate;
		}
		public int SN { get; set; }
		public string ProductName { get; set; }
		public string ProductDesc { get; set; }
		public string WFP { get; set; }
		public string ISO { get; set; }
		public string CreateDate { get; set; }
		public string ModifiedDate { get; set; }

	}


}
