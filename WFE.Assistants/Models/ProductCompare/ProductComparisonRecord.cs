﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFE.Assistants.Models.ProductCompare
{
	public class ProductComparisonRecord
	{
		[DisplayName("序號")]
		public int SN { get; set; }

		[DisplayName("產品比較分析名稱")]
		public string ComparisonName { get; set; }

		[DisplayName("產品比較分析描述")]
		public string ComparisonDesc { get; set; }

		[DisplayName("產品比較分析數量")]
		public int ComparisonCount { get; set; }

		[DisplayName("建立日期")]
		public string CreateDate { get; set; }

		[DisplayName("修改日期")]
		public string ModifiedDate { get; set; }
	}
}
