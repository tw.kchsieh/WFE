﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFE.Assistants.Models.ProductCompare.Enums
{
	public enum Performance_Type
	{
		/// <summary>
		/// 攝像模組
		/// </summary>
		Camera = 1,

		/// <summary>
		/// 液晶螢幕總成
		/// </summary>
		LCD = 2
	}
}
