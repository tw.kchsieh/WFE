﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFE.Assistants.Helpers;

namespace WFE.Assistants.Models.ProductCompare
{
	/// <summary>
	/// WFP (WFN) Evaluate Record
	/// </summary>
	public class WFNEvaluateRecord : BaseEvaluateRecord, IEvaluateRecord
	{
		public override string ProductName
		{
			get
			{
				return base.ProductName;
			}

			set
			{
				base.ProductName = value;
			}
		}

		[DisplayName("總水足跡 (WFN)")]
		public double WFP { get; set; }

		public override double P1
		{
			get
			{
				return base.P1;
			}

			set
			{
				base.P1 = value;
			}
		}

		public override double P2
		{
			get
			{
				return base.P2;
			}

			set
			{
				base.P2 = value;
			}
		}

		public override double P3
		{
			get
			{
				return base.P3;
			}

			set
			{
				base.P3 = value;
			}
		}

		public override double Importance
		{
			get
			{
				return base.Importance;
			}

			set
			{
				base.Importance = value;
			}
		}

		public override double W1
		{
			get
			{
				return base.W1;
			}

			set
			{
				base.W1 = value;
			}
		}

		public override double W2
		{
			get
			{
				return base.W2;
			}

			set
			{
				base.W2 = value;
			}
		}

		public override double W3
		{
			get
			{
				return base.W3;
			}

			set
			{
				base.W3 = value;
			}
		}

		[DisplayName("W1 * K_WFP_i ")]
		public override double E1
		{
			get
			{
				return base.E1;
			}

			set
			{
				base.E1 = value;
			}
		}

		[DisplayName("W2 * K_Performance_i ")]
		public override double E2
		{
			get
			{
				return base.E2;
			}

			set
			{
				base.E2 = value;
			}
		}

		[DisplayName("W3 * K_Importance_i ")]
		public override double E3
		{
			get
			{
				return base.E3;
			}

			set
			{
				base.E3 = value;
			}
		}

		public override double K
		{
			get
			{
				return base.K;
			}

			set
			{
				base.K = value;
			}
		}

		public void Setup(ProductComparisonDetail model, double m_wfp, double m_performance, double m_importance)
		{
			this.ProductName = model.ProductName;
			this.ProductSN = model.ProductSN;
			this.ProductComparisonSN = model.ProductComparisonSN;
			this.SN = model.SN;
			this.P1 = model.P1;
			this.P2 = model.P2;
			this.P3 = model.P3;

			this.W1 = model.W1;
			this.W2 = model.W2;
			this.W3 = model.W3;

			this.Importance = model.Importance;
			this.WFP = model.WFP;

			this.E1 = UIHelper.PrecisionNumberFormat(this.W1 * (this.WFP / m_wfp));
			this.E2 = UIHelper.PrecisionNumberFormat(this.W2 * ((this.P1 + this.P2 + this.P3) / m_performance));
			this.E3 = UIHelper.PrecisionNumberFormat(this.W3 * (this.Importance / m_importance));
			this.K = UIHelper.PrecisionNumberFormat(this.E1 + this.E2 + this.E3);
		}

	}
}
