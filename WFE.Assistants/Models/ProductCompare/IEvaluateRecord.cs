﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFE.Assistants.Models.ProductCompare
{
	interface IEvaluateRecord
	{
		void Setup(ProductComparisonDetail model, double m_wfp, double m_performance, double m_importance);
	}
}
