﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFE.Assistants.Models.ProductCompare
{
	public class BaseEvaluateRecord
	{
		/// <summary>
		/// ProductComparisonDetail.SN
		/// </summary>
		public int SN { get; set; }

		/// <summary>
		/// ProductComparison.SN
		/// </summary>
		public int ProductComparisonSN { get; set; }

		public int ProductSN { get; set; }

		[DisplayName("產品名稱")]
		public virtual string ProductName { get; set; }

		[DisplayName("性能表現 1")]
		public virtual double P1 { get; set; }

		[DisplayName("性能表現 2")]
		public virtual double P2 { get; set; }

		[DisplayName("性能表現 3")]
		public virtual double P3 { get; set; }

		[DisplayName("重要程度")]
		public virtual double Importance { get; set; }

		[DisplayName("權重 W1")]
		public virtual double W1 { get; set; }

		[DisplayName("權重 W2")]
		public virtual double W2 { get; set; }

		[DisplayName("權重 W3")]
		public virtual double W3 { get; set; }

		public virtual double E1 { get; set; }
		public virtual double E2 { get; set; }
		public virtual double E3 { get; set; }

		[DisplayName("K")]
		public virtual double K { get; set; }

		public EvaluateChartModel GetEvaluateChartModel()
		{
			return new EvaluateChartModel()
			{
				ProductName = this.ProductName,
				E1 = this.E1,
				E2 = this.E2,
				E3 = this.E3,
			};
		}
	}
}
