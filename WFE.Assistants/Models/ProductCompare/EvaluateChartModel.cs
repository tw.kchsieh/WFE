﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFE.Assistants.Models.ProductCompare
{
	public class EvaluateChartModel
	{
		public double E1 { get; set; }
		public double E2 { get; set; }
		public double E3 { get; set; }
		public string ProductName { get; internal set; }
	}
}
