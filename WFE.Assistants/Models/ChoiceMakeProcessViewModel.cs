﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFE.Assistants.Helpers;

namespace WFE.Assistants.Models
{
	/// <summary>
	/// 選擇的加工方式模型
	/// </summary>
	public class ChoiceMakeProcessViewModel
	{
		private int _C_SN;
		/// <summary>
		/// 原料/零件 SN
		/// </summary>
		public int C_SN
		{
			get { return _C_SN; }
			set
			{
				if (value == _C_SN)
					return;

				_C_SN = value;
				OnPropertyChanged("C_SN");
			}
		}

		private string _C_Name;
		/// <summary>
		/// 原料/零件 C_Name
		/// </summary>
		public string C_Name
		{
			get { return _C_Name; }
			set
			{
				if (value == _C_Name)
					return;

				_C_Name = value;
				OnPropertyChanged("C_Name");
			}
		}


		private string _Mass;
		/// <summary>
		/// 原料/零件 Mass
		/// </summary>
		public string Mass
		{
			get
			{
				double mass;
				string result = _Mass;
				var v1 = double.TryParse(_Mass, out mass);
				if (v1)
				{
					result = UIHelper.NumberTextFormat(mass);
				}
				return result;
			}
			set
			{
				if (value == _Mass)
					return;

				_Mass = value;
				OnPropertyChanged("Mass");
			}
		}

		private int _SN;
		/// <summary>
		/// 加工方式 SN
		/// </summary>
		public int SN
		{
			get { return _SN; }
			set
			{
				if (value == _SN)
					return;

				_SN = value;
				OnPropertyChanged("SN");
			}
		}

		private string _Name;
		/// <summary>
		/// 加工方式 Name
		/// </summary>
		public string Name
		{
			get { return _Name; }
			set
			{
				if (value == _Name)
					return;

				_Name = value;
				OnPropertyChanged("Name");
			}
		}

		public string CalculateCode { get; set; }

		/// <summary>
		/// 加工時數
		/// </summary>
		public string WorkingHour { get; set; }

		private string _PowerCOEEF;
		/// <summary>
		/// PowerCOEEF (電力系數)
		/// </summary>
		public string PowerCOEEF
		{
			get { return _PowerCOEEF; }
			set
			{
				if (value == _PowerCOEEF)
					return;

				_PowerCOEEF = value;
				OnPropertyChanged("PowerCOEEF");
			}
		}

		private string _PowerConsumption;
		/// <summary>
		/// 電力消耗量
		/// </summary>
		public string PowerConsumption
		{
			get
			{
				double consumption;
				string result = _PowerConsumption;
				var v1 = double.TryParse(_PowerConsumption, out consumption);
				if (v1)
				{
					result = UIHelper.NumberTextFormat(consumption);
				}
				return result;
			}
			set
			{
				if (value == _PowerConsumption)
					return;

				_PowerConsumption = value;
				OnPropertyChanged("PowerConsumption");
			}
		}

		private string _MakeProcessWFP;
		/// <summary>
		/// MakeProcessWFP (加工方式水足跡)
		/// </summary>
		public string MakeProcessWFP
		{
			get
			{
				double wfp;
				string result = _MakeProcessWFP;
				if (double.TryParse(_MakeProcessWFP, out wfp))
				{
					result = UIHelper.NumberTextFormat(wfp);
				}
				return result;
			}
			set
			{
				if (value == _MakeProcessWFP)
					return;

				_MakeProcessWFP = value;
				OnPropertyChanged("MakeProcessWFP");
			}
		}

		private string _MakeProcessISO;
		/// <summary>
		/// MakeProcessISO (加工方式水足跡 ISO 14046)
		/// </summary>
		public string MakeProcessISO
		{
			get
			{
				double wfp;
				string result = _MakeProcessISO;
				if (double.TryParse(_MakeProcessISO, out wfp))
				{
					result = UIHelper.NumberTextFormat(wfp);
				}
				return result;
			}
			set
			{
				if (value == _MakeProcessISO)
					return;

				_MakeProcessISO = value;
				OnPropertyChanged("MakeProcessISO");
			}
		}

		private string _WFP;
		/// <summary>
		/// WFP
		/// <para>依計算公式代碼 Calculate Code 決定計算公式</para>
		/// </summary>
		public string WFP
		{
			get
			{
				string result = "N/A";
				switch (this.CalculateCode)
				{
					case "Power":
						result = calculateByPower();
						break;
					case "WFP":
					default:
						result = calculateByMass(this.MakeProcessWFP);
						break;
				}
				return result;
			}
		}

		private string _ISO;
		/// <summary>
		/// ISO 14046
		/// <para>依計算公式代碼 Calculate Code 決定計算公式</para>
		/// </summary>
		public string ISO
		{
			get
			{
				string result = "N/A";
				switch (this.CalculateCode)
				{
					case "Power":
						result = calculateByPower();
						break;
					case "ISO":
					default:
						result = calculateByMass(this.MakeProcessISO);
						break;
				}
				return result;
			}
		}

		/// <summary>
		/// 電力系數計算公式
		/// <para>水足跡 = 電力系數 * 電力消耗量(kwh) * 加工時數</para>
		/// </summary>
		/// <returns></returns>
		private string calculateByPower()
		{
			var result = "N/A";
			double p_coeef, p_consumption, p_workHour;
			var v3 = double.TryParse(PowerCOEEF, out p_coeef);
			var v4 = double.TryParse(PowerConsumption, out p_consumption);
			var v5 = double.TryParse(WorkingHour, out p_workHour);
			if (v3 && v4 && v5)
			{
				result = UIHelper.NumberTextFormat(p_coeef * p_consumption * p_workHour);
			}
			return result;
		}

		private string calculateByMass(string wfp)
		{
			var result = "N/A";
			double processWFP, mass;
			var v1 = double.TryParse(Mass, out mass);
			var v2 = double.TryParse(wfp, out processWFP);
			if (v1 && v2)
			{
				result = UIHelper.NumberTextFormat(mass * processWFP);
			}
			return result;
		}

		#region INotifyPropertyChanged

		/// <summary>
		/// The PropertyChanged event is used by consuming code
		/// (like WPF's binding infrastructure) to detect when
		/// a value has changed.
		/// </summary>
		public event PropertyChangedEventHandler PropertyChanged;

		/// <summary>
		/// Raise the PropertyChanged event for the 
		/// specified property.
		/// </summary>
		/// <param name="propertyName">
		/// A string representing the name of 
		/// the property that changed.</param>
		/// <remarks>
		/// Only raise the event if the value of the property 
		/// has changed from its previous value</remarks>
		protected void OnPropertyChanged(string propertyName)
		{
			{
				// Validate the property name in debug builds
				VerifyProperty(propertyName);

				if (null != PropertyChanged)
				{
					{
						PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
					}
				}
			}
		}

		/// <summary>
		/// Verifies whether the current class provides a property with a given
		/// name. This method is only invoked in debug builds, and results in
		/// a runtime exception if the <see cref="OnPropertyChanged"/> method
		/// is being invoked with an invalid property name. This may happen if
		/// a property's name was changed but not the parameter of the property's
		/// invocation of <see cref="OnPropertyChanged"/>.
		/// </summary>
		/// <param name="propertyName">The name of the changed property.</param>
		[System.Diagnostics.Conditional("DEBUG")]
		private void VerifyProperty(string propertyName)
		{
			{
				Type type = this.GetType();

				// Look for a *public* property with the specified name
				System.Reflection.PropertyInfo pi = type.GetProperty(propertyName);
				if (pi == null)
				{
					{
						// There is no matching property - notify the developer
						string msg = "OnPropertyChanged was invoked with invalid " +
								"property name {{0}}. {{0}} is not a public " +
								"property of {{1}}.";
						msg = String.Format(msg, propertyName, type.FullName);
						System.Diagnostics.Debug.Fail(msg);
					}
				}
			}
		}

		#endregion
	}
}
