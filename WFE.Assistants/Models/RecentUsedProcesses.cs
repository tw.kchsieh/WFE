﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFE.Assistants.Models
{
	/// <summary>
	/// 最近使用的製程
	/// </summary>
	public class RecentUsedProcesses
	{
		public int ProcessesSN { get; set; }
		public string Name { get; set; }
		public DateTime LastUsed { get; set; }
		public int UsedCount { get; set; }
	}
}
