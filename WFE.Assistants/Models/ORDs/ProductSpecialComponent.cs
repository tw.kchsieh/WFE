﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFE.Assistants.Helpers;

namespace WFE.Assistants.Models.ORDs
{
	/// <summary>
	/// 特殊元件
	/// </summary>
	public class ProductSpecialComponent
	{
		#region CreateDate
		public DateTime CreateDate { get; set; }
		#endregion
		public string SelectedSpeialComponentType { get; set; }

		public ProductSpecialComponent()
		{
			//CreateDate = DateTime.Now;
		}

		public StageC_ViewModel ToStageC_ViewModel()
		{
			double volumn;
			if (!Double.TryParse(this.Component_Volumn, out volumn))
			{
				volumn = 0;
			}
			var model = new StageC_ViewModel()
			{
				//特殊元件
				Component_Name = this.Component_Name,
				Component_Volumn = UIHelper.NumberTextFormat(volumn * Math.Pow(1000, 3)),
				Component_Area = this.Component_Area,

				SelectedSpeialComponentType = this.SelectedSpeialComponentType,

				//製程
				Manufacture_SN = this.ManufactureProcedureSN,
				Manufacture_Name = this.ManufactureProcedure_Name,
				Manufacture_ProcessTypeCode = this.ManufactureProcedure_ProcessTypeCode,
				Manufacture_Density = this.ManufactureProcedure_Density,
				Manufacture_WFP = this.ManufactureProcedure_WFP,
				Manufacture_ISO = this.ManufactureProcedure_ISO,

				//
				CreateDate = this.CreateDate,
			};
			return model;
		}

		private int _SN;
		/// <summary>
		/// SN 流水號
		/// </summary>
		public int SN
		{
			get { return _SN; }
			set
			{
				if (value == _SN)
					return;

				_SN = value;
				OnPropertyChanged("SN");
			}
		}

		private int _Product_SN;
		/// <summary>
		/// Product_SN
		/// </summary>
		public int Product_SN
		{
			get { return _Product_SN; }
			set
			{
				if (value == _Product_SN)
					return;

				_Product_SN = value;
				OnPropertyChanged("Product_SN");
			}
		}

		private string _Component_Name;
		/// <summary>
		/// Component_Name
		/// </summary>
		[DisplayName("特殊零件")]
		public string Component_Name
		{
			get { return _Component_Name; }
			set
			{
				if (value == _Component_Name)
					return;

				_Component_Name = value;
				OnPropertyChanged("Component_Name");
			}
		}

		private string _Component_Volumn;
		/// <summary>
		/// Component_Volumn
		/// </summary>
		public string Component_Volumn
		{
			get { return _Component_Volumn; }
			set
			{
				if (value == _Component_Volumn)
					return;

				_Component_Volumn = value;
				OnPropertyChanged("Component_Volumn");
			}
		}

		private string _Component_Area;
		/// <summary>
		/// Component_Area
		/// </summary>
		public string Component_Area
		{
			get { return _Component_Area; }
			set
			{
				if (value == _Component_Area)
					return;

				_Component_Area = value;
				OnPropertyChanged("Component_Area");
			}
		}

		/// <summary>
		/// Component_Mass = Density * Volumn
		/// <para>計算欄位, ReadOnly</para>
		/// </summary>
		public string Component_Mass
		{
			get
			{
				var result = "0";
				double density, volumn;
				var v1 = Double.TryParse(ManufactureProcedure_Density, out density);
				var v2 = Double.TryParse(Component_Volumn, out volumn);
				if (v1 && v2)
				{
					result = UIHelper.NumberTextFormat(density * volumn);
				}
				return result;
			}
		}

		private string _ManufactureProcedureSN;
		/// <summary>
		/// ManufactureProcedure_SN
		/// </summary>
		public string ManufactureProcedureSN
		{
			get { return _ManufactureProcedureSN; }
			set
			{
				if (value == _ManufactureProcedureSN)
					return;

				_ManufactureProcedureSN = value;
				OnPropertyChanged("ManufactureProcedureSN");
			}
		}

		private string _ManufactureProcedure_Name;
		/// <summary>
		/// ManufactureProcedure_Name
		/// </summary>
		[DisplayName("製程名稱")]
		public string ManufactureProcedure_Name
		{
			get { return _ManufactureProcedure_Name; }
			set
			{
				if (value == _ManufactureProcedure_Name)
					return;

				_ManufactureProcedure_Name = value;
				OnPropertyChanged("ManufactureProcedure_Name");
			}
		}

		private string _ManufactureProcedure_ProcessTypeCode;
		/// <summary>
		/// ManufactureProcedure_ProcessTypeCode
		/// </summary>
		[DisplayName("製程類型")]
		public string ManufactureProcedure_ProcessTypeCode
		{
			get { return _ManufactureProcedure_ProcessTypeCode; }
			set
			{
				if (value == _ManufactureProcedure_ProcessTypeCode)
					return;

				_ManufactureProcedure_ProcessTypeCode = value;
				OnPropertyChanged("ManufactureProcedure_ProcessTypeCode");
			}
		}

		private string _ManufactureProcedure_Density;
		/// <summary>
		/// ManufactureProcedure_Density
		/// </summary>
		public string ManufactureProcedure_Density
		{
			get { return _ManufactureProcedure_Density; }
			set
			{
				if (value == _ManufactureProcedure_Density)
					return;

				_ManufactureProcedure_Density = value;
				OnPropertyChanged("ManufactureProcedure_Density");
			}
		}

		private string _ManufactureProcedure_WFP;
		/// <summary>
		/// ManufactureProcedure_WFP
		/// </summary>
		public string ManufactureProcedure_WFP
		{
			get
			{
				double wfp;
				var result = "0";
				if (Double.TryParse(_ManufactureProcedure_WFP, out wfp))
				{
					result = UIHelper.NumberTextFormat(wfp);
				}
				return result;
			}
			set
			{
				if (value == _ManufactureProcedure_WFP)
					return;

				_ManufactureProcedure_WFP = value;
				OnPropertyChanged("ManufactureProcedure_WFP");
			}
		}

		private string _ManufactureProcedure_ISO;
		/// <summary>
		/// ManufactureProcedure_ISO 14046
		/// </summary>
		public string ManufactureProcedure_ISO
		{
			get
			{
				double wfp;
				var result = "0";
				if (Double.TryParse(_ManufactureProcedure_ISO, out wfp))
				{
					result = UIHelper.NumberTextFormat(wfp);
				}
				return result;
			}
			set
			{
				if (value == _ManufactureProcedure_ISO)
					return;

				_ManufactureProcedure_ISO = value;
				OnPropertyChanged("ManufactureProcedure_ISO");
			}
		}

		/// <summary>
		/// WFP (ReadOnly)
		/// <para>計算欄位</para>
		/// </summary>
		[DisplayName("特殊零件水足跡(WFN)")]
		public string WFP
		{
			get
			{
				var result = "0";
				double mass, m_wfp, area;
				var v1 = Double.TryParse(Component_Mass, out mass);
				var v2 = Double.TryParse(ManufactureProcedure_WFP, out m_wfp);
				var v3 = Double.TryParse(Component_Area, out area);
				switch (this.ManufactureProcedure_ProcessTypeCode)
				{
					case "E":
						//電子元件 : WFP = 質量 * 製程水足跡
						if (v1 && v2)
						{
							result = UIHelper.NumberTextFormat(mass * m_wfp);
						}
						break;
					case "LCD":
					case "M-Hard":
					case "M-Mixed":
						//硬板, LCD
						if (v2 && v3)
						{
							result = UIHelper.NumberTextFormat(m_wfp * area);
						}
						break;
					case "M-Soft":
						//軟板
						if (v2)
						{
							result = UIHelper.NumberTextFormat(m_wfp);
						}
						break;
				}
				return result;
			}
		}

		/// <summary>
		/// ISO 14046 (ReadOnly)
		/// <para>計算欄位</para>
		/// </summary>
		[DisplayName("特殊零件水足跡(ISO)")]
		public string ISO
		{
			get
			{
				var result = "0";
				double mass, m_ISO, area;
				var v1 = Double.TryParse(Component_Mass, out mass);
				var v2 = Double.TryParse(ManufactureProcedure_ISO, out m_ISO);
				var v3 = Double.TryParse(Component_Area, out area);
				switch (this.ManufactureProcedure_ProcessTypeCode)
				{
					case "E":
						//電子元件 : ISO = 質量 * 製程水足跡
						if (v1 && v2)
						{
							result = UIHelper.NumberTextFormat(mass * m_ISO);
						}
						break;
					case "LCD":
					case "M-Hard":
					case "M-Mixed":
						//硬板, LCD
						if (v2 && v3)
						{
							result = UIHelper.NumberTextFormat(m_ISO * area);
						}
						break;
					case "M-Soft":
						//軟板
						if (v2)
						{
							result = UIHelper.NumberTextFormat(m_ISO);
						}
						break;
				}
				return result;
			}
		}

		#region INotifyPropertyChanged

		/// <summary>
		/// The PropertyChanged event is used by consuming code
		/// (like WPF's binding infrastructure) to detect when
		/// a value has changed.
		/// </summary>
		public event PropertyChangedEventHandler PropertyChanged;

		/// <summary>
		/// Raise the PropertyChanged event for the 
		/// specified property.
		/// </summary>
		/// <param name="propertyName">
		/// A string representing the name of 
		/// the property that changed.</param>
		/// <remarks>
		/// Only raise the event if the value of the property 
		/// has changed from its previous value</remarks>
		protected void OnPropertyChanged(string propertyName)
		{
			{
				// Validate the property name in debug builds
				VerifyProperty(propertyName);

				if (null != PropertyChanged)
				{
					{
						PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
					}
				}
			}
		}

		/// <summary>
		/// Verifies whether the current class provides a property with a given
		/// name. This method is only invoked in debug builds, and results in
		/// a runtime exception if the <see cref="OnPropertyChanged"/> method
		/// is being invoked with an invalid property name. This may happen if
		/// a property's name was changed but not the parameter of the property's
		/// invocation of <see cref="OnPropertyChanged"/>.
		/// </summary>
		/// <param name="propertyName">The name of the changed property.</param>
		[System.Diagnostics.Conditional("DEBUG")]
		private void VerifyProperty(string propertyName)
		{
			{
				Type type = this.GetType();

				// Look for a *public* property with the specified name
				System.Reflection.PropertyInfo pi = type.GetProperty(propertyName);
				if (pi == null)
				{
					{
						// There is no matching property - notify the developer
						string msg = "OnPropertyChanged was invoked with invalid " +
								"property name {{0}}. {{0}} is not a public " +
								"property of {{1}}.";
						msg = String.Format(msg, propertyName, type.FullName);
						System.Diagnostics.Debug.Fail(msg);
					}
				}
			}
		}
		#endregion
	}
}
