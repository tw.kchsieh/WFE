﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFE.Assistants.Helpers;

namespace WFE.Assistants.Models.ORDs
{
	/// <summary>
	/// 產品零件
	/// </summary>
	public class ProductComponent
	{
		private string _general_Stage_Total_WFN;
		private string _general_Stage_Total_ISO;

		public DateTime CreateDate { get; set; }
		public ProductComponent()
		{
			init();
		}
		public ProductComponent(string general_total_wfn, string general_total_iso)
		{
			_general_Stage_Total_WFN = general_total_wfn;
			_general_Stage_Total_ISO = general_total_iso;
			init();
		}
		private void init()
		{
			this.ComponentTransports = new List<ComponentTransport>();
			this.ComponentProcesses = new List<ComponentProcess>();
			CreateDate = DateTime.Now;
		}
		#region Public Methods ========================

		/// <summary>
		/// 原料水足跡 + 運輸水足跡 + 加工水足跡
		/// <para>WFN</para>
		/// </summary>
		/// <returns></returns>
		public string Get_StageA_Total_WFN()
		{
			double wfp_val = 0;
			if (!Double.TryParse(this.WFP, out wfp_val))
			{
				wfp_val = 0;
			}
			var wfn = wfp_val + ComponentProcessWFP + ComponentTransportWFP;
			return UIHelper.NumberTextFormat(wfn);
		}
		/// <summary>
		/// 原料水足跡 + 運輸水足跡 + 加工水足跡
		/// <para>ISO 14046</para>
		/// </summary>
		/// <returns></returns>
		public string Get_StageA_Total_ISO()
		{
			double wfp_val;
			if (!Double.TryParse(this.ISO, out wfp_val))
			{
				wfp_val = 0;
			}
			var iso = wfp_val + ComponentProcessISO + ComponentTransportISO;
			return UIHelper.NumberTextFormat(iso);
		}


		/// <summary>
		/// 一般總水 + 後製加工 + 後製運輸
		/// </summary>
		/// <returns></returns>
		public string GetPostStage_Total_WFN()
		{
			double wfpVal = 0;
			if (!Double.TryParse(_general_Stage_Total_WFN, out wfpVal))
			{
				wfpVal = 0;
			}
			return UIHelper.NumberTextFormat(wfpVal + ComponentProcessWFP + ComponentTransportWFP);
		}
		public string GetPostStage_Total_ISO()
		{
			double wfpVal = 0;
			if (!Double.TryParse(_general_Stage_Total_ISO, out wfpVal))
			{
				wfpVal = 0;
			}
			return UIHelper.NumberTextFormat(wfpVal + ComponentProcessISO + ComponentTransportISO);
		}

		/// <summary>
		/// 轉換為 StageA_ViewModel
		/// </summary>
		/// <returns></returns>
		public StageA_ViewModel To_StageA_ViewModel()
		{
			return new StageA_ViewModel()
			{
				Component_Name = this.Component_Name,
				Component_SN = this.Component_SN,
				Component_Volumn = this.Component_Volumn,
				Material_SN = this.Material_SN,
				Material_Name = this.Material_Name,
				Material_Density = this.Material_Density,
				Material_WFP = this.Material_WFP,
				Material_ISO = this.Material_ISO,

			};
		}

		/// <summary>
		/// 複製基本資料，運輸及加工記錄除外
		/// </summary>
		/// <returns></returns>
		public ProductComponent Clone_BasicInfo()
		{
			var general_stage_wfp = this.Get_StageA_Total_WFN();
			var general_stage_iso = this.Get_StageA_Total_ISO();
			var cloneComponent = new ProductComponent(general_stage_wfp, general_stage_iso)
			{
				Component_SN = this.Component_SN,
				Component_Name = this.Component_Name,
				Component_Volumn = this.Component_Volumn,

				Material_Name = this.Material_Name,
				Material_Density = this.Material_Density,
				Material_SN = this.Material_SN,
				Material_WFP = this.Material_WFP,
				Material_ISO = this.Material_ISO,
				Product_SN = this.Product_SN,
				ComponentProcesses = new List<ComponentProcess>(),
				ComponentTransports = new List<ComponentTransport>(),
			};
			return cloneComponent;
		}
		#endregion

		/// <summary>
		/// 運輸記錄
		/// </summary>
		public List<ComponentTransport> ComponentTransports { get; set; }

		/// <summary>
		/// 加工記錄
		/// </summary>
		public List<ComponentProcess> ComponentProcesses { get; set; }

		private int _Product_SN;
		/// <summary>
		/// Product_SN
		/// </summary>
		public int Product_SN
		{
			get { return _Product_SN; }
			set
			{
				if (value == _Product_SN)
					return;

				_Product_SN = value;
				OnPropertyChanged("Product_SN");
			}
		}

		private int _PostComponent_SN;
		/// <summary>
		/// PostComponent_SN (後製加工流水號)
		/// </summary>
		public int PostComponent_SN
		{
			get { return _PostComponent_SN; }
			set
			{
				if (value == _PostComponent_SN)
					return;

				_PostComponent_SN = value;
				OnPropertyChanged("PostComponent_SN");
			}
		}

		private int _Component_SN;
		/// <summary>
		/// Component_SN (流水號)
		/// </summary>
		public int Component_SN
		{
			get { return _Component_SN; }
			set
			{
				if (value == _Component_SN)
					return;

				_Component_SN = value;
				OnPropertyChanged("Component_SN");
			}
		}

		private string _Component_Name;
		/// <summary>
		/// Component_Name
		/// </summary>
		[DisplayName("一般零件")]
		public string Component_Name
		{
			get { return _Component_Name; }
			set
			{
				if (value == _Component_Name)
					return;

				_Component_Name = value;
				OnPropertyChanged("Component_Name");
			}
		}

		private string _Material_SN;
		/// <summary>
		/// Material_SN
		/// </summary>
		public string Material_SN
		{
			get { return _Material_SN; }
			set
			{
				if (value == _Material_SN)
					return;

				_Material_SN = value;
				OnPropertyChanged("Material_SN");
			}
		}

		private string _Material_Name;
		/// <summary>
		/// Material_Name
		/// </summary>
		public string Material_Name
		{
			get { return _Material_Name; }
			set
			{
				if (value == _Material_Name)
					return;

				_Material_Name = value;
				OnPropertyChanged("Material_Name");
			}
		}

		/// <summary>
		/// Mass (質量) = Material_Density * Component_Volumn
		/// <para>計算欄位</para>
		/// </summary>
		public string Mass
		{
			get
			{
				string result = "0";
				double density, volumn;
				var v1 = Double.TryParse(this.Material_Density, out density);
				var v2 = Double.TryParse(this.Component_Volumn, out volumn);
				if (v1 && v2)
				{
					result = UIHelper.NumberTextFormat(density * volumn);
				}
				return result;
			}
		}

		private string _Component_Volumn;
		/// <summary>
		/// Volumn
		/// </summary>		
		public string Component_Volumn
		{
			get { return _Component_Volumn; }
			set
			{
				if (value == _Component_Volumn)
					return;

				_Component_Volumn = value;
				OnPropertyChanged("Component_Volumn");
			}
		}

		private string _Material_Density;
		/// <summary>
		/// Density
		/// </summary>
		public string Material_Density
		{
			get { return _Material_Density; }
			set
			{
				if (value == _Material_Density)
					return;

				_Material_Density = value;
				OnPropertyChanged("Material_Density");
			}
		}

		private string _Material_WFP;
		/// <summary>
		/// Material_WFP (原料水足跡)
		/// </summary>
		public string Material_WFP
		{
			get { return _Material_WFP; }
			set
			{
				if (value == _Material_WFP)
					return;

				_Material_WFP = value;
				OnPropertyChanged("Material_WFP");
			}
		}

		private string _Material_ISO;
		/// <summary>
		/// Material_ISO (ISO 14046 水足跡)
		/// </summary>
		public string Material_ISO
		{
			get { return _Material_ISO; }
			set
			{
				if (value == _Material_ISO)
					return;

				_Material_ISO = value;
				OnPropertyChanged("Material_ISO");
			}
		}

		/// <summary>
		/// WFP (原料階段水足跡) = 質量 * 原料水足跡
		/// <para>計算欄位</para>
		/// </summary>
		[DisplayName("材料水足跡(WFN)")]
		public string WFP
		{
			get
			{
				double mass, material_wfp;
				var v1 = Double.TryParse(this.Mass, out mass);
				var v2 = Double.TryParse(this.Material_WFP, out material_wfp);
				string result = "0";
				if (v1 && v2)
				{
					var wfp = mass * material_wfp;
					result = UIHelper.NumberTextFormat(wfp);
				}
				return result;
			}
		}
		[DisplayName("材料水足跡(ISO)")]
		public string ISO
		{
			get
			{
				double mass, material_ISO;
				var v1 = Double.TryParse(this.Mass, out mass);
				var v2 = Double.TryParse(this.Material_ISO, out material_ISO);
				string result = "0";
				if (v1 && v2)
				{
					var ISO = mass * material_ISO;
					result = UIHelper.NumberTextFormat(ISO);
				}
				return result;
			}
		}

		/// <summary>
		/// 運輸總水足跡 
		/// </summary>
		public double ComponentTransportWFP
		{
			get
			{
				return ComponentTransports.Sum(p => Convert.ToDouble(p.WFP));
			}
		}

		/// <summary>
		/// 運輸總水足跡 ISO 14046
		/// </summary>
		public double ComponentTransportISO
		{
			get
			{
				return ComponentTransports.Sum(p => Convert.ToDouble(p.ISO));
			}
		}

		/// <summary>
		/// 顯示運輸總水足跡 WFN
		/// </summary>
		[DisplayName("運輸水足跡(WFN)")]
		public string DisplayComponentTransportWFP
		{
			get
			{
				return UIHelper.NumberTextFormat(this.ComponentTransportWFP);
			}
		}
		/// <summary>
		/// 顯示運輸總水足跡 ISO 14046
		/// </summary>
		[DisplayName("運輸水足跡(ISO)")]
		public string DisplayComponentTransportISO
		{
			get
			{
				return UIHelper.NumberTextFormat(this.ComponentTransportISO);
			}
		}

		/// <summary>
		/// 加工總水足跡
		/// </summary>
		public double ComponentProcessWFP
		{
			get
			{
				return ComponentProcesses.Sum(p => Convert.ToDouble(p.WFP));
			}
		}

		/// <summary>
		/// 加工總水足跡 ISO 14046
		/// </summary>
		public double ComponentProcessISO
		{
			get
			{
				return ComponentProcesses.Sum(p => Convert.ToDouble(p.ISO));
			}
		}

		[DisplayName("加工水足跡(WFN)")]
		public string DisplayComponentProcessWFP
		{
			get
			{
				return UIHelper.NumberTextFormat(this.ComponentProcessWFP);
			}
		}

		[DisplayName("加工水足跡(ISO)")]
		public string DisplayComponentProcessISO
		{
			get
			{
				return UIHelper.NumberTextFormat(this.ComponentProcessISO);
			}
		}


		#region INotifyPropertyChanged

		/// <summary>
		/// The PropertyChanged event is used by consuming code
		/// (like WPF's binding infrastructure) to detect when
		/// a value has changed.
		/// </summary>
		public event PropertyChangedEventHandler PropertyChanged;

		/// <summary>
		/// Raise the PropertyChanged event for the 
		/// specified property.
		/// </summary>
		/// <param name="propertyName">
		/// A string representing the name of 
		/// the property that changed.</param>
		/// <remarks>
		/// Only raise the event if the value of the property 
		/// has changed from its previous value</remarks>
		protected void OnPropertyChanged(string propertyName)
		{
			{
				// Validate the property name in debug builds
				VerifyProperty(propertyName);

				if (null != PropertyChanged)
				{
					{
						PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
					}
				}
			}
		}

		/// <summary>
		/// Verifies whether the current class provides a property with a given
		/// name. This method is only invoked in debug builds, and results in
		/// a runtime exception if the <see cref="OnPropertyChanged"/> method
		/// is being invoked with an invalid property name. This may happen if
		/// a property's name was changed but not the parameter of the property's
		/// invocation of <see cref="OnPropertyChanged"/>.
		/// </summary>
		/// <param name="propertyName">The name of the changed property.</param>
		[System.Diagnostics.Conditional("DEBUG")]
		private void VerifyProperty(string propertyName)
		{
			{
				Type type = this.GetType();

				// Look for a *public* property with the specified name
				System.Reflection.PropertyInfo pi = type.GetProperty(propertyName);
				if (pi == null)
				{
					{
						// There is no matching property - notify the developer
						string msg = "OnPropertyChanged was invoked with invalid " +
								"property name {{0}}. {{0}} is not a public " +
								"property of {{1}}.";
						msg = String.Format(msg, propertyName, type.FullName);
						System.Diagnostics.Debug.Fail(msg);
					}
				}
			}
		}

		#endregion
	}
}
