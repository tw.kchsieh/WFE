﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFE.Assistants.Helpers;

namespace WFE.Assistants.Models.ORDs
{
	/// <summary>
	/// 原料運輸
	/// </summary>
	public class ComponentTransport
	{
		private int _SN;
		/// <summary>
		/// SN (流水號)
		/// </summary>
		public int SN
		{
			get { return _SN; }
			set
			{
				if (value == _SN)
					return;

				_SN = value;
				OnPropertyChanged("SN");
			}
		}

		private int _C_SN;
		/// <summary>
		/// 材料或零件SN
		/// </summary>
		[DisplayName("SN")]
		public int C_SN
		{
			get { return _C_SN; }
			set
			{
				if (value == _C_SN)
					return;

				_C_SN = value;
				OnPropertyChanged("C_SN");
			}
		}

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("零件")]
		public string C_Name { get; set; }

		private int _TransportSN;
		/// <summary>
		/// 運輸工具 SN
		/// </summary>
		[DisplayName("運輸序號")]
		public int TransportSN
		{
			get { return _TransportSN; }
			set
			{
				if (value == _TransportSN)
					return;

				_TransportSN = value;
				OnPropertyChanged("TransportSN");
			}
		}

		private string _Name;
		/// <summary>
		/// Name
		/// </summary>
		[DisplayName("運輸工具名稱")]
		public string Name
		{
			get { return _Name; }
			set
			{
				if (value == _Name)
					return;

				_Name = value;
				OnPropertyChanged("Name");
			}
		}

		private string _Component_Mass;
		/// <summary>
		/// Weight = Component_Mass (質量)
		/// </summary>
		[DisplayName("重量 Kg")]
		public string Component_Mass
		{
			get { return _Component_Mass; }
			set
			{
				if (value == _Component_Mass)
					return;

				_Component_Mass = value;
				OnPropertyChanged("Component_Mass");
			}
		}
		private string _Distance;
		/// <summary>
		/// Distance
		/// </summary>
		[DisplayName("距離 Km")]
		public string Distance
		{
			get { return _Distance; }
			set
			{
				if (value == _Distance)
					return;

				_Distance = value;
				OnPropertyChanged("Distance");
			}
		}

		/// <summary>
		/// 運輸工具水足跡
		/// </summary>
		[DisplayName("運輸工具水足跡 (WFN)")]
		public string TransportWFP { get; set; }

		/// <summary>
		/// 運輸工具水足跡 ISO 14046
		/// </summary>
		[DisplayName("運輸工具水足跡 (ISO)")]
		public string TransportISO { get; set; }

		private string _WFP;
		/// <summary>
		/// WFP = Distance * Component_Mass * Transport_WFP
		/// <para>計算欄位</para>
		/// </summary>
		[DisplayName("運輸水足跡 (WFN)")]
		public string WFP
		{
			get
			{
				double distance, mass, transport_wfp;
				var result = "0.0";
				var v1 = Double.TryParse(Distance, out distance);
				var v2 = Double.TryParse(Component_Mass, out mass);
				var v3 = Double.TryParse(TransportWFP, out transport_wfp);
				if (v1 && v2 && v3)
				{
					result = UIHelper.NumberTextFormat(distance * (mass) * transport_wfp);
				}
				return result;
			}
			set
			{
				if (value == _WFP)
					return;

				_WFP = value;
				OnPropertyChanged("WFP");
			}
		}

		private string _ISO;
		/// <summary>
		/// ISO = Distance * Component_Mass * Transport_ISO
		/// <para>計算欄位</para>
		/// </summary>
		[DisplayName("運輸水足跡 (ISO)")]
		public string ISO
		{
			get
			{
				double distance, mass, transport_ISO;
				var result = "0.0";
				var v1 = Double.TryParse(Distance, out distance);
				var v2 = Double.TryParse(Component_Mass, out mass);
				var v3 = Double.TryParse(TransportISO, out transport_ISO);
				if (v1 && v2 && v3)
				{
					result = UIHelper.NumberTextFormat(distance * (mass) * transport_ISO);
				}
				return result;
			}
			set
			{
				if (value == _ISO)
					return;

				_ISO = value;
				OnPropertyChanged("ISO");
			}
		}

		#region INotifyPropertyChanged

		/// <summary>
		/// The PropertyChanged event is used by consuming code
		/// (like WPF's binding infrastructure) to detect when
		/// a value has changed.
		/// </summary>
		public event PropertyChangedEventHandler PropertyChanged;

		/// <summary>
		/// Raise the PropertyChanged event for the 
		/// specified property.
		/// </summary>
		/// <param name="propertyName">
		/// A string representing the name of 
		/// the property that changed.</param>
		/// <remarks>
		/// Only raise the event if the value of the property 
		/// has changed from its previous value</remarks>
		protected void OnPropertyChanged(string propertyName)
		{
			{
				// Validate the property name in debug builds
				VerifyProperty(propertyName);

				if (null != PropertyChanged)
				{
					{
						PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
					}
				}
			}
		}

		/// <summary>
		/// Verifies whether the current class provides a property with a given
		/// name. This method is only invoked in debug builds, and results in
		/// a runtime exception if the <see cref="OnPropertyChanged"/> method
		/// is being invoked with an invalid property name. This may happen if
		/// a property's name was changed but not the parameter of the property's
		/// invocation of <see cref="OnPropertyChanged"/>.
		/// </summary>
		/// <param name="propertyName">The name of the changed property.</param>
		[System.Diagnostics.Conditional("DEBUG")]
		private void VerifyProperty(string propertyName)
		{
			{
				Type type = this.GetType();

				// Look for a *public* property with the specified name
				System.Reflection.PropertyInfo pi = type.GetProperty(propertyName);
				if (pi == null)
				{
					{
						// There is no matching property - notify the developer
						string msg = "OnPropertyChanged was invoked with invalid " +
								"property name {{0}}. {{0}} is not a public " +
								"property of {{1}}.";
						msg = String.Format(msg, propertyName, type.FullName);
						System.Diagnostics.Debug.Fail(msg);
					}
				}
			}
		}

		#endregion
	}
}
