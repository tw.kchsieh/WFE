﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFE.Assistants.Helpers;

namespace WFE.Assistants.Models.ORDs
{
	/// <summary>
	/// 加工
	/// </summary>
	public class ComponentProcess
	{
		private int _SN;
		/// <summary>
		/// SN 流水號
		/// </summary>
		public int SN
		{
			get { return _SN; }
			set
			{
				if (value == _SN)
					return;

				_SN = value;
				OnPropertyChanged("SN");
			}
		}

		private int _C_SN;
		/// <summary>
		/// C_SN 原料/零件 SN
		/// </summary>
		public int C_SN
		{
			get { return _C_SN; }
			set
			{
				if (value == _C_SN)
					return;

				_C_SN = value;
				OnPropertyChanged("C_SN");
			}
		}

		private string _C_Name;
		/// <summary>
		/// C_Name 原料/零件名稱
		/// </summary>
		public string C_Name
		{
			get { return _C_Name; }
			set
			{
				if (value == _C_Name)
					return;

				_C_Name = value;
				OnPropertyChanged("C_Name");
			}
		}

		private int _MakeProcessSN;
		/// <summary>
		/// MakeProcessSN
		/// </summary>
		public int MakeProcessSN
		{
			get { return _MakeProcessSN; }
			set
			{
				if (value == _MakeProcessSN)
					return;

				_MakeProcessSN = value;
				OnPropertyChanged("MakeProcessSN");
			}
		}

		private string _MakeProcessName;
		/// <summary>
		/// MakeProcessName
		/// </summary>
		[DisplayName("加工方式")]
		public string MakeProcessName
		{
			get { return _MakeProcessName; }
			set
			{
				if (value == _MakeProcessName)
					return;

				_MakeProcessName = value;
				OnPropertyChanged("MakeProcessName");
			}
		}

		private string _MakeProcessWFP;
		/// <summary>
		/// MakeProcessWFP
		/// </summary>
		[DisplayName("加工方式水足跡 (WFN)")]
		public string MakeProcessWFP
		{
			get { return _MakeProcessWFP; }
			set
			{
				if (value == _MakeProcessWFP)
					return;

				_MakeProcessWFP = value;
				OnPropertyChanged("MakeProcessWFP");
			}
		}

		private string _MakeProcessISO;
		/// <summary>
		/// MakeProcessISO
		/// </summary>
		[DisplayName("加工方式水足跡 (ISO)")]
		public string MakeProcessISO
		{
			get { return _MakeProcessISO; }
			set
			{
				if (value == _MakeProcessISO)
					return;

				_MakeProcessISO = value;
				OnPropertyChanged("MakeProcessISO");
			}
		}

		private string _PowerConsumption;
		/// <summary>
		/// PowerConsumption
		/// </summary>
		[DisplayName("電力消耗量 Kw")]
		public string PowerConsumption
		{
			get { return _PowerConsumption; }
			set
			{
				if (value == _PowerConsumption)
					return;

				_PowerConsumption = value;
				OnPropertyChanged("PowerConsumption");
			}
		}

		private string _PowerCOEEF;
		/// <summary>
		/// PowerCOEEF
		/// </summary>
		[DisplayName("電力系數")]
		public string PowerCOEEF
		{
			get { return _PowerCOEEF; }
			set
			{
				if (value == _PowerCOEEF)
					return;

				_PowerCOEEF = value;
				OnPropertyChanged("PowerCOEEF");
			}
		}

		private string _WorkingHour;
		/// <summary>
		/// WorkingHour
		/// </summary>
		[DisplayName("加工時數 hr")]
		public string WorkingHour
		{
			get { return _WorkingHour; }
			set
			{
				if (value == _WorkingHour)
					return;

				_WorkingHour = value;
				OnPropertyChanged("WorkingHour");
			}
		}

		private string _ComponentMass;
		/// <summary>
		/// ComponentMass
		/// </summary>
		[DisplayName("質量")]
		public string ComponentMass
		{
			get { return _ComponentMass; }
			set
			{
				if (value == _ComponentMass)
					return;

				_ComponentMass = value;
				OnPropertyChanged("ComponentMass");
			}
		}

		private string _CalculateCode;
		/// <summary>
		/// CalculateCode
		/// </summary>
		public string CalculateCode
		{
			get { return _CalculateCode; }
			set
			{
				if (value == _CalculateCode)
					return;

				_CalculateCode = value;
				OnPropertyChanged("CalculateCode");
			}
		}

		private string _WFP;
		/// <summary>
		/// WFP
		/// <param>計算欄位</param>
		/// </summary>
		[DisplayName("加工水足跡(WFN)")]
		public string WFP
		{
			get
			{
				string result = "0";
				switch (this.CalculateCode)
				{
					case "Power":
						result = calculateByPower();
						break;
					case "WFP":
					default:
						result = calculateByMass(this.MakeProcessWFP);
						break;
				}
				return result;
			}
			set
			{
				if (value == _WFP)
					return;

				_WFP = value;
				OnPropertyChanged("WFP");
			}
		}

		private string _ISO;
		/// <summary>
		/// ISO
		/// <param>計算欄位</param>
		/// </summary>
		[DisplayName("加工水足跡(ISO)")]
		public string ISO
		{
			get
			{
				string result = "0";
				switch (this.CalculateCode)
				{
					case "Power":
						result = calculateByPower();
						break;
					case "ISO":
					default:
						result = calculateByMass(this.MakeProcessISO);
						break;
				}
				return result;
			}
			set
			{
				if (value == _ISO)
					return;

				_ISO = value;
				OnPropertyChanged("ISO");
			}
		}

		/// <summary>
		/// 電力系數計算公式
		/// <para>水足跡 = 電力系數 * 電力消耗量(kwh) * 加工時數</para>
		/// </summary>
		/// <returns></returns>
		private string calculateByPower()
		{
			var result = "0";
			double p_coeef, p_consumption, p_workHour;
			var v3 = double.TryParse(PowerCOEEF, out p_coeef);
			var v4 = double.TryParse(PowerConsumption, out p_consumption);
			var v5 = double.TryParse(WorkingHour, out p_workHour);
			if (v3 && v4 && v5)
			{
				result = UIHelper.NumberTextFormat(p_coeef * p_consumption * p_workHour);
			}
			return result;
		}

		private string calculateByMass(string wfp)
		{
			var result = "0";
			double processWFP, mass;
			var v1 = double.TryParse(ComponentMass, out mass);
			var v2 = double.TryParse(wfp, out processWFP);
			if (v1 && v2)
			{
				result = UIHelper.NumberTextFormat(mass * processWFP);
			}
			return result;
		}

		#region INotifyPropertyChanged

		/// <summary>
		/// The PropertyChanged event is used by consuming code
		/// (like WPF's binding infrastructure) to detect when
		/// a value has changed.
		/// </summary>
		public event PropertyChangedEventHandler PropertyChanged;

		/// <summary>
		/// Raise the PropertyChanged event for the 
		/// specified property.
		/// </summary>
		/// <param name="propertyName">
		/// A string representing the name of 
		/// the property that changed.</param>
		/// <remarks>
		/// Only raise the event if the value of the property 
		/// has changed from its previous value</remarks>
		protected void OnPropertyChanged(string propertyName)
		{
			{
				// Validate the property name in debug builds
				VerifyProperty(propertyName);

				if (null != PropertyChanged)
				{
					{
						PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
					}
				}
			}
		}

		/// <summary>
		/// Verifies whether the current class provides a property with a given
		/// name. This method is only invoked in debug builds, and results in
		/// a runtime exception if the <see cref="OnPropertyChanged"/> method
		/// is being invoked with an invalid property name. This may happen if
		/// a property's name was changed but not the parameter of the property's
		/// invocation of <see cref="OnPropertyChanged"/>.
		/// </summary>
		/// <param name="propertyName">The name of the changed property.</param>
		[System.Diagnostics.Conditional("DEBUG")]
		private void VerifyProperty(string propertyName)
		{
			{
				Type type = this.GetType();

				// Look for a *public* property with the specified name
				System.Reflection.PropertyInfo pi = type.GetProperty(propertyName);
				if (pi == null)
				{
					{
						// There is no matching property - notify the developer
						string msg = "OnPropertyChanged was invoked with invalid " +
								"property name {{0}}. {{0}} is not a public " +
								"property of {{1}}.";
						msg = String.Format(msg, propertyName, type.FullName);
						System.Diagnostics.Debug.Fail(msg);
					}
				}
			}
		}

		#endregion
	}
}
