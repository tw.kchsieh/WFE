﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFE.Assistants.Models
{
	/// <summary>
	/// 資料表「材料」對應資料模型
	/// </summary>
	public class Material
	{
		/// <summary>
		/// 識別碼
		/// </summary>
		[DisplayName("序號")]
		public int SN { get; set; }

		/// <summary>
		/// 材料
		/// </summary>
		[DisplayName("材料名稱")]
		public string Name { get; set; }

		/// <summary>
		/// 密度kg/m3
		/// </summary>
		[DisplayName("密度kg/m3")]
		public string Density { get; set; }

		/// <summary>
		/// 藍水(m3)
		/// </summary>
		[DisplayName("藍水 m^3/Kg")]
		public string BlueWater { get; set; }

		/// <summary>
		/// 綠水(m3)
		/// </summary>
		[DisplayName("綠水 m^3/Kg")]
		public string GreenWater { get; set; }

		/// <summary>
		/// 灰水(m3)
		/// </summary>
		[DisplayName("灰水 m^3/Kg")]
		public string GrayWater { get; set; }

		/// <summary>
		/// BOD
		/// <para>水質污染量</para>
		/// </summary>
		[DisplayName("BOD mg/Kg")]
		public string BOD { get; set; }

		/// <summary>
		/// 總水足跡 (m3)
		/// <para>藍水 + 綠水 + 灰水</para>
		/// </summary>
		[DisplayName("總水足跡 (WFN) m^3/Kg")]
		public string WFP { get; set; }

		/// <summary>
		/// ISO-14046
		///<para>水量 (藍水)</para>
		/// </summary>
		[DisplayName("總水足跡 (ISO) m^3/Kg")]
		public string ISO { get; set; }


	}
}
