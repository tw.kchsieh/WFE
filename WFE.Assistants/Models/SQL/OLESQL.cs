﻿using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFE.Assistants.Models.SQL
{
	/// <summary>
	/// OLEDB Model (SQL, ColumnName, ColumnValues, and Parameters)
	/// </summary>
	public class OLESQL
	{
		public string SQL { get; set; }
		public List<OleDbParameter> Parameters { get; set; }
		public List<string> ColumnNames { get; set; }
		public List<string> ColumnValues { get; set; }
		public OLESQL()
		{
			Parameters = new List<OleDbParameter>();
			ColumnNames = new List<string>();
			ColumnValues = new List<string>();
		}

		public void Generate_InsertSQL_With_Dict(string tablename, Dictionary<string, object> param)
		{
			//sql pattern
			var template = @"INSERT INTO {0} ({1}) Values ({2})";

			//
			genColumnNameAndValueFormDict(param);

			//sql query assembly
			var columnNameText = String.Join(",", this.ColumnNames.Select(p => columnNameNormalized(p)).ToArray());
			var columnValueText = String.Join(",", this.ColumnNames.Select(n => "@" + n).ToArray());
			this.SQL = String.Format(template, tablename, columnNameText, columnValueText);
		}

		/// <summary>
		/// 產生 Insert SQL Statement
		/// </summary>
		/// <param name="tablename">資料表名稱</param>
		/// <param name="param">Insert 參數及值 {Key1 = Value1, Key2 = Value2, ...}</param>
		public void GenerateInsertSQL(string tablename, object param)
		{
			//sql pattern
			var template = @"INSERT INTO {0} ({1}) Values ({2})";

			//
			genColumnNameAndValue(param);

			//sql query assembly
			var columnNameText = String.Join(",", this.ColumnNames.Select(p => columnNameNormalized(p)).ToArray());
			var columnValueText = String.Join(",", this.ColumnNames.Select(n => "@" + n).ToArray());
			this.SQL = String.Format(template, tablename, columnNameText, columnValueText);
		}
		public void GenerateInsertSQL(bool doNormalize, string tablename, object param)
		{
			if (doNormalize)
			{
				GenerateInsertSQL(tablename, param);
			}
			else
			{
				//sql pattern
				var template = @"INSERT INTO {0} ({1}) Values ({2})";

				//
				genColumnNameAndValue(param);

				//sql query assembly
				var columnNameText = String.Join(",", this.ColumnNames.Select(p => p).ToArray());
				var columnValueText = String.Join(",", this.ColumnNames.Select(n => "@" + n).ToArray());
				this.SQL = String.Format(template, tablename, columnNameText, columnValueText);
			}

		}

		/// <summary>
		/// 產生 Delete SQL Statement
		/// </summary>
		/// <param name="tablename"></param>
		/// <param name="param"></param>
		public void GenerateDeleteSQL(string tablename, object param)
		{
			var template = @"Delete From {0} Where {1}";
			genColumnNameAndValue(param);

			//build sql
			var setValueAry = this.ColumnNames.Select(a => String.Format("{0}=@{0}\r\n", a)).ToArray();
			var setValueText = String.Join(",", setValueAry);
			this.SQL = String.Format(template, tablename, setValueText);
		}



		/// <summary>
		/// 產生 Update SQL Statement
		/// </summary>
		/// <param name="tableName">資料表名稱</param>
		/// <param name="pKeyName">主鍵欄位名稱</param>
		/// <param name="pKeyValue">主鍵的值</param>
		/// <param name="param">其它 Update 的參數 {Key1 = Value1, Key2 = Value2, ...}</param>
		public void GenerateUpdateSQL(string tableName,
			string pKeyName, string pKeyValue, object param)
		{
			//sql patterns
			var condition = String.Format(@"Where {0} = {1}", pKeyName, pKeyValue);
			var template = @"
UPDATE {0}
SET {1} 
{2}
	";
			//
			genColumnNameAndValue(param);

			//build sql
			var setValueAry = this.ColumnNames.Select(a => String.Format("{0}=@{1}\r\n", columnNameNormalized(a), a)).ToArray();
			var setValueText = String.Join(",", setValueAry);
			this.SQL = String.Format(template, tableName, setValueText, condition);
		}

		#region private method
		/// <summary>
		/// 用 Reflection 技巧產生 Key and Value
		/// </summary>
		/// <param name="param"></param>
		private void genColumnNameAndValue(object param)
		{
			this.ColumnNames.Clear();
			this.ColumnValues.Clear();
			var pps = param.GetType().GetProperties();
			foreach (var pp in pps)
			{
				var pName = pp.Name;
				var rawValue = pp.GetValue(param);
				var pValue = (rawValue != null) ? rawValue.ToString() : "";

				this.ColumnNames.Add(pName);
				this.ColumnValues.Add(pValue);
				this.Parameters.Add(new OleDbParameter("@" + pName, pValue));
			}
		}
		private void genColumnNameAndValueFormDict(Dictionary<string, object> param)
		{
			this.ColumnNames.Clear();
			this.ColumnValues.Clear();

			foreach (var item in param)
			{
				var pName = item.Key;
				var rawValue = item.Value;//.ToString();
				var pValue = (rawValue != null) ? rawValue.ToString() : "";
				this.ColumnNames.Add(pName);
				this.ColumnValues.Add(pValue);
				this.Parameters.Add(new OleDbParameter("@" + pName, pValue));
			}
		}
		private string columnNameNormalized(string name)
		{
			var cName = name;
			if (name == "WFP")
			{
				cName = "[WFP(WFN)]";
			}
			else if (name == "ISO")
			{
				cName = "[WFP(ISO14046)]";
			}
			return cName;
		}
		#endregion
	}
}
