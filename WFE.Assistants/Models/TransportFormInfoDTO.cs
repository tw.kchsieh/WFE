﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFE.Assistants.Models.ORDs;

namespace WFE.Assistants.Models
{
	/// <summary>
	/// 選擇運輸交通工具標題板面資訊
	/// </summary>
	public class TransportFormInfoDTO
	{
		public string FormText { get; set; }
		public string ProductText { get; set; }
		public string C_Name { get; set; }
		public string C_Mass { get; set; }

		public string ProductName;
		public ProductComponent Component;
		public TransportFormInfoDTO(string code)
		{
			switch (code.ToLower())
			{
				case "stagea":
					FormText = "選擇運輸工具 (一般零件)";
					ProductText = "產品";
					C_Name = "零件名稱";
					C_Mass = "零件質量";
					break;
				case "stageb":
					FormText = "選擇運輸工具 (後製加工)";
					ProductText = "產品";
					C_Name = "零件名稱";
					C_Mass = "零件質量";
					break;
			}
		}

		/// <summary>
		/// Use Model
		/// </summary>
		/// <param name="productName"></param>
		/// <param name="curComponent"></param>
		public void UseModel(string productName, ProductComponent curComponent)
		{
			this.Component = curComponent;
			this.ProductName = productName;
		}
	}
}

