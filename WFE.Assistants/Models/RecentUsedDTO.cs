﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFE.Assistants.Models
{
	/// <summary>
	/// 最近曾使用的資料模型
	/// </summary>
	public class RecentUsedDTO
	{
		/// <summary>
		/// Primary Key (Material.SN, Transport.SN, Processes.SN, 加工方式.識別碼)
		/// </summary>
		public int SN { get; set; }

		/// <summary>
		/// 名稱
		/// </summary>
		public string Name { get; set; }

		/// <summary>
		/// 最後使用日期
		/// </summary>
		public DateTime LastUsed { get; set; }

		/// <summary>
		/// 使用次數
		/// </summary>
		public int UsedCount { get; set; }
	}
}
