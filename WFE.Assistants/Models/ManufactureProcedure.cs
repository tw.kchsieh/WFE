﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFE.Assistants.Models
{
	/// <summary>
	/// 製程
	/// </summary>
	public class ManufactureProcedure
	{
		/// <summary>
		/// 序號
		/// </summary>
		[DisplayName("序號")]
		public int SN { get; set; }

		/// <summary>
		/// 製程名稱
		/// </summary>
		[DisplayName("製程名稱")]
		public string ProcessName { get; set; }

		/// <summary>
		/// 密度
		/// </summary>
		[DisplayName("密度 Kg/m^3")]
		public string Density { get; set; }

		/// <summary>
		/// 製程類別
		/// </summary>
		[DisplayName("製程類別")]
		public string ProcessTypeCode { get; set; }

		/// <summary>
		/// 藍水(m3)
		/// </summary>
		[DisplayName("藍水 m^3/Kg")]
		public string BlueWater { get; set; }

		/// <summary>
		/// 綠水(m3)
		/// </summary>
		[DisplayName("綠水 m^3/Kg")]
		public string GreenWater { get; set; }

		/// <summary>
		/// 灰水(m3)
		/// </summary>
		[DisplayName("灰水 m^3/Kg")]
		public string GrayWater { get; set; }

		/// <summary>
		/// BOD
		/// </summary>
		[DisplayName("BOD mg/Kg")]
		public string BOD { get; set; }
		
		/// <summary>
		/// 總水足跡 (m3)
		/// </summary>
		[DisplayName("總水足跡(WFN) m^3/Kg")]
		public string WFP { get; set; }

		/// <summary>
		/// ISO 14046
		/// </summary>
		[DisplayName("總水足跡(ISO) m^3/Kg")]
		public string ISO { get; set; }
	}
}
