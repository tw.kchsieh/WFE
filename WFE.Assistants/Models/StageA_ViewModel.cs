﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFE.Assistants.Helpers;
//
using WFE.Assistants.Models.ORDs;
namespace WFE.Assistants.Models
{
	/// <summary>
	/// 一般製程頁面 ViewModel
	/// </summary>
	public class StageA_ViewModel
	{
		public bool Clear()
		{
			this.Component_SN = 0;
			this.Component_Name = "";
			this.Component_Volumn = "";

			this.Material_SN = "0";
			this.Material_Name = "";
			this.Material_Density = "";
			this.Material_WFP = "";
			this.Material_ISO = "";
			return true;
		}
		public ProductComponent To_ProductComponent()
		{
			var component = new ProductComponent()
			{
				Product_SN = 0,
				Component_SN = this.Component_SN,
				Component_Name = this.Component_Name,
				Component_Volumn = this.Component_Volumn,

				Material_SN = this.Material_SN,
				Material_Name = this.Material_Name,
				Material_Density = this.Material_Density,
				Material_WFP = this.Material_WFP,
				Material_ISO = this.Material_ISO,
				CreateDate = DateTime.Now,
			};
			return component;
		}

		private int _Component_SN;
		/// <summary>
		/// Component_SN
		/// </summary>
		public int Component_SN
		{
			get { return _Component_SN; }
			set
			{
				if (value == _Component_SN)
					return;

				_Component_SN = value;
				OnPropertyChanged("Component_SN");
			}
		}

		private string _Component_Name;
		/// <summary>
		/// Component_Name (獲取零件名稱)
		/// </summary>
		public string Component_Name
		{
			get { return _Component_Name; }
			set
			{
				if (value == _Component_Name)
					return;

				_Component_Name = value;
				OnPropertyChanged("Component_Name");
			}
		}

		private string _Component_Volumn;
		/// <summary>
		/// Component_Volumn (獲取零件體積)
		/// </summary>
		public string Component_Volumn
		{
			get
			{
				var result = _Component_Volumn;
				double volumn;
				if (Double.TryParse(_Component_Volumn, out volumn))
				{
					result = UIHelper.NumberTextFormat(volumn);
				}
				return result;
			}
			set
			{
				if (value == _Component_Volumn)
					return;

				_Component_Volumn = value;
				OnPropertyChanged("Component_Volumn");
			}
		}

		private string _Material_SN;
		/// <summary>
		/// Raw_Material_SN (材料 SN)
		/// </summary>
		public string Material_SN
		{
			get { return _Material_SN; }
			set
			{
				if (value == _Material_SN)
					return;

				_Material_SN = value;
				OnPropertyChanged("Material_SN");
			}
		}

		private string _Material_Name;
		/// <summary>
		/// Raw_Material_Name (材料名稱)
		/// </summary>
		public string Material_Name
		{
			get { return _Material_Name; }
			set
			{
				if (value == _Material_Name)
					return;

				_Material_Name = value;
				OnPropertyChanged("Material_Name");
			}
		}

		private string _Material_Density;
		/// <summary>
		/// Raw_Material_Density (材料密度)
		/// </summary>
		public string Material_Density
		{
			get
			{
				double density;
				string result = _Material_Density;
				var v1 = Double.TryParse(_Material_Density, out density);
				if (v1)
				{
					result = UIHelper.NumberTextFormat(density);
				}
				return result;
			}
			set
			{
				if (value == _Material_Density)
					return;

				_Material_Density = value;
				OnPropertyChanged("Material_Density");
			}
		}

		private string _Component_Mass;
		/// <summary>
		/// Component_Mass (獲取零件的質量)
		/// <para>質量 = 零件體積 * 材料密度</para>
		/// </summary>
		public string Component_Mass
		{
			get
			{
				double volumn, density;
				var v1 = Double.TryParse(Component_Volumn, out volumn);
				var v2 = Double.TryParse(Material_Density, out density);
				if (v1 && v2)
				{
					_Component_Mass = UIHelper.NumberTextFormat(volumn * density);
				}
				else
				{
					_Component_Mass = "";
				}
				return _Component_Mass;
			}
		}


		private string _Material_WFP;
		/// <summary>
		/// Raw_Material_WFP (材料水足跡)
		/// </summary>
		public string Material_WFP
		{
			get
			{
				double wfp;
				var result = _Material_WFP;
				var v1 = double.TryParse(_Material_WFP, out wfp);
				if (v1)
				{
					result = UIHelper.NumberTextFormat(wfp);
				}
				return result;
			}
			set
			{
				if (value == _Material_WFP)
					return;

				_Material_WFP = value;
				OnPropertyChanged("Material_WFP");
			}
		}

		private string _Material_ISO;
		/// <summary>
		/// 材料水足跡 (ISO 14046)
		/// </summary>
		public string Material_ISO
		{
			get
			{
				double wfp;
				var result = _Material_ISO;
				var v1 = double.TryParse(_Material_ISO, out wfp);
				if (v1)
				{
					result = UIHelper.NumberTextFormat(wfp);
				}
				return result;
			}
			set
			{
				if (value == _Material_ISO)
					return;

				_Material_ISO = value;
				OnPropertyChanged("Material_ISO");
			}
		}

		private string _Component_WFP;
		/// <summary>
		/// Component_WFP (原料階段水足跡 - WFN)
		/// <para>Component_Mass * Material_WFP</para>
		/// </summary>
		public string Component_WFP
		{
			get
			{
				double mass, material_wfp;
				var v1 = Double.TryParse(Component_Mass, out mass);
				var v2 = Double.TryParse(Material_WFP, out material_wfp);
				if (v1 && v2)
				{
					_Component_WFP = UIHelper.NumberTextFormat(mass * material_wfp);
				}
				else
				{
					_Component_WFP = "N/A";
				}

				return _Component_WFP;
			}
		}

		private string _Component_ISO;
		/// <summary>
		/// Component_ISO (原料階段水足跡 - ISO 14046)
		/// <para>Component_Mass * Material_ISO</para>
		/// </summary>
		public string Component_ISO
		{
			get
			{
				double mass, material_iso;
				var v1 = Double.TryParse(Component_Mass, out mass);
				var v2 = Double.TryParse(Material_ISO, out material_iso);
				if (v1 && v2)
				{
					_Component_ISO = UIHelper.NumberTextFormat(mass * material_iso);
				}
				else
				{
					_Component_ISO = "N/A";
				}

				return _Component_ISO;
			}
		}

		#region INotifyPropertyChanged

		/// <summary>
		/// The PropertyChanged event is used by consuming code
		/// (like WPF's binding infrastructure) to detect when
		/// a value has changed.
		/// </summary>
		public event PropertyChangedEventHandler PropertyChanged;

		/// <summary>
		/// Raise the PropertyChanged event for the 
		/// specified property.
		/// </summary>
		/// <param name="propertyName">
		/// A string representing the name of 
		/// the property that changed.</param>
		/// <remarks>
		/// Only raise the event if the value of the property 
		/// has changed from its previous value</remarks>
		protected void OnPropertyChanged(string propertyName)
		{
			{
				// Validate the property name in debug builds
				VerifyProperty(propertyName);

				if (null != PropertyChanged)
				{
					{
						PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
					}
				}
			}
		}

		/// <summary>
		/// Verifies whether the current class provides a property with a given
		/// name. This method is only invoked in debug builds, and results in
		/// a runtime exception if the <see cref="OnPropertyChanged"/> method
		/// is being invoked with an invalid property name. This may happen if
		/// a property's name was changed but not the parameter of the property's
		/// invocation of <see cref="OnPropertyChanged"/>.
		/// </summary>
		/// <param name="propertyName">The name of the changed property.</param>
		[System.Diagnostics.Conditional("DEBUG")]
		private void VerifyProperty(string propertyName)
		{
			{
				Type type = this.GetType();

				// Look for a *public* property with the specified name
				System.Reflection.PropertyInfo pi = type.GetProperty(propertyName);
				if (pi == null)
				{
					{
						// There is no matching property - notify the developer
						string msg = "OnPropertyChanged was invoked with invalid " +
								"property name {{0}}. {{0}} is not a public " +
								"property of {{1}}.";
						msg = String.Format(msg, propertyName, type.FullName);
						System.Diagnostics.Debug.Fail(msg);
					}
				}
			}
		}



		#endregion
	}
}
