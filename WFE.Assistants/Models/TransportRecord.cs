﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFE.Assistants.Models
{
	/// <summary>
	/// 運輸工具 WFP
	/// </summary>
	public class TransportRecord
	{
		private string _C_SN;
		/// <summary>
		/// 材料或零件SN
		/// </summary>
		[DisplayName("SN")]
		public string C_SN
		{
			get { return _C_SN; }
			set
			{
				if (value == _C_SN)
					return;

				_C_SN = value;
				OnPropertyChanged("C_SN");
			}
		}

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("原料/零件")]
		public string C_Name { get; set; }

		private int _TransportSN;
		/// <summary>
		/// 運輸工具 SN
		/// </summary>
		[DisplayName("運輸序號")]
		public int TransportSN
		{
			get { return _TransportSN; }
			set
			{
				if (value == _TransportSN)
					return;

				_TransportSN = value;
				OnPropertyChanged("TransportSN");
			}
		}

		private string _Name;
		/// <summary>
		/// Name
		/// </summary>
		[DisplayName("運輸工具")]
		public string Name
		{
			get { return _Name; }
			set
			{
				if (value == _Name)
					return;

				_Name = value;
				OnPropertyChanged("Name");
			}
		}

		/// <summary>
		/// 運輸工具水足跡
		/// </summary>
		[DisplayName("運輸工具水足跡")]
		public string TransportWFP { get; set; }

		private string _Distance;
		/// <summary>
		/// Distance
		/// </summary>
		[DisplayName("距離")]
		public string Distance
		{
			get { return _Distance; }
			set
			{
				if (value == _Distance)
					return;

				_Distance = value;
				OnPropertyChanged("Distance");
			}
		}

		private string _Consumption;
		/// <summary>
		/// Consumption
		/// </summary>
		public string Consumption
		{
			get { return _Consumption; }
			set
			{
				if (value == _Consumption)
					return;

				_Consumption = value;
				OnPropertyChanged("Consumption");
			}
		}

		private string _ConsumptionCOEEF;
		/// <summary>
		/// ConsumptionCOEEF
		/// </summary>
		public string ConsumptionCOEEF
		{
			get { return _ConsumptionCOEEF; }
			set
			{
				if (value == _ConsumptionCOEEF)
					return;

				_ConsumptionCOEEF = value;
				OnPropertyChanged("ConsumptionCOEEF");
			}
		}

		private string _Weight;
		/// <summary>
		/// Weight
		/// </summary>
		[DisplayName("重量")]
		public string Weight
		{
			get { return _Weight; }
			set
			{
				if (value == _Weight)
					return;

				_Weight = value;
				OnPropertyChanged("Weight");
			}
		}

		private string _WFP;
		/// <summary>
		/// WFP
		/// </summary>
		[DisplayName("運輸水足跡")]
		public string WFP
		{
			get { return _WFP; }
			set
			{
				if (value == _WFP)
					return;

				_WFP = value;
				OnPropertyChanged("WFP");
			}
		}

		#region INotifyPropertyChanged

		/// <summary>
		/// The PropertyChanged event is used by consuming code
		/// (like WPF's binding infrastructure) to detect when
		/// a value has changed.
		/// </summary>
		public event PropertyChangedEventHandler PropertyChanged;

		/// <summary>
		/// Raise the PropertyChanged event for the 
		/// specified property.
		/// </summary>
		/// <param name="propertyName">
		/// A string representing the name of 
		/// the property that changed.</param>
		/// <remarks>
		/// Only raise the event if the value of the property 
		/// has changed from its previous value</remarks>
		protected void OnPropertyChanged(string propertyName)
		{
			{
				// Validate the property name in debug builds
				VerifyProperty(propertyName);

				if (null != PropertyChanged)
				{
					{
						PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
					}
				}
			}
		}

		/// <summary>
		/// Verifies whether the current class provides a property with a given
		/// name. This method is only invoked in debug builds, and results in
		/// a runtime exception if the <see cref="OnPropertyChanged"/> method
		/// is being invoked with an invalid property name. This may happen if
		/// a property's name was changed but not the parameter of the property's
		/// invocation of <see cref="OnPropertyChanged"/>.
		/// </summary>
		/// <param name="propertyName">The name of the changed property.</param>
		[System.Diagnostics.Conditional("DEBUG")]
		private void VerifyProperty(string propertyName)
		{
			{
				Type type = this.GetType();

				// Look for a *public* property with the specified name
				System.Reflection.PropertyInfo pi = type.GetProperty(propertyName);
				if (pi == null)
				{
					{
						// There is no matching property - notify the developer
						string msg = "OnPropertyChanged was invoked with invalid " +
								"property name {{0}}. {{0}} is not a public " +
								"property of {{1}}.";
						msg = String.Format(msg, propertyName, type.FullName);
						System.Diagnostics.Debug.Fail(msg);
					}
				}
			}
		}

		#endregion


	}
}
