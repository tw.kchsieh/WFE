﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFE.Assistants.Helpers;
using WFE.Assistants.Models.AssemblyPageViewModels;
using WFE.Assistants.Models.ORDs;

namespace WFE.Assistants.Models
{
	/// <summary>
	/// 產品
	/// </summary>
	public class ProductViewModel
	{
		/// <summary>
		/// 一般零件
		/// <para>From Solidworks</para>
		/// </summary>
		public List<ProductComponent> Components { get; set; }

		/// <summary>
		/// 後製加工零件
		/// <para>選取一般零件作為後製加工</para>
		/// </summary>
		public List<ProductComponent> PostProcessComponents { get; set; }

		/// <summary>
		/// 特殊零件記錄
		/// </summary>
		public List<ProductSpecialComponent> SpecialComponents { get; set; }

		/// <summary>
		/// 零件組裝清單
		/// </summary>
		public List<Component_AssemblyItem> AssemblyItems { get; set; }

		/// <summary>
		/// 組裝細節
		/// </summary>
		public List<Component_AssemblyDetail> AssemblyDetails { get; set; }

		/// <summary>
		/// 零件組裝記錄
		/// <para>組裝清單 join 組裝細節</para>
		/// </summary>
		public List<AssemblyRecord> AssemblyComponents { get; set; }

		/// <summary>
		/// 運輸至組裝廠記錄
		/// </summary>
		public List<ComponentTransport> AssemblyTransports { get; set; }

		/// <summary>
		/// 建構子
		/// </summary>
		public ProductViewModel()
		{
			Components = new List<ProductComponent>();
			PostProcessComponents = new List<ProductComponent>();
			SpecialComponents = new List<ProductSpecialComponent>();

			AssemblyComponents = new List<AssemblyRecord>();
			AssemblyDetails = new List<Component_AssemblyDetail>();
			AssemblyTransports = new List<ComponentTransport>();
			AssemblyItems = new List<Component_AssemblyItem>();

			ProductDesc = "";
			ProductName = "";
		}

		#region Public Methods

		/// <summary>
		/// 流水號設為 0
		/// </summary>
		public void Reset()
		{
			this.SN = 0;
			//一般零件
			foreach (var item in this.Components)
			{
				item.Component_SN = 0;
				item.Product_SN = 0;

				//一般零件:運輸
				foreach (var transport in item.ComponentTransports)
				{
					transport.C_SN = 0;
					transport.SN = 0;
				}
				//一般零件:加工
				foreach (var mkProcess in item.ComponentProcesses)
				{
					mkProcess.SN = 0;
					mkProcess.C_SN = 0;
				}
			}

			//後製加工
			foreach (var postC in this.PostProcessComponents)
			{
				postC.Product_SN = 0;
				postC.Component_SN = 0;
				postC.PostComponent_SN = 0;
				//後製加工:運輸
				foreach (var transport in postC.ComponentTransports)
				{
					transport.C_SN = 0;
					transport.SN = 0;
				}
				//後製加工:加工
				foreach (var mkProcess in postC.ComponentProcesses)
				{
					mkProcess.SN = 0;
					mkProcess.C_SN = 0;
				}
			}

			//特殊零件
			foreach (var special in this.SpecialComponents)
			{
				special.SN = 0;
				special.Product_SN = 0;
			}
		}

		/// <summary>
		/// ReNew
		/// </summary>
		/// <returns></returns>
		public bool ReNew()
		{
			var success = true;
			ProductName = "";
			ProductDesc = "";
			CreateDate = DateTime.Now;
			SN = 0;
			AssemblyComponents.Clear();
			AssemblyDetails.Clear();
			AssemblyTransports.Clear();
			AssemblyItems.Clear();
			return success;
		}


		/// <summary>
		/// 新增零件
		/// <para>Captured by SolidWorks</para>
		/// </summary>
		/// <param name="component"></param>
		public void AddComponent(ProductComponent component)
		{
			this.Components.Add(component);
		}

		/// <summary>
		/// 零件是否存在
		/// <para>以零件名稱判斷</para>
		/// </summary>
		/// <param name="name"></param>
		/// <returns></returns>
		public bool ComponentExists(string name)
		{
			return Components.Any(p => p.Component_Name == name);
		}

		/// <summary>
		/// 取得 ProductComponent by Name
		/// </summary>
		/// <param name="component_Name"></param>
		/// <returns></returns>
		public ProductComponent GetProductComponet(string component_Name)
		{
			return Components.SingleOrDefault(p => p.Component_Name == component_Name);
		}

		/// <summary>
		/// GetComponet (一般，後製，特殊)
		/// </summary>
		/// <param name="component_Name"></param>
		/// <returns></returns>
		public ProductComponent GetComponet(string component_Name, string source)
		{
			ProductComponent component = null;
			switch (source)
			{
				case "general":
					component = Components.SingleOrDefault(p => p.Component_Name == component_Name);
					break;
				case "post":
					component = PostProcessComponents.SingleOrDefault(p => p.Component_Name == component_Name);
					break;
				case "special":
					component = SpecialComponents.Where(p => p.Component_Name == component_Name)
									.Select(p => new ProductComponent()
									{
										Component_Name = p.Component_Name,
										Product_SN = p.Product_SN,
									}).SingleOrDefault();
					break;
				default:
					break;

			}
			return component;
		}

		/// <summary>
		/// 取得一般零件 - 加工階段總水足跡
		/// </summary>
		/// <returns></returns>
		public string GetTotalComponentProcessWFP()
		{
			return UIHelper.NumberTextFormat(Components.Select(p => p.ComponentProcessWFP).Sum());
		}

		/// <summary>
		/// 取得一般零件 - 加工階段總水足跡 ISO 14046
		/// </summary>
		/// <returns></returns>
		public string GetTotalComponentProcessISO()
		{
			return UIHelper.NumberTextFormat(Components.Select(p => p.ComponentProcessISO).Sum());
		}


		/// <summary>
		/// 取得一般零件 - 運輸階段總水足跡
		/// </summary>
		/// <returns></returns>
		public string GetTotalComponentTransportWFP()
		{
			return UIHelper.NumberTextFormat(Components.Select(p => p.ComponentTransportWFP).Sum());
		}

		/// <summary>
		/// 取得一般零件 - 運輸階段總水足跡 (ISO 14046)
		/// </summary>
		/// <returns></returns>
		public string GetTotalComponentTransportISO()
		{

			return UIHelper.NumberTextFormat(Components.Select(p => p.ComponentTransportISO).Sum());
		}

		/// <summary>
		/// 特殊零件是否已存在記錄中
		/// </summary>
		/// <param name="component_Name"></param>
		/// <returns></returns>
		public bool SpecialComponentExists(string component_Name)
		{
			return this.SpecialComponents.Any(p => p.Component_Name == component_Name);
		}
		#endregion

		/// <summary>
		/// Product.SN
		/// </summary>
		public int SN { get; set; }

		private string _ProductName;
		/// <summary>
		/// ProductName
		/// </summary>
		[DisplayName("產	品名稱")]
		public string ProductName
		{
			get { return _ProductName; }
			set
			{
				if (value == _ProductName)
					return;

				_ProductName = value;
				OnPropertyChanged("ProductName");
			}
		}

		/// <summary>
		/// 產品建立時間
		/// </summary>
		public DateTime CreateDate { get; set; }


		/// <summary>
		/// 產品描述
		/// </summary>
		[DisplayName("產	品描述")]
		public string ProductDesc { get; set; }

		/// <summary>
		/// StageA_WFP
		/// </summary>
		[DisplayName("一般製程總水足跡(WFN)")]
		public string StageA_WFP
		{
			get
			{
				//
				var wfp1 = Components.Sum(p => Convert.ToDouble(p.WFP));//原料總水WFN
				var wfp2 = Components.Sum(p => p.ComponentTransportWFP);//加工總水WFN
				var wfp3 = Components.Sum(p => p.ComponentProcessWFP);//運輸總水WFN
				return UIHelper.NumberTextFormat(wfp1 + wfp2 + wfp3);
			}
		}
		//
		/// <summary>
		/// StageA_ISO
		/// </summary>
		[DisplayName("一般製程總水足跡 (ISO)")]
		public string StageA_ISO
		{
			get
			{
				//
				var wfp1 = Components.Sum(p => Convert.ToDouble(p.ISO));//原料總水ISO
				var wfp2 = Components.Sum(p => p.ComponentTransportISO);//加工總水ISO
				var wfp3 = Components.Sum(p => p.ComponentProcessISO);//運輸總水ISO
				return UIHelper.NumberTextFormat(wfp1 + wfp2 + wfp3);
			}
		}

		/// <summary>
		/// StageB_WFP
		/// </summary>
		[DisplayName("後製加工階段總水足跡 (WFN)")]
		public string StageB_WFP
		{
			get
			{
				var wfp1 = PostProcessComponents.Sum(p => p.ComponentProcessWFP);//加工總水
				var wfp2 = PostProcessComponents.Sum(p => p.ComponentTransportWFP);//運輸總水
				return UIHelper.NumberTextFormat(wfp1 + wfp2);
			}
		}
		/// <summary>
		/// StageB_ISO
		/// </summary>
		[DisplayName("後製加工階段總水足跡 (ISO 14046)")]
		public string StageB_ISO
		{
			get
			{
				var ISO1 = PostProcessComponents.Sum(p => p.ComponentProcessISO);//加工總水
				var ISO2 = PostProcessComponents.Sum(p => p.ComponentTransportISO);//運輸總水
				return UIHelper.NumberTextFormat(ISO1 + ISO2);
			}
		}

		/// <summary>
		/// StageC_WFP
		/// </summary>
		[DisplayName("特殊零件階段總水足跡 (WFN)")]
		public string StageC_WFP
		{
			get
			{
				var wfp = SpecialComponents.Sum(p => Convert.ToDouble(p.WFP));
				return UIHelper.NumberTextFormat(wfp);
			}

		}
		/// <summary>
		/// StageC_ISO
		/// </summary>
		[DisplayName("特殊零件階段總水足跡 (ISO 14046)")]
		public string StageC_ISO
		{
			get
			{
				var ISO = SpecialComponents.Sum(p => Convert.ToDouble(p.ISO));
				return UIHelper.NumberTextFormat(ISO);
			}
		}

		/// <summary>
		/// 產品組裝階段總水足跡 (WFN)
		/// <para>組裝運輸總水 + 組裝總水</para>
		/// </summary>
		public string Stage_Assembly_WFP
		{
			get
			{
				var transport = AssemblyTransports.Sum(p => Convert.ToDouble(p.WFP));
				var assembly = AssemblyDetails.Sum(p => Convert.ToDouble(p.CalcWFP));
				return UIHelper.NumberTextFormat(transport + assembly);
			}
		}

		/// <summary>
		/// 產品組裝階段總水足跡 (ISO 14046)
		/// <para>組裝運輸總水 + 組裝總水</para>
		/// </summary>
		public string Stage_Assembly_ISO
		{
			get
			{
				var transport = AssemblyTransports.Sum(p => Convert.ToDouble(p.ISO));
				var assembly = AssemblyDetails.Sum(p => Convert.ToDouble(p.CalcISO));
				return UIHelper.NumberTextFormat(transport + assembly);
			}
		}

		/// <summary>
		/// Total_WFP
		/// </summary>
		[DisplayName("總水足跡 (WFN)")]
		public string Total_WFP
		{
			get
			{
				var result = "0";
				double a, b, c, d;
				var v1 = Double.TryParse(StageA_WFP, out a);
				var v2 = Double.TryParse(StageB_WFP, out b);
				var v3 = Double.TryParse(StageC_WFP, out c);
				var v4 = Double.TryParse(Stage_Assembly_WFP, out d);
				if (v1 && v2 && v3 && v4)
				{
					result = UIHelper.NumberTextFormat(a + b + c + d);
				}
				return result;
			}
		}
		/// <summary>
		/// Total_ISO
		/// </summary>
		[DisplayName("總水足跡 (ISO 14046)")]
		public string Total_ISO
		{
			get
			{
				var result = "0";
				double a, b, c, d;
				var v1 = Double.TryParse(StageA_ISO, out a);
				var v2 = Double.TryParse(StageB_ISO, out b);
				var v3 = Double.TryParse(StageC_ISO, out c);
				var v4 = Double.TryParse(Stage_Assembly_ISO, out d);
				if (v1 && v2 && v3 && v4)
				{
					result = UIHelper.NumberTextFormat(a + b + c + d);
				}
				return result;
			}
		}

		#region INotifyPropertyChanged

		/// <summary>
		/// The PropertyChanged event is used by consuming code
		/// (like WPF's binding infrastructure) to detect when
		/// a value has changed.
		/// </summary>
		public event PropertyChangedEventHandler PropertyChanged;

		/// <summary>
		/// Raise the PropertyChanged event for the 
		/// specified property.
		/// </summary>
		/// <param name="propertyName">
		/// A string representing the name of 
		/// the property that changed.</param>
		/// <remarks>
		/// Only raise the event if the value of the property 
		/// has changed from its previous value</remarks>
		protected void OnPropertyChanged(string propertyName)
		{
			{
				// Validate the property name in debug builds
				VerifyProperty(propertyName);

				if (null != PropertyChanged)
				{
					{
						PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
					}
				}
			}
		}













		/// <summary>
		/// Verifies whether the current class provides a property with a given
		/// name. This method is only invoked in debug builds, and results in
		/// a runtime exception if the <see cref="OnPropertyChanged"/> method
		/// is being invoked with an invalid property name. This may happen if
		/// a property's name was changed but not the parameter of the property's
		/// invocation of <see cref="OnPropertyChanged"/>.
		/// </summary>
		/// <param name="propertyName">The name of the changed property.</param>
		[System.Diagnostics.Conditional("DEBUG")]
		private void VerifyProperty(string propertyName)
		{
			{
				Type type = this.GetType();

				// Look for a *public* property with the specified name
				System.Reflection.PropertyInfo pi = type.GetProperty(propertyName);
				if (pi == null)
				{
					{
						// There is no matching property - notify the developer
						string msg = "OnPropertyChanged was invoked with invalid " +
								"property name {{0}}. {{0}} is not a public " +
								"property of {{1}}.";
						msg = String.Format(msg, propertyName, type.FullName);
						System.Diagnostics.Debug.Fail(msg);
					}
				}
			}
		}



		#endregion
	}
}
