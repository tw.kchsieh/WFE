﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFE.Assistants.Models
{
	public class RecentUsedTransport
	{
		public int TransportSN { get; set; }
		public string Name { get; set; }
		public DateTime LastUsed { get; set; }
		public int UsedCount { get; set; }
	}
}
