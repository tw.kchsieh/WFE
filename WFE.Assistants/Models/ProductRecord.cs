﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFE.Assistants.Models
{
	public class ProductRecord
	{
		[DisplayName("序號")]
		public int SN { get; set; }

		[DisplayName("專案名稱")]
		public string ProductName { get; set; }

		[DisplayName("專案描述")]
		public string ProductDesc { get; set; }

		[DisplayName("建立日期")]
		public string CreateDate { get; set; }

		[DisplayName("修改日期")]
		public string ModifiedDate { get; set; }
	}
}
