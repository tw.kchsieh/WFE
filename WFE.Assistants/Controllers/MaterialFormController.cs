﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
//
using WFE.Assistants.Models;
using WFE.Assistants.Repos;

namespace WFE.Assistants.Controllers
{
	public class MaterialFormController
	{
		#region DataGridView

		//DataGridView Data Binding
		private List<Material> _RawData;
		private BindingSource _bs;// = new BindingSource();
		private BindingList<Material> _blist;
		private int _SelectedMaterialSN = 0;

		//DataGrid View Hide column Names
		private string[] _hideColumnNames;
		#endregion

		#region ComboBox
		private List<RecentUsedMaterial> _RecentUsedMaterials;
		#endregion
		//Controls
		private DataGridView _dv;
		private TextBox _filter_TB;
		private ComboBox _recentUsed_CB;
		private TextBox _selectedMaterial_TB;
		private TextBox _selectedMaterialWFP_TB;
		private TextBox _selectedMaterialISO_TB;
		private Label _filter_info_Label;

		//
		private string _ErrMsg;

		public MaterialFormController()
		{
			_hideColumnNames = new string[] { "SN", "Density" };
		}

		public void Filter(string filter)
		{
			if (String.IsNullOrEmpty(filter))
			{
				CleanFilter();
			}
			else
			{
				var filterResult = _RawData.Where(p => p.Name.Contains(filter)).ToList();
				dataGridViewDataBinding(filterResult);
			}
		}

		public void CleanFilter()
		{
			dataGridViewDataBinding(_RawData);
		}

		public Material GetSelectedMaterialModel()
		{
			var repo = new MaterialRepo();
			var model = repo.GetMaterial(_SelectedMaterialSN);
			return model;
		}

		/// <summary>
		/// Setup
		/// <para>Data Binding</para>
		/// </summary>
		/// <param name="dv"></param>
		/// <param name="filter_TextBox"></param>
		/// <param name="recentUsed_ComboBox"></param>
		/// <param name="selectedMaterial_TextBox"></param>
		/// <param name="selectedMaterialWFP_TextBox"></param>
		public void Setup(DataGridView dv,
			TextBox filter_TextBox,
			ComboBox recentUsed_ComboBox,
			TextBox selectedMaterial_TextBox,
			TextBox selectedMaterialWFP_TextBox,
			TextBox selectedMaterialISO_TextBox,
			Label filter_info_Label)
		{
			_dv = dv;
			_filter_TB = filter_TextBox;
			_recentUsed_CB = recentUsed_ComboBox;
			_selectedMaterial_TB = selectedMaterial_TextBox;
			_selectedMaterialWFP_TB = selectedMaterialWFP_TextBox;
			_selectedMaterialISO_TB = selectedMaterialISO_TextBox;
			_filter_info_Label = filter_info_Label;

			//dataGridView setup
			dataGridViewSetup();

			//ComboBox Setup
			_recentUsed_CB.DropDownStyle = ComboBoxStyle.DropDownList;//整列選取
			comboBoxDataBinding();
		}

		public bool SaveChoiceItem()
		{
			var repo = new MaterialRepo();
			var success = repo.SaveRecentMaterial(_SelectedMaterialSN);
			if (!success)
			{
				_ErrMsg = repo.GetErrMsg();
			}
			return success;
		}

		public string GetErrMsg()
		{
			return _ErrMsg;
		}

		private void comboBoxDataBinding()
		{
			//data
			var repo = new MaterialRepo();
			_RecentUsedMaterials = new List<RecentUsedMaterial>();
			_RecentUsedMaterials.Add(new RecentUsedMaterial()
			{
				MaterialSN = 0,
				Name = "請選擇"
			});
			var records = repo.GetRecentMaterial();
			_RecentUsedMaterials.AddRange(records);

			if (_RecentUsedMaterials.Count < 2)
			{
				_recentUsed_CB.Enabled = false;
			}

			//setup
			_recentUsed_CB.DisplayMember = "Name";
			_recentUsed_CB.ValueMember = "MaterialSN";

			_recentUsed_CB.DataSource = _RecentUsedMaterials;
			_recentUsed_CB.SelectedIndexChanged += _recentUsed_CB_SelectedIndexChanged;

		}

		private void _recentUsed_CB_SelectedIndexChanged(object sender, EventArgs e)
		{
			_SelectedMaterialSN = 0;
			var idx = (sender as ComboBox).SelectedIndex;
			var item = _RecentUsedMaterials[idx];
			if (item.MaterialSN > 0)
			{
				var repo = new MaterialRepo();
				var result = repo.GetMaterial(item.MaterialSN);
				if (result != null)
				{
					_selectedMaterial_TB.Text = result.Name;
					_selectedMaterialWFP_TB.Text = result.WFP;
					_selectedMaterialISO_TB.Text = result.ISO;
					_SelectedMaterialSN = result.SN;
				}
			}
			else
			{
				MessageBox.Show("請選擇項目");
			}
		}

		private void _dv_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
		{
			var dvColumns = _dv.Columns.Cast<DataGridViewColumn>();
			foreach (var name in _hideColumnNames)
			{
				var col = dvColumns.SingleOrDefault(p => p.Name == name);
				if (col != null)
				{
					col.Visible = false;
				}
			}
		}

		private void _dv_CellClick(object sender, DataGridViewCellEventArgs e)
		{
			_SelectedMaterialSN = 0;
			if (e.RowIndex > -1)
			{
				var row = _dv.Rows[e.RowIndex];
				_SelectedMaterialSN = Convert.ToInt32(row.Cells["SN"].Value);
				var nameCell = row.Cells["Name"];//.ToString();
				var wfpCell = row.Cells["WFP"];//.ToString();
				var isoCell = row.Cells["ISO"];
				if (nameCell != null && nameCell.Value != null)
				{
					_selectedMaterial_TB.Text = nameCell.Value.ToString();
				}
				if (wfpCell != null && wfpCell.Value != null)
				{
					_selectedMaterialWFP_TB.Text = wfpCell.Value.ToString();
				}
				if (isoCell != null && isoCell.Value != null)
				{
					_selectedMaterialISO_TB.Text = isoCell.Value.ToString();
				}
			}
		}

		/// <summary>
		/// 資料初始化 & 繫結
		/// </summary>
		private void dataGridViewSetup()
		{
			//setup
			_dv.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;//欄位自動填滿 GridView
			_dv.SelectionMode = DataGridViewSelectionMode.FullRowSelect;//整列選取
			_dv.AllowUserToAddRows = false;//禁止新增 Row
			_dv.AllowUserToDeleteRows = false;//禁止刪除
			_dv.ReadOnly = true;//禁止編輯
			_dv.MultiSelect = false;//禁止多選
			_dv.RowHeadersVisible = false; //左側不顯示
			_dv.AllowUserToResizeRows = false;

			//event
			_dv.CellClick += _dv_CellClick;//選取事件
			_dv.DataBindingComplete += _dv_DataBindingComplete;

			//init Data
			var repo = new MaterialRepo();
			_RawData = repo.GetData();

			//bind data
			dataGridViewDataBinding(_RawData);
		}

		private void dataGridViewDataBinding(List<Material> records)
		{
			_blist = new BindingList<Material>(records);
			_bs = new BindingSource() { DataSource = _blist };
			_dv.DataSource = _bs;
			_filter_info_Label.Text = String.Format("{0} / {1}", records.Count, _RawData.Count);
		}
	}
}
