﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WFE.Assistants.Helpers;
using WFE.Assistants.Models;
using WFE.Assistants.Repos;

namespace WFE.Assistants.Controllers
{
	/// <summary>
	/// 製程 Windows Form Controller
	/// </summary>
	public class ManufactureProcedureFormController
	{
		//private window control variable
		private TextBox _nameFilter_TextBox;
		private ComboBox _recentUsed_ComboBox;
		private DataGridView _dv;
		private TextBox _makeProcessName_TextBox;
		private TextBox _makeProcess_WFP_TextBox;
		private TextBox _makeProcess_ISO_TextBox;
		private Label _filter_Info_Label;
		private TextBox _Density_TextBox;

		private StageC_ViewModel _stageCViewModel;
		//選中的製程 SN
		private int _SelectedSN = 0;

		//Data
		private List<ManufactureProcedure> _RawData;
		private BindingSource _bs;
		private BindingList<ManufactureProcedure> _blist;

		//For ComboBox raw data
		private List<RecentUsedDTO> _RecentUsedData;

		//_ErrMsg
		private string _ErrMsg;

		private string[] _hideColumnNames = new string[] { "SN" };

		public ManufactureProcedure GetSelectedModel()
		{
			var repo = new ManufactureProcedureRepo();
			var model = repo.GetProcesses(_SelectedSN);
			return model;
		}

		public void Filter(string text)
		{
			if (String.IsNullOrEmpty(text))
			{
				dataGridViewDataBinding(_RawData);
			}
			else
			{
				var filterRecords = _RawData.Where(p => p.ProcessName.Contains(text)).ToList();
				dataGridViewDataBinding(filterRecords);
			}
		}

		public string GetErrMsg()
		{
			return _ErrMsg;
		}


		public bool SaveChoiceItem()
		{
			var repo = new ManufactureProcedureRepo();
			var success = repo.SaveRecentUsed(_SelectedSN);
			if (!success)
			{
				_ErrMsg = repo.GetErrMsg();
			}
			return success;
		}

		/// <summary>
		/// Setup
		/// </summary>
		/// <param name="nameFilter_TextBox"></param>
		/// <param name="recentUsed_ComboBox"></param>
		/// <param name="result_DataGridView"></param>
		/// <param name="makeProcessName_TextBox"></param>
		/// <param name="makeProcess_WFP_TextBox"></param>
		public void Setup(TextBox nameFilter_TextBox,
						ComboBox recentUsed_ComboBox,
						DataGridView result_DataGridView,
						TextBox makeProcessName_TextBox,
						TextBox makeProcess_WFP_TextBox,
						TextBox makeProcess_ISO_TextBox,
						Label filter_Info_Label,
						StageC_ViewModel stageCViewModel,
						TextBox Density_TextBox)
		{
			_nameFilter_TextBox = nameFilter_TextBox;
			_recentUsed_ComboBox = recentUsed_ComboBox;
			_dv = result_DataGridView;
			_makeProcessName_TextBox = makeProcessName_TextBox;
			_makeProcess_WFP_TextBox = makeProcess_WFP_TextBox;
			_makeProcess_ISO_TextBox = makeProcess_ISO_TextBox;

			_filter_Info_Label = filter_Info_Label;
			_Density_TextBox = Density_TextBox;
			_stageCViewModel = stageCViewModel;

			//dataGridView Setup
			dataGridViewSetup();

			//ComboBox Setup
			comboBoxSetup();
		}
		private void comboBoxSetup()
		{
			_recentUsed_ComboBox.DropDownStyle = ComboBoxStyle.DropDownList;//整列選取

			comboBoxDataBinding();
		}

		private void comboBoxDataBinding()
		{
			//data
			_RecentUsedData = new List<RecentUsedDTO>();
			_RecentUsedData.Add(new RecentUsedDTO()
			{
				SN = 0,
				Name = "請選擇"
			});

			//
			var repo = new ManufactureProcedureRepo();
			var records = repo.GetRecentUsedData();
			_RecentUsedData.AddRange(records);

			if (_RecentUsedData.Count < 2)
			{
				_recentUsed_ComboBox.Enabled = false;
			}

			//setup
			_recentUsed_ComboBox.DisplayMember = "Name";
			_recentUsed_ComboBox.ValueMember = "MaterialSN";

			_recentUsed_ComboBox.DataSource = _RecentUsedData;
			_recentUsed_ComboBox.SelectedIndexChanged += _recentUsed_ComboBox_SelectedIndexChanged; ;

		}

		private void _recentUsed_ComboBox_SelectedIndexChanged(object sender, EventArgs e)
		{
			_SelectedSN = 0;
			var idx = (sender as ComboBox).SelectedIndex;
			var item = _RecentUsedData[idx];
			if (item.SN > 0)
			{
				var repo = new ManufactureProcedureRepo();
				var result = repo.GetProcesses(item.SN);
				if (result != null)
				{
					_makeProcessName_TextBox.Text = result.ProcessName;
					_makeProcess_WFP_TextBox.Text = result.WFP;
					_Density_TextBox.Text = result.Density;
					_SelectedSN = result.SN;
				}
			}
			else
			{
				MessageBox.Show("請選擇項目");
			}
		}

		private void dataGridViewSetup()
		{
			//setup
			UIHelper.SetDefaultDataGridViewProperties(_dv);

			//Event
			_dv.CellClick += _dv_CellClick;
			_dv.DataBindingComplete += _dv_DataBindingComplete;

			//data
			var repo = new ManufactureProcedureRepo();
			var allData = repo.GetData();
			var processCodes = _stageCViewModel.GetCurrentComponentProcessCode();
			_RawData = allData.Where(p => processCodes.Contains(p.ProcessTypeCode)).ToList();

			//binding
			dataGridViewDataBinding(_RawData);
		}

		private void dataGridViewDataBinding(List<ManufactureProcedure> records)
		{
			_blist = new BindingList<ManufactureProcedure>(records);
			_bs = new BindingSource() { DataSource = _blist };
			_dv.DataSource = _bs;
			_filter_Info_Label.Text = String.Format("{0} / {1}", records.Count, _RawData.Count);
		}

		private void _dv_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
		{
			//Hide Columns if needs
			var dvColumns = _dv.Columns.Cast<DataGridViewColumn>();
			foreach (var name in _hideColumnNames)
			{
				var col = dvColumns.SingleOrDefault(p => p.Name == name);
				if (col != null)
				{
					col.Visible = false;
				}
			}
		}

		private void _dv_CellClick(object sender, DataGridViewCellEventArgs e)
		{
			_SelectedSN = 0;
			if (e.RowIndex > -1)
			{
				var row = _dv.Rows[e.RowIndex];
				_SelectedSN = Convert.ToInt32(row.Cells["SN"].Value);
				var nameCell = row.Cells["ProcessName"];//.ToString();
				var wfpCell = row.Cells["WFP"];//.ToString();
				var isoCell = row.Cells["ISO"];//.ToString();
				var densityCell = row.Cells["Density"];
				if (nameCell != null && nameCell.Value != null)
				{
					_makeProcessName_TextBox.Text = nameCell.Value.ToString();
				}
				if (wfpCell != null && wfpCell.Value != null)
				{
					_makeProcess_WFP_TextBox.Text = wfpCell.Value.ToString();
				}
				if (isoCell != null && isoCell.Value != null)
				{
					_makeProcess_ISO_TextBox.Text = isoCell.Value.ToString();
				}
				if (densityCell != null && densityCell.Value != null)
				{
					_Density_TextBox.Text = densityCell.Value.ToString();
				}
			}
		}
	}
}
