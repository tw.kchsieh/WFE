﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WFE.Assistants.Helpers;
using WFE.Assistants.Models.ProductCompare;
using WFE.Assistants.Repos.ProductCompare;

namespace WFE.Assistants.Controllers.ProductCompare
{
	public class LoadProductComparisonController
	{
		private DataGridView _dv;
		private TextBox _name_Filter_TextBox;
		private Label _filterInfo_Label;
		private Button _clean_filter_Button;
		private TextBox _ComparisonName_TextBox;
		private RichTextBox _ComparisonDesc_RichTextBox;
		//
		private BindingList<ProductComparisonRecord> _blist;
		private BindingSource _bs;
		private List<ProductComparisonRecord> _RawData;

		public bool Load(MainProdcuctComparisonController mainController, out int sn)
		{
			sn = 0;
			var success = false;
			if (_selectedIdx < 0)
			{
				MessageBox.Show("請選擇項目！");
			}
			else
			{
				var model = _blist[_selectedIdx];
				var msg = String.Format("確定載入「{0}」??", model.ComparisonName);
				if (MessageBox.Show(msg, "警告！！", MessageBoxButtons.YesNo) == DialogResult.Yes)
				{
					var data = _repo.GetDetailData(model.SN);
					success = mainController.Load(data);
					if (success)
						sn = model.SN;
				}
			}
			return success;
		}

		//
		private ProductCompareRepo _repo;
		private int _selectedIdx = -1;
		public LoadProductComparisonController()
		{
			_repo = new ProductCompareRepo();
		}
		public void Setup(DataGridView dv,
			TextBox name_Filter_TextBox,
			Label filterInfo_Label,
			Button clean_filter_Button,
			TextBox ComparisonName_TextBox,
			RichTextBox ComparisonDesc_RichTextBox)
		{
			_dv = dv;
			_name_Filter_TextBox = name_Filter_TextBox;
			_filterInfo_Label = filterInfo_Label;
			_clean_filter_Button = clean_filter_Button;
			_ComparisonName_TextBox = ComparisonName_TextBox;
			_ComparisonDesc_RichTextBox = ComparisonDesc_RichTextBox;

			initial();
		}
		public bool Delete()
		{
			var success = true;
			if (_selectedIdx > -1)
			{
				var model = _blist[_selectedIdx];
				var msg = String.Format("確定刪除「{0}」??", model.ComparisonName);
				if (MessageBox.Show(msg, "警告！！", MessageBoxButtons.YesNo) == DialogResult.Yes)
				{
					success = _repo.DeleteProductComparisonItem(model.SN);
					if (!success)
					{
						MessageBox.Show("刪除失敗 : " + _repo.GetErrMsg());
					}
					_blist.RemoveAt(_selectedIdx);
					if (_blist.Count == 0) _selectedIdx = -1;
					if (_blist.Count == 1) _selectedIdx = 0;
					updateUI();
				}
			}
			else
			{
				MessageBox.Show("請選擇項目！");
				success = false;
			}
			return success;
		}
		private void initial()
		{
			//
			_name_Filter_TextBox.TextChanged += _name_Filter_TextBox_TextChanged;
			_clean_filter_Button.Click += _clean_filter_Button_Click;

			//assingn
			_RawData = _repo.GetComparisonRecord();

			//
			dataGridViewSetup();
		}
		private void dataGridViewSetup()
		{
			//setup
			UIHelper.SetDefaultDataGridViewProperties(_dv);

			//event
			_dv.CellClick += _dv_CellClick;//選取事件
			_dv.DataBindingComplete += _dv_DataBindingComplete; //hide Columns

			//bind data
			dataGridViewDataBinding(_RawData);
		}
		private void _dv_CellClick(object sender, DataGridViewCellEventArgs e)
		{
			_selectedIdx = e.RowIndex;
			updateUI();
		}
		private void updateUI()
		{
			if (_selectedIdx > -1)
			{
				var model = _blist[_selectedIdx];
				_ComparisonName_TextBox.Text = model.ComparisonName;
				_ComparisonDesc_RichTextBox.Text = model.ComparisonDesc;
			}
			else
			{
				_ComparisonName_TextBox.Text = "";
				_ComparisonDesc_RichTextBox.Text = "";
			}

		}
		private void _dv_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
		{
			//var dv = sender as DataGridView;
			//var columns = dv.Columns;
		}
		private void _clean_filter_Button_Click(object sender, EventArgs e)
		{
			_name_Filter_TextBox.Text = "";
		}

		private void _name_Filter_TextBox_TextChanged(object sender, EventArgs e)
		{
			var filter = (sender as TextBox).Text.Trim();
			dofilter(filter);
		}
		private void dofilter(string filter)
		{
			if (String.IsNullOrEmpty(filter))
			{
				cleanFilter();
			}
			else
			{
				var filterResult = _RawData.Where(p => p.ComparisonName.Contains(filter)).ToList();
				dataGridViewDataBinding(filterResult);
			}
		}
		private void cleanFilter()
		{
			dataGridViewDataBinding(_RawData);
		}
		private void dataGridViewDataBinding(List<ProductComparisonRecord> records)
		{
			//
			_blist = new BindingList<ProductComparisonRecord>(records);
			_bs = new BindingSource() { DataSource = _blist };
			_dv.DataSource = _bs;
			_filterInfo_Label.Text = String.Format("{0} / {1}", records.Count, _RawData.Count);
		}
	}
}
