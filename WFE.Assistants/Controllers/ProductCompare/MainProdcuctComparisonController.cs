﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WFE.Assistants.Helpers;
using WFE.Assistants.Models.ProductCompare;
using WFE.Assistants.Repos;
using WFE.Assistants.Repos.ProductCompare;

namespace WFE.Assistants.Controllers.ProductCompare
{
	public class MainProdcuctComparisonController
	{
		private DataGridView _dv;
		private TextBox _productName_TextBox;
		private TextBox _WFP_TextBox;
		private TextBox _ISO_TextBox;
		private RadioButton[] _c2Arr;
		private FlowLayoutPanel[] _c2PanelArr;
		private FlowLayoutPanel _importancePanel;
		private TrackBar[] _c3TrackBarArr;
		private TextBox[] _c3TextBoxArr;

		//
		private ProductCompareRepo _repo;

		private ProductRepo _prodRepo;
		//
		private BindingList<ProductComparisonDetail> _blist;
		private BindingSource _bs;
		private List<ProductComparisonDetail> _RawData;

		//
		private string[] _showColumns;
		//
		private int _selectedRowIndex = -1;

		public MainProdcuctComparisonController()
		{
			_repo = new ProductCompareRepo();
			_prodRepo = new ProductRepo();
			_RawData = new List<ProductComparisonDetail>();
			//
			_showColumns = new string[] { "ProductName", "ProductDesc", "WFP", "ISO" };
		}

		public bool Load(List<ProductComparisonDetail> data)
		{
			var success = true;
			CreateEmpty();
			_RawData.AddRange(data);
			dataGridViewDataBinding(_RawData);
			return success;
		}

		public void Setup(DataGridView dv, TextBox[] pInfoArr
			, RadioButton[] c2Arr
			, FlowLayoutPanel[] c2PanelArr
			, FlowLayoutPanel importancePanel
			, TrackBar[] c3TrackBarArr
			, TextBox[] c3TextBoxArr)
		{
			_dv = dv;
			//
			_productName_TextBox = pInfoArr[0];
			_WFP_TextBox = pInfoArr[1];
			_ISO_TextBox = pInfoArr[2];
			//
			_c2Arr = c2Arr;
			_c2PanelArr = c2PanelArr;
			_importancePanel = importancePanel;
			_c3TrackBarArr = c3TrackBarArr;
			_c3TextBoxArr = c3TextBoxArr;

			//
			initial();
		}


		/// <summary>
		/// 建立新的「產品分析比較」
		/// <para>清除資料</para>
		/// </summary>
		/// <returns></returns>
		public bool CreateEmpty()
		{
			var success = true;
			RemoveAllProduct();
			return success;
		}
		public ProductComparison GetProductComparison(int sn)
		{
			var model = _repo.GetItem(sn);
			return model;
		}

		public int SaveProductComparison(ProductComparison model, out string message, out bool success)
		{
			success = true;
			message = "";
			var productComparisonSN = _repo.SaveData(_RawData, model, out success);
			if (!success)
			{
				message = _repo.GetErrMsg();
			}
			else
			{
				foreach (var item in _RawData)
				{
					item.ProductComparisonSN = productComparisonSN;
				}
			}
			return productComparisonSN;
		}

		public List<ProductComparisonDetail> GetCompareProducts()
		{
			return _RawData;
		}
		public int[] GetCurrentProductSNs()
		{
			int[] result = null;
			if (_blist != null)
			{
				result = _blist.Select(p => p.ProductSN).ToArray();
			}
			return result;
		}
		public ProductComparisonDetail GetSelectedProduct()
		{
			ProductComparisonDetail result = null;
			if (_selectedRowIndex > -1 && _blist.Count > 0)
			{
				result = _blist[_selectedRowIndex];
			}
			return result;
		}

		/// <summary>
		/// 新增要比較的產品
		/// </summary>
		/// <param name="new_productSNs"></param>
		public void AddProducts(string new_productSNs)
		{
			//
			if (String.IsNullOrEmpty(new_productSNs)) return;
			var isFirst = _RawData.Count == 0;
			var productSNs = new_productSNs.Split(',').Select(p => Convert.ToInt32(p)).ToArray();
			foreach (var psn in productSNs)
			{
				var model = _prodRepo.GetProduct(psn);
				var record = new ProductComparisonDetail(model);
				_RawData.Add(record);
				dataGridViewDataBinding(_RawData);
			}
			if (isFirst)
			{
				focusOnFirstRow();
			}
		}
		public void RemoveProduct()
		{
			_blist.RemoveAt(_selectedRowIndex);
			if (_blist.Count == 1)
			{
				_selectedRowIndex = 0;
			}
			if (_blist.Count == 0)
			{
				_selectedRowIndex = -1;
				updateUI();
			}

			if (_selectedRowIndex > -1)
			{
				var model = _blist[_selectedRowIndex];
				updateUI(model);
			}
		}
		public bool RemoveAllProduct()
		{
			_blist.Clear();
			_selectedRowIndex = -1;
			updateUI();
			return true;
		}
		#region private methods
		#endregion
		private void initial()
		{
			dataGridViewSetup();

			//events
			bindPerformanceTypeRadioEvent();
			bindPerformanceRadioEvent();
			bindImportanceRadioEvent();
			bindWeightEvent();

			//default select
			_c2Arr[0].Checked = true; //default
			_c2PanelArr[1].Visible = false;
			if (_RawData != null && _RawData.Count > 0)
			{
				focusOnFirstRow();
			}
		}



		private void focusOnFirstRow()
		{
			_selectedRowIndex = 0;
			_dv.Rows[_selectedRowIndex].Selected = true;
			var model = _blist[_selectedRowIndex];
			updateUI(model);
		}

		private void bindWeightEvent()
		{
			foreach (var trackbar in _c3TrackBarArr)
			{
				trackbar.Scroll += Trackbar_Scroll;
			}
			foreach (var tb in _c3TextBoxArr)
			{
				tb.TextChanged += Tb_TextChanged;
			}
		}

		private void Tb_TextChanged(object sender, EventArgs e)
		{
			var tb = sender as TextBox;
			var code = tb.Tag.ToString();
			var value = UIHelper.ToPercentValue(tb.Text.Trim());
			var tkbar = _c3TrackBarArr.SingleOrDefault(p => p.Tag.ToString() == code);
			if (tkbar != null)
			{
				tkbar.Value = value;
			}
		}

		private void Trackbar_Scroll(object sender, EventArgs e)
		{
			var tkbar = sender as TrackBar;
			var code = tkbar.Tag.ToString();
			var value = UIHelper.ToPercentFixedPointText(tkbar.Value);
			var tb = _c3TextBoxArr.SingleOrDefault(p => p.Tag.ToString() == code);
			if (tb != null)
			{
				tb.Text = value;
			}
			//update W
			setWeightValue(code, value);
		}
		private void setWeightValue(string code, string value)
		{
			if (_selectedRowIndex > -1)
			{
				var model = _blist[_selectedRowIndex];
				double dValue;
				if (Double.TryParse(value, out dValue))
				{
					switch (code)
					{
						case "W1":
							model.W1 = dValue;
							break;
						case "W2":
							model.W2 = dValue;
							break;
						case "W3":
							model.W3 = dValue;
							break;
					}
				}
			}

		}

		private void bindImportanceRadioEvent()
		{
			foreach (var rb in _importancePanel.Controls.OfType<RadioButton>())
			{
				rb.CheckedChanged += Importance_Rb_CheckedChanged;
			}
		}

		private void Importance_Rb_CheckedChanged(object sender, EventArgs e)
		{
			var rb = sender as RadioButton;
			var value = rb.Tag.ToString();
			if (rb.Checked)
			{
				//Set Value
				if (_selectedRowIndex > -1)
				{
					var model = _blist[_selectedRowIndex];
					double dValue;
					if (Double.TryParse(value, out dValue))
					{
						model.Importance = dValue;
					}
				}
			}
		}

		private void bindPerformanceRadioEvent()
		{
			foreach (var c2Panel in _c2PanelArr)
			{
				var gbs = c2Panel.Controls.OfType<GroupBox>();
				foreach (var gb in gbs)
				{
					var p_container = gb.Controls.OfType<FlowLayoutPanel>().FirstOrDefault();
					if (p_container != null)
					{
						var radios = p_container.Controls.OfType<RadioButton>();
						foreach (var rb in radios)
						{
							rb.CheckedChanged += Rb_CheckedChanged;
						}
					}
				}
			}
		}

		private void Rb_CheckedChanged(object sender, EventArgs e)
		{
			var rb = sender as RadioButton;
			var value = rb.Tag.ToString();
			if (rb.Checked)
			{
				//Set Value
				var code = rb.Name.Split('_')[0].ToUpper();
				setPerformanceValue(code, value);
			}
		}
		/// <summary>
		/// 
		/// </summary>
		/// <param name="code">P1, P2, P3</param>
		/// <param name="value"></param>
		private void setPerformanceValue(string code, string value)
		{
			if (_selectedRowIndex > -1)
			{
				var model = _blist[_selectedRowIndex];
				if (model != null)
				{
					double dValue;
					if (Double.TryParse(value, out dValue))
					{
						switch (code)
						{
							case "P1":
								model.P1 = dValue;
								break;
							case "P2":
								model.P2 = dValue;
								break;
							case "P3":
								model.P3 = dValue;
								break;
						}
					}

				}
			}
		}

		private void bindPerformanceTypeRadioEvent()
		{
			_c2Arr[0].CheckedChanged += MainProdcuctCompareController_CheckedChanged;
			_c2Arr[1].CheckedChanged += MainProdcuctCompareController_CheckedChanged;
		}

		private void MainProdcuctCompareController_CheckedChanged(object sender, EventArgs e)
		{
			var rb = sender as RadioButton;
			var c2TypeCode = rb.Tag.ToString();
			foreach (var item in _c2PanelArr)
			{
				item.Visible = item.Tag.ToString() == c2TypeCode;
			}
			if (rb.Checked)
			{
				setPerformanceType(c2TypeCode);
			}
		}

		private void dataGridViewSetup()
		{
			//setup
			UIHelper.SetDefaultDataGridViewProperties(_dv);

			//event
			_dv.CellClick += _dv_CellClick;//選取事件
			_dv.DataBindingComplete += _dv_DataBindingComplete; //hide Columns

			//bind data
			dataGridViewDataBinding(_RawData);
		}
		private void _dv_CellClick(object sender, DataGridViewCellEventArgs e)
		{
			_selectedRowIndex = e.RowIndex;
			//載入 Product Info
			if (_selectedRowIndex > -1)
			{
				var model = _blist[_selectedRowIndex];
				updateUI(model);
			}
		}
		private void updateUI()
		{
			_productName_TextBox.Text = "";
			_ISO_TextBox.Text = "";
			_WFP_TextBox.Text = "";
		}
		private void updateUI(ProductComparisonDetail model)
		{
			if (model != null)
			{
				//Product Info
				_productName_TextBox.Text = model.ProductName;
				_ISO_TextBox.Text = model.ISO.ToString();
				_WFP_TextBox.Text = model.WFP.ToString();

				//Performance
				var c2Radiobtn = _c2Arr.SingleOrDefault(p => p.Tag.ToString() == model.PerformanceType);
				if (c2Radiobtn != null)
				{
					c2Radiobtn.Checked = true;
					var c2Code = c2Radiobtn.Tag.ToString();
					var c2Panel = _c2PanelArr.SingleOrDefault(p => p.Tag.ToString() == c2Code);
					if (c2Panel != null)
					{
						setPerformanceCondtionRadioButton(c2Panel, model);
					}
				}

				//Importance
				var importanceRB = _importancePanel.Controls.OfType<RadioButton>().SingleOrDefault(p => p.Tag.ToString() == model.Importance.ToString());
				if (importanceRB != null)
				{
					importanceRB.Checked = true;
				}

				//trackbar
				foreach (var item in _c3TrackBarArr)
				{
					var code = item.Tag.ToString();
					double modelValue = 1.0;
					switch (code)
					{
						case "W1":
							modelValue = model.W1;
							break;
						case "W2":
							modelValue = model.W2;
							break;
						case "W3":
							modelValue = model.W3;
							break;
					}
					item.Value = (int)(modelValue * 100);
					var c3tb = _c3TextBoxArr.SingleOrDefault(p => p.Tag.ToString() == code);
					if (c3tb != null)
					{
						c3tb.Text = UIHelper.ToPercentFixedPointText(item.Value);
					}
				}
			}
		}
		private void setPerformanceCondtionRadioButton(FlowLayoutPanel c2Panel, ProductComparisonDetail model)
		{
			//Set value
			var c2GBs = c2Panel.Controls.OfType<GroupBox>();
			foreach (var c2gb in c2GBs)
			{
				var container = c2gb.Controls.OfType<FlowLayoutPanel>().FirstOrDefault();
				if (container != null)
				{
					var value = "";
					switch (container.Tag.ToString())
					{
						case "P1":
							value = model.P1.ToString();
							break;
						case "P2":
							value = model.P2.ToString();
							break;
						case "P3":
							value = model.P3.ToString();
							break;
					}
					var ck = container.Controls.OfType<RadioButton>().SingleOrDefault(p => p.Tag.ToString() == value);
					if (ck != null)
					{
						ck.Checked = true;
					}
				}
			}
		}
		private void dataGridViewDataBinding(List<ProductComparisonDetail> records)
		{
			//
			_blist = new BindingList<ProductComparisonDetail>(records);
			_bs = new BindingSource() { DataSource = _blist };
			_dv.DataSource = _bs;
		}
		private void _dv_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
		{
			//show / hide cloumns
			var dv = sender as DataGridView;
			var cloumns = dv.Columns;
			foreach (DataGridViewColumn col in cloumns)
			{
				col.Visible = (_showColumns.Contains(col.Name));
			}
		}
		private void setPerformanceType(string code)
		{
			if (_selectedRowIndex > -1)
			{
				var model = _blist[_selectedRowIndex];
				model.PerformanceType = code;
				updateUI(model);
			}
		}

	}
}
