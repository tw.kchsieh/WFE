﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WFE.Assistants.Models.ProductCompare;
using WFE.Assistants.Repos.ProductCompare;

namespace WFE.Assistants.Controllers.ProductCompare
{
	public class SaveComparisonController
	{
		private TextBox _comparisonName_TextBox;
		private RichTextBox _comparisonDesc_RichTextBox;
		private int _comparisonSN;
		//
		private ProductCompareRepo _repo;
		public SaveComparisonController()
		{
			_repo = new ProductCompareRepo();
		}
		public void Setup(TextBox comparisonName_TextBox, RichTextBox comparisonDesc_RichTextBox, int comparisonSN)
		{
			_comparisonName_TextBox = comparisonName_TextBox;
			_comparisonDesc_RichTextBox = comparisonDesc_RichTextBox;
			_comparisonSN = comparisonSN;
			initial();
		}
		private void initial()
		{
			if (_comparisonSN > 0)
			{
				var model = _repo.GetItem(_comparisonSN);
				updateUI(model);
			}
		}
		private void updateUI(ProductComparison model)
		{
			if (model != null)
			{
				_comparisonName_TextBox.Text = model.ComparisonName;
				_comparisonDesc_RichTextBox.Text = model.ComparisonDesc;
				_comparisonSN = model.SN;
			}
		}
	}
}
