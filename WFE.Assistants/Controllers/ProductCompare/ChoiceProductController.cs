﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WFE.Assistants.Helpers;
using WFE.Assistants.Models;
using WFE.Assistants.Models.ProductCompare;
using WFE.Assistants.Repos;

namespace WFE.Assistants.Controllers.ProductCompare
{
	public class ChoiceProductController
	{
		//
		private DataGridView _dv;
		private TextBox _name_Filter_TextBox;
		private Label _filterInfo_Label;
		private Button _clean_filter_Button;
		private Button _choice_all_Button;
		private Button _clean_choice_Button;
		private int[] _chooseProductSNs;
		private List<int> _nowChoiceArr;
		//
		private int _selectedRowIndex = -1;

		//
		private BindingList<ProductRecord> _blist;
		private BindingSource _bs;
		private List<ProductRecord> _RawData;

		//
		private ProductRepo _repo;
		private ProductViewModel _choiceProduct;

		public ChoiceProductController()
		{
			_nowChoiceArr = new List<int>();
		}
		public string GetModel()
		{
			var result = "";
			var rows = _dv.Rows.Cast<DataGridViewRow>()
							.Where(p => p.Cells["Choice"].FormattedValue.ToString() == "True")
							.Select(p => p.Cells["SN"].Value.ToString())
							.ToArray();
			if (rows != null && rows.Count() > 0)
			{
				result = String.Join(",", rows);
			}
			return result;
		}

		public void Setup(DataGridView dv, TextBox name_Filter_TextBox, Label filterInfo_Label, Button[] funcButtons, int[] chooseProductSNs)
		{
			_dv = dv;
			_name_Filter_TextBox = name_Filter_TextBox;
			_filterInfo_Label = filterInfo_Label;
			_clean_filter_Button = funcButtons[0];
			_choice_all_Button = funcButtons[1];
			_clean_choice_Button = funcButtons[2];
			_chooseProductSNs = chooseProductSNs;

			initial();
		}
		private void initial()
		{
			//
			_name_Filter_TextBox.TextChanged += _name_Filter_TextBox_TextChanged;

			//
			_clean_filter_Button.Click += _clean_filter_Button_Click;

			//
			_choice_all_Button.Click += _choice_all_Button_Click;

			//
			_clean_choice_Button.Click += _clean_choice_Button_Click;

			//assing
			_choiceProduct = new ProductViewModel();
			_repo = new ProductRepo();
			_RawData = _repo.GetUnChoicedProduct(_chooseProductSNs) as List<ProductRecord>;

			//
			dataGridViewSetup();
		}

		private void _clean_choice_Button_Click(object sender, EventArgs e)
		{
			foreach (DataGridViewRow row in _dv.Rows)
			{
				row.Cells["Choice"].Value = false;
			}
		}

		private void _choice_all_Button_Click(object sender, EventArgs e)
		{
			foreach (DataGridViewRow row in _dv.Rows)
			{
				row.Cells["Choice"].Value = true;
			}
		}

		private void _clean_filter_Button_Click(object sender, EventArgs e)
		{
			_name_Filter_TextBox.Text = "";
		}

		private void _name_Filter_TextBox_TextChanged(object sender, EventArgs e)
		{
			var filter = (sender as TextBox).Text.Trim();
			dofilter(filter);
		}
		private void dofilter(string filter)
		{
			if (String.IsNullOrEmpty(filter))
			{
				cleanFilter();
			}
			else
			{
				var filterResult = _RawData.Where(p => p.ProductName.Contains(filter)).ToList();
				dataGridViewDataBinding(filterResult);
			}
		}
		private void cleanFilter()
		{
			dataGridViewDataBinding(_RawData);
		}
		private void dataGridViewDataBinding(List<ProductRecord> records)
		{
			//
			_blist = new BindingList<ProductRecord>(records);
			_bs = new BindingSource() { DataSource = _blist };
			_dv.DataSource = _bs;
			_filterInfo_Label.Text = String.Format("{0} / {1}", records.Count, _RawData.Count);
		}
		private void dataGridViewSetup()
		{
			//setup
			UIHelper.SetDefaultDataGridViewProperties(_dv);
			_dv.ReadOnly = false;

			//event
			_dv.CellClick += _dv_CellClick;//選取事件
			_dv.DataBindingComplete += _dv_DataBindingComplete; //hide Columns

			//bind data
			_dv.Columns.Add(new DataGridViewCheckBoxColumn() { Name = "Choice", HeaderText = "選擇", ReadOnly = false, Width = 10 });
			dataGridViewDataBinding(_RawData);
			_dv.Columns["Choice"].Width = 50;
			_dv.Columns["SN"].Width = 50;
		}
		private void _dv_CellClick(object sender, DataGridViewCellEventArgs e)
		{

		}
		private void _dv_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
		{
			var dv = sender as DataGridView;
			var columns = dv.Columns;
			foreach (DataGridViewColumn col in columns)
			{
				if (col.Name == "Choice") continue;
				col.ReadOnly = true;
			}
		}
		private void refreshProductUI(int productSN)
		{
			//
			_choiceProduct = _repo.GetProduct(productSN);

		}
	}
}
