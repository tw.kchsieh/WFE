﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using WFE.Assistants.Helpers;
using WFE.Assistants.Models.ProductCompare;

namespace WFE.Assistants.Controllers.ProductCompare
{
	public class ProductComparisonEvaluateController
	{
		private DataGridView _dv;
		private Chart _chart;
		private Type _dataType;
		private MainProdcuctComparisonController _mainController;
		private List<ProductComparisonDetail> _rawData;
		private BindingSource _bs;
		private string[] _hideColumns;
		public void Setup<T>(DataGridView dv, Chart chart, MainProdcuctComparisonController mainController)
		{
			_chart = chart;
			_mainController = mainController;
			_dataType = typeof(T);
			_dv = dv;
			initial();
		}
		private void initial()
		{
			//"P1", "P2", "P3", "W1", "W2", "W3", "Importance"
			_hideColumns = new string[] { "SN", "ProductSN", "ProductComparisonSN" };
			_rawData = _mainController.GetCompareProducts();

			//set properties
			UIHelper.SetDefaultDataGridViewProperties(_dv);

			//Event
			_dv.DataBindingComplete += _dv_DataBindingComplete;

			clearChartData();
		}

		private void _dv_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
		{
			var dv = sender as DataGridView;
			var columns = dv.Columns.Cast<DataGridViewColumn>();
			foreach (var hideColName in _hideColumns)
			{
				var col = columns.SingleOrDefault(p => p.Name == hideColName);
				if (col != null)
				{
					col.Visible = false;
				}
			}
		}

		public void UpdateData()
		{
			if (_rawData == null || _rawData.Count == 0) return;

			var n = _rawData.Count;
			var m_wfn = _rawData.Sum(p => p.WFP) / n;
			var m_iso = _rawData.Sum(p => p.ISO) / n;
			var m_WFP = (_dataType.Name == "WFNEvaluateRecord") ? m_wfn : m_iso;
			var m_performance = _rawData.Sum(p => p.GetPerformance()) / n;
			var m_importance = _rawData.Sum(p => p.Importance) / n;

			var records = new List<object>();
			foreach (var item in _rawData)
			{
				var obj = Activator.CreateInstance(_dataType);
				(obj as IEvaluateRecord).Setup(item, m_WFP, m_performance, m_importance);
				records.Add(obj);
			}
			//bind
			_bs = new BindingSource() { DataSource = records };
			_dv.DataSource = _bs;

			//chart
			updateChart(records);
		}

		public void ShowMe()
		{
			//MessageBox.Show(_rawData.Count.ToString());
		}
		private void updateChart(List<object> records)
		{
			var e1_series = _chart.Series[0];
			var e2_series = _chart.Series[1];
			var e3_series = _chart.Series[2];

			//clear chart points
			clearChartData();

			//assing new
			for (var idx = 0; idx < records.Count; idx++)
			{
				var item = records[idx];
				var xPos = idx + 1;
				var model = (item as BaseEvaluateRecord).GetEvaluateChartModel();
				var dp1 = new DataPoint(xPos, model.E1) { AxisLabel = model.ProductName, ToolTip = model.E1.ToString() };
				var dp2 = new DataPoint(xPos, model.E2) { ToolTip = model.E2.ToString() };
				var dp3 = new DataPoint(xPos, model.E3) { ToolTip = model.E3.ToString() };
				e1_series.Points.Add(dp1);
				e2_series.Points.Add(dp2);
				e3_series.Points.Add(dp3);
			}
		}
		private void clearChartData()
		{
			foreach (var ss in _chart.Series)
			{
				ss.Points.Clear();
			}
		}
	}
}
