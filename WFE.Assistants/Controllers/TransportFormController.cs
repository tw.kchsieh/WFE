﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WFE.Assistants.Helpers;
using WFE.Assistants.Models;
using WFE.Assistants.Models.ORDs;
using WFE.Assistants.Repos;

namespace WFE.Assistants.Controllers
{
	public class TransportFormController
	{
		#region Controls
		private TextBox _nameFilter_TextBox;
		private ComboBox _recentUsed_ComboBox;
		private Label _Filter_Info_Label;
		private DataGridView _dv;
		private Label _WFP_Label;
		private Label _ISO_Label;
		private TextBox _selected_Name_TextBox;
		private TextBox _transport_Distance_TextBox;
		private DataGridView _transport_Records_DataGridView;
		private TextBox _transport_WFP_TextBox;
		private TextBox _transport_ISO_TextBox;
		#endregion

		private ProductComponent _component;
		private ChoiceTransportViewModel _choiceTransportDTO; //已選的運輸工具水足跡模型

		/// <summary>
		/// 初始化資料
		/// </summary>
		/// <param name="component"></param>
		public void Initial(ProductComponent component)
		{
			_component = component;

			//setup
			setup();
		}

		//DataGridView : for Transport DataGridView
		private List<Transport> _RawData;
		private BindingSource _bs;// = new BindingSource();
		private BindingList<Transport> _blist;
		private int _SelectedTransportSN = 0;

		//因為允許重複加入運輸工具，因此刪除時以 Row Index 作為判斷
		private int _SelectedTransportRowIdx = -1;

		private string[] _hideColumnNames;

		//DataGridView : for Transport Records
		private string[] _hideRecordsColumnNames;
		private BindingList<ComponentTransport> _Record_blist;
		private BindingSource _Record_bs;

		//ErrMsg
		private string _ErrMsg;

		//Recent Used
		private List<RecentUsedTransport> _RecentUsedItems;

		//=====================================================
		//運輸工具水足跡-記錄

		public TransportFormController(TextBox nameFilter_TextBox, ComboBox recentUsed_ComboBox,
			Label filter_Info_Label,
			DataGridView transport_DataGridView,
			TextBox selected_Name_TextBox, 
			TextBox transport_WFP_TextBox, 
			TextBox transport_ISO_TextBox,
			TextBox transport_Distance_TextBox,
			Label WFP_Label,
			Label ISO_Label,
			DataGridView transport_Records_DataGridView)
		{
			this._nameFilter_TextBox = nameFilter_TextBox;
			this._recentUsed_ComboBox = recentUsed_ComboBox;
			this._Filter_Info_Label = filter_Info_Label;
			this._dv = transport_DataGridView;
			this._WFP_Label = WFP_Label;
			this._ISO_Label = ISO_Label;
			this._selected_Name_TextBox = selected_Name_TextBox;
			this._transport_WFP_TextBox = transport_WFP_TextBox;
			this._transport_ISO_TextBox = transport_ISO_TextBox;

			this._transport_Distance_TextBox = transport_Distance_TextBox;
			this._transport_Records_DataGridView = transport_Records_DataGridView;
		}

		/// <summary>
		/// 刪除運輸記錄
		/// </summary>
		public void DeleteTransportRecord()
		{
			if (_SelectedTransportRowIdx > -1)
			{
				var seq = _SelectedTransportRowIdx + 1;
				var model = _Record_blist[_SelectedTransportRowIdx];
				var confirmMsg = String.Format("確定刪除記錄 {2} :「{0}」 距離 : {1}", model.Name, model.Distance, seq);
				if (MessageBox.Show(confirmMsg, "警告！！", MessageBoxButtons.YesNo) == DialogResult.Yes)
				{
					_Record_blist.RemoveAt(_SelectedTransportRowIdx);
				}
			}
			else
			{
				MessageBox.Show("請選擇欲刪除的記錄！");
			}
		}

		/// <summary>
		/// 加入運輸工具水足跡-記錄
		/// <para>同一原料/元件可使用重複的運輸工具</para>
		/// </summary>
		public bool AddTransportRecord()
		{
			var success = false;
			if (_SelectedTransportSN > 0)
			{
				var model = new ComponentTransport()
				{
					C_SN = _choiceTransportDTO.C_SN,
					C_Name = _choiceTransportDTO.C_Name,
					//
					TransportSN = _SelectedTransportSN,
					Name = _choiceTransportDTO.Name,
					Distance = _choiceTransportDTO.Distance,
					TransportWFP = _choiceTransportDTO.TransportWFP,
					TransportISO = _choiceTransportDTO.TransportISO,
					Component_Mass = _choiceTransportDTO.Mass,
				};
				_Record_blist.Add(model);
				success = true;
			}
			return success;
		}


		/// <summary>
		/// 儲存最近使用運輸工具
		/// </summary>
		/// <returns></returns>
		public bool SaveRecentUsedItem()
		{
			var repo = new TransportRepo();
			var success = repo.UpdateRecentUsedItem(_SelectedTransportSN);
			if (!success)
			{
				_ErrMsg = repo.GetErrMsg();
			}
			return success;
		}

		/// <summary>
		/// 取得錯誤訊息
		/// </summary>
		/// <returns></returns>
		public string GetErrMsg()
		{
			return _ErrMsg;
		}

		private void setup()
		{
			//init variables
			_choiceTransportDTO = new ChoiceTransportViewModel()
			{
				C_SN = _component.Component_SN,
				C_Name = _component.Component_Name,
				Mass = _component.Mass,
			};

			//
			_hideColumnNames = new string[] {"SN", "CalcUnit", "UnitOne",
				//"BlueWater", "GreenWater", "GrayWater",
				"MaxLoad",
				"EnergyPerUnit", "ConsumptionOfEnergy", "ConsumptionCoefficient" };

			//Filter
			_nameFilter_TextBox.TextChanged += _nameFilter_TextBox_TextChanged;

			//transportDataGridView
			dataGridViewSetup();

			//recent used combobox
			recentComboBoxSetup();

			//Transport distance
			_transport_WFP_TextBox.TextChanged += _transport_WFP_TextBox_TextChanged;
			_transport_Distance_TextBox.TextChanged += _transport_Distance_TextBox_TextChanged;

			//Transport Records DataGridView
			dataGridViewRecordSetup();
		}

		private void dataGridViewRecordSetup()
		{
			//
			UIHelper.SetDefaultDataGridViewProperties(_transport_Records_DataGridView);
			//
			_hideRecordsColumnNames = new string[] {
				"SN",
				"C_SN", "TransportSN",
				"Weight",
				"Consumption", "ConsumptionCOEEF", };

			//
			_transport_Records_DataGridView.DataBindingComplete += transport_Records_DataGridView_DataBindingComplete;
			_transport_Records_DataGridView.CellClick += _transport_Records_DataGridView_CellClick;

			//binding
			var records = _component.ComponentTransports;
			_Record_blist = new BindingList<ComponentTransport>(records);
			_Record_bs = new BindingSource() { DataSource = _Record_blist };
			_transport_Records_DataGridView.DataSource = _Record_bs;
		}

		private void _transport_Records_DataGridView_CellClick(object sender, DataGridViewCellEventArgs e)
		{
			_SelectedTransportRowIdx = e.RowIndex;
		}

		private void transport_Records_DataGridView_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
		{
			var dvColumns = _transport_Records_DataGridView.Columns.Cast<DataGridViewColumn>();
			foreach (var name in _hideRecordsColumnNames)
			{
				var col = dvColumns.SingleOrDefault(p => p.Name == name);
				if (col != null)
				{
					col.Visible = false;
				}
			}
		}

		private void _transport_WFP_TextBox_TextChanged(object sender, EventArgs e)
		{
			_choiceTransportDTO.TransportWFP = (sender as TextBox).Text;
			updateUI();
		}

		private void _transport_Distance_TextBox_TextChanged(object sender, EventArgs e)
		{
			_choiceTransportDTO.Distance = (sender as TextBox).Text;
			updateUI();
		}

		/// <summary>
		/// 變更運輸工具 or 距離時，更新資料
		/// </summary>
		private void updateUI()
		{
			_selected_Name_TextBox.Text = _choiceTransportDTO.Name;
			_transport_WFP_TextBox.Text = _choiceTransportDTO.TransportWFP;
			_transport_ISO_TextBox.Text = _choiceTransportDTO.TransportISO;

			_WFP_Label.Text = _choiceTransportDTO.WFP;
			_ISO_Label.Text = _choiceTransportDTO.ISO;
		}

		private void recentComboBoxSetup()
		{
			_recentUsed_ComboBox.DropDownStyle = ComboBoxStyle.DropDownList;//整列選取

			//databinding
			recentComboBoxDataBinding();

			//event
			_recentUsed_ComboBox.SelectedIndexChanged += recentUsed_ComboBox_SelectedIndexChanged;
		}

		private void recentComboBoxDataBinding()
		{
			//data
			var repo = new TransportRepo();
			_RecentUsedItems = new List<RecentUsedTransport>();
			_RecentUsedItems.Add(new RecentUsedTransport()
			{
				TransportSN = 0,
				Name = "請選擇"
			});
			var records = repo.GetRecentUsedData() as List<RecentUsedTransport>;
			_RecentUsedItems.AddRange(records);

			if (_RecentUsedItems.Count < 2)
			{
				_recentUsed_ComboBox.Enabled = false;
			}

			//setup
			_recentUsed_ComboBox.DisplayMember = "Name";
			_recentUsed_ComboBox.ValueMember = "TransportSN";
			_recentUsed_ComboBox.DataSource = _RecentUsedItems;
		}

		private void recentUsed_ComboBox_SelectedIndexChanged(object sender, EventArgs e)
		{
			_SelectedTransportSN = 0;
			var idx = (sender as ComboBox).SelectedIndex;
			var item = _RecentUsedItems[idx];
			if (item.TransportSN > 0)
			{
				var repo = new TransportRepo();
				var transportDTO = repo.GetItem(item.TransportSN) as Transport;
				if (transportDTO != null)
				{
					_SelectedTransportSN = transportDTO.SN;
					_choiceTransportDTO.SN = transportDTO.SN;
					_choiceTransportDTO.Name = transportDTO.TransportName;
					_choiceTransportDTO.TransportWFP = transportDTO.WFP;
					_choiceTransportDTO.TransportISO = transportDTO.ISO;

					//update UI
					updateUI();

					//focus
					_transport_Distance_TextBox.Focus();
				}
			}
			else
			{
				MessageBox.Show("請選擇項目");
			}
		}

		private void dataGridViewSetup()
		{
			//setup
			UIHelper.SetDefaultDataGridViewProperties(_dv);

			//event
			_dv.CellClick += _dv_CellClick;//選取事件
			_dv.DataBindingComplete += _dv_DataBindingComplete;

			//init Data
			var repo = new TransportRepo();
			_RawData = repo.GetData() as List<Transport>;

			//bind data
			dataGridViewDataBinding(_RawData);
		}

		private void _dv_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
		{
			//
			var dvColumns = _dv.Columns.Cast<DataGridViewColumn>();
			foreach (var name in _hideColumnNames)
			{
				var col = dvColumns.SingleOrDefault(p => p.Name == name);
				if (col != null)
				{
					col.Visible = false;
				}
			}
		}

		private void _dv_CellClick(object sender, DataGridViewCellEventArgs e)
		{
			_SelectedTransportSN = 0;
			if (e.RowIndex > -1)
			{
				var row = _dv.Rows[e.RowIndex];
				_SelectedTransportSN = Convert.ToInt32(row.Cells["SN"].Value);
				var nameCell = row.Cells["TransportName"];
				var wfpCell = row.Cells["WFP"];
				var isoCell = row.Cells["ISO"];
				if (nameCell != null && nameCell.Value != null)
				{
					_choiceTransportDTO.Name = nameCell.Value.ToString();
				}
				if (wfpCell != null && wfpCell.Value != null)
				{
					_choiceTransportDTO.TransportWFP = wfpCell.Value.ToString();
				}
				if (isoCell != null && isoCell.Value != null)
				{
					_choiceTransportDTO.TransportISO = isoCell.Value.ToString();
				}
				updateUI();
				//focus
				_transport_Distance_TextBox.Focus();
			}
		}

		private void _nameFilter_TextBox_TextChanged(object sender, EventArgs e)
		{
			dofilter(_nameFilter_TextBox.Text.Trim());
		}

		private void dofilter(string filter)
		{
			if (String.IsNullOrEmpty(filter))
			{
				cleanFilter();
			}
			else
			{
				var filterResult = _RawData.Where(p => p.TransportName.Contains(filter)).ToList();
				dataGridViewDataBinding(filterResult);
			}
		}
		private void cleanFilter()
		{
			dataGridViewDataBinding(_RawData);
		}

		private void dataGridViewDataBinding(List<Transport> records)
		{
			_blist = new BindingList<Transport>(records);
			_bs = new BindingSource() { DataSource = _blist };
			_dv.DataSource = _bs;
			_Filter_Info_Label.Text = String.Format("{0} / {1}", records.Count, _RawData.Count);
		}
	}
}
