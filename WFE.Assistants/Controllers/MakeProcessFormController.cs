﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WFE.Assistants.Helpers;
using WFE.Assistants.Models;
using WFE.Assistants.Models.ORDs;
using WFE.Assistants.Repos;

namespace WFE.Assistants.Controllers
{
	public class MakeProcessFormController
	{
		//Controls
		private TextBox _name_Filter_TextBox;
		private Label _Filter_Info_Label;
		private DataGridView _dv;
		private TextBox _process_Name_TextBox;
		private TextBox _process_Power_Consumption_TextBox;
		private TextBox _power_COEEF_TextBox;
		private TextBox _process_WFP_TextBox;
		private ComboBox _recent_Used_ComboBox;
		private TextBox _WorkingHour_TextBox;
		private Label _wFP_Label;
		private Label _ISO_Label;
		private DataGridView _record_DataGridView;
		private FlowLayoutPanel _MassPanel;
		private FlowLayoutPanel _PowerPanel;

		//
		private ProductComponent _component;
		private ChoiceMakeProcessViewModel _choiceDTO; //已選工具模型



		//DataGridView : for Transport DataGridView
		private List<MakeProcess> _RawData;
		private BindingSource _bs;// = new BindingSource();
		private BindingList<MakeProcess> _blist;
		private int _SelectedItemSN = 0;

		//因為允許重複加入，因此刪除時以 Row Index 作為判斷
		private int _SelectedRecordRowIdx = -1;

		private string[] _showColumnNames;

		//DataGridView : for Records
		private string[] _hideRecordsColumnNames;
		private BindingList<ComponentProcess> _Record_blist;
		private BindingSource _Record_bs;

		/// <summary>
		/// 加入最近使用的項目
		/// </summary>
		/// <returns></returns>
		public bool SaveRecentUsedItem()
		{
			var repo = new MakeProcessRepo();
			var success = repo.UpdateRecentUsedItem(_SelectedItemSN);
			if (!success)
			{
				_ErrMsg = repo.GetErrMsg();
			}
			return success;
		}

		public string GetErrMsg()
		{
			return _ErrMsg;
		}

		//ErrMsg
		private string _ErrMsg;

		/// <summary>
		/// 新增記錄
		/// </summary>
		/// <returns></returns>
		public bool AddRecord()
		{
			var success = false;
			if (_SelectedItemSN > 0)
			{
				var model = new ComponentProcess()
				{
					C_SN = _choiceDTO.C_SN,
					C_Name = _choiceDTO.C_Name,
					//
					MakeProcessSN = _SelectedItemSN,
					MakeProcessName = _choiceDTO.Name,
					ComponentMass = _choiceDTO.Mass,
					MakeProcessWFP = _choiceDTO.MakeProcessWFP,
					PowerConsumption = _choiceDTO.PowerConsumption,
					PowerCOEEF = _choiceDTO.PowerCOEEF,
					WorkingHour = _choiceDTO.WorkingHour,
					CalculateCode = _choiceDTO.CalculateCode,
				};
				_Record_blist.Add(model);
				success = true;
			}
			return success;
		}

		/// <summary>
		/// 刪除記錄
		/// </summary>
		public void DeleteRecord()
		{
			if (_SelectedRecordRowIdx > -1)
			{
				var seq = _SelectedRecordRowIdx + 1;
				var model = _Record_blist[_SelectedRecordRowIdx];
				var confirmMsg = String.Format("確定刪除記錄 {2} :「{0}」 水足跡 : {1}", model.MakeProcessName, model.WFP, seq);
				if (MessageBox.Show(confirmMsg, "警告！！", MessageBoxButtons.YesNo) == DialogResult.Yes)
				{
					_Record_blist.RemoveAt(_SelectedRecordRowIdx);
				}
			}
			else
			{
				MessageBox.Show("請先選取欲刪除的記錄！");
			}
		}

		//Recent Used
		private List<RecentUsedDTO> _RecentUsedItems;

		public MakeProcessFormController(TextBox name_Filter_TextBox,
		ComboBox recent_Used_ComboBox,
		Label filter_Info_Label,
		DataGridView process_DataGridView,
		TextBox process_Name_TextBox,
		TextBox process_WFP_TextBox,
		TextBox power_COEEF_TextBox,
		TextBox process_Power_Consumption_TextBox,
		TextBox WorkingHour_TextBox,
		Label wFP_Label,
		Label ISO_Label,
		DataGridView record_DataGridView,
		FlowLayoutPanel massPanel,
		FlowLayoutPanel powerPanel)
		{
			this._name_Filter_TextBox = name_Filter_TextBox;
			this._recent_Used_ComboBox = recent_Used_ComboBox;
			this._Filter_Info_Label = filter_Info_Label;
			this._dv = process_DataGridView;
			this._process_Name_TextBox = process_Name_TextBox;
			this._process_WFP_TextBox = process_WFP_TextBox;
			this._power_COEEF_TextBox = power_COEEF_TextBox;
			this._process_Power_Consumption_TextBox = process_Power_Consumption_TextBox;
			this._WorkingHour_TextBox = WorkingHour_TextBox;
			this._wFP_Label = wFP_Label;
			this._ISO_Label = ISO_Label;
			this._record_DataGridView = record_DataGridView;

			this._MassPanel = massPanel;
			this._PowerPanel = powerPanel;
		}

		public void Initial(ProductComponent component)
		{
			_component = component;
			//setup
			setup();
		}
		private void setup()
		{
			_choiceDTO = new ChoiceMakeProcessViewModel()
			{
				C_SN = _component.Component_SN,
				C_Name = _component.Component_Name,
				Mass = _component.Mass,
			};

			//加工方式篩選
			_name_Filter_TextBox.TextChanged += _name_Filter_TextBox_TextChanged;

			//加工方式 dataGridView
			dataGridViewSetup();

			//加工時數
			_WorkingHour_TextBox.TextChanged += _WorkingHour_TextBox_TextChanged;

			//最近使用
			recentComboBoxSetup();

			//加工記錄
			dataGridViewRecordSetup();
		}

		private void dataGridViewRecordSetup()
		{
			UIHelper.SetDefaultDataGridViewProperties(_record_DataGridView);
			//
			_hideRecordsColumnNames = new string[] {
				"SN",
				"C_SN","C_Name",
				"MakeProcessSN",
				"ComponentMass",
				"CalculateCode",
				"PowerCOEEF",
				"MakeProcessWFP",
				"MakeProcessISO"
				 };

			//
			_record_DataGridView.DataBindingComplete += _record_DataGridView_DataBindingComplete;
			_record_DataGridView.CellClick += _record_DataGridView_CellClick;

			//binding
			var records = _component.ComponentProcesses;
			_Record_blist = new BindingList<ComponentProcess>(records);
			_Record_bs = new BindingSource() { DataSource = _Record_blist };
			_record_DataGridView.DataSource = _Record_bs;
		}

		private void _record_DataGridView_CellClick(object sender, DataGridViewCellEventArgs e)
		{
			_SelectedRecordRowIdx = e.RowIndex;
		}

		private void _record_DataGridView_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
		{
			var dv = sender as DataGridView;
			var dvColumns = dv.Columns.Cast<DataGridViewColumn>();
			foreach (var name in _hideRecordsColumnNames)
			{
				var col = dvColumns.SingleOrDefault(p => p.Name == name);
				if (col != null)
				{
					col.Visible = false;
				}
			}
		}

		private void recentComboBoxSetup()
		{
			_recent_Used_ComboBox.DropDownStyle = ComboBoxStyle.DropDownList;//整列選取

			//databinding
			recentComboBoxDataBinding();

			//event
			_recent_Used_ComboBox.SelectedIndexChanged += _recent_Used_ComboBox_SelectedIndexChanged;
		}

		private void _recent_Used_ComboBox_SelectedIndexChanged(object sender, EventArgs e)
		{
			_SelectedItemSN = 0;
			var idx = (sender as ComboBox).SelectedIndex;
			var item = _RecentUsedItems[idx];
			if (item.SN > 0)
			{
				var repo = new MakeProcessRepo();
				var model = repo.GetItem(item.SN) as MakeProcess;
				if (model != null)
				{
					_SelectedItemSN = model.SN;
					_choiceDTO.SN = model.SN;
					_choiceDTO.Name = model.MakeProcessName;
					_choiceDTO.CalculateCode = model.CalculateCode;
					_choiceDTO.MakeProcessWFP = model.WFP;
					_choiceDTO.PowerConsumption = model.PowerConsumption;
					_choiceDTO.PowerCOEEF = model.PowerCOEFF;

					//update UI
					updateUI();

					//focus
					_WorkingHour_TextBox.Focus();
				}
			}
			else
			{
				MessageBox.Show("請選擇項目");
			}
		}

		private void recentComboBoxDataBinding()
		{
			//data
			var repo = new MakeProcessRepo();
			_RecentUsedItems = new List<RecentUsedDTO>();
			_RecentUsedItems.Add(new RecentUsedDTO()
			{
				SN = 0,
				Name = "請選擇"
			});
			var records = repo.GetRecentUsedData() as List<RecentUsedDTO>;
			if (records != null)
			{
				_RecentUsedItems.AddRange(records);
			}

			if (_RecentUsedItems.Count < 2)
			{
				_recent_Used_ComboBox.Enabled = false;
			}

			//setup
			_recent_Used_ComboBox.DisplayMember = "Name";
			_recent_Used_ComboBox.ValueMember = "SN";
			_recent_Used_ComboBox.DataSource = _RecentUsedItems;
		}

		private void _name_Filter_TextBox_TextChanged(object sender, EventArgs e)
		{
			var filterName = (sender as TextBox).Text.Trim();
			if (String.IsNullOrEmpty(filterName))
			{
				//restore
				dataGridViewDataBinding(_RawData);
			}
			else
			{
				var filterResult = _RawData.Where(p => p.MakeProcessName.Contains(filterName)).ToList();
				//bind data
				dataGridViewDataBinding(filterResult);
			}
		}

		private void _WorkingHour_TextBox_TextChanged(object sender, EventArgs e)
		{
			_choiceDTO.WorkingHour = (sender as TextBox).Text.Trim();
			updateUI();
		}

		private void dataGridViewSetup()
		{
			//setup
			UIHelper.SetDefaultDataGridViewProperties(_dv);

			_showColumnNames = new string[] { "MakeProcessName", "PowerConsumption", "PowerCOEFF" };

			//event
			_dv.CellClick += _dv_CellClick;//選取事件
			_dv.DataBindingComplete += _dv_DataBindingComplete;

			//init Data
			var repo = new MakeProcessRepo();
			_RawData = repo.GetData() as List<MakeProcess>;

			//bind data
			dataGridViewDataBinding(_RawData);
		}

		private void dataGridViewDataBinding(List<MakeProcess> records)
		{
			_blist = new BindingList<MakeProcess>(records);
			_bs = new BindingSource() { DataSource = _blist };
			_dv.DataSource = _bs;
			_Filter_Info_Label.Text = String.Format("{0} / {1}", records.Count, _RawData.Count);
		}

		private void _dv_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
		{
			var dvColumns = _dv.Columns.Cast<DataGridViewColumn>();
			foreach (var col in dvColumns)
			{
				col.Visible = _showColumnNames.Contains(col.Name);
			}
		}

		private void _dv_CellClick(object sender, DataGridViewCellEventArgs e)
		{
			_SelectedItemSN = 0;
			if (e.RowIndex > -1)
			{
				var row = _dv.Rows[e.RowIndex];
				_SelectedItemSN = Convert.ToInt32(row.Cells["SN"].Value);

				var calculateCodeCell = row.Cells["CalculateCode"];
				var nameCell = row.Cells["MakeProcessName"];
				var wfpCell = row.Cells["WFP"];
				var isoCell = row.Cells["ISO"];
				var powerConsumptionCell = row.Cells["PowerConsumption"];
				var powerCOEEFCell = row.Cells["PowerCOEFF"];
				if (nameCell != null && nameCell.Value != null)
				{
					_choiceDTO.Name = nameCell.Value.ToString();
				}
				if (wfpCell != null && wfpCell.Value != null)
				{
					_choiceDTO.MakeProcessWFP = wfpCell.Value.ToString();
				}
				if (isoCell != null && isoCell.Value != null)
				{
					_choiceDTO.MakeProcessISO = isoCell.Value.ToString();
				}
				if (powerConsumptionCell != null && powerConsumptionCell.Value != null)
				{
					_choiceDTO.PowerConsumption = powerConsumptionCell.Value.ToString();
				}
				if (powerCOEEFCell != null && powerCOEEFCell.Value != null)
				{
					_choiceDTO.PowerCOEEF = powerCOEEFCell.Value.ToString();
				}
				if (calculateCodeCell != null && calculateCodeCell.Value != null)
				{
					_choiceDTO.CalculateCode = calculateCodeCell.Value.ToString();
				}
				else
				{
					_choiceDTO.CalculateCode = "WFP";//Default
				}

				updateUI();
				//focus
				_WorkingHour_TextBox.Focus();
			}
		}

		private void updateUI()
		{
			_process_Name_TextBox.Text = _choiceDTO.Name;
			_process_WFP_TextBox.Text = _choiceDTO.MakeProcessWFP;
			_power_COEEF_TextBox.Text = _choiceDTO.PowerCOEEF;
			_process_Power_Consumption_TextBox.Text = _choiceDTO.PowerConsumption;

			//
			_wFP_Label.Text = _choiceDTO.WFP;
			_ISO_Label.Text = _choiceDTO.ISO;

			//Visiable Control
			switchControlVisiable();
		}
		private void switchControlVisiable()
		{
			var powerVisiable = (_choiceDTO.CalculateCode == "Power");
			_PowerPanel.Visible = powerVisiable;
			_MassPanel.Visible = !powerVisiable;
		}
	}
}
