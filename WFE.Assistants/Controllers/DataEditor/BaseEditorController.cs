﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WFE.Assistants.Helpers;
using WFE.Assistants.Models.EventArgs;
using WFE.Assistants.Repos;

namespace WFE.Assistants.Controllers.DataEditor
{
	public class BaseEditorController
	{
		public delegate void RefreshInputsEventHandler(object sender, RefreshInputEventArgs e);

		//For Inputs Data and Function Buttons (Insert, Update, Delete)
		private TextBox[] _inputs;
		private Button[] _buttons;

		//For Filter
		private Label _filterInfoLabel;
		private TextBox _nameFilter;
		private Button _cleanButton;

		//
		private Type _dataType;
		private DataGridView _dv;
		private BindingSource _bs;
		private int _selectedRowIndex = -1;
		private int _TotalRecords = 0;

		private EditDataRepo _repo;
		private string _filterColumnName;
		private string[] _requiredColumnNames;

		//
		public event RefreshInputsEventHandler RefreshInputsCompleted;
		public void Setup(TextBox[] inputs,
						Button[] buttons,

						Button cleanButton,
						Label filterInfoLabel,
						TextBox nameFilter,

						string tableName, string pKeyName, string filterColumnName)
		{
			//assign
			this._inputs = inputs;
			this._buttons = buttons;

			this._cleanButton = cleanButton;
			this._filterInfoLabel = filterInfoLabel;
			this._nameFilter = nameFilter;

			_filterColumnName = filterColumnName;

			_repo = new EditDataRepo(tableName, pKeyName);
			assignControlsTag();

			//binding button Event
			buttons_EventBinding();

			//
			_nameFilter.TextChanged += _nameFilter_TextChanged;
		}

		/// <summary>
		/// 設定必填欄位資訊
		/// </summary>
		/// <param name="requiredColumns"></param>
		public void Setup_RequiredColumnNames(string[] requiredColumns)
		{
			_requiredColumnNames = requiredColumns;
		}

		private string _conditionColName;
		private string _conditionColValue;

		/// <summary>
		/// 設定過濾條件 (Contains condition)
		/// <para>必須在方法 SetupDataGridView(DataGridView dv) 之前設定</para>
		/// </summary>
		/// <param name="conditionColName"></param>
		/// <param name="conditionColValue"></param>
		public void Setup_Data_Condition(string conditionColName, string conditionColValue)
		{
			_conditionColName = conditionColName;
			_conditionColValue = conditionColValue;
			_repo.UseDataCondition(_conditionColName, _conditionColValue);
		}

		private void _nameFilter_TextChanged(object sender, EventArgs e)
		{
			//do Filter, and rebind Data
			var query = (sender as TextBox).Text.Trim();
			List<object> rawData = null;
			if (!String.IsNullOrEmpty(query))
			{
				rawData = _repo.GetData(_filterColumnName, query);
			}
			else
			{
				rawData = _repo.GetData();
			}

			//Rebind
			convertRawDataAndBinding(rawData);
		}

		private void buttons_EventBinding()
		{
			_cleanButton.Click += _cleanButton_Click;

			var insertButton = _buttons.SingleOrDefault(p => p.Tag.ToString() == "Insert");
			if (insertButton != null)
			{
				insertButton.Click += InsertButton_Click;
			}

			var updateButton = _buttons.SingleOrDefault(p => p.Tag.ToString() == "Update");
			if (updateButton != null)
			{
				updateButton.Click += UpdateButton_Click;
			}

			var deleteButton = _buttons.SingleOrDefault(p => p.Tag.ToString() == "Delete");
			if (deleteButton != null)
			{
				deleteButton.Click += DeleteButton_Click;
			}

		}

		private void DeleteButton_Click(object sender, EventArgs e)
		{
			//刪除
			if (_selectedRowIndex > -1)
			{
				var row = _dv.Rows[_selectedRowIndex];
				var pKeySN = row.Cells["SN"].Value.ToString();
				var name = row.Cells[_filterColumnName].Value.ToString();
				var confirmMsg = String.Format(@"確定刪除「{0}」 : 「{1}」? 刪除後，所有相關參考此資料的記錄都會一併刪除！！", pKeySN, name);
				if (MessageBox.Show(confirmMsg, "", MessageBoxButtons.YesNo) == DialogResult.Yes)
				{
					var success = _repo.Delete(pKeySN);
					if (success)
					{
						//Reload Data, 
						reLoadData();
						//Clean Inputs
						clean_inputs_value();

						MessageBox.Show("資料刪除成功");
					}
					else
					{
						show_CRUD_ErrMsg();
					}
				}
			}
			else
			{
				MessageBox.Show("請選擇任一記錄");
			}

		}

		private void UpdateButton_Click(object sender, EventArgs e)
		{
			if (_selectedRowIndex > -1)
			{
				var row = _dv.Rows[_selectedRowIndex];
				var pKeySN = row.Cells["SN"].Value.ToString();
				var name = row.Cells[_filterColumnName].Value.ToString();
				var confirmMsg = String.Format(@"確定更新記錄「{0}」 : 「{1}」?", pKeySN, name);
				if (MessageBox.Show(confirmMsg, "", MessageBoxButtons.YesNo) == DialogResult.Yes)
				{
					var dict = _inputs.Select(p => new { ColumnName = p.Tag.ToString(), ColumnValue = p.Text.Trim() })
					.ToDictionary(item => item.ColumnName, item => (object)item.ColumnValue);

					var allowToInsert = isValid_To_Insert_Check(dict);
					if (allowToInsert)
					{
						dict.Add("ModifiedDate", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
						var success = _repo.Update(dict, pKeySN);
						if (success)
						{
							//Reload Data, 
							reLoadData();
							//Clean Inputs
							clean_inputs_value();

							MessageBox.Show("資料更新成功");
						}
						else
						{
							show_CRUD_ErrMsg();
						}
					}
				}
			}
			else
			{
				MessageBox.Show("請選擇任一記錄");
			}
		}

		private void InsertButton_Click(object sender, EventArgs e)
		{
			//產生 Key , value Dictionary (Column-Name and Column-Value)
			var dict = _inputs.Select(p => new { ColumnName = p.Tag.ToString(), ColumnValue = p.Text.Trim() })
					.ToDictionary(item => item.ColumnName, item => (object)item.ColumnValue);

			if (_repo.IsUseDataCondition)
			{
				if (!dict.ContainsKey(_conditionColName))
				{
					dict.Add(_conditionColName, _conditionColValue);
				}
			}

			var allowToInsert = isValid_To_Insert_Check(dict);
			if (allowToInsert)
			{
				if (MessageBox.Show("確定新增資料", "通知", MessageBoxButtons.YesNo) == DialogResult.Yes)
				{
					var keySN = _repo.Insert(dict);
					if (keySN > 0)
					{
						//Reload Data, 
						reLoadData();
						//Clean Inputs
						clean_inputs_value();

						MessageBox.Show("資料新增成功");
					}
					else
					{
						show_CRUD_ErrMsg();
					}
				}
			}
		}
		private void show_CRUD_ErrMsg()
		{
			MessageBox.Show("操作失敗, 因為: " + _repo.GetErrMsg());
		}
		private void clean_inputs_value()
		{
			foreach (var input in _inputs)
			{
				input.Text = "";
			}
		}

		private bool isValid_To_Insert_Check(Dictionary<string, object> dict)
		{
			var allowToInsert = false;
			if (_requiredColumnNames != null && _requiredColumnNames.Length > 0)
			{
				//檢查必填欄位
				allowToInsert = checkRequiredColumns(dict);
				if (!allowToInsert)
				{
					MessageBox.Show("必填欄位為空");
					var focusInput = _inputs.SingleOrDefault(p => p.Tag.ToString() == _emptyValueColumnName);
					if (focusInput != null)
					{
						focusInput.Focus();
					}
				}
			}
			else
			{
				var allEmpty = isInputValueAllEmpty(dict);
				allowToInsert = !allEmpty;
				if (allEmpty)
				{
					MessageBox.Show("資料不可全為空！");
				}
			}
			return allowToInsert;
		}
		private string _emptyValueColumnName = "";
		private bool checkRequiredColumns(Dictionary<string, object> dict)
		{
			//init
			var containsCount = 0;
			_emptyValueColumnName = "";
			var sb = new StringBuilder();
			//check
			foreach (var requiredCol in _requiredColumnNames)
			{
				if (dict.ContainsKey(requiredCol))
				{
					var value = dict[requiredCol];
					if (value != null && value.ToString() != "")
					{
						containsCount++;
					}
					else
					{
						_emptyValueColumnName = requiredCol;
						break;
					}
				}
				else
				{
					_emptyValueColumnName = requiredCol;
					break;
				}
			}
			return (containsCount == _requiredColumnNames.Length);
		}

		private bool isInputValueAllEmpty(Dictionary<string, object> dict)
		{
			var emptyCount = 0;
			foreach (var item in dict)
			{
				var value = item.Value;
				if (value == null)
				{
					emptyCount++;
				}
				else
				{
					if (String.IsNullOrEmpty(value.ToString()))
					{
						emptyCount++;
					}
				}
			}
			return emptyCount == dict.Count;
		}

		private void _cleanButton_Click(object sender, EventArgs e)
		{
			_nameFilter.Text = "";
		}

		public void SetupDataGridView<T>(DataGridView dv)
		{
			//assign
			_dataType = typeof(T);
			_dv = dv;

			//set properties
			UIHelper.SetDefaultDataGridViewProperties(_dv);

			//Event
			_dv.CellClick += _dv_CellClick;
			_dv.KeyUp += _dv_KeyUp;

			//DataBinding
			reLoadData();
		}

		private void reLoadData()
		{
			var rawData = _repo.GetData();
			_TotalRecords = rawData.Count;
			convertRawDataAndBinding(rawData);
		}

		private void convertRawDataAndBinding(List<object> rawData)
		{
			if (rawData != null)
			{
				var records = convertToList(rawData);

				dv_DataBinding(records);
			}
		}

		private void _dv_KeyUp(object sender, KeyEventArgs e)
		{
			var keyCode = e.KeyCode;
			if (keyCode == Keys.Up || keyCode == Keys.Down)
			{
				var dv = sender as DataGridView;
				_selectedRowIndex = dv.CurrentRow.Index;
				if (_selectedRowIndex > -1)
				{
					var row = dv.Rows[_selectedRowIndex];
					update_inputs_value(row);
				}
			}
		}

		private void _dv_CellClick(object sender, DataGridViewCellEventArgs e)
		{
			_selectedRowIndex = e.RowIndex;
			if (e.RowIndex > -1)
			{
				var dv = sender as DataGridView;
				var row = dv.Rows[e.RowIndex];
				update_inputs_value(row);
			}
		}

		private void update_inputs_value(DataGridViewRow row)
		{
			foreach (var input in _inputs)
			{
				var name = input.Tag.ToString();
				try
				{
					var cell = row.Cells[name];
					if (cell != null)
					{
						input.Text = cell.Value.ToString();
					}
				}
				catch (Exception)
				{
					//Passed
				}
			}
			if (this.RefreshInputsCompleted != null)
			{
				var requiredInputs = _inputs.Where(p => _requiredColumnNames.Contains(p.Tag.ToString())).ToList();

				var dict = requiredInputs.Select(p => new { Name = p.Name, Value = p.Text.Trim() })
					.ToDictionary(item => item.Name, item => item.Value);

				this.RefreshInputsCompleted(this, new RefreshInputEventArgs()
				{
					RequiredInputDict = dict,
				});
			}
		}

		private void dv_DataBinding(List<object> records)
		{
			_bs = new BindingSource() { DataSource = records };
			_dv.DataSource = _bs;
			updateFilterInfo(records.Count.ToString());
		}
		private void updateFilterInfo(string filters)
		{
			_filterInfoLabel.Text = String.Format("{0} / {1}", filters, _TotalRecords);
		}

		/// <summary>
		/// 由 IDictionary 轉為 List of Object
		/// </summary>
		/// <param name="rawData"></param>
		/// <returns></returns>
		private List<object> convertToList(List<object> rawData)
		{
			var aList = new List<object>();
			foreach (IDictionary<string, object> record in rawData)
			{
				var obj = Activator.CreateInstance(_dataType);
				foreach (var kv in record)
				{
					var value = (kv.Value != null) ? kv.Value.ToString() : "";
					var pName = keyNameNormalize(kv.Key);
					try
					{
						_dataType.GetProperty(pName).SetValue(obj, value);
					}
					catch (Exception ex)
					{
						var msg = ex.Message;
						throw;
					}
				}
				aList.Add(obj);
			}
			return aList;
		}
		private string keyNameNormalize(string keyName)
		{
			var norKeyName = keyName;
			switch (keyName)
			{
				case "WFP(WFN)":
					norKeyName = "WFP";
					break;
				case "WFP(ISO14046)":
					norKeyName = "ISO";
					break;
			}
			return norKeyName;
		}

		private void assignControlsTag()
		{
			/*
			 利用 Input Name 的命名慣例 : 前置詞_欄位名稱_TextBox
			 取出 "欄位名稱" 指定到 Tag 屬性
			 */
			foreach (var input in this._inputs)
			{
				var parts = input.Name.Split('_');
				var colName = parts.Length > 1 ? parts[1] : "";
				input.Tag = colName;
			}

			//功能按鈕
			foreach (var btn in this._buttons)
			{
				var parts = btn.Name.Split('_');
				btn.Tag = parts[1];
			}
		}
	}
}
