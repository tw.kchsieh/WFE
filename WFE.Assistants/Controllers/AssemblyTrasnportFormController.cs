﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WFE.Assistants.Helpers;
using WFE.Assistants.Models;
using WFE.Assistants.Models.AssemblyPageViewModels;
using WFE.Assistants.Models.ORDs;
using WFE.Assistants.Repos;

namespace WFE.Assistants.Controllers
{
	public class AssemblyTrasnportFormController
	{
		#region Controls
		private TextBox _nameFilter_TextBox;
		private ComboBox _recentUsed_ComboBox;
		private Label _Filter_Info_Label;
		private DataGridView _dv;
		private Label _WFP_Label;
		private Label _ISO_Label;
		private Label _Sum_WFP_Label;
		private Label _Sum_ISO_Label;
		private TextBox _selected_Name_TextBox;
		private TextBox _transport_Distance_TextBox;
		private TextBox _transport_WFP_TextBox;
		private TextBox _transport_ISO_TextBox;
		private DataGridView _transport_Records_DataGridView;
		#endregion

		#region variables
		private ComponentTransportToAssemblyDTO _component;
		private List<ComponentTransport> _assemblyTransports;

		//運輸工具
		private ChoiceTransportViewModel _choiceTransportDTO; //已選的運輸工具水足跡模型
		private List<Transport> _RawData;
		private BindingSource _bs;// = new BindingSource();
		private BindingList<Transport> _blist;
		private int _SelectedTransportSN = 0;

		//因為允許重複加入運輸工具，因此刪除時以 Row Index 作為判斷
		private int _SelectedTransportRowIdx = -1;

		private string[] _hideColumnNames;

		//RecentUsed
		private List<RecentUsedTransport> _RecentUsedItems;

		//===
		private string[] _hideRecordsColumnNames;
		private BindingList<ComponentTransport> _Record_blist;
		private BindingSource _Record_bs;
		private string _ErrMsg;

		#endregion

		public AssemblyTrasnportFormController(TextBox nameFilter_TextBox, ComboBox recentUsed_ComboBox,
			Label filter_Info_Label,
			DataGridView transport_DataGridView,
			TextBox selected_Name_TextBox,
			TextBox transport_WFP_TextBox,
			TextBox transport_ISO_TextBox,
			TextBox transport_Distance_TextBox,
			Label WFP_Label,
			Label ISO_Label,
			DataGridView transport_Records_DataGridView,
			Label Sum_WFP_Label,
			Label Sum_ISO_Label)
		{
			this._nameFilter_TextBox = nameFilter_TextBox;
			this._recentUsed_ComboBox = recentUsed_ComboBox;
			this._Filter_Info_Label = filter_Info_Label;
			this._dv = transport_DataGridView;
			this._WFP_Label = WFP_Label;
			this._ISO_Label = ISO_Label;
			this._selected_Name_TextBox = selected_Name_TextBox;
			this._transport_WFP_TextBox = transport_WFP_TextBox;
			this._transport_ISO_TextBox = transport_ISO_TextBox;
			this._Sum_WFP_Label = Sum_WFP_Label;
			this._Sum_ISO_Label = Sum_ISO_Label;

			this._transport_Distance_TextBox = transport_Distance_TextBox;
			this._transport_Records_DataGridView = transport_Records_DataGridView;
		}

		public void Initial(List<ComponentTransport> assemblyTransports, ComponentTransportToAssemblyDTO component)
		{
			_assemblyTransports = assemblyTransports;
			_component = component;
			//
			init();
		}

		private void init()
		{
			//init variables
			_choiceTransportDTO = new ChoiceTransportViewModel()
			{
				//C_SN = _component.,
				C_Name = _component.ComponentName,
				Mass = _component.ComponentMass,
			};

			//
			_hideColumnNames = new string[] {"SN", "CalcUnit", "UnitOne",
				//"BlueWater", "GreenWater", "GrayWater",
				"MaxLoad",
				"EnergyPerUnit", "ConsumptionOfEnergy", "ConsumptionCoefficient" };

			//Filter
			_nameFilter_TextBox.TextChanged += _nameFilter_TextBox_TextChanged;

			//transportDataGridView
			transportDV_Setup();

			//recent used combobox
			recentComboBoxSetup();

			//Transport distance
			_transport_WFP_TextBox.TextChanged += _transport_WFP_TextBox_TextChanged;
			_transport_Distance_TextBox.TextChanged += _transport_Distance_TextBox_TextChanged;

			//Transport Records DataGridView
			dataGridViewRecordSetup();
		}
		private void _nameFilter_TextBox_TextChanged(object sender, EventArgs e)
		{
			dofilter(_nameFilter_TextBox.Text.Trim());
		}
		private void dofilter(string filter)
		{
			if (String.IsNullOrEmpty(filter))
			{
				cleanFilter();
			}
			else
			{
				var filterResult = _RawData.Where(p => p.TransportName.Contains(filter)).ToList();
				transportDV_DataBinding(filterResult);
			}
		}
		private void cleanFilter()
		{
			transportDV_DataBinding(_RawData);
		}
		private void transportDV_DataBinding(List<Transport> records)
		{
			_blist = new BindingList<Transport>(records);
			_bs = new BindingSource() { DataSource = _blist };
			_dv.DataSource = _bs;
			_Filter_Info_Label.Text = String.Format("{0} / {1}", records.Count, _RawData.Count);
		}
		private void transportDV_Setup()
		{
			//setup
			UIHelper.SetDefaultDataGridViewProperties(_dv);

			//event
			_dv.CellClick += _transport_dv_CellClick;//選取事件
			_dv.DataBindingComplete += _transport_dv_DataBindingComplete;

			//init Data
			var repo = new TransportRepo();
			_RawData = repo.GetData() as List<Transport>;

			//bind data
			transportDV_DataBinding(_RawData);
		}
		private void _transport_dv_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
		{
			//
			var dvColumns = _dv.Columns.Cast<DataGridViewColumn>();
			foreach (var name in _hideColumnNames)
			{
				var col = dvColumns.SingleOrDefault(p => p.Name == name);
				if (col != null)
				{
					col.Visible = false;
				}
			}
		}
		private void _transport_dv_CellClick(object sender, DataGridViewCellEventArgs e)
		{
			_SelectedTransportSN = 0;
			if (e.RowIndex > -1)
			{
				var row = _dv.Rows[e.RowIndex];
				_SelectedTransportSN = Convert.ToInt32(row.Cells["SN"].Value);
				var nameCell = row.Cells["TransportName"];
				var wfpCell = row.Cells["WFP"];
				var isoCell = row.Cells["ISO"];
				if (nameCell != null && nameCell.Value != null)
				{
					_choiceTransportDTO.Name = nameCell.Value.ToString();
				}
				if (wfpCell != null && wfpCell.Value != null)
				{
					_choiceTransportDTO.TransportWFP = wfpCell.Value.ToString();
				}
				if (isoCell != null && isoCell.Value != null)
				{
					_choiceTransportDTO.TransportISO = isoCell.Value.ToString();
				}
				updateUI();
				//focus
				_transport_Distance_TextBox.Focus();
			}
		}
		/// <summary>
		/// 變更運輸工具 or 距離時，更新資料
		/// </summary>
		private void updateUI()
		{
			_selected_Name_TextBox.Text = _choiceTransportDTO.Name;
			_transport_WFP_TextBox.Text = _choiceTransportDTO.TransportWFP;
			_transport_ISO_TextBox.Text = _choiceTransportDTO.TransportISO;

			_WFP_Label.Text = _choiceTransportDTO.WFP;
			_ISO_Label.Text = _choiceTransportDTO.ISO;
		}

		//===================================
		private void recentComboBoxSetup()
		{
			_recentUsed_ComboBox.DropDownStyle = ComboBoxStyle.DropDownList;//整列選取

			//databinding
			recentComboBoxDataBinding();

			//event
			_recentUsed_ComboBox.SelectedIndexChanged += recentUsed_ComboBox_SelectedIndexChanged;
		}
		private void recentComboBoxDataBinding()
		{
			//data
			var repo = new TransportRepo();
			_RecentUsedItems = new List<RecentUsedTransport>();
			_RecentUsedItems.Add(new RecentUsedTransport()
			{
				TransportSN = 0,
				Name = "請選擇"
			});
			var records = repo.GetRecentUsedData() as List<RecentUsedTransport>;
			_RecentUsedItems.AddRange(records);

			if (_RecentUsedItems.Count < 2)
			{
				_recentUsed_ComboBox.Enabled = false;
			}

			//setup
			_recentUsed_ComboBox.DisplayMember = "Name";
			_recentUsed_ComboBox.ValueMember = "TransportSN";
			_recentUsed_ComboBox.DataSource = _RecentUsedItems;
		}
		private void recentUsed_ComboBox_SelectedIndexChanged(object sender, EventArgs e)
		{
			_SelectedTransportSN = 0;
			var idx = (sender as ComboBox).SelectedIndex;
			var item = _RecentUsedItems[idx];
			if (item.TransportSN > 0)
			{
				var repo = new TransportRepo();
				var transportDTO = repo.GetItem(item.TransportSN) as Transport;
				if (transportDTO != null)
				{
					_SelectedTransportSN = transportDTO.SN;
					_choiceTransportDTO.SN = transportDTO.SN;
					_choiceTransportDTO.Name = transportDTO.TransportName;
					_choiceTransportDTO.TransportWFP = transportDTO.WFP;
					_choiceTransportDTO.TransportISO = transportDTO.ISO;

					//update UI
					updateUI();

					//focus
					_transport_Distance_TextBox.Focus();
				}
			}
			else
			{
				MessageBox.Show("請選擇項目");
			}
		}


		//======================================
		private void _transport_WFP_TextBox_TextChanged(object sender, EventArgs e)
		{
			_choiceTransportDTO.TransportWFP = (sender as TextBox).Text;
			updateUI();
		}
		private void _transport_Distance_TextBox_TextChanged(object sender, EventArgs e)
		{
			_choiceTransportDTO.Distance = (sender as TextBox).Text;
			updateUI();
		}
		//=======================================
		private void dataGridViewRecordSetup()
		{
			//
			UIHelper.SetDefaultDataGridViewProperties(_transport_Records_DataGridView);
			//
			_hideRecordsColumnNames = new string[] {
				"SN",
				"C_SN", "TransportSN",
				"Component_Mass",
				"Consumption", "ConsumptionCOEEF", };

			//
			_transport_Records_DataGridView.DataBindingComplete += transport_Records_DataGridView_DataBindingComplete;
			_transport_Records_DataGridView.CellClick += _transport_Records_DataGridView_CellClick;

			//binding
			assemblyTransportDataBinding();
		}
		private void assemblyTransportDataBinding()
		{
			var records = _assemblyTransports.Where(p => p.C_Name == _component.ComponentName).ToList();
			_Record_blist = new BindingList<ComponentTransport>(records);
			_Record_bs = new BindingSource() { DataSource = _Record_blist };
			_transport_Records_DataGridView.DataSource = _Record_bs;

			//
			updateSumInfo();
		}

		private void updateSumInfo()
		{
			_Sum_WFP_Label.Text = UIHelper.NumberTextFormat(_Record_blist.Sum(p => Convert.ToDouble(p.WFP)));
			_Sum_ISO_Label.Text = UIHelper.NumberTextFormat(_Record_blist.Sum(p => Convert.ToDouble(p.ISO)));
		}

		private void _transport_Records_DataGridView_CellClick(object sender, DataGridViewCellEventArgs e)
		{
			_SelectedTransportRowIdx = e.RowIndex;
		}
		private void transport_Records_DataGridView_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
		{
			var dvColumns = _transport_Records_DataGridView.Columns.Cast<DataGridViewColumn>();
			foreach (var name in _hideRecordsColumnNames)
			{
				var col = dvColumns.SingleOrDefault(p => p.Name == name);
				if (col != null)
				{
					col.Visible = false;
				}
			}
		}
		#region Public Methods
		
		public object SaveRecentUsedItem()
		{
			var repo = new TransportRepo();
			var success = repo.UpdateRecentUsedItem(_SelectedTransportSN);
			if (!success)
			{
				_ErrMsg = repo.GetErrMsg();
			}
			return success;
		}

		public string GetErrMsg()
		{
			return _ErrMsg;
		}

		public object AddTransportRecord()
		{
			var success = false;
			if (_SelectedTransportSN > 0)
			{
				var model = new ComponentTransport()
				{
					C_SN = _choiceTransportDTO.C_SN,
					C_Name = _choiceTransportDTO.C_Name,
					//
					TransportSN = _SelectedTransportSN,
					Name = _choiceTransportDTO.Name,
					Distance = _choiceTransportDTO.Distance,
					TransportWFP = _choiceTransportDTO.TransportWFP,
					TransportISO = _choiceTransportDTO.TransportISO,
					Component_Mass = _choiceTransportDTO.Mass,
				};
				_Record_blist.Add(model);
				updateSumInfo();
				success = true;
			}
			return success;
		}

		public void DeleteTransportRecord()
		{
			if (_SelectedTransportRowIdx > -1)
			{
				var seq = _SelectedTransportRowIdx + 1;
				var model = _Record_blist[_SelectedTransportRowIdx];
				var confirmMsg = String.Format("確定刪除記錄 {2} :「{0}」 距離 : {1}", model.Name, model.Distance, seq);
				if (MessageBox.Show(confirmMsg, "警告！！", MessageBoxButtons.YesNo) == DialogResult.Yes)
				{
					_Record_blist.RemoveAt(_SelectedTransportRowIdx);
					updateSumInfo();
				}
			}
			else
			{
				MessageBox.Show("請選擇欲刪除的記錄！");
			}
		}

		public void CompletedTransportRecord()
		{
			_assemblyTransports.RemoveAll(p => p.C_Name == _component.ComponentName);
			_assemblyTransports.AddRange(_Record_blist);
		}
		#endregion

	}
}
