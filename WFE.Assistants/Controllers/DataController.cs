﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using WFE.Assistants.Controllers.TabPageControl;
using WFE.Assistants.Helpers;
using WFE.Assistants.Models;
using WFE.Assistants.Models.ORDs;
using WFE.Assistants.Repos;

namespace WFE.Assistants.Controllers
{
	public delegate void Update_CapturedSpecialComponentInfo_Delegate();
	/// <summary>
	/// 控制資料的存取
	/// </summary>
	public class DataController
	{
		private ConfigRepo _configRepo;
		private string _dbConnectionString;
		private string _errMsg;
		private string _SpecialComponentType; //特殊零件類型

		#region Page Controller
		/// <summary>
		/// A-1 一般零件
		/// </summary>
		public PageA TabPageA { get; set; }

		/// <summary>
		/// A-2 後製加工
		/// </summary>
		public PageB TabPageB { get; set; }

		/// <summary>
		/// B 特殊零件
		/// </summary>
		public PageC TabPageC { get; set; }

		public AssemblyTabPage AssemblyPage { get; set; }

		/// <summary>
		/// D 綜合分析
		/// </summary>
		public PageD TabPageD { get; set; }
		public int GenVal { get; private set; }
		public int PostProcVal { get; private set; }
		public int SpecialComponentVal { get; private set; }
		#endregion

		//TabPage ViewModels
		public StageA_ViewModel StageAViewModel { get; set; }
		public StageC_ViewModel StageCViewModel { get; set; }


		private DataController _this;
		/// <summary>
		/// 請呼叫 Setup 進行設定
		/// </summary>
		public DataController()
		{
			_this = this;
			//
			TabPageA = new PageA();
			TabPageB = new PageB();
			TabPageC = new PageC();
			TabPageD = new PageD();

			AssemblyPage = new AssemblyTabPage();

			//
			StageAViewModel = new StageA_ViewModel();
			StageCViewModel = new StageC_ViewModel();
			ProductDTO = new ProductViewModel();
		}
		public string GetErrorMsg()
		{
			return _errMsg;
		}
		public void SetupAssemblyPage(DataGridView dv, TextBox choiceTextBox
			, Button choice_transport_Button, Button add_assembly_Button
			, Label assembly_wfn_Label, Label assembly_iso_Label
			//, Button assemblyButton, Button disassemblyButton
			, TreeView assembly_TreeView
			, DataGridView assembly_record_DataGridView
			, TextBox assembly_total_wfn_TextBox, TextBox assembly_total_iso_TextBox
			, Button function_ok_Button
			, Label asm_stage_wfn_Label
			, Label asm_stage_iso_Label)
		{
			AssemblyPage.Setup(dv, choiceTextBox
				, choice_transport_Button, add_assembly_Button
				, assembly_wfn_Label, assembly_iso_Label
				//, assemblyButton, disassemblyButton
				, assembly_TreeView
				, assembly_record_DataGridView
				, assembly_total_wfn_TextBox, assembly_total_iso_TextBox
				, function_ok_Button,
				asm_stage_wfn_Label, asm_stage_iso_Label
				, this.ProductDTO);
		}
		/// <summary>
		/// AssemblyPage Update
		/// </summary>
		public void AssemblyPageUpdate()
		{
			AssemblyPage.Update();
		}


		public void SetupTabPageC(DataGridView c_Special_Components_DataGridView
			, Update_CapturedSpecialComponentInfo_Delegate func)
		{
			TabPageC.Setup(c_Special_Components_DataGridView
				, ProductDTO.SpecialComponents
				, func
				, _this);
		}

		/// <summary>
		/// StageA 是否可用
		/// </summary>
		/// <returns>True / False</returns>
		public bool IsStageViewModelValid()
		{
			var model = this.ProductDTO.GetProductComponet(this.StageAViewModel.Component_Name);
			return model != null;
		}

		/// <summary>
		/// 載入產品資訊
		/// </summary>
		/// <param name="product"></param>
		/// <returns></returns>
		public bool LoadProduct(ProductViewModel product)
		{
			var success = true;
			var fail = false;
			//先清空產品，再放入新的
			var v1 = this.ReNewProduct();
			if (!v1)
			{
				_errMsg = "ReNew Product Failed";
				return fail;
			}

			//Assign
			this.ProductDTO.ProductName = product.ProductName;
			this.ProductDTO.SN = product.SN;
			this.ProductDTO.ProductDesc = product.ProductDesc;
			this.ProductDTO.Components = product.Components;
			this.ProductDTO.PostProcessComponents = product.PostProcessComponents;
			this.ProductDTO.SpecialComponents = product.SpecialComponents;
			/*
			 AssemblyComponents = new List<AssemblyRecord>();
			AssemblyDetails = new List<Component_AssemblyDetail>();
			AssemblyTransports = new List<ComponentTransport>();
			AssemblyItems = new List<Component_AssemblyItem>();
			 */
			this.ProductDTO.AssemblyComponents = product.AssemblyComponents;
			this.ProductDTO.AssemblyDetails = product.AssemblyDetails;
			this.ProductDTO.AssemblyTransports = product.AssemblyTransports;
			this.ProductDTO.AssemblyItems = product.AssemblyItems;

			var v2 = this.TabPageA.LoadProduct(product.Components);
			if (!v2)
			{
				_errMsg = this.TabPageA.GetErrMsg();
				return fail;
			}
			var components = this.TabPageA.GetBindingList();
			var v3 = this.TabPageB.LoadProduct(components, product.PostProcessComponents);
			var v4 = this.TabPageC.LoadProduct(product.SpecialComponents);

			double wfp_1, wfp_2, wfp_3, wfp_4, iso_1, iso_2, iso_3, iso_4;
			var s1 = Double.TryParse(product.StageA_WFP, out wfp_1);
			var s2 = Double.TryParse(product.StageB_WFP, out wfp_2);
			var s3 = Double.TryParse(product.StageC_WFP, out wfp_3);
			var s7 = Double.TryParse(product.Stage_Assembly_WFP, out wfp_4);

			var s4 = Double.TryParse(product.StageA_ISO, out iso_1);
			var s5 = Double.TryParse(product.StageB_ISO, out iso_2);
			var s6 = Double.TryParse(product.StageC_ISO, out iso_3);
			var s8 = Double.TryParse(product.Stage_Assembly_ISO, out iso_4);
			if (s1 && s2 && s3 && s4 && s5 && s6)
			{
				this.TabPageD.Update(wfp_1, wfp_2, wfp_3, wfp_4, iso_1, iso_2, iso_3, iso_4);
			}
			return success;
		}


		public void SetupTabPageB(DataGridView b_ChoiceComponent_DataGridView,
			DataGridView B_Component_Post_Process_DataGridView,
			TextBox B_Name_Filter_TextBox, TextBox B_Choice_Component_TextBox)
		{
			var postComponents = ProductDTO.PostProcessComponents;
			TabPageB.Setup(b_ChoiceComponent_DataGridView,
				B_Component_Post_Process_DataGridView, postComponents,
				B_Name_Filter_TextBox, B_Choice_Component_TextBox, TabPageA.GetBindingList());
		}

		public void Init(string settingFilepath)
		{
			_configRepo = new ConfigRepo(settingFilepath);
			_dbConnectionString = _configRepo.GetDBConnectionString();
		}

		public void SetupTabPageA(DataGridView a_DataGridView)
		{
			TabPageA.Setup(a_DataGridView, ProductDTO.Components);
		}

		public string GetDBConnectionString()
		{
			return _dbConnectionString;
		}

		/// <summary>
		/// 儲存產品
		/// </summary>
		public bool SaveProduct(bool isSaveAs)
		{
			var repo = new ProductRepo();
			var product = this.ProductDTO;
			var success = false;
			if (product.SN > 0 && !isSaveAs)
			{
				success = repo.Update(product);
			}
			else if (product.SN > 0 && isSaveAs)
			{
				success = repo.SaveAs(product);
			}
			else
			{
				success = repo.Insert(product);
				if (!success)
				{
					var v1 = repo.Delete(this.ProductDTO);
					if (v1)
					{
						this.ProductDTO.Reset();
					}
					_errMsg = repo.GetErrMsg();
				}
			}
			return success;
		}

		/// <summary>
		/// 另存產品
		/// </summary>
		/// <returns></returns>
		public bool SaveAsProduct()
		{
			//TODO:另存產品
			return false;
		}

		/// <summary>
		/// 後製加工零件是否已存在
		/// </summary>
		/// <param name="componentName"></param>
		/// <returns></returns>
		public bool PostComponentExists(string componentName)
		{
			return ProductDTO.PostProcessComponents
							.Any(p => p.Component_Name == componentName);
		}

		/// <summary>
		/// 產品資訊
		/// </summary>
		public ProductViewModel ProductDTO { get; set; }

		/// <summary>
		/// 產品名稱
		/// </summary>
		public string ProductName { get; set; }

		/// <summary>
		/// 目前選取的材料模型
		/// </summary>
		public Material SelectedMaterialModel { get; set; }

		/// <summary>
		/// 選擇的製程模型
		/// </summary>
		public ManufactureProcedure SelectedManufactureProcedureModel { get; set; }

		public void GenRandomChartData()
		{
			var seed = (int)DateTime.Now.Ticks;
			var ran = new Random(seed);
			var v1 = ran.Next(0, 100);
			Thread.Sleep(20);
			var v2 = ran.Next(0, 100 - v1);
			Thread.Sleep(20);
			var v3 = 100 - v1 - v2;
			Thread.Sleep(20);
			//
			GenVal = v1;
			PostProcVal = v2;
			SpecialComponentVal = v3;
		}

		/// <summary>
		/// 設定 TabPage D
		/// </summary>
		/// <param name="gen"></param>
		/// <param name="post"></param>
		/// <param name="special"></param>
		/// <param name="total"></param>
		/// <param name="barChart"></param>
		/// <param name="pieChart"></param>
		public void SetupTabPageD(TextBox gen, TextBox post, TextBox special, TextBox total,
			Chart barChart, Chart pieChart, TextBox iso_1, TextBox iso_2, TextBox iso_3, TextBox iso_total,
			Chart iso_pieChart,
			TextBox D_WFN_4_TextBox, TextBox D_ISO_4_TextBox)
		{
			TabPageD.Setup(gen, post, special, total, barChart, pieChart, iso_1, iso_2, iso_3, iso_total, iso_pieChart, D_WFN_4_TextBox, D_ISO_4_TextBox);
		}

		/// <summary>
		/// 更新 Tab Page D 圖形顯示
		/// </summary>
		public void UpdateChart()
		{
			var wfn_1 = Convert.ToDouble(ProductDTO.StageA_WFP);
			var wfn_2 = Convert.ToDouble(ProductDTO.StageB_WFP);
			var wfn_3 = Convert.ToDouble(ProductDTO.StageC_WFP);
			var wfn_4 = Convert.ToDouble(ProductDTO.Stage_Assembly_WFP);

			var iso_1 = Convert.ToDouble(ProductDTO.StageA_ISO);
			var iso_2 = Convert.ToDouble(ProductDTO.StageB_ISO);
			var iso_3 = Convert.ToDouble(ProductDTO.StageC_ISO);
			var iso_4 = Convert.ToDouble(ProductDTO.Stage_Assembly_ISO);
			TabPageD.Update(wfn_1, wfn_2, wfn_3, wfn_4, iso_1, iso_2, iso_3, iso_4);
		}

		/// <summary>
		/// 刪除已選零件 
		/// </summary>
		public void DeleteComponent()
		{
			TabPageA.DeleteComponent();
		}

		/// <summary>
		/// 取得後製加工零件 - 運輸總水足跡 WFN
		/// </summary>
		/// <returns></returns>
		public string GetTotalPostComponentTrasnportWFP()
		{
			var wfp = "N/A";
			var postComponents = this.ProductDTO.PostProcessComponents;
			if (postComponents != null)
			{
				wfp = UIHelper.NumberTextFormat(postComponents.Select(p => p.ComponentTransportWFP).Sum());
			}

			return wfp;
		}
		/// <summary>
		/// 取得後製加工零件 - 運輸總水足跡 ISO
		/// </summary>
		/// <returns></returns>
		public string GetTotalPostComponentTrasnportISO()
		{
			var wfp = "N/A";
			var postComponents = this.ProductDTO.PostProcessComponents;
			if (postComponents != null)
			{
				wfp = UIHelper.NumberTextFormat(postComponents.Select(p => p.ComponentTransportISO).Sum());
			}
			return wfp;
		}

		/// <summary>
		/// 取得後製加工零件 - 加工總水足跡 WFN
		/// </summary>
		/// <returns></returns>
		public string GetTotalPostComponentProcessWFP()
		{
			var wfp = "N/A";
			var postComponents = this.ProductDTO.PostProcessComponents;
			if (postComponents != null)
			{
				wfp = UIHelper.NumberTextFormat(postComponents.Select(p => p.ComponentProcessWFP).Sum());
			}
			return wfp;
		}
		/// <summary>
		/// 取得後製加工零件 - 加工總水足跡 ISO
		/// </summary>
		/// <returns></returns>
		public string GetTotalPostComponentProcessISO()
		{
			var wfp = "N/A";
			var postComponents = this.ProductDTO.PostProcessComponents;
			if (postComponents != null)
			{
				wfp = UIHelper.NumberTextFormat(postComponents.Select(p => p.ComponentProcessISO).Sum());
			}
			return wfp;
		}

		/// <summary>
		/// 取得後製加工記錄總水足跡 WFN
		/// </summary>
		/// <returns></returns>
		public string GetTotalPostComponentWFP()
		{
			var wfp = "N/A";
			var postComponents = this.ProductDTO.PostProcessComponents;
			if (postComponents != null)
			{
				var sumTransport = postComponents.Select(p => p.ComponentTransportWFP).Sum();
				var sumProcesses = postComponents.Select(p => p.ComponentProcessWFP).Sum();

				wfp = UIHelper.NumberTextFormat(sumTransport + sumProcesses);
			}
			return wfp;
		}
		/// <summary>
		/// 取得後製加工記錄總水足跡 ISO
		/// </summary>
		/// <returns></returns>
		public string GetTotalPostComponentISO()
		{
			var wfp = "N/A";
			var postComponents = this.ProductDTO.PostProcessComponents;
			if (postComponents != null)
			{
				var sumTransport = postComponents.Select(p => p.ComponentTransportISO).Sum();
				var sumProcesses = postComponents.Select(p => p.ComponentProcessISO).Sum();

				wfp = UIHelper.NumberTextFormat(sumTransport + sumProcesses);
			}
			return wfp;
		}

		/// <summary>
		/// 特殊零件總水足跡(WFN)
		/// </summary>
		/// <returns></returns>
		public string GetTotalSpecialComponetWFP()
		{
			var wfp = "N/A";
			var components = this.ProductDTO.SpecialComponents;
			if (components != null)
			{
				wfp = UIHelper.NumberTextFormat(components.Sum(p => Convert.ToDouble(p.WFP)));
			}
			return wfp;
		}
		/// <summary>
		/// 特殊零件總水足跡(ISO 14046)
		/// </summary>
		/// <returns></returns>
		public string GetTotalSpecialComponetISO()
		{
			var wfp = "N/A";
			var components = this.ProductDTO.SpecialComponents;
			if (components != null)
			{
				wfp = UIHelper.NumberTextFormat(components.Sum(p => Convert.ToDouble(p.ISO)));
			}
			return wfp;
		}

		public void SetSpecialComponentType(string v)
		{
			StageCViewModel.SelectedSpeialComponentType = v;
			_SpecialComponentType = v;
		}

		public bool ReNewProduct()
		{
			bool v1 = this.ProductDTO.ReNew();
			bool v5 = this.StageAViewModel.Clear();
			bool v6 = this.StageCViewModel.Clear();
			bool v2 = TabPageA.ReNew();
			bool v3 = TabPageB.ReNew();
			bool v4 = TabPageC.ReNew();
			bool v7 = AssemblyPage.ReNew();
			//

			return (v1 && v2 && v3 && v4 && v5 && v6 && v7);
		}

		/// <summary>
		/// IsProductEmpty
		/// </summary>
		/// <returns></returns>
		public bool IsProductEmpty()
		{
			var v1Exists = this.ProductDTO.Components.Count > 0;
			var v2Exists = this.ProductDTO.PostProcessComponents.Count > 0;
			var v3Exists = this.ProductDTO.SpecialComponents.Count > 0;
			return (!v1Exists && !v2Exists && !v3Exists);
		}
	}

}
