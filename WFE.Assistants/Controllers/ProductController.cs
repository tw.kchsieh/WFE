﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WFE.Assistants.Helpers;
using WFE.Assistants.Models;
using WFE.Assistants.Repos;

namespace WFE.Assistants.Controllers
{
	public class ProductController
	{
		#region controlls
		TextBox _name_Filter_TextBox;
		ComboBox _recentUsed_ComboBox;
		Label _filterInfo_Label;
		DataGridView _dv;
		TextBox _productName_TextBox;
		TextBox _stageA_WFP_TextBox;
		TextBox _stageB_WFP_TextBox;
		TextBox _stageC_WFP_TextBox;
		TextBox _stage_asm_WFP_TextBox;
		TextBox _total_WFP_TextBox;

		TextBox _stageA_ISO_TextBox;
		TextBox _stageB_ISO_TextBox;
		TextBox _stageC_ISO_TextBox;
		TextBox _stage_asm_ISO_TextBox;
		TextBox _total_ISO_TextBox;
		#endregion
		private int _selectedRowIndex = -1;
		private DataController _dc;

		//
		private BindingList<ProductRecord> _blist;
		private BindingSource _bs;
		private List<ProductRecord> _RawData;

		//
		//Recent Used
		private List<RecentUsedDTO> _RecentUsedItems;

		//
		private ProductViewModel _choiceProduct;
		private ProductRepo _repo;
		private string _errMsg;

		public ProductController()
		{
			_repo = new ProductRepo();
		}

		public string GetErrMsg()
		{
			return _errMsg;
		}

		/// <summary>
		/// 刪除產品
		/// </summary>
		public void Delete()
		{
			//Seleted Row
			if (_selectedRowIndex > -1)
			{
				var model = _blist[_selectedRowIndex];
				var textMsg = String.Format(@"刪除產品「{0}」 ", model.ProductName);
				if (MessageBox.Show("確定" + textMsg + "？", "警告！！！", MessageBoxButtons.YesNo) == DialogResult.Yes)
				{
					//TODO: Delete From Repo.DeleteProduct, then Remove
					bool success = _repo.Delete(model.SN);
					if (success)
					{
						_blist.Remove(model);
						MessageBox.Show(textMsg + "成功");
						refreshProductUI(0);
					}
					else
					{
						MessageBox.Show(textMsg + "失敗");
					}
				}
			}
		}

		public bool LoadProduct()
		{
			var success = false;
			if (_choiceProduct != null && _choiceProduct.SN > 0)
			{
				var confirmMsg = String.Format("確定要載入產品 : 「{0}」 總水足跡 : {1}", _choiceProduct.ProductName, _choiceProduct.Total_WFP);
				if (MessageBox.Show(confirmMsg, "通知！", MessageBoxButtons.YesNo) == DialogResult.Yes)
				{
					//Load Product , 把資料放入 _dc 中
					success = _dc.LoadProduct(_choiceProduct);
					if (!success)
					{
						_errMsg = _dc.GetErrorMsg();
					}
					else
					{
						//儲存最近使用的產品
						var saveRecentedUsedSuccess = saveRecentUsedItem(_choiceProduct.SN);
						if (!saveRecentedUsedSuccess)
						{
							MessageBox.Show("儲存最近使近使用的產品失敗");
						}
					}
				}
			}
			else
			{
				MessageBox.Show("請選擇產品！！");
			}
			return success;
		}

		/// <summary>
		/// 儲存最近使用的項目
		/// </summary>
		/// <returns></returns>
		private bool saveRecentUsedItem(int productSN)
		{
			var repo = new ProductRepo();
			var success = repo.UpdateRecentUsedItem(productSN);
			if (!success)
			{
				_errMsg = repo.GetErrMsg();
			}
			return success;
		}

		public void Setup(TextBox name_Filter_TextBox,
			ComboBox recentUsed_ComboBox,
			Label filterInfo_Label,
			DataGridView product_DataGridView,
			TextBox productName_TextBox,
			TextBox stageA_WFP_TextBox,
			TextBox stageB_WFP_TextBox,
			TextBox stageC_WFP_TextBox,
			TextBox total_WFP_TextBox,
			TextBox stageA_ISO_TextBox,
			TextBox stageB_ISO_TextBox,
			TextBox stageC_ISO_TextBox,
			TextBox total_ISO_TextBox,
			TextBox stage_asm_WFP_TextBox,
			TextBox stage_asm_ISO_TextBox,
			DataController dc)
		{
			_name_Filter_TextBox = name_Filter_TextBox;
			_recentUsed_ComboBox = recentUsed_ComboBox;
			_filterInfo_Label = filterInfo_Label;
			_dv = product_DataGridView;
			_productName_TextBox = productName_TextBox;
			_stageA_WFP_TextBox = stageA_WFP_TextBox;
			_stageB_WFP_TextBox = stageB_WFP_TextBox;
			_stageC_WFP_TextBox = stageC_WFP_TextBox;
			_stage_asm_WFP_TextBox = stage_asm_WFP_TextBox;
			_total_WFP_TextBox = total_WFP_TextBox;

			_stageA_ISO_TextBox = stageA_ISO_TextBox;
			_stageB_ISO_TextBox = stageB_ISO_TextBox;
			_stageC_ISO_TextBox = stageC_ISO_TextBox;
			_stage_asm_ISO_TextBox = stage_asm_ISO_TextBox;
			_total_ISO_TextBox = total_ISO_TextBox;

			//
			_dc = dc;

			//
			initial();
		}

		private void initial()
		{
			//
			_name_Filter_TextBox.TextChanged += _name_Filter_TextBox_TextChanged;

			//assing
			_choiceProduct = new ProductViewModel();
			_RawData = _repo.GetData() as List<ProductRecord>;

			//
			dataGridViewSetup();

			////recent used combobox
			recentComboBoxSetup();
		}

		private void recentComboBoxSetup()
		{
			_recentUsed_ComboBox.DropDownStyle = ComboBoxStyle.DropDownList;//整列選取

			//databinding
			recentComboBoxDataBinding();

			//event
			_recentUsed_ComboBox.SelectedIndexChanged += _recentUsed_ComboBox_SelectedIndexChanged;
		}

		private void recentComboBoxDataBinding()
		{
			//data
			var records = _repo.GetRecentUsedData() as List<RecentUsedDTO>;

			_RecentUsedItems = new List<RecentUsedDTO>();
			_RecentUsedItems.Add(new RecentUsedDTO()
			{
				SN = 0,
				Name = "請選擇"
			});
			_RecentUsedItems.AddRange(records);

			if (_RecentUsedItems.Count < 2)
			{
				_recentUsed_ComboBox.Enabled = false;
			}

			//setup
			_recentUsed_ComboBox.DisplayMember = "Name";
			_recentUsed_ComboBox.ValueMember = "SN";
			_recentUsed_ComboBox.DataSource = _RecentUsedItems;
		}

		private void _recentUsed_ComboBox_SelectedIndexChanged(object sender, EventArgs e)
		{
			var idx = (sender as ComboBox).SelectedIndex;
			var item = _RecentUsedItems[idx];
			if (item.SN > 0)
			{
				if (_repo.IsProductExists(item.SN))
				{
					refreshProductUI(item.SN);
				}
				else
				{
					MessageBox.Show("選擇的產品不存在");
				}
			}
			else
			{
				MessageBox.Show("請選擇項目");
			}
		}

		private void _name_Filter_TextBox_TextChanged(object sender, EventArgs e)
		{
			var filter = (sender as TextBox).Text.Trim();
			dofilter(filter);
		}
		private void dofilter(string filter)
		{
			if (String.IsNullOrEmpty(filter))
			{
				cleanFilter();
			}
			else
			{
				var filterResult = _RawData.Where(p => p.ProductName.Contains(filter)).ToList();
				dataGridViewDataBinding(filterResult);
			}
		}
		private void cleanFilter()
		{
			dataGridViewDataBinding(_RawData);
		}

		private void dataGridViewSetup()
		{
			//setup
			UIHelper.SetDefaultDataGridViewProperties(_dv);

			//event
			_dv.CellClick += _dv_CellClick;//選取事件
			_dv.DataBindingComplete += _dv_DataBindingComplete; //hide Columns

			//bind data
			dataGridViewDataBinding(_RawData);
		}

		private void dataGridViewDataBinding(List<ProductRecord> records)
		{
			//
			_blist = new BindingList<ProductRecord>(records);
			_bs = new BindingSource() { DataSource = _blist };
			_dv.DataSource = _bs;
			_filterInfo_Label.Text = String.Format("{0} / {1}", records.Count, _RawData.Count);
		}

		private void _dv_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
		{
			//throw new NotImplementedException();
		}

		private void _dv_CellClick(object sender, DataGridViewCellEventArgs e)
		{
			_selectedRowIndex = e.RowIndex;
			//載入 Product Info
			if (_selectedRowIndex > -1)
			{
				var productSN = _blist[_selectedRowIndex].SN;
				refreshProductUI(productSN);
			}
		}
		private void refreshProductUI(int productSN)
		{
			//HERE
			_choiceProduct = _repo.GetProduct(productSN);
			if (_choiceProduct != null)
			{
				//UI Refresh
				_productName_TextBox.Text = _choiceProduct.ProductName;
				_stageA_WFP_TextBox.Text = _choiceProduct.StageA_WFP;
				_stageB_WFP_TextBox.Text = _choiceProduct.StageB_WFP;
				_stageC_WFP_TextBox.Text = _choiceProduct.StageC_WFP;
				_stage_asm_WFP_TextBox.Text = _choiceProduct.Stage_Assembly_WFP;

				_total_WFP_TextBox.Text = _choiceProduct.Total_WFP;

				_stageA_ISO_TextBox.Text = _choiceProduct.StageA_ISO;
				_stageB_ISO_TextBox.Text = _choiceProduct.StageB_ISO;
				_stageC_ISO_TextBox.Text = _choiceProduct.StageC_ISO;
				_stage_asm_ISO_TextBox.Text = _choiceProduct.Stage_Assembly_ISO;

				_total_ISO_TextBox.Text = _choiceProduct.Total_ISO;
			}
			else
			{
				_productName_TextBox.Text = "";
				_stageA_WFP_TextBox.Text = "";
				_stageB_WFP_TextBox.Text = "";
				_stageC_WFP_TextBox.Text = "";
				_stage_asm_WFP_TextBox.Text = "";
				_total_WFP_TextBox.Text = "";

				_stageA_ISO_TextBox.Text = "";
				_stageB_ISO_TextBox.Text = "";
				_stageC_ISO_TextBox.Text = "";
				_stage_asm_ISO_TextBox.Text = "";
				_total_ISO_TextBox.Text = "";
			}

		}
	}
}
