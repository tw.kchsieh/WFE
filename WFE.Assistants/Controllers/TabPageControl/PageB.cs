﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WFE.Assistants.Helpers;
using WFE.Assistants.Models;
using WFE.Assistants.Models.ORDs;

namespace WFE.Assistants.Controllers
{
	public class PageB
	{
		//已選零件清單
		private DataGridView _dv;
		private BindingList<ProductComponent> _blist;
		private BindingSource _bs;
		private string[] _showColumnNames;
		private int _selectedRowIndex = -1;

		//加入後製加工處理清單
		private DataGridView _PostProcessDV;
		private BindingList<ProductComponent> _record_blist;
		private BindingSource _record_bs;
		private string[] _showColumnNames_Of_RecordDV;
		private int _selected_record_row_index = -1;

		private ProductComponent _curPostComponent;

		private List<ProductComponent> _rawPostComponents;

		private TextBox _B_Name_Filter_TextBox;
		private TextBox _B_Choice_Component_TextBox;

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public bool ReNew()
		{
			_selected_record_row_index = -1;
			_selectedRowIndex = -1;
			_record_blist.Clear();
			return true;
		}

		public bool LoadProduct(BindingList<ProductComponent> components, List<ProductComponent> postComponents)
		{
			var success = true;
			_blist = components;
			dataGridViewDataBinding(_blist);

			_rawPostComponents = postComponents;
			_record_blist = new BindingList<ProductComponent>(_rawPostComponents);
			_record_bs = new BindingSource() { DataSource = _record_blist };
			_PostProcessDV.DataSource = _record_bs;

			return success;
		}

		//
		public void Setup(DataGridView b_ChoiceComponent_DataGridView,
			DataGridView B_Component_Post_Process_DataGridView,
			List<ProductComponent> postComponets,
			TextBox B_Name_Filter_TextBox, TextBox B_Choice_Component_TextBox,
			BindingList<ProductComponent> blist
			)
		{
			//assign;
			_dv = b_ChoiceComponent_DataGridView;
			_blist = blist;//已選零件
			_PostProcessDV = B_Component_Post_Process_DataGridView;
			_rawPostComponents = postComponets;

			_B_Name_Filter_TextBox = B_Name_Filter_TextBox;
			_B_Choice_Component_TextBox = B_Choice_Component_TextBox;

			//
			initial();
		}

		//新增後製加工記錄
		public void AddPostComponent(ProductComponent component)
		{
			var model = component.Clone_BasicInfo();
			_record_blist.Add(model);
			afterChoiceComponent_Do_Focus_On_PostProcessDV(model.Component_Name);
		}

		public void DeletePostComponent()
		{
			if (_selected_record_row_index > -1)
			{
				var model = _record_blist[_selected_record_row_index];
				var confirmMsg = String.Format("確定刪除記錄{0} : {1}", _selected_record_row_index + 1, model.Component_Name);
				if (MessageBox.Show(confirmMsg, "警告！！", MessageBoxButtons.YesNo) == DialogResult.Yes)
				{
					_record_blist.Remove(model);
				}
			}
			else
			{
				MessageBox.Show("請先選擇任一筆記錄！");
			}
		}

		/// <summary>
		/// 取得已選擇的後製加工零件
		/// </summary>
		/// <returns></returns>
		public ProductComponent GetSelectedPostProductComponent()
		{
			ProductComponent model = null;
			if (_selected_record_row_index > -1)
			{
				model = _record_blist[_selected_record_row_index];
			}
			return model;
		}

		/// <summary>
		/// 取得已選零件
		/// </summary>
		/// <returns></returns>
		public ProductComponent GetSelectedProductComponent()
		{
			ProductComponent model = null;
			if (_selectedRowIndex > -1)
			{
				model = _blist[_selectedRowIndex];
			}
			return model;
		}

		private void initial()
		{
			//已選零件列表
			dataGridViewSetup();

			//篩選零件
			_B_Name_Filter_TextBox.TextChanged += _B_Name_Filter_TextBox_TextChanged;

			//後製加工記錄
			postProcessDataGridViewSetup();
		}

		private void postProcessDataGridViewSetup()
		{
			//
			UIHelper.SetDefaultDataGridViewProperties(_PostProcessDV);
			_showColumnNames_Of_RecordDV = new string[] {
				"Component_Name",
				"DisplayComponentTransportWFP","DisplayComponentTransportISO",
				"DisplayComponentProcessWFP", "DisplayComponentProcessISO"
			};

			//event
			_PostProcessDV.DataBindingComplete += _PostProcessDV_DataBindingComplete;
			_PostProcessDV.CellClick += _PostProcessDV_CellClick;

			//binding data
			_record_blist = new BindingList<ProductComponent>(_rawPostComponents);
			_record_bs = new BindingSource() { DataSource = _record_blist };
			_PostProcessDV.DataSource = _record_bs;

			//Change Column Name
			var nameCol = _PostProcessDV.Columns["Component_Name"];
			if (nameCol != null)
			{
				nameCol.HeaderText = "後製加工零件";
			}
		}

		private void _PostProcessDV_CellClick(object sender, DataGridViewCellEventArgs e)
		{
			_selected_record_row_index = e.RowIndex;
		}

		private void _PostProcessDV_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
		{
			var dv = sender as DataGridView;
			var dvColumns = dv.Columns.Cast<DataGridViewColumn>();
			foreach (var col in dvColumns)
			{
				col.Visible = _showColumnNames_Of_RecordDV.Contains(col.Name);
			}
		}

		private void _B_Name_Filter_TextBox_TextChanged(object sender, EventArgs e)
		{
			var filter = (sender as TextBox).Text.Trim();
			if (String.IsNullOrEmpty(filter))
			{
				//No Change
				dataGridViewDataBinding(_blist);
			}
			else
			{
				var filterResult = _blist.Where(p => p.Component_Name.Contains(filter)).ToList();
				BindingList<ProductComponent> filter_blist = new BindingList<ProductComponent>(filterResult);
				_bs = new BindingSource() { DataSource = filter_blist };
				_dv.DataSource = _bs;
			}
		}

		private void dataGridViewSetup()
		{
			//setup
			UIHelper.SetDefaultDataGridViewProperties(_dv);

			//event
			_dv.CellClick += _dv_CellClick;//選取事件
			_dv.DataBindingComplete += _dv_DataBindingComplete; //hide Columns

			//show columns
			_showColumnNames = new string[] { "Component_Name", "WFP", "ISO",
				"DisplayComponentTransportWFP","DisplayComponentTransportISO",
				"DisplayComponentProcessWFP", "DisplayComponentProcessISO" };

			//bind data
			dataGridViewDataBinding(_blist);
		}

		private void dataGridViewDataBinding(BindingList<ProductComponent> blist)
		{
			_bs = new BindingSource() { DataSource = blist };
			_dv.DataSource = _bs;
		}

		private void _dv_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
		{
			var dvColumns = _dv.Columns.Cast<DataGridViewColumn>();
			foreach (var col in dvColumns)
			{
				col.Visible = _showColumnNames.Contains(col.Name);
			}
		}

		private void _dv_CellClick(object sender, DataGridViewCellEventArgs e)
		{
			_selectedRowIndex = e.RowIndex;
			if (_selectedRowIndex > -1)
			{
				var records = _bs.DataSource as BindingList<ProductComponent>;
				if (records != null)
				{
					var model = records[_selectedRowIndex];
					_B_Choice_Component_TextBox.Text = model.Component_Name;
					afterChoiceComponent_Do_Focus_On_PostProcessDV(model.Component_Name);
				}
			}
		}
		private void afterChoiceComponent_Do_Focus_On_PostProcessDV(string component_Name)
		{
			//選擇的零件若已加入「後製加工清單」，則 Focus 在該欄位上
			var post_Com = _record_blist.SingleOrDefault(p => p.Component_Name == component_Name);
			if (post_Com != null)
			{
				//Focus
				_selected_record_row_index = _record_blist.IndexOf(post_Com);
				_PostProcessDV.Rows[_selected_record_row_index].Selected = true;
			}
		}

		/// <summary>
		/// 是否已選取零件
		/// </summary>
		/// <returns></returns>
		public bool IsSelectComponent()
		{
			return _selectedRowIndex > -1 && !String.IsNullOrEmpty(_B_Choice_Component_TextBox.Text.Trim());
		}

		public void DeletePostComponentByName(string component_Name)
		{
			var model = _record_blist.SingleOrDefault(p => p.Component_Name == component_Name);
			_record_blist.Remove(model);
			_selected_record_row_index = -1;
			_B_Choice_Component_TextBox.Text = "";
		}
	}
}
