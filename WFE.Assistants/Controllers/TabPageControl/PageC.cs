﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WFE.Assistants.Helpers;
using WFE.Assistants.Models.ORDs;

namespace WFE.Assistants.Controllers
{
	/// <summary>
	/// TabControl Tabpage C Controller
	/// </summary>
	public class PageC
	{
		private DataGridView _dv;
		private BindingList<ProductSpecialComponent> _blist;
		private BindingSource _bs;
		private string[] _showColumnNames;
		private int _selectedRowIndex = -1;

		private List<ProductSpecialComponent> _components;

		/// <summary>
		/// ReNew
		/// </summary>
		/// <returns></returns>
		public bool ReNew()
		{
			_selectedRowIndex = -1;
			_blist.Clear();
			return true;
		}

		public bool LoadProduct(List<ProductSpecialComponent> specialComponents)
		{
			_components = specialComponents;
			_blist = new BindingList<ProductSpecialComponent>(_components);
			dataGridViewDataBinding(_blist);
			return true;
		}

		private DataController _dataC;
		private Update_CapturedSpecialComponentInfo_Delegate _DataGridClickCallback;
		public void Setup(DataGridView c_Special_Components_DataGridView
			, List<ProductSpecialComponent> specialComponents
			, Update_CapturedSpecialComponentInfo_Delegate callback,
			DataController dataC)
		{
			_dv = c_Special_Components_DataGridView;
			_components = specialComponents;
			_dataC = dataC;
			_DataGridClickCallback = callback;
			init();
		}

		/// <summary>
		/// 初始化
		/// </summary>
		private void init()
		{
			dataGridViewSetup();
		}
		private void dataGridViewSetup()
		{
			//setup
			UIHelper.SetDefaultDataGridViewProperties(_dv);

			//event
			_dv.CellClick += _dv_CellClick;//選取事件
			_dv.DataBindingComplete += _dv_DataBindingComplete; //hide Columns

			//show columns
			_showColumnNames = new string[] {
				"Component_Name",
				"ManufactureProcedure_Name",
				"ManufactureProcedure_ProcessTypeCode",
				"WFP",
				"ISO"
			};

			//bind data
			_blist = new BindingList<ProductSpecialComponent>(_components);
			dataGridViewDataBinding(_blist);
		}

		private void dataGridViewDataBinding(BindingList<ProductSpecialComponent> blist)
		{
			_bs = new BindingSource() { DataSource = blist };
			_dv.DataSource = _bs;
		}

		private void _dv_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
		{
			var dv = sender as DataGridView;
			var dvColumns = dv.Columns.Cast<DataGridViewColumn>();
			foreach (var col in dvColumns)
			{
				col.Visible = _showColumnNames.Contains(col.Name);
			}
		}

		private void _dv_CellClick(object sender, DataGridViewCellEventArgs e)
		{
			_selectedRowIndex = e.RowIndex;
			if (e.RowIndex > -1)
			{
				var model = _blist[e.RowIndex];
				if (model != null)
				{
					_dataC.StageCViewModel = model.ToStageC_ViewModel();
					_DataGridClickCallback();
				}

			}
		}

		/// <summary>
		/// 新增特殊零件記錄
		/// </summary>
		/// <param name="component"></param>
		public void AddComponent(ProductSpecialComponent component)
		{
			_blist.Add(component);

			//只有一筆時，預設選取第一筆
			if (_blist.Count == 1)
			{
				_selectedRowIndex = 0;
			}
		}

		/// <summary>
		/// 刪除特殊零件記錄
		/// </summary>
		public void DeleteComponent()
		{
			if (_selectedRowIndex > -1)
			{
				var model = _blist[_selectedRowIndex];
				var confirmMsg = String.Format("確定刪除記錄 {0} : 特殊零件名稱 : {1}, 水足跡:{2}", _selectedRowIndex + 1, model.Component_Name, model.WFP);
				if (MessageBox.Show(confirmMsg, "警告！！", MessageBoxButtons.YesNo) == DialogResult.Yes)
				{
					_blist.Remove(model);
				}
			}
			else
			{
				MessageBox.Show("請先選擇欲刪除的記錄！！");
			}
		}


		#region private methods

		#endregion
	}
}
