﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WFE.Assistants.Helpers;
using WFE.Assistants.Models;
using WFE.Assistants.Models.AssemblyPageViewModels;
using WFE.Assistants.Models.ORDs;
using WFE.Assistants.Views;

namespace WFE.Assistants.Controllers.TabPageControl
{
	public class AssemblyTabPage
	{
		#region Form Control variables and Data Model
		private DataGridView _component_to_assembly_dv;
		private TextBox _choiceTextBox;
		private Button _assembly_choice_transport_Button;
		private Button _add_assembly_Button;
		private Label _assembly_transport_wfn_Label;
		private Label _assembly_transport_iso_Label;
		private Button _function_ok_Button;
		private TreeView _assembly_TreeView;
		private DataGridView _assembly_record_DataGridView;
		private TextBox _assembly_total_wfn_TextBox;
		private TextBox _assembly_total_iso_TextBox;
		private Label _asm_stage_wfn_Label;
		private Label _asm_stage_iso_Label;
		private ProductViewModel _model;
		#endregion

		#region _component_to_assembly_dv DataBinding
		private ComponentTransportToAssemblyDTO _selected_CTARecord;
		private List<ComponentTransportToAssemblyDTO> _CTA_Records;
		private BindingList<ComponentTransportToAssemblyDTO> _CTA_List;
		private BindingSource _CTA_Source;
		//private BindingList<> _blis;
		#endregion

		#region _assembly_record_DataGridView
		private List<AssemblyRecord> _Assembly_Records;
		#endregion

		#region TreeView
		private TreeViewHelper _treeVHelper;
		private string _lastCheckNodeID = "";
		private List<string> _checkedNodeIDs;
		#endregion


		#region Public methods

		/// <summary>
		/// Setup
		/// </summary>
		public void Setup(DataGridView dv, TextBox choiceTextBox
			, Button assembly_choice_transport_Button, Button add_assembly_Button
			, Label assembly_wfn_Label, Label assembly_iso_Label
			, TreeView assembly_TreeView
			, DataGridView assembly_record_DataGridView
			, TextBox assembly_total_wfn_TextBox, TextBox assembly_total_iso_TextBox
			, Button function_ok_Button
			, Label asm_stage_wfn_Label
			, Label asm_stage_iso_Label
			, ProductViewModel model)
		{
			_component_to_assembly_dv = dv;
			_choiceTextBox = choiceTextBox;
			_assembly_choice_transport_Button = assembly_choice_transport_Button;
			_add_assembly_Button = add_assembly_Button;
			_assembly_transport_wfn_Label = assembly_wfn_Label;
			_assembly_transport_iso_Label = assembly_iso_Label;
			_assembly_TreeView = assembly_TreeView;
			_assembly_record_DataGridView = assembly_record_DataGridView;
			_assembly_total_wfn_TextBox = assembly_total_wfn_TextBox;
			_assembly_total_iso_TextBox = assembly_total_iso_TextBox;
			_function_ok_Button = function_ok_Button;
			_asm_stage_wfn_Label = asm_stage_wfn_Label;
			_asm_stage_iso_Label = asm_stage_iso_Label;
			_model = model;

			//init valuse			
			_function_ok_Button.Click += Function_ok_Button_Click;

			init();
		}
		private void init()
		{
			_function_ok_Button.Visible = false;

			//運輸至組裝廠DataGridView : _component_to_assembly_dv
			_CTA_Records = new List<ComponentTransportToAssemblyDTO>();
			initCTA_DV();
			CTADataBinding();

			//組裝記錄 DataGridView : _assembly_record_DataGridView
			init_Asm_Record_DV();

			//treeView
			init_TreeView();
		}

		private void Function_ok_Button_Click(object sender, EventArgs e)
		{
			if (_func != "")
			{
				switch (_func)
				{
					case "merge":
						doMerge();
						break;
					case "disassembly":
						doDisassembly();
						break;
				}
			}
		}

		private void init_TreeView()
		{
			_treeVHelper = new TreeViewHelper();
			_checkedNodeIDs = new List<string>();

			_assembly_TreeView.AfterCheck += _assembly_TreeView_AfterCheck;
			_assembly_TreeView.BeforeCheck += _assembly_TreeView_BeforeCheck;
		}
		private void _assembly_TreeView_AfterCheck(object sender, TreeViewEventArgs e)
		{
			if (e.Node.Checked)
			{
				_checkedNodeIDs.Add(e.Node.Name);
			}
			else
			{
				_checkedNodeIDs.Remove(e.Node.Name);
			}
			if (_func == "merge")
			{
				_function_ok_Button.Visible = _checkedNodeIDs.Count > 1;
			}
			else if (_func == "disassembly")
			{
				_function_ok_Button.Visible = _checkedNodeIDs.Count > 0;
			}
			_lastCheckNodeID = (e.Node.Checked) ? e.Node.Name : "";
		}
		private void _assembly_TreeView_BeforeCheck(object sender, TreeViewCancelEventArgs e)
		{
			if (_func == "merge")
			{
				if (e.Node.Checked == false && _checkedNodeIDs.Count == 2)
				{
					e.Cancel = true;
					MessageBox.Show("只能選擇 2 個零件");
				}
			}
			else if (_func == "disassembly")
			{
				if (e.Node.Checked == false && _checkedNodeIDs.Count == 1)
				{
					e.Cancel = true;
					MessageBox.Show("一次只能選擇 1 個零件進行分解/移除");
				}
			}
		}

		private void init_Asm_Record_DV()
		{
			//
			UIHelper.SetDefaultDataGridViewProperties(_assembly_record_DataGridView);
			_assembly_record_DataGridView.DataBindingComplete += (sender, e) =>
			{
				//hideColumns
				var hideColumns = new string[] { "ComponentID" };
				var dv = sender as DataGridView;
				var dvColumns = dv.Columns.Cast<DataGridViewColumn>();
				foreach (var columnName in hideColumns)
				{
					var col = dvColumns.SingleOrDefault(p => p.Name == columnName);
					if (col != null)
					{
						col.Visible = false;
					}
				}
			};
			Asm_Record_DV_DataBinding();
		}
		class TransportWFPDTO
		{
			public double WFN { get; set; }
			public double ISO { get; set; }
		}
		class TMergeDTO
		{
			public string ComponentID { get; set; }
			public string ParentID { get; set; }
			public TransportWFPDTO TransportItem { get; set; }
		}
		private TransportWFPDTO getAsmItemTransportWFN(string componentID)
		{
			TransportWFPDTO result = null;
			var model = _CTA_Records.SingleOrDefault(item => item.ComponentID == componentID);
			if (model != null)
			{
				result = new TransportWFPDTO()
				{
					WFN = Convert.ToDouble(model.TransportWFN),
					ISO = Convert.ToDouble(model.TransportISO)
				};
			}
			return result;
		}
		private void Asm_Record_DV_DataBinding()
		{
			var tRecord = _model.AssemblyItems.Select(p => new TMergeDTO()
			{
				ComponentID = p.ComponentID,
				TransportItem = getAsmItemTransportWFN(p.ComponentID),
				ParentID = p.ParentID
			}).ToList();
			for (var idx = 0; idx < tRecord.Count(); idx++)
			{
				var item = tRecord[idx];
				if (item.TransportItem == null)
				{
					var tmp = tRecord.Where(p => p.ParentID == item.ComponentID).ToList();
					if (tmp != null)
					{
						var tItem = new TransportWFPDTO()
						{
							ISO = tmp.Sum(q => q.TransportItem.ISO),
							WFN = tmp.Sum(q => q.TransportItem.WFN)
						};
						item.TransportItem = tItem;
					}
				}
			}

			//init
			_Assembly_Records = _model.AssemblyItems.Join(_model.AssemblyDetails,
								m => m.ComponentID,
								n => n.ComponentID,
								(m, n) => new AssemblyRecord
								{
									ComponentID = m.ComponentID,
									ComponentName = m.ComponentName,
									AssemblyMethodName = n.AssemblyMethodName,
									WFN = n.CalcWFP,
									ISO = n.CalcISO,
								}).ToList();
			foreach (var item in _Assembly_Records)
			{
				var model = tRecord.SingleOrDefault(p => p.ComponentID == item.ComponentID);
				if (model != null)
				{
					item.TransportWFN = UIHelper.NumberTextFormat(model.TransportItem.WFN);
					item.TransportISO = UIHelper.NumberTextFormat(model.TransportItem.ISO);
				}
			}
			_assembly_record_DataGridView.DataSource = _Assembly_Records;
		}
		private void update_assembly_transport_wfp_statstics()
		{
			var sumResults = asm_stage_transport_and_assembly_sum();

			//transport
			_assembly_transport_wfn_Label.Text = UIHelper.NumberTextFormat(sumResults.Item3);
			_assembly_transport_iso_Label.Text = UIHelper.NumberTextFormat(sumResults.Item4);

			//Total
			var total_wfn = sumResults.Item1 + sumResults.Item3;
			var total_iso = sumResults.Item2 + sumResults.Item4;
			_assembly_total_wfn_TextBox.Text = UIHelper.NumberTextFormat(total_wfn);
			_assembly_total_iso_TextBox.Text = UIHelper.NumberTextFormat(total_iso);
		}

		#region public methods =====================
		public bool ReNew()
		{
			bool success = true;
			//rebind, ui update
			_selected_CTARecord = null;
			_choiceTextBox.Text = "";
			_assembly_transport_wfn_Label.Text = "N/A";
			_assembly_transport_iso_Label.Text = "N/A";
			_assembly_total_wfn_TextBox.Text = "";
			_assembly_total_iso_TextBox.Text = "";
			_asm_stage_iso_Label.Text = "N/A";
			_asm_stage_wfn_Label.Text = "N/A";

			CTADataBinding();
			Asm_Record_DV_DataBinding();
			bindTreeNodes();
			return success;
		}
		public bool IsComponentNodesMatch()
		{
			return _checkedNodeIDs.Count > 1;
		}
		/// <summary>
		/// 更新資料繫結
		/// </summary>
		public void Update()
		{
			_selected_CTARecord = null;
			_choiceTextBox.Text = "";

			//組裝廠運輸
			CTADataBinding();

			//組裝記錄
			Asm_Record_DV_DataBinding();

			//Build Tree
			bindTreeNodes();

			update_assembly_transport_wfp_statstics();

			update_asm_record_statistical();
		}
		public bool IsCTA_Item_Selected()
		{
			return _selected_CTARecord != null;
		}
		public ComponentTransportToAssemblyDTO GetSelectedItem()
		{
			return _selected_CTARecord;
		}
		public bool IsCTA_Item_Added()
		{
			var isAdded = true;
			if (_selected_CTARecord != null)
			{
				isAdded = _model.AssemblyItems.Any(p => p.ComponentID == _selected_CTARecord.ComponentID);
			}
			return isAdded;
		}
		public void Add_CTA_Item_To_Assembly()
		{
			if (!IsCTA_Item_Added())
			{
				var sn = _model.AssemblyItems.Count + 1;
				_model.AssemblyItems.Add(new Component_AssemblyItem()
				{
					SN = sn,
					ProductSN = _model.SN,
					ComponentID = _selected_CTARecord.ComponentID,
					ComponentName = _selected_CTARecord.ComponentName,
					CreateDate = DateTime.Now,
					ParentID = "",
				});
				updateTreeView();
			}
		}
		public void UpdateTransportWFP()
		{
			//被選項目運輸水足跡更新
			if (_selected_CTARecord != null)
			{
				var component_transport_wfp = _model.AssemblyTransports.Where(p => p.C_Name == _selected_CTARecord.ComponentName)
									.Sum(p => Convert.ToDouble(p.WFP));
				var component_transport_iso = _model.AssemblyTransports.Where(p => p.C_Name == _selected_CTARecord.ComponentName)
										.Sum(p => Convert.ToDouble(p.ISO));
				_selected_CTARecord.TransportWFN = UIHelper.NumberTextFormat(component_transport_wfp);
				_selected_CTARecord.TransportISO = UIHelper.NumberTextFormat(component_transport_iso);
			}
			_component_to_assembly_dv.Refresh();
			update_assembly_transport_wfp_statstics();
		}

		public string TreeToggle()
		{
			var text = "收合";
			var isExpanded = _assembly_TreeView.Nodes.Cast<TreeNode>().Any(p => p.IsExpanded);
			if (isExpanded)
			{
				_assembly_TreeView.CollapseAll();
				text = "展開";
			}
			else
			{
				_assembly_TreeView.ExpandAll();
			}
			return text;
		}
		private string _func = "";
		private CheckBox _Function_Checkbox;
		public void EnableMerge(object sender)
		{
			_Function_Checkbox = sender as CheckBox;
			var enabled = (_Function_Checkbox.Checked);
			if (enabled)
			{
				_func = "merge";
				enableFunction();
			}
			else
			{
				disableFunction();
			}
			//var isPreviousFunctionEnabled = (_func == "merge" && _func != "");
			//if (isPreviousFunctionEnabled)
			//{
			//	disableFunction();
			//}
			//else
			//{
			//	_func = "merge";
			//	enableFunction();

			//}
			_checkedNodeIDs.Clear();
			selectedTreeNodeExpand();
		}
		public void EnableDisassembly(object sender)
		{
			_Function_Checkbox = sender as CheckBox;
			var enabled = (_Function_Checkbox.Checked);
			if (enabled)
			{
				_func = "disassembly";
				enableFunction();
			}
			else
			{
				disableFunction();
			}
			_checkedNodeIDs.Clear();
			selectedTreeNodeExpand();
		}
		#endregion
		private void enableFunction()
		{
			_assembly_TreeView.CheckBoxes = true;
			resetTreeNodeCheckBox();
		}
		private void disableFunction()
		{
			_func = "";
			_assembly_TreeView.CheckBoxes = false;
			_function_ok_Button.Visible = false;
		}
		private void selectedTreeNodeExpand()
		{
			var sNode = _assembly_TreeView.SelectedNode;
			if (sNode != null)
			{
				sNode.ExpandAll();
			}
			else
			{
				_assembly_TreeView.ExpandAll();
			}
		}
		private void resetTreeNodeCheckBox()
		{
			var treeNodes = _assembly_TreeView.Nodes;

			if (_func == "disassembly")
			{
				foreach (TreeNode node in treeNodes)
				{
					setDisassemblyFunction_TreeNodeCheckBox(node);
				}
			}
			else
			{
				foreach (TreeNode node in treeNodes)
				{
					setMergeFunction_TreeNode_CheckBox(node);
				}
			}
		}
		private bool doMerge()
		{
			var success = false;
			var isMatch = _checkedNodeIDs.Count > 1;
			if (!isMatch)
			{
				MessageBox.Show("請再選擇 1 個零件作組裝");
			}
			else
			{
				//找 Checked Items
				var cks = _model.AssemblyItems.Where(p => _checkedNodeIDs.Contains(p.ComponentID)).ToList();
				var dialog = new AssemblyForm()
				{
					StartPosition = FormStartPosition.CenterParent,
					TopMost = true,
				};
				var c1 = cks[0];
				var c2 = cks[1];
				dialog.Setup(c1.ComponentName, c2.ComponentName);
				if (dialog.ShowDialog() == DialogResult.OK)
				{
					var detail = dialog.GetModel();
					_model.AssemblyDetails.Add(detail);

					//1. 建立 Parent Record
					var parent = new Component_AssemblyItem()
					{
						ComponentID = detail.ComponentID,
						ComponentName = detail.ComponentName,
						ParentID = "",
					};
					_model.AssemblyItems.Add(parent);

					//MessageBox.Show(detail.ComponentName);
					//2. 設定 ck items ParentID = parent.ID
					c1.ParentID = parent.ComponentID;
					c2.ParentID = parent.ComponentID;

					//3. 重綁定 Tree
					bindTreeNodes();

					//4. Done
					functionWorkDone();
				}
			}
			return success;
		}
		private void doDisassembly()
		{
			if (_checkedNodeIDs.Count < 1)
			{
				MessageBox.Show("請選擇要分解 / 移除的零件");
				return;
			}
			var cks = _model.AssemblyItems.Where(p => _checkedNodeIDs.Contains(p.ComponentID)).ToList();
			var msg = String.Format("確定分解 / 移除「{0}」", cks[0].ComponentName);
			if (MessageBox.Show(msg, "確認", MessageBoxButtons.YesNo) == DialogResult.Yes)
			{
				deleteRecord(cks[0]);

				//重新建立 TreeView
				bindTreeNodes();

				//Done
				functionWorkDone();
			}
		}
		private void deleteRecord(Component_AssemblyItem record)
		{
			var records = _model.AssemblyItems;
			//1. find child, clean parent
			var childs = records.Where(p => p.ParentID == record.ComponentID).ToList();
			foreach (var child in childs)
			{
				child.ParentID = "";
			}
			//2.find parant
			if (record.ParentID != "")
			{
				var parent = records.SingleOrDefault(p => p.ComponentID == record.ParentID);
				var parentChildCount = records.Count(p => p.ParentID == parent.ComponentID);
				if (parentChildCount > 2)
				{
					//doNothing
				}
				else
				{
					deleteRecord(parent);
				}
			}
			//3. 移除自己
			var detail = _model.AssemblyDetails.SingleOrDefault(p => p.ComponentID == record.ComponentID);
			if (detail != null)
			{
				_model.AssemblyDetails.Remove(detail);
			}
			records.Remove(record);

		}
		private void functionWorkDone()
		{
			_checkedNodeIDs.Clear();
			_func = "";
			_function_ok_Button.Visible = false;
			_assembly_TreeView.ExpandAll();
			if (_Function_Checkbox != null)
			{
				_Function_Checkbox.Checked = false;
			}
			_assembly_TreeView.CheckBoxes = false;

			Asm_Record_DV_DataBinding();
			//
			update_asm_record_statistical();
		}
		/// <summary>
		/// 更新組裝記錄總水資訊
		/// </summary>
		private void update_asm_record_statistical()
		{
			//assembly
			var sumResults = asm_stage_transport_and_assembly_sum();
			_asm_stage_wfn_Label.Text = UIHelper.NumberTextFormat(sumResults.Item1);
			_asm_stage_iso_Label.Text = UIHelper.NumberTextFormat(sumResults.Item2);

			//Total
			var total_wfn = sumResults.Item1 + sumResults.Item3;
			var total_iso = sumResults.Item2 + sumResults.Item4;
			_assembly_total_wfn_TextBox.Text = UIHelper.NumberTextFormat(total_wfn);
			_assembly_total_iso_TextBox.Text = UIHelper.NumberTextFormat(total_iso);
		}
		private Tuple<double, double, double, double> asm_stage_transport_and_assembly_sum()
		{
			var asm_wfn_sum = _model.AssemblyDetails.Sum(p => Convert.ToDouble(p.CalcWFP));
			var asm_iso_sum = _model.AssemblyDetails.Sum(p => Convert.ToDouble(p.CalcISO));
			var asm_t_wfn_sum = _model.AssemblyTransports.Sum(p => Convert.ToDouble(p.WFP));
			var asm_t_iso_sum = _model.AssemblyTransports.Sum(p => Convert.ToDouble(p.ISO));
			return Tuple.Create<double, double, double, double>(asm_wfn_sum, asm_iso_sum,
																asm_t_wfn_sum, asm_t_iso_sum);
		}
		private void updateTreeView()
		{
			bindTreeNodes();
		}
		private void bindTreeNodes(string selectedNodeID)
		{
			bindTreeNodes();
			if (selectedNodeID != "")
			{
				var node = _assembly_TreeView.Nodes.Find(selectedNodeID, true).FirstOrDefault();
				if (node != null)
				{
					_assembly_TreeView.SelectedNode = node;
				}
			}
		}

		private void bindTreeNodes()
		{
			_assembly_TreeView.Nodes.Clear();
			var treeNodes = _assembly_TreeView.Nodes;
			var tmp = new List<TreeNode>();

			//依 Record, 建立所有的 TreeNode
			foreach (var item in _model.AssemblyItems)
			{
				tmp.Add(item.ToTreeNode());
			}
			while (tmp.Any(p => p.Tag.ToString() != ""))
			{
				var curNode = tmp[0];
				var pID = curNode.Tag.ToString();
				var copyNode = curNode.Clone() as TreeNode;
				if (pID != "")
				{
					//找 ParentNode
					var pNode = tmp.SingleOrDefault(p => p.Name == pID);
					//Here
					if (pNode != null)
					{
						pNode.Nodes.Add(copyNode);
					}
					else
					{
						var xx = pID;
					}
				}
				else
				{
					//add to Tree
					treeNodes.Add(copyNode);
				}
				//Remove
				tmp.RemoveAt(0);
			}
			//Final
			foreach (var node in tmp)
			{
				var fNode = node.Clone() as TreeNode;
				treeNodes.Add(fNode);
			}

			//Checkbox do
			foreach (TreeNode node in treeNodes)
			{
				setDisassemblyFunction_TreeNodeCheckBox(node);
			}
			_assembly_TreeView.ExpandAll();
			_checkedNodeIDs.Clear();
		}
		private void setDisassemblyFunction_TreeNodeCheckBox(TreeNode node)
		{
			var childNode = node.Nodes;
			//非第 0 階且沒有小孩的節點，不顯示 Checkbox
			var nodeShouldHide = (node.Level != 0 && childNode.Count == 0);
			if (nodeShouldHide)
			{
				_treeVHelper.HideCheckBox(_assembly_TreeView, node);
			}
			//
			foreach (TreeNode item in childNode)
			{
				setDisassemblyFunction_TreeNodeCheckBox(item);
			}
		}
		private void setMergeFunction_TreeNode_CheckBox(TreeNode node)
		{
			var childNode = node.Nodes;
			//非第 0 階不顯示 Checkbox
			var nodeShouldHide = (node.Level != 0);
			if (nodeShouldHide)
			{
				_treeVHelper.HideCheckBox(_assembly_TreeView, node);
			}
			//
			foreach (TreeNode item in childNode)
			{
				setMergeFunction_TreeNode_CheckBox(item);
			}
		}

		#endregion
		/// <summary>
		/// 運輸至組裝廠DataGridView : _component_to_assembly_dv (CTA, Component to Assembly) 初始化
		/// </summary>
		private void initCTA_DV()
		{
			//
			UIHelper.SetDefaultDataGridViewProperties(_component_to_assembly_dv);

			//Event
			_component_to_assembly_dv.CellClick += (sender, e) =>
			{
				if (e.RowIndex > -1)
				{
					_selected_CTARecord = _CTA_Records[e.RowIndex];
					_choiceTextBox.Text = _selected_CTARecord.ComponentName;
				}
			};
			_component_to_assembly_dv.DataBindingComplete += (sender, e) =>
			{
				//hideColumns
				var hideColumns = new string[] { "ComponentID", "ComponentTypeCode", "ComponentMass", "ComponentCreateDate" };
				var dv = sender as DataGridView;
				var dvColumns = dv.Columns.Cast<DataGridViewColumn>();
				foreach (var columnName in hideColumns)
				{
					var col = dvColumns.SingleOrDefault(p => p.Name == columnName);
					if (col != null)
					{
						col.Visible = false;
					}
				}
			};
		}

		private void CTADataBinding()
		{
			//init
			_CTA_Records.Clear();

			//data
			var genComponents = _model.Components
								.Where(item => !_model.PostProcessComponents.Any(p => p.Component_Name == item.Component_Name))
								.Select(item => new ComponentTransportToAssemblyDTO()
								{
									ComponentName = item.Component_Name,
									ComponentCreateDate = item.CreateDate,
									ComponentType = "A-1",
									ComponentTypeCode = "general",
									ComponentMass = item.Mass,
									PreviousStageWFN = item.Get_StageA_Total_WFN(),
									PreviousStageISO = item.Get_StageA_Total_ISO(),
									TransportWFN = UIHelper.NumberTextFormat(_model.AssemblyTransports.Where(p => p.C_Name == item.Component_Name).Sum(p => Convert.ToDouble(p.WFP))),
									TransportISO = UIHelper.NumberTextFormat(_model.AssemblyTransports.Where(p => p.C_Name == item.Component_Name).Sum(p => Convert.ToDouble(p.ISO))),
								}).ToList();

			var postComponets = _model.PostProcessComponents.Select(item => new ComponentTransportToAssemblyDTO()
			{
				ComponentName = item.Component_Name,
				ComponentCreateDate = item.CreateDate,
				ComponentType = "A-2",
				ComponentTypeCode = "post",
				ComponentMass = item.Mass,
				PreviousStageWFN = item.GetPostStage_Total_WFN(),
				PreviousStageISO = item.GetPostStage_Total_ISO(),
				TransportWFN = UIHelper.NumberTextFormat(_model.AssemblyTransports.Where(p => p.C_Name == item.Component_Name).Sum(p => Convert.ToDouble(p.WFP))),
				TransportISO = UIHelper.NumberTextFormat(_model.AssemblyTransports.Where(p => p.C_Name == item.Component_Name).Sum(p => Convert.ToDouble(p.ISO))),
			}).ToList();

			var spComponents = _model.SpecialComponents.Select(item => new ComponentTransportToAssemblyDTO()
			{
				ComponentName = item.Component_Name,
				ComponentCreateDate = item.CreateDate,
				ComponentType = "B",
				ComponentTypeCode = "special",
				ComponentMass = item.Component_Mass,
				PreviousStageWFN = item.WFP,
				PreviousStageISO = item.ISO,
				TransportWFN = UIHelper.NumberTextFormat(_model.AssemblyTransports.Where(p => p.C_Name == item.Component_Name).Sum(p => Convert.ToDouble(p.WFP))),
				TransportISO = UIHelper.NumberTextFormat(_model.AssemblyTransports.Where(p => p.C_Name == item.Component_Name).Sum(p => Convert.ToDouble(p.ISO))),
			}).ToList();

			_CTA_Records.AddRange(genComponents);
			_CTA_Records.AddRange(postComponets);
			_CTA_Records.AddRange(spComponents);

			_CTA_List = new BindingList<ComponentTransportToAssemblyDTO>(_CTA_Records);
			_CTA_Source = new BindingSource() { DataSource = _CTA_List };
			_component_to_assembly_dv.DataSource = _CTA_Source;
		}
	}
}
