﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WFE.Assistants.Helpers;
using WFE.Assistants.Models.EventArgs;
using WFE.Assistants.Models.ORDs;

namespace WFE.Assistants.Controllers
{
	/// <summary>
	/// TabPage A UI Controller
	/// </summary>
	public class PageA
	{
		private DataGridView _dv;
		private BindingSource _bs;
		private BindingList<ProductComponent> _blist;

		private List<ProductComponent> _RawData;
		private string[] _showColumnNames;
		private int _selectedRowIndex;

		private string _errMsg;

		public delegate void ComponentClickEventHandler(object sender, ProductComponentEventArgs e);
		public delegate void ComponentDeleteEventHandler(object sender, ProductComponentEventArgs e);
		public event ComponentClickEventHandler ProductComponetClick;
		public event ComponentDeleteEventHandler ProductComponetDeleted;

		public void Setup(DataGridView a_DataGridView,
			List<ProductComponent> components)
		{
			_dv = a_DataGridView;
			_RawData = components;
			_showColumnNames = new string[] { "Component_Name", "WFP", "ISO",
											"DisplayComponentTransportWFP", "DisplayComponentTransportISO",
											"DisplayComponentProcessWFP", "DisplayComponentProcessISO"  };
			dataGridViewSetup();
		}

		public string GetErrMsg()
		{
			return _errMsg;
		}

		/// <summary>
		/// ReNew
		/// <para>Clear Record</para>
		/// </summary>
		/// <returns></returns>
		public bool ReNew()
		{
			_selectedRowIndex = -1;
			_blist.Clear();
			return true;
		}

		/// <summary>
		/// 載入產品
		/// </summary>
		/// <param name="components"></param>
		/// <param name="msg"></param>
		/// <returns></returns>
		public bool LoadProduct(List<ProductComponent> components)
		{
			var success = true;
			_RawData = components;
			dataGridViewDataBinding(_RawData);
			return success;
		}

		/// <summary>
		/// 新增零件
		/// </summary>
		/// <param name="component"></param>
		public void AddComponent(ProductComponent component)
		{
			_blist.Add(component);
		}

		/// <summary>
		/// 取得目前選取的 Product Component
		/// </summary>
		/// <returns></returns>
		public ProductComponent GetSelectedProductComponent()
		{
			ProductComponent model = null;
			if (_selectedRowIndex > -1)
			{
				model = _blist[_selectedRowIndex];
			}
			return model;
		}

		public BindingList<ProductComponent> GetBindingList()
		{
			return _blist;
		}

		/// <summary>
		/// 刪除已選零件
		/// </summary>
		public void DeleteComponent()
		{
			//Seleted Row
			if (_selectedRowIndex > -1)
			{
				var model = _blist[_selectedRowIndex];
				var textMsg = String.Format(@"確定刪除零件「{0}」? 
刪除後，該零件的「運輸」、「加工」及「後製加工」記錄都會一併刪除", model.Component_Name);
				if (MessageBox.Show(textMsg, "警告！！！", MessageBoxButtons.YesNo) == DialogResult.Yes)
				{
					if (ProductComponetDeleted != null)
					{
						ProductComponetDeleted(this, new ProductComponentEventArgs()
						{
							Data = model.Clone_BasicInfo()
						});
					}
					_blist.Remove(model);
				}
			}
		}

		private void dataGridViewSetup()
		{
			//setup
			UIHelper.SetDefaultDataGridViewProperties(_dv);

			//event
			_dv.CellClick += _dv_CellClick;//選取事件
			_dv.DataBindingComplete += _dv_DataBindingComplete; //hide Columns

			//bind data
			dataGridViewDataBinding(_RawData);
		}

		private void dataGridViewDataBinding(List<ProductComponent> records)
		{
			_blist = new BindingList<ProductComponent>(records);
			_bs = new BindingSource() { DataSource = _blist };
			_dv.DataSource = _bs;
		}

		private void _dv_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
		{
			var dv = sender as DataGridView;
			var dvColumns = dv.Columns.Cast<DataGridViewColumn>();
			foreach (var col in dvColumns)
			{
				col.Visible = _showColumnNames.Contains(col.Name);
			}
		}

		private void _dv_CellClick(object sender, DataGridViewCellEventArgs e)
		{
			_selectedRowIndex = e.RowIndex;
			if (_selectedRowIndex > -1)
			{
				if (ProductComponetClick != null)
				{
					var model = GetSelectedProductComponent();
					ProductComponetClick(this, new ProductComponentEventArgs()
					{
						Data = model
					});
				}
			}
		}
	}
}
