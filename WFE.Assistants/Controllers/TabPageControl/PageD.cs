﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using WFE.Assistants.Helpers;

namespace WFE.Assistants.Controllers
{
	/// <summary>
	/// 綜合分析頁面
	/// </summary>
	public class PageD
	{
		private TextBox _genTB;
		private TextBox _postTB;
		private TextBox _specialTB;
		private TextBox _totalTB;
		private TextBox _WFN_4_TB;

		//ISO 14046
		private TextBox _ISO_1_TB;
		private TextBox _ISO_2_TB;
		private TextBox _ISO_3_TB;
		private TextBox _ISO_4_TB;
		private TextBox _ISO_Total_TB;

		//
		private Chart _barChart;
		private Chart _pieChart;
		private Chart _iso_pieChart;

		public void Setup(TextBox gen, TextBox post, TextBox special, TextBox total,
			Chart barChart, Chart pieChart,
			TextBox iso_1, TextBox iso_2, TextBox iso_3, TextBox iso_total, Chart iso_pieChart,
			TextBox D_WFN_4_TextBox, TextBox D_ISO_4_TextBox)
		{
			_genTB = gen;
			_postTB = post;
			_specialTB = special;
			_totalTB = total;

			_ISO_1_TB = iso_1;
			_ISO_2_TB = iso_2;
			_ISO_3_TB = iso_3;
			_ISO_Total_TB = iso_total;
			_iso_pieChart = iso_pieChart;

			_barChart = barChart;
			_pieChart = pieChart;

			_WFN_4_TB = D_WFN_4_TextBox;
			_ISO_4_TB = D_ISO_4_TextBox;
		}

		private void getPercentageOfWFE(double genVal, double postVal, double specVal,
			out double geneVal_p, out double postVal_p, out double specVal_p)
		{
			var sum = (genVal + postVal + specVal);
			geneVal_p = (genVal > 0) ? (genVal / sum) * 100 : 0.0;
			postVal_p = (postVal > 0) ? (postVal / sum) * 100 : 0.0;
			specVal_p = (specVal > 0) ? (specVal / sum) * 100 : 0.0;
		}
		private void getPercentageOfWFE(double sum, double item, out double percentage)
		{
			percentage = (item > 0) ? (item / sum) * 100 : 0.0;
		}

		/// <summary>
		/// 更新 PieChar, Stack Column Chart
		/// </summary>
		/// <param name="wfn_1"></param>
		/// <param name="wfn_2"></param>
		/// <param name="wfn_3"></param>
		/// <param name="iso_1"></param>
		/// <param name="iso_2"></param>
		/// <param name="iso_3"></param>
		/// <param name=""></param>
		public void Update(double wfn_1, double wfn_2, double wfn_3, double wfn_4,
			double iso_1, double iso_2, double iso_3, double iso_4)
		{
			//TextBox UI
			var totalWFP = wfn_1 + wfn_2 + wfn_3 + wfn_4;
			_genTB.Text = UIHelper.NumberTextFormat(wfn_1);
			_postTB.Text = UIHelper.NumberTextFormat(wfn_2);
			_specialTB.Text = UIHelper.NumberTextFormat(wfn_3);
			_WFN_4_TB.Text = UIHelper.NumberTextFormat(wfn_4);

			_totalTB.Text = UIHelper.NumberTextFormat(totalWFP);

			//
			var totalISO = iso_1 + iso_2 + iso_3 + iso_4;
			_ISO_1_TB.Text = UIHelper.NumberTextFormat(iso_1);
			_ISO_2_TB.Text = UIHelper.NumberTextFormat(iso_2);
			_ISO_3_TB.Text = UIHelper.NumberTextFormat(iso_3);
			_ISO_4_TB.Text = UIHelper.NumberTextFormat(iso_4);
			_ISO_Total_TB.Text = UIHelper.NumberTextFormat(totalISO);

			//換算為百分比
			double stage_1_WFN, stage_2_WFN, stage_3_WFN, stage_4_WFN;
			getPercentageOfWFE(totalWFP, wfn_1, out stage_1_WFN);
			getPercentageOfWFE(totalWFP, wfn_2, out stage_2_WFN);
			getPercentageOfWFE(totalWFP, wfn_3, out stage_3_WFN);
			getPercentageOfWFE(totalWFP, wfn_4, out stage_4_WFN);
			/*
			getPercentageOfWFE(wfn_1, wfn_2, wfn_3,
				out stage_1_WFN, out stage_2_WFN, out stage_3_WFN);*/

			double stage_1_ISO, stage_2_ISO, stage_3_ISO, stage_4_ISO;
			getPercentageOfWFE(totalISO, iso_1, out stage_1_ISO);
			getPercentageOfWFE(totalISO, iso_2, out stage_2_ISO);
			getPercentageOfWFE(totalISO, iso_3, out stage_3_ISO);
			getPercentageOfWFE(totalISO, iso_4, out stage_4_ISO);

			/*getPercentageOfWFE(iso_1, iso_2, iso_3,
				out stage_1_ISO, out stage_2_ISO, out stage_3_ISO);*/

			//update chart
			updatePieChart(stage_1_WFN, stage_2_WFN, stage_3_WFN, stage_4_WFN, _pieChart);
			updatePieChart(stage_1_ISO, stage_2_ISO, stage_3_ISO, stage_4_ISO, _iso_pieChart);

			updateStackColumnChart(stage_1_WFN, stage_2_WFN, stage_3_WFN, stage_4_WFN, 0, _barChart);
			updateStackColumnChart(stage_1_ISO, stage_2_ISO, stage_3_ISO, stage_4_ISO, 1, _barChart);
		}

		private void updatePieChart(double a1, double a2, double a3, double a4, Chart pieChart)
		{
			var precision_digit = 2;//計算精度位數

			var genVal = Math.Round(a1, precision_digit);
			var postProcVal = Math.Round(a2, precision_digit);
			var specialComponentVal = Math.Round(a3, precision_digit);
			var assemblyVal = Math.Round(a4, precision_digit);

			//PieChart, Only One Series
			var pie_PointA = pieChart.Series[0].Points[0];
			var pie_PointB = pieChart.Series[0].Points[1];
			var pie_PointC = pieChart.Series[0].Points[2];
			var pie_PointD = pieChart.Series[0].Points[3];


			pie_PointA.SetValueY(genVal);
			pie_PointA.LabelToolTip = pie_PointA.Label = String.Format("{0}%", genVal);
			pie_PointA.LegendToolTip = pie_PointA.ToolTip = String.Format("{0} ({1}%)", _STAGE_A_Label, genVal);

			pie_PointB.SetValueY(postProcVal);
			pie_PointB.LabelToolTip = pie_PointB.Label = String.Format("{0}%", postProcVal);
			pie_PointB.LegendToolTip = pie_PointB.ToolTip = String.Format("{0} ({1}%)", _STAGE_B_Label, postProcVal);

			pie_PointC.SetValueY(specialComponentVal);
			pie_PointC.LabelToolTip = pie_PointC.Label = String.Format("{0}%", specialComponentVal);
			pie_PointC.LegendToolTip = pie_PointC.ToolTip = String.Format("{0} ({1}%)", _STAGE_C_Label, specialComponentVal);

			pie_PointD.SetValueY(assemblyVal);
			pie_PointD.LabelToolTip = pie_PointD.Label = String.Format("{0}%", assemblyVal);
			pie_PointD.LegendToolTip = pie_PointD.ToolTip = String.Format("{0} ({1}%)", _STAGE_D_Label, assemblyVal);

			//設定 Label , 任一個為 百分比為 0 則不顯示 Label
			var zeroPercent = 0.0;
			if (genVal <= zeroPercent)
			{
				pie_PointA.Label = "";
			}
			if (postProcVal <= zeroPercent)
			{
				pie_PointB.Label = "";
			}
			if (specialComponentVal <= zeroPercent)
			{
				pie_PointC.Label = "";
			}
			if (assemblyVal <= zeroPercent)
			{
				pie_PointD.Label = "";
			}


			//設定 Label, 若有任一個百分比 > dominatePercent 則其它 2 個不顯示 Label 
			var dominatePercent = 97.5;
			if (genVal > dominatePercent)
			{
				pie_PointB.Label = "";
				pie_PointC.Label = "";
				pie_PointD.Label = "";
			}
			else if (postProcVal > dominatePercent)
			{
				pie_PointA.Label = "";
				pie_PointC.Label = "";
				pie_PointD.Label = "";
			}
			else if (specialComponentVal > dominatePercent)
			{
				pie_PointA.Label = "";
				pie_PointB.Label = "";
				pie_PointD.Label = "";
			}
			else if (assemblyVal > dominatePercent)
			{
				pie_PointA.Label = "";
				pie_PointB.Label = "";
				pie_PointC.Label = "";
			}

			pieChart.ChartAreas[0].RecalculateAxesScale();
			updateLegneds(pieChart);
			pieChart.Refresh();
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="a1"></param>
		/// <param name="a2"></param>
		/// <param name="a3"></param>
		/// <param name="x">x = 0, WFN; x = 1, ISO 14046</param>
		/// <param name="stackColumnChart"></param>
		private void updateStackColumnChart(double a1, double a2, double a3, double a4, int x, Chart stackColumnChart)
		{
			/* 3 series, [StageA, StageB, StageC]
			 * each series have two point (1, ?) and (2, ?) 
			 * X = 1, WFN
			 * X = 2, ISO 14046
			 */
			var precision_digit = 2;//計算精度位數
			var genVal = Math.Round(a1, precision_digit);
			var postProcVal = Math.Round(a2, precision_digit);
			var specialComponentVal = Math.Round(a3, precision_digit);
			var assemblyVal = Math.Round(a4, precision_digit);

			//set value
			var stageA_point = stackColumnChart.Series[0].Points[x];
			var stageB_point = stackColumnChart.Series[1].Points[x];
			var stageC_point = stackColumnChart.Series[2].Points[x];
			var stageD_point = stackColumnChart.Series[3].Points[x];

			stageA_point.SetValueY(genVal);
			stageA_point.Label = String.Format("{0}%", genVal);

			stageB_point.SetValueY(postProcVal);
			stageB_point.Label = String.Format("{0}%", postProcVal);

			stageC_point.SetValueY(specialComponentVal);
			stageC_point.Label = String.Format("{0}%", specialComponentVal);

			stageD_point.SetValueY(assemblyVal);
			stageD_point.Label = String.Format("{0}%", assemblyVal);

			//設定 Label , 任一個為 百分比為 0 則不顯示 Label
			var zeroPercent = 0.0;
			if (genVal <= zeroPercent)
			{
				stageA_point.Label = "";
			}
			if (postProcVal <= zeroPercent)
			{
				stageB_point.Label = "";
			}
			if (specialComponentVal <= zeroPercent)
			{
				stageC_point.Label = "";
			}
			if (assemblyVal <= zeroPercent)
			{
				stageD_point.Label = "";
			}

			//
			stackColumnChart.ChartAreas[0].RecalculateAxesScale();
			updateLegneds(stackColumnChart);
			stackColumnChart.Refresh();
		}
		private void updateLegneds(Chart theChart)
		{
			var dLegendPos = theChart.Legends[0].Position;
			for (var idx = 1; idx < theChart.Legends.Count; idx++)
			{
				theChart.Legends[idx].Position.X = dLegendPos.X;
				theChart.Legends[idx].Position.Width = dLegendPos.Width;
				theChart.Legends[idx].Position.Height = dLegendPos.Height;
			}
		}
		private string _STAGE_A_Label = "一般零件階段";
		private string _STAGE_B_Label = "後製加工階段";
		private string _STAGE_C_Label = "特殊零件階段";
		private string _STAGE_D_Label = "產品組裝階段";
	}
}
