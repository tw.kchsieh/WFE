﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WFE.Assistants.Helpers;
using WFE.Assistants.Models;
using WFE.Assistants.Models.AssemblyPageViewModels;
using WFE.Assistants.Repos;

namespace WFE.Assistants.Views
{
	public partial class AssemblyForm : Form
	{
		private Component_AssemblyDetail _detail;

		//AssemblyMethod DataGridview
		private List<AssemblyMethod> _RawData;
		private BindingList<AssemblyMethod> _blist;
		private BindingSource _bs;
		private int _SelectedItemSN = 0;

		//recent used AssemblyMethod
		private List<RecentUsedDTO> _RecentUsedItems;

		//
		private AssemblyMethodRepo _repo;
		public AssemblyForm()
		{
			InitializeComponent();
		}

		private void AssemblyForm_Load(object sender, EventArgs e)
		{
			_repo = new AssemblyMethodRepo();
			_detail = new Component_AssemblyDetail();
			mergeName_textBox.Focus();

			//
			recentComboBoxDataBinding();

			//datagridview
			UIHelper.SetDefaultDataGridViewProperties(assemblyMethod_dataGridView);
			assemblyMethod_dataGridView.CellClick += AssemblyMethod_dataGridView_CellClick;
			_RawData = _repo.GetData() as List<AssemblyMethod>;

			//dataBinding
			dataGridViewDataBinding(_RawData);
		}

		private void AssemblyMethod_dataGridView_CellClick(object sender, DataGridViewCellEventArgs e)
		{
			if (e.RowIndex > -1)
			{
				var model = _blist[e.RowIndex];
				_detail.AssemblyMethodSN = model.SN;
				_detail.AssemblyMethodName = model.AssemblyMethodName;
				_detail.PowerConsumption = model.PowerConsumption;
				_detail.PowerCoefficient = model.PowerCoefficient;
				updateUI();
			}
		}

		private void recentComboBoxDataBinding()
		{
			//data
			_RecentUsedItems = new List<RecentUsedDTO>();
			_RecentUsedItems.Add(new RecentUsedDTO()
			{
				SN = 0,
				Name = "請選擇"
			});
			var records = _repo.GetRecentUsedData() as List<RecentUsedDTO>;
			if (records != null)
			{
				_RecentUsedItems.AddRange(records);
			}

			if (_RecentUsedItems.Count < 2)
			{
				RecentUsed_ComboBox.Enabled = false;
			}

			//setup
			RecentUsed_ComboBox.DisplayMember = "Name";
			RecentUsed_ComboBox.ValueMember = "SN";
			RecentUsed_ComboBox.DataSource = _RecentUsedItems;
		}
		private void dataGridViewDataBinding(List<AssemblyMethod> records)
		{
			_blist = new BindingList<AssemblyMethod>(records);
			_bs = new BindingSource() { DataSource = _blist };
			assemblyMethod_dataGridView.DataSource = _bs;
			Filter_Info_Label.Text = String.Format("{0} / {1}", records.Count, _RawData.Count);
		}

		#region public methods
		public void Setup(string componentName1, string componentName2)
		{
			c1_label.Text = componentName1;
			c2_label.Text = componentName2;
		}
		public Component_AssemblyDetail GetModel()
		{
			return _detail;
		}
		#endregion
		private void smart_merg_name_button_Click(object sender, EventArgs e)
		{
			var name1 = c1_label.Text + " + " + c2_label.Text;
			var name2 = Guid.NewGuid().ToString().Substring(0, 6);
			var name3 = "C_" + name2;
			var name4 = "Name_" + name2;
			var name5 = "Merge(" + name1 + ")";
			var smartNames = new string[] { name1, name2, name3, name4, name5 };
			var ran = new Random((int)DateTime.Now.Ticks);
			var idx = ran.Next(0, 1000) % 5;
			mergeName_textBox.Text = smartNames[idx];
		}
		private void RecentUsed_ComboBox_SelectedIndexChanged(object sender, EventArgs e)
		{
			var idx = (sender as ComboBox).SelectedIndex;
			var item = _RecentUsedItems[idx];
			if (item.SN > 0)
			{
				var model = _RawData.SingleOrDefault(p => p.SN == item.SN);
				if (model != null)
				{
					//update view
					_detail.AssemblyMethodSN = model.SN;
					_detail.AssemblyMethodName = model.AssemblyMethodName;
					_detail.PowerConsumption = model.PowerConsumption;
					_detail.PowerCoefficient = model.PowerCoefficient;
					updateUI();
				}
			}
		}
		private void NameFilter_TextBox_TextChanged(object sender, EventArgs e)
		{
			var querytext = (sender as TextBox).Text.Trim();
			if (String.IsNullOrEmpty(querytext))
			{
				//rebinding
				dataGridViewDataBinding(_RawData);
			}
			else
			{
				var result = _RawData.Where(p => p.AssemblyMethodName.Contains(querytext)).ToList();
				dataGridViewDataBinding(result);
			}
		}

		private void updateUI()
		{
			assemblyName_textBox.Text = _detail.AssemblyMethodName;
			assembly_WFP_textBox.Text = _detail.PowerConsumption;
			assembly_ISO_textBox.Text = _detail.PowerCoefficient;
			asm_calc_wfn_label.Text = _detail.CalcWFP;
			asm_calc_iso_label.Text = _detail.CalcISO;
			hour_textBox.Focus();
		}

		private void mergeName_textBox_TextChanged(object sender, EventArgs e)
		{
			_detail.ComponentName = (sender as TextBox).Text.Trim();
			cNameLenCountLabel.Text = _detail.ComponentName.Length.ToString();
		}

		private void ok_button_Click(object sender, EventArgs e)
		{
			if (String.IsNullOrEmpty(_detail.ComponentName))
			{
				MessageBox.Show("尚未填入「組裝零件名稱」");
				mergeName_textBox.Focus();
				return;
			}
			if (_detail.Valid())
			{
				this.DialogResult = DialogResult.OK;
				var success = _repo.UpdateRecentUsedItem(_detail.AssemblyMethodSN);
				if (!success)
				{
					MessageBox.Show("儲存最新使用「組裝方式」錯誤 :" + _repo.GetErrMsg());
				}
				this.Close();
			}
			else
			{
				var msg = "尚未選擇正確的組裝方式, 確定要關閉？";
				if (MessageBox.Show(msg, "警告！！", MessageBoxButtons.YesNo) == DialogResult.Yes)
				{
					this.DialogResult = DialogResult.No;
					this.Close();
				}
			}
		}

		private void CleanFilter_Button_Click(object sender, EventArgs e)
		{
			NameFilter_TextBox.Text = "";
		}

		private void hour_textBox_TextChanged(object sender, EventArgs e)
		{
			var tb = sender as TextBox;
			var txt = tb.Text.Trim();
			double dVal;
			if (!Double.TryParse(txt, out dVal))
			{
				dVal = 0;
			}
			_detail.WorkingHour = dVal.ToString();
			updateUI();
		}
	}
}
