﻿namespace WFE.Assistants.Views
{
	partial class AssemblyForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AssemblyForm));
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.c2_label = new System.Windows.Forms.Label();
			this.c1_label = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
			this.label3 = new System.Windows.Forms.Label();
			this.mergeName_textBox = new System.Windows.Forms.TextBox();
			this.smart_merg_name_button = new System.Windows.Forms.Button();
			this.groupBox3 = new System.Windows.Forms.GroupBox();
			this.assemblyMethod_dataGridView = new System.Windows.Forms.DataGridView();
			this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
			this.label4 = new System.Windows.Forms.Label();
			this.NameFilter_TextBox = new System.Windows.Forms.TextBox();
			this.CleanFilter_Button = new System.Windows.Forms.Button();
			this.label5 = new System.Windows.Forms.Label();
			this.RecentUsed_ComboBox = new System.Windows.Forms.ComboBox();
			this.label6 = new System.Windows.Forms.Label();
			this.Filter_Info_Label = new System.Windows.Forms.Label();
			this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
			this.label7 = new System.Windows.Forms.Label();
			this.assemblyName_textBox = new System.Windows.Forms.TextBox();
			this.label8 = new System.Windows.Forms.Label();
			this.Label27 = new System.Windows.Forms.Label();
			this.assembly_WFP_textBox = new System.Windows.Forms.TextBox();
			this.label14 = new System.Windows.Forms.Label();
			this.Label28 = new System.Windows.Forms.Label();
			this.assembly_ISO_textBox = new System.Windows.Forms.TextBox();
			this.label15 = new System.Windows.Forms.Label();
			this.label9 = new System.Windows.Forms.Label();
			this.hour_textBox = new System.Windows.Forms.TextBox();
			this.label10 = new System.Windows.Forms.Label();
			this.label11 = new System.Windows.Forms.Label();
			this.label12 = new System.Windows.Forms.Label();
			this.asm_calc_wfn_label = new System.Windows.Forms.Label();
			this.label16 = new System.Windows.Forms.Label();
			this.label13 = new System.Windows.Forms.Label();
			this.asm_calc_iso_label = new System.Windows.Forms.Label();
			this.label17 = new System.Windows.Forms.Label();
			this.ok_button = new System.Windows.Forms.Button();
			this.label18 = new System.Windows.Forms.Label();
			this.cNameLenCountLabel = new System.Windows.Forms.Label();
			this.groupBox1.SuspendLayout();
			this.groupBox2.SuspendLayout();
			this.flowLayoutPanel2.SuspendLayout();
			this.groupBox3.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.assemblyMethod_dataGridView)).BeginInit();
			this.flowLayoutPanel1.SuspendLayout();
			this.flowLayoutPanel3.SuspendLayout();
			this.SuspendLayout();
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.c2_label);
			this.groupBox1.Controls.Add(this.c1_label);
			this.groupBox1.Controls.Add(this.label2);
			this.groupBox1.Controls.Add(this.label1);
			this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
			this.groupBox1.ForeColor = System.Drawing.Color.Coral;
			this.groupBox1.Location = new System.Drawing.Point(0, 0);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(702, 79);
			this.groupBox1.TabIndex = 0;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "1. 零件資訊";
			// 
			// c2_label
			// 
			this.c2_label.AutoSize = true;
			this.c2_label.ForeColor = System.Drawing.Color.Blue;
			this.c2_label.Location = new System.Drawing.Point(158, 51);
			this.c2_label.Name = "c2_label";
			this.c2_label.Size = new System.Drawing.Size(31, 15);
			this.c2_label.TabIndex = 3;
			this.c2_label.Text = "N/A";
			// 
			// c1_label
			// 
			this.c1_label.AutoSize = true;
			this.c1_label.ForeColor = System.Drawing.Color.Blue;
			this.c1_label.Location = new System.Drawing.Point(158, 21);
			this.c1_label.Name = "c1_label";
			this.c1_label.Size = new System.Drawing.Size(31, 15);
			this.c1_label.TabIndex = 2;
			this.c1_label.Text = "N/A";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Font = new System.Drawing.Font("PMingLiU", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
			this.label2.ForeColor = System.Drawing.Color.Black;
			this.label2.Location = new System.Drawing.Point(12, 51);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(116, 15);
			this.label2.TabIndex = 1;
			this.label2.Text = "組裝零件名稱 2";
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Font = new System.Drawing.Font("PMingLiU", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
			this.label1.ForeColor = System.Drawing.Color.Black;
			this.label1.Location = new System.Drawing.Point(12, 21);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(116, 15);
			this.label1.TabIndex = 0;
			this.label1.Text = "組裝零件名稱 1";
			// 
			// groupBox2
			// 
			this.groupBox2.Controls.Add(this.flowLayoutPanel2);
			this.groupBox2.Dock = System.Windows.Forms.DockStyle.Top;
			this.groupBox2.ForeColor = System.Drawing.Color.Green;
			this.groupBox2.Location = new System.Drawing.Point(0, 79);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Size = new System.Drawing.Size(702, 81);
			this.groupBox2.TabIndex = 1;
			this.groupBox2.TabStop = false;
			this.groupBox2.Text = "2. 輸入組裝後零件名稱";
			// 
			// flowLayoutPanel2
			// 
			this.flowLayoutPanel2.Controls.Add(this.label3);
			this.flowLayoutPanel2.Controls.Add(this.mergeName_textBox);
			this.flowLayoutPanel2.Controls.Add(this.smart_merg_name_button);
			this.flowLayoutPanel2.Controls.Add(this.label18);
			this.flowLayoutPanel2.Controls.Add(this.cNameLenCountLabel);
			this.flowLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.flowLayoutPanel2.Location = new System.Drawing.Point(3, 21);
			this.flowLayoutPanel2.Name = "flowLayoutPanel2";
			this.flowLayoutPanel2.Size = new System.Drawing.Size(696, 57);
			this.flowLayoutPanel2.TabIndex = 2;
			// 
			// label3
			// 
			this.label3.Font = new System.Drawing.Font("PMingLiU", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
			this.label3.ForeColor = System.Drawing.Color.OrangeRed;
			this.label3.Location = new System.Drawing.Point(3, 8);
			this.label3.Margin = new System.Windows.Forms.Padding(3, 8, 3, 0);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(146, 23);
			this.label3.TabIndex = 0;
			this.label3.Text = "組裝零件名稱";
			// 
			// mergeName_textBox
			// 
			this.mergeName_textBox.Location = new System.Drawing.Point(155, 3);
			this.mergeName_textBox.Name = "mergeName_textBox";
			this.mergeName_textBox.Size = new System.Drawing.Size(430, 25);
			this.mergeName_textBox.TabIndex = 1;
			this.mergeName_textBox.TextChanged += new System.EventHandler(this.mergeName_textBox_TextChanged);
			// 
			// smart_merg_name_button
			// 
			this.smart_merg_name_button.Location = new System.Drawing.Point(591, 3);
			this.smart_merg_name_button.Name = "smart_merg_name_button";
			this.smart_merg_name_button.Size = new System.Drawing.Size(94, 26);
			this.smart_merg_name_button.TabIndex = 2;
			this.smart_merg_name_button.Text = "隨機名稱";
			this.smart_merg_name_button.UseVisualStyleBackColor = true;
			this.smart_merg_name_button.Click += new System.EventHandler(this.smart_merg_name_button_Click);
			// 
			// groupBox3
			// 
			this.groupBox3.Controls.Add(this.assemblyMethod_dataGridView);
			this.groupBox3.Controls.Add(this.flowLayoutPanel1);
			this.groupBox3.Dock = System.Windows.Forms.DockStyle.Top;
			this.groupBox3.ForeColor = System.Drawing.Color.Orchid;
			this.groupBox3.Location = new System.Drawing.Point(0, 160);
			this.groupBox3.Name = "groupBox3";
			this.groupBox3.Size = new System.Drawing.Size(702, 387);
			this.groupBox3.TabIndex = 2;
			this.groupBox3.TabStop = false;
			this.groupBox3.Text = "3.選擇組裝方式";
			// 
			// assemblyMethod_dataGridView
			// 
			this.assemblyMethod_dataGridView.AllowUserToAddRows = false;
			this.assemblyMethod_dataGridView.AllowUserToDeleteRows = false;
			this.assemblyMethod_dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.assemblyMethod_dataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
			this.assemblyMethod_dataGridView.Location = new System.Drawing.Point(3, 124);
			this.assemblyMethod_dataGridView.Name = "assemblyMethod_dataGridView";
			this.assemblyMethod_dataGridView.ReadOnly = true;
			this.assemblyMethod_dataGridView.RowTemplate.Height = 27;
			this.assemblyMethod_dataGridView.Size = new System.Drawing.Size(696, 260);
			this.assemblyMethod_dataGridView.TabIndex = 1;
			// 
			// flowLayoutPanel1
			// 
			this.flowLayoutPanel1.Controls.Add(this.label4);
			this.flowLayoutPanel1.Controls.Add(this.NameFilter_TextBox);
			this.flowLayoutPanel1.Controls.Add(this.CleanFilter_Button);
			this.flowLayoutPanel1.Controls.Add(this.label5);
			this.flowLayoutPanel1.Controls.Add(this.RecentUsed_ComboBox);
			this.flowLayoutPanel1.Controls.Add(this.label6);
			this.flowLayoutPanel1.Controls.Add(this.Filter_Info_Label);
			this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top;
			this.flowLayoutPanel1.Location = new System.Drawing.Point(3, 21);
			this.flowLayoutPanel1.Name = "flowLayoutPanel1";
			this.flowLayoutPanel1.Size = new System.Drawing.Size(696, 103);
			this.flowLayoutPanel1.TabIndex = 0;
			// 
			// label4
			// 
			this.label4.Font = new System.Drawing.Font("PMingLiU", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
			this.label4.ForeColor = System.Drawing.Color.Black;
			this.label4.Location = new System.Drawing.Point(3, 8);
			this.label4.Margin = new System.Windows.Forms.Padding(3, 8, 3, 0);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(146, 23);
			this.label4.TabIndex = 0;
			this.label4.Text = "組裝方式名稱篩選";
			// 
			// NameFilter_TextBox
			// 
			this.NameFilter_TextBox.Location = new System.Drawing.Point(155, 3);
			this.NameFilter_TextBox.Name = "NameFilter_TextBox";
			this.NameFilter_TextBox.Size = new System.Drawing.Size(430, 25);
			this.NameFilter_TextBox.TabIndex = 1;
			this.NameFilter_TextBox.TextChanged += new System.EventHandler(this.NameFilter_TextBox_TextChanged);
			// 
			// CleanFilter_Button
			// 
			this.CleanFilter_Button.ForeColor = System.Drawing.Color.Black;
			this.CleanFilter_Button.Location = new System.Drawing.Point(591, 3);
			this.CleanFilter_Button.Name = "CleanFilter_Button";
			this.CleanFilter_Button.Size = new System.Drawing.Size(94, 26);
			this.CleanFilter_Button.TabIndex = 2;
			this.CleanFilter_Button.Text = "清除篩選";
			this.CleanFilter_Button.UseVisualStyleBackColor = true;
			this.CleanFilter_Button.Click += new System.EventHandler(this.CleanFilter_Button_Click);
			// 
			// label5
			// 
			this.label5.Font = new System.Drawing.Font("PMingLiU", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
			this.label5.ForeColor = System.Drawing.Color.Black;
			this.label5.Location = new System.Drawing.Point(3, 40);
			this.label5.Margin = new System.Windows.Forms.Padding(3, 8, 3, 0);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(146, 23);
			this.label5.TabIndex = 3;
			this.label5.Text = "最近曾用組裝方式";
			// 
			// RecentUsed_ComboBox
			// 
			this.RecentUsed_ComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.RecentUsed_ComboBox.FormattingEnabled = true;
			this.RecentUsed_ComboBox.Location = new System.Drawing.Point(155, 35);
			this.RecentUsed_ComboBox.Name = "RecentUsed_ComboBox";
			this.RecentUsed_ComboBox.Size = new System.Drawing.Size(530, 23);
			this.RecentUsed_ComboBox.TabIndex = 4;
			this.RecentUsed_ComboBox.SelectedIndexChanged += new System.EventHandler(this.RecentUsed_ComboBox_SelectedIndexChanged);
			// 
			// label6
			// 
			this.label6.Font = new System.Drawing.Font("PMingLiU", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
			this.label6.ForeColor = System.Drawing.Color.Black;
			this.label6.Location = new System.Drawing.Point(3, 71);
			this.label6.Margin = new System.Windows.Forms.Padding(3, 8, 3, 0);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(146, 23);
			this.label6.TabIndex = 5;
			this.label6.Text = "篩選筆數 / 總筆數";
			// 
			// Filter_Info_Label
			// 
			this.Filter_Info_Label.AutoSize = true;
			this.Filter_Info_Label.Location = new System.Drawing.Point(155, 71);
			this.Filter_Info_Label.Margin = new System.Windows.Forms.Padding(3, 8, 3, 0);
			this.Filter_Info_Label.Name = "Filter_Info_Label";
			this.Filter_Info_Label.Size = new System.Drawing.Size(31, 15);
			this.Filter_Info_Label.TabIndex = 6;
			this.Filter_Info_Label.Text = "N/A";
			// 
			// flowLayoutPanel3
			// 
			this.flowLayoutPanel3.Controls.Add(this.label7);
			this.flowLayoutPanel3.Controls.Add(this.assemblyName_textBox);
			this.flowLayoutPanel3.Controls.Add(this.label8);
			this.flowLayoutPanel3.Controls.Add(this.Label27);
			this.flowLayoutPanel3.Controls.Add(this.assembly_WFP_textBox);
			this.flowLayoutPanel3.Controls.Add(this.label14);
			this.flowLayoutPanel3.Controls.Add(this.Label28);
			this.flowLayoutPanel3.Controls.Add(this.assembly_ISO_textBox);
			this.flowLayoutPanel3.Controls.Add(this.label15);
			this.flowLayoutPanel3.Controls.Add(this.label9);
			this.flowLayoutPanel3.Controls.Add(this.hour_textBox);
			this.flowLayoutPanel3.Controls.Add(this.label10);
			this.flowLayoutPanel3.Controls.Add(this.label11);
			this.flowLayoutPanel3.Controls.Add(this.label12);
			this.flowLayoutPanel3.Controls.Add(this.asm_calc_wfn_label);
			this.flowLayoutPanel3.Controls.Add(this.label16);
			this.flowLayoutPanel3.Controls.Add(this.label13);
			this.flowLayoutPanel3.Controls.Add(this.asm_calc_iso_label);
			this.flowLayoutPanel3.Controls.Add(this.label17);
			this.flowLayoutPanel3.Controls.Add(this.ok_button);
			this.flowLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
			this.flowLayoutPanel3.Location = new System.Drawing.Point(0, 547);
			this.flowLayoutPanel3.Name = "flowLayoutPanel3";
			this.flowLayoutPanel3.Size = new System.Drawing.Size(702, 241);
			this.flowLayoutPanel3.TabIndex = 3;
			// 
			// label7
			// 
			this.label7.Font = new System.Drawing.Font("PMingLiU", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
			this.label7.ForeColor = System.Drawing.Color.Black;
			this.label7.Location = new System.Drawing.Point(3, 8);
			this.label7.Margin = new System.Windows.Forms.Padding(3, 8, 3, 0);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(133, 23);
			this.label7.TabIndex = 1;
			this.label7.Text = "組裝方式";
			// 
			// assemblyName_textBox
			// 
			this.assemblyName_textBox.Location = new System.Drawing.Point(142, 3);
			this.assemblyName_textBox.Name = "assemblyName_textBox";
			this.assemblyName_textBox.ReadOnly = true;
			this.assemblyName_textBox.Size = new System.Drawing.Size(446, 25);
			this.assemblyName_textBox.TabIndex = 2;
			// 
			// label8
			// 
			this.label8.Font = new System.Drawing.Font("PMingLiU", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
			this.label8.ForeColor = System.Drawing.Color.Black;
			this.label8.Location = new System.Drawing.Point(3, 39);
			this.label8.Margin = new System.Windows.Forms.Padding(3, 8, 3, 0);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(133, 23);
			this.label8.TabIndex = 3;
			this.label8.Text = "組裝水足跡";
			// 
			// Label27
			// 
			this.Label27.Font = new System.Drawing.Font("PMingLiU", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
			this.Label27.ForeColor = System.Drawing.Color.Purple;
			this.Label27.Location = new System.Drawing.Point(142, 41);
			this.Label27.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
			this.Label27.Name = "Label27";
			this.Label27.Size = new System.Drawing.Size(93, 18);
			this.Label27.TabIndex = 34;
			this.Label27.Text = "電力消耗量";
			this.Label27.TextAlign = System.Drawing.ContentAlignment.TopRight;
			// 
			// assembly_WFP_textBox
			// 
			this.assembly_WFP_textBox.Location = new System.Drawing.Point(241, 34);
			this.assembly_WFP_textBox.Name = "assembly_WFP_textBox";
			this.assembly_WFP_textBox.ReadOnly = true;
			this.assembly_WFP_textBox.Size = new System.Drawing.Size(347, 25);
			this.assembly_WFP_textBox.TabIndex = 36;
			// 
			// label14
			// 
			this.label14.AutoSize = true;
			this.label14.Font = new System.Drawing.Font("PMingLiU", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
			this.label14.ForeColor = System.Drawing.Color.Black;
			this.label14.Location = new System.Drawing.Point(594, 39);
			this.label14.Margin = new System.Windows.Forms.Padding(3, 8, 3, 0);
			this.label14.Name = "label14";
			this.label14.Size = new System.Drawing.Size(27, 15);
			this.label14.TabIndex = 46;
			this.label14.Text = "Kw";
			// 
			// Label28
			// 
			this.Label28.Font = new System.Drawing.Font("PMingLiU", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
			this.Label28.ForeColor = System.Drawing.Color.Black;
			this.Label28.Location = new System.Drawing.Point(3, 72);
			this.Label28.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
			this.Label28.Name = "Label28";
			this.Label28.Size = new System.Drawing.Size(232, 18);
			this.Label28.TabIndex = 35;
			this.Label28.Text = "電力系數";
			this.Label28.TextAlign = System.Drawing.ContentAlignment.TopRight;
			// 
			// assembly_ISO_textBox
			// 
			this.assembly_ISO_textBox.Location = new System.Drawing.Point(241, 65);
			this.assembly_ISO_textBox.Name = "assembly_ISO_textBox";
			this.assembly_ISO_textBox.ReadOnly = true;
			this.assembly_ISO_textBox.Size = new System.Drawing.Size(347, 25);
			this.assembly_ISO_textBox.TabIndex = 37;
			// 
			// label15
			// 
			this.label15.AutoSize = true;
			this.label15.Font = new System.Drawing.Font("PMingLiU", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
			this.label15.ForeColor = System.Drawing.Color.Black;
			this.label15.Location = new System.Drawing.Point(594, 70);
			this.label15.Margin = new System.Windows.Forms.Padding(3, 8, 3, 0);
			this.label15.Name = "label15";
			this.label15.Size = new System.Drawing.Size(83, 15);
			this.label15.TabIndex = 47;
			this.label15.Text = "m^3/Kw．hr";
			// 
			// label9
			// 
			this.label9.Font = new System.Drawing.Font("PMingLiU", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
			this.label9.ForeColor = System.Drawing.Color.OrangeRed;
			this.label9.Location = new System.Drawing.Point(3, 101);
			this.label9.Margin = new System.Windows.Forms.Padding(3, 8, 3, 0);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(133, 23);
			this.label9.TabIndex = 38;
			this.label9.Text = "組裝時數";
			// 
			// hour_textBox
			// 
			this.hour_textBox.Location = new System.Drawing.Point(142, 96);
			this.hour_textBox.Name = "hour_textBox";
			this.hour_textBox.Size = new System.Drawing.Size(446, 25);
			this.hour_textBox.TabIndex = 39;
			this.hour_textBox.TextChanged += new System.EventHandler(this.hour_textBox_TextChanged);
			// 
			// label10
			// 
			this.label10.Font = new System.Drawing.Font("PMingLiU", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
			this.label10.ForeColor = System.Drawing.Color.Black;
			this.label10.Location = new System.Drawing.Point(594, 101);
			this.label10.Margin = new System.Windows.Forms.Padding(3, 8, 3, 0);
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size(104, 23);
			this.label10.TabIndex = 40;
			this.label10.Text = "hr";
			// 
			// label11
			// 
			this.label11.Font = new System.Drawing.Font("PMingLiU", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
			this.label11.ForeColor = System.Drawing.Color.Black;
			this.label11.Location = new System.Drawing.Point(3, 132);
			this.label11.Margin = new System.Windows.Forms.Padding(3, 8, 3, 0);
			this.label11.Name = "label11";
			this.label11.Size = new System.Drawing.Size(133, 23);
			this.label11.TabIndex = 41;
			this.label11.Text = "當前組裝水足跡";
			// 
			// label12
			// 
			this.label12.Font = new System.Drawing.Font("PMingLiU", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
			this.label12.ForeColor = System.Drawing.Color.Purple;
			this.label12.Location = new System.Drawing.Point(142, 134);
			this.label12.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
			this.label12.Name = "label12";
			this.label12.Size = new System.Drawing.Size(78, 18);
			this.label12.TabIndex = 42;
			this.label12.Text = "WFN";
			// 
			// asm_calc_wfn_label
			// 
			this.asm_calc_wfn_label.Font = new System.Drawing.Font("PMingLiU", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
			this.asm_calc_wfn_label.ForeColor = System.Drawing.Color.Blue;
			this.asm_calc_wfn_label.Location = new System.Drawing.Point(226, 134);
			this.asm_calc_wfn_label.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
			this.asm_calc_wfn_label.Name = "asm_calc_wfn_label";
			this.asm_calc_wfn_label.Size = new System.Drawing.Size(362, 18);
			this.asm_calc_wfn_label.TabIndex = 44;
			this.asm_calc_wfn_label.Text = "N/A";
			// 
			// label16
			// 
			this.label16.AutoSize = true;
			this.label16.Location = new System.Drawing.Point(594, 134);
			this.label16.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
			this.label16.Name = "label16";
			this.label16.Size = new System.Drawing.Size(32, 15);
			this.label16.TabIndex = 48;
			this.label16.Text = "m^3";
			// 
			// label13
			// 
			this.label13.Font = new System.Drawing.Font("PMingLiU", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
			this.label13.ForeColor = System.Drawing.Color.Black;
			this.label13.Location = new System.Drawing.Point(3, 165);
			this.label13.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
			this.label13.Name = "label13";
			this.label13.Size = new System.Drawing.Size(217, 18);
			this.label13.TabIndex = 43;
			this.label13.Text = "ISO 14046";
			this.label13.TextAlign = System.Drawing.ContentAlignment.TopRight;
			// 
			// asm_calc_iso_label
			// 
			this.asm_calc_iso_label.Font = new System.Drawing.Font("PMingLiU", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
			this.asm_calc_iso_label.ForeColor = System.Drawing.Color.Blue;
			this.asm_calc_iso_label.Location = new System.Drawing.Point(226, 165);
			this.asm_calc_iso_label.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
			this.asm_calc_iso_label.Name = "asm_calc_iso_label";
			this.asm_calc_iso_label.Size = new System.Drawing.Size(362, 18);
			this.asm_calc_iso_label.TabIndex = 45;
			this.asm_calc_iso_label.Text = "N/A";
			// 
			// label17
			// 
			this.label17.AutoSize = true;
			this.label17.Location = new System.Drawing.Point(594, 165);
			this.label17.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
			this.label17.Name = "label17";
			this.label17.Size = new System.Drawing.Size(32, 15);
			this.label17.TabIndex = 49;
			this.label17.Text = "m^3";
			// 
			// ok_button
			// 
			this.ok_button.Location = new System.Drawing.Point(3, 203);
			this.ok_button.Margin = new System.Windows.Forms.Padding(3, 20, 3, 3);
			this.ok_button.Name = "ok_button";
			this.ok_button.Size = new System.Drawing.Size(696, 32);
			this.ok_button.TabIndex = 4;
			this.ok_button.Text = "完成";
			this.ok_button.UseVisualStyleBackColor = true;
			this.ok_button.Click += new System.EventHandler(this.ok_button_Click);
			// 
			// label18
			// 
			this.label18.ForeColor = System.Drawing.Color.Red;
			this.label18.Location = new System.Drawing.Point(3, 32);
			this.label18.Name = "label18";
			this.label18.Size = new System.Drawing.Size(550, 23);
			this.label18.TabIndex = 3;
			this.label18.Text = "** 最大長度限制為 255 個字元，目前長度為 : ";
			this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// cNameLenCountLabel
			// 
			this.cNameLenCountLabel.ForeColor = System.Drawing.Color.Magenta;
			this.cNameLenCountLabel.Location = new System.Drawing.Point(559, 32);
			this.cNameLenCountLabel.Name = "cNameLenCountLabel";
			this.cNameLenCountLabel.Size = new System.Drawing.Size(103, 23);
			this.cNameLenCountLabel.TabIndex = 4;
			this.cNameLenCountLabel.Text = "N/A";
			this.cNameLenCountLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// AssemblyForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(702, 788);
			this.Controls.Add(this.flowLayoutPanel3);
			this.Controls.Add(this.groupBox3);
			this.Controls.Add(this.groupBox2);
			this.Controls.Add(this.groupBox1);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "AssemblyForm";
			this.Text = "組裝";
			this.Load += new System.EventHandler(this.AssemblyForm_Load);
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			this.groupBox2.ResumeLayout(false);
			this.flowLayoutPanel2.ResumeLayout(false);
			this.flowLayoutPanel2.PerformLayout();
			this.groupBox3.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.assemblyMethod_dataGridView)).EndInit();
			this.flowLayoutPanel1.ResumeLayout(false);
			this.flowLayoutPanel1.PerformLayout();
			this.flowLayoutPanel3.ResumeLayout(false);
			this.flowLayoutPanel3.PerformLayout();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.Label c2_label;
		private System.Windows.Forms.Label c1_label;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.TextBox mergeName_textBox;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.GroupBox groupBox3;
		private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.TextBox NameFilter_TextBox;
		private System.Windows.Forms.Button CleanFilter_Button;
		private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.ComboBox RecentUsed_ComboBox;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label Filter_Info_Label;
		private System.Windows.Forms.DataGridView assemblyMethod_dataGridView;
		private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.TextBox assemblyName_textBox;
		private System.Windows.Forms.Label label8;
		internal System.Windows.Forms.Label Label27;
		internal System.Windows.Forms.Label Label28;
		private System.Windows.Forms.TextBox assembly_WFP_textBox;
		private System.Windows.Forms.TextBox assembly_ISO_textBox;
		private System.Windows.Forms.Button ok_button;
		private System.Windows.Forms.Button smart_merg_name_button;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.TextBox hour_textBox;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.Label label11;
		internal System.Windows.Forms.Label label12;
		private System.Windows.Forms.Label asm_calc_wfn_label;
		internal System.Windows.Forms.Label label13;
		private System.Windows.Forms.Label asm_calc_iso_label;
		private System.Windows.Forms.Label label14;
		private System.Windows.Forms.Label label15;
		private System.Windows.Forms.Label label16;
		private System.Windows.Forms.Label label17;
		private System.Windows.Forms.Label label18;
		private System.Windows.Forms.Label cNameLenCountLabel;
	}
}