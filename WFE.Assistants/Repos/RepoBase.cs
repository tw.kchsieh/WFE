﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFE.Assistants.Repos
{
	public class RepoBase
	{
		protected string Conn;
		protected string ErrMsg;
		public RepoBase()
		{
			var config = ConfigurationManager.AppSettings["settings"] ?? "";
			if (!String.IsNullOrEmpty(config))
			{
				var repo = new ConfigRepo(config);
				if (repo.IsValid)
				{
					Conn = repo.GetDBConnectionString();
				}
			}
		}

		/// <summary>
		/// 取得錯誤訊息
		/// </summary>
		/// <returns></returns>
		public virtual string GetErrMsg()
		{
			return ErrMsg;
		}
	}
}
