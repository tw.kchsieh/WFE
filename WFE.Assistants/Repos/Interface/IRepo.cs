﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFE.Assistants.Repos
{
	interface IRepo
	{
		/// <summary>
		/// 取得所有資料
		/// </summary>
		/// <returns></returns>
		object GetData();

		/// <summary>
		/// 取得單一筆資料
		/// </summary>
		/// <param name="sn"></param>
		/// <returns></returns>
		object GetItem(int sn);

		/// <summary>
		/// 取得最近使用的資料 - All
		/// </summary>
		/// <returns></returns>
		object GetRecentUsedData();

		/// <summary>
		/// 取得最近使用的資料 - 單筆
		/// </summary>
		/// <param name="sn"></param>
		/// <returns></returns>
		object GetRecentUsedItem(int sn);

		/// <summary>
		/// 更新最近使用的資料
		/// </summary>
		/// <param name="sn"></param>
		/// <returns></returns>
		bool UpdateRecentUsedItem(int sn);

		/// <summary>
		/// 新增最近使用的資料
		/// </summary>
		/// <param name="sn"></param>
		/// <returns></returns>
		bool AddRecentUsedItem(int sn);
		
	}
}
