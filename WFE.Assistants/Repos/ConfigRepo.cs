﻿using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace WFE.Assistants.Repos
{
	/// <summary>
	/// 設定檔
	/// </summary>
	public class ConfigRepo
	{
		private XDocument _xDoc;
		private XElement _root;

		private bool _valid;
		private string _settingPath;
		private string _ErrMsg;

		public string GetPath()
		{
			return _settingPath;
		}

		//
		private string _provider = "Microsoft.Jet.OLEDB.4.0";

		public ConfigRepo(string settingFilepath)
		{
			var execLoc = Assembly.GetExecutingAssembly().Location;
			var execDir = Path.GetDirectoryName(execLoc);
			_settingPath = Path.Combine(execDir, settingFilepath);
			loadDoc();
		}

		/// <summary>
		/// PrecisionNumber
		/// <para>計算精度，預設為 6</para>
		/// </summary>
		public int PrecisionNumber
		{
			get
			{
				var defValue = 6;
				var settingVal = defValue;
				if (_valid)
				{
					var elem = _root.Element("PrecisionNumber");
					if (elem != null)
					{
						if (!Int32.TryParse(elem.Value, out settingVal))
						{
							settingVal = defValue;
						}
					}
				}
				return settingVal;
			}
		}

		/// <summary>
		/// 儲存計算精度
		/// </summary>
		/// <param name="value"></param>
		/// <returns></returns>
		public bool SavePrecisionNumber(string value)
		{
			return saveElementValue("PrecisionNumber", value);
		}

		/// <summary>
		/// NumberDisplayType
		/// </summary>
		public string NumberDisplayType
		{
			get
			{
				var defValue = "F";//Fixed point
				var settingVal = defValue;
				if (_valid)
				{
					var elem = _root.Element("NumberDisplayType");
					if (elem != null)
					{
						settingVal = elem.Value;
					}
				}
				return settingVal;
			}
		}

		public bool SaveNumberDisplayType(string value)
		{
			return saveElementValue("NumberDisplayType", value);
		}

		/// <summary>
		/// 取得數值顯示格式 Pattern
		/// <para>For String.Format(pattern, arg1, arg2,...)</para>
		/// </summary>
		/// <returns></returns>
		public string GetNumberFormatPattern()
		{
			return String.Format("{{0:{0}{1}}}", NumberDisplayType, PrecisionNumber);
		}

		/// <summary>
		/// 設定檔是否正確 
		/// </summary>
		public bool IsValid { get { return _valid; } }

		/// <summary>
		/// 取得資料庫來源
		/// </summary>
		/// <returns></returns>
		public string GetDBSource()
		{
			var source = "";
			if (_valid)
			{
				var elem = _root.Element("MSAccessSource");
				if (elem != null)
				{
					source = elem.Value;
				}
			}
			return source;
		}

		/// <summary>
		/// 設定資料庫來源
		/// </summary>
		/// <param name="filepath"></param>
		/// <returns></returns>
		public bool SetDBSource(string filepath)
		{
			return saveElementValue("MSAccessSource", filepath);
			/*
			var success = false;
			if (_valid)
			{
				var elem = _root.Element("MSAccessSource");
				if (elem != null)
				{
					elem.Value = filepath;
					success = saveXDoc();
				}
			}
			return success;
			*/
		}

		/// <summary>
		/// 目前資料庫連線測試
		/// </summary>
		/// <returns></returns>
		public bool CurrentDBConnectionCheck()
		{
			var dbSource = this.GetDBSource();
			return DBConnectionTest(dbSource);
		}
		/// <summary>
		/// 資料庫連線測試
		/// </summary>
		/// <param name="db_source"></param>
		/// <returns></returns>
		public bool DBConnectionTest(string db_source)
		{
			bool success = false;
			//_provider = "Microsoft.ACE.OLEDB.12.0";
			//_provider = "Microsoft.Jet.OLEDB.4.0";
			var _oleConn = String.Format(@"Provider={0};Data Source={1}", _provider, db_source);
			try
			{
				using (var conn = new OleDbConnection(_oleConn))
				{
					conn.Open();
					success = conn.State == System.Data.ConnectionState.Open;
				}
			}
			catch (Exception ex)
			{
				_ErrMsg = ex.Message;
			}
			return success;
		}

		/// <summary>
		/// 取得 Microsoft Office Access Connection String
		/// </summary>
		/// <returns></returns>
		public string GetDBConnectionString()
		{
			var db_source = GetDBSource();
			return String.Format(@"Provider={0};Data Source={1}", _provider, db_source);
		}

		public string GetErrMsg()
		{
			return _ErrMsg;
		}

		#region private methods
		private bool saveXDoc()
		{
			try
			{
				_xDoc.Save(_settingPath);
				loadDoc();
			}
			catch (Exception ex)
			{
				_ErrMsg = ex.Message;
			}
			return _valid;
		}

		private bool saveElementValue(string elemName, string elemValue)
		{
			var success = false;
			if (_valid)
			{
				var elem = _root.Element(elemName);
				if (elem != null)
				{
					elem.SetValue(elemValue);
					success = saveXDoc();
				}
			}
			return success;
		}

		/// <summary>
		/// 載入檔案
		/// </summary>
		private void loadDoc()
		{
			if (File.Exists(_settingPath))
			{
				try
				{
					_xDoc = XDocument.Load(_settingPath);
					_valid = (_xDoc != null);
					if (_valid)
					{
						_root = _xDoc.Root;
					}
				}
				catch (Exception ex)
				{
					_ErrMsg = ex.Message;
				}
			}
		}
		#endregion
	}
}
