﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFE.Assistants.Models;

namespace WFE.Assistants.Repos
{
	/// <summary>
	/// 加工方式
	/// </summary>
	public class MakeProcessRepo : RepoBase, IRepo
	{
		public object GetData()
		{
			var records = new List<MakeProcess>();
			using (OleDbConnection connection = new OleDbConnection(Conn))
			{
				string condition = "Where [WFP(WFN)] <> null OR PowerCoefficient <> null";
				string sql = String.Format(@"{0} {1}", getDataQueryString(), condition);
				records = connection.Query<MakeProcess>(sql).ToList();
			}
			return records;
		}

		public object GetItem(int sn)
		{
			MakeProcess model = null;
			using (OleDbConnection connection = new OleDbConnection(Conn))
			{
				string condition = "Where SN = @SN";
				string sql = String.Format(@"{0} {1}", getDataQueryString(), condition);
				var param = new { SN = sn };
				model = connection.Query<MakeProcess>(sql, param).SingleOrDefault();
			}
			return model;
		}

		public object GetRecentUsedData()
		{
			var records = new List<RecentUsedDTO>();
			using (OleDbConnection connection = new OleDbConnection(Conn))
			{
				var sql = getRecentUsedDataQueryString();
				records = connection.Query<RecentUsedDTO>(sql).ToList();
			}
			return records;
		}

		public object GetRecentUsedItem(int sn)
		{
			RecentUsedDTO model = null;
			using (OleDbConnection connection = new OleDbConnection(Conn))
			{
				var condition = @"Where ComponentProcessSN = @SN";
				var sql = String.Format(@"SELECT * FROM RecentUsedComponentProcess {0}", condition);
				var parm = new { SN = sn };
				model = connection.Query<RecentUsedDTO>(sql, parm).SingleOrDefault();
			}
			return model;
		}

		public bool AddRecentUsedItem(int sn)
		{
			var success = false;
			using (OleDbConnection connection = new OleDbConnection(Conn))
			{
				var modifiedDate = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
				var parm = new { SN = sn, Now = modifiedDate };
				var sql = @"Insert INTO RecentUsedComponentProcess(ComponentProcessSN, LastUsed, UsedCount) 
																Values(@SN, @Now, 1)";
				try
				{
					var affected = connection.Execute(sql, parm);
					success = affected > 0;
				}
				catch (Exception ex)
				{
					ErrMsg = ex.Message;
				}
			}
			return success;
		}

		public bool UpdateRecentUsedItem(int sn)
		{
			var success = false;
			using (OleDbConnection connection = new OleDbConnection(Conn))
			{
				try
				{
					var modifiedDate = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
					var result = GetRecentUsedItem(sn) as RecentUsedDTO;
					if (result != null)
					{
						//Do Update
						var count = result.UsedCount + 1;
						var updateSQL = @"
					UPDATE RecentUsedComponentProcess
					SET LastUsed = @Now, UsedCount = @Count
					Where ComponentProcessSN = @SN
					";
						var update_param = new { Now = modifiedDate, Count = count, SN = sn, };
						var affected = connection.Execute(updateSQL, update_param);
						success = affected > 0;
					}
					else
					{
						//Do Insert
						success = AddRecentUsedItem(sn);
					}
				}
				catch (Exception ex)
				{
					ErrMsg = ex.Message;
				}
			}
			return success;
		}

		#region private methods
		private string getDataQueryString()
		{
			return @"
SELECT	
		SN, 
		MakeProcessName, 
		CalculateCode,
		[WFP(WFN)] as WFP,
		[WFP(ISO14046)] as ISO,
		[BOD],
		PowerConsumption,
		PowerCoefficient as PowerCOEFF
FROM MakeProcess
";
		}

		private string getRecentUsedDataQueryString()
		{
			return @"
SELECT	RT.ComponentProcessSN as SN,
		RT.LastUsed,
		RT.UsedCount,
		TT.MakeProcessName as Name		
FROM	RecentUsedComponentProcess RT
		INNER JOIN MakeProcess TT ON RT.ComponentProcessSN = TT.SN";
		}
		#endregion
	}
}
