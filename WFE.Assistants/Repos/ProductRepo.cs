﻿using Dapper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFE.Assistants.Helpers;
using WFE.Assistants.Models;
using WFE.Assistants.Models.ORDs;
using WFE.Assistants.Models.SQL;
using WFE.Assistants.Models.ProductCompare;
using WFE.Assistants.Models.AssemblyPageViewModels;

namespace WFE.Assistants.Repos
{
	public class ProductRepo : RepoBase, IRepo
	{
		private NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
		public ProductRepo()
		{

		}

		#region Methods of Using OLE Transaction
		class ProductDTO
		{
			public int SN { get; set; }
			public string ProductName { get; set; }
			public string ProductDesc { get; set; }
		}
		public bool IsProductExists(int sn)
		{
			var tableName = "Product";
			var records = new List<ProductDTO>();
			using (var connection = new OleDbConnection(Conn))
			{
				var sql = String.Format(@"SELECT {0} Where SN = @SN", tableName);
				var param = new { SN = sn };
				records = connection.Query<ProductDTO>(sql, param).ToList();
			}
			return records.Count > 0;
		}
		/// <summary>
		/// 儲存產品
		/// </summary>
		/// <param name="product"></param>
		/// <returns></returns>
		public bool Insert(ProductViewModel product)
		{
			//Insert
			return insertProduct(product);
		}

		/// <summary>
		/// 另存產品
		/// </summary>
		/// <param name="product"></param>
		/// <returns></returns>
		public bool SaveAs(ProductViewModel product)
		{
			/*
			 * 需先變更 Component_AssemblyItem.Component 及 Product_Assembly.ComponentID 
			 * 因為 Key 值重複問題
			 */
			//組裝
			var asmItems = product.AssemblyItems;
			var asmDetails = product.AssemblyDetails;
			for (int idx = 0; idx < asmItems.Count; idx++)
			{
				var item = asmItems[idx];
				var oriID = item.ComponentID;

				//更新自已的 ID
				var nowID = oriID + Guid.NewGuid().ToString().Substring(0, 4);
				item.ComponentID = nowID;

				//更新 Child 的 ParentID
				foreach (var child in asmItems.Where(p => p.ParentID == oriID))
				{
					child.ParentID = item.ComponentID;
				}

				//更新組裝記錄 Detail.ComponentID
				foreach (var detail in asmDetails.Where(p => p.ComponentID == oriID))
				{
					detail.ComponentID = item.ComponentID;
				}
			}
			return insertProduct(product);
		}

		/// <summary>
		/// 更新產品
		/// </summary>
		/// <param name="product"></param>
		/// <returns></returns>
		public bool Update(ProductViewModel product)
		{
			//Update
			return updateProduct(product);
		}

		/// <summary>
		/// 刪除產品
		/// </summary>
		/// <param name="product"></param>
		/// <returns></returns>
		public bool Delete(ProductViewModel product)
		{
			return Delete(product.SN);
		}

		/// <summary>
		/// 刪除產品
		/// </summary>
		/// <param name="productSN"></param>
		/// <returns></returns>
		public bool Delete(int productSN)
		{
			var ole = get_delete_product(productSN);
			var oleDB = new OLEDBHelper();
			var success = oleDB.Execute(ole);
			if (!success)
			{
				this.ErrMsg = oleDB.GetErrMsg();
			}
			return success;
		}

		private bool updateProduct(ProductViewModel product)
		{
			/*
			 先刪除一般/特殊零件，再重新新增
			 */
			bool success = true;

			//var deleteOles = get_delete_all_product_componentsOLE(product.SN);

			//刪除
			var v1 = delete_all_product_components(product.SN);
			if (!v1)
			{
				_logger.Debug("Error v1");
				return v1;
			}

			//新增
			var v2 = insert_all_product_components(product);
			if (!v2)
			{
				_logger.Debug("Error v2");
				return v2;
			}

			//更新 ProductName, ProductDesc
			var v3 = update_product_info(product);
			if (!v3)
			{
				_logger.Debug("Error v3");
				return v3;
			}
			return success;
		}



		private bool update_product_info(ProductViewModel product)
		{
			var oleDB = new OLEDBHelper();
			var productSN = product.SN.ToString();
			var param = new
			{
				ProductName = product.ProductName,
				ProductDesc = product.ProductDesc,
				ModifiedDate = DateTime.Now,
			};
			var updateOle = new OLESQL();
			updateOle.GenerateUpdateSQL("Product", "SN", productSN, param);
			var v1 = oleDB.Execute(updateOle);
			if (!v1)
			{
				ErrMsg = oleDB.GetErrMsg();
			}
			return v1;
		}

		private List<OLESQL> get_delete_all_product_componentsOLE(int productSN)
		{
			List<OLESQL> oles = new List<OLESQL>();
			var param = new
			{
				ProductSN = productSN
			};
			//刪除: 一般零件
			var componentOle = get_delete_model("Product_Component", param);

			//刪除 : 特殊零件
			var specialOle = get_delete_model("Product_SpecialComponent", param);

			oles.Add(componentOle);
			oles.Add(specialOle);
			return oles;
		}

		private bool delete_all_product_components(int productSN)
		{
			var oleDB = new OLEDBHelper();
			var param = new
			{
				ProductSN = productSN
			};

			//刪除: 一般零件
			var componentOle = get_delete_model("Product_Component", param);
			var v1 = oleDB.Execute(componentOle);
			if (!v1)
			{
				ErrMsg = oleDB.GetErrMsg();
				_logger.Debug("刪除: 一般零件 Err" + ErrMsg);
				return v1;
			}

			//刪除 : 特殊零件
			var specialOle = get_delete_model("Product_SpecialComponent", param);
			var v2 = oleDB.Execute(specialOle);
			if (!v2)
			{
				ErrMsg = oleDB.GetErrMsg();
				_logger.Debug("刪除: 特殊零件 Err" + ErrMsg);
				return v2;
			}
			#region === 刪除 : 產品組裝-> 運輸，組裝 ===
			//運輸
			var asmTransOle = get_delete_model("Product_Assembly_Transport", param);
			var v3 = oleDB.Execute(asmTransOle);
			if (!v3)
			{
				ErrMsg = oleDB.GetErrMsg();
				_logger.Debug("刪除: 運輸 Err" + ErrMsg);
				return v3;
			}
			//組裝
			var asmOle = get_delete_model("Component_AssemblyItem", param);
			var v4 = oleDB.Execute(asmOle);
			if (!v4)
			{
				ErrMsg = oleDB.GetErrMsg();
				_logger.Debug("刪除: 組裝 Err" + ErrMsg);
				return v4;
			}
			#endregion

			return true;
		}

		private OLESQL get_delete_model(string tablename, object param)
		{
			var ole = new OLESQL();
			ole.GenerateDeleteSQL(tablename, param);
			return ole;
		}

		private int getInsertProductSN(ProductViewModel product)
		{
			var oleDB = new OLEDBHelper();

			//先取得 Insert 的 Product.SN
			var productOLE = get_insert_product(product);
			int productSN;
			var v1 = oleDB.Execute(productOLE, out productSN);
			if (!v1)
			{
				this.ErrMsg = oleDB.GetErrMsg();
			}
			return productSN;
		}

		private bool insertProduct(ProductViewModel product)
		{
			var oleDB = new OLEDBHelper();

			//先取得 Insert 的 Product.SN
			int productSN = getInsertProductSN(product);
			if (!(productSN > 0))
			{
				return false;
			}
			product.SN = productSN;
			var v1 = insert_all_product_components(product);

			return v1;
		}

		private bool insert_all_product_components(ProductViewModel product)
		{
			var success = true;
			var fail = false;
			var oleDB = new OLEDBHelper();

			#region 一般零件 -> 「運輸」，「加工」，「後製加工」
			var postComponents = product.PostProcessComponents;
			foreach (var component in product.Components)
			{
				var ole = get_Insert_Product_Component(component, product.SN);
				int c_sn;
				var v2 = oleDB.Execute(ole, out c_sn);
				if (!v2)
				{
					this.ErrMsg = oleDB.GetErrMsg();
					return fail;
				}
				if (v2 && c_sn > 0)
				{
					#region 一般零件: 運輸
					component.Component_SN = c_sn;
					foreach (var transport in component.ComponentTransports)
					{
						var oleTransport = get_Insert_Component_Transport(transport, component.Component_SN);
						int t_sn;
						var v3 = oleDB.Execute(oleTransport, out t_sn);
						if (!v3)
						{
							this.ErrMsg = oleDB.GetErrMsg();
							return fail;
						}
						if (v3 && t_sn > 0)
						{
							transport.SN = t_sn;
						}
					}
					#endregion

					#region 一般零件: 加工
					//加工
					foreach (var mkProcess in component.ComponentProcesses)
					{
						var oleProcess = get_Insert_Component_Process(mkProcess, component.Component_SN);
						int mk_sn;
						var v4 = oleDB.Execute(oleProcess, out mk_sn);
						if (!v4)
						{
							this.ErrMsg = oleDB.GetErrMsg();
							return fail;
						}
						if (v4 && mk_sn > 0)
						{
							mkProcess.SN = mk_sn;
						}
					}
					#endregion

					var postComponent = postComponents.SingleOrDefault(p => p.Component_Name == component.Component_Name);
					if (postComponent != null)
					{
						postComponent.Component_SN = component.Component_SN;
					}
				}
			}//endforeach Product_Components (一般零件)
			#endregion

			#region 後製加工 -> 「運輸」，「加工」

			foreach (var postCom in postComponents.Where(p => p.Component_SN > 0))
			{
				var postOLE = get_Insert_Component_PostProcess(postCom.Component_SN);
				int post_sn;
				var v5 = oleDB.Execute(postOLE, out post_sn);
				if (!v5)
				{
					this.ErrMsg = oleDB.GetErrMsg();
					return fail;
				}
				if (v5 && post_sn > 0)
				{
					postCom.PostComponent_SN = post_sn;
					//後製加工: 運輸
					foreach (var postTransport in postCom.ComponentTransports)
					{
						var postTransportOLE = get_Insert_Component_PostProcess_Transport(postTransport, post_sn);
						int post_t_sn;
						var v6 = oleDB.Execute(postTransportOLE, out post_t_sn);
						if (!v6)
						{
							this.ErrMsg = oleDB.GetErrMsg();
							return fail;
						}
						if (v6 && post_t_sn > 0)
						{
							postTransport.SN = post_t_sn;
						}
					}

					//後製加工: 加工
					foreach (var postProcess in postCom.ComponentProcesses)
					{
						var postProcessOLE = get_Insert_Component_PostProcess_Process(postProcess, post_sn);
						int post_p_sn;
						var v7 = oleDB.Execute(postProcessOLE, out post_p_sn);
						if (!v7)
						{
							this.ErrMsg = oleDB.GetErrMsg();
							return fail;
						}
						if (v7 && post_p_sn > 0)
						{
							postProcess.SN = post_p_sn;
						}
					}
				}
			}//endforeach
			#endregion

			#region 特殊零件
			foreach (var special in product.SpecialComponents)
			{
				var specialOLE = get_Insert_Product_SpecialComponent(special, product.SN);
				int special_sn;
				var v8 = oleDB.Execute(specialOLE, out special_sn);
				if (!v8)
				{
					this.ErrMsg = oleDB.GetErrMsg();
					return fail;
				}
				if (v8 && special_sn > 0)
				{
					special.SN = special_sn;
				}
			}//endforeach
			#endregion

			#region 產品組裝 -> 「運輸」，「組裝」
			//運輸
			foreach (var asmTransport in product.AssemblyTransports)
			{
				var asmTransportOLE = get_Insert_Product_Assembly_Transport(asmTransport, product.SN, asmTransport.C_Name);
				int asm_tran_sn;
				var valid = oleDB.Execute(asmTransportOLE, out asm_tran_sn);
				if (!valid)
				{
					this.ErrMsg = oleDB.GetErrMsg();
					return fail;
				}
				if (valid && asm_tran_sn > 0)
				{
					asmTransport.SN = asm_tran_sn;
				}
			}
			//組裝
			foreach (var asmItem in product.AssemblyItems)
			{
				var asmItemOLE = get_Insert_Component_AssemblyItem(asmItem, product.SN);
				int asm_sn;
				var valid = oleDB.Execute(asmItemOLE, out asm_sn);
				if (!valid)
				{
					this.ErrMsg = oleDB.GetErrMsg();
					return fail;
				}
				if (valid && asm_sn > 0)
				{
					asmItem.SN = asm_sn;
				}
			}
			//組裝記錄 Detail
			foreach (var asmDetail in product.AssemblyDetails)
			{
				var asmDetailOLE = get_Insert_Product_Assembly(asmDetail);
				int asm_detail_sn;
				var valid = oleDB.Execute(asmDetailOLE, out asm_detail_sn);
				if (!valid)
				{
					this.ErrMsg = oleDB.GetErrMsg();
					return fail;
				}
				if (valid && asm_detail_sn > 0)
				{
					asmDetail.SN = asm_detail_sn;
				}
			}
			#endregion

			return success;
		}

		private OLESQL get_Insert_Product_Assembly(Component_AssemblyDetail item)
		{
			//產品組裝 :Detail
			var ole = new OLESQL();
			var oleParam = new
			{
				ComponentID = item.ComponentID,
				AssemblyMethodSN = item.AssemblyMethodSN,
				WorkingHour = item.WorkingHour,
			};
			ole.GenerateInsertSQL("Product_Assembly", oleParam);
			return ole;
		}

		private OLESQL get_Insert_Product_Assembly_Transport(ComponentTransport item, int productSN, string componentName)
		{
			//產品組裝 : 運輸
			var ole = new OLESQL();
			var oleParam = new
			{
				ProductSN = productSN,
				ComponentName = componentName,
				ComponentMass = item.Component_Mass,
				TransportSN = item.TransportSN,
				Distance = item.Distance,
			};
			ole.GenerateInsertSQL("Product_Assembly_Transport", oleParam);
			return ole;
		}

		private OLESQL get_Insert_Component_AssemblyItem(Component_AssemblyItem item, int productSN)
		{
			//產品組裝
			var ole = new OLESQL();
			var oleParam = new
			{
				ProductSN = productSN,
				ComponentName = item.ComponentName,
				ComponentID = item.ComponentID,
				ParentID = item.ParentID,
			};
			ole.GenerateInsertSQL("Component_AssemblyItem", oleParam);
			return ole;
		}

		private OLESQL get_delete_product(ProductViewModel product)
		{
			return get_delete_product(product.SN);
		}
		private OLESQL get_delete_product(int productSN)
		{
			var ole = new OLESQL();
			var param = new
			{
				SN = productSN
			};
			ole.GenerateDeleteSQL("Product", param);
			return ole;
		}

		private OLESQL get_insert_product(ProductViewModel product)
		{
			var ole = new OLESQL();
			var param = new
			{
				ProductName = product.ProductName,
				ProductDesc = product.ProductDesc,
			};
			ole.GenerateInsertSQL("Product", param);
			return ole;
		}

		/// <summary>
		/// 後製加工 : 加工
		/// </summary>
		/// <param name="postComponent"></param>
		/// <returns></returns>
		[Obsolete("NotUsed")]
		private List<OLESQL> get_Insert_Component_PostProcess_Process(ProductComponent postComponent)
		{
			//後製加工 : 加工
			var oleSQLs = new List<OLESQL>();
			foreach (var item in postComponent.ComponentProcesses)
			{
				var ole = new OLESQL();
				var oleParam = new
				{
					ComponentPostProcessSN = item.C_SN,
					ComponentProcessSN = item.MakeProcessSN,
					ComponentMass = item.ComponentMass,
					WorkingHour = item.WorkingHour,
				};
				ole.GenerateInsertSQL("Component_PostProcess_Process", oleParam);
				oleSQLs.Add(ole);
			}
			return oleSQLs;
		}
		private OLESQL get_Insert_Component_PostProcess_Process(ComponentProcess item, int postSN)
		{
			//後製加工 : 加工
			var ole = new OLESQL();
			var oleParam = new
			{
				ComponentPostProcessSN = postSN,
				ComponentProcessSN = item.MakeProcessSN,
				ComponentMass = item.ComponentMass,
				WorkingHour = item.WorkingHour,
			};
			ole.GenerateInsertSQL("Component_PostProcess_Process", oleParam);
			return ole;
		}

		/// <summary>
		/// 後製加工 : 運輸
		/// </summary>
		/// <param name="component"></param>
		/// <returns></returns>
		[Obsolete("NotUsed")]
		private List<OLESQL> get_Insert_Component_PostProcess_Transport(ProductComponent postComponent)
		{
			//後製加工 : 運輸
			var oleSQLs = new List<OLESQL>();
			foreach (var item in postComponent.ComponentTransports)
			{
				var ole = new OLESQL();
				var oleParam = new
				{
					ComponentPostProcessSN = item.C_SN,
					TransportSN = item.TransportSN,
					Distance = item.Distance,
				};
				ole.GenerateInsertSQL("Component_PostProcess_Transport", oleParam);
				oleSQLs.Add(ole);
			}
			return oleSQLs;
		}
		private OLESQL get_Insert_Component_PostProcess_Transport(ComponentTransport item, int postSN)
		{
			//後製加工 : 運輸
			var ole = new OLESQL();
			var oleParam = new
			{
				ComponentPostProcessSN = postSN,
				TransportSN = item.TransportSN,
				Distance = item.Distance,
			};
			ole.GenerateInsertSQL("Component_PostProcess_Transport", oleParam);
			return ole;
		}

		/// <summary>
		/// 後製加工
		/// </summary>
		/// <param name="product"></param>
		/// <returns></returns>
		[Obsolete("NotUsed")]
		private List<OLESQL> get_Insert_Component_PostProcess(ProductViewModel product)
		{
			//後製加工
			var oleSQLs = new List<OLESQL>();
			foreach (var item in product.PostProcessComponents)
			{
				var ole = new OLESQL();
				var oleParam = new
				{
					ComponentSN = item.Component_SN,
				};
				ole.GenerateInsertSQL("Component_PostProcess", oleParam);
				oleSQLs.Add(ole);
			}
			return oleSQLs;
		}
		private OLESQL get_Insert_Component_PostProcess(int componentSN)
		{
			var ole = new OLESQL();
			var oleParam = new
			{
				ComponentSN = componentSN,
			};
			ole.GenerateInsertSQL("Component_PostProcess", oleParam);
			return ole;
		}

		/// <summary>
		/// 一般零件 : 加工記錄
		/// </summary>
		/// <param name="component"></param>
		/// <returns></returns>
		[Obsolete("NotUsed")]
		private List<OLESQL> get_Insert_Component_Process(ProductComponent component)
		{
			//一般零件 : 加工記錄
			var oleSQLs = new List<OLESQL>();
			foreach (var item in component.ComponentProcesses)
			{
				var ole = get_Insert_Component_Process(item, component.Component_SN);
				oleSQLs.Add(ole);
			}
			return oleSQLs;
		}
		/// <summary>
		/// //一般零件 : 加工記錄
		/// </summary>
		/// <param name="item"></param>
		/// <param name="componentSN"></param>
		/// <returns></returns>
		private OLESQL get_Insert_Component_Process(ComponentProcess item, int componentSN)
		{
			var ole = new OLESQL();
			var oleParam = new
			{
				ComponentSN = componentSN,
				ComponentProcessSN = item.MakeProcessSN,
				ComponentMass = item.ComponentMass,
				WorkingHour = item.WorkingHour,
			};
			ole.GenerateInsertSQL("Component_Process", oleParam);
			return ole;
		}


		/// <summary>
		/// 一般零件 : 運輸記錄
		/// </summary>
		/// <param name="component"></param>
		/// <returns></returns>
		[Obsolete("NotUsed")]
		private List<OLESQL> get_Insert_Component_Transport(ProductComponent component)
		{
			//一般零件 : 運輸記錄
			var oleSQLs = new List<OLESQL>();
			foreach (var item in component.ComponentTransports)
			{
				var ole = get_Insert_Component_Transport(item, component.Component_SN);
				oleSQLs.Add(ole);
			}
			return oleSQLs;
		}
		private OLESQL get_Insert_Component_Transport(ComponentTransport item, int componentSN)
		{
			var ole = new OLESQL();
			var oleParam = new
			{
				ComponentSN = componentSN,
				TransportSN = item.TransportSN,
				Distance = item.Distance,
			};
			ole.GenerateInsertSQL("Component_Transport", oleParam);
			return ole;
		}

		private OLESQL get_Insert_Product_Component(ProductComponent item, int productSN)
		{
			OLESQL ole = new OLESQL();
			var oleParam = new
			{
				ProductSN = productSN,
				MaterialSN = item.Material_SN,
				ComponentName = item.Component_Name,
				Volumn = item.Component_Volumn,
				CreateDate = item.CreateDate
			};
			ole.GenerateInsertSQL("Product_Component", oleParam);
			return ole;
		}

		private OLESQL get_Insert_Product_SpecialComponent(ProductSpecialComponent item, int productSN)
		{
			var ole = new OLESQL();
			var oleParam = new
			{
				ProductSN = productSN,
				ManufactureProcedureSN = item.ManufactureProcedureSN,
				ComponentName = item.Component_Name,
				Area = item.Component_Area,
				Volumn = item.Component_Volumn,
				CreateDate = item.CreateDate,
			};
			ole.GenerateInsertSQL("Product_SpecialComponent", oleParam);
			return ole;
		}

		#endregion

		#region =========== Methods of Using Dapper Library  =================
		public List<ProductRecord> GetUnChoicedProduct(int[] chooseProductSNs)
		{
			var products = GetDataExclude(chooseProductSNs) as List<ProductRecord>;
			return products;
		}

		/// <summary>
		/// 取得產品資訊
		/// </summary>
		/// <param name="productSN"></param>
		/// <returns></returns>
		public ProductViewModel GetProduct(int productSN)
		{
			ProductViewModel product = new ProductViewModel();
			using (var connection = new OleDbConnection(Conn))
			{
				var sql = @"SELECT SN, ProductName, ProductDesc, CreateDate FROM Product Where SN = @SN";
				var param = new { SN = productSN };
				product = connection.Query<ProductViewModel>(sql, param).SingleOrDefault();
				if (product != null)
				{
					//一般零件
					product.Components = GetProductComponents(productSN);

					//後製加工零件
					if (product.Components != null && product.Components.Count > 0)
					{
						var component_SNS = product.Components.Select(p => p.Component_SN).ToArray();
						var componentCreateDate_Dict = product.Components.ToDictionary(k => k.Component_SN, v => v.CreateDate);
						product.PostProcessComponents = GetPostComponents(component_SNS);
						foreach (var postComponent in product.PostProcessComponents)
						{
							if (componentCreateDate_Dict.ContainsKey(postComponent.Component_SN))
							{
								postComponent.CreateDate = componentCreateDate_Dict[postComponent.Component_SN];
							}
						}
					}
					//特殊零件
					product.SpecialComponents = GetSpecialComponents(productSN);

					#region === 產品組裝 => 運輸，組裝 ===

					//產品組裝運輸
					product.AssemblyTransports = Get_Asm_Transports(productSN);

					//產品組裝清單
					product.AssemblyItems = Get_Asm_Item(productSN);

					//產品組裝細節
					//var asmItemIDs = product.AssemblyItems.Select(p => p.ComponentID).ToArray();
					product.AssemblyDetails = Get_Asm_Detail(productSN);
					#endregion
				}
			}
			return product;
		}
		#region  === Assembly ===
		private List<ComponentTransport> Get_Asm_Transports(int productSN)
		{
			var tableName = "Product_Assembly_Transport_View";
			var records = new List<ComponentTransport>();
			using (var connection = new OleDbConnection(Conn))
			{
				var sql = String.Format(@"SELECT * From {0} Where ProductSN = @SN ", tableName);
				var param = new { SN = productSN };
				records = connection.Query<ComponentTransport>(sql, param).ToList();
			}
			return records;
		}

		private List<Component_AssemblyItem> Get_Asm_Item(int productSN)
		{
			var tableName = "Component_AssemblyItem";
			var records = new List<Component_AssemblyItem>();
			using (var connection = new OleDbConnection(Conn))
			{
				var sql = String.Format(@"SELECT * From {0} Where ProductSN = @SN", tableName);
				var param = new { SN = productSN };
				records = connection.Query<Component_AssemblyItem>(sql, param).ToList();
			}
			return records;
		}

		private List<Component_AssemblyDetail> Get_Asm_Detail(int productSN)
		{
			var tableName = "Component_AssemblyDetail_View";
			var records = new List<Component_AssemblyDetail>();
			using (var connection = new OleDbConnection(Conn))
			{
				var sql = String.Format(@"SELECT * From {0} Where ProductSN = @SN", tableName);
				var param = new { SN = productSN };
				records = connection.Query<Component_AssemblyDetail>(sql, param).ToList();
			}
			return records;
		}

		#endregion

		public List<ProductSpecialComponent> GetSpecialComponents(int productSN)
		{
			var spComponents = new List<ProductSpecialComponent>();
			using (var connection = new OleDbConnection(Conn))
			{
				var sql = String.Format("{0} Where Product_SN = @SN", get_query_product_specialComponent_view());
				var param = new { SN = productSN };
				spComponents = connection.Query<ProductSpecialComponent>(sql, param).ToList();
			}
			return spComponents;
		}

		/// <summary>
		/// 取得後製加工零件，包含其子項目(運輸，加工記錄)
		/// </summary>
		/// <param name="c_SNS"></param>
		/// <returns></returns>
		public List<ProductComponent> GetPostComponents(int[] c_SNS)
		{
			//後製加工零件
			var records = new List<ProductComponent>();
			using (var connection = new OleDbConnection(Conn))
			{
				var sql = String.Format("{0} Where Component_SN IN @SNS", get_query_postComponent_view());
				var param = new { SNS = c_SNS };
				records = connection.Query<ProductComponent>(sql, param).ToList();

				//後製加工零件:運輸, 加工
				foreach (var record in records)
				{
					var postComponentSN = record.PostComponent_SN;
					var component_Name = record.Component_Name;
					var component_Mass = record.Mass;

					//運輸
					record.ComponentTransports = GetPostComponentTransports(postComponentSN, component_Name, component_Mass);

					//加工
					record.ComponentProcesses = GetPostComponentProcess(postComponentSN, component_Name, component_Mass);
				}
			}
			return records;
		}

		/// <summary>
		/// 取得後製加工零件: 加工記錄
		/// </summary>
		/// <param name="component_SN"></param>
		/// <param name="component_Name"></param>
		/// <param name="component_Mass"></param>
		/// <returns></returns>
		public List<ComponentProcess> GetPostComponentProcess(int component_SN, string component_Name, string component_Mass)
		{
			var records = new List<ComponentProcess>();
			using (var connection = new OleDbConnection(Conn))
			{
				var sql = String.Format("{0} Where C_SN = @SN", get_query_postComponent_makeProcess_view());
				var param = new { SN = component_SN };
				records = connection.Query<ComponentProcess>(sql, param).ToList();

				//assign name and mass
				foreach (var record in records)
				{
					record.C_Name = component_Name;
					record.ComponentMass = component_Mass;
				}
			}
			return records;
		}

		/// <summary>
		/// 取得後製加工零件: 運輸記錄
		/// </summary>
		/// <param name="component_SN"></param>
		/// <param name="component_Name"></param>
		/// <param name="component_Mass"></param>
		/// <returns></returns>
		public List<ComponentTransport> GetPostComponentTransports(int component_SN, string component_Name, string component_Mass)
		{
			var records = new List<ComponentTransport>();
			using (var connection = new OleDbConnection(Conn))
			{
				var sql = String.Format("{0} Where Component_SN = @SN", get_query_postComponent_transport_view());
				var param = new { SN = component_SN };
				records = connection.Query<ComponentTransport>(sql, param).ToList();

				//assign name and mass
				foreach (var record in records)
				{
					record.C_Name = component_Name;
					record.Component_Mass = component_Mass;
				}
			}
			return records;
		}

		/// <summary>
		/// 取得一般零件 : 包含其子項目 (運輸、加工)
		/// </summary>
		/// <param name="productSN"></param>
		/// <returns></returns>
		public List<ProductComponent> GetProductComponents(int productSN)
		{
			//assign
			var records = new List<ProductComponent>();
			using (var connection = new OleDbConnection(Conn))
			{
				var sql = String.Format("{0} Where Product_SN = @SN", get_query_product_component_view());
				var param = new { SN = productSN };
				records = connection.Query<ProductComponent>(sql, param).ToList();

				//一般零件:運輸, 加工
				foreach (var record in records)
				{
					var c_sn = record.Component_SN;
					var c_mass = record.Mass;
					var c_name = record.Component_Name;

					//運輸
					record.ComponentTransports = GetComponentTransports(c_sn, c_mass, c_name);

					//加工
					record.ComponentProcesses = GetComponentProcesses(c_sn, c_mass, c_name);
				}
			}
			return records;
		}

		/// <summary>
		/// 一般零件:運輸
		/// </summary>
		/// <param name="component_SN"></param>
		/// <param name="component_Mass"></param>
		/// <param name="component_Name"></param>
		/// <returns></returns>
		public List<ComponentTransport> GetComponentTransports(int component_SN,
			string component_Mass, string component_Name)
		{
			var records = new List<ComponentTransport>();
			using (var connection = new OleDbConnection(Conn))
			{
				var sql = String.Format("{0} Where ComponentSN = @SN", get_query_component_transport_view());
				var param = new { SN = component_SN };
				records = connection.Query<ComponentTransport>(sql, param).ToList();

				//一般零件:運輸
				foreach (var record in records)
				{
					//assign name and mass
					record.C_Name = component_Name;
					record.Component_Mass = component_Mass;
				}
			}
			return records;
		}

		/// <summary>
		/// 一般零件 : 加工
		/// </summary>
		/// <param name="component_SN"></param>
		/// <param name="component_Mass"></param>
		/// <param name="component_Name"></param>
		/// <returns></returns>
		public List<ComponentProcess> GetComponentProcesses(int component_SN,
			string component_Mass, string component_Name)
		{
			var records = new List<ComponentProcess>();
			using (var connection = new OleDbConnection(Conn))
			{
				var sql = String.Format("{0} Where C_SN = @SN", get_query_component_makeProcess_view());
				var param = new { SN = component_SN };
				records = connection.Query<ComponentProcess>(sql, param).ToList();

				//一般零件:加工
				foreach (var record in records)
				{
					//assign name and mass
					record.C_Name = component_Name;
					record.ComponentMass = component_Mass;
				}
			}
			return records;
		}

		/// <summary>
		/// 取得 Product 的基本資料 (SN, Name, Desc, CreateDate, ModifiedDate)
		/// </summary>
		/// <returns>ProductRecords</returns>
		public object GetData()
		{
			var records = new List<ProductRecord>();
			using (OleDbConnection connection = new OleDbConnection(Conn))
			{
				string sql = getDataQueryString();
				records = connection.Query<ProductRecord>(sql).ToList();
			}
			return records;
		}
		public object GetDataExclude(int[] excludeSNs)
		{
			var records = new List<ProductRecord>();
			using (OleDbConnection connection = new OleDbConnection(Conn))
			{
				var condition = "";
				if (excludeSNs.Count() > 0)
				{
					condition = String.Format("Where SN Not IN ({0})", String.Join(",", excludeSNs));
				}
				string sql = String.Format("{0} {1}", getDataQueryString(), condition);
				records = connection.Query<ProductRecord>(sql).ToList();
			}
			return records;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="sn"></param>
		/// <returns>ProductRecord</returns>
		public object GetItem(int sn)
		{
			ProductRecord model = null;
			using (OleDbConnection connection = new OleDbConnection(Conn))
			{
				string condition = "Where SN = @SN";
				string sql = String.Format(@"{0} {1}", getDataQueryString(), condition);
				var param = new { SN = sn };
				model = connection.Query<ProductRecord>(sql, param).SingleOrDefault();
			}
			return model;
		}

		public object GetRecentUsedData()
		{
			var records = new List<RecentUsedDTO>();
			using (OleDbConnection connection = new OleDbConnection(Conn))
			{
				var sql = getRecentUsedDataQueryString();
				records = connection.Query<RecentUsedDTO>(sql).ToList();
			}
			return records;
		}

		public object GetRecentUsedItem(int sn)
		{
			RecentUsedDTO model = null;
			using (OleDbConnection connection = new OleDbConnection(Conn))
			{
				var condition = @"Where RT.ProductSN = @SN";
				var query = getRecentUsedDataQueryString();
				var sql = String.Format(@"{0} {1}", query, condition);
				var parm = new { SN = sn };
				model = connection.Query<RecentUsedDTO>(sql, parm).SingleOrDefault();
			}
			return model;
		}

		public bool UpdateRecentUsedItem(int sn)
		{
			var success = false;
			using (OleDbConnection connection = new OleDbConnection(Conn))
			{
				try
				{
					var modifiedDate = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
					var result = GetRecentUsedItem(sn) as RecentUsedDTO;
					if (result != null)
					{
						//Do Update
						var count = result.UsedCount + 1;
						var updateSQL = @"
					UPDATE RecentUsedProduct
					SET LastUsed = @Now, UsedCount = @Count
					Where ProductSN = @SN
					";
						var update_param = new { Now = modifiedDate, Count = count, SN = sn, };
						var affected = connection.Execute(updateSQL, update_param);
						success = affected > 0;
					}
					else
					{
						//Do Insert
						success = AddRecentUsedItem(sn);
					}
				}
				catch (Exception ex)
				{
					ErrMsg = ex.Message;
				}
			}
			return success;
		}

		public bool AddRecentUsedItem(int sn)
		{
			var success = false;
			using (OleDbConnection connection = new OleDbConnection(Conn))
			{
				var modifiedDate = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
				var parm = new { SN = sn, Now = modifiedDate };
				var sql = @"Insert INTO RecentUsedProduct(ProductSN, LastUsed, UsedCount) 
																Values(@SN, @Now, 1)";
				try
				{
					var affected = connection.Execute(sql, parm);
					success = affected > 0;
				}
				catch (Exception ex)
				{
					ErrMsg = ex.Message;
				}
			}
			return success;
		}

		#region private methods =====
		private string get_query_product_component_view()
		{
			return @"SELECT * FROM Product_Component_View";
		}
		private string get_query_component_transport_view()
		{
			return @"SELECT 
					Component_Transport_SN as SN,
					ComponentSN as C_SN,
					TransportSN,
					TransportName as Name,
					TransportWFP,
					TransportISO,
					Distance
					FROM Component_Transport_View
";
		}
		private string get_query_component_makeProcess_view()
		{
			return @"SELECT * FROM Component_MakeProcess_View";
		}
		private string get_query_product_specialComponent_view()
		{
			return @"SELECT * FROM Product_SpecialComponent_View";
		}
		private string get_query_postComponent_view()
		{
			return @"SELECT 
					SN as PostComponent_SN, 
					Component_SN, 
					Component_Name,
					Component_Volumn,
					Material_SN,
					Material_Name,
					Material_Density,
					Material_WFP,
					Material_ISO
					FROM PostComponent_View";
		}
		private string get_query_postComponent_transport_view()
		{
			return @"SELECT * FROM PostComponent_Transport_View";
		}
		private string get_query_postComponent_makeProcess_view()
		{
			return @"SELECT * FROM PostComponent_MakeProcess_View";
		}

		private string getDataQueryString()
		{
			return @"
SELECT	SN, 
		ProductName, 
		ProductDesc, 
		CreateDate, 
		ModifiedDate
FROM	Product
";
		}

		private string getRecentUsedDataQueryString()
		{
			return @"
SELECT	RT.ProductSN as SN,
		RT.LastUsed,
		RT.UsedCount,
		TT.ProductName as Name		
FROM	RecentUsedProduct RT
		INNER JOIN Product TT ON RT.ProductSN = TT.SN";
		}
		#endregion

		#endregion

	}
}
