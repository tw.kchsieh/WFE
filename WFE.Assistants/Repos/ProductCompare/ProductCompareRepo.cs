﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFE.Assistants.Helpers;
using WFE.Assistants.Models.ProductCompare;
using WFE.Assistants.Models.SQL;

namespace WFE.Assistants.Repos.ProductCompare
{
	public class ProductCompareRepo : RepoBase
	{
		private OLEDBHelper _dbHP;
		public ProductCompareRepo()
		{
			_dbHP = new OLEDBHelper();
		}
		public List<ProductComparisonRecord> GetComparisonRecord()
		{
			List<ProductComparisonRecord> result = null;
			var sql = @"
SELECT	p.*, v.ComparisonCount			 
FROM	ProductComparison p 
		Inner JOIN ProductComparisonCount_View v ON p.SN = v.ProductComparisonSN
;";
			using (OleDbConnection connection = new OleDbConnection(Conn))
			{
				result = connection.Query<ProductComparisonRecord>(sql).ToList();
			}
			return result;
		}

		public ProductComparison GetItem(int sn)
		{
			ProductComparison item = null;
			using (OleDbConnection connection = new OleDbConnection(Conn))
			{
				string condition = "Where SN = @SN";
				string sql = String.Format(@"{0} {1}", getDataQueryString(), condition);
				var param = new { SN = sn };
				item = connection.Query<ProductComparison>(sql, param).SingleOrDefault();
			}
			return item;
		}
		public ProductComparisonDetail GetDetailItem(int sn)
		{
			ProductComparisonDetail item = null;
			using (OleDbConnection connection = new OleDbConnection(Conn))
			{
				string condition = "Where SN = @SN";
				string sql = String.Format(@"{0} {1}", getDetailQueryString(), condition);
				var param = new { SN = sn };
				item = connection.Query<ProductComparisonDetail>(sql, param).SingleOrDefault();
			}
			return item;
		}

		public bool DeleteProductComparisonItem(int comparisonSN)
		{
			var ole = new OLESQL();
			ole.GenerateDeleteSQL("ProductComparison", new { SN = comparisonSN });
			var success = _dbHP.Execute(ole);
			return success;
		}

		public List<ProductComparisonDetail> GetDetailData(int comparisonSN)
		{
			List<ProductComparisonDetail> records = null;
			using (OleDbConnection connection = new OleDbConnection(Conn))
			{
				string condition = "Where ProductComparisonSN = @SN";
				string sql = String.Format(@"{0} {1}", getDetailQueryString(), condition);
				var param = new { SN = comparisonSN };
				records = connection.Query<ProductComparisonDetail>(sql, param).ToList();
			}
			return records;
		}
		public bool DeleteDetails(int comparisonSN)
		{
			var ole = new OLESQL();
			ole.GenerateDeleteSQL("ProductComparisonDetail", new { ProductComparisonSN = comparisonSN });
			var success = _dbHP.Execute(ole);
			return success;
		}

		public List<ProductComparisonDetail> GetData()
		{
			List<ProductComparisonDetail> data = new List<ProductComparisonDetail>();
			for (int i = 0; i < 10; i++)
			{
				data.Add(new ProductComparisonDetail() { });
			}
			return data;
		}
		public int SaveData(List<ProductComparisonDetail> rawData, ProductComparison model, out bool success)
		{
			success = true;
			var productComparisonSN = model.SN;
			var result = GetItem(model.SN);
			var isExists = (result != null);
			if (isExists)
			{
				//update
				success = doUpdate(rawData, result);
			}
			else
			{
				//Insert
				productComparisonSN = doInsert(rawData, model, out success);
			}
			return productComparisonSN;
		}
		private bool doUpdate(List<ProductComparisonDetail> rawData, ProductComparison model)
		{
			var success = true;
			var db = new OLEDBHelper();

			//update Comparison
			var comparisonExists = (GetItem(model.SN) != null);
			if (comparisonExists)
			{
				var ole = new OLESQL();
				ole.GenerateUpdateSQL("ProductComparison", "SN", model.SN.ToString(),
					new
					{
						ComparisonName = model.ComparisonName,
						ComparisonDesc = model.ComparisonDesc,
						ModifiedDate = DateTime.Now
					});
				success = db.Execute(ole);
				if (!success)
				{
					base.ErrMsg = db.GetErrMsg();
					return success;
				}
				//delete all, then Insert
				success = DeleteDetails(model.SN);
				if (!success)
				{
					base.ErrMsg = db.GetErrMsg();
					return success;
				}
				//insert Comparison Details
				foreach (var item in rawData)
				{
					var detailOLE = getInsertProductComparisonDetailOLESQL(item, model.SN);
					var detailSN = 0;
					success = db.Execute(detailOLE, out detailSN);
					if (!success)
					{
						base.ErrMsg = db.GetErrMsg();
						return success;
					}
				}
			}
			return success;
		}
		private int doInsert(List<ProductComparisonDetail> rawData, ProductComparison model, out bool success)
		{
			var sn = 0;
			var ole = new OLESQL();
			ole.GenerateInsertSQL("ProductComparison", new { ComparisonName = model.ComparisonName, ComparisonDesc = model.ComparisonDesc });
			OLEDBHelper db = new OLEDBHelper();
			success = db.Execute(ole, out sn);
			if (success)
			{
				foreach (var item in rawData)
				{
					var detailOLE = getInsertProductComparisonDetailOLESQL(item, sn);
					var detailSN = 0;
					try
					{
						success = db.Execute(detailOLE, out detailSN);
						if (!success)
						{
							base.ErrMsg = db.GetErrMsg();
							//do - delete
							doDelete(sn);
							break;
						}
						item.SN = detailSN;
					}
					catch (Exception ex)
					{
						base.ErrMsg = ex.Message;
						doDelete(sn);
						success = false;
						break;
					}
				}
			}
			return sn;
		}

		private void doDelete(int comparisonSN)
		{
			OLEDBHelper db = new OLEDBHelper();
			var ole = new OLESQL();
			ole.GenerateDeleteSQL("ProductComparison", new { SN = comparisonSN });
			var success = db.Execute(ole);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="item"></param>
		/// <param name="sn">ProductComparisonSN</param>
		/// <returns></returns>
		private OLESQL getInsertProductComparisonDetailOLESQL(ProductComparisonDetail item, int sn)
		{
			var detailOLE = new OLESQL();
			detailOLE.GenerateInsertSQL(false, "ProductComparisonDetail", new
			{
				ProductComparisonSN = sn,
				ProductSN = item.ProductSN,
				ProductName = item.ProductName,
				ProductDesc = item.ProductDesc,
				WFP = item.WFP,
				ISO = item.ISO,
				PerformanceType = item.PerformanceType,
				P1 = item.P1,
				P2 = item.P2,
				P3 = item.P3,
				Importance = item.Importance,
				W1 = item.W1,
				W2 = item.W2,
				W3 = item.W3,
			});
			return detailOLE;
		}
		#region  private methods
		private string getDataQueryString()
		{
			return @"SELECT * FROM ProductComparison";
		}
		private string getDetailQueryString()
		{
			return @"SELECT * FROM ProductComparisonDetail";
		}
		#endregion
	}
}
