﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFE.Assistants.Helpers;
using WFE.Assistants.Models.SQL;

namespace WFE.Assistants.Repos
{
	/// <summary>
	/// 資料編輯 Repo
	/// </summary>
	public class EditDataRepo : RepoBase
	{
		/// <summary>
		/// 資料表名稱
		/// </summary>
		public string TableName { get; }

		/// <summary>
		/// 資料表主鍵欄位名稱
		/// </summary>
		public string PKeyName { get; }

		private SQLStatementHelper _sqlHelper;
		private OLEDBHelper _oleDB;
		public EditDataRepo(string tableName, string pKeyName)
		{
			TableName = tableName;
			PKeyName = pKeyName;
			_sqlHelper = new SQLStatementHelper(tableName, pKeyName);
			_oleDB = new OLEDBHelper();
		}

		private string _conditionColumnName;
		private string _conditionColumnValue;
		private bool _useDataCondition = false;
		public void UseDataCondition(string conditionColumnName, string conditionColumnValue)
		{
			_conditionColumnName = conditionColumnName;
			_conditionColumnValue = conditionColumnValue;
			_useDataCondition = (!String.IsNullOrEmpty(_conditionColumnName) && !String.IsNullOrEmpty(_conditionColumnValue));
		}
		public bool IsUseDataCondition { get { return _useDataCondition; } }

		/// <summary>
		/// 新增資料
		/// </summary>
		/// <param name="param"></param>
		/// <returns>Primary Key value</returns>
		public int Insert(Dictionary<string, object> param)
		{
			//用 OLEDB，取得新增資料的 SN
			int keySN = 0;
			var ole = new OLESQL();
			ole.Generate_InsertSQL_With_Dict(this.TableName, param);
			var success = _oleDB.Execute(ole, out keySN);
			if (!success) ErrMsg = _oleDB.GetErrMsg();

			return keySN;
		}

		/// <summary>
		/// 刪除資料
		/// </summary>
		/// <param name="pKeyValue"></param>
		/// <returns></returns>
		public bool Delete(string pKeyValue)
		{
			var sql = _sqlHelper.Generate_Delete(pKeyValue);
			return executeNonQuery(sql, null);
		}

		/// <summary>
		/// 更新資料
		/// </summary>
		/// <param name="param"></param>
		/// <param name="pKeyVal">Primary Key value</param>
		/// <returns></returns>
		public bool Update(Dictionary<string, object> param, string pKeyVal)
		{
			var sql = _sqlHelper.Generate_Update(param, pKeyVal);
			return executeNonQuery(sql, param);
		}

		/// <summary>
		/// 取得所有資料
		/// </summary>
		/// <returns></returns>
		public List<object> GetData()
		{
			var sql = String.Format("SELECT * FROM {0}", this.TableName);
			if (_useDataCondition)
			{
				var condition = String.Format(" {0} Like '{1}%'", _conditionColumnName, _conditionColumnValue);
				sql = String.Format(@"{0} Where {1}", sql, condition);
			}
			return executeQuery(sql, null);
		}

		public List<object> GetData(string columnName, string queryText)
		{
			//assingn
			var sql = String.Format(@"SELECT * FROM {0} ", this.TableName);
			var dataConditionSQL = "";

			//set Filter condition
			var containsValue = String.Format("%{0}%", queryText);
			var param = new { Query = containsValue };
			var filterConditionSQL = String.Format("{0} Like @Query", columnName);

			if (_useDataCondition)
			{
				dataConditionSQL = String.Format("{0} Like '{1}%'", _conditionColumnName, _conditionColumnValue);
				sql = String.Format("{0} Where {1} And {2}", sql, dataConditionSQL, filterConditionSQL);
			}
			else
			{
				sql = String.Format(@"{0} Where {1} ", sql, filterConditionSQL);
			}
			return executeQuery(sql, param);
		}

		#region Execute Query Using Dapper

		private bool executeNonQuery(string sql, object param)
		{
			var success = true;
			using (var connection = new OleDbConnection(this.Conn))
			{
				try
				{
					if (param != null)
					{
						var affectedRows = connection.Execute(sql, param);
					}
					else
					{
						var affectedRows = connection.Execute(sql);
					}
				}
				catch (Exception ex)
				{
					ErrMsg = ex.Message;
					success = false;
				}
			}
			return success;

		}

		private List<object> executeQuery(string sql, object param)
		{
			List<object> records = null;
			ErrMsg = "";
			using (var connection = new OleDbConnection(this.Conn))
			{
				try
				{
					if (param != null)
					{
						records = connection.Query(sql, param).ToList();
					}
					else
					{
						records = connection.Query(sql).ToList();
					}
				}
				catch (Exception ex)
				{
					ErrMsg = ex.Message;
				}
			}
			return records;
		}
		#endregion
	}
}
