﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFE.Assistants.Models;

namespace WFE.Assistants.Repos
{
	/// <summary>
	/// 材料
	/// </summary>
	public class MaterialRepo
	{
		private string _conn;
		private string _ErrMsg;
		public MaterialRepo()
		{
			var config = ConfigurationManager.AppSettings["settings"] ?? "";
			if (!String.IsNullOrEmpty(config))
			{
				var repo = new ConfigRepo(config);
				if (repo.IsValid)
				{
					_conn = repo.GetDBConnectionString();
				}
			}
		}

		/// <summary>
		/// 取得 Material Records
		/// </summary>
		/// <returns></returns>
		public List<Material> GetData()
		{
			var records = new List<Material>();
			using (OleDbConnection connection = new OleDbConnection(_conn))
			{
				var condition = @" WHERE m.[WFP(WFN)] <> null And m.Density <> null";
				string queryString = getQueryString() + condition;
				records = connection.Query<Material>(queryString).ToList();//
			}
			return records;
		}

		/// <summary>
		/// 取得材料
		/// </summary>
		/// <param name="materialSN"></param>
		/// <returns></returns>
		public Material GetMaterial(int materialSN)
		{
			Material result = null;
			using (OleDbConnection connection = new OleDbConnection(_conn))
			{
				var condition = " WHERE	SN = @SN";
				string queryString = getQueryString() + condition;
				var param = new { SN = materialSN };
				try
				{
					result = connection.Query<Material>(queryString, param).SingleOrDefault();
				}
				catch (Exception ex)
				{
					_ErrMsg = ex.Message;
				}
			}
			return result;
		}

		/// <summary>
		/// 取得最近使用的材料
		/// </summary>
		/// <returns></returns>
		public List<RecentUsedMaterial> GetRecentMaterial()
		{
			var records = new List<RecentUsedMaterial>();
			using (OleDbConnection connection = new OleDbConnection(_conn))
			{
				string queryString = @"
				SELECT	RUM.MaterialSN, 
						RUM.LastUsed, 
						RUM.UsedCount, 
						MM.MaterialName as Name
				FROM	RecentUsedMaterial RUM INNER JOIN Material MM 
						ON RUM.MaterialSN = MM.SN
				ORDER BY RUM.LastUsed DESC
";
				records = connection.Query<RecentUsedMaterial>(queryString).ToList();//
			}
			return records;
		}

		/// <summary>
		/// 取得錯誤訊息
		/// </summary>
		/// <returns></returns>
		public string GetErrMsg()
		{
			return _ErrMsg;
		}

		/// <summary>
		/// 儲存最近使用的材料資訊
		/// </summary>
		/// <param name="materialSN"></param>
		/// <returns></returns>
		public bool SaveRecentMaterial(int materialSN)
		{
			var success = false;
			using (OleDbConnection connection = new OleDbConnection(_conn))
			{
				try
				{
					var modifiedDate = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
					string queryString = @"
				Select	MaterialSN, UsedCount 
				From	RecentUsedMaterial 
				Where	MaterialSN = @SN";
					var param = new { SN = materialSN };
					var result = connection.Query<RecentUsedMaterial>(queryString, param).SingleOrDefault();
					if (result != null)
					{
						//Do Update
						var count = result.UsedCount + 1;
						var updateSQL = @"
					UPDATE RecentUsedMaterial
					SET LastUsed = @Now, UsedCount = @Count
					Where MaterialSN = @SN
					";
						var update_param = new { Now = modifiedDate, Count = count, SN = materialSN, };
						var affected = connection.Execute(updateSQL, update_param);
					}
					else
					{
						//Do Insert
						var insert_param = new { SN = materialSN, Now = modifiedDate };
						var insertSQL = @"Insert INTO RecentUsedMaterial(MaterialSN, LastUsed, UsedCount) 
																Values(@SN, @Now, 1)";
						var affected = connection.Execute(insertSQL, insert_param);
					}
					success = true;
				}
				catch (Exception ex)
				{
					_ErrMsg = ex.Message;
					success = false;
				}
			}
			return success;
		}

		private string getQueryString()
		{
			return @"
			SELECT	SN, 
					m.MaterialName as Name,
					m.Density,
					m.BlueWater,
					m.GreenWater,
					m.GrayWater,
					m.[WFP(WFN)] as WFP,
					m.[WFP(ISO14046)] as ISO,
					m.BOD
			FROM 	Material m";
		}
	}
}
