﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFE.Assistants.Models;

namespace WFE.Assistants.Repos
{
	public class AssemblyMethodRepo : RepoBase, IRepo
	{
		private string _mainTableName = "AssemblyMethod";
		private string _recentUsedItemTableName = "RecentUsedAssemblyMethod";
		private string _recentUsedItemKeyName = "AssemblyMethodSN";
		public object GetData()
		{
			var records = new List<AssemblyMethod>();
			using (OleDbConnection connection = new OleDbConnection(Conn))
			{
				string condition = "Where PowerConsumption <> null";
				string sql = String.Format(@"{0} {1}", getDataQueryString(), condition);
				records = connection.Query<AssemblyMethod>(sql).ToList();
			}
			return records;
		}

		public object GetItem(int sn)
		{
			AssemblyMethod model = null;
			using (OleDbConnection connection = new OleDbConnection(Conn))
			{
				string condition = "Where SN = @SN";
				string sql = String.Format(@"{0} {1}", getDataQueryString(), condition);
				var param = new { SN = sn };
				model = connection.Query<AssemblyMethod>(sql, param).SingleOrDefault();
			}
			return model;
		}

		public object GetRecentUsedData()
		{
			var records = new List<RecentUsedDTO>();
			using (OleDbConnection connection = new OleDbConnection(Conn))
			{
				var sql = getRecentUsedDataQueryString();
				records = connection.Query<RecentUsedDTO>(sql).ToList();
			}
			return records;
		}

		public object GetRecentUsedItem(int sn)
		{
			RecentUsedDTO model = null;
			using (OleDbConnection connection = new OleDbConnection(Conn))
			{
				var condition = String.Format(@"Where {0} = @SN", _recentUsedItemKeyName);
				var sql = String.Format(@"SELECT * FROM {0} {1}", _recentUsedItemTableName, condition);
				var parm = new { SN = sn };
				model = connection.Query<RecentUsedDTO>(sql, parm).SingleOrDefault();
			}
			return model;
		}

		public bool AddRecentUsedItem(int sn)
		{
			var success = false;
			using (OleDbConnection connection = new OleDbConnection(Conn))
			{
				var modifiedDate = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
				var parm = new { SN = sn, Now = modifiedDate };
				var sql = String.Format(@"Insert INTO {0}({1}, LastUsed, UsedCount) 
										Values(@SN, @Now, 1)", _recentUsedItemTableName, _recentUsedItemKeyName);
				try
				{
					var affected = connection.Execute(sql, parm);
					success = affected > 0;
				}
				catch (Exception ex)
				{
					ErrMsg = ex.Message;
				}
			}
			return success;
		}

		public bool UpdateRecentUsedItem(int sn)
		{
			var success = false;
			using (OleDbConnection connection = new OleDbConnection(Conn))
			{
				try
				{
					var modifiedDate = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
					var result = GetRecentUsedItem(sn) as RecentUsedDTO;
					if (result != null)
					{
						//Do Update
						var count = result.UsedCount + 1;
						var updateSQL = String.Format(@"
					UPDATE {0}
					SET LastUsed = @Now, UsedCount = @Count
					Where {1} = @SN
					", _recentUsedItemTableName, _recentUsedItemKeyName);
						var update_param = new { Now = modifiedDate, Count = count, SN = sn, };
						var affected = connection.Execute(updateSQL, update_param);
						success = affected > 0;
					}
					else
					{
						//Do Insert
						success = AddRecentUsedItem(sn);
					}
				}
				catch (Exception ex)
				{
					ErrMsg = ex.Message;
				}
			}
			return success;
		}

		#region private methods
		private string getDataQueryString()
		{
			return String.Format(@"
SELECT	
		SN, 
		AssemblyMethodName, 
		CalculateUnit,
		PowerConsumption,
		PowerCoefficient,
		Remark,
		DataSource
FROM {0}
", _mainTableName);
		}

		private string getRecentUsedDataQueryString()
		{
			return @"
SELECT	RT.AssemblyMethodSN as SN,
		RT.LastUsed,
		RT.UsedCount,
		TT.AssemblyMethodName as Name		
FROM	RecentUsedAssemblyMethod RT
		INNER JOIN AssemblyMethod TT ON RT.AssemblyMethodSN = TT.SN";
		}
		#endregion
	}
}
