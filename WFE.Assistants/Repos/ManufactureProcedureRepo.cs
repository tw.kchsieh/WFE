﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFE.Assistants.Models;

namespace WFE.Assistants.Repos
{
	/// <summary>
	/// 製程
	/// </summary>
	public class ManufactureProcedureRepo
	{
		private string _conn;
		private string _ErrMsg;

		public ManufactureProcedureRepo()
		{
			var config = ConfigurationManager.AppSettings["settings"] ?? "";
			if (!String.IsNullOrEmpty(config))
			{
				var repo = new ConfigRepo(config);
				if (repo.IsValid)
				{
					_conn = repo.GetDBConnectionString();
				}
			}
		}

		public List<RecentUsedDTO> GetRecentUsedData()
		{
			var records = new List<RecentUsedDTO>();
			using (OleDbConnection connection = new OleDbConnection(_conn))
			{
				string queryString = @"
				SELECT	RP.ProcessesSN as SN,						   
						RP.LastUsed, 
						RP.UsedCount,
						PS.ProcessName as Name
				FROM	RecentUsedManufactureProcedure RP 
						INNER JOIN ManufactureProcedure PS ON RP.ProcessesSN = PS.SN
				ORDER BY RP.LastUsed DESC
";
				records = connection.Query<RecentUsedDTO>(queryString).ToList();//
			}
			return records;
		}

		//
		public List<ManufactureProcedure> GetData()
		{
			var records = new List<ManufactureProcedure>();
			using (OleDbConnection connection = new OleDbConnection(_conn))
			{
				var condition = @"WHERE	[WFP(WFN)] <> null OR Density <> null";
				string queryString = String.Format("{0} {1}", getQueryString(), condition);
				records = connection.Query<ManufactureProcedure>(queryString).ToList();//
			}
			return records;
		}

		public bool SaveRecentUsed(int selectedSN)
		{
			var success = false;
			using (OleDbConnection connection = new OleDbConnection(_conn))
			{
				try
				{
					var modifiedDate = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
					string queryString = @"
				Select	ProcessesSN as SN,
						UsedCount 
				From	RecentUsedManufactureProcedure
				Where	ProcessesSN = @SN";
					var param = new { SN = selectedSN };
					var result = connection.Query<RecentUsedDTO>(queryString, param).SingleOrDefault();

					if (result != null)
					{
						//Do Update
						var count = result.UsedCount + 1;
						var updateSQL = @"
					UPDATE RecentUsedManufactureProcedure
					SET LastUsed = @Now, UsedCount = @Count
					Where ProcessesSN = @SN";
						var update_param = new { Now = modifiedDate, Count = count, SN = selectedSN, };
						var affected = connection.Execute(updateSQL, update_param);
					}
					else
					{
						//Do Insert
						var insert_param = new { SN = selectedSN, Now = modifiedDate };
						var insertSQL = @"Insert INTO RecentUsedManufactureProcedure(ProcessesSN, LastUsed, UsedCount) 
																Values(@SN, @Now, 1)";
						var affected = connection.Execute(insertSQL, insert_param);
					}
					success = true;
				}
				catch (Exception ex)
				{
					_ErrMsg = ex.Message;
					success = false;
				}
			}
			return success;
		}

		public ManufactureProcedure GetProcesses(int sn)
		{
			ManufactureProcedure result = null;
			using (OleDbConnection connection = new OleDbConnection(_conn))
			{
				var condition = @"WHERE	SN = @SN";
				string queryString = String.Format(@"{0} {1}", getQueryString(), condition);
				var param = new { SN = sn };
				try
				{
					result = connection.Query<ManufactureProcedure>(queryString, param).SingleOrDefault();
				}
				catch (Exception ex)
				{
					_ErrMsg = ex.Message;
				}
			}
			return result;
		}

		public string GetErrMsg()
		{
			return _ErrMsg;
		}

		//
		private string getQueryString()
		{
			return @"
			SELECT	SN,
					ProcessName,
					Density,
					ProcessTypeCode,
					BlueWater,
					GreenWater,
					GrayWater,
					[WFP(WFN)] as WFP,
					[WFP(ISO14046)] as ISO,
					BOD
			FROM 	ManufactureProcedure";
		}


	}
}
