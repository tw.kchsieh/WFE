﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFE.Assistants.Models;

namespace WFE.Assistants.Repos
{
	public class TransportRepo : RepoBase, IRepo
	{
		/// <summary>
		/// 取得運輸工具所有資料
		/// </summary>
		/// <returns></returns>
		public object GetData()
		{
			var records = new List<Transport>();
			using (OleDbConnection connection = new OleDbConnection(Conn))
			{
				string condition = "Where [WFP(WFN)] and [WFP(ISO14046)] <> null";
				string sql = String.Format(@"{0} {1}", getDataQueryString(), condition);
				records = connection.Query<Transport>(sql).ToList();
			}
			return records;
		}

		/// <summary>
		/// 取得運輸工具單一資料
		/// </summary>
		/// <param name="sn"></param>
		/// <returns></returns>
		public object GetItem(int sn)
		{
			Transport model = null;
			using (OleDbConnection connection = new OleDbConnection(Conn))
			{
				string condition = "Where SN = @SN";
				string sql = String.Format(@"{0} {1}", getDataQueryString(), condition);
				var param = new { SN = sn };
				model = connection.Query<Transport>(sql, param).SingleOrDefault();
			}
			return model;
		}

		/// <summary>
		/// 取得最近常用運輸工具所有資料
		/// </summary>
		/// <returns></returns>
		public object GetRecentUsedData()
		{
			var records = new List<RecentUsedTransport>();
			using (OleDbConnection connection = new OleDbConnection(Conn))
			{
				var sql = getRecentUsedDataQueryString();
				records = connection.Query<RecentUsedTransport>(sql).ToList();
			}
			return records;
		}

		/// <summary>
		/// 取得最近常用運輸工具單一資料
		/// </summary>
		/// <param name="sn"></param>
		/// <returns></returns>
		public object GetRecentUsedItem(int sn)
		{
			RecentUsedTransport model = null;
			using (OleDbConnection connection = new OleDbConnection(Conn))
			{
				var condition = @"Where TransportSN = @SN";
				var sql = String.Format(@"SELECT * FROM RecentUsedTransport {0}", condition);
				var parm = new { SN = sn };
				model = connection.Query<RecentUsedTransport>(sql, parm).SingleOrDefault();
			}
			return model;
		}

		/// <summary>
		/// 加入一筆最近常用運輸工具
		/// </summary>
		/// <param name="sn"></param>
		/// <returns></returns>
		public bool AddRecentUsedItem(int sn)
		{
			var success = false;
			using (OleDbConnection connection = new OleDbConnection(Conn))
			{
				var modifiedDate = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
				var parm = new { SN = sn, Now = modifiedDate };
				var sql = @"Insert INTO RecentUsedTransport(TransportSN, LastUsed, UsedCount) 
																Values(@SN, @Now, 1)";
				try
				{
					var affected = connection.Execute(sql, parm);
					success = affected > 0;
				}
				catch (Exception ex)
				{
					ErrMsg = ex.Message;
				}
			}
			return success;
		}

		/// <summary>
		/// 更新最近常用運輸工具
		/// </summary>
		/// <param name="sn"></param>
		/// <returns></returns>
		public bool UpdateRecentUsedItem(int sn)
		{
			var success = false;
			using (OleDbConnection connection = new OleDbConnection(Conn))
			{
				try
				{
					var modifiedDate = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
					var result = GetRecentUsedItem(sn) as RecentUsedTransport;
					if (result != null)
					{
						//Do Update
						var count = result.UsedCount + 1;
						var updateSQL = @"
					UPDATE RecentUsedTransport
					SET LastUsed = @Now, UsedCount = @Count
					Where TransportSN = @SN
					";
						var update_param = new { Now = modifiedDate, Count = count, SN = sn, };
						var affected = connection.Execute(updateSQL, update_param);
						success = affected > 0;
					}
					else
					{
						//Do Insert
						success = AddRecentUsedItem(sn);
					}
				}
				catch (Exception ex)
				{
					ErrMsg = ex.Message;
				}
			}
			return success;
		}

		#region private methods
		private string getDataQueryString()
		{
			return @"
SELECT	SN,
		TT.TransportName,
		TT.MaxLoad,
		TT.[WFP(WFN)] as WFP,
		TT.[WFP(ISO14046)] as ISO,
		TT.BOD,
		TT.BlueWater,
		TT.GreenWater,
		TT.GrayWater,
		TT.CalculateUnit as CalcUnit,
		TT.Unit as UnitOne,
		TT.EnergyPerUnit
FROM	Transport TT";
		}

		private string getRecentUsedDataQueryString()
		{
			return @"
SELECT	RT.TransportSN,
		RT.LastUsed,
		RT.UsedCount,
		TT.TransportName as Name		
FROM	RecentUsedTransport RT
		INNER JOIN Transport TT ON RT.TransportSN = TT.SN";
		}
		#endregion
	}
}
