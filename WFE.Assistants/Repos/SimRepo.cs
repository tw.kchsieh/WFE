﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFE.Assistants.Repos
{
	public class SimRepo
	{
		private Random _ran;
		public string GetRandomName()
		{
			return "Name_" + Guid.NewGuid().ToString().Substring(0, 4);
		}
		public string GetRandomVolumn()
		{
			var seed = (int)DateTime.Now.Ticks;
			_ran = new Random(seed);
			return (_ran.NextDouble() * 1000).ToString();
		}

		public double GetRandomArea()
		{
			var seed = (int)DateTime.Now.Ticks;
			_ran = new Random(seed);
			return _ran.Next(100, 1000);
		}
	}
}
