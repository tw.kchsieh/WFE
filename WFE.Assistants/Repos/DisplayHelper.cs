﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFE.Assistants.Repos
{
	public class DisplayHelper
	{
		public TransportDispDTO GetTransportDisplayModel(string code)
		{
			return new TransportDispDTO(code);
		}
	}
	public class TransportDispDTO
	{
		public TransportDispDTO(string code)
		{
			switch (code.ToLower())
			{
				case "stagea":
					FormText = "選擇運輸工具 (一般零件)";
					CaptionName = "原料名稱";
					CaptionMass = "原料質量";
					CaptionWFP = "運輸水足跡 : ";
					break;
				case "stageb":
					FormText = "選擇運輸工具 (後製加工)";
					CaptionName = "零件名稱";
					CaptionMass = "零件質量";
					CaptionWFP = "運輸水足跡 : ";
					break;
			}
		}
		public string FormText { get; set; }
		public string CaptionName { get; set; }
		public string CaptionMass { get; set; }
		public string CaptionWFP { get; set; }
	}
}
