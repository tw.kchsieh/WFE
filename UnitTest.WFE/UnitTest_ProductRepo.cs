﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFE.Assistants.Repos;
namespace UnitTest.WFE
{
	/// <summary>
	/// 單元測試: ProductRepo
	/// </summary>
	[TestClass]
	public class UnitTest_ProductRepo
	{
		private ProductRepo _repo = new ProductRepo();
		public UnitTest_ProductRepo()
		{
			_repo = new ProductRepo();
		}

		[TestMethod]
		public void TestMethod_GetProductComponents()
		{
			//a1
			var productSN = 24;

			//a2
			var records = _repo.GetProductComponents(productSN);// as List<ProductComponent>;

			//a3
			Assert.IsTrue(records.Count > 0, " > 0");

		}

		[TestMethod]
		public void TestMethod_GetComponentTransports()
		{
			//a1
			var componentSN = 2;
			var component_Mass = "1";
			var component_Name = "test_1928";

			//a2
			var records = _repo.GetComponentTransports(componentSN, component_Mass, component_Name);// as List<ProductComponent>;

			//a3
			Assert.IsTrue(records.Count > 0, " > 0");
		}

		[TestMethod]
		public void TestMethod_GetComponentProcesses()
		{
			//a1
			var componentSN = 20;
			var component_Mass = "12";
			var component_Name = "test_9453";

			//a2
			var records = _repo.GetComponentProcesses(componentSN, component_Mass, component_Name);// as List<ProductComponent>;

			//a3
			Assert.IsTrue(records.Count > 0, " > 0");
		}

		[TestMethod]
		public void TestMethod_GetPostProcessComponents()
		{
			//a1
			var componentSNs = new int[] { 29 };


			//a2
			var records = _repo.GetPostComponents(componentSNs);// as List<ProductComponent>;

			//a3
			Assert.IsTrue(records.Count == 1, " > 0");
		}

		[TestMethod]
		public void TestMethod_GetPostComponentTransports()
		{
			//a1
			var componentSN = 10;
			var component_Mass = "129";
			var component_Name = "test_8991";

			//a2
			var records = _repo.GetPostComponentTransports(componentSN, component_Name, component_Mass);

			//a3
			Assert.IsTrue(records.Count == 1, " > 0");
		}

		[TestMethod]
		public void TestMethod_GetPostComponentProcess()
		{
			//a1
			var componentSN = 3;
			var component_Mass = "12";
			var component_Name = "test_9453";

			//a2
			var records = _repo.GetPostComponentProcess(componentSN, component_Name, component_Mass);

			//a3
			Assert.IsTrue(records.Count > 0, " > 0");
		}

	}
}
