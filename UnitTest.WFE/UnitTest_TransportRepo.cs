﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WFE.Assistants.Repos;
using WFE.Assistants.Models;
using System.Collections.Generic;

namespace UnitTest.WFE
{
	[TestClass]
	public class UnitTest_TransportRepo
	{
		private TransportRepo _repo;
		public UnitTest_TransportRepo()
		{
			_repo = new TransportRepo();
		}
		[TestMethod]
		public void TestMethod_GetData()
		{
			//a2
			var records = _repo.GetData() as List<Transport>;

			//a3
			Assert.IsTrue(records.Count > 0, " > 0");

		}

		[TestMethod]
		public void TestMethod_GetItem()
		{
			//a1
			var sn = 41;

			//a2
			var item = _repo.GetItem(sn) as Transport;

			//a3
			Assert.IsTrue(item != null && item.SN == sn);
		}

		[TestMethod]
		public void TestMethod_GetRecentUsedData()
		{
			//a2
			var item = _repo.GetRecentUsedData() as List<RecentUsedTransport>;

			//a3
			Assert.IsTrue(item != null);
		}

		[TestMethod]
		public void TestMethod_GetRecentUsedItem()
		{
			//a1
			var sn = 60;

			//a2
			var item = _repo.GetRecentUsedItem(sn) as RecentUsedTransport;

			//a3
			Assert.IsTrue(item != null);
		}

		[TestMethod]
		public void TestMethod_UpdateRecentUsedData()
		{
			//a1
			var sn = 187;

			//a2
			var success = _repo.UpdateRecentUsedItem(sn);

			//a3
			Assert.IsTrue(success);
			//
		}
	}
}
