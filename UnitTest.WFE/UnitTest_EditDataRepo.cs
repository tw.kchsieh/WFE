﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WFE.Assistants.Repos;

namespace UnitTest.WFE
{
	/// <summary>
	/// Summary description for UnitTest_EditDataRepo
	/// </summary>
	[TestClass]
	public class UnitTest_EditDataRepo
	{
		private EditDataRepo _repo;
		public UnitTest_EditDataRepo()
		{
			//
			// TODO: Add constructor logic here
			//
			_repo = new EditDataRepo("MyTest", "SN");
		}

		private TestContext testContextInstance;

		/// <summary>
		///Gets or sets the test context which provides
		///information about and functionality for the current test run.
		///</summary>
		public TestContext TestContext
		{
			get
			{
				return testContextInstance;
			}
			set
			{
				testContextInstance = value;
			}
		}

		#region Additional test attributes
		//
		// You can use the following additional attributes as you write your tests:
		//
		// Use ClassInitialize to run code before running the first test in the class
		// [ClassInitialize()]
		// public static void MyClassInitialize(TestContext testContext) { }
		//
		// Use ClassCleanup to run code after all tests in a class have run
		// [ClassCleanup()]
		// public static void MyClassCleanup() { }
		//
		// Use TestInitialize to run code before running each test 
		// [TestInitialize()]
		// public void MyTestInitialize() { }
		//
		// Use TestCleanup to run code after each test has run
		// [TestCleanup()]
		// public void MyTestCleanup() { }
		//
		#endregion

		/// <summary>
		/// 新增
		/// </summary>
		[TestMethod]
		public void TestMethod_Insert()
		{
			//assign
			var param = new Dictionary<string, object>();

			param.Add("MyName", "Joe");
			param.Add("MyAge", "789");

			//act
			var sn = _repo.Insert(param);

			//assert	
			Assert.IsTrue(sn > 0);
		}

		/// <summary>
		/// 更新
		/// </summary>
		[TestMethod]
		public void TestMethod_Update()
		{
			//assign
			var param = new Dictionary<string, object>();
			param.Add("MyName", "Mary");
			param.Add("MyAge", "456");

			var keySN = 1;

			//act
			var success = _repo.Update(param, keySN.ToString());

			//assert	
			Assert.IsTrue(success);
		}

		/// <summary>
		/// 刪除
		/// </summary>
		[TestMethod]
		public void TestMethod_Delete()
		{
			//assign
			var keySN = 1;

			//act
			var success = _repo.Delete(keySN.ToString());

			//assert	
			Assert.IsTrue(success);
		}

		/// <summary>
		/// 取資料
		/// </summary>
		[TestMethod]
		public void TestMethod_GetData()
		{
			//act
			var records = _repo.GetData();

			//assert	
			Assert.IsTrue(records != null);
		}

		/// <summary>
		/// 取資料, Use Filter
		/// </summary>
		[TestMethod]
		public void TestMethod_GetDataWithFilter()
		{
			//assign
			var columnName = "MyName";
			var query = "o";

			//act
			var records = _repo.GetData(columnName, query);

			//assert	
			Assert.IsTrue(records != null);
		}

		/// <summary>
		/// 取資料, Use Default Data Condition
		/// <para>基本的篩選</para>
		/// </summary>
		[TestMethod]
		public void TestMethod_GetDataWithDataCondtion()
		{
			//assign
			//var columnName = "MyName";
			//var query = "o";
			var dataConditionColName = "TypeCode";
			var dataConditionColValue = "A";

			//act
			_repo.UseDataCondition(dataConditionColName, dataConditionColValue);
			var records = _repo.GetData();

			//assert	
			Assert.IsTrue(records != null);
		}

		[TestMethod]
		public void TestMethod_GetDataWithDataCondtionAnd_Filter()
		{
			//assign
			var columnName = "MyName";
			var query = "h";
			var dataConditionColName = "TypeCode";
			var dataConditionColValue = "A";

			//act
			_repo.UseDataCondition(dataConditionColName, dataConditionColValue);
			var records = _repo.GetData(columnName, query);

			//assert	
			Assert.IsTrue(records != null);
		}
	}
}
