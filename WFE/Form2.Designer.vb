﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form2
    Inherits System.Windows.Forms.Form

    'Form 覆寫 Dispose 以清除元件清單。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    '為 Windows Form 設計工具的必要項
    Private components As System.ComponentModel.IContainer

    '注意: 以下為 Windows Form 設計工具所需的程序
    '可以使用 Windows Form 設計工具進行修改。
    '請勿使用程式碼編輯器進行修改。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.TextBox2 = New System.Windows.Forms.TextBox()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.識別碼DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.種類DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.計算單位DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.最大載貨重量DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.總水足跡m3DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.藍水m3DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.綠水m3DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.灰水m3DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.單位1DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.能源單位1DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.能耗量1DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.耗能係數1DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.運輸工具BindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.WFEDataSet2 = New WFE.WFEDataSet2()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.DataGridView2 = New System.Windows.Forms.DataGridView()
        Me.識別碼DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.運輸工具DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.距離DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.耗油LDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.油耗係數DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.水足跡DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.貨物重量DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.已選運輸資訊BindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.WFEDataSet4 = New WFE.WFEDataSet4()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.運輸工具TableAdapter = New WFE.WFEDataSet2TableAdapters.運輸工具TableAdapter()
        Me.WFEDataSet3 = New WFE.WFEDataSet3()
        Me.已選運輸資訊TableAdapter = New WFE.WFEDataSet4TableAdapters.已選運輸資訊TableAdapter()
        Me.GroupBox1.SuspendLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.運輸工具BindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.WFEDataSet2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        CType(Me.DataGridView2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.已選運輸資訊BindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.WFEDataSet4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.WFEDataSet3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Button1)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.TextBox2)
        Me.GroupBox1.Controls.Add(Me.TextBox1)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.DataGridView1)
        Me.GroupBox1.Font = New System.Drawing.Font("微軟正黑體", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(12, 12)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(338, 217)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "運輸工具"
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(238, 161)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(84, 31)
        Me.Button1.TabIndex = 7
        Me.Button1.Text = "確定/新增"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(205, 182)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(32, 19)
        Me.Label4.TabIndex = 6
        Me.Label4.Text = "Km"
        '
        'TextBox2
        '
        Me.TextBox2.Location = New System.Drawing.Point(107, 179)
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.Size = New System.Drawing.Size(93, 27)
        Me.TextBox2.TabIndex = 5
        '
        'TextBox1
        '
        Me.TextBox1.Location = New System.Drawing.Point(107, 149)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(93, 27)
        Me.TextBox1.TabIndex = 4
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(205, 149)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(28, 19)
        Me.Label3.TabIndex = 3
        Me.Label3.Text = "Kg"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(16, 179)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(69, 19)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "運輸距離"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(16, 149)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(84, 19)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "該零件重量"
        '
        'DataGridView1
        '
        Me.DataGridView1.AutoGenerateColumns = False
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.識別碼DataGridViewTextBoxColumn, Me.種類DataGridViewTextBoxColumn, Me.計算單位DataGridViewTextBoxColumn, Me.最大載貨重量DataGridViewTextBoxColumn, Me.總水足跡m3DataGridViewTextBoxColumn, Me.藍水m3DataGridViewTextBoxColumn, Me.綠水m3DataGridViewTextBoxColumn, Me.灰水m3DataGridViewTextBoxColumn, Me.單位1DataGridViewTextBoxColumn, Me.能源單位1DataGridViewTextBoxColumn, Me.能耗量1DataGridViewTextBoxColumn, Me.耗能係數1DataGridViewTextBoxColumn})
        Me.DataGridView1.DataSource = Me.運輸工具BindingSource
        Me.DataGridView1.Location = New System.Drawing.Point(16, 24)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.RowTemplate.Height = 27
        Me.DataGridView1.Size = New System.Drawing.Size(305, 118)
        Me.DataGridView1.TabIndex = 0
        '
        '識別碼DataGridViewTextBoxColumn
        '
        Me.識別碼DataGridViewTextBoxColumn.DataPropertyName = "識別碼"
        Me.識別碼DataGridViewTextBoxColumn.HeaderText = "識別碼"
        Me.識別碼DataGridViewTextBoxColumn.Name = "識別碼DataGridViewTextBoxColumn"
        '
        '種類DataGridViewTextBoxColumn
        '
        Me.種類DataGridViewTextBoxColumn.DataPropertyName = "種類"
        Me.種類DataGridViewTextBoxColumn.HeaderText = "種類"
        Me.種類DataGridViewTextBoxColumn.Name = "種類DataGridViewTextBoxColumn"
        '
        '計算單位DataGridViewTextBoxColumn
        '
        Me.計算單位DataGridViewTextBoxColumn.DataPropertyName = "計算單位"
        Me.計算單位DataGridViewTextBoxColumn.HeaderText = "計算單位"
        Me.計算單位DataGridViewTextBoxColumn.Name = "計算單位DataGridViewTextBoxColumn"
        '
        '最大載貨重量DataGridViewTextBoxColumn
        '
        Me.最大載貨重量DataGridViewTextBoxColumn.DataPropertyName = "最大載貨重量"
        Me.最大載貨重量DataGridViewTextBoxColumn.HeaderText = "最大載貨重量"
        Me.最大載貨重量DataGridViewTextBoxColumn.Name = "最大載貨重量DataGridViewTextBoxColumn"
        '
        '總水足跡m3DataGridViewTextBoxColumn
        '
        Me.總水足跡m3DataGridViewTextBoxColumn.DataPropertyName = "總水足跡(m3)"
        Me.總水足跡m3DataGridViewTextBoxColumn.HeaderText = "總水足跡(m3)"
        Me.總水足跡m3DataGridViewTextBoxColumn.Name = "總水足跡m3DataGridViewTextBoxColumn"
        '
        '藍水m3DataGridViewTextBoxColumn
        '
        Me.藍水m3DataGridViewTextBoxColumn.DataPropertyName = "藍水(m3)"
        Me.藍水m3DataGridViewTextBoxColumn.HeaderText = "藍水(m3)"
        Me.藍水m3DataGridViewTextBoxColumn.Name = "藍水m3DataGridViewTextBoxColumn"
        '
        '綠水m3DataGridViewTextBoxColumn
        '
        Me.綠水m3DataGridViewTextBoxColumn.DataPropertyName = "綠水(m3)"
        Me.綠水m3DataGridViewTextBoxColumn.HeaderText = "綠水(m3)"
        Me.綠水m3DataGridViewTextBoxColumn.Name = "綠水m3DataGridViewTextBoxColumn"
        '
        '灰水m3DataGridViewTextBoxColumn
        '
        Me.灰水m3DataGridViewTextBoxColumn.DataPropertyName = "灰水(m3)"
        Me.灰水m3DataGridViewTextBoxColumn.HeaderText = "灰水(m3)"
        Me.灰水m3DataGridViewTextBoxColumn.Name = "灰水m3DataGridViewTextBoxColumn"
        '
        '單位1DataGridViewTextBoxColumn
        '
        Me.單位1DataGridViewTextBoxColumn.DataPropertyName = "單位1"
        Me.單位1DataGridViewTextBoxColumn.HeaderText = "單位1"
        Me.單位1DataGridViewTextBoxColumn.Name = "單位1DataGridViewTextBoxColumn"
        '
        '能源單位1DataGridViewTextBoxColumn
        '
        Me.能源單位1DataGridViewTextBoxColumn.DataPropertyName = "能源/單位1"
        Me.能源單位1DataGridViewTextBoxColumn.HeaderText = "能源/單位1"
        Me.能源單位1DataGridViewTextBoxColumn.Name = "能源單位1DataGridViewTextBoxColumn"
        '
        '能耗量1DataGridViewTextBoxColumn
        '
        Me.能耗量1DataGridViewTextBoxColumn.DataPropertyName = "能耗量1"
        Me.能耗量1DataGridViewTextBoxColumn.HeaderText = "能耗量1"
        Me.能耗量1DataGridViewTextBoxColumn.Name = "能耗量1DataGridViewTextBoxColumn"
        '
        '耗能係數1DataGridViewTextBoxColumn
        '
        Me.耗能係數1DataGridViewTextBoxColumn.DataPropertyName = "耗能係數1"
        Me.耗能係數1DataGridViewTextBoxColumn.HeaderText = "耗能係數1"
        Me.耗能係數1DataGridViewTextBoxColumn.Name = "耗能係數1DataGridViewTextBoxColumn"
        '
        '運輸工具BindingSource
        '
        Me.運輸工具BindingSource.DataMember = "運輸工具"
        Me.運輸工具BindingSource.DataSource = Me.WFEDataSet2
        '
        'WFEDataSet2
        '
        Me.WFEDataSet2.DataSetName = "WFEDataSet2"
        Me.WFEDataSet2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.Button2)
        Me.GroupBox2.Controls.Add(Me.DataGridView2)
        Me.GroupBox2.Font = New System.Drawing.Font("微軟正黑體", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(12, 235)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(338, 194)
        Me.GroupBox2.TabIndex = 1
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "運輸紀錄"
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(116, 150)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(111, 30)
        Me.Button2.TabIndex = 1
        Me.Button2.Text = "刪除"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'DataGridView2
        '
        Me.DataGridView2.AutoGenerateColumns = False
        Me.DataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView2.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.識別碼DataGridViewTextBoxColumn1, Me.運輸工具DataGridViewTextBoxColumn, Me.距離DataGridViewTextBoxColumn, Me.耗油LDataGridViewTextBoxColumn, Me.油耗係數DataGridViewTextBoxColumn, Me.水足跡DataGridViewTextBoxColumn, Me.貨物重量DataGridViewTextBoxColumn})
        Me.DataGridView2.DataSource = Me.已選運輸資訊BindingSource
        Me.DataGridView2.Location = New System.Drawing.Point(20, 26)
        Me.DataGridView2.Name = "DataGridView2"
        Me.DataGridView2.RowTemplate.Height = 27
        Me.DataGridView2.Size = New System.Drawing.Size(305, 118)
        Me.DataGridView2.TabIndex = 0
        '
        '識別碼DataGridViewTextBoxColumn1
        '
        Me.識別碼DataGridViewTextBoxColumn1.DataPropertyName = "識別碼"
        Me.識別碼DataGridViewTextBoxColumn1.HeaderText = "識別碼"
        Me.識別碼DataGridViewTextBoxColumn1.Name = "識別碼DataGridViewTextBoxColumn1"
        '
        '運輸工具DataGridViewTextBoxColumn
        '
        Me.運輸工具DataGridViewTextBoxColumn.DataPropertyName = "運輸工具"
        Me.運輸工具DataGridViewTextBoxColumn.HeaderText = "運輸工具"
        Me.運輸工具DataGridViewTextBoxColumn.Name = "運輸工具DataGridViewTextBoxColumn"
        '
        '距離DataGridViewTextBoxColumn
        '
        Me.距離DataGridViewTextBoxColumn.DataPropertyName = "距離"
        Me.距離DataGridViewTextBoxColumn.HeaderText = "距離"
        Me.距離DataGridViewTextBoxColumn.Name = "距離DataGridViewTextBoxColumn"
        '
        '耗油LDataGridViewTextBoxColumn
        '
        Me.耗油LDataGridViewTextBoxColumn.DataPropertyName = "耗油/L"
        Me.耗油LDataGridViewTextBoxColumn.HeaderText = "耗油/L"
        Me.耗油LDataGridViewTextBoxColumn.Name = "耗油LDataGridViewTextBoxColumn"
        '
        '油耗係數DataGridViewTextBoxColumn
        '
        Me.油耗係數DataGridViewTextBoxColumn.DataPropertyName = "油耗係數"
        Me.油耗係數DataGridViewTextBoxColumn.HeaderText = "油耗係數"
        Me.油耗係數DataGridViewTextBoxColumn.Name = "油耗係數DataGridViewTextBoxColumn"
        '
        '水足跡DataGridViewTextBoxColumn
        '
        Me.水足跡DataGridViewTextBoxColumn.DataPropertyName = "水足跡"
        Me.水足跡DataGridViewTextBoxColumn.HeaderText = "水足跡"
        Me.水足跡DataGridViewTextBoxColumn.Name = "水足跡DataGridViewTextBoxColumn"
        '
        '貨物重量DataGridViewTextBoxColumn
        '
        Me.貨物重量DataGridViewTextBoxColumn.DataPropertyName = "貨物重量"
        Me.貨物重量DataGridViewTextBoxColumn.HeaderText = "貨物重量"
        Me.貨物重量DataGridViewTextBoxColumn.Name = "貨物重量DataGridViewTextBoxColumn"
        '
        '已選運輸資訊BindingSource
        '
        Me.已選運輸資訊BindingSource.DataMember = "已選運輸資訊"
        Me.已選運輸資訊BindingSource.DataSource = Me.WFEDataSet4
        '
        'WFEDataSet4
        '
        Me.WFEDataSet4.DataSetName = "WFEDataSet4"
        Me.WFEDataSet4.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Button3
        '
        Me.Button3.Location = New System.Drawing.Point(128, 435)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(111, 30)
        Me.Button3.TabIndex = 2
        Me.Button3.Text = "完成"
        Me.Button3.UseVisualStyleBackColor = True
        '
        '運輸工具TableAdapter
        '
        Me.運輸工具TableAdapter.ClearBeforeFill = True
        '
        'WFEDataSet3
        '
        Me.WFEDataSet3.DataSetName = "WFEDataSet3"
        Me.WFEDataSet3.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        '已選運輸資訊TableAdapter
        '
        Me.已選運輸資訊TableAdapter.ClearBeforeFill = True
        '
        'Form2
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(362, 480)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Name = "Form2"
        Me.Text = "Water Footprint Evaluator"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.運輸工具BindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.WFEDataSet2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        CType(Me.DataGridView2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.已選運輸資訊BindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.WFEDataSet4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.WFEDataSet3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents GroupBox1 As Windows.Forms.GroupBox
    Friend WithEvents Label3 As Windows.Forms.Label
    Friend WithEvents Label2 As Windows.Forms.Label
    Friend WithEvents Label1 As Windows.Forms.Label
    Friend WithEvents DataGridView1 As Windows.Forms.DataGridView
    Friend WithEvents TextBox2 As Windows.Forms.TextBox
    Friend WithEvents TextBox1 As Windows.Forms.TextBox
    Friend WithEvents Button1 As Windows.Forms.Button
    Friend WithEvents Label4 As Windows.Forms.Label
    Friend WithEvents GroupBox2 As Windows.Forms.GroupBox
    Friend WithEvents Button2 As Windows.Forms.Button
    Friend WithEvents DataGridView2 As Windows.Forms.DataGridView
    Friend WithEvents Button3 As Windows.Forms.Button
    Friend WithEvents WFEDataSet2 As WFEDataSet2
    Friend WithEvents 運輸工具BindingSource As Windows.Forms.BindingSource
    Friend WithEvents 運輸工具TableAdapter As WFEDataSet2TableAdapters.運輸工具TableAdapter
    Friend WithEvents 識別碼DataGridViewTextBoxColumn As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents 種類DataGridViewTextBoxColumn As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents 計算單位DataGridViewTextBoxColumn As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents 最大載貨重量DataGridViewTextBoxColumn As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents 總水足跡m3DataGridViewTextBoxColumn As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents 藍水m3DataGridViewTextBoxColumn As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents 綠水m3DataGridViewTextBoxColumn As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents 灰水m3DataGridViewTextBoxColumn As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents 單位1DataGridViewTextBoxColumn As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents 能源單位1DataGridViewTextBoxColumn As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents 能耗量1DataGridViewTextBoxColumn As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents 耗能係數1DataGridViewTextBoxColumn As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents WFEDataSet3 As WFEDataSet3
    Friend WithEvents WFEDataSet4 As WFEDataSet4
    Friend WithEvents 已選運輸資訊BindingSource As Windows.Forms.BindingSource
    Friend WithEvents 已選運輸資訊TableAdapter As WFEDataSet4TableAdapters.已選運輸資訊TableAdapter
    Friend WithEvents 識別碼DataGridViewTextBoxColumn1 As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents 運輸工具DataGridViewTextBoxColumn As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents 距離DataGridViewTextBoxColumn As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents 耗油LDataGridViewTextBoxColumn As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents 油耗係數DataGridViewTextBoxColumn As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents 水足跡DataGridViewTextBoxColumn As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents 貨物重量DataGridViewTextBoxColumn As Windows.Forms.DataGridViewTextBoxColumn
End Class
