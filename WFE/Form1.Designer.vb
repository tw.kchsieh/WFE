﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form 覆寫 Dispose 以清除元件清單。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    '為 Windows Form 設計工具的必要項
    Private components As System.ComponentModel.IContainer

    '注意: 以下為 Windows Form 設計工具所需的程序
    '可以使用 Windows Form 設計工具進行修改。
    '請勿使用程式碼編輯器進行修改。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
		Me.components = New System.ComponentModel.Container()
		Me.TabPage6 = New System.Windows.Forms.TabPage()
		Me.Button23 = New System.Windows.Forms.Button()
		Me.Button22 = New System.Windows.Forms.Button()
		Me.RadioButton3 = New System.Windows.Forms.RadioButton()
		Me.GroupBox14 = New System.Windows.Forms.GroupBox()
		Me.Label53 = New System.Windows.Forms.Label()
		Me.Label52 = New System.Windows.Forms.Label()
		Me.TextBox27 = New System.Windows.Forms.TextBox()
		Me.TextBox26 = New System.Windows.Forms.TextBox()
		Me.TextBox25 = New System.Windows.Forms.TextBox()
		Me.Label51 = New System.Windows.Forms.Label()
		Me.Label50 = New System.Windows.Forms.Label()
		Me.Label49 = New System.Windows.Forms.Label()
		Me.DataGridView10 = New System.Windows.Forms.DataGridView()
		Me.識別碼DataGridViewTextBoxColumn9 = New System.Windows.Forms.DataGridViewTextBoxColumn()
		Me.種類DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
		Me.計算單位DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
		Me.最大載貨重量DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
		Me.總水足跡m3DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
		Me.藍水m3DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
		Me.綠水m3DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
		Me.灰水m3DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
		Me.單位1DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
		Me.能源單位1DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
		Me.能耗量1DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
		Me.耗能係數1DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
		Me.運輸工具BindingSource = New System.Windows.Forms.BindingSource(Me.components)
		Me.WFEDataSet2 = New WFE.WFEDataSet2()
		Me.GroupBox13 = New System.Windows.Forms.GroupBox()
		Me.Label48 = New System.Windows.Forms.Label()
		Me.TextBox24 = New System.Windows.Forms.TextBox()
		Me.TextBox23 = New System.Windows.Forms.TextBox()
		Me.TextBox22 = New System.Windows.Forms.TextBox()
		Me.Label47 = New System.Windows.Forms.Label()
		Me.Label46 = New System.Windows.Forms.Label()
		Me.Label45 = New System.Windows.Forms.Label()
		Me.DataGridView9 = New System.Windows.Forms.DataGridView()
		Me.識別碼DataGridViewTextBoxColumn8 = New System.Windows.Forms.DataGridViewTextBoxColumn()
		Me.方式DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
		Me.計算單位DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
		Me.水足跡m3DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
		Me.藍水m3DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
		Me.綠水m3DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
		Me.灰水m3DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
		Me.機具名稱DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
		Me.能源單位DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
		Me.耗能時DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
		Me.加工方式BindingSource = New System.Windows.Forms.BindingSource(Me.components)
		Me.WFEDataSet5 = New WFE.WFEDataSet5()
		Me.RadioButton2 = New System.Windows.Forms.RadioButton()
		Me.RadioButton1 = New System.Windows.Forms.RadioButton()
		Me.GroupBox12 = New System.Windows.Forms.GroupBox()
		Me.Label44 = New System.Windows.Forms.Label()
		Me.TextBox21 = New System.Windows.Forms.TextBox()
		Me.Label42 = New System.Windows.Forms.Label()
		Me.Label43 = New System.Windows.Forms.Label()
		Me.TextBox20 = New System.Windows.Forms.TextBox()
		Me.TextBox19 = New System.Windows.Forms.TextBox()
		Me.Label41 = New System.Windows.Forms.Label()
		Me.Label40 = New System.Windows.Forms.Label()
		Me.DataGridView8 = New System.Windows.Forms.DataGridView()
		Me.識別碼DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn()
		Me.材料DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
		Me.密度kgm3DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
		Me.藍水m3DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
		Me.綠水m3DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
		Me.灰水m3DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
		Me.總水足跡m3DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
		Me.材料BindingSource = New System.Windows.Forms.BindingSource(Me.components)
		Me.WFEDataSet = New WFE.WFEDataSet()
		Me.TabPage5 = New System.Windows.Forms.TabPage()
		Me.Button21 = New System.Windows.Forms.Button()
		Me.Label39 = New System.Windows.Forms.Label()
		Me.Label38 = New System.Windows.Forms.Label()
		Me.Label37 = New System.Windows.Forms.Label()
		Me.TextBox18 = New System.Windows.Forms.TextBox()
		Me.TextBox17 = New System.Windows.Forms.TextBox()
		Me.TextBox16 = New System.Windows.Forms.TextBox()
		Me.Label36 = New System.Windows.Forms.Label()
		Me.TextBox15 = New System.Windows.Forms.TextBox()
		Me.Label35 = New System.Windows.Forms.Label()
		Me.Label34 = New System.Windows.Forms.Label()
		Me.Label33 = New System.Windows.Forms.Label()
		Me.Label32 = New System.Windows.Forms.Label()
		Me.Label31 = New System.Windows.Forms.Label()
		Me.Label30 = New System.Windows.Forms.Label()
		Me.Label29 = New System.Windows.Forms.Label()
		Me.Label28 = New System.Windows.Forms.Label()
		Me.PictureBox1 = New System.Windows.Forms.PictureBox()
		Me.TabPage3 = New System.Windows.Forms.TabPage()
		Me.Button20 = New System.Windows.Forms.Button()
		Me.TextBox14 = New System.Windows.Forms.TextBox()
		Me.Label27 = New System.Windows.Forms.Label()
		Me.Label26 = New System.Windows.Forms.Label()
		Me.GroupBox11 = New System.Windows.Forms.GroupBox()
		Me.Button19 = New System.Windows.Forms.Button()
		Me.DataGridView7 = New System.Windows.Forms.DataGridView()
		Me.識別碼DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn()
		Me.零件名稱DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
		Me.材料DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
		Me.質量DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
		Me.碳足跡DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
		Me.加工方式DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
		Me.特殊零件DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
		Me.已選特殊零件BindingSource = New System.Windows.Forms.BindingSource(Me.components)
		Me.WFEDataSet11 = New WFE.WFEDataSet11()
		Me.GroupBox10 = New System.Windows.Forms.GroupBox()
		Me.Label25 = New System.Windows.Forms.Label()
		Me.Label24 = New System.Windows.Forms.Label()
		Me.TextBox13 = New System.Windows.Forms.TextBox()
		Me.TextBox12 = New System.Windows.Forms.TextBox()
		Me.Button18 = New System.Windows.Forms.Button()
		Me.Button17 = New System.Windows.Forms.Button()
		Me.Button16 = New System.Windows.Forms.Button()
		Me.DataGridView6 = New System.Windows.Forms.DataGridView()
		Me.識別碼DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
		Me.製程DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
		Me.藍水m3DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
		Me.綠水m3DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
		Me.灰水m3DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
		Me.總水足跡m3DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
		Me.電子元件製程BindingSource = New System.Windows.Forms.BindingSource(Me.components)
		Me.WFEDataSet10 = New WFE.WFEDataSet10()
		Me.TextBox11 = New System.Windows.Forms.TextBox()
		Me.Label23 = New System.Windows.Forms.Label()
		Me.Label22 = New System.Windows.Forms.Label()
		Me.Label21 = New System.Windows.Forms.Label()
		Me.Label20 = New System.Windows.Forms.Label()
		Me.GroupBox9 = New System.Windows.Forms.GroupBox()
		Me.Label19 = New System.Windows.Forms.Label()
		Me.Label18 = New System.Windows.Forms.Label()
		Me.Button15 = New System.Windows.Forms.Button()
		Me.Button14 = New System.Windows.Forms.Button()
		Me.Button13 = New System.Windows.Forms.Button()
		Me.TextBox10 = New System.Windows.Forms.TextBox()
		Me.TextBox9 = New System.Windows.Forms.TextBox()
		Me.DataGridView5 = New System.Windows.Forms.DataGridView()
		Me.識別碼DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
		Me.製程DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
		Me.藍水m3DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
		Me.綠水m3DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
		Me.灰水m3DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
		Me.總水足跡m3DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
		Me.電路板製程BindingSource = New System.Windows.Forms.BindingSource(Me.components)
		Me.WFEDataSet9 = New WFE.WFEDataSet9()
		Me.TextBox8 = New System.Windows.Forms.TextBox()
		Me.Label17 = New System.Windows.Forms.Label()
		Me.Label16 = New System.Windows.Forms.Label()
		Me.Label15 = New System.Windows.Forms.Label()
		Me.Label14 = New System.Windows.Forms.Label()
		Me.TabPage2 = New System.Windows.Forms.TabPage()
		Me.Button12 = New System.Windows.Forms.Button()
		Me.Label13 = New System.Windows.Forms.Label()
		Me.TextBox7 = New System.Windows.Forms.TextBox()
		Me.Label12 = New System.Windows.Forms.Label()
		Me.GroupBox8 = New System.Windows.Forms.GroupBox()
		Me.Button11 = New System.Windows.Forms.Button()
		Me.DataGridView4 = New System.Windows.Forms.DataGridView()
		Me.識別碼DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
		Me.方式DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
		Me.耗電時DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
		Me.數量DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
		Me.計算單位DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
		Me.係數DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
		Me.水足跡DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
		Me.已選加工資訊BindingSource = New System.Windows.Forms.BindingSource(Me.components)
		Me.WFEDataSet8 = New WFE.WFEDataSet8()
		Me.GroupBox7 = New System.Windows.Forms.GroupBox()
		Me.Button10 = New System.Windows.Forms.Button()
		Me.GroupBox6 = New System.Windows.Forms.GroupBox()
		Me.Button9 = New System.Windows.Forms.Button()
		Me.GroupBox5 = New System.Windows.Forms.GroupBox()
		Me.Button8 = New System.Windows.Forms.Button()
		Me.Label11 = New System.Windows.Forms.Label()
		Me.TextBox6 = New System.Windows.Forms.TextBox()
		Me.DataGridView3 = New System.Windows.Forms.DataGridView()
		Me.識別碼DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
		Me.零件名稱DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
		Me.材料DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
		Me.質量DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
		Me.水足跡DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
		Me.加工方式DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
		Me.已選一般零件BindingSource = New System.Windows.Forms.BindingSource(Me.components)
		Me.WFEDataSet1 = New WFE.WFEDataSet1()
		Me.TabPage1 = New System.Windows.Forms.TabPage()
		Me.Button7 = New System.Windows.Forms.Button()
		Me.Label10 = New System.Windows.Forms.Label()
		Me.TextBox5 = New System.Windows.Forms.TextBox()
		Me.Label9 = New System.Windows.Forms.Label()
		Me.GroupBox4 = New System.Windows.Forms.GroupBox()
		Me.Button6 = New System.Windows.Forms.Button()
		Me.DataGridView2 = New System.Windows.Forms.DataGridView()
		Me.識別碼DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
		Me.零件名稱DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
		Me.材料DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
		Me.質量DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
		Me.水足跡DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
		Me.加工方式DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
		Me.GroupBox3 = New System.Windows.Forms.GroupBox()
		Me.Button5 = New System.Windows.Forms.Button()
		Me.GroupBox2 = New System.Windows.Forms.GroupBox()
		Me.Button4 = New System.Windows.Forms.Button()
		Me.GroupBox1 = New System.Windows.Forms.GroupBox()
		Me.Label8 = New System.Windows.Forms.Label()
		Me.Label7 = New System.Windows.Forms.Label()
		Me.Label6 = New System.Windows.Forms.Label()
		Me.Button3 = New System.Windows.Forms.Button()
		Me.Button2 = New System.Windows.Forms.Button()
		Me.Button1 = New System.Windows.Forms.Button()
		Me.TextBox4 = New System.Windows.Forms.TextBox()
		Me.TextBox3 = New System.Windows.Forms.TextBox()
		Me.TextBox2 = New System.Windows.Forms.TextBox()
		Me.Label5 = New System.Windows.Forms.Label()
		Me.TextBox1 = New System.Windows.Forms.TextBox()
		Me.DataGridView1 = New System.Windows.Forms.DataGridView()
		Me.識別碼DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
		Me.材料DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
		Me.密度kgm3DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
		Me.藍水m3DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
		Me.綠水m3DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
		Me.灰水m3DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
		Me.總水足跡m3DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
		Me.Label4 = New System.Windows.Forms.Label()
		Me.Label3 = New System.Windows.Forms.Label()
		Me.Label2 = New System.Windows.Forms.Label()
		Me.Label1 = New System.Windows.Forms.Label()
		Me.TabControl1 = New System.Windows.Forms.TabControl()
		Me.材料TableAdapter = New WFE.WFEDataSetTableAdapters.材料TableAdapter()
		Me.已選一般零件TableAdapter = New WFE.WFEDataSet1TableAdapters.已選一般零件TableAdapter()
		Me.已選加工資訊TableAdapter = New WFE.WFEDataSet8TableAdapters.已選加工資訊TableAdapter()
		Me.電路板製程TableAdapter = New WFE.WFEDataSet9TableAdapters.電路板製程TableAdapter()
		Me.電子元件製程TableAdapter = New WFE.WFEDataSet10TableAdapters.電子元件製程TableAdapter()
		Me.已選特殊零件TableAdapter = New WFE.WFEDataSet11TableAdapters.已選特殊零件TableAdapter()
		Me.加工方式TableAdapter = New WFE.WFEDataSet5TableAdapters.加工方式TableAdapter()
		Me.運輸工具TableAdapter = New WFE.WFEDataSet2TableAdapters.運輸工具TableAdapter()
		Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
		Me.TabControl2 = New System.Windows.Forms.TabControl()
		Me.TabPage4 = New System.Windows.Forms.TabPage()
		Me.TabPage7 = New System.Windows.Forms.TabPage()
		Me.TabPage8 = New System.Windows.Forms.TabPage()
		Me.TextBox28 = New System.Windows.Forms.TextBox()
		Me.Label54 = New System.Windows.Forms.Label()
		Me.Button24 = New System.Windows.Forms.Button()
		Me.TabControl3 = New System.Windows.Forms.TabControl()
		Me.TabPage9 = New System.Windows.Forms.TabPage()
		Me.TabPage10 = New System.Windows.Forms.TabPage()
		Me.TabPage11 = New System.Windows.Forms.TabPage()
		Me.GroupBox15 = New System.Windows.Forms.GroupBox()
		Me.GroupBox16 = New System.Windows.Forms.GroupBox()
		Me.GroupBox17 = New System.Windows.Forms.GroupBox()
		Me.GroupBox18 = New System.Windows.Forms.GroupBox()
		Me.Button25 = New System.Windows.Forms.Button()
		Me.TabPage6.SuspendLayout()
		Me.GroupBox14.SuspendLayout()
		CType(Me.DataGridView10, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.運輸工具BindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.WFEDataSet2, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.GroupBox13.SuspendLayout()
		CType(Me.DataGridView9, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.加工方式BindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.WFEDataSet5, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.GroupBox12.SuspendLayout()
		CType(Me.DataGridView8, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.材料BindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.WFEDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.TabPage5.SuspendLayout()
		CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.TabPage3.SuspendLayout()
		Me.GroupBox11.SuspendLayout()
		CType(Me.DataGridView7, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.已選特殊零件BindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.WFEDataSet11, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.GroupBox10.SuspendLayout()
		CType(Me.DataGridView6, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.電子元件製程BindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.WFEDataSet10, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.GroupBox9.SuspendLayout()
		CType(Me.DataGridView5, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.電路板製程BindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.WFEDataSet9, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.TabPage2.SuspendLayout()
		Me.GroupBox8.SuspendLayout()
		CType(Me.DataGridView4, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.已選加工資訊BindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.WFEDataSet8, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.GroupBox7.SuspendLayout()
		Me.GroupBox6.SuspendLayout()
		Me.GroupBox5.SuspendLayout()
		CType(Me.DataGridView3, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.已選一般零件BindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.WFEDataSet1, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.TabPage1.SuspendLayout()
		Me.GroupBox4.SuspendLayout()
		CType(Me.DataGridView2, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.GroupBox3.SuspendLayout()
		Me.GroupBox2.SuspendLayout()
		Me.GroupBox1.SuspendLayout()
		CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.TabControl1.SuspendLayout()
		Me.FlowLayoutPanel1.SuspendLayout()
		Me.TabControl2.SuspendLayout()
		Me.TabPage4.SuspendLayout()
		Me.TabControl3.SuspendLayout()
		Me.TabPage9.SuspendLayout()
		Me.SuspendLayout()
		'
		'TabPage6
		'
		Me.TabPage6.Controls.Add(Me.Button23)
		Me.TabPage6.Controls.Add(Me.Button22)
		Me.TabPage6.Controls.Add(Me.RadioButton3)
		Me.TabPage6.Controls.Add(Me.GroupBox14)
		Me.TabPage6.Controls.Add(Me.GroupBox13)
		Me.TabPage6.Controls.Add(Me.RadioButton2)
		Me.TabPage6.Controls.Add(Me.RadioButton1)
		Me.TabPage6.Controls.Add(Me.GroupBox12)
		Me.TabPage6.Location = New System.Drawing.Point(4, 25)
		Me.TabPage6.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
		Me.TabPage6.Name = "TabPage6"
		Me.TabPage6.Size = New System.Drawing.Size(328, 581)
		Me.TabPage6.TabIndex = 5
		Me.TabPage6.Text = "E.資料新增/刪除"
		Me.TabPage6.UseVisualStyleBackColor = True
		'
		'Button23
		'
		Me.Button23.Location = New System.Drawing.Point(152, 543)
		Me.Button23.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
		Me.Button23.Name = "Button23"
		Me.Button23.Size = New System.Drawing.Size(82, 27)
		Me.Button23.TabIndex = 7
		Me.Button23.Text = "刪除"
		Me.Button23.UseVisualStyleBackColor = True
		'
		'Button22
		'
		Me.Button22.Location = New System.Drawing.Point(48, 543)
		Me.Button22.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
		Me.Button22.Name = "Button22"
		Me.Button22.Size = New System.Drawing.Size(82, 27)
		Me.Button22.TabIndex = 6
		Me.Button22.Text = "新增"
		Me.Button22.UseVisualStyleBackColor = True
		'
		'RadioButton3
		'
		Me.RadioButton3.AutoSize = True
		Me.RadioButton3.Location = New System.Drawing.Point(11, 430)
		Me.RadioButton3.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
		Me.RadioButton3.Name = "RadioButton3"
		Me.RadioButton3.Size = New System.Drawing.Size(14, 13)
		Me.RadioButton3.TabIndex = 5
		Me.RadioButton3.TabStop = True
		Me.RadioButton3.UseVisualStyleBackColor = True
		'
		'GroupBox14
		'
		Me.GroupBox14.Controls.Add(Me.Label53)
		Me.GroupBox14.Controls.Add(Me.Label52)
		Me.GroupBox14.Controls.Add(Me.TextBox27)
		Me.GroupBox14.Controls.Add(Me.TextBox26)
		Me.GroupBox14.Controls.Add(Me.TextBox25)
		Me.GroupBox14.Controls.Add(Me.Label51)
		Me.GroupBox14.Controls.Add(Me.Label50)
		Me.GroupBox14.Controls.Add(Me.Label49)
		Me.GroupBox14.Controls.Add(Me.DataGridView10)
		Me.GroupBox14.Location = New System.Drawing.Point(28, 366)
		Me.GroupBox14.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
		Me.GroupBox14.Name = "GroupBox14"
		Me.GroupBox14.Padding = New System.Windows.Forms.Padding(2, 2, 2, 2)
		Me.GroupBox14.Size = New System.Drawing.Size(241, 173)
		Me.GroupBox14.TabIndex = 4
		Me.GroupBox14.TabStop = False
		Me.GroupBox14.Text = "運輸工具"
		'
		'Label53
		'
		Me.Label53.AutoSize = True
		Me.Label53.Location = New System.Drawing.Point(184, 147)
		Me.Label53.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
		Me.Label53.Name = "Label53"
		Me.Label53.Size = New System.Drawing.Size(35, 16)
		Me.Label53.TabIndex = 21
		Me.Label53.Text = "m^3"
		'
		'Label52
		'
		Me.Label52.AutoSize = True
		Me.Label52.Location = New System.Drawing.Point(184, 121)
		Me.Label52.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
		Me.Label52.Name = "Label52"
		Me.Label52.Size = New System.Drawing.Size(23, 16)
		Me.Label52.TabIndex = 20
		Me.Label52.Text = "Kg"
		'
		'TextBox27
		'
		Me.TextBox27.Location = New System.Drawing.Point(66, 145)
		Me.TextBox27.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
		Me.TextBox27.Name = "TextBox27"
		Me.TextBox27.Size = New System.Drawing.Size(111, 23)
		Me.TextBox27.TabIndex = 19
		'
		'TextBox26
		'
		Me.TextBox26.Location = New System.Drawing.Point(91, 118)
		Me.TextBox26.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
		Me.TextBox26.Name = "TextBox26"
		Me.TextBox26.Size = New System.Drawing.Size(86, 23)
		Me.TextBox26.TabIndex = 18
		'
		'TextBox25
		'
		Me.TextBox25.Location = New System.Drawing.Point(66, 91)
		Me.TextBox25.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
		Me.TextBox25.Name = "TextBox25"
		Me.TextBox25.Size = New System.Drawing.Size(111, 23)
		Me.TextBox25.TabIndex = 17
		'
		'Label51
		'
		Me.Label51.AutoSize = True
		Me.Label51.Location = New System.Drawing.Point(12, 144)
		Me.Label51.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
		Me.Label51.Name = "Label51"
		Me.Label51.Size = New System.Drawing.Size(44, 16)
		Me.Label51.TabIndex = 16
		Me.Label51.Text = "水足跡"
		'
		'Label50
		'
		Me.Label50.AutoSize = True
		Me.Label50.Location = New System.Drawing.Point(12, 121)
		Me.Label50.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
		Me.Label50.Name = "Label50"
		Me.Label50.Size = New System.Drawing.Size(80, 16)
		Me.Label50.TabIndex = 15
		Me.Label50.Text = "最大載貨重量"
		'
		'Label49
		'
		Me.Label49.AutoSize = True
		Me.Label49.Location = New System.Drawing.Point(12, 94)
		Me.Label49.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
		Me.Label49.Name = "Label49"
		Me.Label49.Size = New System.Drawing.Size(56, 16)
		Me.Label49.TabIndex = 14
		Me.Label49.Text = "運輸工具"
		'
		'DataGridView10
		'
		Me.DataGridView10.AutoGenerateColumns = False
		Me.DataGridView10.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
		Me.DataGridView10.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.識別碼DataGridViewTextBoxColumn9, Me.種類DataGridViewTextBoxColumn, Me.計算單位DataGridViewTextBoxColumn2, Me.最大載貨重量DataGridViewTextBoxColumn, Me.總水足跡m3DataGridViewTextBoxColumn4, Me.藍水m3DataGridViewTextBoxColumn5, Me.綠水m3DataGridViewTextBoxColumn5, Me.灰水m3DataGridViewTextBoxColumn5, Me.單位1DataGridViewTextBoxColumn, Me.能源單位1DataGridViewTextBoxColumn, Me.能耗量1DataGridViewTextBoxColumn, Me.耗能係數1DataGridViewTextBoxColumn})
		Me.DataGridView10.DataSource = Me.運輸工具BindingSource
		Me.DataGridView10.Location = New System.Drawing.Point(10, 21)
		Me.DataGridView10.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
		Me.DataGridView10.Name = "DataGridView10"
		Me.DataGridView10.RowTemplate.Height = 27
		Me.DataGridView10.Size = New System.Drawing.Size(220, 66)
		Me.DataGridView10.TabIndex = 0
		'
		'識別碼DataGridViewTextBoxColumn9
		'
		Me.識別碼DataGridViewTextBoxColumn9.DataPropertyName = "識別碼"
		Me.識別碼DataGridViewTextBoxColumn9.HeaderText = "識別碼"
		Me.識別碼DataGridViewTextBoxColumn9.Name = "識別碼DataGridViewTextBoxColumn9"
		'
		'種類DataGridViewTextBoxColumn
		'
		Me.種類DataGridViewTextBoxColumn.DataPropertyName = "種類"
		Me.種類DataGridViewTextBoxColumn.HeaderText = "種類"
		Me.種類DataGridViewTextBoxColumn.Name = "種類DataGridViewTextBoxColumn"
		'
		'計算單位DataGridViewTextBoxColumn2
		'
		Me.計算單位DataGridViewTextBoxColumn2.DataPropertyName = "計算單位"
		Me.計算單位DataGridViewTextBoxColumn2.HeaderText = "計算單位"
		Me.計算單位DataGridViewTextBoxColumn2.Name = "計算單位DataGridViewTextBoxColumn2"
		'
		'最大載貨重量DataGridViewTextBoxColumn
		'
		Me.最大載貨重量DataGridViewTextBoxColumn.DataPropertyName = "最大載貨重量"
		Me.最大載貨重量DataGridViewTextBoxColumn.HeaderText = "最大載貨重量"
		Me.最大載貨重量DataGridViewTextBoxColumn.Name = "最大載貨重量DataGridViewTextBoxColumn"
		'
		'總水足跡m3DataGridViewTextBoxColumn4
		'
		Me.總水足跡m3DataGridViewTextBoxColumn4.DataPropertyName = "總水足跡(m3)"
		Me.總水足跡m3DataGridViewTextBoxColumn4.HeaderText = "總水足跡(m3)"
		Me.總水足跡m3DataGridViewTextBoxColumn4.Name = "總水足跡m3DataGridViewTextBoxColumn4"
		'
		'藍水m3DataGridViewTextBoxColumn5
		'
		Me.藍水m3DataGridViewTextBoxColumn5.DataPropertyName = "藍水(m3)"
		Me.藍水m3DataGridViewTextBoxColumn5.HeaderText = "藍水(m3)"
		Me.藍水m3DataGridViewTextBoxColumn5.Name = "藍水m3DataGridViewTextBoxColumn5"
		'
		'綠水m3DataGridViewTextBoxColumn5
		'
		Me.綠水m3DataGridViewTextBoxColumn5.DataPropertyName = "綠水(m3)"
		Me.綠水m3DataGridViewTextBoxColumn5.HeaderText = "綠水(m3)"
		Me.綠水m3DataGridViewTextBoxColumn5.Name = "綠水m3DataGridViewTextBoxColumn5"
		'
		'灰水m3DataGridViewTextBoxColumn5
		'
		Me.灰水m3DataGridViewTextBoxColumn5.DataPropertyName = "灰水(m3)"
		Me.灰水m3DataGridViewTextBoxColumn5.HeaderText = "灰水(m3)"
		Me.灰水m3DataGridViewTextBoxColumn5.Name = "灰水m3DataGridViewTextBoxColumn5"
		'
		'單位1DataGridViewTextBoxColumn
		'
		Me.單位1DataGridViewTextBoxColumn.DataPropertyName = "單位1"
		Me.單位1DataGridViewTextBoxColumn.HeaderText = "單位1"
		Me.單位1DataGridViewTextBoxColumn.Name = "單位1DataGridViewTextBoxColumn"
		'
		'能源單位1DataGridViewTextBoxColumn
		'
		Me.能源單位1DataGridViewTextBoxColumn.DataPropertyName = "能源/單位1"
		Me.能源單位1DataGridViewTextBoxColumn.HeaderText = "能源/單位1"
		Me.能源單位1DataGridViewTextBoxColumn.Name = "能源單位1DataGridViewTextBoxColumn"
		'
		'能耗量1DataGridViewTextBoxColumn
		'
		Me.能耗量1DataGridViewTextBoxColumn.DataPropertyName = "能耗量1"
		Me.能耗量1DataGridViewTextBoxColumn.HeaderText = "能耗量1"
		Me.能耗量1DataGridViewTextBoxColumn.Name = "能耗量1DataGridViewTextBoxColumn"
		'
		'耗能係數1DataGridViewTextBoxColumn
		'
		Me.耗能係數1DataGridViewTextBoxColumn.DataPropertyName = "耗能係數1"
		Me.耗能係數1DataGridViewTextBoxColumn.HeaderText = "耗能係數1"
		Me.耗能係數1DataGridViewTextBoxColumn.Name = "耗能係數1DataGridViewTextBoxColumn"
		'
		'運輸工具BindingSource
		'
		Me.運輸工具BindingSource.DataMember = "運輸工具"
		Me.運輸工具BindingSource.DataSource = Me.WFEDataSet2
		'
		'WFEDataSet2
		'
		Me.WFEDataSet2.DataSetName = "WFEDataSet2"
		Me.WFEDataSet2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
		'
		'GroupBox13
		'
		Me.GroupBox13.Controls.Add(Me.Label48)
		Me.GroupBox13.Controls.Add(Me.TextBox24)
		Me.GroupBox13.Controls.Add(Me.TextBox23)
		Me.GroupBox13.Controls.Add(Me.TextBox22)
		Me.GroupBox13.Controls.Add(Me.Label47)
		Me.GroupBox13.Controls.Add(Me.Label46)
		Me.GroupBox13.Controls.Add(Me.Label45)
		Me.GroupBox13.Controls.Add(Me.DataGridView9)
		Me.GroupBox13.Location = New System.Drawing.Point(28, 192)
		Me.GroupBox13.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
		Me.GroupBox13.Name = "GroupBox13"
		Me.GroupBox13.Padding = New System.Windows.Forms.Padding(2, 2, 2, 2)
		Me.GroupBox13.Size = New System.Drawing.Size(241, 173)
		Me.GroupBox13.TabIndex = 3
		Me.GroupBox13.TabStop = False
		Me.GroupBox13.Text = "加工方式"
		'
		'Label48
		'
		Me.Label48.AutoSize = True
		Me.Label48.Location = New System.Drawing.Point(181, 150)
		Me.Label48.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
		Me.Label48.Name = "Label48"
		Me.Label48.Size = New System.Drawing.Size(35, 16)
		Me.Label48.TabIndex = 13
		Me.Label48.Text = "m^3"
		'
		'TextBox24
		'
		Me.TextBox24.Location = New System.Drawing.Point(66, 143)
		Me.TextBox24.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
		Me.TextBox24.Name = "TextBox24"
		Me.TextBox24.Size = New System.Drawing.Size(111, 23)
		Me.TextBox24.TabIndex = 12
		'
		'TextBox23
		'
		Me.TextBox23.Location = New System.Drawing.Point(66, 116)
		Me.TextBox23.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
		Me.TextBox23.Name = "TextBox23"
		Me.TextBox23.Size = New System.Drawing.Size(111, 23)
		Me.TextBox23.TabIndex = 11
		'
		'TextBox22
		'
		Me.TextBox22.Location = New System.Drawing.Point(66, 90)
		Me.TextBox22.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
		Me.TextBox22.Name = "TextBox22"
		Me.TextBox22.Size = New System.Drawing.Size(111, 23)
		Me.TextBox22.TabIndex = 10
		'
		'Label47
		'
		Me.Label47.AutoSize = True
		Me.Label47.Location = New System.Drawing.Point(12, 146)
		Me.Label47.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
		Me.Label47.Name = "Label47"
		Me.Label47.Size = New System.Drawing.Size(44, 16)
		Me.Label47.TabIndex = 9
		Me.Label47.Text = "水足跡"
		'
		'Label46
		'
		Me.Label46.AutoSize = True
		Me.Label46.Location = New System.Drawing.Point(12, 118)
		Me.Label46.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
		Me.Label46.Name = "Label46"
		Me.Label46.Size = New System.Drawing.Size(56, 16)
		Me.Label46.TabIndex = 8
		Me.Label46.Text = "計量單位"
		'
		'Label45
		'
		Me.Label45.AutoSize = True
		Me.Label45.Location = New System.Drawing.Point(12, 92)
		Me.Label45.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
		Me.Label45.Name = "Label45"
		Me.Label45.Size = New System.Drawing.Size(56, 16)
		Me.Label45.TabIndex = 7
		Me.Label45.Text = "加工方式"
		'
		'DataGridView9
		'
		Me.DataGridView9.AutoGenerateColumns = False
		Me.DataGridView9.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
		Me.DataGridView9.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.識別碼DataGridViewTextBoxColumn8, Me.方式DataGridViewTextBoxColumn1, Me.計算單位DataGridViewTextBoxColumn1, Me.水足跡m3DataGridViewTextBoxColumn, Me.藍水m3DataGridViewTextBoxColumn4, Me.綠水m3DataGridViewTextBoxColumn4, Me.灰水m3DataGridViewTextBoxColumn4, Me.機具名稱DataGridViewTextBoxColumn, Me.能源單位DataGridViewTextBoxColumn, Me.耗能時DataGridViewTextBoxColumn})
		Me.DataGridView9.DataSource = Me.加工方式BindingSource
		Me.DataGridView9.Location = New System.Drawing.Point(10, 21)
		Me.DataGridView9.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
		Me.DataGridView9.Name = "DataGridView9"
		Me.DataGridView9.RowTemplate.Height = 27
		Me.DataGridView9.Size = New System.Drawing.Size(220, 66)
		Me.DataGridView9.TabIndex = 6
		'
		'識別碼DataGridViewTextBoxColumn8
		'
		Me.識別碼DataGridViewTextBoxColumn8.DataPropertyName = "識別碼"
		Me.識別碼DataGridViewTextBoxColumn8.HeaderText = "識別碼"
		Me.識別碼DataGridViewTextBoxColumn8.Name = "識別碼DataGridViewTextBoxColumn8"
		'
		'方式DataGridViewTextBoxColumn1
		'
		Me.方式DataGridViewTextBoxColumn1.DataPropertyName = "方式"
		Me.方式DataGridViewTextBoxColumn1.HeaderText = "方式"
		Me.方式DataGridViewTextBoxColumn1.Name = "方式DataGridViewTextBoxColumn1"
		'
		'計算單位DataGridViewTextBoxColumn1
		'
		Me.計算單位DataGridViewTextBoxColumn1.DataPropertyName = "計算單位"
		Me.計算單位DataGridViewTextBoxColumn1.HeaderText = "計算單位"
		Me.計算單位DataGridViewTextBoxColumn1.Name = "計算單位DataGridViewTextBoxColumn1"
		'
		'水足跡m3DataGridViewTextBoxColumn
		'
		Me.水足跡m3DataGridViewTextBoxColumn.DataPropertyName = "水足跡(m3)"
		Me.水足跡m3DataGridViewTextBoxColumn.HeaderText = "水足跡(m3)"
		Me.水足跡m3DataGridViewTextBoxColumn.Name = "水足跡m3DataGridViewTextBoxColumn"
		'
		'藍水m3DataGridViewTextBoxColumn4
		'
		Me.藍水m3DataGridViewTextBoxColumn4.DataPropertyName = "藍水(m3)"
		Me.藍水m3DataGridViewTextBoxColumn4.HeaderText = "藍水(m3)"
		Me.藍水m3DataGridViewTextBoxColumn4.Name = "藍水m3DataGridViewTextBoxColumn4"
		'
		'綠水m3DataGridViewTextBoxColumn4
		'
		Me.綠水m3DataGridViewTextBoxColumn4.DataPropertyName = "綠水(m3)"
		Me.綠水m3DataGridViewTextBoxColumn4.HeaderText = "綠水(m3)"
		Me.綠水m3DataGridViewTextBoxColumn4.Name = "綠水m3DataGridViewTextBoxColumn4"
		'
		'灰水m3DataGridViewTextBoxColumn4
		'
		Me.灰水m3DataGridViewTextBoxColumn4.DataPropertyName = "灰水(m3)"
		Me.灰水m3DataGridViewTextBoxColumn4.HeaderText = "灰水(m3)"
		Me.灰水m3DataGridViewTextBoxColumn4.Name = "灰水m3DataGridViewTextBoxColumn4"
		'
		'機具名稱DataGridViewTextBoxColumn
		'
		Me.機具名稱DataGridViewTextBoxColumn.DataPropertyName = "機具名稱"
		Me.機具名稱DataGridViewTextBoxColumn.HeaderText = "機具名稱"
		Me.機具名稱DataGridViewTextBoxColumn.Name = "機具名稱DataGridViewTextBoxColumn"
		'
		'能源單位DataGridViewTextBoxColumn
		'
		Me.能源單位DataGridViewTextBoxColumn.DataPropertyName = "能源/單位"
		Me.能源單位DataGridViewTextBoxColumn.HeaderText = "能源/單位"
		Me.能源單位DataGridViewTextBoxColumn.Name = "能源單位DataGridViewTextBoxColumn"
		'
		'耗能時DataGridViewTextBoxColumn
		'
		Me.耗能時DataGridViewTextBoxColumn.DataPropertyName = "耗能 / 時"
		Me.耗能時DataGridViewTextBoxColumn.HeaderText = "耗能 / 時"
		Me.耗能時DataGridViewTextBoxColumn.Name = "耗能時DataGridViewTextBoxColumn"
		'
		'加工方式BindingSource
		'
		Me.加工方式BindingSource.DataMember = "加工方式"
		Me.加工方式BindingSource.DataSource = Me.WFEDataSet5
		'
		'WFEDataSet5
		'
		Me.WFEDataSet5.DataSetName = "WFEDataSet5"
		Me.WFEDataSet5.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
		'
		'RadioButton2
		'
		Me.RadioButton2.AutoSize = True
		Me.RadioButton2.Location = New System.Drawing.Point(11, 266)
		Me.RadioButton2.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
		Me.RadioButton2.Name = "RadioButton2"
		Me.RadioButton2.Size = New System.Drawing.Size(14, 13)
		Me.RadioButton2.TabIndex = 2
		Me.RadioButton2.TabStop = True
		Me.RadioButton2.UseVisualStyleBackColor = True
		'
		'RadioButton1
		'
		Me.RadioButton1.AutoSize = True
		Me.RadioButton1.Location = New System.Drawing.Point(11, 104)
		Me.RadioButton1.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
		Me.RadioButton1.Name = "RadioButton1"
		Me.RadioButton1.Size = New System.Drawing.Size(14, 13)
		Me.RadioButton1.TabIndex = 1
		Me.RadioButton1.TabStop = True
		Me.RadioButton1.UseVisualStyleBackColor = True
		'
		'GroupBox12
		'
		Me.GroupBox12.Controls.Add(Me.Label44)
		Me.GroupBox12.Controls.Add(Me.TextBox21)
		Me.GroupBox12.Controls.Add(Me.Label42)
		Me.GroupBox12.Controls.Add(Me.Label43)
		Me.GroupBox12.Controls.Add(Me.TextBox20)
		Me.GroupBox12.Controls.Add(Me.TextBox19)
		Me.GroupBox12.Controls.Add(Me.Label41)
		Me.GroupBox12.Controls.Add(Me.Label40)
		Me.GroupBox12.Controls.Add(Me.DataGridView8)
		Me.GroupBox12.Location = New System.Drawing.Point(28, 14)
		Me.GroupBox12.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
		Me.GroupBox12.Name = "GroupBox12"
		Me.GroupBox12.Padding = New System.Windows.Forms.Padding(2, 2, 2, 2)
		Me.GroupBox12.Size = New System.Drawing.Size(241, 173)
		Me.GroupBox12.TabIndex = 0
		Me.GroupBox12.TabStop = False
		Me.GroupBox12.Text = "材料"
		'
		'Label44
		'
		Me.Label44.AutoSize = True
		Me.Label44.Location = New System.Drawing.Point(181, 147)
		Me.Label44.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
		Me.Label44.Name = "Label44"
		Me.Label44.Size = New System.Drawing.Size(35, 16)
		Me.Label44.TabIndex = 8
		Me.Label44.Text = "m^3"
		'
		'TextBox21
		'
		Me.TextBox21.Location = New System.Drawing.Point(66, 145)
		Me.TextBox21.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
		Me.TextBox21.Name = "TextBox21"
		Me.TextBox21.Size = New System.Drawing.Size(111, 23)
		Me.TextBox21.TabIndex = 8
		'
		'Label42
		'
		Me.Label42.AutoSize = True
		Me.Label42.Location = New System.Drawing.Point(181, 124)
		Me.Label42.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
		Me.Label42.Name = "Label42"
		Me.Label42.Size = New System.Drawing.Size(55, 16)
		Me.Label42.TabIndex = 5
		Me.Label42.Text = "Kg/m^3"
		'
		'Label43
		'
		Me.Label43.AutoSize = True
		Me.Label43.Location = New System.Drawing.Point(12, 147)
		Me.Label43.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
		Me.Label43.Name = "Label43"
		Me.Label43.Size = New System.Drawing.Size(44, 16)
		Me.Label43.TabIndex = 7
		Me.Label43.Text = "水足跡"
		'
		'TextBox20
		'
		Me.TextBox20.Location = New System.Drawing.Point(66, 118)
		Me.TextBox20.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
		Me.TextBox20.Name = "TextBox20"
		Me.TextBox20.Size = New System.Drawing.Size(111, 23)
		Me.TextBox20.TabIndex = 4
		'
		'TextBox19
		'
		Me.TextBox19.Location = New System.Drawing.Point(66, 91)
		Me.TextBox19.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
		Me.TextBox19.Name = "TextBox19"
		Me.TextBox19.Size = New System.Drawing.Size(111, 23)
		Me.TextBox19.TabIndex = 3
		'
		'Label41
		'
		Me.Label41.AutoSize = True
		Me.Label41.Location = New System.Drawing.Point(12, 124)
		Me.Label41.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
		Me.Label41.Name = "Label41"
		Me.Label41.Size = New System.Drawing.Size(32, 16)
		Me.Label41.TabIndex = 2
		Me.Label41.Text = "密度"
		'
		'Label40
		'
		Me.Label40.AutoSize = True
		Me.Label40.Location = New System.Drawing.Point(9, 94)
		Me.Label40.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
		Me.Label40.Name = "Label40"
		Me.Label40.Size = New System.Drawing.Size(56, 16)
		Me.Label40.TabIndex = 1
		Me.Label40.Text = "材料名稱"
		'
		'DataGridView8
		'
		Me.DataGridView8.AutoGenerateColumns = False
		Me.DataGridView8.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
		Me.DataGridView8.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.識別碼DataGridViewTextBoxColumn7, Me.材料DataGridViewTextBoxColumn4, Me.密度kgm3DataGridViewTextBoxColumn1, Me.藍水m3DataGridViewTextBoxColumn3, Me.綠水m3DataGridViewTextBoxColumn3, Me.灰水m3DataGridViewTextBoxColumn3, Me.總水足跡m3DataGridViewTextBoxColumn3})
		Me.DataGridView8.DataSource = Me.材料BindingSource
		Me.DataGridView8.Location = New System.Drawing.Point(10, 21)
		Me.DataGridView8.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
		Me.DataGridView8.Name = "DataGridView8"
		Me.DataGridView8.RowTemplate.Height = 27
		Me.DataGridView8.Size = New System.Drawing.Size(220, 66)
		Me.DataGridView8.TabIndex = 0
		'
		'識別碼DataGridViewTextBoxColumn7
		'
		Me.識別碼DataGridViewTextBoxColumn7.DataPropertyName = "識別碼"
		Me.識別碼DataGridViewTextBoxColumn7.HeaderText = "識別碼"
		Me.識別碼DataGridViewTextBoxColumn7.Name = "識別碼DataGridViewTextBoxColumn7"
		'
		'材料DataGridViewTextBoxColumn4
		'
		Me.材料DataGridViewTextBoxColumn4.DataPropertyName = "材料"
		Me.材料DataGridViewTextBoxColumn4.HeaderText = "材料"
		Me.材料DataGridViewTextBoxColumn4.Name = "材料DataGridViewTextBoxColumn4"
		'
		'密度kgm3DataGridViewTextBoxColumn1
		'
		Me.密度kgm3DataGridViewTextBoxColumn1.DataPropertyName = "密度kg/m3"
		Me.密度kgm3DataGridViewTextBoxColumn1.HeaderText = "密度kg/m3"
		Me.密度kgm3DataGridViewTextBoxColumn1.Name = "密度kgm3DataGridViewTextBoxColumn1"
		'
		'藍水m3DataGridViewTextBoxColumn3
		'
		Me.藍水m3DataGridViewTextBoxColumn3.DataPropertyName = "藍水(m3)"
		Me.藍水m3DataGridViewTextBoxColumn3.HeaderText = "藍水(m3)"
		Me.藍水m3DataGridViewTextBoxColumn3.Name = "藍水m3DataGridViewTextBoxColumn3"
		'
		'綠水m3DataGridViewTextBoxColumn3
		'
		Me.綠水m3DataGridViewTextBoxColumn3.DataPropertyName = "綠水(m3)"
		Me.綠水m3DataGridViewTextBoxColumn3.HeaderText = "綠水(m3)"
		Me.綠水m3DataGridViewTextBoxColumn3.Name = "綠水m3DataGridViewTextBoxColumn3"
		'
		'灰水m3DataGridViewTextBoxColumn3
		'
		Me.灰水m3DataGridViewTextBoxColumn3.DataPropertyName = "灰水(m3)"
		Me.灰水m3DataGridViewTextBoxColumn3.HeaderText = "灰水(m3)"
		Me.灰水m3DataGridViewTextBoxColumn3.Name = "灰水m3DataGridViewTextBoxColumn3"
		'
		'總水足跡m3DataGridViewTextBoxColumn3
		'
		Me.總水足跡m3DataGridViewTextBoxColumn3.DataPropertyName = "總水足跡 (m3)"
		Me.總水足跡m3DataGridViewTextBoxColumn3.HeaderText = "總水足跡 (m3)"
		Me.總水足跡m3DataGridViewTextBoxColumn3.Name = "總水足跡m3DataGridViewTextBoxColumn3"
		'
		'材料BindingSource
		'
		Me.材料BindingSource.DataMember = "材料"
		Me.材料BindingSource.DataSource = Me.WFEDataSet
		'
		'WFEDataSet
		'
		Me.WFEDataSet.DataSetName = "WFEDataSet"
		Me.WFEDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
		'
		'TabPage5
		'
		Me.TabPage5.Controls.Add(Me.Button21)
		Me.TabPage5.Controls.Add(Me.Label39)
		Me.TabPage5.Controls.Add(Me.Label38)
		Me.TabPage5.Controls.Add(Me.Label37)
		Me.TabPage5.Controls.Add(Me.TextBox18)
		Me.TabPage5.Controls.Add(Me.TextBox17)
		Me.TabPage5.Controls.Add(Me.TextBox16)
		Me.TabPage5.Controls.Add(Me.Label36)
		Me.TabPage5.Controls.Add(Me.TextBox15)
		Me.TabPage5.Controls.Add(Me.Label35)
		Me.TabPage5.Controls.Add(Me.Label34)
		Me.TabPage5.Controls.Add(Me.Label33)
		Me.TabPage5.Controls.Add(Me.Label32)
		Me.TabPage5.Controls.Add(Me.Label31)
		Me.TabPage5.Controls.Add(Me.Label30)
		Me.TabPage5.Controls.Add(Me.Label29)
		Me.TabPage5.Controls.Add(Me.Label28)
		Me.TabPage5.Controls.Add(Me.PictureBox1)
		Me.TabPage5.Location = New System.Drawing.Point(4, 25)
		Me.TabPage5.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
		Me.TabPage5.Name = "TabPage5"
		Me.TabPage5.Size = New System.Drawing.Size(328, 581)
		Me.TabPage5.TabIndex = 4
		Me.TabPage5.Text = "D.綜合分析"
		Me.TabPage5.UseVisualStyleBackColor = True
		'
		'Button21
		'
		Me.Button21.Location = New System.Drawing.Point(90, 547)
		Me.Button21.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
		Me.Button21.Name = "Button21"
		Me.Button21.Size = New System.Drawing.Size(118, 24)
		Me.Button21.TabIndex = 17
		Me.Button21.Text = "選擇產品"
		Me.Button21.UseVisualStyleBackColor = True
		'
		'Label39
		'
		Me.Label39.AutoSize = True
		Me.Label39.Location = New System.Drawing.Point(234, 514)
		Me.Label39.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
		Me.Label39.Name = "Label39"
		Me.Label39.Size = New System.Drawing.Size(35, 16)
		Me.Label39.TabIndex = 16
		Me.Label39.Text = "m^3"
		'
		'Label38
		'
		Me.Label38.AutoSize = True
		Me.Label38.Location = New System.Drawing.Point(234, 486)
		Me.Label38.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
		Me.Label38.Name = "Label38"
		Me.Label38.Size = New System.Drawing.Size(35, 16)
		Me.Label38.TabIndex = 15
		Me.Label38.Text = "m^3"
		'
		'Label37
		'
		Me.Label37.AutoSize = True
		Me.Label37.Location = New System.Drawing.Point(234, 459)
		Me.Label37.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
		Me.Label37.Name = "Label37"
		Me.Label37.Size = New System.Drawing.Size(35, 16)
		Me.Label37.TabIndex = 14
		Me.Label37.Text = "m^3"
		'
		'TextBox18
		'
		Me.TextBox18.Location = New System.Drawing.Point(115, 511)
		Me.TextBox18.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
		Me.TextBox18.Name = "TextBox18"
		Me.TextBox18.Size = New System.Drawing.Size(116, 23)
		Me.TextBox18.TabIndex = 13
		'
		'TextBox17
		'
		Me.TextBox17.Location = New System.Drawing.Point(115, 484)
		Me.TextBox17.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
		Me.TextBox17.Name = "TextBox17"
		Me.TextBox17.Size = New System.Drawing.Size(116, 23)
		Me.TextBox17.TabIndex = 12
		'
		'TextBox16
		'
		Me.TextBox16.Location = New System.Drawing.Point(115, 457)
		Me.TextBox16.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
		Me.TextBox16.Name = "TextBox16"
		Me.TextBox16.Size = New System.Drawing.Size(115, 23)
		Me.TextBox16.TabIndex = 11
		'
		'Label36
		'
		Me.Label36.AutoSize = True
		Me.Label36.Location = New System.Drawing.Point(234, 432)
		Me.Label36.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
		Me.Label36.Name = "Label36"
		Me.Label36.Size = New System.Drawing.Size(35, 16)
		Me.Label36.TabIndex = 10
		Me.Label36.Text = "m^3"
		'
		'TextBox15
		'
		Me.TextBox15.Location = New System.Drawing.Point(115, 430)
		Me.TextBox15.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
		Me.TextBox15.Name = "TextBox15"
		Me.TextBox15.Size = New System.Drawing.Size(116, 23)
		Me.TextBox15.TabIndex = 9
		'
		'Label35
		'
		Me.Label35.AutoSize = True
		Me.Label35.Location = New System.Drawing.Point(58, 511)
		Me.Label35.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
		Me.Label35.Name = "Label35"
		Me.Label35.Size = New System.Drawing.Size(56, 16)
		Me.Label35.TabIndex = 8
		Me.Label35.Text = "總水足跡"
		'
		'Label34
		'
		Me.Label34.AutoSize = True
		Me.Label34.Location = New System.Drawing.Point(14, 486)
		Me.Label34.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
		Me.Label34.Name = "Label34"
		Me.Label34.Size = New System.Drawing.Size(104, 16)
		Me.Label34.TabIndex = 7
		Me.Label34.Text = "階段三：特殊零件"
		'
		'Label33
		'
		Me.Label33.AutoSize = True
		Me.Label33.Location = New System.Drawing.Point(14, 459)
		Me.Label33.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
		Me.Label33.Name = "Label33"
		Me.Label33.Size = New System.Drawing.Size(104, 16)
		Me.Label33.TabIndex = 6
		Me.Label33.Text = "階段二：後製加工"
		'
		'Label32
		'
		Me.Label32.AutoSize = True
		Me.Label32.Location = New System.Drawing.Point(14, 432)
		Me.Label32.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
		Me.Label32.Name = "Label32"
		Me.Label32.Size = New System.Drawing.Size(104, 16)
		Me.Label32.TabIndex = 5
		Me.Label32.Text = "階段一：一般零件"
		'
		'Label31
		'
		Me.Label31.AutoSize = True
		Me.Label31.Location = New System.Drawing.Point(178, 398)
		Me.Label31.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
		Me.Label31.Name = "Label31"
		Me.Label31.Size = New System.Drawing.Size(44, 16)
		Me.Label31.TabIndex = 4
		Me.Label31.Text = "階段三"
		'
		'Label30
		'
		Me.Label30.AutoSize = True
		Me.Label30.Location = New System.Drawing.Point(119, 398)
		Me.Label30.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
		Me.Label30.Name = "Label30"
		Me.Label30.Size = New System.Drawing.Size(44, 16)
		Me.Label30.TabIndex = 3
		Me.Label30.Text = "階段二"
		'
		'Label29
		'
		Me.Label29.AutoSize = True
		Me.Label29.Location = New System.Drawing.Point(51, 398)
		Me.Label29.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
		Me.Label29.Name = "Label29"
		Me.Label29.Size = New System.Drawing.Size(44, 16)
		Me.Label29.TabIndex = 2
		Me.Label29.Text = "階段一"
		'
		'Label28
		'
		Me.Label28.AutoSize = True
		Me.Label28.Font = New System.Drawing.Font("Microsoft JhengHei", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
		Me.Label28.Location = New System.Drawing.Point(64, 15)
		Me.Label28.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
		Me.Label28.Name = "Label28"
		Me.Label28.Size = New System.Drawing.Size(153, 20)
		Me.Label28.TabIndex = 1
		Me.Label28.Text = "各階段水足跡百分比"
		'
		'PictureBox1
		'
		Me.PictureBox1.Location = New System.Drawing.Point(20, 38)
		Me.PictureBox1.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
		Me.PictureBox1.Name = "PictureBox1"
		Me.PictureBox1.Size = New System.Drawing.Size(237, 358)
		Me.PictureBox1.TabIndex = 0
		Me.PictureBox1.TabStop = False
		'
		'TabPage3
		'
		Me.TabPage3.Controls.Add(Me.Button20)
		Me.TabPage3.Controls.Add(Me.TextBox14)
		Me.TabPage3.Controls.Add(Me.Label27)
		Me.TabPage3.Controls.Add(Me.Label26)
		Me.TabPage3.Controls.Add(Me.GroupBox11)
		Me.TabPage3.Controls.Add(Me.GroupBox10)
		Me.TabPage3.Controls.Add(Me.GroupBox9)
		Me.TabPage3.Location = New System.Drawing.Point(4, 25)
		Me.TabPage3.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
		Me.TabPage3.Name = "TabPage3"
		Me.TabPage3.Size = New System.Drawing.Size(328, 581)
		Me.TabPage3.TabIndex = 2
		Me.TabPage3.Text = "C.特殊零件"
		Me.TabPage3.UseVisualStyleBackColor = True
		'
		'Button20
		'
		Me.Button20.Location = New System.Drawing.Point(80, 557)
		Me.Button20.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
		Me.Button20.Name = "Button20"
		Me.Button20.Size = New System.Drawing.Size(118, 24)
		Me.Button20.TabIndex = 6
		Me.Button20.Text = "完成此階段評估"
		Me.Button20.UseVisualStyleBackColor = True
		'
		'TextBox14
		'
		Me.TextBox14.Location = New System.Drawing.Point(63, 532)
		Me.TextBox14.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
		Me.TextBox14.Name = "TextBox14"
		Me.TextBox14.Size = New System.Drawing.Size(143, 23)
		Me.TextBox14.TabIndex = 5
		'
		'Label27
		'
		Me.Label27.AutoSize = True
		Me.Label27.Location = New System.Drawing.Point(209, 536)
		Me.Label27.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
		Me.Label27.Name = "Label27"
		Me.Label27.Size = New System.Drawing.Size(35, 16)
		Me.Label27.TabIndex = 4
		Me.Label27.Text = "m^3"
		'
		'Label26
		'
		Me.Label26.AutoSize = True
		Me.Label26.Location = New System.Drawing.Point(92, 514)
		Me.Label26.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
		Me.Label26.Name = "Label26"
		Me.Label26.Size = New System.Drawing.Size(104, 16)
		Me.Label26.TabIndex = 3
		Me.Label26.Text = "特殊零件總水足跡"
		'
		'GroupBox11
		'
		Me.GroupBox11.Controls.Add(Me.Button19)
		Me.GroupBox11.Controls.Add(Me.DataGridView7)
		Me.GroupBox11.Location = New System.Drawing.Point(14, 362)
		Me.GroupBox11.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
		Me.GroupBox11.Name = "GroupBox11"
		Me.GroupBox11.Padding = New System.Windows.Forms.Padding(2, 2, 2, 2)
		Me.GroupBox11.Size = New System.Drawing.Size(250, 149)
		Me.GroupBox11.TabIndex = 2
		Me.GroupBox11.TabStop = False
		Me.GroupBox11.Text = "已選特殊零件"
		'
		'Button19
		'
		Me.Button19.Location = New System.Drawing.Point(67, 118)
		Me.Button19.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
		Me.Button19.Name = "Button19"
		Me.Button19.Size = New System.Drawing.Size(118, 24)
		Me.Button19.TabIndex = 1
		Me.Button19.Text = "刪除資料"
		Me.Button19.UseVisualStyleBackColor = True
		'
		'DataGridView7
		'
		Me.DataGridView7.AutoGenerateColumns = False
		Me.DataGridView7.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
		Me.DataGridView7.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.識別碼DataGridViewTextBoxColumn6, Me.零件名稱DataGridViewTextBoxColumn2, Me.材料DataGridViewTextBoxColumn3, Me.質量DataGridViewTextBoxColumn2, Me.碳足跡DataGridViewTextBoxColumn, Me.加工方式DataGridViewTextBoxColumn2, Me.特殊零件DataGridViewTextBoxColumn})
		Me.DataGridView7.DataSource = Me.已選特殊零件BindingSource
		Me.DataGridView7.Location = New System.Drawing.Point(16, 21)
		Me.DataGridView7.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
		Me.DataGridView7.Name = "DataGridView7"
		Me.DataGridView7.RowTemplate.Height = 27
		Me.DataGridView7.Size = New System.Drawing.Size(218, 92)
		Me.DataGridView7.TabIndex = 0
		'
		'識別碼DataGridViewTextBoxColumn6
		'
		Me.識別碼DataGridViewTextBoxColumn6.DataPropertyName = "識別碼"
		Me.識別碼DataGridViewTextBoxColumn6.HeaderText = "識別碼"
		Me.識別碼DataGridViewTextBoxColumn6.Name = "識別碼DataGridViewTextBoxColumn6"
		'
		'零件名稱DataGridViewTextBoxColumn2
		'
		Me.零件名稱DataGridViewTextBoxColumn2.DataPropertyName = "零件名稱"
		Me.零件名稱DataGridViewTextBoxColumn2.HeaderText = "零件名稱"
		Me.零件名稱DataGridViewTextBoxColumn2.Name = "零件名稱DataGridViewTextBoxColumn2"
		'
		'材料DataGridViewTextBoxColumn3
		'
		Me.材料DataGridViewTextBoxColumn3.DataPropertyName = "材料"
		Me.材料DataGridViewTextBoxColumn3.HeaderText = "材料"
		Me.材料DataGridViewTextBoxColumn3.Name = "材料DataGridViewTextBoxColumn3"
		'
		'質量DataGridViewTextBoxColumn2
		'
		Me.質量DataGridViewTextBoxColumn2.DataPropertyName = "質量"
		Me.質量DataGridViewTextBoxColumn2.HeaderText = "質量"
		Me.質量DataGridViewTextBoxColumn2.Name = "質量DataGridViewTextBoxColumn2"
		'
		'碳足跡DataGridViewTextBoxColumn
		'
		Me.碳足跡DataGridViewTextBoxColumn.DataPropertyName = "碳足跡"
		Me.碳足跡DataGridViewTextBoxColumn.HeaderText = "碳足跡"
		Me.碳足跡DataGridViewTextBoxColumn.Name = "碳足跡DataGridViewTextBoxColumn"
		'
		'加工方式DataGridViewTextBoxColumn2
		'
		Me.加工方式DataGridViewTextBoxColumn2.DataPropertyName = "加工方式"
		Me.加工方式DataGridViewTextBoxColumn2.HeaderText = "加工方式"
		Me.加工方式DataGridViewTextBoxColumn2.Name = "加工方式DataGridViewTextBoxColumn2"
		'
		'特殊零件DataGridViewTextBoxColumn
		'
		Me.特殊零件DataGridViewTextBoxColumn.DataPropertyName = "特殊零件"
		Me.特殊零件DataGridViewTextBoxColumn.HeaderText = "特殊零件"
		Me.特殊零件DataGridViewTextBoxColumn.Name = "特殊零件DataGridViewTextBoxColumn"
		'
		'已選特殊零件BindingSource
		'
		Me.已選特殊零件BindingSource.DataMember = "已選特殊零件"
		Me.已選特殊零件BindingSource.DataSource = Me.WFEDataSet11
		'
		'WFEDataSet11
		'
		Me.WFEDataSet11.DataSetName = "WFEDataSet11"
		Me.WFEDataSet11.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
		'
		'GroupBox10
		'
		Me.GroupBox10.Controls.Add(Me.Label25)
		Me.GroupBox10.Controls.Add(Me.Label24)
		Me.GroupBox10.Controls.Add(Me.TextBox13)
		Me.GroupBox10.Controls.Add(Me.TextBox12)
		Me.GroupBox10.Controls.Add(Me.Button18)
		Me.GroupBox10.Controls.Add(Me.Button17)
		Me.GroupBox10.Controls.Add(Me.Button16)
		Me.GroupBox10.Controls.Add(Me.DataGridView6)
		Me.GroupBox10.Controls.Add(Me.TextBox11)
		Me.GroupBox10.Controls.Add(Me.Label23)
		Me.GroupBox10.Controls.Add(Me.Label22)
		Me.GroupBox10.Controls.Add(Me.Label21)
		Me.GroupBox10.Controls.Add(Me.Label20)
		Me.GroupBox10.Location = New System.Drawing.Point(14, 182)
		Me.GroupBox10.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
		Me.GroupBox10.Name = "GroupBox10"
		Me.GroupBox10.Padding = New System.Windows.Forms.Padding(2, 2, 2, 2)
		Me.GroupBox10.Size = New System.Drawing.Size(250, 175)
		Me.GroupBox10.TabIndex = 1
		Me.GroupBox10.TabStop = False
		Me.GroupBox10.Text = "選擇電子元件"
		'
		'Label25
		'
		Me.Label25.AutoSize = True
		Me.Label25.Location = New System.Drawing.Point(194, 148)
		Me.Label25.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
		Me.Label25.Name = "Label25"
		Me.Label25.Size = New System.Drawing.Size(35, 16)
		Me.Label25.TabIndex = 11
		Me.Label25.Text = "m^3"
		'
		'Label24
		'
		Me.Label24.AutoSize = True
		Me.Label24.Location = New System.Drawing.Point(190, 121)
		Me.Label24.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
		Me.Label24.Name = "Label24"
		Me.Label24.Size = New System.Drawing.Size(46, 16)
		Me.Label24.TabIndex = 10
		Me.Label24.Text = "mm^3"
		'
		'TextBox13
		'
		Me.TextBox13.Location = New System.Drawing.Point(50, 146)
		Me.TextBox13.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
		Me.TextBox13.Name = "TextBox13"
		Me.TextBox13.Size = New System.Drawing.Size(137, 23)
		Me.TextBox13.TabIndex = 9
		'
		'TextBox12
		'
		Me.TextBox12.Location = New System.Drawing.Point(50, 118)
		Me.TextBox12.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
		Me.TextBox12.Name = "TextBox12"
		Me.TextBox12.Size = New System.Drawing.Size(137, 23)
		Me.TextBox12.TabIndex = 8
		'
		'Button18
		'
		Me.Button18.Location = New System.Drawing.Point(190, 88)
		Me.Button18.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
		Me.Button18.Name = "Button18"
		Me.Button18.Size = New System.Drawing.Size(52, 27)
		Me.Button18.TabIndex = 7
		Me.Button18.Text = "刪除"
		Me.Button18.UseVisualStyleBackColor = True
		'
		'Button17
		'
		Me.Button17.Location = New System.Drawing.Point(190, 56)
		Me.Button17.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
		Me.Button17.Name = "Button17"
		Me.Button17.Size = New System.Drawing.Size(52, 27)
		Me.Button17.TabIndex = 6
		Me.Button17.Text = "確定"
		Me.Button17.UseVisualStyleBackColor = True
		'
		'Button16
		'
		Me.Button16.Location = New System.Drawing.Point(190, 24)
		Me.Button16.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
		Me.Button16.Name = "Button16"
		Me.Button16.Size = New System.Drawing.Size(52, 27)
		Me.Button16.TabIndex = 0
		Me.Button16.Text = "獲取"
		Me.Button16.UseVisualStyleBackColor = True
		'
		'DataGridView6
		'
		Me.DataGridView6.AutoGenerateColumns = False
		Me.DataGridView6.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
		Me.DataGridView6.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.識別碼DataGridViewTextBoxColumn5, Me.製程DataGridViewTextBoxColumn1, Me.藍水m3DataGridViewTextBoxColumn2, Me.綠水m3DataGridViewTextBoxColumn2, Me.灰水m3DataGridViewTextBoxColumn2, Me.總水足跡m3DataGridViewTextBoxColumn2})
		Me.DataGridView6.DataSource = Me.電子元件製程BindingSource
		Me.DataGridView6.Location = New System.Drawing.Point(50, 47)
		Me.DataGridView6.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
		Me.DataGridView6.Name = "DataGridView6"
		Me.DataGridView6.RowTemplate.Height = 27
		Me.DataGridView6.Size = New System.Drawing.Size(136, 66)
		Me.DataGridView6.TabIndex = 5
		'
		'識別碼DataGridViewTextBoxColumn5
		'
		Me.識別碼DataGridViewTextBoxColumn5.DataPropertyName = "識別碼"
		Me.識別碼DataGridViewTextBoxColumn5.HeaderText = "識別碼"
		Me.識別碼DataGridViewTextBoxColumn5.Name = "識別碼DataGridViewTextBoxColumn5"
		'
		'製程DataGridViewTextBoxColumn1
		'
		Me.製程DataGridViewTextBoxColumn1.DataPropertyName = "製程"
		Me.製程DataGridViewTextBoxColumn1.HeaderText = "製程"
		Me.製程DataGridViewTextBoxColumn1.Name = "製程DataGridViewTextBoxColumn1"
		'
		'藍水m3DataGridViewTextBoxColumn2
		'
		Me.藍水m3DataGridViewTextBoxColumn2.DataPropertyName = "藍水(m3)"
		Me.藍水m3DataGridViewTextBoxColumn2.HeaderText = "藍水(m3)"
		Me.藍水m3DataGridViewTextBoxColumn2.Name = "藍水m3DataGridViewTextBoxColumn2"
		'
		'綠水m3DataGridViewTextBoxColumn2
		'
		Me.綠水m3DataGridViewTextBoxColumn2.DataPropertyName = "綠水(m3)"
		Me.綠水m3DataGridViewTextBoxColumn2.HeaderText = "綠水(m3)"
		Me.綠水m3DataGridViewTextBoxColumn2.Name = "綠水m3DataGridViewTextBoxColumn2"
		'
		'灰水m3DataGridViewTextBoxColumn2
		'
		Me.灰水m3DataGridViewTextBoxColumn2.DataPropertyName = "灰水(m3)"
		Me.灰水m3DataGridViewTextBoxColumn2.HeaderText = "灰水(m3)"
		Me.灰水m3DataGridViewTextBoxColumn2.Name = "灰水m3DataGridViewTextBoxColumn2"
		'
		'總水足跡m3DataGridViewTextBoxColumn2
		'
		Me.總水足跡m3DataGridViewTextBoxColumn2.DataPropertyName = "總水足跡(m3)"
		Me.總水足跡m3DataGridViewTextBoxColumn2.HeaderText = "總水足跡(m3)"
		Me.總水足跡m3DataGridViewTextBoxColumn2.Name = "總水足跡m3DataGridViewTextBoxColumn2"
		'
		'電子元件製程BindingSource
		'
		Me.電子元件製程BindingSource.DataMember = "電子元件製程"
		Me.電子元件製程BindingSource.DataSource = Me.WFEDataSet10
		'
		'WFEDataSet10
		'
		Me.WFEDataSet10.DataSetName = "WFEDataSet10"
		Me.WFEDataSet10.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
		'
		'TextBox11
		'
		Me.TextBox11.Location = New System.Drawing.Point(50, 21)
		Me.TextBox11.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
		Me.TextBox11.Name = "TextBox11"
		Me.TextBox11.Size = New System.Drawing.Size(137, 23)
		Me.TextBox11.TabIndex = 4
		'
		'Label23
		'
		Me.Label23.AutoSize = True
		Me.Label23.Location = New System.Drawing.Point(8, 148)
		Me.Label23.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
		Me.Label23.Name = "Label23"
		Me.Label23.Size = New System.Drawing.Size(44, 16)
		Me.Label23.TabIndex = 3
		Me.Label23.Text = "水足跡"
		'
		'Label22
		'
		Me.Label22.AutoSize = True
		Me.Label22.Location = New System.Drawing.Point(8, 118)
		Me.Label22.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
		Me.Label22.Name = "Label22"
		Me.Label22.Size = New System.Drawing.Size(32, 16)
		Me.Label22.TabIndex = 2
		Me.Label22.Text = "體積"
		'
		'Label21
		'
		Me.Label21.AutoSize = True
		Me.Label21.Location = New System.Drawing.Point(8, 68)
		Me.Label21.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
		Me.Label21.Name = "Label21"
		Me.Label21.Size = New System.Drawing.Size(32, 16)
		Me.Label21.TabIndex = 1
		Me.Label21.Text = "製程"
		'
		'Label20
		'
		Me.Label20.AutoSize = True
		Me.Label20.Location = New System.Drawing.Point(8, 24)
		Me.Label20.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
		Me.Label20.Name = "Label20"
		Me.Label20.Size = New System.Drawing.Size(32, 16)
		Me.Label20.TabIndex = 0
		Me.Label20.Text = "元件"
		'
		'GroupBox9
		'
		Me.GroupBox9.Controls.Add(Me.Label19)
		Me.GroupBox9.Controls.Add(Me.Label18)
		Me.GroupBox9.Controls.Add(Me.Button15)
		Me.GroupBox9.Controls.Add(Me.Button14)
		Me.GroupBox9.Controls.Add(Me.Button13)
		Me.GroupBox9.Controls.Add(Me.TextBox10)
		Me.GroupBox9.Controls.Add(Me.TextBox9)
		Me.GroupBox9.Controls.Add(Me.DataGridView5)
		Me.GroupBox9.Controls.Add(Me.TextBox8)
		Me.GroupBox9.Controls.Add(Me.Label17)
		Me.GroupBox9.Controls.Add(Me.Label16)
		Me.GroupBox9.Controls.Add(Me.Label15)
		Me.GroupBox9.Controls.Add(Me.Label14)
		Me.GroupBox9.Location = New System.Drawing.Point(14, 2)
		Me.GroupBox9.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
		Me.GroupBox9.Name = "GroupBox9"
		Me.GroupBox9.Padding = New System.Windows.Forms.Padding(2, 2, 2, 2)
		Me.GroupBox9.Size = New System.Drawing.Size(250, 175)
		Me.GroupBox9.TabIndex = 0
		Me.GroupBox9.TabStop = False
		Me.GroupBox9.Text = "選擇電路板零件"
		'
		'Label19
		'
		Me.Label19.AutoSize = True
		Me.Label19.Location = New System.Drawing.Point(194, 145)
		Me.Label19.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
		Me.Label19.Name = "Label19"
		Me.Label19.Size = New System.Drawing.Size(35, 16)
		Me.Label19.TabIndex = 12
		Me.Label19.Text = "m^3"
		'
		'Label18
		'
		Me.Label18.AutoSize = True
		Me.Label18.Location = New System.Drawing.Point(190, 119)
		Me.Label18.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
		Me.Label18.Name = "Label18"
		Me.Label18.Size = New System.Drawing.Size(46, 16)
		Me.Label18.TabIndex = 11
		Me.Label18.Text = "mm^2"
		'
		'Button15
		'
		Me.Button15.Location = New System.Drawing.Point(194, 85)
		Me.Button15.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
		Me.Button15.Name = "Button15"
		Me.Button15.Size = New System.Drawing.Size(52, 27)
		Me.Button15.TabIndex = 10
		Me.Button15.Text = "重選"
		Me.Button15.UseVisualStyleBackColor = True
		'
		'Button14
		'
		Me.Button14.Location = New System.Drawing.Point(194, 51)
		Me.Button14.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
		Me.Button14.Name = "Button14"
		Me.Button14.Size = New System.Drawing.Size(52, 27)
		Me.Button14.TabIndex = 9
		Me.Button14.Text = "確定"
		Me.Button14.UseVisualStyleBackColor = True
		'
		'Button13
		'
		Me.Button13.Location = New System.Drawing.Point(194, 19)
		Me.Button13.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
		Me.Button13.Name = "Button13"
		Me.Button13.Size = New System.Drawing.Size(52, 27)
		Me.Button13.TabIndex = 8
		Me.Button13.Text = "獲取"
		Me.Button13.UseVisualStyleBackColor = True
		'
		'TextBox10
		'
		Me.TextBox10.Location = New System.Drawing.Point(50, 143)
		Me.TextBox10.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
		Me.TextBox10.Name = "TextBox10"
		Me.TextBox10.Size = New System.Drawing.Size(137, 23)
		Me.TextBox10.TabIndex = 7
		'
		'TextBox9
		'
		Me.TextBox9.Location = New System.Drawing.Point(50, 117)
		Me.TextBox9.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
		Me.TextBox9.Name = "TextBox9"
		Me.TextBox9.Size = New System.Drawing.Size(137, 23)
		Me.TextBox9.TabIndex = 6
		'
		'DataGridView5
		'
		Me.DataGridView5.AutoGenerateColumns = False
		Me.DataGridView5.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
		Me.DataGridView5.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.識別碼DataGridViewTextBoxColumn4, Me.製程DataGridViewTextBoxColumn, Me.藍水m3DataGridViewTextBoxColumn1, Me.綠水m3DataGridViewTextBoxColumn1, Me.灰水m3DataGridViewTextBoxColumn1, Me.總水足跡m3DataGridViewTextBoxColumn1})
		Me.DataGridView5.DataSource = Me.電路板製程BindingSource
		Me.DataGridView5.Location = New System.Drawing.Point(50, 46)
		Me.DataGridView5.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
		Me.DataGridView5.Name = "DataGridView5"
		Me.DataGridView5.RowTemplate.Height = 27
		Me.DataGridView5.Size = New System.Drawing.Size(136, 66)
		Me.DataGridView5.TabIndex = 5
		'
		'識別碼DataGridViewTextBoxColumn4
		'
		Me.識別碼DataGridViewTextBoxColumn4.DataPropertyName = "識別碼"
		Me.識別碼DataGridViewTextBoxColumn4.HeaderText = "識別碼"
		Me.識別碼DataGridViewTextBoxColumn4.Name = "識別碼DataGridViewTextBoxColumn4"
		'
		'製程DataGridViewTextBoxColumn
		'
		Me.製程DataGridViewTextBoxColumn.DataPropertyName = "製程"
		Me.製程DataGridViewTextBoxColumn.HeaderText = "製程"
		Me.製程DataGridViewTextBoxColumn.Name = "製程DataGridViewTextBoxColumn"
		'
		'藍水m3DataGridViewTextBoxColumn1
		'
		Me.藍水m3DataGridViewTextBoxColumn1.DataPropertyName = "藍水(m3)"
		Me.藍水m3DataGridViewTextBoxColumn1.HeaderText = "藍水(m3)"
		Me.藍水m3DataGridViewTextBoxColumn1.Name = "藍水m3DataGridViewTextBoxColumn1"
		'
		'綠水m3DataGridViewTextBoxColumn1
		'
		Me.綠水m3DataGridViewTextBoxColumn1.DataPropertyName = "綠水(m3)"
		Me.綠水m3DataGridViewTextBoxColumn1.HeaderText = "綠水(m3)"
		Me.綠水m3DataGridViewTextBoxColumn1.Name = "綠水m3DataGridViewTextBoxColumn1"
		'
		'灰水m3DataGridViewTextBoxColumn1
		'
		Me.灰水m3DataGridViewTextBoxColumn1.DataPropertyName = "灰水(m3)"
		Me.灰水m3DataGridViewTextBoxColumn1.HeaderText = "灰水(m3)"
		Me.灰水m3DataGridViewTextBoxColumn1.Name = "灰水m3DataGridViewTextBoxColumn1"
		'
		'總水足跡m3DataGridViewTextBoxColumn1
		'
		Me.總水足跡m3DataGridViewTextBoxColumn1.DataPropertyName = "總水足跡(m3)"
		Me.總水足跡m3DataGridViewTextBoxColumn1.HeaderText = "總水足跡(m3)"
		Me.總水足跡m3DataGridViewTextBoxColumn1.Name = "總水足跡m3DataGridViewTextBoxColumn1"
		'
		'電路板製程BindingSource
		'
		Me.電路板製程BindingSource.DataMember = "電路板製程"
		Me.電路板製程BindingSource.DataSource = Me.WFEDataSet9
		'
		'WFEDataSet9
		'
		Me.WFEDataSet9.DataSetName = "WFEDataSet9"
		Me.WFEDataSet9.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
		'
		'TextBox8
		'
		Me.TextBox8.Location = New System.Drawing.Point(50, 20)
		Me.TextBox8.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
		Me.TextBox8.Name = "TextBox8"
		Me.TextBox8.Size = New System.Drawing.Size(137, 23)
		Me.TextBox8.TabIndex = 4
		'
		'Label17
		'
		Me.Label17.AutoSize = True
		Me.Label17.Location = New System.Drawing.Point(4, 142)
		Me.Label17.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
		Me.Label17.Name = "Label17"
		Me.Label17.Size = New System.Drawing.Size(44, 16)
		Me.Label17.TabIndex = 3
		Me.Label17.Text = "水足跡"
		'
		'Label16
		'
		Me.Label16.AutoSize = True
		Me.Label16.Location = New System.Drawing.Point(4, 118)
		Me.Label16.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
		Me.Label16.Name = "Label16"
		Me.Label16.Size = New System.Drawing.Size(32, 16)
		Me.Label16.TabIndex = 2
		Me.Label16.Text = "面積"
		'
		'Label15
		'
		Me.Label15.AutoSize = True
		Me.Label15.Location = New System.Drawing.Point(4, 63)
		Me.Label15.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
		Me.Label15.Name = "Label15"
		Me.Label15.Size = New System.Drawing.Size(32, 16)
		Me.Label15.TabIndex = 1
		Me.Label15.Text = "製程"
		'
		'Label14
		'
		Me.Label14.AutoSize = True
		Me.Label14.Location = New System.Drawing.Point(4, 22)
		Me.Label14.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
		Me.Label14.Name = "Label14"
		Me.Label14.Size = New System.Drawing.Size(44, 16)
		Me.Label14.TabIndex = 0
		Me.Label14.Text = "電路板"
		'
		'TabPage2
		'
		Me.TabPage2.Controls.Add(Me.Button12)
		Me.TabPage2.Controls.Add(Me.Label13)
		Me.TabPage2.Controls.Add(Me.TextBox7)
		Me.TabPage2.Controls.Add(Me.Label12)
		Me.TabPage2.Controls.Add(Me.GroupBox8)
		Me.TabPage2.Controls.Add(Me.GroupBox7)
		Me.TabPage2.Controls.Add(Me.GroupBox6)
		Me.TabPage2.Controls.Add(Me.GroupBox5)
		Me.TabPage2.Location = New System.Drawing.Point(4, 25)
		Me.TabPage2.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
		Me.TabPage2.Name = "TabPage2"
		Me.TabPage2.Padding = New System.Windows.Forms.Padding(2, 2, 2, 2)
		Me.TabPage2.Size = New System.Drawing.Size(328, 581)
		Me.TabPage2.TabIndex = 1
		Me.TabPage2.Text = "B.後製加工"
		Me.TabPage2.UseVisualStyleBackColor = True
		'
		'Button12
		'
		Me.Button12.Location = New System.Drawing.Point(76, 555)
		Me.Button12.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
		Me.Button12.Name = "Button12"
		Me.Button12.Size = New System.Drawing.Size(118, 24)
		Me.Button12.TabIndex = 7
		Me.Button12.Text = "完成此階段評估"
		Me.Button12.UseVisualStyleBackColor = True
		'
		'Label13
		'
		Me.Label13.AutoSize = True
		Me.Label13.Location = New System.Drawing.Point(209, 529)
		Me.Label13.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
		Me.Label13.Name = "Label13"
		Me.Label13.Size = New System.Drawing.Size(35, 16)
		Me.Label13.TabIndex = 6
		Me.Label13.Text = "m^3"
		'
		'TextBox7
		'
		Me.TextBox7.Location = New System.Drawing.Point(62, 529)
		Me.TextBox7.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
		Me.TextBox7.Name = "TextBox7"
		Me.TextBox7.Size = New System.Drawing.Size(143, 23)
		Me.TextBox7.TabIndex = 5
		'
		'Label12
		'
		Me.Label12.AutoSize = True
		Me.Label12.Location = New System.Drawing.Point(96, 511)
		Me.Label12.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
		Me.Label12.Name = "Label12"
		Me.Label12.Size = New System.Drawing.Size(92, 16)
		Me.Label12.TabIndex = 4
		Me.Label12.Text = "後製加工水足跡"
		'
		'GroupBox8
		'
		Me.GroupBox8.Controls.Add(Me.Button11)
		Me.GroupBox8.Controls.Add(Me.DataGridView4)
		Me.GroupBox8.Location = New System.Drawing.Point(14, 355)
		Me.GroupBox8.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
		Me.GroupBox8.Name = "GroupBox8"
		Me.GroupBox8.Padding = New System.Windows.Forms.Padding(2, 2, 2, 2)
		Me.GroupBox8.Size = New System.Drawing.Size(250, 151)
		Me.GroupBox8.TabIndex = 3
		Me.GroupBox8.TabStop = False
		Me.GroupBox8.Text = "後製加工紀錄"
		'
		'Button11
		'
		Me.Button11.Location = New System.Drawing.Point(62, 120)
		Me.Button11.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
		Me.Button11.Name = "Button11"
		Me.Button11.Size = New System.Drawing.Size(118, 24)
		Me.Button11.TabIndex = 1
		Me.Button11.Text = "刪除"
		Me.Button11.UseVisualStyleBackColor = True
		'
		'DataGridView4
		'
		Me.DataGridView4.AutoGenerateColumns = False
		Me.DataGridView4.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
		Me.DataGridView4.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.識別碼DataGridViewTextBoxColumn3, Me.方式DataGridViewTextBoxColumn, Me.耗電時DataGridViewTextBoxColumn, Me.數量DataGridViewTextBoxColumn, Me.計算單位DataGridViewTextBoxColumn, Me.係數DataGridViewTextBoxColumn, Me.水足跡DataGridViewTextBoxColumn2})
		Me.DataGridView4.DataSource = Me.已選加工資訊BindingSource
		Me.DataGridView4.Location = New System.Drawing.Point(14, 21)
		Me.DataGridView4.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
		Me.DataGridView4.Name = "DataGridView4"
		Me.DataGridView4.RowTemplate.Height = 27
		Me.DataGridView4.Size = New System.Drawing.Size(219, 90)
		Me.DataGridView4.TabIndex = 0
		'
		'識別碼DataGridViewTextBoxColumn3
		'
		Me.識別碼DataGridViewTextBoxColumn3.DataPropertyName = "識別碼"
		Me.識別碼DataGridViewTextBoxColumn3.HeaderText = "識別碼"
		Me.識別碼DataGridViewTextBoxColumn3.Name = "識別碼DataGridViewTextBoxColumn3"
		'
		'方式DataGridViewTextBoxColumn
		'
		Me.方式DataGridViewTextBoxColumn.DataPropertyName = "方式"
		Me.方式DataGridViewTextBoxColumn.HeaderText = "方式"
		Me.方式DataGridViewTextBoxColumn.Name = "方式DataGridViewTextBoxColumn"
		'
		'耗電時DataGridViewTextBoxColumn
		'
		Me.耗電時DataGridViewTextBoxColumn.DataPropertyName = "耗電/時"
		Me.耗電時DataGridViewTextBoxColumn.HeaderText = "耗電/時"
		Me.耗電時DataGridViewTextBoxColumn.Name = "耗電時DataGridViewTextBoxColumn"
		'
		'數量DataGridViewTextBoxColumn
		'
		Me.數量DataGridViewTextBoxColumn.DataPropertyName = "數量"
		Me.數量DataGridViewTextBoxColumn.HeaderText = "數量"
		Me.數量DataGridViewTextBoxColumn.Name = "數量DataGridViewTextBoxColumn"
		'
		'計算單位DataGridViewTextBoxColumn
		'
		Me.計算單位DataGridViewTextBoxColumn.DataPropertyName = "計算單位"
		Me.計算單位DataGridViewTextBoxColumn.HeaderText = "計算單位"
		Me.計算單位DataGridViewTextBoxColumn.Name = "計算單位DataGridViewTextBoxColumn"
		'
		'係數DataGridViewTextBoxColumn
		'
		Me.係數DataGridViewTextBoxColumn.DataPropertyName = "係數"
		Me.係數DataGridViewTextBoxColumn.HeaderText = "係數"
		Me.係數DataGridViewTextBoxColumn.Name = "係數DataGridViewTextBoxColumn"
		'
		'水足跡DataGridViewTextBoxColumn2
		'
		Me.水足跡DataGridViewTextBoxColumn2.DataPropertyName = "水足跡"
		Me.水足跡DataGridViewTextBoxColumn2.HeaderText = "水足跡"
		Me.水足跡DataGridViewTextBoxColumn2.Name = "水足跡DataGridViewTextBoxColumn2"
		'
		'已選加工資訊BindingSource
		'
		Me.已選加工資訊BindingSource.DataMember = "已選加工資訊"
		Me.已選加工資訊BindingSource.DataSource = Me.WFEDataSet8
		'
		'WFEDataSet8
		'
		Me.WFEDataSet8.DataSetName = "WFEDataSet8"
		Me.WFEDataSet8.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
		'
		'GroupBox7
		'
		Me.GroupBox7.Controls.Add(Me.Button10)
		Me.GroupBox7.Location = New System.Drawing.Point(14, 285)
		Me.GroupBox7.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
		Me.GroupBox7.Name = "GroupBox7"
		Me.GroupBox7.Padding = New System.Windows.Forms.Padding(2, 2, 2, 2)
		Me.GroupBox7.Size = New System.Drawing.Size(250, 65)
		Me.GroupBox7.TabIndex = 2
		Me.GroupBox7.TabStop = False
		Me.GroupBox7.Text = "3.進行後製加工"
		'
		'Button10
		'
		Me.Button10.Location = New System.Drawing.Point(62, 24)
		Me.Button10.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
		Me.Button10.Name = "Button10"
		Me.Button10.Size = New System.Drawing.Size(118, 24)
		Me.Button10.TabIndex = 0
		Me.Button10.Text = "選擇加工方式"
		Me.Button10.UseVisualStyleBackColor = True
		'
		'GroupBox6
		'
		Me.GroupBox6.Controls.Add(Me.Button9)
		Me.GroupBox6.Location = New System.Drawing.Point(14, 215)
		Me.GroupBox6.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
		Me.GroupBox6.Name = "GroupBox6"
		Me.GroupBox6.Padding = New System.Windows.Forms.Padding(2, 2, 2, 2)
		Me.GroupBox6.Size = New System.Drawing.Size(250, 65)
		Me.GroupBox6.TabIndex = 1
		Me.GroupBox6.TabStop = False
		Me.GroupBox6.Text = "2.運輸至加工廠"
		'
		'Button9
		'
		Me.Button9.Location = New System.Drawing.Point(62, 25)
		Me.Button9.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
		Me.Button9.Name = "Button9"
		Me.Button9.Size = New System.Drawing.Size(118, 24)
		Me.Button9.TabIndex = 1
		Me.Button9.Text = "選擇運輸工具"
		Me.Button9.UseVisualStyleBackColor = True
		'
		'GroupBox5
		'
		Me.GroupBox5.Controls.Add(Me.Button8)
		Me.GroupBox5.Controls.Add(Me.Label11)
		Me.GroupBox5.Controls.Add(Me.TextBox6)
		Me.GroupBox5.Controls.Add(Me.DataGridView3)
		Me.GroupBox5.Location = New System.Drawing.Point(14, 5)
		Me.GroupBox5.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
		Me.GroupBox5.Name = "GroupBox5"
		Me.GroupBox5.Padding = New System.Windows.Forms.Padding(2, 2, 2, 2)
		Me.GroupBox5.Size = New System.Drawing.Size(250, 205)
		Me.GroupBox5.TabIndex = 0
		Me.GroupBox5.TabStop = False
		Me.GroupBox5.Text = "1.選擇零件"
		'
		'Button8
		'
		Me.Button8.Location = New System.Drawing.Point(62, 166)
		Me.Button8.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
		Me.Button8.Name = "Button8"
		Me.Button8.Size = New System.Drawing.Size(118, 24)
		Me.Button8.TabIndex = 3
		Me.Button8.Text = "確定"
		Me.Button8.UseVisualStyleBackColor = True
		'
		'Label11
		'
		Me.Label11.AutoSize = True
		Me.Label11.Location = New System.Drawing.Point(14, 137)
		Me.Label11.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
		Me.Label11.Name = "Label11"
		Me.Label11.Size = New System.Drawing.Size(32, 16)
		Me.Label11.TabIndex = 2
		Me.Label11.Text = "零件"
		'
		'TextBox6
		'
		Me.TextBox6.Location = New System.Drawing.Point(48, 134)
		Me.TextBox6.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
		Me.TextBox6.Name = "TextBox6"
		Me.TextBox6.Size = New System.Drawing.Size(166, 23)
		Me.TextBox6.TabIndex = 1
		'
		'DataGridView3
		'
		Me.DataGridView3.AutoGenerateColumns = False
		Me.DataGridView3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
		Me.DataGridView3.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.識別碼DataGridViewTextBoxColumn2, Me.零件名稱DataGridViewTextBoxColumn1, Me.材料DataGridViewTextBoxColumn2, Me.質量DataGridViewTextBoxColumn1, Me.水足跡DataGridViewTextBoxColumn1, Me.加工方式DataGridViewTextBoxColumn1})
		Me.DataGridView3.DataSource = Me.已選一般零件BindingSource
		Me.DataGridView3.Location = New System.Drawing.Point(14, 21)
		Me.DataGridView3.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
		Me.DataGridView3.Name = "DataGridView3"
		Me.DataGridView3.RowTemplate.Height = 27
		Me.DataGridView3.Size = New System.Drawing.Size(219, 103)
		Me.DataGridView3.TabIndex = 0
		'
		'識別碼DataGridViewTextBoxColumn2
		'
		Me.識別碼DataGridViewTextBoxColumn2.DataPropertyName = "識別碼"
		Me.識別碼DataGridViewTextBoxColumn2.HeaderText = "識別碼"
		Me.識別碼DataGridViewTextBoxColumn2.Name = "識別碼DataGridViewTextBoxColumn2"
		'
		'零件名稱DataGridViewTextBoxColumn1
		'
		Me.零件名稱DataGridViewTextBoxColumn1.DataPropertyName = "零件名稱"
		Me.零件名稱DataGridViewTextBoxColumn1.HeaderText = "零件名稱"
		Me.零件名稱DataGridViewTextBoxColumn1.Name = "零件名稱DataGridViewTextBoxColumn1"
		'
		'材料DataGridViewTextBoxColumn2
		'
		Me.材料DataGridViewTextBoxColumn2.DataPropertyName = "材料"
		Me.材料DataGridViewTextBoxColumn2.HeaderText = "材料"
		Me.材料DataGridViewTextBoxColumn2.Name = "材料DataGridViewTextBoxColumn2"
		'
		'質量DataGridViewTextBoxColumn1
		'
		Me.質量DataGridViewTextBoxColumn1.DataPropertyName = "質量"
		Me.質量DataGridViewTextBoxColumn1.HeaderText = "質量"
		Me.質量DataGridViewTextBoxColumn1.Name = "質量DataGridViewTextBoxColumn1"
		'
		'水足跡DataGridViewTextBoxColumn1
		'
		Me.水足跡DataGridViewTextBoxColumn1.DataPropertyName = "水足跡"
		Me.水足跡DataGridViewTextBoxColumn1.HeaderText = "水足跡"
		Me.水足跡DataGridViewTextBoxColumn1.Name = "水足跡DataGridViewTextBoxColumn1"
		'
		'加工方式DataGridViewTextBoxColumn1
		'
		Me.加工方式DataGridViewTextBoxColumn1.DataPropertyName = "加工方式"
		Me.加工方式DataGridViewTextBoxColumn1.HeaderText = "加工方式"
		Me.加工方式DataGridViewTextBoxColumn1.Name = "加工方式DataGridViewTextBoxColumn1"
		'
		'已選一般零件BindingSource
		'
		Me.已選一般零件BindingSource.DataMember = "已選一般零件"
		Me.已選一般零件BindingSource.DataSource = Me.WFEDataSet1
		'
		'WFEDataSet1
		'
		Me.WFEDataSet1.DataSetName = "WFEDataSet1"
		Me.WFEDataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
		'
		'TabPage1
		'
		Me.TabPage1.Controls.Add(Me.Button7)
		Me.TabPage1.Controls.Add(Me.Label10)
		Me.TabPage1.Controls.Add(Me.TextBox5)
		Me.TabPage1.Controls.Add(Me.Label9)
		Me.TabPage1.Controls.Add(Me.GroupBox4)
		Me.TabPage1.Controls.Add(Me.GroupBox3)
		Me.TabPage1.Controls.Add(Me.GroupBox2)
		Me.TabPage1.Controls.Add(Me.GroupBox1)
		Me.TabPage1.Location = New System.Drawing.Point(4, 25)
		Me.TabPage1.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
		Me.TabPage1.Name = "TabPage1"
		Me.TabPage1.Padding = New System.Windows.Forms.Padding(2, 2, 2, 2)
		Me.TabPage1.Size = New System.Drawing.Size(328, 581)
		Me.TabPage1.TabIndex = 0
		Me.TabPage1.Text = "A.一般零件"
		Me.TabPage1.UseVisualStyleBackColor = True
		'
		'Button7
		'
		Me.Button7.Location = New System.Drawing.Point(79, 553)
		Me.Button7.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
		Me.Button7.Name = "Button7"
		Me.Button7.Size = New System.Drawing.Size(118, 24)
		Me.Button7.TabIndex = 7
		Me.Button7.Text = "完成此階段評估"
		Me.Button7.UseVisualStyleBackColor = True
		'
		'Label10
		'
		Me.Label10.AutoSize = True
		Me.Label10.Location = New System.Drawing.Point(217, 529)
		Me.Label10.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
		Me.Label10.Name = "Label10"
		Me.Label10.Size = New System.Drawing.Size(35, 16)
		Me.Label10.TabIndex = 6
		Me.Label10.Text = "m^3"
		'
		'TextBox5
		'
		Me.TextBox5.Location = New System.Drawing.Point(69, 526)
		Me.TextBox5.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
		Me.TextBox5.Name = "TextBox5"
		Me.TextBox5.Size = New System.Drawing.Size(143, 23)
		Me.TextBox5.TabIndex = 5
		'
		'Label9
		'
		Me.Label9.AutoSize = True
		Me.Label9.Location = New System.Drawing.Point(78, 509)
		Me.Label9.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
		Me.Label9.Name = "Label9"
		Me.Label9.Size = New System.Drawing.Size(128, 16)
		Me.Label9.TabIndex = 4
		Me.Label9.Text = "一般零件製程總水足跡"
		'
		'GroupBox4
		'
		Me.GroupBox4.Controls.Add(Me.Button6)
		Me.GroupBox4.Controls.Add(Me.DataGridView2)
		Me.GroupBox4.Location = New System.Drawing.Point(14, 354)
		Me.GroupBox4.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
		Me.GroupBox4.Name = "GroupBox4"
		Me.GroupBox4.Padding = New System.Windows.Forms.Padding(2, 2, 2, 2)
		Me.GroupBox4.Size = New System.Drawing.Size(250, 152)
		Me.GroupBox4.TabIndex = 3
		Me.GroupBox4.TabStop = False
		Me.GroupBox4.Text = "已選零件"
		'
		'Button6
		'
		Me.Button6.Location = New System.Drawing.Point(64, 118)
		Me.Button6.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
		Me.Button6.Name = "Button6"
		Me.Button6.Size = New System.Drawing.Size(118, 24)
		Me.Button6.TabIndex = 1
		Me.Button6.Text = "刪除資料"
		Me.Button6.UseVisualStyleBackColor = True
		'
		'DataGridView2
		'
		Me.DataGridView2.AutoGenerateColumns = False
		Me.DataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
		Me.DataGridView2.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.識別碼DataGridViewTextBoxColumn1, Me.零件名稱DataGridViewTextBoxColumn, Me.材料DataGridViewTextBoxColumn1, Me.質量DataGridViewTextBoxColumn, Me.水足跡DataGridViewTextBoxColumn, Me.加工方式DataGridViewTextBoxColumn})
		Me.DataGridView2.DataSource = Me.已選一般零件BindingSource
		Me.DataGridView2.Location = New System.Drawing.Point(16, 21)
		Me.DataGridView2.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
		Me.DataGridView2.Name = "DataGridView2"
		Me.DataGridView2.RowTemplate.Height = 27
		Me.DataGridView2.Size = New System.Drawing.Size(218, 92)
		Me.DataGridView2.TabIndex = 0
		'
		'識別碼DataGridViewTextBoxColumn1
		'
		Me.識別碼DataGridViewTextBoxColumn1.DataPropertyName = "識別碼"
		Me.識別碼DataGridViewTextBoxColumn1.HeaderText = "識別碼"
		Me.識別碼DataGridViewTextBoxColumn1.Name = "識別碼DataGridViewTextBoxColumn1"
		'
		'零件名稱DataGridViewTextBoxColumn
		'
		Me.零件名稱DataGridViewTextBoxColumn.DataPropertyName = "零件名稱"
		Me.零件名稱DataGridViewTextBoxColumn.HeaderText = "零件名稱"
		Me.零件名稱DataGridViewTextBoxColumn.Name = "零件名稱DataGridViewTextBoxColumn"
		'
		'材料DataGridViewTextBoxColumn1
		'
		Me.材料DataGridViewTextBoxColumn1.DataPropertyName = "材料"
		Me.材料DataGridViewTextBoxColumn1.HeaderText = "材料"
		Me.材料DataGridViewTextBoxColumn1.Name = "材料DataGridViewTextBoxColumn1"
		'
		'質量DataGridViewTextBoxColumn
		'
		Me.質量DataGridViewTextBoxColumn.DataPropertyName = "質量"
		Me.質量DataGridViewTextBoxColumn.HeaderText = "質量"
		Me.質量DataGridViewTextBoxColumn.Name = "質量DataGridViewTextBoxColumn"
		'
		'水足跡DataGridViewTextBoxColumn
		'
		Me.水足跡DataGridViewTextBoxColumn.DataPropertyName = "水足跡"
		Me.水足跡DataGridViewTextBoxColumn.HeaderText = "水足跡"
		Me.水足跡DataGridViewTextBoxColumn.Name = "水足跡DataGridViewTextBoxColumn"
		'
		'加工方式DataGridViewTextBoxColumn
		'
		Me.加工方式DataGridViewTextBoxColumn.DataPropertyName = "加工方式"
		Me.加工方式DataGridViewTextBoxColumn.HeaderText = "加工方式"
		Me.加工方式DataGridViewTextBoxColumn.Name = "加工方式DataGridViewTextBoxColumn"
		'
		'GroupBox3
		'
		Me.GroupBox3.Controls.Add(Me.Button5)
		Me.GroupBox3.Location = New System.Drawing.Point(14, 284)
		Me.GroupBox3.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
		Me.GroupBox3.Name = "GroupBox3"
		Me.GroupBox3.Padding = New System.Windows.Forms.Padding(2, 2, 2, 2)
		Me.GroupBox3.Size = New System.Drawing.Size(250, 65)
		Me.GroupBox3.TabIndex = 2
		Me.GroupBox3.TabStop = False
		Me.GroupBox3.Text = "3.製程水足跡"
		'
		'Button5
		'
		Me.Button5.Location = New System.Drawing.Point(66, 24)
		Me.Button5.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
		Me.Button5.Name = "Button5"
		Me.Button5.Size = New System.Drawing.Size(118, 24)
		Me.Button5.TabIndex = 0
		Me.Button5.Text = "選擇加工方式"
		Me.Button5.UseVisualStyleBackColor = True
		'
		'GroupBox2
		'
		Me.GroupBox2.Controls.Add(Me.Button4)
		Me.GroupBox2.Location = New System.Drawing.Point(14, 214)
		Me.GroupBox2.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
		Me.GroupBox2.Name = "GroupBox2"
		Me.GroupBox2.Padding = New System.Windows.Forms.Padding(2, 2, 2, 2)
		Me.GroupBox2.Size = New System.Drawing.Size(250, 65)
		Me.GroupBox2.TabIndex = 1
		Me.GroupBox2.TabStop = False
		Me.GroupBox2.Text = "2.運輸至製造廠"
		'
		'Button4
		'
		Me.Button4.Location = New System.Drawing.Point(66, 24)
		Me.Button4.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
		Me.Button4.Name = "Button4"
		Me.Button4.Size = New System.Drawing.Size(118, 24)
		Me.Button4.TabIndex = 0
		Me.Button4.Text = "選擇運輸工具"
		Me.Button4.UseVisualStyleBackColor = True
		'
		'GroupBox1
		'
		Me.GroupBox1.Controls.Add(Me.Label8)
		Me.GroupBox1.Controls.Add(Me.Label7)
		Me.GroupBox1.Controls.Add(Me.Label6)
		Me.GroupBox1.Controls.Add(Me.Button3)
		Me.GroupBox1.Controls.Add(Me.Button2)
		Me.GroupBox1.Controls.Add(Me.Button1)
		Me.GroupBox1.Controls.Add(Me.TextBox4)
		Me.GroupBox1.Controls.Add(Me.TextBox3)
		Me.GroupBox1.Controls.Add(Me.TextBox2)
		Me.GroupBox1.Controls.Add(Me.Label5)
		Me.GroupBox1.Controls.Add(Me.TextBox1)
		Me.GroupBox1.Controls.Add(Me.DataGridView1)
		Me.GroupBox1.Controls.Add(Me.Label4)
		Me.GroupBox1.Controls.Add(Me.Label3)
		Me.GroupBox1.Controls.Add(Me.Label2)
		Me.GroupBox1.Controls.Add(Me.Label1)
		Me.GroupBox1.Location = New System.Drawing.Point(14, 5)
		Me.GroupBox1.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
		Me.GroupBox1.Name = "GroupBox1"
		Me.GroupBox1.Padding = New System.Windows.Forms.Padding(2, 2, 2, 2)
		Me.GroupBox1.Size = New System.Drawing.Size(250, 205)
		Me.GroupBox1.TabIndex = 0
		Me.GroupBox1.TabStop = False
		Me.GroupBox1.Text = "1.選擇零件材料"
		'
		'Label8
		'
		Me.Label8.AutoSize = True
		Me.Label8.Location = New System.Drawing.Point(196, 175)
		Me.Label8.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
		Me.Label8.Name = "Label8"
		Me.Label8.Size = New System.Drawing.Size(23, 16)
		Me.Label8.TabIndex = 14
		Me.Label8.Text = "Kg"
		'
		'Label7
		'
		Me.Label7.AutoSize = True
		Me.Label7.Location = New System.Drawing.Point(195, 150)
		Me.Label7.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
		Me.Label7.Name = "Label7"
		Me.Label7.Size = New System.Drawing.Size(55, 16)
		Me.Label7.TabIndex = 13
		Me.Label7.Text = "Kg/m^3"
		'
		'Label6
		'
		Me.Label6.AutoSize = True
		Me.Label6.Location = New System.Drawing.Point(195, 122)
		Me.Label6.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
		Me.Label6.Name = "Label6"
		Me.Label6.Size = New System.Drawing.Size(46, 16)
		Me.Label6.TabIndex = 12
		Me.Label6.Text = "mm^3"
		'
		'Button3
		'
		Me.Button3.Location = New System.Drawing.Point(194, 90)
		Me.Button3.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
		Me.Button3.Name = "Button3"
		Me.Button3.Size = New System.Drawing.Size(52, 27)
		Me.Button3.TabIndex = 11
		Me.Button3.Text = "重選"
		Me.Button3.UseVisualStyleBackColor = True
		'
		'Button2
		'
		Me.Button2.Location = New System.Drawing.Point(194, 58)
		Me.Button2.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
		Me.Button2.Name = "Button2"
		Me.Button2.Size = New System.Drawing.Size(52, 27)
		Me.Button2.TabIndex = 10
		Me.Button2.Text = "確定"
		Me.Button2.UseVisualStyleBackColor = True
		'
		'Button1
		'
		Me.Button1.Location = New System.Drawing.Point(194, 25)
		Me.Button1.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
		Me.Button1.Name = "Button1"
		Me.Button1.Size = New System.Drawing.Size(52, 27)
		Me.Button1.TabIndex = 9
		Me.Button1.Text = "獲取"
		Me.Button1.UseVisualStyleBackColor = True
		'
		'TextBox4
		'
		Me.TextBox4.Location = New System.Drawing.Point(50, 174)
		Me.TextBox4.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
		Me.TextBox4.Name = "TextBox4"
		Me.TextBox4.Size = New System.Drawing.Size(137, 23)
		Me.TextBox4.TabIndex = 1
		'
		'TextBox3
		'
		Me.TextBox3.Location = New System.Drawing.Point(50, 148)
		Me.TextBox3.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
		Me.TextBox3.Name = "TextBox3"
		Me.TextBox3.Size = New System.Drawing.Size(137, 23)
		Me.TextBox3.TabIndex = 8
		'
		'TextBox2
		'
		Me.TextBox2.Location = New System.Drawing.Point(50, 121)
		Me.TextBox2.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
		Me.TextBox2.Name = "TextBox2"
		Me.TextBox2.Size = New System.Drawing.Size(137, 23)
		Me.TextBox2.TabIndex = 7
		'
		'Label5
		'
		Me.Label5.AutoSize = True
		Me.Label5.Location = New System.Drawing.Point(14, 174)
		Me.Label5.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
		Me.Label5.Name = "Label5"
		Me.Label5.Size = New System.Drawing.Size(32, 16)
		Me.Label5.TabIndex = 6
		Me.Label5.Text = "質量"
		'
		'TextBox1
		'
		Me.TextBox1.Location = New System.Drawing.Point(50, 25)
		Me.TextBox1.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
		Me.TextBox1.Name = "TextBox1"
		Me.TextBox1.Size = New System.Drawing.Size(135, 23)
		Me.TextBox1.TabIndex = 5
		'
		'DataGridView1
		'
		Me.DataGridView1.AutoGenerateColumns = False
		Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
		Me.DataGridView1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.識別碼DataGridViewTextBoxColumn, Me.材料DataGridViewTextBoxColumn, Me.密度kgm3DataGridViewTextBoxColumn, Me.藍水m3DataGridViewTextBoxColumn, Me.綠水m3DataGridViewTextBoxColumn, Me.灰水m3DataGridViewTextBoxColumn, Me.總水足跡m3DataGridViewTextBoxColumn})
		Me.DataGridView1.DataSource = Me.材料BindingSource
		Me.DataGridView1.Location = New System.Drawing.Point(50, 51)
		Me.DataGridView1.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
		Me.DataGridView1.Name = "DataGridView1"
		Me.DataGridView1.RowTemplate.Height = 27
		Me.DataGridView1.Size = New System.Drawing.Size(136, 66)
		Me.DataGridView1.TabIndex = 4
		'
		'識別碼DataGridViewTextBoxColumn
		'
		Me.識別碼DataGridViewTextBoxColumn.DataPropertyName = "識別碼"
		Me.識別碼DataGridViewTextBoxColumn.HeaderText = "識別碼"
		Me.識別碼DataGridViewTextBoxColumn.Name = "識別碼DataGridViewTextBoxColumn"
		'
		'材料DataGridViewTextBoxColumn
		'
		Me.材料DataGridViewTextBoxColumn.DataPropertyName = "材料"
		Me.材料DataGridViewTextBoxColumn.HeaderText = "材料"
		Me.材料DataGridViewTextBoxColumn.Name = "材料DataGridViewTextBoxColumn"
		'
		'密度kgm3DataGridViewTextBoxColumn
		'
		Me.密度kgm3DataGridViewTextBoxColumn.DataPropertyName = "密度kg/m3"
		Me.密度kgm3DataGridViewTextBoxColumn.HeaderText = "密度kg/m3"
		Me.密度kgm3DataGridViewTextBoxColumn.Name = "密度kgm3DataGridViewTextBoxColumn"
		'
		'藍水m3DataGridViewTextBoxColumn
		'
		Me.藍水m3DataGridViewTextBoxColumn.DataPropertyName = "藍水(m3)"
		Me.藍水m3DataGridViewTextBoxColumn.HeaderText = "藍水(m3)"
		Me.藍水m3DataGridViewTextBoxColumn.Name = "藍水m3DataGridViewTextBoxColumn"
		'
		'綠水m3DataGridViewTextBoxColumn
		'
		Me.綠水m3DataGridViewTextBoxColumn.DataPropertyName = "綠水(m3)"
		Me.綠水m3DataGridViewTextBoxColumn.HeaderText = "綠水(m3)"
		Me.綠水m3DataGridViewTextBoxColumn.Name = "綠水m3DataGridViewTextBoxColumn"
		'
		'灰水m3DataGridViewTextBoxColumn
		'
		Me.灰水m3DataGridViewTextBoxColumn.DataPropertyName = "灰水(m3)"
		Me.灰水m3DataGridViewTextBoxColumn.HeaderText = "灰水(m3)"
		Me.灰水m3DataGridViewTextBoxColumn.Name = "灰水m3DataGridViewTextBoxColumn"
		'
		'總水足跡m3DataGridViewTextBoxColumn
		'
		Me.總水足跡m3DataGridViewTextBoxColumn.DataPropertyName = "總水足跡 (m3)"
		Me.總水足跡m3DataGridViewTextBoxColumn.HeaderText = "總水足跡 (m3)"
		Me.總水足跡m3DataGridViewTextBoxColumn.Name = "總水足跡m3DataGridViewTextBoxColumn"
		'
		'Label4
		'
		Me.Label4.AutoSize = True
		Me.Label4.Location = New System.Drawing.Point(14, 150)
		Me.Label4.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
		Me.Label4.Name = "Label4"
		Me.Label4.Size = New System.Drawing.Size(32, 16)
		Me.Label4.TabIndex = 3
		Me.Label4.Text = "密度"
		'
		'Label3
		'
		Me.Label3.AutoSize = True
		Me.Label3.Location = New System.Drawing.Point(14, 127)
		Me.Label3.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
		Me.Label3.Name = "Label3"
		Me.Label3.Size = New System.Drawing.Size(32, 16)
		Me.Label3.TabIndex = 2
		Me.Label3.Text = "體積"
		'
		'Label2
		'
		Me.Label2.AutoSize = True
		Me.Label2.Location = New System.Drawing.Point(14, 68)
		Me.Label2.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
		Me.Label2.Name = "Label2"
		Me.Label2.Size = New System.Drawing.Size(32, 16)
		Me.Label2.TabIndex = 1
		Me.Label2.Text = "材料"
		'
		'Label1
		'
		Me.Label1.AutoSize = True
		Me.Label1.Location = New System.Drawing.Point(14, 26)
		Me.Label1.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
		Me.Label1.Name = "Label1"
		Me.Label1.Size = New System.Drawing.Size(32, 16)
		Me.Label1.TabIndex = 0
		Me.Label1.Text = "零件"
		'
		'TabControl1
		'
		Me.TabControl1.Controls.Add(Me.TabPage1)
		Me.TabControl1.Controls.Add(Me.TabPage2)
		Me.TabControl1.Controls.Add(Me.TabPage3)
		Me.TabControl1.Controls.Add(Me.TabPage5)
		Me.TabControl1.Controls.Add(Me.TabPage6)
		Me.TabControl1.Font = New System.Drawing.Font("Microsoft JhengHei", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
		Me.TabControl1.Location = New System.Drawing.Point(9, 10)
		Me.TabControl1.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
		Me.TabControl1.Name = "TabControl1"
		Me.TabControl1.SelectedIndex = 0
		Me.TabControl1.Size = New System.Drawing.Size(336, 610)
		Me.TabControl1.TabIndex = 0
		'
		'材料TableAdapter
		'
		Me.材料TableAdapter.ClearBeforeFill = True
		'
		'已選一般零件TableAdapter
		'
		Me.已選一般零件TableAdapter.ClearBeforeFill = True
		'
		'已選加工資訊TableAdapter
		'
		Me.已選加工資訊TableAdapter.ClearBeforeFill = True
		'
		'電路板製程TableAdapter
		'
		Me.電路板製程TableAdapter.ClearBeforeFill = True
		'
		'電子元件製程TableAdapter
		'
		Me.電子元件製程TableAdapter.ClearBeforeFill = True
		'
		'已選特殊零件TableAdapter
		'
		Me.已選特殊零件TableAdapter.ClearBeforeFill = True
		'
		'加工方式TableAdapter
		'
		Me.加工方式TableAdapter.ClearBeforeFill = True
		'
		'運輸工具TableAdapter
		'
		Me.運輸工具TableAdapter.ClearBeforeFill = True
		'
		'FlowLayoutPanel1
		'
		Me.FlowLayoutPanel1.Controls.Add(Me.TabControl2)
		Me.FlowLayoutPanel1.Location = New System.Drawing.Point(404, 16)
		Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
		Me.FlowLayoutPanel1.Size = New System.Drawing.Size(596, 599)
		Me.FlowLayoutPanel1.TabIndex = 1
		'
		'TabControl2
		'
		Me.TabControl2.Controls.Add(Me.TabPage4)
		Me.TabControl2.Controls.Add(Me.TabPage7)
		Me.TabControl2.Controls.Add(Me.TabPage8)
		Me.TabControl2.Location = New System.Drawing.Point(3, 3)
		Me.TabControl2.Name = "TabControl2"
		Me.TabControl2.SelectedIndex = 0
		Me.TabControl2.Size = New System.Drawing.Size(450, 591)
		Me.TabControl2.TabIndex = 0
		'
		'TabPage4
		'
		Me.TabPage4.Controls.Add(Me.TabControl3)
		Me.TabPage4.Controls.Add(Me.Button24)
		Me.TabPage4.Controls.Add(Me.Label54)
		Me.TabPage4.Controls.Add(Me.TextBox28)
		Me.TabPage4.Location = New System.Drawing.Point(4, 22)
		Me.TabPage4.Name = "TabPage4"
		Me.TabPage4.Padding = New System.Windows.Forms.Padding(3)
		Me.TabPage4.Size = New System.Drawing.Size(442, 565)
		Me.TabPage4.TabIndex = 0
		Me.TabPage4.Text = "新增"
		Me.TabPage4.UseVisualStyleBackColor = True
		'
		'TabPage7
		'
		Me.TabPage7.Location = New System.Drawing.Point(4, 22)
		Me.TabPage7.Name = "TabPage7"
		Me.TabPage7.Padding = New System.Windows.Forms.Padding(3)
		Me.TabPage7.Size = New System.Drawing.Size(442, 565)
		Me.TabPage7.TabIndex = 1
		Me.TabPage7.Text = "查詢"
		Me.TabPage7.UseVisualStyleBackColor = True
		'
		'TabPage8
		'
		Me.TabPage8.Location = New System.Drawing.Point(4, 22)
		Me.TabPage8.Name = "TabPage8"
		Me.TabPage8.Size = New System.Drawing.Size(442, 565)
		Me.TabPage8.TabIndex = 2
		Me.TabPage8.Text = "分析"
		Me.TabPage8.UseVisualStyleBackColor = True
		'
		'TextBox28
		'
		Me.TextBox28.Location = New System.Drawing.Point(102, 19)
		Me.TextBox28.Name = "TextBox28"
		Me.TextBox28.Size = New System.Drawing.Size(138, 22)
		Me.TextBox28.TabIndex = 1
		Me.TextBox28.Text = "透鏡模組"
		'
		'Label54
		'
		Me.Label54.AutoSize = True
		Me.Label54.Location = New System.Drawing.Point(16, 26)
		Me.Label54.Name = "Label54"
		Me.Label54.Size = New System.Drawing.Size(80, 12)
		Me.Label54.TabIndex = 2
		Me.Label54.Text = "產品/模組名稱"
		'
		'Button24
		'
		Me.Button24.Location = New System.Drawing.Point(246, 19)
		Me.Button24.Name = "Button24"
		Me.Button24.Size = New System.Drawing.Size(73, 22)
		Me.Button24.TabIndex = 3
		Me.Button24.Text = "新增"
		Me.Button24.UseVisualStyleBackColor = True
		'
		'TabControl3
		'
		Me.TabControl3.Controls.Add(Me.TabPage9)
		Me.TabControl3.Controls.Add(Me.TabPage10)
		Me.TabControl3.Controls.Add(Me.TabPage11)
		Me.TabControl3.Location = New System.Drawing.Point(16, 55)
		Me.TabControl3.Name = "TabControl3"
		Me.TabControl3.SelectedIndex = 0
		Me.TabControl3.Size = New System.Drawing.Size(405, 448)
		Me.TabControl3.TabIndex = 4
		'
		'TabPage9
		'
		Me.TabPage9.Controls.Add(Me.Button25)
		Me.TabPage9.Controls.Add(Me.GroupBox18)
		Me.TabPage9.Controls.Add(Me.GroupBox17)
		Me.TabPage9.Controls.Add(Me.GroupBox16)
		Me.TabPage9.Controls.Add(Me.GroupBox15)
		Me.TabPage9.Location = New System.Drawing.Point(4, 22)
		Me.TabPage9.Name = "TabPage9"
		Me.TabPage9.Padding = New System.Windows.Forms.Padding(3)
		Me.TabPage9.Size = New System.Drawing.Size(397, 422)
		Me.TabPage9.TabIndex = 0
		Me.TabPage9.Text = "一般零件"
		Me.TabPage9.UseVisualStyleBackColor = True
		'
		'TabPage10
		'
		Me.TabPage10.Location = New System.Drawing.Point(4, 22)
		Me.TabPage10.Name = "TabPage10"
		Me.TabPage10.Padding = New System.Windows.Forms.Padding(3)
		Me.TabPage10.Size = New System.Drawing.Size(397, 422)
		Me.TabPage10.TabIndex = 1
		Me.TabPage10.Text = "特殊零件"
		Me.TabPage10.UseVisualStyleBackColor = True
		'
		'TabPage11
		'
		Me.TabPage11.Location = New System.Drawing.Point(4, 22)
		Me.TabPage11.Name = "TabPage11"
		Me.TabPage11.Size = New System.Drawing.Size(397, 422)
		Me.TabPage11.TabIndex = 2
		Me.TabPage11.Text = "綜合分析"
		Me.TabPage11.UseVisualStyleBackColor = True
		'
		'GroupBox15
		'
		Me.GroupBox15.Location = New System.Drawing.Point(21, 17)
		Me.GroupBox15.Name = "GroupBox15"
		Me.GroupBox15.Size = New System.Drawing.Size(277, 80)
		Me.GroupBox15.TabIndex = 0
		Me.GroupBox15.TabStop = False
		Me.GroupBox15.Text = "選擇材料"
		'
		'GroupBox16
		'
		Me.GroupBox16.Location = New System.Drawing.Point(21, 103)
		Me.GroupBox16.Name = "GroupBox16"
		Me.GroupBox16.Size = New System.Drawing.Size(277, 80)
		Me.GroupBox16.TabIndex = 1
		Me.GroupBox16.TabStop = False
		Me.GroupBox16.Text = "製程"
		'
		'GroupBox17
		'
		Me.GroupBox17.Location = New System.Drawing.Point(22, 189)
		Me.GroupBox17.Name = "GroupBox17"
		Me.GroupBox17.Size = New System.Drawing.Size(277, 80)
		Me.GroupBox17.TabIndex = 2
		Me.GroupBox17.TabStop = False
		Me.GroupBox17.Text = "運輸"
		'
		'GroupBox18
		'
		Me.GroupBox18.Location = New System.Drawing.Point(22, 292)
		Me.GroupBox18.Name = "GroupBox18"
		Me.GroupBox18.Size = New System.Drawing.Size(277, 80)
		Me.GroupBox18.TabIndex = 3
		Me.GroupBox18.TabStop = False
		Me.GroupBox18.Text = "結果"
		'
		'Button25
		'
		Me.Button25.Location = New System.Drawing.Point(304, 27)
		Me.Button25.Name = "Button25"
		Me.Button25.Size = New System.Drawing.Size(63, 22)
		Me.Button25.TabIndex = 5
		Me.Button25.Text = "下一個"
		Me.Button25.UseVisualStyleBackColor = True
		'
		'Form1
		'
		Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.ClientSize = New System.Drawing.Size(353, 622)
		Me.Controls.Add(Me.FlowLayoutPanel1)
		Me.Controls.Add(Me.TabControl1)
		Me.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
		Me.Name = "Form1"
		Me.Text = "Water Footprint Evaluator"
		Me.TabPage6.ResumeLayout(False)
		Me.TabPage6.PerformLayout()
		Me.GroupBox14.ResumeLayout(False)
		Me.GroupBox14.PerformLayout()
		CType(Me.DataGridView10, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.運輸工具BindingSource, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.WFEDataSet2, System.ComponentModel.ISupportInitialize).EndInit()
		Me.GroupBox13.ResumeLayout(False)
		Me.GroupBox13.PerformLayout()
		CType(Me.DataGridView9, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.加工方式BindingSource, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.WFEDataSet5, System.ComponentModel.ISupportInitialize).EndInit()
		Me.GroupBox12.ResumeLayout(False)
		Me.GroupBox12.PerformLayout()
		CType(Me.DataGridView8, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.材料BindingSource, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.WFEDataSet, System.ComponentModel.ISupportInitialize).EndInit()
		Me.TabPage5.ResumeLayout(False)
		Me.TabPage5.PerformLayout()
		CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
		Me.TabPage3.ResumeLayout(False)
		Me.TabPage3.PerformLayout()
		Me.GroupBox11.ResumeLayout(False)
		CType(Me.DataGridView7, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.已選特殊零件BindingSource, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.WFEDataSet11, System.ComponentModel.ISupportInitialize).EndInit()
		Me.GroupBox10.ResumeLayout(False)
		Me.GroupBox10.PerformLayout()
		CType(Me.DataGridView6, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.電子元件製程BindingSource, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.WFEDataSet10, System.ComponentModel.ISupportInitialize).EndInit()
		Me.GroupBox9.ResumeLayout(False)
		Me.GroupBox9.PerformLayout()
		CType(Me.DataGridView5, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.電路板製程BindingSource, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.WFEDataSet9, System.ComponentModel.ISupportInitialize).EndInit()
		Me.TabPage2.ResumeLayout(False)
		Me.TabPage2.PerformLayout()
		Me.GroupBox8.ResumeLayout(False)
		CType(Me.DataGridView4, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.已選加工資訊BindingSource, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.WFEDataSet8, System.ComponentModel.ISupportInitialize).EndInit()
		Me.GroupBox7.ResumeLayout(False)
		Me.GroupBox6.ResumeLayout(False)
		Me.GroupBox5.ResumeLayout(False)
		Me.GroupBox5.PerformLayout()
		CType(Me.DataGridView3, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.已選一般零件BindingSource, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.WFEDataSet1, System.ComponentModel.ISupportInitialize).EndInit()
		Me.TabPage1.ResumeLayout(False)
		Me.TabPage1.PerformLayout()
		Me.GroupBox4.ResumeLayout(False)
		CType(Me.DataGridView2, System.ComponentModel.ISupportInitialize).EndInit()
		Me.GroupBox3.ResumeLayout(False)
		Me.GroupBox2.ResumeLayout(False)
		Me.GroupBox1.ResumeLayout(False)
		Me.GroupBox1.PerformLayout()
		CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
		Me.TabControl1.ResumeLayout(False)
		Me.FlowLayoutPanel1.ResumeLayout(False)
		Me.TabControl2.ResumeLayout(False)
		Me.TabPage4.ResumeLayout(False)
		Me.TabPage4.PerformLayout()
		Me.TabControl3.ResumeLayout(False)
		Me.TabPage9.ResumeLayout(False)
		Me.ResumeLayout(False)

	End Sub

	Friend WithEvents TabPage6 As Windows.Forms.TabPage
    Friend WithEvents TabPage5 As Windows.Forms.TabPage
    Friend WithEvents TabPage3 As Windows.Forms.TabPage
    Friend WithEvents GroupBox9 As Windows.Forms.GroupBox
    Friend WithEvents Button13 As Windows.Forms.Button
    Friend WithEvents TextBox10 As Windows.Forms.TextBox
    Friend WithEvents TextBox9 As Windows.Forms.TextBox
    Friend WithEvents DataGridView5 As Windows.Forms.DataGridView
    Friend WithEvents TextBox8 As Windows.Forms.TextBox
    Friend WithEvents Label17 As Windows.Forms.Label
    Friend WithEvents Label16 As Windows.Forms.Label
    Friend WithEvents Label15 As Windows.Forms.Label
    Friend WithEvents Label14 As Windows.Forms.Label
    Friend WithEvents TabPage2 As Windows.Forms.TabPage
    Friend WithEvents Button12 As Windows.Forms.Button
    Friend WithEvents Label13 As Windows.Forms.Label
    Friend WithEvents TextBox7 As Windows.Forms.TextBox
    Friend WithEvents Label12 As Windows.Forms.Label
    Friend WithEvents GroupBox8 As Windows.Forms.GroupBox
    Friend WithEvents Button11 As Windows.Forms.Button
    Friend WithEvents DataGridView4 As Windows.Forms.DataGridView
    Friend WithEvents GroupBox7 As Windows.Forms.GroupBox
    Friend WithEvents Button10 As Windows.Forms.Button
    Friend WithEvents GroupBox6 As Windows.Forms.GroupBox
    Friend WithEvents Button9 As Windows.Forms.Button
    Friend WithEvents GroupBox5 As Windows.Forms.GroupBox
    Friend WithEvents Button8 As Windows.Forms.Button
    Friend WithEvents Label11 As Windows.Forms.Label
    Friend WithEvents TextBox6 As Windows.Forms.TextBox
    Friend WithEvents DataGridView3 As Windows.Forms.DataGridView
    Friend WithEvents TabPage1 As Windows.Forms.TabPage
    Friend WithEvents Button7 As Windows.Forms.Button
    Friend WithEvents Label10 As Windows.Forms.Label
    Friend WithEvents TextBox5 As Windows.Forms.TextBox
    Friend WithEvents Label9 As Windows.Forms.Label
    Friend WithEvents GroupBox4 As Windows.Forms.GroupBox
    Friend WithEvents Button6 As Windows.Forms.Button
    Friend WithEvents DataGridView2 As Windows.Forms.DataGridView
    Friend WithEvents GroupBox3 As Windows.Forms.GroupBox
    Friend WithEvents Button5 As Windows.Forms.Button
    Friend WithEvents GroupBox2 As Windows.Forms.GroupBox
    Friend WithEvents Button4 As Windows.Forms.Button
    Friend WithEvents GroupBox1 As Windows.Forms.GroupBox
    Friend WithEvents Label8 As Windows.Forms.Label
    Friend WithEvents Label7 As Windows.Forms.Label
    Friend WithEvents Label6 As Windows.Forms.Label
    Friend WithEvents Button3 As Windows.Forms.Button
    Friend WithEvents Button2 As Windows.Forms.Button
    Friend WithEvents Button1 As Windows.Forms.Button
    Friend WithEvents TextBox4 As Windows.Forms.TextBox
    Friend WithEvents TextBox3 As Windows.Forms.TextBox
    Friend WithEvents TextBox2 As Windows.Forms.TextBox
    Friend WithEvents Label5 As Windows.Forms.Label
    Friend WithEvents TextBox1 As Windows.Forms.TextBox
    Friend WithEvents DataGridView1 As Windows.Forms.DataGridView
    Friend WithEvents Label4 As Windows.Forms.Label
    Friend WithEvents Label3 As Windows.Forms.Label
    Friend WithEvents Label2 As Windows.Forms.Label
    Friend WithEvents Label1 As Windows.Forms.Label
    Friend WithEvents TabControl1 As Windows.Forms.TabControl
    Friend WithEvents GroupBox10 As Windows.Forms.GroupBox
    Friend WithEvents Label19 As Windows.Forms.Label
    Friend WithEvents Label18 As Windows.Forms.Label
    Friend WithEvents Button15 As Windows.Forms.Button
    Friend WithEvents Button14 As Windows.Forms.Button
    Friend WithEvents GroupBox11 As Windows.Forms.GroupBox
    Friend WithEvents Label25 As Windows.Forms.Label
    Friend WithEvents Label24 As Windows.Forms.Label
    Friend WithEvents TextBox13 As Windows.Forms.TextBox
    Friend WithEvents TextBox12 As Windows.Forms.TextBox
    Friend WithEvents Button18 As Windows.Forms.Button
    Friend WithEvents Button17 As Windows.Forms.Button
    Friend WithEvents Button16 As Windows.Forms.Button
    Friend WithEvents DataGridView6 As Windows.Forms.DataGridView
    Friend WithEvents TextBox11 As Windows.Forms.TextBox
    Friend WithEvents Label23 As Windows.Forms.Label
    Friend WithEvents Label22 As Windows.Forms.Label
    Friend WithEvents Label21 As Windows.Forms.Label
    Friend WithEvents Label20 As Windows.Forms.Label
    Friend WithEvents Button20 As Windows.Forms.Button
    Friend WithEvents TextBox14 As Windows.Forms.TextBox
    Friend WithEvents Label27 As Windows.Forms.Label
    Friend WithEvents Label26 As Windows.Forms.Label
    Friend WithEvents Button19 As Windows.Forms.Button
    Friend WithEvents DataGridView7 As Windows.Forms.DataGridView
    Friend WithEvents Label38 As Windows.Forms.Label
    Friend WithEvents Label37 As Windows.Forms.Label
    Friend WithEvents TextBox18 As Windows.Forms.TextBox
    Friend WithEvents TextBox17 As Windows.Forms.TextBox
    Friend WithEvents TextBox16 As Windows.Forms.TextBox
    Friend WithEvents Label36 As Windows.Forms.Label
    Friend WithEvents TextBox15 As Windows.Forms.TextBox
    Friend WithEvents Label35 As Windows.Forms.Label
    Friend WithEvents Label34 As Windows.Forms.Label
    Friend WithEvents Label33 As Windows.Forms.Label
    Friend WithEvents Label32 As Windows.Forms.Label
    Friend WithEvents Label31 As Windows.Forms.Label
    Friend WithEvents Label30 As Windows.Forms.Label
    Friend WithEvents Label29 As Windows.Forms.Label
    Friend WithEvents Label28 As Windows.Forms.Label
    Friend WithEvents PictureBox1 As Windows.Forms.PictureBox
    Friend WithEvents Button21 As Windows.Forms.Button
    Friend WithEvents Label39 As Windows.Forms.Label
    Friend WithEvents RadioButton3 As Windows.Forms.RadioButton
    Friend WithEvents GroupBox14 As Windows.Forms.GroupBox
    Friend WithEvents GroupBox13 As Windows.Forms.GroupBox
    Friend WithEvents RadioButton2 As Windows.Forms.RadioButton
    Friend WithEvents RadioButton1 As Windows.Forms.RadioButton
    Friend WithEvents GroupBox12 As Windows.Forms.GroupBox
    Friend WithEvents Label42 As Windows.Forms.Label
    Friend WithEvents TextBox20 As Windows.Forms.TextBox
    Friend WithEvents TextBox19 As Windows.Forms.TextBox
    Friend WithEvents Label41 As Windows.Forms.Label
    Friend WithEvents Label40 As Windows.Forms.Label
    Friend WithEvents DataGridView8 As Windows.Forms.DataGridView
    Friend WithEvents Button23 As Windows.Forms.Button
    Friend WithEvents Button22 As Windows.Forms.Button
    Friend WithEvents DataGridView10 As Windows.Forms.DataGridView
    Friend WithEvents DataGridView9 As Windows.Forms.DataGridView
    Friend WithEvents Label44 As Windows.Forms.Label
    Friend WithEvents TextBox21 As Windows.Forms.TextBox
    Friend WithEvents Label43 As Windows.Forms.Label
    Friend WithEvents TextBox24 As Windows.Forms.TextBox
    Friend WithEvents TextBox23 As Windows.Forms.TextBox
    Friend WithEvents TextBox22 As Windows.Forms.TextBox
    Friend WithEvents Label47 As Windows.Forms.Label
    Friend WithEvents Label46 As Windows.Forms.Label
    Friend WithEvents Label45 As Windows.Forms.Label
    Friend WithEvents Label49 As Windows.Forms.Label
    Friend WithEvents Label48 As Windows.Forms.Label
    Friend WithEvents Label51 As Windows.Forms.Label
    Friend WithEvents Label50 As Windows.Forms.Label
    Friend WithEvents Label53 As Windows.Forms.Label
    Friend WithEvents Label52 As Windows.Forms.Label
    Friend WithEvents TextBox27 As Windows.Forms.TextBox
    Friend WithEvents TextBox26 As Windows.Forms.TextBox
    Friend WithEvents TextBox25 As Windows.Forms.TextBox
    Friend WithEvents WFEDataSet As WFEDataSet
    Friend WithEvents 材料BindingSource As Windows.Forms.BindingSource
    Friend WithEvents 材料TableAdapter As WFEDataSetTableAdapters.材料TableAdapter
    Friend WithEvents 識別碼DataGridViewTextBoxColumn As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents 材料DataGridViewTextBoxColumn As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents 密度kgm3DataGridViewTextBoxColumn As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents 藍水m3DataGridViewTextBoxColumn As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents 綠水m3DataGridViewTextBoxColumn As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents 灰水m3DataGridViewTextBoxColumn As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents 總水足跡m3DataGridViewTextBoxColumn As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents WFEDataSet1 As WFEDataSet1
    Friend WithEvents 已選一般零件BindingSource As Windows.Forms.BindingSource
    Friend WithEvents 已選一般零件TableAdapter As WFEDataSet1TableAdapters.已選一般零件TableAdapter
    Friend WithEvents 識別碼DataGridViewTextBoxColumn2 As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents 零件名稱DataGridViewTextBoxColumn1 As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents 材料DataGridViewTextBoxColumn2 As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents 質量DataGridViewTextBoxColumn1 As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents 水足跡DataGridViewTextBoxColumn1 As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents 加工方式DataGridViewTextBoxColumn1 As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents 識別碼DataGridViewTextBoxColumn1 As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents 零件名稱DataGridViewTextBoxColumn As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents 材料DataGridViewTextBoxColumn1 As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents 質量DataGridViewTextBoxColumn As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents 水足跡DataGridViewTextBoxColumn As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents 加工方式DataGridViewTextBoxColumn As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents WFEDataSet8 As WFEDataSet8
    Friend WithEvents 已選加工資訊BindingSource As Windows.Forms.BindingSource
    Friend WithEvents 已選加工資訊TableAdapter As WFEDataSet8TableAdapters.已選加工資訊TableAdapter
    Friend WithEvents 識別碼DataGridViewTextBoxColumn3 As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents 方式DataGridViewTextBoxColumn As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents 耗電時DataGridViewTextBoxColumn As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents 數量DataGridViewTextBoxColumn As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents 計算單位DataGridViewTextBoxColumn As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents 係數DataGridViewTextBoxColumn As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents 水足跡DataGridViewTextBoxColumn2 As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents WFEDataSet9 As WFEDataSet9
    Friend WithEvents 電路板製程BindingSource As Windows.Forms.BindingSource
    Friend WithEvents 電路板製程TableAdapter As WFEDataSet9TableAdapters.電路板製程TableAdapter
    Friend WithEvents 識別碼DataGridViewTextBoxColumn4 As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents 製程DataGridViewTextBoxColumn As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents 藍水m3DataGridViewTextBoxColumn1 As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents 綠水m3DataGridViewTextBoxColumn1 As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents 灰水m3DataGridViewTextBoxColumn1 As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents 總水足跡m3DataGridViewTextBoxColumn1 As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents WFEDataSet10 As WFEDataSet10
    Friend WithEvents 電子元件製程BindingSource As Windows.Forms.BindingSource
    Friend WithEvents 電子元件製程TableAdapter As WFEDataSet10TableAdapters.電子元件製程TableAdapter
    Friend WithEvents 識別碼DataGridViewTextBoxColumn5 As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents 製程DataGridViewTextBoxColumn1 As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents 藍水m3DataGridViewTextBoxColumn2 As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents 綠水m3DataGridViewTextBoxColumn2 As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents 灰水m3DataGridViewTextBoxColumn2 As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents 總水足跡m3DataGridViewTextBoxColumn2 As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents WFEDataSet11 As WFEDataSet11
    Friend WithEvents 已選特殊零件BindingSource As Windows.Forms.BindingSource
    Friend WithEvents 已選特殊零件TableAdapter As WFEDataSet11TableAdapters.已選特殊零件TableAdapter
    Friend WithEvents 識別碼DataGridViewTextBoxColumn7 As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents 材料DataGridViewTextBoxColumn4 As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents 密度kgm3DataGridViewTextBoxColumn1 As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents 藍水m3DataGridViewTextBoxColumn3 As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents 綠水m3DataGridViewTextBoxColumn3 As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents 灰水m3DataGridViewTextBoxColumn3 As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents 總水足跡m3DataGridViewTextBoxColumn3 As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents 識別碼DataGridViewTextBoxColumn6 As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents 零件名稱DataGridViewTextBoxColumn2 As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents 材料DataGridViewTextBoxColumn3 As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents 質量DataGridViewTextBoxColumn2 As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents 碳足跡DataGridViewTextBoxColumn As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents 加工方式DataGridViewTextBoxColumn2 As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents 特殊零件DataGridViewTextBoxColumn As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents WFEDataSet5 As WFEDataSet5
    Friend WithEvents 加工方式BindingSource As Windows.Forms.BindingSource
    Friend WithEvents 加工方式TableAdapter As WFEDataSet5TableAdapters.加工方式TableAdapter
    Friend WithEvents 識別碼DataGridViewTextBoxColumn8 As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents 方式DataGridViewTextBoxColumn1 As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents 計算單位DataGridViewTextBoxColumn1 As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents 水足跡m3DataGridViewTextBoxColumn As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents 藍水m3DataGridViewTextBoxColumn4 As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents 綠水m3DataGridViewTextBoxColumn4 As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents 灰水m3DataGridViewTextBoxColumn4 As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents 機具名稱DataGridViewTextBoxColumn As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents 能源單位DataGridViewTextBoxColumn As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents 耗能時DataGridViewTextBoxColumn As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents WFEDataSet2 As WFEDataSet2
    Friend WithEvents 運輸工具BindingSource As Windows.Forms.BindingSource
    Friend WithEvents 運輸工具TableAdapter As WFEDataSet2TableAdapters.運輸工具TableAdapter
    Friend WithEvents 識別碼DataGridViewTextBoxColumn9 As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents 種類DataGridViewTextBoxColumn As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents 計算單位DataGridViewTextBoxColumn2 As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents 最大載貨重量DataGridViewTextBoxColumn As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents 總水足跡m3DataGridViewTextBoxColumn4 As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents 藍水m3DataGridViewTextBoxColumn5 As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents 綠水m3DataGridViewTextBoxColumn5 As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents 灰水m3DataGridViewTextBoxColumn5 As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents 單位1DataGridViewTextBoxColumn As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents 能源單位1DataGridViewTextBoxColumn As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents 能耗量1DataGridViewTextBoxColumn As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents 耗能係數1DataGridViewTextBoxColumn As Windows.Forms.DataGridViewTextBoxColumn
	Friend WithEvents FlowLayoutPanel1 As Windows.Forms.FlowLayoutPanel
	Friend WithEvents TabControl2 As Windows.Forms.TabControl
	Friend WithEvents TabPage4 As Windows.Forms.TabPage
	Friend WithEvents TabControl3 As Windows.Forms.TabControl
	Friend WithEvents TabPage9 As Windows.Forms.TabPage
	Friend WithEvents Button25 As Windows.Forms.Button
	Friend WithEvents GroupBox18 As Windows.Forms.GroupBox
	Friend WithEvents GroupBox17 As Windows.Forms.GroupBox
	Friend WithEvents GroupBox16 As Windows.Forms.GroupBox
	Friend WithEvents GroupBox15 As Windows.Forms.GroupBox
	Friend WithEvents TabPage10 As Windows.Forms.TabPage
	Friend WithEvents TabPage11 As Windows.Forms.TabPage
	Friend WithEvents Button24 As Windows.Forms.Button
	Friend WithEvents Label54 As Windows.Forms.Label
	Friend WithEvents TextBox28 As Windows.Forms.TextBox
	Friend WithEvents TabPage7 As Windows.Forms.TabPage
	Friend WithEvents TabPage8 As Windows.Forms.TabPage
End Class
