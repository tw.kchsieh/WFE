﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form3
    Inherits System.Windows.Forms.Form

    'Form 覆寫 Dispose 以清除元件清單。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    '為 Windows Form 設計工具的必要項
    Private components As System.ComponentModel.IContainer

    '注意: 以下為 Windows Form 設計工具所需的程序
    '可以使用 Windows Form 設計工具進行修改。
    '請勿使用程式碼編輯器進行修改。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.識別碼DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.方式DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.計算單位DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.水足跡m3DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.藍水m3DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.綠水m3DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.灰水m3DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.機具名稱DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.能源單位DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.耗能時DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.加工方式BindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.WFEDataSet5 = New WFE.WFEDataSet5()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.DataGridView2 = New System.Windows.Forms.DataGridView()
        Me.識別碼DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.方式DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.耗電時DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.數量DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.計算單位DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.係數DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.水足跡DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.已選加工資訊BindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.WFEDataSet6 = New WFE.WFEDataSet6()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.加工方式TableAdapter = New WFE.WFEDataSet5TableAdapters.加工方式TableAdapter()
        Me.已選加工資訊TableAdapter = New WFE.WFEDataSet6TableAdapters.已選加工資訊TableAdapter()
        Me.GroupBox1.SuspendLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.加工方式BindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.WFEDataSet5, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        CType(Me.DataGridView2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.已選加工資訊BindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.WFEDataSet6, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Button1)
        Me.GroupBox1.Controls.Add(Me.TextBox1)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.DataGridView1)
        Me.GroupBox1.Font = New System.Drawing.Font("微軟正黑體", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(12, 12)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(338, 217)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "加工方式"
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(241, 162)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(84, 31)
        Me.Button1.TabIndex = 3
        Me.Button1.Text = "確定/新增"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'TextBox1
        '
        Me.TextBox1.Location = New System.Drawing.Point(20, 166)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(165, 27)
        Me.TextBox1.TabIndex = 2
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(191, 169)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(24, 19)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "無"
        '
        'DataGridView1
        '
        Me.DataGridView1.AutoGenerateColumns = False
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.識別碼DataGridViewTextBoxColumn, Me.方式DataGridViewTextBoxColumn, Me.計算單位DataGridViewTextBoxColumn, Me.水足跡m3DataGridViewTextBoxColumn, Me.藍水m3DataGridViewTextBoxColumn, Me.綠水m3DataGridViewTextBoxColumn, Me.灰水m3DataGridViewTextBoxColumn, Me.機具名稱DataGridViewTextBoxColumn, Me.能源單位DataGridViewTextBoxColumn, Me.耗能時DataGridViewTextBoxColumn})
        Me.DataGridView1.DataSource = Me.加工方式BindingSource
        Me.DataGridView1.Location = New System.Drawing.Point(19, 26)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.RowTemplate.Height = 27
        Me.DataGridView1.Size = New System.Drawing.Size(305, 118)
        Me.DataGridView1.TabIndex = 0
        '
        '識別碼DataGridViewTextBoxColumn
        '
        Me.識別碼DataGridViewTextBoxColumn.DataPropertyName = "識別碼"
        Me.識別碼DataGridViewTextBoxColumn.HeaderText = "識別碼"
        Me.識別碼DataGridViewTextBoxColumn.Name = "識別碼DataGridViewTextBoxColumn"
        '
        '方式DataGridViewTextBoxColumn
        '
        Me.方式DataGridViewTextBoxColumn.DataPropertyName = "方式"
        Me.方式DataGridViewTextBoxColumn.HeaderText = "方式"
        Me.方式DataGridViewTextBoxColumn.Name = "方式DataGridViewTextBoxColumn"
        '
        '計算單位DataGridViewTextBoxColumn
        '
        Me.計算單位DataGridViewTextBoxColumn.DataPropertyName = "計算單位"
        Me.計算單位DataGridViewTextBoxColumn.HeaderText = "計算單位"
        Me.計算單位DataGridViewTextBoxColumn.Name = "計算單位DataGridViewTextBoxColumn"
        '
        '水足跡m3DataGridViewTextBoxColumn
        '
        Me.水足跡m3DataGridViewTextBoxColumn.DataPropertyName = "水足跡(m3)"
        Me.水足跡m3DataGridViewTextBoxColumn.HeaderText = "水足跡(m3)"
        Me.水足跡m3DataGridViewTextBoxColumn.Name = "水足跡m3DataGridViewTextBoxColumn"
        '
        '藍水m3DataGridViewTextBoxColumn
        '
        Me.藍水m3DataGridViewTextBoxColumn.DataPropertyName = "藍水(m3)"
        Me.藍水m3DataGridViewTextBoxColumn.HeaderText = "藍水(m3)"
        Me.藍水m3DataGridViewTextBoxColumn.Name = "藍水m3DataGridViewTextBoxColumn"
        '
        '綠水m3DataGridViewTextBoxColumn
        '
        Me.綠水m3DataGridViewTextBoxColumn.DataPropertyName = "綠水(m3)"
        Me.綠水m3DataGridViewTextBoxColumn.HeaderText = "綠水(m3)"
        Me.綠水m3DataGridViewTextBoxColumn.Name = "綠水m3DataGridViewTextBoxColumn"
        '
        '灰水m3DataGridViewTextBoxColumn
        '
        Me.灰水m3DataGridViewTextBoxColumn.DataPropertyName = "灰水(m3)"
        Me.灰水m3DataGridViewTextBoxColumn.HeaderText = "灰水(m3)"
        Me.灰水m3DataGridViewTextBoxColumn.Name = "灰水m3DataGridViewTextBoxColumn"
        '
        '機具名稱DataGridViewTextBoxColumn
        '
        Me.機具名稱DataGridViewTextBoxColumn.DataPropertyName = "機具名稱"
        Me.機具名稱DataGridViewTextBoxColumn.HeaderText = "機具名稱"
        Me.機具名稱DataGridViewTextBoxColumn.Name = "機具名稱DataGridViewTextBoxColumn"
        '
        '能源單位DataGridViewTextBoxColumn
        '
        Me.能源單位DataGridViewTextBoxColumn.DataPropertyName = "能源/單位"
        Me.能源單位DataGridViewTextBoxColumn.HeaderText = "能源/單位"
        Me.能源單位DataGridViewTextBoxColumn.Name = "能源單位DataGridViewTextBoxColumn"
        '
        '耗能時DataGridViewTextBoxColumn
        '
        Me.耗能時DataGridViewTextBoxColumn.DataPropertyName = "耗能 / 時"
        Me.耗能時DataGridViewTextBoxColumn.HeaderText = "耗能 / 時"
        Me.耗能時DataGridViewTextBoxColumn.Name = "耗能時DataGridViewTextBoxColumn"
        '
        '加工方式BindingSource
        '
        Me.加工方式BindingSource.DataMember = "加工方式"
        Me.加工方式BindingSource.DataSource = Me.WFEDataSet5
        '
        'WFEDataSet5
        '
        Me.WFEDataSet5.DataSetName = "WFEDataSet5"
        Me.WFEDataSet5.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.Button2)
        Me.GroupBox2.Controls.Add(Me.DataGridView2)
        Me.GroupBox2.Font = New System.Drawing.Font("微軟正黑體", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(12, 235)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(338, 194)
        Me.GroupBox2.TabIndex = 1
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "已選加工紀錄"
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(115, 150)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(111, 30)
        Me.Button2.TabIndex = 1
        Me.Button2.Text = "刪除"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'DataGridView2
        '
        Me.DataGridView2.AutoGenerateColumns = False
        Me.DataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView2.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.識別碼DataGridViewTextBoxColumn1, Me.方式DataGridViewTextBoxColumn1, Me.耗電時DataGridViewTextBoxColumn, Me.數量DataGridViewTextBoxColumn, Me.計算單位DataGridViewTextBoxColumn1, Me.係數DataGridViewTextBoxColumn, Me.水足跡DataGridViewTextBoxColumn})
        Me.DataGridView2.DataSource = Me.已選加工資訊BindingSource
        Me.DataGridView2.Location = New System.Drawing.Point(19, 26)
        Me.DataGridView2.Name = "DataGridView2"
        Me.DataGridView2.RowTemplate.Height = 27
        Me.DataGridView2.Size = New System.Drawing.Size(305, 118)
        Me.DataGridView2.TabIndex = 0
        '
        '識別碼DataGridViewTextBoxColumn1
        '
        Me.識別碼DataGridViewTextBoxColumn1.DataPropertyName = "識別碼"
        Me.識別碼DataGridViewTextBoxColumn1.HeaderText = "識別碼"
        Me.識別碼DataGridViewTextBoxColumn1.Name = "識別碼DataGridViewTextBoxColumn1"
        '
        '方式DataGridViewTextBoxColumn1
        '
        Me.方式DataGridViewTextBoxColumn1.DataPropertyName = "方式"
        Me.方式DataGridViewTextBoxColumn1.HeaderText = "方式"
        Me.方式DataGridViewTextBoxColumn1.Name = "方式DataGridViewTextBoxColumn1"
        '
        '耗電時DataGridViewTextBoxColumn
        '
        Me.耗電時DataGridViewTextBoxColumn.DataPropertyName = "耗電/時"
        Me.耗電時DataGridViewTextBoxColumn.HeaderText = "耗電/時"
        Me.耗電時DataGridViewTextBoxColumn.Name = "耗電時DataGridViewTextBoxColumn"
        '
        '數量DataGridViewTextBoxColumn
        '
        Me.數量DataGridViewTextBoxColumn.DataPropertyName = "數量"
        Me.數量DataGridViewTextBoxColumn.HeaderText = "數量"
        Me.數量DataGridViewTextBoxColumn.Name = "數量DataGridViewTextBoxColumn"
        '
        '計算單位DataGridViewTextBoxColumn1
        '
        Me.計算單位DataGridViewTextBoxColumn1.DataPropertyName = "計算單位"
        Me.計算單位DataGridViewTextBoxColumn1.HeaderText = "計算單位"
        Me.計算單位DataGridViewTextBoxColumn1.Name = "計算單位DataGridViewTextBoxColumn1"
        '
        '係數DataGridViewTextBoxColumn
        '
        Me.係數DataGridViewTextBoxColumn.DataPropertyName = "係數"
        Me.係數DataGridViewTextBoxColumn.HeaderText = "係數"
        Me.係數DataGridViewTextBoxColumn.Name = "係數DataGridViewTextBoxColumn"
        '
        '水足跡DataGridViewTextBoxColumn
        '
        Me.水足跡DataGridViewTextBoxColumn.DataPropertyName = "水足跡"
        Me.水足跡DataGridViewTextBoxColumn.HeaderText = "水足跡"
        Me.水足跡DataGridViewTextBoxColumn.Name = "水足跡DataGridViewTextBoxColumn"
        '
        '已選加工資訊BindingSource
        '
        Me.已選加工資訊BindingSource.DataMember = "已選加工資訊"
        Me.已選加工資訊BindingSource.DataSource = Me.WFEDataSet6
        '
        'WFEDataSet6
        '
        Me.WFEDataSet6.DataSetName = "WFEDataSet6"
        Me.WFEDataSet6.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Button3
        '
        Me.Button3.Location = New System.Drawing.Point(127, 435)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(111, 30)
        Me.Button3.TabIndex = 2
        Me.Button3.Text = "完成"
        Me.Button3.UseVisualStyleBackColor = True
        '
        '加工方式TableAdapter
        '
        Me.加工方式TableAdapter.ClearBeforeFill = True
        '
        '已選加工資訊TableAdapter
        '
        Me.已選加工資訊TableAdapter.ClearBeforeFill = True
        '
        'Form3
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(362, 480)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Name = "Form3"
        Me.Text = "Water Footprint Evaluator"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.加工方式BindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.WFEDataSet5, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        CType(Me.DataGridView2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.已選加工資訊BindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.WFEDataSet6, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents GroupBox1 As Windows.Forms.GroupBox
    Friend WithEvents GroupBox2 As Windows.Forms.GroupBox
    Friend WithEvents DataGridView1 As Windows.Forms.DataGridView
    Friend WithEvents Button1 As Windows.Forms.Button
    Friend WithEvents TextBox1 As Windows.Forms.TextBox
    Friend WithEvents Label1 As Windows.Forms.Label
    Friend WithEvents DataGridView2 As Windows.Forms.DataGridView
    Friend WithEvents Button2 As Windows.Forms.Button
    Friend WithEvents Button3 As Windows.Forms.Button
    Friend WithEvents WFEDataSet5 As WFEDataSet5
    Friend WithEvents 加工方式BindingSource As Windows.Forms.BindingSource
    Friend WithEvents 加工方式TableAdapter As WFEDataSet5TableAdapters.加工方式TableAdapter
    Friend WithEvents 識別碼DataGridViewTextBoxColumn As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents 方式DataGridViewTextBoxColumn As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents 計算單位DataGridViewTextBoxColumn As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents 水足跡m3DataGridViewTextBoxColumn As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents 藍水m3DataGridViewTextBoxColumn As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents 綠水m3DataGridViewTextBoxColumn As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents 灰水m3DataGridViewTextBoxColumn As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents 機具名稱DataGridViewTextBoxColumn As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents 能源單位DataGridViewTextBoxColumn As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents 耗能時DataGridViewTextBoxColumn As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents WFEDataSet6 As WFEDataSet6
    Friend WithEvents 已選加工資訊BindingSource As Windows.Forms.BindingSource
    Friend WithEvents 已選加工資訊TableAdapter As WFEDataSet6TableAdapters.已選加工資訊TableAdapter
    Friend WithEvents 識別碼DataGridViewTextBoxColumn1 As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents 方式DataGridViewTextBoxColumn1 As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents 耗電時DataGridViewTextBoxColumn As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents 數量DataGridViewTextBoxColumn As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents 計算單位DataGridViewTextBoxColumn1 As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents 係數DataGridViewTextBoxColumn As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents 水足跡DataGridViewTextBoxColumn As Windows.Forms.DataGridViewTextBoxColumn
End Class
