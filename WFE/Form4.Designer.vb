﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form4
    Inherits System.Windows.Forms.Form

    'Form 覆寫 Dispose 以清除元件清單。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    '為 Windows Form 設計工具的必要項
    Private components As System.ComponentModel.IContainer

    '注意: 以下為 Windows Form 設計工具所需的程序
    '可以使用 Windows Form 設計工具進行修改。
    '請勿使用程式碼編輯器進行修改。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.識別碼DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.產品名稱DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.一般零件製程總水足跡DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.後製加工總水足跡DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.產品總水足跡DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.產品總資訊BindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.WFEDataSet7 = New WFE.WFEDataSet7()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.產品總資訊TableAdapter = New WFE.WFEDataSet7TableAdapters.產品總資訊TableAdapter()
        Me.GroupBox1.SuspendLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.產品總資訊BindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.WFEDataSet7, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Button2)
        Me.GroupBox1.Controls.Add(Me.Button1)
        Me.GroupBox1.Controls.Add(Me.DataGridView1)
        Me.GroupBox1.Font = New System.Drawing.Font("微軟正黑體", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(12, 12)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(303, 213)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "產品選擇"
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(166, 177)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(111, 30)
        Me.Button2.TabIndex = 2
        Me.Button2.Text = "刪除產品紀錄"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(26, 177)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(111, 30)
        Me.Button1.TabIndex = 1
        Me.Button1.Text = "進行圖表統計"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'DataGridView1
        '
        Me.DataGridView1.AutoGenerateColumns = False
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.識別碼DataGridViewTextBoxColumn, Me.產品名稱DataGridViewTextBoxColumn, Me.一般零件製程總水足跡DataGridViewTextBoxColumn, Me.後製加工總水足跡DataGridViewTextBoxColumn, Me.產品總水足跡DataGridViewTextBoxColumn})
        Me.DataGridView1.DataSource = Me.產品總資訊BindingSource
        Me.DataGridView1.Location = New System.Drawing.Point(9, 24)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.RowTemplate.Height = 27
        Me.DataGridView1.Size = New System.Drawing.Size(281, 147)
        Me.DataGridView1.TabIndex = 0
        '
        '識別碼DataGridViewTextBoxColumn
        '
        Me.識別碼DataGridViewTextBoxColumn.DataPropertyName = "識別碼"
        Me.識別碼DataGridViewTextBoxColumn.HeaderText = "識別碼"
        Me.識別碼DataGridViewTextBoxColumn.Name = "識別碼DataGridViewTextBoxColumn"
        '
        '產品名稱DataGridViewTextBoxColumn
        '
        Me.產品名稱DataGridViewTextBoxColumn.DataPropertyName = "產品名稱"
        Me.產品名稱DataGridViewTextBoxColumn.HeaderText = "產品名稱"
        Me.產品名稱DataGridViewTextBoxColumn.Name = "產品名稱DataGridViewTextBoxColumn"
        '
        '一般零件製程總水足跡DataGridViewTextBoxColumn
        '
        Me.一般零件製程總水足跡DataGridViewTextBoxColumn.DataPropertyName = "一般零件製程總水足跡"
        Me.一般零件製程總水足跡DataGridViewTextBoxColumn.HeaderText = "一般零件製程總水足跡"
        Me.一般零件製程總水足跡DataGridViewTextBoxColumn.Name = "一般零件製程總水足跡DataGridViewTextBoxColumn"
        '
        '後製加工總水足跡DataGridViewTextBoxColumn
        '
        Me.後製加工總水足跡DataGridViewTextBoxColumn.DataPropertyName = "後製加工總水足跡"
        Me.後製加工總水足跡DataGridViewTextBoxColumn.HeaderText = "後製加工總水足跡"
        Me.後製加工總水足跡DataGridViewTextBoxColumn.Name = "後製加工總水足跡DataGridViewTextBoxColumn"
        '
        '產品總水足跡DataGridViewTextBoxColumn
        '
        Me.產品總水足跡DataGridViewTextBoxColumn.DataPropertyName = "產品總水足跡"
        Me.產品總水足跡DataGridViewTextBoxColumn.HeaderText = "產品總水足跡"
        Me.產品總水足跡DataGridViewTextBoxColumn.Name = "產品總水足跡DataGridViewTextBoxColumn"
        '
        '產品總資訊BindingSource
        '
        Me.產品總資訊BindingSource.DataMember = "產品總資訊"
        Me.產品總資訊BindingSource.DataSource = Me.WFEDataSet7
        '
        'WFEDataSet7
        '
        Me.WFEDataSet7.DataSetName = "WFEDataSet7"
        Me.WFEDataSet7.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Button3
        '
        Me.Button3.Location = New System.Drawing.Point(101, 231)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(111, 30)
        Me.Button3.TabIndex = 1
        Me.Button3.Text = "關閉"
        Me.Button3.UseVisualStyleBackColor = True
        '
        '產品總資訊TableAdapter
        '
        Me.產品總資訊TableAdapter.ClearBeforeFill = True
        '
        'Form4
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(326, 268)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.GroupBox1)
        Me.Name = "Form4"
        Me.Text = "Water Footprint Evaluator"
        Me.GroupBox1.ResumeLayout(False)
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.產品總資訊BindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.WFEDataSet7, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents GroupBox1 As Windows.Forms.GroupBox
    Friend WithEvents DataGridView1 As Windows.Forms.DataGridView
    Friend WithEvents Button1 As Windows.Forms.Button
    Friend WithEvents Button2 As Windows.Forms.Button
    Friend WithEvents Button3 As Windows.Forms.Button
    Friend WithEvents WFEDataSet7 As WFEDataSet7
    Friend WithEvents 產品總資訊BindingSource As Windows.Forms.BindingSource
    Friend WithEvents 產品總資訊TableAdapter As WFEDataSet7TableAdapters.產品總資訊TableAdapter
    Friend WithEvents 識別碼DataGridViewTextBoxColumn As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents 產品名稱DataGridViewTextBoxColumn As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents 一般零件製程總水足跡DataGridViewTextBoxColumn As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents 後製加工總水足跡DataGridViewTextBoxColumn As Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents 產品總水足跡DataGridViewTextBoxColumn As Windows.Forms.DataGridViewTextBoxColumn
End Class
