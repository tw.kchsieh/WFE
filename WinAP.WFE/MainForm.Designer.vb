﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class MainForm
	Inherits System.Windows.Forms.Form

	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()>
	Protected Overrides Sub Dispose(ByVal disposing As Boolean)
		Try
			If disposing AndAlso components IsNot Nothing Then
				components.Dispose()
			End If
		Finally
			MyBase.Dispose(disposing)
		End Try
	End Sub

	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer

	'NOTE: The following procedure is required by the Windows Form Designer
	'It can be modified using the Windows Form Designer.  
	'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()>
	Private Sub InitializeComponent()
		Me.components = New System.ComponentModel.Container()
		Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(MainForm))
		Dim ChartArea7 As System.Windows.Forms.DataVisualization.Charting.ChartArea = New System.Windows.Forms.DataVisualization.Charting.ChartArea()
		Dim Legend25 As System.Windows.Forms.DataVisualization.Charting.Legend = New System.Windows.Forms.DataVisualization.Charting.Legend()
		Dim LegendItem25 As System.Windows.Forms.DataVisualization.Charting.LegendItem = New System.Windows.Forms.DataVisualization.Charting.LegendItem()
		Dim Legend26 As System.Windows.Forms.DataVisualization.Charting.Legend = New System.Windows.Forms.DataVisualization.Charting.Legend()
		Dim LegendItem26 As System.Windows.Forms.DataVisualization.Charting.LegendItem = New System.Windows.Forms.DataVisualization.Charting.LegendItem()
		Dim Legend27 As System.Windows.Forms.DataVisualization.Charting.Legend = New System.Windows.Forms.DataVisualization.Charting.Legend()
		Dim LegendItem27 As System.Windows.Forms.DataVisualization.Charting.LegendItem = New System.Windows.Forms.DataVisualization.Charting.LegendItem()
		Dim Legend28 As System.Windows.Forms.DataVisualization.Charting.Legend = New System.Windows.Forms.DataVisualization.Charting.Legend()
		Dim LegendItem28 As System.Windows.Forms.DataVisualization.Charting.LegendItem = New System.Windows.Forms.DataVisualization.Charting.LegendItem()
		Dim Series13 As System.Windows.Forms.DataVisualization.Charting.Series = New System.Windows.Forms.DataVisualization.Charting.Series()
		Dim DataPoint33 As System.Windows.Forms.DataVisualization.Charting.DataPoint = New System.Windows.Forms.DataVisualization.Charting.DataPoint(1.0R, 30.0R)
		Dim DataPoint34 As System.Windows.Forms.DataVisualization.Charting.DataPoint = New System.Windows.Forms.DataVisualization.Charting.DataPoint(2.0R, 40.0R)
		Dim Series14 As System.Windows.Forms.DataVisualization.Charting.Series = New System.Windows.Forms.DataVisualization.Charting.Series()
		Dim DataPoint35 As System.Windows.Forms.DataVisualization.Charting.DataPoint = New System.Windows.Forms.DataVisualization.Charting.DataPoint(1.0R, 30.0R)
		Dim DataPoint36 As System.Windows.Forms.DataVisualization.Charting.DataPoint = New System.Windows.Forms.DataVisualization.Charting.DataPoint(2.0R, 40.0R)
		Dim Series15 As System.Windows.Forms.DataVisualization.Charting.Series = New System.Windows.Forms.DataVisualization.Charting.Series()
		Dim DataPoint37 As System.Windows.Forms.DataVisualization.Charting.DataPoint = New System.Windows.Forms.DataVisualization.Charting.DataPoint(1.0R, 40.0R)
		Dim DataPoint38 As System.Windows.Forms.DataVisualization.Charting.DataPoint = New System.Windows.Forms.DataVisualization.Charting.DataPoint(2.0R, 20.0R)
		Dim Series16 As System.Windows.Forms.DataVisualization.Charting.Series = New System.Windows.Forms.DataVisualization.Charting.Series()
		Dim DataPoint39 As System.Windows.Forms.DataVisualization.Charting.DataPoint = New System.Windows.Forms.DataVisualization.Charting.DataPoint(1.0R, 5.0R)
		Dim DataPoint40 As System.Windows.Forms.DataVisualization.Charting.DataPoint = New System.Windows.Forms.DataVisualization.Charting.DataPoint(2.0R, 5.0R)
		Dim ChartArea8 As System.Windows.Forms.DataVisualization.Charting.ChartArea = New System.Windows.Forms.DataVisualization.Charting.ChartArea()
		Dim Legend29 As System.Windows.Forms.DataVisualization.Charting.Legend = New System.Windows.Forms.DataVisualization.Charting.Legend()
		Dim LegendItem29 As System.Windows.Forms.DataVisualization.Charting.LegendItem = New System.Windows.Forms.DataVisualization.Charting.LegendItem()
		Dim Legend30 As System.Windows.Forms.DataVisualization.Charting.Legend = New System.Windows.Forms.DataVisualization.Charting.Legend()
		Dim LegendItem30 As System.Windows.Forms.DataVisualization.Charting.LegendItem = New System.Windows.Forms.DataVisualization.Charting.LegendItem()
		Dim Legend31 As System.Windows.Forms.DataVisualization.Charting.Legend = New System.Windows.Forms.DataVisualization.Charting.Legend()
		Dim LegendItem31 As System.Windows.Forms.DataVisualization.Charting.LegendItem = New System.Windows.Forms.DataVisualization.Charting.LegendItem()
		Dim Legend32 As System.Windows.Forms.DataVisualization.Charting.Legend = New System.Windows.Forms.DataVisualization.Charting.Legend()
		Dim LegendItem32 As System.Windows.Forms.DataVisualization.Charting.LegendItem = New System.Windows.Forms.DataVisualization.Charting.LegendItem()
		Dim Series17 As System.Windows.Forms.DataVisualization.Charting.Series = New System.Windows.Forms.DataVisualization.Charting.Series()
		Dim DataPoint41 As System.Windows.Forms.DataVisualization.Charting.DataPoint = New System.Windows.Forms.DataVisualization.Charting.DataPoint(1.0R, 33.0R)
		Dim DataPoint42 As System.Windows.Forms.DataVisualization.Charting.DataPoint = New System.Windows.Forms.DataVisualization.Charting.DataPoint(2.0R, 57.0R)
		Dim DataPoint43 As System.Windows.Forms.DataVisualization.Charting.DataPoint = New System.Windows.Forms.DataVisualization.Charting.DataPoint(3.0R, 10.0R)
		Dim DataPoint44 As System.Windows.Forms.DataVisualization.Charting.DataPoint = New System.Windows.Forms.DataVisualization.Charting.DataPoint(0R, 0R)
		Dim Title5 As System.Windows.Forms.DataVisualization.Charting.Title = New System.Windows.Forms.DataVisualization.Charting.Title()
		Dim ChartArea9 As System.Windows.Forms.DataVisualization.Charting.ChartArea = New System.Windows.Forms.DataVisualization.Charting.ChartArea()
		Dim Legend33 As System.Windows.Forms.DataVisualization.Charting.Legend = New System.Windows.Forms.DataVisualization.Charting.Legend()
		Dim LegendItem33 As System.Windows.Forms.DataVisualization.Charting.LegendItem = New System.Windows.Forms.DataVisualization.Charting.LegendItem()
		Dim Legend34 As System.Windows.Forms.DataVisualization.Charting.Legend = New System.Windows.Forms.DataVisualization.Charting.Legend()
		Dim LegendItem34 As System.Windows.Forms.DataVisualization.Charting.LegendItem = New System.Windows.Forms.DataVisualization.Charting.LegendItem()
		Dim Legend35 As System.Windows.Forms.DataVisualization.Charting.Legend = New System.Windows.Forms.DataVisualization.Charting.Legend()
		Dim LegendItem35 As System.Windows.Forms.DataVisualization.Charting.LegendItem = New System.Windows.Forms.DataVisualization.Charting.LegendItem()
		Dim Legend36 As System.Windows.Forms.DataVisualization.Charting.Legend = New System.Windows.Forms.DataVisualization.Charting.Legend()
		Dim LegendItem36 As System.Windows.Forms.DataVisualization.Charting.LegendItem = New System.Windows.Forms.DataVisualization.Charting.LegendItem()
		Dim Series18 As System.Windows.Forms.DataVisualization.Charting.Series = New System.Windows.Forms.DataVisualization.Charting.Series()
		Dim DataPoint45 As System.Windows.Forms.DataVisualization.Charting.DataPoint = New System.Windows.Forms.DataVisualization.Charting.DataPoint(1.0R, 33.0R)
		Dim DataPoint46 As System.Windows.Forms.DataVisualization.Charting.DataPoint = New System.Windows.Forms.DataVisualization.Charting.DataPoint(2.0R, 57.0R)
		Dim DataPoint47 As System.Windows.Forms.DataVisualization.Charting.DataPoint = New System.Windows.Forms.DataVisualization.Charting.DataPoint(3.0R, 10.0R)
		Dim DataPoint48 As System.Windows.Forms.DataVisualization.Charting.DataPoint = New System.Windows.Forms.DataVisualization.Charting.DataPoint(4.0R, 5.0R)
		Dim Title6 As System.Windows.Forms.DataVisualization.Charting.Title = New System.Windows.Forms.DataVisualization.Charting.Title()
		Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
		Me.FileToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
		Me.NewToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
		Me.OpenToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
		Me.toolStripSeparator = New System.Windows.Forms.ToolStripSeparator()
		Me.SaveToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
		Me.SaveAsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
		Me.EditToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
		Me.TransportToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
		Me.MaterialToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
		Me.MakeProcessToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
		Me.SPMakeProcessToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
		Me.AssemblyMethodToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
		Me.ProductCompareToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
		Me.ProductComparisonToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
		Me.SettiingsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
		Me.DBToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
		Me.CalculateToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
		Me.HelpToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
		Me.CopyrightInfoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
		Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
		Me.ProductName_StatusLabel = New System.Windows.Forms.ToolStripStatusLabel()
		Me.ToolStripStatusLabel1 = New System.Windows.Forms.ToolStripStatusLabel()
		Me.WFETabControl = New System.Windows.Forms.TabControl()
		Me.TabPage1 = New System.Windows.Forms.TabPage()
		Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
		Me.GroupBox1 = New System.Windows.Forms.GroupBox()
		Me.Label84 = New System.Windows.Forms.Label()
		Me.Label83 = New System.Windows.Forms.Label()
		Me.a_Component_ISO_Label = New System.Windows.Forms.Label()
		Me.Label21 = New System.Windows.Forms.Label()
		Me.Label19 = New System.Windows.Forms.Label()
		Me.Label17 = New System.Windows.Forms.Label()
		Me.Label10 = New System.Windows.Forms.Label()
		Me.a_Material_ISO_TextBox = New System.Windows.Forms.TextBox()
		Me.Label14 = New System.Windows.Forms.Label()
		Me.a_Componet_Calculate_Button = New System.Windows.Forms.Button()
		Me.a_Component_WFP_Label = New System.Windows.Forms.Label()
		Me.Label15 = New System.Windows.Forms.Label()
		Me.Label9 = New System.Windows.Forms.Label()
		Me.a_Material_WFP_TextBox = New System.Windows.Forms.TextBox()
		Me.Label13 = New System.Windows.Forms.Label()
		Me.Label8 = New System.Windows.Forms.Label()
		Me.Label7 = New System.Windows.Forms.Label()
		Me.Label6 = New System.Windows.Forms.Label()
		Me.a_Componet_Mass_TextBox = New System.Windows.Forms.TextBox()
		Me.Label5 = New System.Windows.Forms.Label()
		Me.a_Material_Density_TextBox = New System.Windows.Forms.TextBox()
		Me.Label4 = New System.Windows.Forms.Label()
		Me.a_VolumnTextBox = New System.Windows.Forms.TextBox()
		Me.Label3 = New System.Windows.Forms.Label()
		Me.A_ChoiceMaterialButton = New System.Windows.Forms.Button()
		Me.a_Material_TextBox = New System.Windows.Forms.TextBox()
		Me.Label2 = New System.Windows.Forms.Label()
		Me.A_CaptureComponentButton = New System.Windows.Forms.Button()
		Me.a_ComponnetNameTextBox = New System.Windows.Forms.TextBox()
		Me.Label1 = New System.Windows.Forms.Label()
		Me.GroupBox2 = New System.Windows.Forms.GroupBox()
		Me.Label86 = New System.Windows.Forms.Label()
		Me.Label85 = New System.Windows.Forms.Label()
		Me.Label28 = New System.Windows.Forms.Label()
		Me.Label27 = New System.Windows.Forms.Label()
		Me.a_Transport_Sum_ISO_Label = New System.Windows.Forms.Label()
		Me.a_Transport_Sum_WFP_Label = New System.Windows.Forms.Label()
		Me.Label22 = New System.Windows.Forms.Label()
		Me.A_ChoiceTransportButton = New System.Windows.Forms.Button()
		Me.GroupBox3 = New System.Windows.Forms.GroupBox()
		Me.Label88 = New System.Windows.Forms.Label()
		Me.Label87 = New System.Windows.Forms.Label()
		Me.Label32 = New System.Windows.Forms.Label()
		Me.Label31 = New System.Windows.Forms.Label()
		Me.a_Process_Sum_ISO_Label = New System.Windows.Forms.Label()
		Me.a_Process_Sum_WFP_Label = New System.Windows.Forms.Label()
		Me.Label20 = New System.Windows.Forms.Label()
		Me.A_ChoiceMakeProcButton = New System.Windows.Forms.Button()
		Me.GroupBox4 = New System.Windows.Forms.GroupBox()
		Me.a_DataGridView = New System.Windows.Forms.DataGridView()
		Me.a_delete_Button = New System.Windows.Forms.Button()
		Me.Panel1 = New System.Windows.Forms.Panel()
		Me.Label30 = New System.Windows.Forms.Label()
		Me.a_Total_ISO_TextBox = New System.Windows.Forms.TextBox()
		Me.Label29 = New System.Windows.Forms.Label()
		Me.Label26 = New System.Windows.Forms.Label()
		Me.Label12 = New System.Windows.Forms.Label()
		Me.a_TotalWFP_TextBox = New System.Windows.Forms.TextBox()
		Me.Label11 = New System.Windows.Forms.Label()
		Me.A_CompletedButton = New System.Windows.Forms.Button()
		Me.TabPage2 = New System.Windows.Forms.TabPage()
		Me.FlowLayoutPanel2 = New System.Windows.Forms.FlowLayoutPanel()
		Me.GroupBox5 = New System.Windows.Forms.GroupBox()
		Me.B_ChoiceComponent_DataGridView = New System.Windows.Forms.DataGridView()
		Me.Panel3 = New System.Windows.Forms.Panel()
		Me.b_AddComponent_Button = New System.Windows.Forms.Button()
		Me.B_Choice_Component_TextBox = New System.Windows.Forms.TextBox()
		Me.Label23 = New System.Windows.Forms.Label()
		Me.FlowLayoutPanel3 = New System.Windows.Forms.FlowLayoutPanel()
		Me.Label24 = New System.Windows.Forms.Label()
		Me.B_Name_Filter_TextBox = New System.Windows.Forms.TextBox()
		Me.B_CleanFilterButton = New System.Windows.Forms.Button()
		Me.GroupBox6 = New System.Windows.Forms.GroupBox()
		Me.Label90 = New System.Windows.Forms.Label()
		Me.Label89 = New System.Windows.Forms.Label()
		Me.b_post_transport_sum_iso_Label = New System.Windows.Forms.Label()
		Me.Label34 = New System.Windows.Forms.Label()
		Me.Label43 = New System.Windows.Forms.Label()
		Me.b_post_transport_sum_wfp_Label = New System.Windows.Forms.Label()
		Me.Label16 = New System.Windows.Forms.Label()
		Me.B_ChoiceTransportButton = New System.Windows.Forms.Button()
		Me.GroupBox7 = New System.Windows.Forms.GroupBox()
		Me.Label92 = New System.Windows.Forms.Label()
		Me.Label91 = New System.Windows.Forms.Label()
		Me.b_post_process_sum_iso_Label = New System.Windows.Forms.Label()
		Me.Label51 = New System.Windows.Forms.Label()
		Me.Label55 = New System.Windows.Forms.Label()
		Me.b_post_process_sum_wfp_Label = New System.Windows.Forms.Label()
		Me.Label18 = New System.Windows.Forms.Label()
		Me.B_ChoiceMakeProcessButton = New System.Windows.Forms.Button()
		Me.GroupBox8 = New System.Windows.Forms.GroupBox()
		Me.B_Component_Post_Process_DataGridView = New System.Windows.Forms.DataGridView()
		Me.B_Delete_Component_Post_Process_Button = New System.Windows.Forms.Button()
		Me.Panel2 = New System.Windows.Forms.Panel()
		Me.Label59 = New System.Windows.Forms.Label()
		Me.B_Component_Post_Process_ISO_TextBox = New System.Windows.Forms.TextBox()
		Me.Label57 = New System.Windows.Forms.Label()
		Me.Label58 = New System.Windows.Forms.Label()
		Me.Label35 = New System.Windows.Forms.Label()
		Me.B_Component_Post_Process_WFP_TextBox = New System.Windows.Forms.TextBox()
		Me.Label36 = New System.Windows.Forms.Label()
		Me.B_CompletedButton = New System.Windows.Forms.Button()
		Me.TabPage3 = New System.Windows.Forms.TabPage()
		Me.FlowLayoutPanel4 = New System.Windows.Forms.FlowLayoutPanel()
		Me.GroupBox9 = New System.Windows.Forms.GroupBox()
		Me.FlowLayoutPanel6 = New System.Windows.Forms.FlowLayoutPanel()
		Me.Label44 = New System.Windows.Forms.Label()
		Me.C_Component_Name_TextBox = New System.Windows.Forms.TextBox()
		Me.C_CaptureComponentButton = New System.Windows.Forms.Button()
		Me.Label45 = New System.Windows.Forms.Label()
		Me.C_Manufacture_Name_TextBox = New System.Windows.Forms.TextBox()
		Me.C_ChoiceMakeProcessButton = New System.Windows.Forms.Button()
		Me.C_Area_Panel = New System.Windows.Forms.Panel()
		Me.C_AreaTextBox = New System.Windows.Forms.TextBox()
		Me.C_AreaLabel = New System.Windows.Forms.Label()
		Me.C_AreaTitleLabel = New System.Windows.Forms.Label()
		Me.C_Volumn_Panel = New System.Windows.Forms.Panel()
		Me.C_VolumnTextBox = New System.Windows.Forms.TextBox()
		Me.C_VolumnLabel = New System.Windows.Forms.Label()
		Me.C_VolumnTitleLabel = New System.Windows.Forms.Label()
		Me.C_Density_Panel = New System.Windows.Forms.Panel()
		Me.C_Manufacture_Density_TextBox = New System.Windows.Forms.TextBox()
		Me.C_DensityLabel = New System.Windows.Forms.Label()
		Me.C_DensityTitleLabel = New System.Windows.Forms.Label()
		Me.C_Mass_Panel = New System.Windows.Forms.Panel()
		Me.C_MassTextBox = New System.Windows.Forms.TextBox()
		Me.C_MassLabel = New System.Windows.Forms.Label()
		Me.C_MassTitleLabel = New System.Windows.Forms.Label()
		Me.D_ProcessWFE_Panel = New System.Windows.Forms.Panel()
		Me.Label56 = New System.Windows.Forms.Label()
		Me.C_Manufacture_ISO_TextBox = New System.Windows.Forms.TextBox()
		Me.Label33 = New System.Windows.Forms.Label()
		Me.Label48 = New System.Windows.Forms.Label()
		Me.Label39 = New System.Windows.Forms.Label()
		Me.C_Manufacture_WFP_TextBox = New System.Windows.Forms.TextBox()
		Me.Label40 = New System.Windows.Forms.Label()
		Me.Panel5 = New System.Windows.Forms.Panel()
		Me.Label94 = New System.Windows.Forms.Label()
		Me.Label93 = New System.Windows.Forms.Label()
		Me.C_Component_ISO_Label = New System.Windows.Forms.Label()
		Me.Label60 = New System.Windows.Forms.Label()
		Me.Label61 = New System.Windows.Forms.Label()
		Me.c_add_component_Button = New System.Windows.Forms.Button()
		Me.C_Component_WFP_Label = New System.Windows.Forms.Label()
		Me.Label25 = New System.Windows.Forms.Label()
		Me.FlowLayoutPanel5 = New System.Windows.Forms.FlowLayoutPanel()
		Me.EBoardRadioButton = New System.Windows.Forms.RadioButton()
		Me.EComopnentRadioButton = New System.Windows.Forms.RadioButton()
		Me.LCDRadioButton = New System.Windows.Forms.RadioButton()
		Me.GroupBox10 = New System.Windows.Forms.GroupBox()
		Me.c_Special_Components_DataGridView = New System.Windows.Forms.DataGridView()
		Me.C_Delete_Button = New System.Windows.Forms.Button()
		Me.Panel4 = New System.Windows.Forms.Panel()
		Me.Label65 = New System.Windows.Forms.Label()
		Me.C_Total_ISO_TextBox = New System.Windows.Forms.TextBox()
		Me.Label63 = New System.Windows.Forms.Label()
		Me.Label64 = New System.Windows.Forms.Label()
		Me.Label41 = New System.Windows.Forms.Label()
		Me.C_Total_WFP_TextBox = New System.Windows.Forms.TextBox()
		Me.Label42 = New System.Windows.Forms.Label()
		Me.C_CompletedButton = New System.Windows.Forms.Button()
		Me.TabPage7 = New System.Windows.Forms.TabPage()
		Me.product_assembly_tabpage_FlowLayoutPanel = New System.Windows.Forms.FlowLayoutPanel()
		Me.GroupBox13 = New System.Windows.Forms.GroupBox()
		Me.FlowLayoutPanel10 = New System.Windows.Forms.FlowLayoutPanel()
		Me.assemblyDataGridView = New System.Windows.Forms.DataGridView()
		Me.Label71 = New System.Windows.Forms.Label()
		Me.assembly_choiceTextBox = New System.Windows.Forms.TextBox()
		Me.assembly_choice_transport_Button = New System.Windows.Forms.Button()
		Me.add_assembly_Button = New System.Windows.Forms.Button()
		Me.Panel6 = New System.Windows.Forms.Panel()
		Me.Label96 = New System.Windows.Forms.Label()
		Me.Label95 = New System.Windows.Forms.Label()
		Me.assembly_iso_Label = New System.Windows.Forms.Label()
		Me.Label76 = New System.Windows.Forms.Label()
		Me.Label73 = New System.Windows.Forms.Label()
		Me.assembly_wfn_Label = New System.Windows.Forms.Label()
		Me.Label74 = New System.Windows.Forms.Label()
		Me.GroupBox14 = New System.Windows.Forms.GroupBox()
		Me.assembly_TreeView = New System.Windows.Forms.TreeView()
		Me.FlowLayoutPanel11 = New System.Windows.Forms.FlowLayoutPanel()
		Me.enable_merge_CheckBox = New System.Windows.Forms.CheckBox()
		Me.enable_disassembly_CheckBox = New System.Windows.Forms.CheckBox()
		Me.function_ok_Button = New System.Windows.Forms.Button()
		Me.assembly_tree_toggle_Button = New System.Windows.Forms.Button()
		Me.Panel8 = New System.Windows.Forms.Panel()
		Me.Label97 = New System.Windows.Forms.Label()
		Me.Label98 = New System.Windows.Forms.Label()
		Me.asm_stage_iso_Label = New System.Windows.Forms.Label()
		Me.Label100 = New System.Windows.Forms.Label()
		Me.Label101 = New System.Windows.Forms.Label()
		Me.asm_stage_wfn_Label = New System.Windows.Forms.Label()
		Me.Label103 = New System.Windows.Forms.Label()
		Me.GroupBox15 = New System.Windows.Forms.GroupBox()
		Me.assembly_record_DataGridView = New System.Windows.Forms.DataGridView()
		Me.Panel7 = New System.Windows.Forms.Panel()
		Me.Label77 = New System.Windows.Forms.Label()
		Me.assembly_total_iso_TextBox = New System.Windows.Forms.TextBox()
		Me.Label78 = New System.Windows.Forms.Label()
		Me.Label79 = New System.Windows.Forms.Label()
		Me.Label80 = New System.Windows.Forms.Label()
		Me.assembly_total_wfn_TextBox = New System.Windows.Forms.TextBox()
		Me.Label81 = New System.Windows.Forms.Label()
		Me.assembly_stage_finish_Button = New System.Windows.Forms.Button()
		Me.TabPage4 = New System.Windows.Forms.TabPage()
		Me.FlowLayoutPanel7 = New System.Windows.Forms.FlowLayoutPanel()
		Me.Label37 = New System.Windows.Forms.Label()
		Me.GroupBox11 = New System.Windows.Forms.GroupBox()
		Me.TabControl2 = New System.Windows.Forms.TabControl()
		Me.TabPage5 = New System.Windows.Forms.TabPage()
		Me.BarChart = New System.Windows.Forms.DataVisualization.Charting.Chart()
		Me.TabPage6 = New System.Windows.Forms.TabPage()
		Me.ISO_PieChart = New System.Windows.Forms.DataVisualization.Charting.Chart()
		Me.PieChart = New System.Windows.Forms.DataVisualization.Charting.Chart()
		Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
		Me.GroupBox12 = New System.Windows.Forms.GroupBox()
		Me.FlowLayoutPanel8 = New System.Windows.Forms.FlowLayoutPanel()
		Me.FlowLayoutPanel9 = New System.Windows.Forms.FlowLayoutPanel()
		Me.Label62 = New System.Windows.Forms.Label()
		Me.Label66 = New System.Windows.Forms.Label()
		Me.Label38 = New System.Windows.Forms.Label()
		Me.D_GenValTextBox = New System.Windows.Forms.TextBox()
		Me.Label46 = New System.Windows.Forms.Label()
		Me.D_ISO_1_TextBox = New System.Windows.Forms.TextBox()
		Me.Label67 = New System.Windows.Forms.Label()
		Me.Label47 = New System.Windows.Forms.Label()
		Me.D_PostProcValTextBox = New System.Windows.Forms.TextBox()
		Me.Label49 = New System.Windows.Forms.Label()
		Me.D_ISO_2_TextBox = New System.Windows.Forms.TextBox()
		Me.Label68 = New System.Windows.Forms.Label()
		Me.Label50 = New System.Windows.Forms.Label()
		Me.D_SpecialComponentTextBox = New System.Windows.Forms.TextBox()
		Me.Label52 = New System.Windows.Forms.Label()
		Me.D_ISO_3_TextBox = New System.Windows.Forms.TextBox()
		Me.Label69 = New System.Windows.Forms.Label()
		Me.Label72 = New System.Windows.Forms.Label()
		Me.D_WFN_4_TextBox = New System.Windows.Forms.TextBox()
		Me.Label75 = New System.Windows.Forms.Label()
		Me.D_ISO_4_TextBox = New System.Windows.Forms.TextBox()
		Me.Label82 = New System.Windows.Forms.Label()
		Me.Label53 = New System.Windows.Forms.Label()
		Me.D_TotalWFValTextBox = New System.Windows.Forms.TextBox()
		Me.Label54 = New System.Windows.Forms.Label()
		Me.D_ISO_Total_TextBox = New System.Windows.Forms.TextBox()
		Me.Label70 = New System.Windows.Forms.Label()
		Me.D_ChoiceProductButton = New System.Windows.Forms.Button()
		Me.MenuStrip1.SuspendLayout()
		Me.StatusStrip1.SuspendLayout()
		Me.WFETabControl.SuspendLayout()
		Me.TabPage1.SuspendLayout()
		Me.FlowLayoutPanel1.SuspendLayout()
		Me.GroupBox1.SuspendLayout()
		Me.GroupBox2.SuspendLayout()
		Me.GroupBox3.SuspendLayout()
		Me.GroupBox4.SuspendLayout()
		CType(Me.a_DataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.Panel1.SuspendLayout()
		Me.TabPage2.SuspendLayout()
		Me.FlowLayoutPanel2.SuspendLayout()
		Me.GroupBox5.SuspendLayout()
		CType(Me.B_ChoiceComponent_DataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.Panel3.SuspendLayout()
		Me.FlowLayoutPanel3.SuspendLayout()
		Me.GroupBox6.SuspendLayout()
		Me.GroupBox7.SuspendLayout()
		Me.GroupBox8.SuspendLayout()
		CType(Me.B_Component_Post_Process_DataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.Panel2.SuspendLayout()
		Me.TabPage3.SuspendLayout()
		Me.FlowLayoutPanel4.SuspendLayout()
		Me.GroupBox9.SuspendLayout()
		Me.FlowLayoutPanel6.SuspendLayout()
		Me.C_Area_Panel.SuspendLayout()
		Me.C_Volumn_Panel.SuspendLayout()
		Me.C_Density_Panel.SuspendLayout()
		Me.C_Mass_Panel.SuspendLayout()
		Me.D_ProcessWFE_Panel.SuspendLayout()
		Me.Panel5.SuspendLayout()
		Me.FlowLayoutPanel5.SuspendLayout()
		Me.GroupBox10.SuspendLayout()
		CType(Me.c_Special_Components_DataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.Panel4.SuspendLayout()
		Me.TabPage7.SuspendLayout()
		Me.product_assembly_tabpage_FlowLayoutPanel.SuspendLayout()
		Me.GroupBox13.SuspendLayout()
		Me.FlowLayoutPanel10.SuspendLayout()
		CType(Me.assemblyDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.Panel6.SuspendLayout()
		Me.GroupBox14.SuspendLayout()
		Me.FlowLayoutPanel11.SuspendLayout()
		Me.Panel8.SuspendLayout()
		Me.GroupBox15.SuspendLayout()
		CType(Me.assembly_record_DataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.Panel7.SuspendLayout()
		Me.TabPage4.SuspendLayout()
		Me.FlowLayoutPanel7.SuspendLayout()
		Me.GroupBox11.SuspendLayout()
		Me.TabControl2.SuspendLayout()
		Me.TabPage5.SuspendLayout()
		CType(Me.BarChart, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.TabPage6.SuspendLayout()
		CType(Me.ISO_PieChart, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.PieChart, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.GroupBox12.SuspendLayout()
		Me.FlowLayoutPanel8.SuspendLayout()
		Me.FlowLayoutPanel9.SuspendLayout()
		Me.SuspendLayout()
		'
		'MenuStrip1
		'
		Me.MenuStrip1.ImageScalingSize = New System.Drawing.Size(20, 20)
		Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.FileToolStripMenuItem, Me.EditToolStripMenuItem, Me.ProductCompareToolStripMenuItem, Me.SettiingsToolStripMenuItem, Me.HelpToolStripMenuItem})
		Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
		Me.MenuStrip1.Name = "MenuStrip1"
		Me.MenuStrip1.Padding = New System.Windows.Forms.Padding(8, 2, 0, 2)
		Me.MenuStrip1.Size = New System.Drawing.Size(564, 28)
		Me.MenuStrip1.TabIndex = 1
		Me.MenuStrip1.Text = "MenuStrip1"
		'
		'FileToolStripMenuItem
		'
		Me.FileToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.NewToolStripMenuItem, Me.OpenToolStripMenuItem, Me.toolStripSeparator, Me.SaveToolStripMenuItem, Me.SaveAsToolStripMenuItem})
		Me.FileToolStripMenuItem.Image = Global.WinAP.WFE.My.Resources.Resources.product
		Me.FileToolStripMenuItem.Name = "FileToolStripMenuItem"
		Me.FileToolStripMenuItem.Size = New System.Drawing.Size(73, 24)
		Me.FileToolStripMenuItem.Text = "專案"
		'
		'NewToolStripMenuItem
		'
		Me.NewToolStripMenuItem.Image = CType(resources.GetObject("NewToolStripMenuItem.Image"), System.Drawing.Image)
		Me.NewToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta
		Me.NewToolStripMenuItem.Name = "NewToolStripMenuItem"
		Me.NewToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.N), System.Windows.Forms.Keys)
		Me.NewToolStripMenuItem.Size = New System.Drawing.Size(201, 26)
		Me.NewToolStripMenuItem.Text = "新增專案"
		'
		'OpenToolStripMenuItem
		'
		Me.OpenToolStripMenuItem.Image = CType(resources.GetObject("OpenToolStripMenuItem.Image"), System.Drawing.Image)
		Me.OpenToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta
		Me.OpenToolStripMenuItem.Name = "OpenToolStripMenuItem"
		Me.OpenToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.O), System.Windows.Forms.Keys)
		Me.OpenToolStripMenuItem.Size = New System.Drawing.Size(201, 26)
		Me.OpenToolStripMenuItem.Text = "載入專案"
		'
		'toolStripSeparator
		'
		Me.toolStripSeparator.Name = "toolStripSeparator"
		Me.toolStripSeparator.Size = New System.Drawing.Size(198, 6)
		'
		'SaveToolStripMenuItem
		'
		Me.SaveToolStripMenuItem.Image = CType(resources.GetObject("SaveToolStripMenuItem.Image"), System.Drawing.Image)
		Me.SaveToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta
		Me.SaveToolStripMenuItem.Name = "SaveToolStripMenuItem"
		Me.SaveToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.S), System.Windows.Forms.Keys)
		Me.SaveToolStripMenuItem.Size = New System.Drawing.Size(201, 26)
		Me.SaveToolStripMenuItem.Text = "儲存專案"
		'
		'SaveAsToolStripMenuItem
		'
		Me.SaveAsToolStripMenuItem.Image = Global.WinAP.WFE.My.Resources.Resources.saveas_32
		Me.SaveAsToolStripMenuItem.Name = "SaveAsToolStripMenuItem"
		Me.SaveAsToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.A), System.Windows.Forms.Keys)
		Me.SaveAsToolStripMenuItem.Size = New System.Drawing.Size(201, 26)
		Me.SaveAsToolStripMenuItem.Text = "另存專案"
		'
		'EditToolStripMenuItem
		'
		Me.EditToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.TransportToolStripMenuItem, Me.MaterialToolStripMenuItem, Me.MakeProcessToolStripMenuItem, Me.SPMakeProcessToolStripMenuItem, Me.AssemblyMethodToolStripMenuItem})
		Me.EditToolStripMenuItem.Image = Global.WinAP.WFE.My.Resources.Resources.edit
		Me.EditToolStripMenuItem.Name = "EditToolStripMenuItem"
		Me.EditToolStripMenuItem.Size = New System.Drawing.Size(121, 24)
		Me.EditToolStripMenuItem.Text = "資料庫維護"
		'
		'TransportToolStripMenuItem
		'
		Me.TransportToolStripMenuItem.Image = Global.WinAP.WFE.My.Resources.Resources.transport_2
		Me.TransportToolStripMenuItem.Name = "TransportToolStripMenuItem"
		Me.TransportToolStripMenuItem.Size = New System.Drawing.Size(228, 26)
		Me.TransportToolStripMenuItem.Text = "運輸工具"
		'
		'MaterialToolStripMenuItem
		'
		Me.MaterialToolStripMenuItem.Image = Global.WinAP.WFE.My.Resources.Resources.material_16
		Me.MaterialToolStripMenuItem.Name = "MaterialToolStripMenuItem"
		Me.MaterialToolStripMenuItem.Size = New System.Drawing.Size(228, 26)
		Me.MaterialToolStripMenuItem.Text = "一般零件的材料"
		'
		'MakeProcessToolStripMenuItem
		'
		Me.MakeProcessToolStripMenuItem.Image = Global.WinAP.WFE.My.Resources.Resources.make_process
		Me.MakeProcessToolStripMenuItem.Name = "MakeProcessToolStripMenuItem"
		Me.MakeProcessToolStripMenuItem.Size = New System.Drawing.Size(228, 26)
		Me.MakeProcessToolStripMenuItem.Text = "一般零件的加工方式"
		'
		'SPMakeProcessToolStripMenuItem
		'
		Me.SPMakeProcessToolStripMenuItem.Image = Global.WinAP.WFE.My.Resources.Resources.sp_make_process
		Me.SPMakeProcessToolStripMenuItem.Name = "SPMakeProcessToolStripMenuItem"
		Me.SPMakeProcessToolStripMenuItem.Size = New System.Drawing.Size(228, 26)
		Me.SPMakeProcessToolStripMenuItem.Text = "特殊零件的製程"
		'
		'AssemblyMethodToolStripMenuItem
		'
		Me.AssemblyMethodToolStripMenuItem.Image = Global.WinAP.WFE.My.Resources.Resources.assembly_32
		Me.AssemblyMethodToolStripMenuItem.Name = "AssemblyMethodToolStripMenuItem"
		Me.AssemblyMethodToolStripMenuItem.Size = New System.Drawing.Size(228, 26)
		Me.AssemblyMethodToolStripMenuItem.Text = "組裝方式"
		'
		'ProductCompareToolStripMenuItem
		'
		Me.ProductCompareToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ProductComparisonToolStripMenuItem})
		Me.ProductCompareToolStripMenuItem.Image = Global.WinAP.WFE.My.Resources.Resources.product_compare
		Me.ProductCompareToolStripMenuItem.Name = "ProductCompareToolStripMenuItem"
		Me.ProductCompareToolStripMenuItem.Size = New System.Drawing.Size(105, 24)
		Me.ProductCompareToolStripMenuItem.Text = "比較分析"
		Me.ProductCompareToolStripMenuItem.ToolTipText = "產品比較"
		'
		'ProductComparisonToolStripMenuItem
		'
		Me.ProductComparisonToolStripMenuItem.Image = Global.WinAP.WFE.My.Resources.Resources.compare_analysis
		Me.ProductComparisonToolStripMenuItem.Name = "ProductComparisonToolStripMenuItem"
		Me.ProductComparisonToolStripMenuItem.Size = New System.Drawing.Size(181, 26)
		Me.ProductComparisonToolStripMenuItem.Text = "產品比較分析"
		'
		'SettiingsToolStripMenuItem
		'
		Me.SettiingsToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.DBToolStripMenuItem, Me.CalculateToolStripMenuItem})
		Me.SettiingsToolStripMenuItem.Image = Global.WinAP.WFE.My.Resources.Resources.settings
		Me.SettiingsToolStripMenuItem.Name = "SettiingsToolStripMenuItem"
		Me.SettiingsToolStripMenuItem.Size = New System.Drawing.Size(73, 24)
		Me.SettiingsToolStripMenuItem.Text = "設定"
		'
		'DBToolStripMenuItem
		'
		Me.DBToolStripMenuItem.Image = Global.WinAP.WFE.My.Resources.Resources.db_3
		Me.DBToolStripMenuItem.Name = "DBToolStripMenuItem"
		Me.DBToolStripMenuItem.Size = New System.Drawing.Size(180, 26)
		Me.DBToolStripMenuItem.Text = "資料來源"
		'
		'CalculateToolStripMenuItem
		'
		Me.CalculateToolStripMenuItem.Image = Global.WinAP.WFE.My.Resources.Resources.science
		Me.CalculateToolStripMenuItem.Name = "CalculateToolStripMenuItem"
		Me.CalculateToolStripMenuItem.Size = New System.Drawing.Size(180, 26)
		Me.CalculateToolStripMenuItem.Text = "數值計算設定"
		'
		'HelpToolStripMenuItem
		'
		Me.HelpToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.CopyrightInfoToolStripMenuItem})
		Me.HelpToolStripMenuItem.Image = Global.WinAP.WFE.My.Resources.Resources.question_mark_font_bold_48
		Me.HelpToolStripMenuItem.Name = "HelpToolStripMenuItem"
		Me.HelpToolStripMenuItem.Size = New System.Drawing.Size(73, 24)
		Me.HelpToolStripMenuItem.Text = "說明"
		'
		'CopyrightInfoToolStripMenuItem
		'
		Me.CopyrightInfoToolStripMenuItem.Image = Global.WinAP.WFE.My.Resources.Resources.version
		Me.CopyrightInfoToolStripMenuItem.Name = "CopyrightInfoToolStripMenuItem"
		Me.CopyrightInfoToolStripMenuItem.Size = New System.Drawing.Size(148, 26)
		Me.CopyrightInfoToolStripMenuItem.Text = "版本說明"
		'
		'StatusStrip1
		'
		Me.StatusStrip1.ImageScalingSize = New System.Drawing.Size(20, 20)
		Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ProductName_StatusLabel, Me.ToolStripStatusLabel1})
		Me.StatusStrip1.Location = New System.Drawing.Point(0, 1016)
		Me.StatusStrip1.Name = "StatusStrip1"
		Me.StatusStrip1.Padding = New System.Windows.Forms.Padding(1, 0, 19, 0)
		Me.StatusStrip1.ShowItemToolTips = True
		Me.StatusStrip1.Size = New System.Drawing.Size(564, 25)
		Me.StatusStrip1.TabIndex = 2
		Me.StatusStrip1.Text = "StatusStrip1"
		'
		'ProductName_StatusLabel
		'
		Me.ProductName_StatusLabel.AutoToolTip = True
		Me.ProductName_StatusLabel.Image = Global.WinAP.WFE.My.Resources.Resources.product
		Me.ProductName_StatusLabel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.ProductName_StatusLabel.Name = "ProductName_StatusLabel"
		Me.ProductName_StatusLabel.Size = New System.Drawing.Size(485, 20)
		Me.ProductName_StatusLabel.Spring = True
		Me.ProductName_StatusLabel.Text = "N/A"
		Me.ProductName_StatusLabel.ToolTipText = "產品資訊"
		'
		'ToolStripStatusLabel1
		'
		Me.ToolStripStatusLabel1.AutoToolTip = True
		Me.ToolStripStatusLabel1.Font = New System.Drawing.Font("Microsoft JhengHei UI", 9.0!, System.Drawing.FontStyle.Underline)
		Me.ToolStripStatusLabel1.Image = Global.WinAP.WFE.My.Resources.Resources.showLog
		Me.ToolStripStatusLabel1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.ToolStripStatusLabel1.IsLink = True
		Me.ToolStripStatusLabel1.LinkVisited = True
		Me.ToolStripStatusLabel1.Name = "ToolStripStatusLabel1"
		Me.ToolStripStatusLabel1.Size = New System.Drawing.Size(59, 20)
		Me.ToolStripStatusLabel1.Text = "訊息"
		Me.ToolStripStatusLabel1.ToolTipText = "訊息"
		'
		'WFETabControl
		'
		Me.WFETabControl.Controls.Add(Me.TabPage1)
		Me.WFETabControl.Controls.Add(Me.TabPage2)
		Me.WFETabControl.Controls.Add(Me.TabPage3)
		Me.WFETabControl.Controls.Add(Me.TabPage7)
		Me.WFETabControl.Controls.Add(Me.TabPage4)
		Me.WFETabControl.Dock = System.Windows.Forms.DockStyle.Fill
		Me.WFETabControl.ImageList = Me.ImageList1
		Me.WFETabControl.Location = New System.Drawing.Point(0, 28)
		Me.WFETabControl.Margin = New System.Windows.Forms.Padding(4, 3, 4, 4)
		Me.WFETabControl.Name = "WFETabControl"
		Me.WFETabControl.SelectedIndex = 0
		Me.WFETabControl.Size = New System.Drawing.Size(564, 988)
		Me.WFETabControl.TabIndex = 3
		'
		'TabPage1
		'
		Me.TabPage1.Controls.Add(Me.FlowLayoutPanel1)
		Me.TabPage1.ImageIndex = 0
		Me.TabPage1.Location = New System.Drawing.Point(4, 25)
		Me.TabPage1.Margin = New System.Windows.Forms.Padding(4)
		Me.TabPage1.Name = "TabPage1"
		Me.TabPage1.Padding = New System.Windows.Forms.Padding(4)
		Me.TabPage1.Size = New System.Drawing.Size(556, 959)
		Me.TabPage1.TabIndex = 0
		Me.TabPage1.Text = "A-1. 一般零件"
		Me.TabPage1.UseVisualStyleBackColor = True
		'
		'FlowLayoutPanel1
		'
		Me.FlowLayoutPanel1.BackColor = System.Drawing.Color.Transparent
		Me.FlowLayoutPanel1.Controls.Add(Me.GroupBox1)
		Me.FlowLayoutPanel1.Controls.Add(Me.GroupBox2)
		Me.FlowLayoutPanel1.Controls.Add(Me.GroupBox3)
		Me.FlowLayoutPanel1.Controls.Add(Me.GroupBox4)
		Me.FlowLayoutPanel1.Controls.Add(Me.Panel1)
		Me.FlowLayoutPanel1.Controls.Add(Me.A_CompletedButton)
		Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
		Me.FlowLayoutPanel1.Location = New System.Drawing.Point(4, 4)
		Me.FlowLayoutPanel1.Margin = New System.Windows.Forms.Padding(4)
		Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
		Me.FlowLayoutPanel1.Size = New System.Drawing.Size(548, 951)
		Me.FlowLayoutPanel1.TabIndex = 0
		'
		'GroupBox1
		'
		Me.GroupBox1.Controls.Add(Me.Label84)
		Me.GroupBox1.Controls.Add(Me.Label83)
		Me.GroupBox1.Controls.Add(Me.a_Component_ISO_Label)
		Me.GroupBox1.Controls.Add(Me.Label21)
		Me.GroupBox1.Controls.Add(Me.Label19)
		Me.GroupBox1.Controls.Add(Me.Label17)
		Me.GroupBox1.Controls.Add(Me.Label10)
		Me.GroupBox1.Controls.Add(Me.a_Material_ISO_TextBox)
		Me.GroupBox1.Controls.Add(Me.Label14)
		Me.GroupBox1.Controls.Add(Me.a_Componet_Calculate_Button)
		Me.GroupBox1.Controls.Add(Me.a_Component_WFP_Label)
		Me.GroupBox1.Controls.Add(Me.Label15)
		Me.GroupBox1.Controls.Add(Me.Label9)
		Me.GroupBox1.Controls.Add(Me.a_Material_WFP_TextBox)
		Me.GroupBox1.Controls.Add(Me.Label13)
		Me.GroupBox1.Controls.Add(Me.Label8)
		Me.GroupBox1.Controls.Add(Me.Label7)
		Me.GroupBox1.Controls.Add(Me.Label6)
		Me.GroupBox1.Controls.Add(Me.a_Componet_Mass_TextBox)
		Me.GroupBox1.Controls.Add(Me.Label5)
		Me.GroupBox1.Controls.Add(Me.a_Material_Density_TextBox)
		Me.GroupBox1.Controls.Add(Me.Label4)
		Me.GroupBox1.Controls.Add(Me.a_VolumnTextBox)
		Me.GroupBox1.Controls.Add(Me.Label3)
		Me.GroupBox1.Controls.Add(Me.A_ChoiceMaterialButton)
		Me.GroupBox1.Controls.Add(Me.a_Material_TextBox)
		Me.GroupBox1.Controls.Add(Me.Label2)
		Me.GroupBox1.Controls.Add(Me.A_CaptureComponentButton)
		Me.GroupBox1.Controls.Add(Me.a_ComponnetNameTextBox)
		Me.GroupBox1.Controls.Add(Me.Label1)
		Me.GroupBox1.Location = New System.Drawing.Point(4, 4)
		Me.GroupBox1.Margin = New System.Windows.Forms.Padding(4)
		Me.GroupBox1.Name = "GroupBox1"
		Me.GroupBox1.Padding = New System.Windows.Forms.Padding(4)
		Me.GroupBox1.Size = New System.Drawing.Size(532, 329)
		Me.GroupBox1.TabIndex = 0
		Me.GroupBox1.TabStop = False
		Me.GroupBox1.Text = "1.材料生產工廠造成的水足跡"
		'
		'Label84
		'
		Me.Label84.AutoSize = True
		Me.Label84.Font = New System.Drawing.Font("PMingLiU", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
		Me.Label84.Location = New System.Drawing.Point(384, 302)
		Me.Label84.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
		Me.Label84.Name = "Label84"
		Me.Label84.Size = New System.Drawing.Size(32, 15)
		Me.Label84.TabIndex = 33
		Me.Label84.Text = "m^3"
		'
		'Label83
		'
		Me.Label83.AutoSize = True
		Me.Label83.Font = New System.Drawing.Font("PMingLiU", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
		Me.Label83.Location = New System.Drawing.Point(384, 270)
		Me.Label83.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
		Me.Label83.Name = "Label83"
		Me.Label83.Size = New System.Drawing.Size(32, 15)
		Me.Label83.TabIndex = 32
		Me.Label83.Text = "m^3"
		'
		'a_Component_ISO_Label
		'
		Me.a_Component_ISO_Label.AutoSize = True
		Me.a_Component_ISO_Label.BackColor = System.Drawing.Color.Transparent
		Me.a_Component_ISO_Label.Font = New System.Drawing.Font("PMingLiU", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
		Me.a_Component_ISO_Label.ForeColor = System.Drawing.Color.CornflowerBlue
		Me.a_Component_ISO_Label.Location = New System.Drawing.Point(220, 302)
		Me.a_Component_ISO_Label.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
		Me.a_Component_ISO_Label.Name = "a_Component_ISO_Label"
		Me.a_Component_ISO_Label.Size = New System.Drawing.Size(31, 15)
		Me.a_Component_ISO_Label.TabIndex = 31
		Me.a_Component_ISO_Label.Text = "N/A"
		'
		'Label21
		'
		Me.Label21.AutoSize = True
		Me.Label21.Font = New System.Drawing.Font("PMingLiU", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
		Me.Label21.Location = New System.Drawing.Point(131, 302)
		Me.Label21.Name = "Label21"
		Me.Label21.Size = New System.Drawing.Size(78, 15)
		Me.Label21.TabIndex = 30
		Me.Label21.Text = "ISO 14046"
		'
		'Label19
		'
		Me.Label19.AutoSize = True
		Me.Label19.Font = New System.Drawing.Font("PMingLiU", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
		Me.Label19.ForeColor = System.Drawing.Color.Purple
		Me.Label19.Location = New System.Drawing.Point(165, 270)
		Me.Label19.Name = "Label19"
		Me.Label19.Size = New System.Drawing.Size(41, 15)
		Me.Label19.TabIndex = 29
		Me.Label19.Text = "WFN"
		'
		'Label17
		'
		Me.Label17.AutoSize = True
		Me.Label17.Font = New System.Drawing.Font("PMingLiU", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
		Me.Label17.ForeColor = System.Drawing.Color.Purple
		Me.Label17.Location = New System.Drawing.Point(165, 205)
		Me.Label17.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
		Me.Label17.Name = "Label17"
		Me.Label17.Size = New System.Drawing.Size(41, 15)
		Me.Label17.TabIndex = 28
		Me.Label17.Text = "WFN"
		'
		'Label10
		'
		Me.Label10.AutoSize = True
		Me.Label10.Font = New System.Drawing.Font("PMingLiU", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
		Me.Label10.Location = New System.Drawing.Point(433, 235)
		Me.Label10.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
		Me.Label10.Name = "Label10"
		Me.Label10.Size = New System.Drawing.Size(53, 15)
		Me.Label10.TabIndex = 27
		Me.Label10.Text = "m^3/Kg"
		'
		'a_Material_ISO_TextBox
		'
		Me.a_Material_ISO_TextBox.Location = New System.Drawing.Point(223, 232)
		Me.a_Material_ISO_TextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.a_Material_ISO_TextBox.Name = "a_Material_ISO_TextBox"
		Me.a_Material_ISO_TextBox.ReadOnly = True
		Me.a_Material_ISO_TextBox.Size = New System.Drawing.Size(193, 25)
		Me.a_Material_ISO_TextBox.TabIndex = 26
		'
		'Label14
		'
		Me.Label14.AutoSize = True
		Me.Label14.Font = New System.Drawing.Font("PMingLiU", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
		Me.Label14.Location = New System.Drawing.Point(131, 235)
		Me.Label14.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
		Me.Label14.Name = "Label14"
		Me.Label14.Size = New System.Drawing.Size(78, 15)
		Me.Label14.TabIndex = 25
		Me.Label14.Text = "ISO 14046"
		'
		'a_Componet_Calculate_Button
		'
		Me.a_Componet_Calculate_Button.Location = New System.Drawing.Point(425, 292)
		Me.a_Componet_Calculate_Button.Margin = New System.Windows.Forms.Padding(4)
		Me.a_Componet_Calculate_Button.Name = "a_Componet_Calculate_Button"
		Me.a_Componet_Calculate_Button.Size = New System.Drawing.Size(100, 29)
		Me.a_Componet_Calculate_Button.TabIndex = 24
		Me.a_Componet_Calculate_Button.Text = "新增"
		Me.a_Componet_Calculate_Button.UseVisualStyleBackColor = True
		'
		'a_Component_WFP_Label
		'
		Me.a_Component_WFP_Label.AutoSize = True
		Me.a_Component_WFP_Label.BackColor = System.Drawing.Color.Transparent
		Me.a_Component_WFP_Label.Font = New System.Drawing.Font("PMingLiU", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
		Me.a_Component_WFP_Label.ForeColor = System.Drawing.Color.CornflowerBlue
		Me.a_Component_WFP_Label.Location = New System.Drawing.Point(220, 270)
		Me.a_Component_WFP_Label.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
		Me.a_Component_WFP_Label.Name = "a_Component_WFP_Label"
		Me.a_Component_WFP_Label.Size = New System.Drawing.Size(31, 15)
		Me.a_Component_WFP_Label.TabIndex = 23
		Me.a_Component_WFP_Label.Text = "N/A"
		'
		'Label15
		'
		Me.Label15.AutoSize = True
		Me.Label15.Font = New System.Drawing.Font("PMingLiU", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
		Me.Label15.Location = New System.Drawing.Point(8, 270)
		Me.Label15.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
		Me.Label15.Name = "Label15"
		Me.Label15.Size = New System.Drawing.Size(103, 15)
		Me.Label15.TabIndex = 21
		Me.Label15.Text = "本零件水足跡"
		'
		'Label9
		'
		Me.Label9.AutoSize = True
		Me.Label9.Font = New System.Drawing.Font("PMingLiU", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
		Me.Label9.Location = New System.Drawing.Point(433, 202)
		Me.Label9.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
		Me.Label9.Name = "Label9"
		Me.Label9.Size = New System.Drawing.Size(53, 15)
		Me.Label9.TabIndex = 20
		Me.Label9.Text = "m^3/Kg"
		'
		'a_Material_WFP_TextBox
		'
		Me.a_Material_WFP_TextBox.Location = New System.Drawing.Point(223, 199)
		Me.a_Material_WFP_TextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.a_Material_WFP_TextBox.Name = "a_Material_WFP_TextBox"
		Me.a_Material_WFP_TextBox.ReadOnly = True
		Me.a_Material_WFP_TextBox.Size = New System.Drawing.Size(193, 25)
		Me.a_Material_WFP_TextBox.TabIndex = 19
		'
		'Label13
		'
		Me.Label13.AutoSize = True
		Me.Label13.Font = New System.Drawing.Font("PMingLiU", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
		Me.Label13.Location = New System.Drawing.Point(8, 205)
		Me.Label13.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
		Me.Label13.Name = "Label13"
		Me.Label13.Size = New System.Drawing.Size(87, 15)
		Me.Label13.TabIndex = 18
		Me.Label13.Text = "材料水足跡"
		'
		'Label8
		'
		Me.Label8.AutoSize = True
		Me.Label8.Font = New System.Drawing.Font("PMingLiU", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
		Me.Label8.Location = New System.Drawing.Point(433, 168)
		Me.Label8.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
		Me.Label8.Name = "Label8"
		Me.Label8.Size = New System.Drawing.Size(24, 15)
		Me.Label8.TabIndex = 17
		Me.Label8.Text = "Kg"
		'
		'Label7
		'
		Me.Label7.AutoSize = True
		Me.Label7.Font = New System.Drawing.Font("PMingLiU", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
		Me.Label7.Location = New System.Drawing.Point(433, 132)
		Me.Label7.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
		Me.Label7.Name = "Label7"
		Me.Label7.Size = New System.Drawing.Size(61, 15)
		Me.Label7.TabIndex = 16
		Me.Label7.Text = "Kg / m^3"
		'
		'Label6
		'
		Me.Label6.AutoSize = True
		Me.Label6.Font = New System.Drawing.Font("PMingLiU", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
		Me.Label6.Location = New System.Drawing.Point(433, 98)
		Me.Label6.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
		Me.Label6.Name = "Label6"
		Me.Label6.Size = New System.Drawing.Size(32, 15)
		Me.Label6.TabIndex = 15
		Me.Label6.Text = "m^3"
		'
		'a_Componet_Mass_TextBox
		'
		Me.a_Componet_Mass_TextBox.Location = New System.Drawing.Point(65, 164)
		Me.a_Componet_Mass_TextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.a_Componet_Mass_TextBox.Name = "a_Componet_Mass_TextBox"
		Me.a_Componet_Mass_TextBox.ReadOnly = True
		Me.a_Componet_Mass_TextBox.Size = New System.Drawing.Size(352, 25)
		Me.a_Componet_Mass_TextBox.TabIndex = 13
		'
		'Label5
		'
		Me.Label5.AutoSize = True
		Me.Label5.Font = New System.Drawing.Font("PMingLiU", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
		Me.Label5.Location = New System.Drawing.Point(8, 170)
		Me.Label5.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
		Me.Label5.Name = "Label5"
		Me.Label5.Size = New System.Drawing.Size(37, 15)
		Me.Label5.TabIndex = 12
		Me.Label5.Text = "質量"
		'
		'a_Material_Density_TextBox
		'
		Me.a_Material_Density_TextBox.Location = New System.Drawing.Point(65, 129)
		Me.a_Material_Density_TextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.a_Material_Density_TextBox.Name = "a_Material_Density_TextBox"
		Me.a_Material_Density_TextBox.ReadOnly = True
		Me.a_Material_Density_TextBox.Size = New System.Drawing.Size(352, 25)
		Me.a_Material_Density_TextBox.TabIndex = 10
		'
		'Label4
		'
		Me.Label4.AutoSize = True
		Me.Label4.Font = New System.Drawing.Font("PMingLiU", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
		Me.Label4.Location = New System.Drawing.Point(8, 135)
		Me.Label4.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
		Me.Label4.Name = "Label4"
		Me.Label4.Size = New System.Drawing.Size(37, 15)
		Me.Label4.TabIndex = 9
		Me.Label4.Text = "密度"
		'
		'a_VolumnTextBox
		'
		Me.a_VolumnTextBox.Location = New System.Drawing.Point(65, 94)
		Me.a_VolumnTextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.a_VolumnTextBox.Name = "a_VolumnTextBox"
		Me.a_VolumnTextBox.ReadOnly = True
		Me.a_VolumnTextBox.Size = New System.Drawing.Size(352, 25)
		Me.a_VolumnTextBox.TabIndex = 7
		'
		'Label3
		'
		Me.Label3.AutoSize = True
		Me.Label3.Font = New System.Drawing.Font("PMingLiU", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
		Me.Label3.Location = New System.Drawing.Point(8, 100)
		Me.Label3.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
		Me.Label3.Name = "Label3"
		Me.Label3.Size = New System.Drawing.Size(37, 15)
		Me.Label3.TabIndex = 6
		Me.Label3.Text = "體積"
		'
		'A_ChoiceMaterialButton
		'
		Me.A_ChoiceMaterialButton.Location = New System.Drawing.Point(425, 58)
		Me.A_ChoiceMaterialButton.Margin = New System.Windows.Forms.Padding(4)
		Me.A_ChoiceMaterialButton.Name = "A_ChoiceMaterialButton"
		Me.A_ChoiceMaterialButton.Size = New System.Drawing.Size(100, 29)
		Me.A_ChoiceMaterialButton.TabIndex = 5
		Me.A_ChoiceMaterialButton.Text = "選擇材料"
		Me.A_ChoiceMaterialButton.UseVisualStyleBackColor = True
		'
		'a_Material_TextBox
		'
		Me.a_Material_TextBox.Location = New System.Drawing.Point(65, 58)
		Me.a_Material_TextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.a_Material_TextBox.Name = "a_Material_TextBox"
		Me.a_Material_TextBox.ReadOnly = True
		Me.a_Material_TextBox.Size = New System.Drawing.Size(352, 25)
		Me.a_Material_TextBox.TabIndex = 4
		'
		'Label2
		'
		Me.Label2.AutoSize = True
		Me.Label2.Font = New System.Drawing.Font("PMingLiU", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
		Me.Label2.Location = New System.Drawing.Point(8, 64)
		Me.Label2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
		Me.Label2.Name = "Label2"
		Me.Label2.Size = New System.Drawing.Size(37, 15)
		Me.Label2.TabIndex = 3
		Me.Label2.Text = "材料"
		'
		'A_CaptureComponentButton
		'
		Me.A_CaptureComponentButton.Location = New System.Drawing.Point(425, 20)
		Me.A_CaptureComponentButton.Margin = New System.Windows.Forms.Padding(4)
		Me.A_CaptureComponentButton.Name = "A_CaptureComponentButton"
		Me.A_CaptureComponentButton.Size = New System.Drawing.Size(100, 29)
		Me.A_CaptureComponentButton.TabIndex = 2
		Me.A_CaptureComponentButton.Text = "獲取零件"
		Me.A_CaptureComponentButton.UseVisualStyleBackColor = True
		'
		'a_ComponnetNameTextBox
		'
		Me.a_ComponnetNameTextBox.Location = New System.Drawing.Point(65, 22)
		Me.a_ComponnetNameTextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.a_ComponnetNameTextBox.Name = "a_ComponnetNameTextBox"
		Me.a_ComponnetNameTextBox.ReadOnly = True
		Me.a_ComponnetNameTextBox.Size = New System.Drawing.Size(352, 25)
		Me.a_ComponnetNameTextBox.TabIndex = 1
		'
		'Label1
		'
		Me.Label1.AutoSize = True
		Me.Label1.Font = New System.Drawing.Font("PMingLiU", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
		Me.Label1.Location = New System.Drawing.Point(8, 29)
		Me.Label1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
		Me.Label1.Name = "Label1"
		Me.Label1.Size = New System.Drawing.Size(37, 15)
		Me.Label1.TabIndex = 0
		Me.Label1.Text = "零件"
		'
		'GroupBox2
		'
		Me.GroupBox2.Controls.Add(Me.Label86)
		Me.GroupBox2.Controls.Add(Me.Label85)
		Me.GroupBox2.Controls.Add(Me.Label28)
		Me.GroupBox2.Controls.Add(Me.Label27)
		Me.GroupBox2.Controls.Add(Me.a_Transport_Sum_ISO_Label)
		Me.GroupBox2.Controls.Add(Me.a_Transport_Sum_WFP_Label)
		Me.GroupBox2.Controls.Add(Me.Label22)
		Me.GroupBox2.Controls.Add(Me.A_ChoiceTransportButton)
		Me.GroupBox2.Location = New System.Drawing.Point(4, 341)
		Me.GroupBox2.Margin = New System.Windows.Forms.Padding(4)
		Me.GroupBox2.Name = "GroupBox2"
		Me.GroupBox2.Padding = New System.Windows.Forms.Padding(4)
		Me.GroupBox2.Size = New System.Drawing.Size(532, 110)
		Me.GroupBox2.TabIndex = 1
		Me.GroupBox2.TabStop = False
		Me.GroupBox2.Text = "2. 自材料工廠運輸至零件製造工廠造成的水足跡"
		'
		'Label86
		'
		Me.Label86.AutoSize = True
		Me.Label86.Font = New System.Drawing.Font("PMingLiU", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
		Me.Label86.Location = New System.Drawing.Point(384, 91)
		Me.Label86.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
		Me.Label86.Name = "Label86"
		Me.Label86.Size = New System.Drawing.Size(32, 15)
		Me.Label86.TabIndex = 34
		Me.Label86.Text = "m^3"
		'
		'Label85
		'
		Me.Label85.AutoSize = True
		Me.Label85.Font = New System.Drawing.Font("PMingLiU", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
		Me.Label85.Location = New System.Drawing.Point(384, 62)
		Me.Label85.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
		Me.Label85.Name = "Label85"
		Me.Label85.Size = New System.Drawing.Size(32, 15)
		Me.Label85.TabIndex = 33
		Me.Label85.Text = "m^3"
		'
		'Label28
		'
		Me.Label28.AutoSize = True
		Me.Label28.Font = New System.Drawing.Font("PMingLiU", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
		Me.Label28.Location = New System.Drawing.Point(128, 91)
		Me.Label28.Name = "Label28"
		Me.Label28.Size = New System.Drawing.Size(78, 15)
		Me.Label28.TabIndex = 31
		Me.Label28.Text = "ISO 14046"
		'
		'Label27
		'
		Me.Label27.AutoSize = True
		Me.Label27.Font = New System.Drawing.Font("PMingLiU", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
		Me.Label27.ForeColor = System.Drawing.Color.Purple
		Me.Label27.Location = New System.Drawing.Point(165, 62)
		Me.Label27.Name = "Label27"
		Me.Label27.Size = New System.Drawing.Size(41, 15)
		Me.Label27.TabIndex = 30
		Me.Label27.Text = "WFN"
		'
		'a_Transport_Sum_ISO_Label
		'
		Me.a_Transport_Sum_ISO_Label.AutoSize = True
		Me.a_Transport_Sum_ISO_Label.ForeColor = System.Drawing.Color.CornflowerBlue
		Me.a_Transport_Sum_ISO_Label.Location = New System.Drawing.Point(220, 91)
		Me.a_Transport_Sum_ISO_Label.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
		Me.a_Transport_Sum_ISO_Label.Name = "a_Transport_Sum_ISO_Label"
		Me.a_Transport_Sum_ISO_Label.Size = New System.Drawing.Size(31, 15)
		Me.a_Transport_Sum_ISO_Label.TabIndex = 26
		Me.a_Transport_Sum_ISO_Label.Text = "N/A"
		'
		'a_Transport_Sum_WFP_Label
		'
		Me.a_Transport_Sum_WFP_Label.AutoSize = True
		Me.a_Transport_Sum_WFP_Label.ForeColor = System.Drawing.Color.CornflowerBlue
		Me.a_Transport_Sum_WFP_Label.Location = New System.Drawing.Point(220, 62)
		Me.a_Transport_Sum_WFP_Label.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
		Me.a_Transport_Sum_WFP_Label.Name = "a_Transport_Sum_WFP_Label"
		Me.a_Transport_Sum_WFP_Label.Size = New System.Drawing.Size(31, 15)
		Me.a_Transport_Sum_WFP_Label.TabIndex = 23
		Me.a_Transport_Sum_WFP_Label.Text = "N/A"
		'
		'Label22
		'
		Me.Label22.AutoSize = True
		Me.Label22.Location = New System.Drawing.Point(8, 62)
		Me.Label22.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
		Me.Label22.Name = "Label22"
		Me.Label22.Size = New System.Drawing.Size(112, 15)
		Me.Label22.TabIndex = 22
		Me.Label22.Text = "運輸階段水足跡"
		'
		'A_ChoiceTransportButton
		'
		Me.A_ChoiceTransportButton.Location = New System.Drawing.Point(11, 20)
		Me.A_ChoiceTransportButton.Margin = New System.Windows.Forms.Padding(4)
		Me.A_ChoiceTransportButton.Name = "A_ChoiceTransportButton"
		Me.A_ChoiceTransportButton.Size = New System.Drawing.Size(513, 38)
		Me.A_ChoiceTransportButton.TabIndex = 8
		Me.A_ChoiceTransportButton.Text = "選擇運輸工具"
		Me.A_ChoiceTransportButton.UseVisualStyleBackColor = True
		'
		'GroupBox3
		'
		Me.GroupBox3.Controls.Add(Me.Label88)
		Me.GroupBox3.Controls.Add(Me.Label87)
		Me.GroupBox3.Controls.Add(Me.Label32)
		Me.GroupBox3.Controls.Add(Me.Label31)
		Me.GroupBox3.Controls.Add(Me.a_Process_Sum_ISO_Label)
		Me.GroupBox3.Controls.Add(Me.a_Process_Sum_WFP_Label)
		Me.GroupBox3.Controls.Add(Me.Label20)
		Me.GroupBox3.Controls.Add(Me.A_ChoiceMakeProcButton)
		Me.GroupBox3.Location = New System.Drawing.Point(4, 459)
		Me.GroupBox3.Margin = New System.Windows.Forms.Padding(4)
		Me.GroupBox3.Name = "GroupBox3"
		Me.GroupBox3.Padding = New System.Windows.Forms.Padding(4)
		Me.GroupBox3.Size = New System.Drawing.Size(532, 112)
		Me.GroupBox3.TabIndex = 2
		Me.GroupBox3.TabStop = False
		Me.GroupBox3.Text = "3. 零件製造工廠造成的水足跡"
		'
		'Label88
		'
		Me.Label88.AutoSize = True
		Me.Label88.Font = New System.Drawing.Font("PMingLiU", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
		Me.Label88.Location = New System.Drawing.Point(384, 93)
		Me.Label88.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
		Me.Label88.Name = "Label88"
		Me.Label88.Size = New System.Drawing.Size(32, 15)
		Me.Label88.TabIndex = 34
		Me.Label88.Text = "m^3"
		'
		'Label87
		'
		Me.Label87.AutoSize = True
		Me.Label87.Font = New System.Drawing.Font("PMingLiU", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
		Me.Label87.Location = New System.Drawing.Point(384, 62)
		Me.Label87.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
		Me.Label87.Name = "Label87"
		Me.Label87.Size = New System.Drawing.Size(32, 15)
		Me.Label87.TabIndex = 33
		Me.Label87.Text = "m^3"
		'
		'Label32
		'
		Me.Label32.AutoSize = True
		Me.Label32.Font = New System.Drawing.Font("PMingLiU", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
		Me.Label32.Location = New System.Drawing.Point(129, 92)
		Me.Label32.Name = "Label32"
		Me.Label32.Size = New System.Drawing.Size(78, 15)
		Me.Label32.TabIndex = 32
		Me.Label32.Text = "ISO 14046"
		'
		'Label31
		'
		Me.Label31.AutoSize = True
		Me.Label31.Font = New System.Drawing.Font("PMingLiU", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
		Me.Label31.ForeColor = System.Drawing.Color.Purple
		Me.Label31.Location = New System.Drawing.Point(165, 62)
		Me.Label31.Name = "Label31"
		Me.Label31.Size = New System.Drawing.Size(41, 15)
		Me.Label31.TabIndex = 30
		Me.Label31.Text = "WFN"
		'
		'a_Process_Sum_ISO_Label
		'
		Me.a_Process_Sum_ISO_Label.AutoSize = True
		Me.a_Process_Sum_ISO_Label.ForeColor = System.Drawing.Color.CornflowerBlue
		Me.a_Process_Sum_ISO_Label.Location = New System.Drawing.Point(220, 92)
		Me.a_Process_Sum_ISO_Label.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
		Me.a_Process_Sum_ISO_Label.Name = "a_Process_Sum_ISO_Label"
		Me.a_Process_Sum_ISO_Label.Size = New System.Drawing.Size(31, 15)
		Me.a_Process_Sum_ISO_Label.TabIndex = 24
		Me.a_Process_Sum_ISO_Label.Text = "N/A"
		'
		'a_Process_Sum_WFP_Label
		'
		Me.a_Process_Sum_WFP_Label.AutoSize = True
		Me.a_Process_Sum_WFP_Label.ForeColor = System.Drawing.Color.CornflowerBlue
		Me.a_Process_Sum_WFP_Label.Location = New System.Drawing.Point(220, 62)
		Me.a_Process_Sum_WFP_Label.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
		Me.a_Process_Sum_WFP_Label.Name = "a_Process_Sum_WFP_Label"
		Me.a_Process_Sum_WFP_Label.Size = New System.Drawing.Size(31, 15)
		Me.a_Process_Sum_WFP_Label.TabIndex = 23
		Me.a_Process_Sum_WFP_Label.Text = "N/A"
		'
		'Label20
		'
		Me.Label20.AutoSize = True
		Me.Label20.Location = New System.Drawing.Point(8, 62)
		Me.Label20.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
		Me.Label20.Name = "Label20"
		Me.Label20.Size = New System.Drawing.Size(112, 15)
		Me.Label20.TabIndex = 22
		Me.Label20.Text = "加工階段水足跡"
		'
		'A_ChoiceMakeProcButton
		'
		Me.A_ChoiceMakeProcButton.Location = New System.Drawing.Point(11, 20)
		Me.A_ChoiceMakeProcButton.Margin = New System.Windows.Forms.Padding(4)
		Me.A_ChoiceMakeProcButton.Name = "A_ChoiceMakeProcButton"
		Me.A_ChoiceMakeProcButton.Size = New System.Drawing.Size(513, 38)
		Me.A_ChoiceMakeProcButton.TabIndex = 11
		Me.A_ChoiceMakeProcButton.Text = "選擇加工方式"
		Me.A_ChoiceMakeProcButton.UseVisualStyleBackColor = True
		'
		'GroupBox4
		'
		Me.GroupBox4.Controls.Add(Me.a_DataGridView)
		Me.GroupBox4.Controls.Add(Me.a_delete_Button)
		Me.GroupBox4.Location = New System.Drawing.Point(4, 579)
		Me.GroupBox4.Margin = New System.Windows.Forms.Padding(4)
		Me.GroupBox4.Name = "GroupBox4"
		Me.GroupBox4.Padding = New System.Windows.Forms.Padding(4)
		Me.GroupBox4.Size = New System.Drawing.Size(532, 232)
		Me.GroupBox4.TabIndex = 3
		Me.GroupBox4.TabStop = False
		Me.GroupBox4.Text = "已選一般零件(以下水足跡單位為 m^3)"
		'
		'a_DataGridView
		'
		Me.a_DataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
		Me.a_DataGridView.Dock = System.Windows.Forms.DockStyle.Fill
		Me.a_DataGridView.Location = New System.Drawing.Point(4, 22)
		Me.a_DataGridView.Margin = New System.Windows.Forms.Padding(4)
		Me.a_DataGridView.Name = "a_DataGridView"
		Me.a_DataGridView.RowTemplate.Height = 24
		Me.a_DataGridView.Size = New System.Drawing.Size(524, 168)
		Me.a_DataGridView.TabIndex = 1
		'
		'a_delete_Button
		'
		Me.a_delete_Button.Dock = System.Windows.Forms.DockStyle.Bottom
		Me.a_delete_Button.Location = New System.Drawing.Point(4, 190)
		Me.a_delete_Button.Margin = New System.Windows.Forms.Padding(4)
		Me.a_delete_Button.Name = "a_delete_Button"
		Me.a_delete_Button.Size = New System.Drawing.Size(524, 38)
		Me.a_delete_Button.TabIndex = 0
		Me.a_delete_Button.Text = "刪除記錄"
		Me.a_delete_Button.UseVisualStyleBackColor = True
		'
		'Panel1
		'
		Me.Panel1.Controls.Add(Me.Label30)
		Me.Panel1.Controls.Add(Me.a_Total_ISO_TextBox)
		Me.Panel1.Controls.Add(Me.Label29)
		Me.Panel1.Controls.Add(Me.Label26)
		Me.Panel1.Controls.Add(Me.Label12)
		Me.Panel1.Controls.Add(Me.a_TotalWFP_TextBox)
		Me.Panel1.Controls.Add(Me.Label11)
		Me.Panel1.Location = New System.Drawing.Point(4, 819)
		Me.Panel1.Margin = New System.Windows.Forms.Padding(4)
		Me.Panel1.Name = "Panel1"
		Me.Panel1.Size = New System.Drawing.Size(532, 74)
		Me.Panel1.TabIndex = 4
		'
		'Label30
		'
		Me.Label30.AutoSize = True
		Me.Label30.Location = New System.Drawing.Point(389, 44)
		Me.Label30.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
		Me.Label30.Name = "Label30"
		Me.Label30.Size = New System.Drawing.Size(32, 15)
		Me.Label30.TabIndex = 35
		Me.Label30.Text = "m^3"
		'
		'a_Total_ISO_TextBox
		'
		Me.a_Total_ISO_TextBox.Location = New System.Drawing.Point(223, 39)
		Me.a_Total_ISO_TextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.a_Total_ISO_TextBox.Name = "a_Total_ISO_TextBox"
		Me.a_Total_ISO_TextBox.ReadOnly = True
		Me.a_Total_ISO_TextBox.Size = New System.Drawing.Size(149, 25)
		Me.a_Total_ISO_TextBox.TabIndex = 34
		'
		'Label29
		'
		Me.Label29.AutoSize = True
		Me.Label29.Font = New System.Drawing.Font("PMingLiU", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
		Me.Label29.Location = New System.Drawing.Point(128, 44)
		Me.Label29.Name = "Label29"
		Me.Label29.Size = New System.Drawing.Size(78, 15)
		Me.Label29.TabIndex = 33
		Me.Label29.Text = "ISO 14046"
		'
		'Label26
		'
		Me.Label26.AutoSize = True
		Me.Label26.Font = New System.Drawing.Font("PMingLiU", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
		Me.Label26.ForeColor = System.Drawing.Color.Purple
		Me.Label26.Location = New System.Drawing.Point(165, 9)
		Me.Label26.Name = "Label26"
		Me.Label26.Size = New System.Drawing.Size(41, 15)
		Me.Label26.TabIndex = 31
		Me.Label26.Text = "WFN"
		'
		'Label12
		'
		Me.Label12.AutoSize = True
		Me.Label12.Location = New System.Drawing.Point(389, 9)
		Me.Label12.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
		Me.Label12.Name = "Label12"
		Me.Label12.Size = New System.Drawing.Size(32, 15)
		Me.Label12.TabIndex = 2
		Me.Label12.Text = "m^3"
		'
		'a_TotalWFP_TextBox
		'
		Me.a_TotalWFP_TextBox.Location = New System.Drawing.Point(223, 6)
		Me.a_TotalWFP_TextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.a_TotalWFP_TextBox.Name = "a_TotalWFP_TextBox"
		Me.a_TotalWFP_TextBox.ReadOnly = True
		Me.a_TotalWFP_TextBox.Size = New System.Drawing.Size(149, 25)
		Me.a_TotalWFP_TextBox.TabIndex = 1
		'
		'Label11
		'
		Me.Label11.ForeColor = System.Drawing.Color.CornflowerBlue
		Me.Label11.Location = New System.Drawing.Point(8, 0)
		Me.Label11.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
		Me.Label11.Name = "Label11"
		Me.Label11.Size = New System.Drawing.Size(115, 74)
		Me.Label11.TabIndex = 0
		Me.Label11.Text = "所有一般零件水足跡小計"
		Me.Label11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
		'
		'A_CompletedButton
		'
		Me.A_CompletedButton.Location = New System.Drawing.Point(4, 897)
		Me.A_CompletedButton.Margin = New System.Windows.Forms.Padding(4, 0, 4, 4)
		Me.A_CompletedButton.Name = "A_CompletedButton"
		Me.A_CompletedButton.Size = New System.Drawing.Size(535, 40)
		Me.A_CompletedButton.TabIndex = 5
		Me.A_CompletedButton.Text = "完成此階段評估"
		Me.A_CompletedButton.UseVisualStyleBackColor = True
		'
		'TabPage2
		'
		Me.TabPage2.Controls.Add(Me.FlowLayoutPanel2)
		Me.TabPage2.ImageIndex = 1
		Me.TabPage2.Location = New System.Drawing.Point(4, 25)
		Me.TabPage2.Margin = New System.Windows.Forms.Padding(4)
		Me.TabPage2.Name = "TabPage2"
		Me.TabPage2.Padding = New System.Windows.Forms.Padding(4)
		Me.TabPage2.Size = New System.Drawing.Size(556, 959)
		Me.TabPage2.TabIndex = 1
		Me.TabPage2.Text = "A-2. 後製加工"
		Me.TabPage2.UseVisualStyleBackColor = True
		'
		'FlowLayoutPanel2
		'
		Me.FlowLayoutPanel2.Controls.Add(Me.GroupBox5)
		Me.FlowLayoutPanel2.Controls.Add(Me.GroupBox6)
		Me.FlowLayoutPanel2.Controls.Add(Me.GroupBox7)
		Me.FlowLayoutPanel2.Controls.Add(Me.GroupBox8)
		Me.FlowLayoutPanel2.Controls.Add(Me.Panel2)
		Me.FlowLayoutPanel2.Controls.Add(Me.B_CompletedButton)
		Me.FlowLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill
		Me.FlowLayoutPanel2.Location = New System.Drawing.Point(4, 4)
		Me.FlowLayoutPanel2.Margin = New System.Windows.Forms.Padding(4)
		Me.FlowLayoutPanel2.Name = "FlowLayoutPanel2"
		Me.FlowLayoutPanel2.Size = New System.Drawing.Size(548, 951)
		Me.FlowLayoutPanel2.TabIndex = 0
		'
		'GroupBox5
		'
		Me.GroupBox5.Controls.Add(Me.B_ChoiceComponent_DataGridView)
		Me.GroupBox5.Controls.Add(Me.Panel3)
		Me.GroupBox5.Controls.Add(Me.FlowLayoutPanel3)
		Me.GroupBox5.Location = New System.Drawing.Point(4, 4)
		Me.GroupBox5.Margin = New System.Windows.Forms.Padding(4)
		Me.GroupBox5.Name = "GroupBox5"
		Me.GroupBox5.Padding = New System.Windows.Forms.Padding(4)
		Me.GroupBox5.Size = New System.Drawing.Size(532, 270)
		Me.GroupBox5.TabIndex = 0
		Me.GroupBox5.TabStop = False
		Me.GroupBox5.Text = "1. 來自前一階段 A-1 累計的水足跡(以下水足跡單位為 m^3)"
		'
		'B_ChoiceComponent_DataGridView
		'
		Me.B_ChoiceComponent_DataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
		Me.B_ChoiceComponent_DataGridView.Dock = System.Windows.Forms.DockStyle.Fill
		Me.B_ChoiceComponent_DataGridView.Location = New System.Drawing.Point(4, 61)
		Me.B_ChoiceComponent_DataGridView.Margin = New System.Windows.Forms.Padding(4)
		Me.B_ChoiceComponent_DataGridView.Name = "B_ChoiceComponent_DataGridView"
		Me.B_ChoiceComponent_DataGridView.RowTemplate.Height = 24
		Me.B_ChoiceComponent_DataGridView.Size = New System.Drawing.Size(524, 170)
		Me.B_ChoiceComponent_DataGridView.TabIndex = 4
		'
		'Panel3
		'
		Me.Panel3.Controls.Add(Me.b_AddComponent_Button)
		Me.Panel3.Controls.Add(Me.B_Choice_Component_TextBox)
		Me.Panel3.Controls.Add(Me.Label23)
		Me.Panel3.Dock = System.Windows.Forms.DockStyle.Bottom
		Me.Panel3.Location = New System.Drawing.Point(4, 231)
		Me.Panel3.Margin = New System.Windows.Forms.Padding(4)
		Me.Panel3.Name = "Panel3"
		Me.Panel3.Size = New System.Drawing.Size(524, 35)
		Me.Panel3.TabIndex = 3
		'
		'b_AddComponent_Button
		'
		Me.b_AddComponent_Button.Location = New System.Drawing.Point(413, 2)
		Me.b_AddComponent_Button.Margin = New System.Windows.Forms.Padding(4)
		Me.b_AddComponent_Button.Name = "b_AddComponent_Button"
		Me.b_AddComponent_Button.Size = New System.Drawing.Size(100, 29)
		Me.b_AddComponent_Button.TabIndex = 2
		Me.b_AddComponent_Button.Text = "新增"
		Me.b_AddComponent_Button.UseVisualStyleBackColor = True
		'
		'B_Choice_Component_TextBox
		'
		Me.B_Choice_Component_TextBox.Location = New System.Drawing.Point(83, 4)
		Me.B_Choice_Component_TextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.B_Choice_Component_TextBox.Name = "B_Choice_Component_TextBox"
		Me.B_Choice_Component_TextBox.ReadOnly = True
		Me.B_Choice_Component_TextBox.Size = New System.Drawing.Size(321, 25)
		Me.B_Choice_Component_TextBox.TabIndex = 1
		'
		'Label23
		'
		Me.Label23.AutoSize = True
		Me.Label23.Location = New System.Drawing.Point(4, 10)
		Me.Label23.Margin = New System.Windows.Forms.Padding(4, 4, 4, 0)
		Me.Label23.Name = "Label23"
		Me.Label23.Size = New System.Drawing.Size(67, 15)
		Me.Label23.TabIndex = 0
		Me.Label23.Text = "已選零件"
		'
		'FlowLayoutPanel3
		'
		Me.FlowLayoutPanel3.Controls.Add(Me.Label24)
		Me.FlowLayoutPanel3.Controls.Add(Me.B_Name_Filter_TextBox)
		Me.FlowLayoutPanel3.Controls.Add(Me.B_CleanFilterButton)
		Me.FlowLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Top
		Me.FlowLayoutPanel3.Location = New System.Drawing.Point(4, 22)
		Me.FlowLayoutPanel3.Margin = New System.Windows.Forms.Padding(4)
		Me.FlowLayoutPanel3.Name = "FlowLayoutPanel3"
		Me.FlowLayoutPanel3.Size = New System.Drawing.Size(524, 39)
		Me.FlowLayoutPanel3.TabIndex = 2
		'
		'Label24
		'
		Me.Label24.AutoSize = True
		Me.Label24.Location = New System.Drawing.Point(4, 8)
		Me.Label24.Margin = New System.Windows.Forms.Padding(4, 8, 4, 0)
		Me.Label24.Name = "Label24"
		Me.Label24.Size = New System.Drawing.Size(67, 15)
		Me.Label24.TabIndex = 0
		Me.Label24.Text = "零件篩選"
		'
		'B_Name_Filter_TextBox
		'
		Me.B_Name_Filter_TextBox.Location = New System.Drawing.Point(79, 4)
		Me.B_Name_Filter_TextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.B_Name_Filter_TextBox.Name = "B_Name_Filter_TextBox"
		Me.B_Name_Filter_TextBox.Size = New System.Drawing.Size(327, 25)
		Me.B_Name_Filter_TextBox.TabIndex = 1
		'
		'B_CleanFilterButton
		'
		Me.B_CleanFilterButton.Location = New System.Drawing.Point(414, 4)
		Me.B_CleanFilterButton.Margin = New System.Windows.Forms.Padding(4)
		Me.B_CleanFilterButton.Name = "B_CleanFilterButton"
		Me.B_CleanFilterButton.Size = New System.Drawing.Size(100, 28)
		Me.B_CleanFilterButton.TabIndex = 2
		Me.B_CleanFilterButton.Text = "清除"
		Me.B_CleanFilterButton.UseVisualStyleBackColor = True
		'
		'GroupBox6
		'
		Me.GroupBox6.Controls.Add(Me.Label90)
		Me.GroupBox6.Controls.Add(Me.Label89)
		Me.GroupBox6.Controls.Add(Me.b_post_transport_sum_iso_Label)
		Me.GroupBox6.Controls.Add(Me.Label34)
		Me.GroupBox6.Controls.Add(Me.Label43)
		Me.GroupBox6.Controls.Add(Me.b_post_transport_sum_wfp_Label)
		Me.GroupBox6.Controls.Add(Me.Label16)
		Me.GroupBox6.Controls.Add(Me.B_ChoiceTransportButton)
		Me.GroupBox6.Location = New System.Drawing.Point(4, 282)
		Me.GroupBox6.Margin = New System.Windows.Forms.Padding(4)
		Me.GroupBox6.Name = "GroupBox6"
		Me.GroupBox6.Padding = New System.Windows.Forms.Padding(4)
		Me.GroupBox6.Size = New System.Drawing.Size(528, 122)
		Me.GroupBox6.TabIndex = 1
		Me.GroupBox6.TabStop = False
		Me.GroupBox6.Text = "2. 自零件製造工廠運輸至後製加工廠造成的水足跡"
		'
		'Label90
		'
		Me.Label90.AutoSize = True
		Me.Label90.Location = New System.Drawing.Point(415, 94)
		Me.Label90.Name = "Label90"
		Me.Label90.Size = New System.Drawing.Size(32, 15)
		Me.Label90.TabIndex = 36
		Me.Label90.Text = "m^3"
		'
		'Label89
		'
		Me.Label89.AutoSize = True
		Me.Label89.Location = New System.Drawing.Point(415, 65)
		Me.Label89.Name = "Label89"
		Me.Label89.Size = New System.Drawing.Size(32, 15)
		Me.Label89.TabIndex = 35
		Me.Label89.Text = "m^3"
		'
		'b_post_transport_sum_iso_Label
		'
		Me.b_post_transport_sum_iso_Label.AutoSize = True
		Me.b_post_transport_sum_iso_Label.ForeColor = System.Drawing.Color.CornflowerBlue
		Me.b_post_transport_sum_iso_Label.Location = New System.Drawing.Point(237, 94)
		Me.b_post_transport_sum_iso_Label.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
		Me.b_post_transport_sum_iso_Label.Name = "b_post_transport_sum_iso_Label"
		Me.b_post_transport_sum_iso_Label.Size = New System.Drawing.Size(31, 15)
		Me.b_post_transport_sum_iso_Label.TabIndex = 34
		Me.b_post_transport_sum_iso_Label.Text = "N/A"
		'
		'Label34
		'
		Me.Label34.AutoSize = True
		Me.Label34.Font = New System.Drawing.Font("PMingLiU", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
		Me.Label34.Location = New System.Drawing.Point(131, 94)
		Me.Label34.Name = "Label34"
		Me.Label34.Size = New System.Drawing.Size(78, 15)
		Me.Label34.TabIndex = 33
		Me.Label34.Text = "ISO 14046"
		'
		'Label43
		'
		Me.Label43.AutoSize = True
		Me.Label43.Font = New System.Drawing.Font("PMingLiU", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
		Me.Label43.ForeColor = System.Drawing.Color.Purple
		Me.Label43.Location = New System.Drawing.Point(168, 65)
		Me.Label43.Name = "Label43"
		Me.Label43.Size = New System.Drawing.Size(41, 15)
		Me.Label43.TabIndex = 32
		Me.Label43.Text = "WFN"
		'
		'b_post_transport_sum_wfp_Label
		'
		Me.b_post_transport_sum_wfp_Label.AutoSize = True
		Me.b_post_transport_sum_wfp_Label.ForeColor = System.Drawing.Color.CornflowerBlue
		Me.b_post_transport_sum_wfp_Label.Location = New System.Drawing.Point(237, 65)
		Me.b_post_transport_sum_wfp_Label.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
		Me.b_post_transport_sum_wfp_Label.Name = "b_post_transport_sum_wfp_Label"
		Me.b_post_transport_sum_wfp_Label.Size = New System.Drawing.Size(31, 15)
		Me.b_post_transport_sum_wfp_Label.TabIndex = 28
		Me.b_post_transport_sum_wfp_Label.Text = "N/A"
		'
		'Label16
		'
		Me.Label16.AutoSize = True
		Me.Label16.Location = New System.Drawing.Point(8, 62)
		Me.Label16.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
		Me.Label16.Name = "Label16"
		Me.Label16.Size = New System.Drawing.Size(112, 15)
		Me.Label16.TabIndex = 27
		Me.Label16.Text = "運輸階段水足跡"
		'
		'B_ChoiceTransportButton
		'
		Me.B_ChoiceTransportButton.Location = New System.Drawing.Point(11, 20)
		Me.B_ChoiceTransportButton.Margin = New System.Windows.Forms.Padding(4)
		Me.B_ChoiceTransportButton.Name = "B_ChoiceTransportButton"
		Me.B_ChoiceTransportButton.Size = New System.Drawing.Size(507, 38)
		Me.B_ChoiceTransportButton.TabIndex = 26
		Me.B_ChoiceTransportButton.Text = "選擇運輸工具"
		Me.B_ChoiceTransportButton.UseVisualStyleBackColor = True
		'
		'GroupBox7
		'
		Me.GroupBox7.Controls.Add(Me.Label92)
		Me.GroupBox7.Controls.Add(Me.Label91)
		Me.GroupBox7.Controls.Add(Me.b_post_process_sum_iso_Label)
		Me.GroupBox7.Controls.Add(Me.Label51)
		Me.GroupBox7.Controls.Add(Me.Label55)
		Me.GroupBox7.Controls.Add(Me.b_post_process_sum_wfp_Label)
		Me.GroupBox7.Controls.Add(Me.Label18)
		Me.GroupBox7.Controls.Add(Me.B_ChoiceMakeProcessButton)
		Me.GroupBox7.Location = New System.Drawing.Point(4, 412)
		Me.GroupBox7.Margin = New System.Windows.Forms.Padding(4)
		Me.GroupBox7.Name = "GroupBox7"
		Me.GroupBox7.Padding = New System.Windows.Forms.Padding(4)
		Me.GroupBox7.Size = New System.Drawing.Size(528, 116)
		Me.GroupBox7.TabIndex = 2
		Me.GroupBox7.TabStop = False
		Me.GroupBox7.Text = "3. 後製加工工廠造成的水足跡"
		'
		'Label92
		'
		Me.Label92.AutoSize = True
		Me.Label92.Location = New System.Drawing.Point(415, 92)
		Me.Label92.Name = "Label92"
		Me.Label92.Size = New System.Drawing.Size(32, 15)
		Me.Label92.TabIndex = 38
		Me.Label92.Text = "m^3"
		'
		'Label91
		'
		Me.Label91.AutoSize = True
		Me.Label91.Location = New System.Drawing.Point(414, 62)
		Me.Label91.Name = "Label91"
		Me.Label91.Size = New System.Drawing.Size(32, 15)
		Me.Label91.TabIndex = 37
		Me.Label91.Text = "m^3"
		'
		'b_post_process_sum_iso_Label
		'
		Me.b_post_process_sum_iso_Label.AutoSize = True
		Me.b_post_process_sum_iso_Label.ForeColor = System.Drawing.Color.CornflowerBlue
		Me.b_post_process_sum_iso_Label.Location = New System.Drawing.Point(237, 92)
		Me.b_post_process_sum_iso_Label.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
		Me.b_post_process_sum_iso_Label.Name = "b_post_process_sum_iso_Label"
		Me.b_post_process_sum_iso_Label.Size = New System.Drawing.Size(31, 15)
		Me.b_post_process_sum_iso_Label.TabIndex = 36
		Me.b_post_process_sum_iso_Label.Text = "N/A"
		'
		'Label51
		'
		Me.Label51.AutoSize = True
		Me.Label51.Font = New System.Drawing.Font("PMingLiU", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
		Me.Label51.Location = New System.Drawing.Point(131, 92)
		Me.Label51.Name = "Label51"
		Me.Label51.Size = New System.Drawing.Size(78, 15)
		Me.Label51.TabIndex = 35
		Me.Label51.Text = "ISO 14046"
		'
		'Label55
		'
		Me.Label55.AutoSize = True
		Me.Label55.Font = New System.Drawing.Font("PMingLiU", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
		Me.Label55.ForeColor = System.Drawing.Color.Purple
		Me.Label55.Location = New System.Drawing.Point(168, 62)
		Me.Label55.Name = "Label55"
		Me.Label55.Size = New System.Drawing.Size(41, 15)
		Me.Label55.TabIndex = 34
		Me.Label55.Text = "WFN"
		'
		'b_post_process_sum_wfp_Label
		'
		Me.b_post_process_sum_wfp_Label.AutoSize = True
		Me.b_post_process_sum_wfp_Label.ForeColor = System.Drawing.Color.CornflowerBlue
		Me.b_post_process_sum_wfp_Label.Location = New System.Drawing.Point(237, 62)
		Me.b_post_process_sum_wfp_Label.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
		Me.b_post_process_sum_wfp_Label.Name = "b_post_process_sum_wfp_Label"
		Me.b_post_process_sum_wfp_Label.Size = New System.Drawing.Size(31, 15)
		Me.b_post_process_sum_wfp_Label.TabIndex = 28
		Me.b_post_process_sum_wfp_Label.Text = "N/A"
		'
		'Label18
		'
		Me.Label18.AutoSize = True
		Me.Label18.Location = New System.Drawing.Point(8, 62)
		Me.Label18.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
		Me.Label18.Name = "Label18"
		Me.Label18.Size = New System.Drawing.Size(112, 15)
		Me.Label18.TabIndex = 27
		Me.Label18.Text = "加工階段水足跡"
		'
		'B_ChoiceMakeProcessButton
		'
		Me.B_ChoiceMakeProcessButton.Location = New System.Drawing.Point(11, 20)
		Me.B_ChoiceMakeProcessButton.Margin = New System.Windows.Forms.Padding(4)
		Me.B_ChoiceMakeProcessButton.Name = "B_ChoiceMakeProcessButton"
		Me.B_ChoiceMakeProcessButton.Size = New System.Drawing.Size(507, 38)
		Me.B_ChoiceMakeProcessButton.TabIndex = 26
		Me.B_ChoiceMakeProcessButton.Text = "選擇加工方式"
		Me.B_ChoiceMakeProcessButton.UseVisualStyleBackColor = True
		'
		'GroupBox8
		'
		Me.GroupBox8.Controls.Add(Me.B_Component_Post_Process_DataGridView)
		Me.GroupBox8.Controls.Add(Me.B_Delete_Component_Post_Process_Button)
		Me.GroupBox8.Location = New System.Drawing.Point(4, 536)
		Me.GroupBox8.Margin = New System.Windows.Forms.Padding(4)
		Me.GroupBox8.Name = "GroupBox8"
		Me.GroupBox8.Padding = New System.Windows.Forms.Padding(4)
		Me.GroupBox8.Size = New System.Drawing.Size(528, 274)
		Me.GroupBox8.TabIndex = 3
		Me.GroupBox8.TabStop = False
		Me.GroupBox8.Text = "後製加工記錄(以下水足跡單位為 m^3)"
		'
		'B_Component_Post_Process_DataGridView
		'
		Me.B_Component_Post_Process_DataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
		Me.B_Component_Post_Process_DataGridView.Dock = System.Windows.Forms.DockStyle.Fill
		Me.B_Component_Post_Process_DataGridView.Location = New System.Drawing.Point(4, 22)
		Me.B_Component_Post_Process_DataGridView.Margin = New System.Windows.Forms.Padding(4)
		Me.B_Component_Post_Process_DataGridView.Name = "B_Component_Post_Process_DataGridView"
		Me.B_Component_Post_Process_DataGridView.RowTemplate.Height = 24
		Me.B_Component_Post_Process_DataGridView.Size = New System.Drawing.Size(520, 210)
		Me.B_Component_Post_Process_DataGridView.TabIndex = 1
		'
		'B_Delete_Component_Post_Process_Button
		'
		Me.B_Delete_Component_Post_Process_Button.Dock = System.Windows.Forms.DockStyle.Bottom
		Me.B_Delete_Component_Post_Process_Button.Location = New System.Drawing.Point(4, 232)
		Me.B_Delete_Component_Post_Process_Button.Margin = New System.Windows.Forms.Padding(4)
		Me.B_Delete_Component_Post_Process_Button.Name = "B_Delete_Component_Post_Process_Button"
		Me.B_Delete_Component_Post_Process_Button.Size = New System.Drawing.Size(520, 38)
		Me.B_Delete_Component_Post_Process_Button.TabIndex = 0
		Me.B_Delete_Component_Post_Process_Button.Text = "刪除記錄"
		Me.B_Delete_Component_Post_Process_Button.UseVisualStyleBackColor = True
		'
		'Panel2
		'
		Me.Panel2.Controls.Add(Me.Label59)
		Me.Panel2.Controls.Add(Me.B_Component_Post_Process_ISO_TextBox)
		Me.Panel2.Controls.Add(Me.Label57)
		Me.Panel2.Controls.Add(Me.Label58)
		Me.Panel2.Controls.Add(Me.Label35)
		Me.Panel2.Controls.Add(Me.B_Component_Post_Process_WFP_TextBox)
		Me.Panel2.Controls.Add(Me.Label36)
		Me.Panel2.Location = New System.Drawing.Point(4, 818)
		Me.Panel2.Margin = New System.Windows.Forms.Padding(4)
		Me.Panel2.Name = "Panel2"
		Me.Panel2.Size = New System.Drawing.Size(528, 75)
		Me.Panel2.TabIndex = 4
		'
		'Label59
		'
		Me.Label59.AutoSize = True
		Me.Label59.Location = New System.Drawing.Point(388, 44)
		Me.Label59.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
		Me.Label59.Name = "Label59"
		Me.Label59.Size = New System.Drawing.Size(32, 15)
		Me.Label59.TabIndex = 39
		Me.Label59.Text = "m^3"
		'
		'B_Component_Post_Process_ISO_TextBox
		'
		Me.B_Component_Post_Process_ISO_TextBox.Location = New System.Drawing.Point(223, 39)
		Me.B_Component_Post_Process_ISO_TextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.B_Component_Post_Process_ISO_TextBox.Name = "B_Component_Post_Process_ISO_TextBox"
		Me.B_Component_Post_Process_ISO_TextBox.ReadOnly = True
		Me.B_Component_Post_Process_ISO_TextBox.Size = New System.Drawing.Size(157, 25)
		Me.B_Component_Post_Process_ISO_TextBox.TabIndex = 38
		'
		'Label57
		'
		Me.Label57.AutoSize = True
		Me.Label57.Font = New System.Drawing.Font("PMingLiU", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
		Me.Label57.Location = New System.Drawing.Point(128, 44)
		Me.Label57.Name = "Label57"
		Me.Label57.Size = New System.Drawing.Size(78, 15)
		Me.Label57.TabIndex = 37
		Me.Label57.Text = "ISO 14046"
		'
		'Label58
		'
		Me.Label58.AutoSize = True
		Me.Label58.Font = New System.Drawing.Font("PMingLiU", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
		Me.Label58.ForeColor = System.Drawing.Color.Purple
		Me.Label58.Location = New System.Drawing.Point(165, 9)
		Me.Label58.Name = "Label58"
		Me.Label58.Size = New System.Drawing.Size(41, 15)
		Me.Label58.TabIndex = 36
		Me.Label58.Text = "WFN"
		'
		'Label35
		'
		Me.Label35.AutoSize = True
		Me.Label35.Location = New System.Drawing.Point(388, 9)
		Me.Label35.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
		Me.Label35.Name = "Label35"
		Me.Label35.Size = New System.Drawing.Size(32, 15)
		Me.Label35.TabIndex = 6
		Me.Label35.Text = "m^3"
		'
		'B_Component_Post_Process_WFP_TextBox
		'
		Me.B_Component_Post_Process_WFP_TextBox.Location = New System.Drawing.Point(223, 6)
		Me.B_Component_Post_Process_WFP_TextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.B_Component_Post_Process_WFP_TextBox.Name = "B_Component_Post_Process_WFP_TextBox"
		Me.B_Component_Post_Process_WFP_TextBox.ReadOnly = True
		Me.B_Component_Post_Process_WFP_TextBox.Size = New System.Drawing.Size(157, 25)
		Me.B_Component_Post_Process_WFP_TextBox.TabIndex = 5
		'
		'Label36
		'
		Me.Label36.ForeColor = System.Drawing.Color.CornflowerBlue
		Me.Label36.Location = New System.Drawing.Point(8, 0)
		Me.Label36.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
		Me.Label36.Name = "Label36"
		Me.Label36.Size = New System.Drawing.Size(115, 75)
		Me.Label36.TabIndex = 4
		Me.Label36.Text = "所有後製加工水足跡小計"
		Me.Label36.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
		'
		'B_CompletedButton
		'
		Me.B_CompletedButton.Location = New System.Drawing.Point(4, 897)
		Me.B_CompletedButton.Margin = New System.Windows.Forms.Padding(4, 0, 4, 4)
		Me.B_CompletedButton.Name = "B_CompletedButton"
		Me.B_CompletedButton.Size = New System.Drawing.Size(535, 40)
		Me.B_CompletedButton.TabIndex = 8
		Me.B_CompletedButton.Text = "完成此階段評估"
		Me.B_CompletedButton.UseVisualStyleBackColor = True
		'
		'TabPage3
		'
		Me.TabPage3.Controls.Add(Me.FlowLayoutPanel4)
		Me.TabPage3.ImageIndex = 2
		Me.TabPage3.Location = New System.Drawing.Point(4, 25)
		Me.TabPage3.Margin = New System.Windows.Forms.Padding(4)
		Me.TabPage3.Name = "TabPage3"
		Me.TabPage3.Size = New System.Drawing.Size(556, 959)
		Me.TabPage3.TabIndex = 2
		Me.TabPage3.Text = "B. 特殊零件"
		Me.TabPage3.UseVisualStyleBackColor = True
		'
		'FlowLayoutPanel4
		'
		Me.FlowLayoutPanel4.Controls.Add(Me.GroupBox9)
		Me.FlowLayoutPanel4.Controls.Add(Me.GroupBox10)
		Me.FlowLayoutPanel4.Controls.Add(Me.Panel4)
		Me.FlowLayoutPanel4.Controls.Add(Me.C_CompletedButton)
		Me.FlowLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill
		Me.FlowLayoutPanel4.Location = New System.Drawing.Point(0, 0)
		Me.FlowLayoutPanel4.Margin = New System.Windows.Forms.Padding(4)
		Me.FlowLayoutPanel4.Name = "FlowLayoutPanel4"
		Me.FlowLayoutPanel4.Size = New System.Drawing.Size(556, 959)
		Me.FlowLayoutPanel4.TabIndex = 0
		'
		'GroupBox9
		'
		Me.GroupBox9.Controls.Add(Me.FlowLayoutPanel6)
		Me.GroupBox9.Controls.Add(Me.FlowLayoutPanel5)
		Me.GroupBox9.Location = New System.Drawing.Point(4, 4)
		Me.GroupBox9.Margin = New System.Windows.Forms.Padding(4)
		Me.GroupBox9.Name = "GroupBox9"
		Me.GroupBox9.Padding = New System.Windows.Forms.Padding(4)
		Me.GroupBox9.Size = New System.Drawing.Size(536, 492)
		Me.GroupBox9.TabIndex = 0
		Me.GroupBox9.TabStop = False
		Me.GroupBox9.Text = "零件類型"
		'
		'FlowLayoutPanel6
		'
		Me.FlowLayoutPanel6.Controls.Add(Me.Label44)
		Me.FlowLayoutPanel6.Controls.Add(Me.C_Component_Name_TextBox)
		Me.FlowLayoutPanel6.Controls.Add(Me.C_CaptureComponentButton)
		Me.FlowLayoutPanel6.Controls.Add(Me.Label45)
		Me.FlowLayoutPanel6.Controls.Add(Me.C_Manufacture_Name_TextBox)
		Me.FlowLayoutPanel6.Controls.Add(Me.C_ChoiceMakeProcessButton)
		Me.FlowLayoutPanel6.Controls.Add(Me.C_Area_Panel)
		Me.FlowLayoutPanel6.Controls.Add(Me.C_Volumn_Panel)
		Me.FlowLayoutPanel6.Controls.Add(Me.C_Density_Panel)
		Me.FlowLayoutPanel6.Controls.Add(Me.C_Mass_Panel)
		Me.FlowLayoutPanel6.Controls.Add(Me.D_ProcessWFE_Panel)
		Me.FlowLayoutPanel6.Controls.Add(Me.Panel5)
		Me.FlowLayoutPanel6.Dock = System.Windows.Forms.DockStyle.Fill
		Me.FlowLayoutPanel6.Location = New System.Drawing.Point(4, 53)
		Me.FlowLayoutPanel6.Margin = New System.Windows.Forms.Padding(4)
		Me.FlowLayoutPanel6.Name = "FlowLayoutPanel6"
		Me.FlowLayoutPanel6.Size = New System.Drawing.Size(528, 435)
		Me.FlowLayoutPanel6.TabIndex = 1
		'
		'Label44
		'
		Me.Label44.AutoSize = True
		Me.Label44.Font = New System.Drawing.Font("PMingLiU", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
		Me.Label44.Location = New System.Drawing.Point(4, 10)
		Me.Label44.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label44.Name = "Label44"
		Me.Label44.Size = New System.Drawing.Size(37, 15)
		Me.Label44.TabIndex = 9
		Me.Label44.Text = "零件"
		'
		'C_Component_Name_TextBox
		'
		Me.C_Component_Name_TextBox.Location = New System.Drawing.Point(49, 4)
		Me.C_Component_Name_TextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.C_Component_Name_TextBox.Name = "C_Component_Name_TextBox"
		Me.C_Component_Name_TextBox.ReadOnly = True
		Me.C_Component_Name_TextBox.Size = New System.Drawing.Size(351, 25)
		Me.C_Component_Name_TextBox.TabIndex = 10
		'
		'C_CaptureComponentButton
		'
		Me.C_CaptureComponentButton.Location = New System.Drawing.Point(408, 4)
		Me.C_CaptureComponentButton.Margin = New System.Windows.Forms.Padding(4)
		Me.C_CaptureComponentButton.Name = "C_CaptureComponentButton"
		Me.C_CaptureComponentButton.Size = New System.Drawing.Size(100, 29)
		Me.C_CaptureComponentButton.TabIndex = 11
		Me.C_CaptureComponentButton.Text = "獲取零件"
		Me.C_CaptureComponentButton.UseVisualStyleBackColor = True
		'
		'Label45
		'
		Me.Label45.AutoSize = True
		Me.Label45.Font = New System.Drawing.Font("PMingLiU", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
		Me.Label45.Location = New System.Drawing.Point(4, 47)
		Me.Label45.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label45.Name = "Label45"
		Me.Label45.Size = New System.Drawing.Size(37, 15)
		Me.Label45.TabIndex = 12
		Me.Label45.Text = "製程"
		'
		'C_Manufacture_Name_TextBox
		'
		Me.C_Manufacture_Name_TextBox.Location = New System.Drawing.Point(49, 41)
		Me.C_Manufacture_Name_TextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.C_Manufacture_Name_TextBox.Name = "C_Manufacture_Name_TextBox"
		Me.C_Manufacture_Name_TextBox.ReadOnly = True
		Me.C_Manufacture_Name_TextBox.Size = New System.Drawing.Size(351, 25)
		Me.C_Manufacture_Name_TextBox.TabIndex = 13
		'
		'C_ChoiceMakeProcessButton
		'
		Me.C_ChoiceMakeProcessButton.Location = New System.Drawing.Point(408, 41)
		Me.C_ChoiceMakeProcessButton.Margin = New System.Windows.Forms.Padding(4)
		Me.C_ChoiceMakeProcessButton.Name = "C_ChoiceMakeProcessButton"
		Me.C_ChoiceMakeProcessButton.Size = New System.Drawing.Size(100, 29)
		Me.C_ChoiceMakeProcessButton.TabIndex = 18
		Me.C_ChoiceMakeProcessButton.Text = "選擇製程"
		Me.C_ChoiceMakeProcessButton.UseVisualStyleBackColor = True
		'
		'C_Area_Panel
		'
		Me.C_Area_Panel.Controls.Add(Me.C_AreaTextBox)
		Me.C_Area_Panel.Controls.Add(Me.C_AreaLabel)
		Me.C_Area_Panel.Controls.Add(Me.C_AreaTitleLabel)
		Me.C_Area_Panel.Location = New System.Drawing.Point(4, 78)
		Me.C_Area_Panel.Margin = New System.Windows.Forms.Padding(4)
		Me.C_Area_Panel.Name = "C_Area_Panel"
		Me.C_Area_Panel.Size = New System.Drawing.Size(520, 38)
		Me.C_Area_Panel.TabIndex = 31
		'
		'C_AreaTextBox
		'
		Me.C_AreaTextBox.Location = New System.Drawing.Point(45, 4)
		Me.C_AreaTextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.C_AreaTextBox.Name = "C_AreaTextBox"
		Me.C_AreaTextBox.ReadOnly = True
		Me.C_AreaTextBox.Size = New System.Drawing.Size(351, 25)
		Me.C_AreaTextBox.TabIndex = 16
		'
		'C_AreaLabel
		'
		Me.C_AreaLabel.Location = New System.Drawing.Point(419, 10)
		Me.C_AreaLabel.Margin = New System.Windows.Forms.Padding(7, 10, 4, 0)
		Me.C_AreaLabel.Name = "C_AreaLabel"
		Me.C_AreaLabel.Size = New System.Drawing.Size(84, 15)
		Me.C_AreaLabel.TabIndex = 17
		Me.C_AreaLabel.Text = "mm^2"
		'
		'C_AreaTitleLabel
		'
		Me.C_AreaTitleLabel.AutoSize = True
		Me.C_AreaTitleLabel.Font = New System.Drawing.Font("PMingLiU", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
		Me.C_AreaTitleLabel.Location = New System.Drawing.Point(0, 10)
		Me.C_AreaTitleLabel.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.C_AreaTitleLabel.Name = "C_AreaTitleLabel"
		Me.C_AreaTitleLabel.Size = New System.Drawing.Size(37, 15)
		Me.C_AreaTitleLabel.TabIndex = 15
		Me.C_AreaTitleLabel.Text = "面積"
		'
		'C_Volumn_Panel
		'
		Me.C_Volumn_Panel.Controls.Add(Me.C_VolumnTextBox)
		Me.C_Volumn_Panel.Controls.Add(Me.C_VolumnLabel)
		Me.C_Volumn_Panel.Controls.Add(Me.C_VolumnTitleLabel)
		Me.C_Volumn_Panel.Location = New System.Drawing.Point(4, 124)
		Me.C_Volumn_Panel.Margin = New System.Windows.Forms.Padding(4)
		Me.C_Volumn_Panel.Name = "C_Volumn_Panel"
		Me.C_Volumn_Panel.Size = New System.Drawing.Size(520, 38)
		Me.C_Volumn_Panel.TabIndex = 32
		'
		'C_VolumnTextBox
		'
		Me.C_VolumnTextBox.Location = New System.Drawing.Point(47, 4)
		Me.C_VolumnTextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.C_VolumnTextBox.Name = "C_VolumnTextBox"
		Me.C_VolumnTextBox.ReadOnly = True
		Me.C_VolumnTextBox.Size = New System.Drawing.Size(348, 25)
		Me.C_VolumnTextBox.TabIndex = 20
		'
		'C_VolumnLabel
		'
		Me.C_VolumnLabel.Location = New System.Drawing.Point(419, 8)
		Me.C_VolumnLabel.Margin = New System.Windows.Forms.Padding(7, 10, 4, 0)
		Me.C_VolumnLabel.Name = "C_VolumnLabel"
		Me.C_VolumnLabel.Size = New System.Drawing.Size(84, 15)
		Me.C_VolumnLabel.TabIndex = 21
		Me.C_VolumnLabel.Text = "mm^3"
		'
		'C_VolumnTitleLabel
		'
		Me.C_VolumnTitleLabel.AutoSize = True
		Me.C_VolumnTitleLabel.Font = New System.Drawing.Font("PMingLiU", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
		Me.C_VolumnTitleLabel.Location = New System.Drawing.Point(0, 10)
		Me.C_VolumnTitleLabel.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.C_VolumnTitleLabel.Name = "C_VolumnTitleLabel"
		Me.C_VolumnTitleLabel.Size = New System.Drawing.Size(37, 15)
		Me.C_VolumnTitleLabel.TabIndex = 19
		Me.C_VolumnTitleLabel.Text = "體積"
		'
		'C_Density_Panel
		'
		Me.C_Density_Panel.Controls.Add(Me.C_Manufacture_Density_TextBox)
		Me.C_Density_Panel.Controls.Add(Me.C_DensityLabel)
		Me.C_Density_Panel.Controls.Add(Me.C_DensityTitleLabel)
		Me.C_Density_Panel.Location = New System.Drawing.Point(4, 170)
		Me.C_Density_Panel.Margin = New System.Windows.Forms.Padding(4)
		Me.C_Density_Panel.Name = "C_Density_Panel"
		Me.C_Density_Panel.Size = New System.Drawing.Size(520, 38)
		Me.C_Density_Panel.TabIndex = 33
		'
		'C_Manufacture_Density_TextBox
		'
		Me.C_Manufacture_Density_TextBox.Location = New System.Drawing.Point(47, 4)
		Me.C_Manufacture_Density_TextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.C_Manufacture_Density_TextBox.Name = "C_Manufacture_Density_TextBox"
		Me.C_Manufacture_Density_TextBox.ReadOnly = True
		Me.C_Manufacture_Density_TextBox.Size = New System.Drawing.Size(348, 25)
		Me.C_Manufacture_Density_TextBox.TabIndex = 23
		'
		'C_DensityLabel
		'
		Me.C_DensityLabel.Location = New System.Drawing.Point(419, 10)
		Me.C_DensityLabel.Margin = New System.Windows.Forms.Padding(7, 10, 4, 0)
		Me.C_DensityLabel.Name = "C_DensityLabel"
		Me.C_DensityLabel.Size = New System.Drawing.Size(84, 15)
		Me.C_DensityLabel.TabIndex = 27
		Me.C_DensityLabel.Text = "Kg / m^3"
		'
		'C_DensityTitleLabel
		'
		Me.C_DensityTitleLabel.AutoSize = True
		Me.C_DensityTitleLabel.Font = New System.Drawing.Font("PMingLiU", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
		Me.C_DensityTitleLabel.Location = New System.Drawing.Point(0, 15)
		Me.C_DensityTitleLabel.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.C_DensityTitleLabel.Name = "C_DensityTitleLabel"
		Me.C_DensityTitleLabel.Size = New System.Drawing.Size(37, 15)
		Me.C_DensityTitleLabel.TabIndex = 25
		Me.C_DensityTitleLabel.Text = "密度"
		'
		'C_Mass_Panel
		'
		Me.C_Mass_Panel.Controls.Add(Me.C_MassTextBox)
		Me.C_Mass_Panel.Controls.Add(Me.C_MassLabel)
		Me.C_Mass_Panel.Controls.Add(Me.C_MassTitleLabel)
		Me.C_Mass_Panel.Location = New System.Drawing.Point(4, 216)
		Me.C_Mass_Panel.Margin = New System.Windows.Forms.Padding(4)
		Me.C_Mass_Panel.Name = "C_Mass_Panel"
		Me.C_Mass_Panel.Size = New System.Drawing.Size(520, 38)
		Me.C_Mass_Panel.TabIndex = 34
		'
		'C_MassTextBox
		'
		Me.C_MassTextBox.Location = New System.Drawing.Point(47, 4)
		Me.C_MassTextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.C_MassTextBox.Name = "C_MassTextBox"
		Me.C_MassTextBox.ReadOnly = True
		Me.C_MassTextBox.Size = New System.Drawing.Size(348, 25)
		Me.C_MassTextBox.TabIndex = 28
		'
		'C_MassLabel
		'
		Me.C_MassLabel.Location = New System.Drawing.Point(419, 10)
		Me.C_MassLabel.Margin = New System.Windows.Forms.Padding(7, 10, 4, 0)
		Me.C_MassLabel.Name = "C_MassLabel"
		Me.C_MassLabel.Size = New System.Drawing.Size(84, 15)
		Me.C_MassLabel.TabIndex = 30
		Me.C_MassLabel.Text = "Kg"
		'
		'C_MassTitleLabel
		'
		Me.C_MassTitleLabel.AutoSize = True
		Me.C_MassTitleLabel.Font = New System.Drawing.Font("PMingLiU", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
		Me.C_MassTitleLabel.Location = New System.Drawing.Point(0, 10)
		Me.C_MassTitleLabel.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.C_MassTitleLabel.Name = "C_MassTitleLabel"
		Me.C_MassTitleLabel.Size = New System.Drawing.Size(37, 15)
		Me.C_MassTitleLabel.TabIndex = 29
		Me.C_MassTitleLabel.Text = "質量"
		'
		'D_ProcessWFE_Panel
		'
		Me.D_ProcessWFE_Panel.Controls.Add(Me.Label56)
		Me.D_ProcessWFE_Panel.Controls.Add(Me.C_Manufacture_ISO_TextBox)
		Me.D_ProcessWFE_Panel.Controls.Add(Me.Label33)
		Me.D_ProcessWFE_Panel.Controls.Add(Me.Label48)
		Me.D_ProcessWFE_Panel.Controls.Add(Me.Label39)
		Me.D_ProcessWFE_Panel.Controls.Add(Me.C_Manufacture_WFP_TextBox)
		Me.D_ProcessWFE_Panel.Controls.Add(Me.Label40)
		Me.D_ProcessWFE_Panel.Location = New System.Drawing.Point(4, 262)
		Me.D_ProcessWFE_Panel.Margin = New System.Windows.Forms.Padding(4)
		Me.D_ProcessWFE_Panel.Name = "D_ProcessWFE_Panel"
		Me.D_ProcessWFE_Panel.Size = New System.Drawing.Size(520, 71)
		Me.D_ProcessWFE_Panel.TabIndex = 35
		'
		'Label56
		'
		Me.Label56.Location = New System.Drawing.Point(419, 42)
		Me.Label56.Margin = New System.Windows.Forms.Padding(7, 10, 4, 0)
		Me.Label56.Name = "Label56"
		Me.Label56.Size = New System.Drawing.Size(84, 15)
		Me.Label56.TabIndex = 37
		Me.Label56.Text = "m^3"
		'
		'C_Manufacture_ISO_TextBox
		'
		Me.C_Manufacture_ISO_TextBox.Location = New System.Drawing.Point(187, 40)
		Me.C_Manufacture_ISO_TextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.C_Manufacture_ISO_TextBox.Name = "C_Manufacture_ISO_TextBox"
		Me.C_Manufacture_ISO_TextBox.ReadOnly = True
		Me.C_Manufacture_ISO_TextBox.Size = New System.Drawing.Size(208, 25)
		Me.C_Manufacture_ISO_TextBox.TabIndex = 36
		'
		'Label33
		'
		Me.Label33.AutoSize = True
		Me.Label33.Font = New System.Drawing.Font("PMingLiU", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
		Me.Label33.Location = New System.Drawing.Point(97, 42)
		Me.Label33.Name = "Label33"
		Me.Label33.Size = New System.Drawing.Size(78, 15)
		Me.Label33.TabIndex = 35
		Me.Label33.Text = "ISO 14046"
		'
		'Label48
		'
		Me.Label48.AutoSize = True
		Me.Label48.Font = New System.Drawing.Font("PMingLiU", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
		Me.Label48.ForeColor = System.Drawing.Color.Purple
		Me.Label48.Location = New System.Drawing.Point(133, 10)
		Me.Label48.Name = "Label48"
		Me.Label48.Size = New System.Drawing.Size(41, 15)
		Me.Label48.TabIndex = 34
		Me.Label48.Text = "WFN"
		'
		'Label39
		'
		Me.Label39.AutoSize = True
		Me.Label39.Font = New System.Drawing.Font("PMingLiU", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
		Me.Label39.ForeColor = System.Drawing.Color.Black
		Me.Label39.Location = New System.Drawing.Point(4, 10)
		Me.Label39.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label39.Name = "Label39"
		Me.Label39.Size = New System.Drawing.Size(82, 15)
		Me.Label39.TabIndex = 22
		Me.Label39.Text = "製程水足跡"
		'
		'C_Manufacture_WFP_TextBox
		'
		Me.C_Manufacture_WFP_TextBox.Location = New System.Drawing.Point(187, 8)
		Me.C_Manufacture_WFP_TextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.C_Manufacture_WFP_TextBox.Name = "C_Manufacture_WFP_TextBox"
		Me.C_Manufacture_WFP_TextBox.ReadOnly = True
		Me.C_Manufacture_WFP_TextBox.Size = New System.Drawing.Size(208, 25)
		Me.C_Manufacture_WFP_TextBox.TabIndex = 26
		'
		'Label40
		'
		Me.Label40.Location = New System.Drawing.Point(419, 8)
		Me.Label40.Margin = New System.Windows.Forms.Padding(7, 10, 4, 0)
		Me.Label40.Name = "Label40"
		Me.Label40.Size = New System.Drawing.Size(84, 15)
		Me.Label40.TabIndex = 24
		Me.Label40.Text = "m^3"
		'
		'Panel5
		'
		Me.Panel5.Controls.Add(Me.Label94)
		Me.Panel5.Controls.Add(Me.Label93)
		Me.Panel5.Controls.Add(Me.C_Component_ISO_Label)
		Me.Panel5.Controls.Add(Me.Label60)
		Me.Panel5.Controls.Add(Me.Label61)
		Me.Panel5.Controls.Add(Me.c_add_component_Button)
		Me.Panel5.Controls.Add(Me.C_Component_WFP_Label)
		Me.Panel5.Controls.Add(Me.Label25)
		Me.Panel5.Location = New System.Drawing.Point(4, 341)
		Me.Panel5.Margin = New System.Windows.Forms.Padding(4)
		Me.Panel5.Name = "Panel5"
		Me.Panel5.Size = New System.Drawing.Size(520, 69)
		Me.Panel5.TabIndex = 36
		'
		'Label94
		'
		Me.Label94.Location = New System.Drawing.Point(349, 42)
		Me.Label94.Margin = New System.Windows.Forms.Padding(7, 10, 4, 0)
		Me.Label94.Name = "Label94"
		Me.Label94.Size = New System.Drawing.Size(47, 17)
		Me.Label94.TabIndex = 40
		Me.Label94.Text = "m^3"
		'
		'Label93
		'
		Me.Label93.Location = New System.Drawing.Point(349, 11)
		Me.Label93.Margin = New System.Windows.Forms.Padding(7, 10, 4, 0)
		Me.Label93.Name = "Label93"
		Me.Label93.Size = New System.Drawing.Size(47, 17)
		Me.Label93.TabIndex = 39
		Me.Label93.Text = "m^3"
		'
		'C_Component_ISO_Label
		'
		Me.C_Component_ISO_Label.AutoSize = True
		Me.C_Component_ISO_Label.Location = New System.Drawing.Point(184, 44)
		Me.C_Component_ISO_Label.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
		Me.C_Component_ISO_Label.Name = "C_Component_ISO_Label"
		Me.C_Component_ISO_Label.Size = New System.Drawing.Size(31, 15)
		Me.C_Component_ISO_Label.TabIndex = 38
		Me.C_Component_ISO_Label.Text = "N/A"
		'
		'Label60
		'
		Me.Label60.AutoSize = True
		Me.Label60.Font = New System.Drawing.Font("PMingLiU", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
		Me.Label60.Location = New System.Drawing.Point(97, 44)
		Me.Label60.Name = "Label60"
		Me.Label60.Size = New System.Drawing.Size(78, 15)
		Me.Label60.TabIndex = 37
		Me.Label60.Text = "ISO 14046"
		'
		'Label61
		'
		Me.Label61.AutoSize = True
		Me.Label61.Font = New System.Drawing.Font("PMingLiU", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
		Me.Label61.ForeColor = System.Drawing.Color.Purple
		Me.Label61.Location = New System.Drawing.Point(133, 11)
		Me.Label61.Name = "Label61"
		Me.Label61.Size = New System.Drawing.Size(41, 15)
		Me.Label61.TabIndex = 36
		Me.Label61.Text = "WFN"
		'
		'c_add_component_Button
		'
		Me.c_add_component_Button.Location = New System.Drawing.Point(403, 8)
		Me.c_add_component_Button.Margin = New System.Windows.Forms.Padding(4)
		Me.c_add_component_Button.Name = "c_add_component_Button"
		Me.c_add_component_Button.Size = New System.Drawing.Size(100, 55)
		Me.c_add_component_Button.TabIndex = 24
		Me.c_add_component_Button.Text = "新增"
		Me.c_add_component_Button.UseVisualStyleBackColor = True
		'
		'C_Component_WFP_Label
		'
		Me.C_Component_WFP_Label.AutoSize = True
		Me.C_Component_WFP_Label.Location = New System.Drawing.Point(184, 11)
		Me.C_Component_WFP_Label.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
		Me.C_Component_WFP_Label.Name = "C_Component_WFP_Label"
		Me.C_Component_WFP_Label.Size = New System.Drawing.Size(31, 15)
		Me.C_Component_WFP_Label.TabIndex = 23
		Me.C_Component_WFP_Label.Text = "N/A"
		'
		'Label25
		'
		Me.Label25.AutoSize = True
		Me.Label25.Font = New System.Drawing.Font("PMingLiU", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
		Me.Label25.ForeColor = System.Drawing.Color.CornflowerBlue
		Me.Label25.Location = New System.Drawing.Point(4, 10)
		Me.Label25.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label25.Name = "Label25"
		Me.Label25.Size = New System.Drawing.Size(97, 15)
		Me.Label25.TabIndex = 22
		Me.Label25.Text = "本零件水足跡"
		'
		'FlowLayoutPanel5
		'
		Me.FlowLayoutPanel5.Controls.Add(Me.EBoardRadioButton)
		Me.FlowLayoutPanel5.Controls.Add(Me.EComopnentRadioButton)
		Me.FlowLayoutPanel5.Controls.Add(Me.LCDRadioButton)
		Me.FlowLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Top
		Me.FlowLayoutPanel5.Location = New System.Drawing.Point(4, 22)
		Me.FlowLayoutPanel5.Margin = New System.Windows.Forms.Padding(4)
		Me.FlowLayoutPanel5.Name = "FlowLayoutPanel5"
		Me.FlowLayoutPanel5.Size = New System.Drawing.Size(528, 31)
		Me.FlowLayoutPanel5.TabIndex = 0
		'
		'EBoardRadioButton
		'
		Me.EBoardRadioButton.AutoSize = True
		Me.EBoardRadioButton.ForeColor = System.Drawing.Color.DarkGreen
		Me.EBoardRadioButton.Location = New System.Drawing.Point(4, 4)
		Me.EBoardRadioButton.Margin = New System.Windows.Forms.Padding(4)
		Me.EBoardRadioButton.Name = "EBoardRadioButton"
		Me.EBoardRadioButton.Size = New System.Drawing.Size(103, 19)
		Me.EBoardRadioButton.TabIndex = 0
		Me.EBoardRadioButton.TabStop = True
		Me.EBoardRadioButton.Text = "電路板零件"
		Me.EBoardRadioButton.UseVisualStyleBackColor = True
		'
		'EComopnentRadioButton
		'
		Me.EComopnentRadioButton.AutoSize = True
		Me.EComopnentRadioButton.ForeColor = System.Drawing.Color.Navy
		Me.EComopnentRadioButton.Location = New System.Drawing.Point(115, 4)
		Me.EComopnentRadioButton.Margin = New System.Windows.Forms.Padding(4)
		Me.EComopnentRadioButton.Name = "EComopnentRadioButton"
		Me.EComopnentRadioButton.Size = New System.Drawing.Size(218, 19)
		Me.EComopnentRadioButton.TabIndex = 1
		Me.EComopnentRadioButton.TabStop = True
		Me.EComopnentRadioButton.Text = "電子元件(晶片、電容與電阻)"
		Me.EComopnentRadioButton.UseVisualStyleBackColor = True
		'
		'LCDRadioButton
		'
		Me.LCDRadioButton.AutoSize = True
		Me.LCDRadioButton.ForeColor = System.Drawing.Color.Red
		Me.LCDRadioButton.Location = New System.Drawing.Point(341, 4)
		Me.LCDRadioButton.Margin = New System.Windows.Forms.Padding(4)
		Me.LCDRadioButton.Name = "LCDRadioButton"
		Me.LCDRadioButton.Size = New System.Drawing.Size(103, 19)
		Me.LCDRadioButton.TabIndex = 2
		Me.LCDRadioButton.TabStop = True
		Me.LCDRadioButton.Text = "液晶顯示器"
		Me.LCDRadioButton.UseVisualStyleBackColor = True
		'
		'GroupBox10
		'
		Me.GroupBox10.Controls.Add(Me.c_Special_Components_DataGridView)
		Me.GroupBox10.Controls.Add(Me.C_Delete_Button)
		Me.GroupBox10.Location = New System.Drawing.Point(4, 504)
		Me.GroupBox10.Margin = New System.Windows.Forms.Padding(4)
		Me.GroupBox10.Name = "GroupBox10"
		Me.GroupBox10.Padding = New System.Windows.Forms.Padding(4)
		Me.GroupBox10.Size = New System.Drawing.Size(532, 313)
		Me.GroupBox10.TabIndex = 1
		Me.GroupBox10.TabStop = False
		Me.GroupBox10.Text = "已選特殊零件(以下水足跡單位為 m^3)"
		'
		'c_Special_Components_DataGridView
		'
		Me.c_Special_Components_DataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
		Me.c_Special_Components_DataGridView.Dock = System.Windows.Forms.DockStyle.Fill
		Me.c_Special_Components_DataGridView.Location = New System.Drawing.Point(4, 22)
		Me.c_Special_Components_DataGridView.Margin = New System.Windows.Forms.Padding(4)
		Me.c_Special_Components_DataGridView.Name = "c_Special_Components_DataGridView"
		Me.c_Special_Components_DataGridView.RowTemplate.Height = 24
		Me.c_Special_Components_DataGridView.Size = New System.Drawing.Size(524, 249)
		Me.c_Special_Components_DataGridView.TabIndex = 1
		'
		'C_Delete_Button
		'
		Me.C_Delete_Button.Dock = System.Windows.Forms.DockStyle.Bottom
		Me.C_Delete_Button.Location = New System.Drawing.Point(4, 271)
		Me.C_Delete_Button.Margin = New System.Windows.Forms.Padding(4)
		Me.C_Delete_Button.Name = "C_Delete_Button"
		Me.C_Delete_Button.Size = New System.Drawing.Size(524, 38)
		Me.C_Delete_Button.TabIndex = 0
		Me.C_Delete_Button.Text = "刪除記錄"
		Me.C_Delete_Button.UseVisualStyleBackColor = True
		'
		'Panel4
		'
		Me.Panel4.Controls.Add(Me.Label65)
		Me.Panel4.Controls.Add(Me.C_Total_ISO_TextBox)
		Me.Panel4.Controls.Add(Me.Label63)
		Me.Panel4.Controls.Add(Me.Label64)
		Me.Panel4.Controls.Add(Me.Label41)
		Me.Panel4.Controls.Add(Me.C_Total_WFP_TextBox)
		Me.Panel4.Controls.Add(Me.Label42)
		Me.Panel4.Location = New System.Drawing.Point(4, 825)
		Me.Panel4.Margin = New System.Windows.Forms.Padding(4)
		Me.Panel4.Name = "Panel4"
		Me.Panel4.Size = New System.Drawing.Size(528, 68)
		Me.Panel4.TabIndex = 5
		'
		'Label65
		'
		Me.Label65.AutoSize = True
		Me.Label65.Location = New System.Drawing.Point(427, 44)
		Me.Label65.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
		Me.Label65.Name = "Label65"
		Me.Label65.Size = New System.Drawing.Size(32, 15)
		Me.Label65.TabIndex = 41
		Me.Label65.Text = "m^3"
		'
		'C_Total_ISO_TextBox
		'
		Me.C_Total_ISO_TextBox.Location = New System.Drawing.Point(231, 38)
		Me.C_Total_ISO_TextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.C_Total_ISO_TextBox.Name = "C_Total_ISO_TextBox"
		Me.C_Total_ISO_TextBox.ReadOnly = True
		Me.C_Total_ISO_TextBox.Size = New System.Drawing.Size(172, 25)
		Me.C_Total_ISO_TextBox.TabIndex = 40
		'
		'Label63
		'
		Me.Label63.AutoSize = True
		Me.Label63.Font = New System.Drawing.Font("PMingLiU", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
		Me.Label63.Location = New System.Drawing.Point(128, 44)
		Me.Label63.Name = "Label63"
		Me.Label63.Size = New System.Drawing.Size(78, 15)
		Me.Label63.TabIndex = 39
		Me.Label63.Text = "ISO 14046"
		'
		'Label64
		'
		Me.Label64.AutoSize = True
		Me.Label64.Font = New System.Drawing.Font("PMingLiU", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
		Me.Label64.ForeColor = System.Drawing.Color.Purple
		Me.Label64.Location = New System.Drawing.Point(165, 9)
		Me.Label64.Name = "Label64"
		Me.Label64.Size = New System.Drawing.Size(41, 15)
		Me.Label64.TabIndex = 38
		Me.Label64.Text = "WFN"
		'
		'Label41
		'
		Me.Label41.AutoSize = True
		Me.Label41.Location = New System.Drawing.Point(427, 9)
		Me.Label41.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
		Me.Label41.Name = "Label41"
		Me.Label41.Size = New System.Drawing.Size(32, 15)
		Me.Label41.TabIndex = 2
		Me.Label41.Text = "m^3"
		'
		'C_Total_WFP_TextBox
		'
		Me.C_Total_WFP_TextBox.Location = New System.Drawing.Point(231, 4)
		Me.C_Total_WFP_TextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.C_Total_WFP_TextBox.Name = "C_Total_WFP_TextBox"
		Me.C_Total_WFP_TextBox.ReadOnly = True
		Me.C_Total_WFP_TextBox.Size = New System.Drawing.Size(172, 25)
		Me.C_Total_WFP_TextBox.TabIndex = 1
		'
		'Label42
		'
		Me.Label42.ForeColor = System.Drawing.Color.CornflowerBlue
		Me.Label42.Location = New System.Drawing.Point(12, 0)
		Me.Label42.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
		Me.Label42.Name = "Label42"
		Me.Label42.Size = New System.Drawing.Size(115, 68)
		Me.Label42.TabIndex = 0
		Me.Label42.Text = "所有特殊零件水足跡小計"
		Me.Label42.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
		'
		'C_CompletedButton
		'
		Me.C_CompletedButton.Location = New System.Drawing.Point(8, 901)
		Me.C_CompletedButton.Margin = New System.Windows.Forms.Padding(8, 4, 4, 4)
		Me.C_CompletedButton.Name = "C_CompletedButton"
		Me.C_CompletedButton.Size = New System.Drawing.Size(535, 40)
		Me.C_CompletedButton.TabIndex = 9
		Me.C_CompletedButton.Text = "完成此階段評估"
		Me.C_CompletedButton.UseVisualStyleBackColor = True
		'
		'TabPage7
		'
		Me.TabPage7.Controls.Add(Me.product_assembly_tabpage_FlowLayoutPanel)
		Me.TabPage7.ImageIndex = 6
		Me.TabPage7.Location = New System.Drawing.Point(4, 25)
		Me.TabPage7.Name = "TabPage7"
		Me.TabPage7.Padding = New System.Windows.Forms.Padding(3)
		Me.TabPage7.Size = New System.Drawing.Size(556, 959)
		Me.TabPage7.TabIndex = 4
		Me.TabPage7.Text = "C. 產品組裝"
		Me.TabPage7.UseVisualStyleBackColor = True
		'
		'product_assembly_tabpage_FlowLayoutPanel
		'
		Me.product_assembly_tabpage_FlowLayoutPanel.Controls.Add(Me.GroupBox13)
		Me.product_assembly_tabpage_FlowLayoutPanel.Controls.Add(Me.GroupBox14)
		Me.product_assembly_tabpage_FlowLayoutPanel.Controls.Add(Me.Panel8)
		Me.product_assembly_tabpage_FlowLayoutPanel.Controls.Add(Me.GroupBox15)
		Me.product_assembly_tabpage_FlowLayoutPanel.Controls.Add(Me.Panel7)
		Me.product_assembly_tabpage_FlowLayoutPanel.Controls.Add(Me.assembly_stage_finish_Button)
		Me.product_assembly_tabpage_FlowLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill
		Me.product_assembly_tabpage_FlowLayoutPanel.Location = New System.Drawing.Point(3, 3)
		Me.product_assembly_tabpage_FlowLayoutPanel.Name = "product_assembly_tabpage_FlowLayoutPanel"
		Me.product_assembly_tabpage_FlowLayoutPanel.Size = New System.Drawing.Size(550, 953)
		Me.product_assembly_tabpage_FlowLayoutPanel.TabIndex = 0
		'
		'GroupBox13
		'
		Me.GroupBox13.Controls.Add(Me.FlowLayoutPanel10)
		Me.GroupBox13.Location = New System.Drawing.Point(3, 3)
		Me.GroupBox13.Name = "GroupBox13"
		Me.GroupBox13.Size = New System.Drawing.Size(542, 345)
		Me.GroupBox13.TabIndex = 0
		Me.GroupBox13.TabStop = False
		Me.GroupBox13.Text = "1. 來自前一階段 A-1 或 A-2 或 B 累計的水足跡(以下水足跡單位為 m^3)"
		'
		'FlowLayoutPanel10
		'
		Me.FlowLayoutPanel10.Controls.Add(Me.assemblyDataGridView)
		Me.FlowLayoutPanel10.Controls.Add(Me.Label71)
		Me.FlowLayoutPanel10.Controls.Add(Me.assembly_choiceTextBox)
		Me.FlowLayoutPanel10.Controls.Add(Me.assembly_choice_transport_Button)
		Me.FlowLayoutPanel10.Controls.Add(Me.add_assembly_Button)
		Me.FlowLayoutPanel10.Controls.Add(Me.Panel6)
		Me.FlowLayoutPanel10.Dock = System.Windows.Forms.DockStyle.Fill
		Me.FlowLayoutPanel10.Location = New System.Drawing.Point(3, 21)
		Me.FlowLayoutPanel10.Name = "FlowLayoutPanel10"
		Me.FlowLayoutPanel10.Size = New System.Drawing.Size(536, 321)
		Me.FlowLayoutPanel10.TabIndex = 0
		'
		'assemblyDataGridView
		'
		Me.assemblyDataGridView.AllowUserToAddRows = False
		Me.assemblyDataGridView.AllowUserToDeleteRows = False
		Me.assemblyDataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
		Me.assemblyDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
		Me.assemblyDataGridView.Location = New System.Drawing.Point(3, 3)
		Me.assemblyDataGridView.Name = "assemblyDataGridView"
		Me.assemblyDataGridView.ReadOnly = True
		Me.assemblyDataGridView.RowHeadersVisible = False
		Me.assemblyDataGridView.RowTemplate.Height = 27
		Me.assemblyDataGridView.Size = New System.Drawing.Size(530, 177)
		Me.assemblyDataGridView.TabIndex = 0
		'
		'Label71
		'
		Me.Label71.AutoSize = True
		Me.Label71.Location = New System.Drawing.Point(3, 191)
		Me.Label71.Margin = New System.Windows.Forms.Padding(3, 8, 3, 0)
		Me.Label71.Name = "Label71"
		Me.Label71.Size = New System.Drawing.Size(67, 15)
		Me.Label71.TabIndex = 1
		Me.Label71.Text = "已選零件"
		'
		'assembly_choiceTextBox
		'
		Me.assembly_choiceTextBox.Location = New System.Drawing.Point(76, 186)
		Me.assembly_choiceTextBox.Name = "assembly_choiceTextBox"
		Me.assembly_choiceTextBox.ReadOnly = True
		Me.assembly_choiceTextBox.Size = New System.Drawing.Size(450, 25)
		Me.assembly_choiceTextBox.TabIndex = 2
		'
		'assembly_choice_transport_Button
		'
		Me.assembly_choice_transport_Button.Location = New System.Drawing.Point(4, 218)
		Me.assembly_choice_transport_Button.Margin = New System.Windows.Forms.Padding(4)
		Me.assembly_choice_transport_Button.Name = "assembly_choice_transport_Button"
		Me.assembly_choice_transport_Button.Size = New System.Drawing.Size(380, 38)
		Me.assembly_choice_transport_Button.TabIndex = 26
		Me.assembly_choice_transport_Button.Text = "1.1 選擇運輸工具"
		Me.assembly_choice_transport_Button.UseVisualStyleBackColor = True
		'
		'add_assembly_Button
		'
		Me.add_assembly_Button.Location = New System.Drawing.Point(392, 218)
		Me.add_assembly_Button.Margin = New System.Windows.Forms.Padding(4)
		Me.add_assembly_Button.Name = "add_assembly_Button"
		Me.add_assembly_Button.Size = New System.Drawing.Size(134, 38)
		Me.add_assembly_Button.TabIndex = 28
		Me.add_assembly_Button.Text = "1.2 加入組裝"
		Me.add_assembly_Button.UseVisualStyleBackColor = True
		'
		'Panel6
		'
		Me.Panel6.Controls.Add(Me.Label96)
		Me.Panel6.Controls.Add(Me.Label95)
		Me.Panel6.Controls.Add(Me.assembly_iso_Label)
		Me.Panel6.Controls.Add(Me.Label76)
		Me.Panel6.Controls.Add(Me.Label73)
		Me.Panel6.Controls.Add(Me.assembly_wfn_Label)
		Me.Panel6.Controls.Add(Me.Label74)
		Me.Panel6.Location = New System.Drawing.Point(3, 260)
		Me.Panel6.Margin = New System.Windows.Forms.Padding(3, 0, 3, 3)
		Me.Panel6.Name = "Panel6"
		Me.Panel6.Size = New System.Drawing.Size(530, 63)
		Me.Panel6.TabIndex = 27
		'
		'Label96
		'
		Me.Label96.AutoSize = True
		Me.Label96.Location = New System.Drawing.Point(386, 42)
		Me.Label96.Name = "Label96"
		Me.Label96.Size = New System.Drawing.Size(32, 15)
		Me.Label96.TabIndex = 36
		Me.Label96.Text = "m^3"
		'
		'Label95
		'
		Me.Label95.AutoSize = True
		Me.Label95.Location = New System.Drawing.Point(386, 13)
		Me.Label95.Name = "Label95"
		Me.Label95.Size = New System.Drawing.Size(32, 15)
		Me.Label95.TabIndex = 35
		Me.Label95.Text = "m^3"
		'
		'assembly_iso_Label
		'
		Me.assembly_iso_Label.AutoSize = True
		Me.assembly_iso_Label.ForeColor = System.Drawing.Color.CornflowerBlue
		Me.assembly_iso_Label.Location = New System.Drawing.Point(233, 42)
		Me.assembly_iso_Label.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
		Me.assembly_iso_Label.Name = "assembly_iso_Label"
		Me.assembly_iso_Label.Size = New System.Drawing.Size(31, 15)
		Me.assembly_iso_Label.TabIndex = 34
		Me.assembly_iso_Label.Text = "N/A"
		'
		'Label76
		'
		Me.Label76.AutoSize = True
		Me.Label76.Location = New System.Drawing.Point(4, 10)
		Me.Label76.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
		Me.Label76.Name = "Label76"
		Me.Label76.Size = New System.Drawing.Size(142, 15)
		Me.Label76.TabIndex = 27
		Me.Label76.Text = "組裝廠運輸總水足跡"
		'
		'Label73
		'
		Me.Label73.AutoSize = True
		Me.Label73.Font = New System.Drawing.Font("PMingLiU", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
		Me.Label73.Location = New System.Drawing.Point(127, 42)
		Me.Label73.Name = "Label73"
		Me.Label73.Size = New System.Drawing.Size(78, 15)
		Me.Label73.TabIndex = 33
		Me.Label73.Text = "ISO 14046"
		'
		'assembly_wfn_Label
		'
		Me.assembly_wfn_Label.AutoSize = True
		Me.assembly_wfn_Label.ForeColor = System.Drawing.Color.CornflowerBlue
		Me.assembly_wfn_Label.Location = New System.Drawing.Point(233, 13)
		Me.assembly_wfn_Label.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
		Me.assembly_wfn_Label.Name = "assembly_wfn_Label"
		Me.assembly_wfn_Label.Size = New System.Drawing.Size(31, 15)
		Me.assembly_wfn_Label.TabIndex = 28
		Me.assembly_wfn_Label.Text = "N/A"
		'
		'Label74
		'
		Me.Label74.AutoSize = True
		Me.Label74.Font = New System.Drawing.Font("PMingLiU", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
		Me.Label74.ForeColor = System.Drawing.Color.Purple
		Me.Label74.Location = New System.Drawing.Point(164, 13)
		Me.Label74.Name = "Label74"
		Me.Label74.Size = New System.Drawing.Size(41, 15)
		Me.Label74.TabIndex = 32
		Me.Label74.Text = "WFN"
		'
		'GroupBox14
		'
		Me.GroupBox14.Controls.Add(Me.assembly_TreeView)
		Me.GroupBox14.Controls.Add(Me.FlowLayoutPanel11)
		Me.GroupBox14.Location = New System.Drawing.Point(3, 354)
		Me.GroupBox14.Name = "GroupBox14"
		Me.GroupBox14.Size = New System.Drawing.Size(541, 233)
		Me.GroupBox14.TabIndex = 3
		Me.GroupBox14.TabStop = False
		Me.GroupBox14.Text = "2. 進行產品組裝"
		'
		'assembly_TreeView
		'
		Me.assembly_TreeView.Dock = System.Windows.Forms.DockStyle.Fill
		Me.assembly_TreeView.Location = New System.Drawing.Point(3, 61)
		Me.assembly_TreeView.Name = "assembly_TreeView"
		Me.assembly_TreeView.Size = New System.Drawing.Size(535, 169)
		Me.assembly_TreeView.TabIndex = 1
		'
		'FlowLayoutPanel11
		'
		Me.FlowLayoutPanel11.Controls.Add(Me.enable_merge_CheckBox)
		Me.FlowLayoutPanel11.Controls.Add(Me.enable_disassembly_CheckBox)
		Me.FlowLayoutPanel11.Controls.Add(Me.function_ok_Button)
		Me.FlowLayoutPanel11.Controls.Add(Me.assembly_tree_toggle_Button)
		Me.FlowLayoutPanel11.Dock = System.Windows.Forms.DockStyle.Top
		Me.FlowLayoutPanel11.Location = New System.Drawing.Point(3, 21)
		Me.FlowLayoutPanel11.Name = "FlowLayoutPanel11"
		Me.FlowLayoutPanel11.Size = New System.Drawing.Size(535, 40)
		Me.FlowLayoutPanel11.TabIndex = 0
		'
		'enable_merge_CheckBox
		'
		Me.enable_merge_CheckBox.Appearance = System.Windows.Forms.Appearance.Button
		Me.enable_merge_CheckBox.ForeColor = System.Drawing.Color.DarkCyan
		Me.enable_merge_CheckBox.Location = New System.Drawing.Point(3, 3)
		Me.enable_merge_CheckBox.Name = "enable_merge_CheckBox"
		Me.enable_merge_CheckBox.Size = New System.Drawing.Size(104, 32)
		Me.enable_merge_CheckBox.TabIndex = 5
		Me.enable_merge_CheckBox.Text = "組裝"
		Me.enable_merge_CheckBox.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		Me.enable_merge_CheckBox.UseVisualStyleBackColor = True
		'
		'enable_disassembly_CheckBox
		'
		Me.enable_disassembly_CheckBox.Appearance = System.Windows.Forms.Appearance.Button
		Me.enable_disassembly_CheckBox.ForeColor = System.Drawing.Color.Magenta
		Me.enable_disassembly_CheckBox.Location = New System.Drawing.Point(113, 3)
		Me.enable_disassembly_CheckBox.Name = "enable_disassembly_CheckBox"
		Me.enable_disassembly_CheckBox.Size = New System.Drawing.Size(104, 32)
		Me.enable_disassembly_CheckBox.TabIndex = 6
		Me.enable_disassembly_CheckBox.Text = "分解 / 移除"
		Me.enable_disassembly_CheckBox.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		Me.enable_disassembly_CheckBox.UseVisualStyleBackColor = True
		'
		'function_ok_Button
		'
		Me.function_ok_Button.Font = New System.Drawing.Font("PMingLiU", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
		Me.function_ok_Button.ForeColor = System.Drawing.Color.Green
		Me.function_ok_Button.Location = New System.Drawing.Point(223, 3)
		Me.function_ok_Button.Name = "function_ok_Button"
		Me.function_ok_Button.Size = New System.Drawing.Size(120, 32)
		Me.function_ok_Button.TabIndex = 4
		Me.function_ok_Button.Text = "確定"
		Me.function_ok_Button.UseVisualStyleBackColor = True
		'
		'assembly_tree_toggle_Button
		'
		Me.assembly_tree_toggle_Button.ForeColor = System.Drawing.Color.Blue
		Me.assembly_tree_toggle_Button.Location = New System.Drawing.Point(349, 3)
		Me.assembly_tree_toggle_Button.Name = "assembly_tree_toggle_Button"
		Me.assembly_tree_toggle_Button.Size = New System.Drawing.Size(120, 32)
		Me.assembly_tree_toggle_Button.TabIndex = 3
		Me.assembly_tree_toggle_Button.Text = "收合節點"
		Me.assembly_tree_toggle_Button.UseVisualStyleBackColor = True
		'
		'Panel8
		'
		Me.Panel8.Controls.Add(Me.Label97)
		Me.Panel8.Controls.Add(Me.Label98)
		Me.Panel8.Controls.Add(Me.asm_stage_iso_Label)
		Me.Panel8.Controls.Add(Me.Label100)
		Me.Panel8.Controls.Add(Me.Label101)
		Me.Panel8.Controls.Add(Me.asm_stage_wfn_Label)
		Me.Panel8.Controls.Add(Me.Label103)
		Me.Panel8.Location = New System.Drawing.Point(3, 590)
		Me.Panel8.Margin = New System.Windows.Forms.Padding(3, 0, 3, 3)
		Me.Panel8.Name = "Panel8"
		Me.Panel8.Size = New System.Drawing.Size(536, 63)
		Me.Panel8.TabIndex = 28
		'
		'Label97
		'
		Me.Label97.AutoSize = True
		Me.Label97.Location = New System.Drawing.Point(386, 42)
		Me.Label97.Name = "Label97"
		Me.Label97.Size = New System.Drawing.Size(32, 15)
		Me.Label97.TabIndex = 36
		Me.Label97.Text = "m^3"
		'
		'Label98
		'
		Me.Label98.AutoSize = True
		Me.Label98.Location = New System.Drawing.Point(386, 13)
		Me.Label98.Name = "Label98"
		Me.Label98.Size = New System.Drawing.Size(32, 15)
		Me.Label98.TabIndex = 35
		Me.Label98.Text = "m^3"
		'
		'asm_stage_iso_Label
		'
		Me.asm_stage_iso_Label.AutoSize = True
		Me.asm_stage_iso_Label.ForeColor = System.Drawing.Color.CornflowerBlue
		Me.asm_stage_iso_Label.Location = New System.Drawing.Point(233, 42)
		Me.asm_stage_iso_Label.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
		Me.asm_stage_iso_Label.Name = "asm_stage_iso_Label"
		Me.asm_stage_iso_Label.Size = New System.Drawing.Size(31, 15)
		Me.asm_stage_iso_Label.TabIndex = 34
		Me.asm_stage_iso_Label.Text = "N/A"
		'
		'Label100
		'
		Me.Label100.AutoSize = True
		Me.Label100.Location = New System.Drawing.Point(4, 10)
		Me.Label100.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
		Me.Label100.Name = "Label100"
		Me.Label100.Size = New System.Drawing.Size(112, 15)
		Me.Label100.TabIndex = 27
		Me.Label100.Text = "組裝階段水足跡"
		'
		'Label101
		'
		Me.Label101.AutoSize = True
		Me.Label101.Font = New System.Drawing.Font("PMingLiU", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
		Me.Label101.Location = New System.Drawing.Point(127, 42)
		Me.Label101.Name = "Label101"
		Me.Label101.Size = New System.Drawing.Size(78, 15)
		Me.Label101.TabIndex = 33
		Me.Label101.Text = "ISO 14046"
		'
		'asm_stage_wfn_Label
		'
		Me.asm_stage_wfn_Label.AutoSize = True
		Me.asm_stage_wfn_Label.ForeColor = System.Drawing.Color.CornflowerBlue
		Me.asm_stage_wfn_Label.Location = New System.Drawing.Point(233, 13)
		Me.asm_stage_wfn_Label.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
		Me.asm_stage_wfn_Label.Name = "asm_stage_wfn_Label"
		Me.asm_stage_wfn_Label.Size = New System.Drawing.Size(31, 15)
		Me.asm_stage_wfn_Label.TabIndex = 28
		Me.asm_stage_wfn_Label.Text = "N/A"
		'
		'Label103
		'
		Me.Label103.AutoSize = True
		Me.Label103.Font = New System.Drawing.Font("PMingLiU", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
		Me.Label103.ForeColor = System.Drawing.Color.Purple
		Me.Label103.Location = New System.Drawing.Point(164, 13)
		Me.Label103.Name = "Label103"
		Me.Label103.Size = New System.Drawing.Size(41, 15)
		Me.Label103.TabIndex = 32
		Me.Label103.Text = "WFN"
		'
		'GroupBox15
		'
		Me.GroupBox15.Controls.Add(Me.assembly_record_DataGridView)
		Me.GroupBox15.Location = New System.Drawing.Point(3, 659)
		Me.GroupBox15.Name = "GroupBox15"
		Me.GroupBox15.Size = New System.Drawing.Size(541, 166)
		Me.GroupBox15.TabIndex = 4
		Me.GroupBox15.TabStop = False
		Me.GroupBox15.Text = "組裝記錄(以下水足跡單位為 m^3)"
		'
		'assembly_record_DataGridView
		'
		Me.assembly_record_DataGridView.AllowUserToAddRows = False
		Me.assembly_record_DataGridView.AllowUserToDeleteRows = False
		Me.assembly_record_DataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
		Me.assembly_record_DataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
		Me.assembly_record_DataGridView.Dock = System.Windows.Forms.DockStyle.Fill
		Me.assembly_record_DataGridView.Location = New System.Drawing.Point(3, 21)
		Me.assembly_record_DataGridView.Name = "assembly_record_DataGridView"
		Me.assembly_record_DataGridView.ReadOnly = True
		Me.assembly_record_DataGridView.RowHeadersVisible = False
		Me.assembly_record_DataGridView.RowTemplate.Height = 27
		Me.assembly_record_DataGridView.Size = New System.Drawing.Size(535, 142)
		Me.assembly_record_DataGridView.TabIndex = 0
		'
		'Panel7
		'
		Me.Panel7.Controls.Add(Me.Label77)
		Me.Panel7.Controls.Add(Me.assembly_total_iso_TextBox)
		Me.Panel7.Controls.Add(Me.Label78)
		Me.Panel7.Controls.Add(Me.Label79)
		Me.Panel7.Controls.Add(Me.Label80)
		Me.Panel7.Controls.Add(Me.assembly_total_wfn_TextBox)
		Me.Panel7.Controls.Add(Me.Label81)
		Me.Panel7.Location = New System.Drawing.Point(4, 828)
		Me.Panel7.Margin = New System.Windows.Forms.Padding(4, 0, 4, 3)
		Me.Panel7.Name = "Panel7"
		Me.Panel7.Size = New System.Drawing.Size(538, 70)
		Me.Panel7.TabIndex = 5
		'
		'Label77
		'
		Me.Label77.AutoSize = True
		Me.Label77.Location = New System.Drawing.Point(389, 44)
		Me.Label77.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
		Me.Label77.Name = "Label77"
		Me.Label77.Size = New System.Drawing.Size(32, 15)
		Me.Label77.TabIndex = 35
		Me.Label77.Text = "m^3"
		'
		'assembly_total_iso_TextBox
		'
		Me.assembly_total_iso_TextBox.Location = New System.Drawing.Point(223, 39)
		Me.assembly_total_iso_TextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.assembly_total_iso_TextBox.Name = "assembly_total_iso_TextBox"
		Me.assembly_total_iso_TextBox.ReadOnly = True
		Me.assembly_total_iso_TextBox.Size = New System.Drawing.Size(149, 25)
		Me.assembly_total_iso_TextBox.TabIndex = 34
		'
		'Label78
		'
		Me.Label78.AutoSize = True
		Me.Label78.Font = New System.Drawing.Font("PMingLiU", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
		Me.Label78.Location = New System.Drawing.Point(128, 44)
		Me.Label78.Name = "Label78"
		Me.Label78.Size = New System.Drawing.Size(78, 15)
		Me.Label78.TabIndex = 33
		Me.Label78.Text = "ISO 14046"
		'
		'Label79
		'
		Me.Label79.AutoSize = True
		Me.Label79.Font = New System.Drawing.Font("PMingLiU", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
		Me.Label79.ForeColor = System.Drawing.Color.Purple
		Me.Label79.Location = New System.Drawing.Point(165, 9)
		Me.Label79.Name = "Label79"
		Me.Label79.Size = New System.Drawing.Size(41, 15)
		Me.Label79.TabIndex = 31
		Me.Label79.Text = "WFN"
		'
		'Label80
		'
		Me.Label80.AutoSize = True
		Me.Label80.Location = New System.Drawing.Point(389, 9)
		Me.Label80.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
		Me.Label80.Name = "Label80"
		Me.Label80.Size = New System.Drawing.Size(32, 15)
		Me.Label80.TabIndex = 2
		Me.Label80.Text = "m^3"
		'
		'assembly_total_wfn_TextBox
		'
		Me.assembly_total_wfn_TextBox.Location = New System.Drawing.Point(223, 6)
		Me.assembly_total_wfn_TextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.assembly_total_wfn_TextBox.Name = "assembly_total_wfn_TextBox"
		Me.assembly_total_wfn_TextBox.ReadOnly = True
		Me.assembly_total_wfn_TextBox.Size = New System.Drawing.Size(149, 25)
		Me.assembly_total_wfn_TextBox.TabIndex = 1
		'
		'Label81
		'
		Me.Label81.ForeColor = System.Drawing.Color.CornflowerBlue
		Me.Label81.Location = New System.Drawing.Point(8, 0)
		Me.Label81.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
		Me.Label81.Name = "Label81"
		Me.Label81.Size = New System.Drawing.Size(115, 74)
		Me.Label81.TabIndex = 0
		Me.Label81.Text = "所有產品組裝水足跡小計"
		Me.Label81.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
		'
		'assembly_stage_finish_Button
		'
		Me.assembly_stage_finish_Button.Location = New System.Drawing.Point(6, 901)
		Me.assembly_stage_finish_Button.Margin = New System.Windows.Forms.Padding(6, 0, 4, 4)
		Me.assembly_stage_finish_Button.Name = "assembly_stage_finish_Button"
		Me.assembly_stage_finish_Button.Size = New System.Drawing.Size(535, 40)
		Me.assembly_stage_finish_Button.TabIndex = 6
		Me.assembly_stage_finish_Button.Text = "完成此階段評估"
		Me.assembly_stage_finish_Button.UseVisualStyleBackColor = True
		'
		'TabPage4
		'
		Me.TabPage4.Controls.Add(Me.FlowLayoutPanel7)
		Me.TabPage4.ImageIndex = 3
		Me.TabPage4.Location = New System.Drawing.Point(4, 25)
		Me.TabPage4.Margin = New System.Windows.Forms.Padding(4)
		Me.TabPage4.Name = "TabPage4"
		Me.TabPage4.Size = New System.Drawing.Size(556, 959)
		Me.TabPage4.TabIndex = 3
		Me.TabPage4.Text = "D. 綜合分析"
		Me.TabPage4.UseVisualStyleBackColor = True
		'
		'FlowLayoutPanel7
		'
		Me.FlowLayoutPanel7.Controls.Add(Me.Label37)
		Me.FlowLayoutPanel7.Controls.Add(Me.GroupBox11)
		Me.FlowLayoutPanel7.Controls.Add(Me.GroupBox12)
		Me.FlowLayoutPanel7.Controls.Add(Me.D_ChoiceProductButton)
		Me.FlowLayoutPanel7.Dock = System.Windows.Forms.DockStyle.Fill
		Me.FlowLayoutPanel7.Location = New System.Drawing.Point(0, 0)
		Me.FlowLayoutPanel7.Margin = New System.Windows.Forms.Padding(4)
		Me.FlowLayoutPanel7.Name = "FlowLayoutPanel7"
		Me.FlowLayoutPanel7.Size = New System.Drawing.Size(556, 959)
		Me.FlowLayoutPanel7.TabIndex = 0
		'
		'Label37
		'
		Me.Label37.Font = New System.Drawing.Font("Microsoft JhengHei", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
		Me.Label37.Location = New System.Drawing.Point(4, 4)
		Me.Label37.Margin = New System.Windows.Forms.Padding(4, 4, 4, 0)
		Me.Label37.Name = "Label37"
		Me.Label37.Size = New System.Drawing.Size(500, 34)
		Me.Label37.TabIndex = 0
		Me.Label37.Text = "各階段水足跡百分比"
		Me.Label37.TextAlign = System.Drawing.ContentAlignment.TopCenter
		'
		'GroupBox11
		'
		Me.GroupBox11.Controls.Add(Me.TabControl2)
		Me.GroupBox11.Location = New System.Drawing.Point(4, 38)
		Me.GroupBox11.Margin = New System.Windows.Forms.Padding(4, 0, 4, 4)
		Me.GroupBox11.Name = "GroupBox11"
		Me.GroupBox11.Padding = New System.Windows.Forms.Padding(4)
		Me.GroupBox11.Size = New System.Drawing.Size(546, 613)
		Me.GroupBox11.TabIndex = 1
		Me.GroupBox11.TabStop = False
		'
		'TabControl2
		'
		Me.TabControl2.Controls.Add(Me.TabPage5)
		Me.TabControl2.Controls.Add(Me.TabPage6)
		Me.TabControl2.Dock = System.Windows.Forms.DockStyle.Fill
		Me.TabControl2.ImageList = Me.ImageList1
		Me.TabControl2.Location = New System.Drawing.Point(4, 22)
		Me.TabControl2.Margin = New System.Windows.Forms.Padding(4)
		Me.TabControl2.Name = "TabControl2"
		Me.TabControl2.SelectedIndex = 0
		Me.TabControl2.ShowToolTips = True
		Me.TabControl2.Size = New System.Drawing.Size(538, 587)
		Me.TabControl2.TabIndex = 0
		'
		'TabPage5
		'
		Me.TabPage5.Controls.Add(Me.BarChart)
		Me.TabPage5.ImageIndex = 4
		Me.TabPage5.Location = New System.Drawing.Point(4, 25)
		Me.TabPage5.Margin = New System.Windows.Forms.Padding(4)
		Me.TabPage5.Name = "TabPage5"
		Me.TabPage5.Padding = New System.Windows.Forms.Padding(4)
		Me.TabPage5.Size = New System.Drawing.Size(530, 558)
		Me.TabPage5.TabIndex = 0
		Me.TabPage5.Text = "長條圖"
		Me.TabPage5.UseVisualStyleBackColor = True
		'
		'BarChart
		'
		ChartArea7.AxisY.Maximum = 100.0R
		ChartArea7.Name = "ChartArea1"
		Me.BarChart.ChartAreas.Add(ChartArea7)
		Me.BarChart.Dock = System.Windows.Forms.DockStyle.Fill
		LegendItem25.Color = System.Drawing.Color.Blue
		LegendItem25.Name = "一般零件階段"
		Legend25.CustomItems.Add(LegendItem25)
		Legend25.ForeColor = System.Drawing.Color.Blue
		Legend25.Name = "Legend1"
		LegendItem26.Color = System.Drawing.Color.Orange
		LegendItem26.Name = "後製加工階段"
		Legend26.CustomItems.Add(LegendItem26)
		Legend26.ForeColor = System.Drawing.Color.Orange
		Legend26.Name = "Legend2"
		Legend26.Position.Auto = False
		Legend26.Position.Height = 5.282331!
		Legend26.Position.Width = 29.15851!
		Legend26.Position.X = 67.84149!
		Legend26.Position.Y = 7.0!
		LegendItem27.Color = System.Drawing.Color.Red
		LegendItem27.Name = "特殊零件階段"
		Legend27.CustomItems.Add(LegendItem27)
		Legend27.ForeColor = System.Drawing.Color.Red
		Legend27.Name = "Legend3"
		Legend27.Position.Auto = False
		Legend27.Position.Height = 5.282331!
		Legend27.Position.Width = 29.15851!
		Legend27.Position.X = 67.84149!
		Legend27.Position.Y = 11.0!
		LegendItem28.Color = System.Drawing.Color.Green
		LegendItem28.Name = "產品組裝階段"
		Legend28.CustomItems.Add(LegendItem28)
		Legend28.ForeColor = System.Drawing.Color.Green
		Legend28.Name = "Legend4"
		Legend28.Position.Auto = False
		Legend28.Position.Height = 5.282331!
		Legend28.Position.Width = 29.15851!
		Legend28.Position.X = 67.84149!
		Legend28.Position.Y = 15.0!
		Me.BarChart.Legends.Add(Legend25)
		Me.BarChart.Legends.Add(Legend26)
		Me.BarChart.Legends.Add(Legend27)
		Me.BarChart.Legends.Add(Legend28)
		Me.BarChart.Location = New System.Drawing.Point(4, 4)
		Me.BarChart.Margin = New System.Windows.Forms.Padding(4)
		Me.BarChart.Name = "BarChart"
		Me.BarChart.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.None
		Me.BarChart.PaletteCustomColors = New System.Drawing.Color() {System.Drawing.Color.Blue, System.Drawing.Color.Orange, System.Drawing.Color.Red, System.Drawing.Color.Green}
		Series13.ChartArea = "ChartArea1"
		Series13.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.StackedColumn
		Series13.IsVisibleInLegend = False
		Series13.LabelForeColor = System.Drawing.Color.WhiteSmoke
		Series13.Legend = "Legend1"
		Series13.LegendText = "一般零件階段"
		Series13.Name = "Series1"
		DataPoint33.AxisLabel = "WFN"
		DataPoint33.Color = System.Drawing.Color.Blue
		DataPoint33.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!)
		DataPoint33.LabelForeColor = System.Drawing.Color.WhiteSmoke
		DataPoint34.AxisLabel = "ISO 14046"
		DataPoint34.Color = System.Drawing.Color.Blue
		Series13.Points.Add(DataPoint33)
		Series13.Points.Add(DataPoint34)
		Series14.ChartArea = "ChartArea1"
		Series14.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.StackedColumn
		Series14.IsVisibleInLegend = False
		Series14.LabelForeColor = System.Drawing.Color.WhiteSmoke
		Series14.Legend = "Legend1"
		Series14.LegendText = "後製加工階段"
		Series14.Name = "Series2"
		Series14.Points.Add(DataPoint35)
		Series14.Points.Add(DataPoint36)
		Series15.ChartArea = "ChartArea1"
		Series15.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.StackedColumn
		Series15.IsVisibleInLegend = False
		Series15.LabelForeColor = System.Drawing.Color.WhiteSmoke
		Series15.Legend = "Legend1"
		Series15.LegendText = "特殊零件階段"
		Series15.Name = "Series3"
		Series15.Points.Add(DataPoint37)
		Series15.Points.Add(DataPoint38)
		Series16.ChartArea = "ChartArea1"
		Series16.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.StackedColumn
		Series16.IsVisibleInLegend = False
		Series16.LabelForeColor = System.Drawing.Color.WhiteSmoke
		Series16.Legend = "Legend1"
		Series16.LegendText = "產品組裝階段"
		Series16.Name = "Series4"
		DataPoint39.Color = System.Drawing.Color.DarkGreen
		DataPoint40.Color = System.Drawing.Color.DarkGreen
		Series16.Points.Add(DataPoint39)
		Series16.Points.Add(DataPoint40)
		Me.BarChart.Series.Add(Series13)
		Me.BarChart.Series.Add(Series14)
		Me.BarChart.Series.Add(Series15)
		Me.BarChart.Series.Add(Series16)
		Me.BarChart.Size = New System.Drawing.Size(522, 550)
		Me.BarChart.TabIndex = 0
		Me.BarChart.Text = "BarChart"
		'
		'TabPage6
		'
		Me.TabPage6.Controls.Add(Me.ISO_PieChart)
		Me.TabPage6.Controls.Add(Me.PieChart)
		Me.TabPage6.ImageIndex = 5
		Me.TabPage6.Location = New System.Drawing.Point(4, 25)
		Me.TabPage6.Margin = New System.Windows.Forms.Padding(4)
		Me.TabPage6.Name = "TabPage6"
		Me.TabPage6.Padding = New System.Windows.Forms.Padding(4)
		Me.TabPage6.Size = New System.Drawing.Size(530, 558)
		Me.TabPage6.TabIndex = 1
		Me.TabPage6.Text = "圓餅圖"
		Me.TabPage6.UseVisualStyleBackColor = True
		'
		'ISO_PieChart
		'
		ChartArea8.Name = "ChartArea1"
		Me.ISO_PieChart.ChartAreas.Add(ChartArea8)
		Me.ISO_PieChart.Dock = System.Windows.Forms.DockStyle.Fill
		LegendItem29.Color = System.Drawing.Color.Blue
		LegendItem29.Name = "一般零件階段"
		Legend29.CustomItems.Add(LegendItem29)
		Legend29.ForeColor = System.Drawing.Color.Blue
		Legend29.Name = "Legend1"
		LegendItem30.Color = System.Drawing.Color.Orange
		LegendItem30.Name = "後製加工階段"
		Legend30.CustomItems.Add(LegendItem30)
		Legend30.ForeColor = System.Drawing.Color.Orange
		Legend30.Name = "Legend2"
		Legend30.Position.Auto = False
		Legend30.Position.Height = 10.58394!
		Legend30.Position.Width = 29.15851!
		Legend30.Position.X = 67.84149!
		Legend30.Position.Y = 20.11599!
		LegendItem31.Color = System.Drawing.Color.Red
		LegendItem31.Name = "特殊零件階段"
		Legend31.CustomItems.Add(LegendItem31)
		Legend31.ForeColor = System.Drawing.Color.Red
		Legend31.Name = "Legend3"
		Legend31.Position.Auto = False
		Legend31.Position.Height = 10.58394!
		Legend31.Position.Width = 29.15851!
		Legend31.Position.X = 67.84149!
		Legend31.Position.Y = 28.11599!
		LegendItem32.Color = System.Drawing.Color.Green
		LegendItem32.Name = "產品組裝階段"
		Legend32.CustomItems.Add(LegendItem32)
		Legend32.ForeColor = System.Drawing.Color.Green
		Legend32.Name = "Legend4"
		Legend32.Position.Auto = False
		Legend32.Position.Height = 10.58394!
		Legend32.Position.Width = 29.15851!
		Legend32.Position.X = 67.84149!
		Legend32.Position.Y = 36.11599!
		Me.ISO_PieChart.Legends.Add(Legend29)
		Me.ISO_PieChart.Legends.Add(Legend30)
		Me.ISO_PieChart.Legends.Add(Legend31)
		Me.ISO_PieChart.Legends.Add(Legend32)
		Me.ISO_PieChart.Location = New System.Drawing.Point(4, 279)
		Me.ISO_PieChart.Margin = New System.Windows.Forms.Padding(4)
		Me.ISO_PieChart.Name = "ISO_PieChart"
		Me.ISO_PieChart.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.None
		Me.ISO_PieChart.PaletteCustomColors = New System.Drawing.Color() {System.Drawing.Color.Blue, System.Drawing.Color.Orange, System.Drawing.Color.Red, System.Drawing.Color.Green}
		Series17.ChartArea = "ChartArea1"
		Series17.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Pie
		Series17.IsVisibleInLegend = False
		Series17.LabelForeColor = System.Drawing.Color.WhiteSmoke
		Series17.Legend = "Legend1"
		Series17.Name = "Series1"
		DataPoint41.AxisLabel = ""
		DataPoint41.Label = "一般零件階段"
		DataPoint41.LabelToolTip = "33"
		DataPoint41.LegendText = "一般零件階段"
		DataPoint42.AxisLabel = ""
		DataPoint42.Label = "後製加工階段"
		DataPoint42.LabelToolTip = "57"
		DataPoint42.LegendText = "後製加工階段"
		DataPoint42.LegendToolTip = ""
		DataPoint43.AxisLabel = ""
		DataPoint43.Label = "特殊零件階段"
		DataPoint43.LabelToolTip = "10"
		DataPoint43.LegendText = "特殊零件階段"
		DataPoint44.Color = System.Drawing.Color.DarkGreen
		DataPoint44.Label = "產品組裝階段"
		DataPoint44.LegendText = "產品組裝階段"
		Series17.Points.Add(DataPoint41)
		Series17.Points.Add(DataPoint42)
		Series17.Points.Add(DataPoint43)
		Series17.Points.Add(DataPoint44)
		Me.ISO_PieChart.Series.Add(Series17)
		Me.ISO_PieChart.Size = New System.Drawing.Size(522, 275)
		Me.ISO_PieChart.TabIndex = 1
		Me.ISO_PieChart.Text = "Chart1"
		Title5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Bold)
		Title5.Name = "Title1"
		Title5.Text = "ISO 14046"
		Me.ISO_PieChart.Titles.Add(Title5)
		'
		'PieChart
		'
		ChartArea9.Name = "ChartArea1"
		Me.PieChart.ChartAreas.Add(ChartArea9)
		Me.PieChart.Dock = System.Windows.Forms.DockStyle.Top
		LegendItem33.Color = System.Drawing.Color.Blue
		LegendItem33.Name = "一般零件階段"
		Legend33.CustomItems.Add(LegendItem33)
		Legend33.ForeColor = System.Drawing.Color.Blue
		Legend33.Name = "Legend1"
		LegendItem34.Color = System.Drawing.Color.Orange
		LegendItem34.Name = "後製加工階段"
		Legend34.CustomItems.Add(LegendItem34)
		Legend34.ForeColor = System.Drawing.Color.Orange
		Legend34.Name = "Legend2"
		Legend34.Position.Auto = False
		Legend34.Position.Height = 10.58394!
		Legend34.Position.Width = 29.15851!
		Legend34.Position.X = 67.84149!
		Legend34.Position.Y = 20.11599!
		LegendItem35.Color = System.Drawing.Color.Red
		LegendItem35.Name = "特殊零件階段"
		Legend35.CustomItems.Add(LegendItem35)
		Legend35.ForeColor = System.Drawing.Color.Red
		Legend35.Name = "Legend3"
		Legend35.Position.Auto = False
		Legend35.Position.Height = 10.58394!
		Legend35.Position.Width = 29.15851!
		Legend35.Position.X = 67.84149!
		Legend35.Position.Y = 28.11599!
		LegendItem36.Color = System.Drawing.Color.Green
		LegendItem36.Name = "產品組裝階段"
		Legend36.CustomItems.Add(LegendItem36)
		Legend36.ForeColor = System.Drawing.Color.Green
		Legend36.Name = "Legend4"
		Legend36.Position.Auto = False
		Legend36.Position.Height = 10.58394!
		Legend36.Position.Width = 29.15851!
		Legend36.Position.X = 67.84149!
		Legend36.Position.Y = 36.11599!
		Me.PieChart.Legends.Add(Legend33)
		Me.PieChart.Legends.Add(Legend34)
		Me.PieChart.Legends.Add(Legend35)
		Me.PieChart.Legends.Add(Legend36)
		Me.PieChart.Location = New System.Drawing.Point(4, 4)
		Me.PieChart.Margin = New System.Windows.Forms.Padding(4)
		Me.PieChart.Name = "PieChart"
		Me.PieChart.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.None
		Me.PieChart.PaletteCustomColors = New System.Drawing.Color() {System.Drawing.Color.Blue, System.Drawing.Color.Orange, System.Drawing.Color.Red, System.Drawing.Color.Green}
		Series18.ChartArea = "ChartArea1"
		Series18.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Pie
		Series18.IsVisibleInLegend = False
		Series18.LabelForeColor = System.Drawing.Color.WhiteSmoke
		Series18.Legend = "Legend1"
		Series18.Name = "Series1"
		DataPoint45.AxisLabel = ""
		DataPoint45.Label = "一般零件階段"
		DataPoint45.LabelToolTip = "33"
		DataPoint45.LegendText = "一般零件階段"
		DataPoint46.AxisLabel = ""
		DataPoint46.Label = "後製加工階段"
		DataPoint46.LabelToolTip = "57"
		DataPoint46.LegendText = "後製加工階段"
		DataPoint46.LegendToolTip = ""
		DataPoint47.AxisLabel = ""
		DataPoint47.Label = "特殊零件階段"
		DataPoint47.LabelToolTip = "10"
		DataPoint47.LegendText = "特殊零件階段"
		DataPoint48.Color = System.Drawing.Color.DarkGreen
		DataPoint48.Label = "產品組裝階段"
		DataPoint48.LabelToolTip = "5"
		DataPoint48.LegendText = "產品組裝階段"
		Series18.Points.Add(DataPoint45)
		Series18.Points.Add(DataPoint46)
		Series18.Points.Add(DataPoint47)
		Series18.Points.Add(DataPoint48)
		Me.PieChart.Series.Add(Series18)
		Me.PieChart.Size = New System.Drawing.Size(522, 275)
		Me.PieChart.TabIndex = 0
		Me.PieChart.Text = "PieChart"
		Title6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Bold)
		Title6.ForeColor = System.Drawing.Color.Purple
		Title6.Name = "Title1"
		Title6.Text = "WFN"
		Me.PieChart.Titles.Add(Title6)
		'
		'ImageList1
		'
		Me.ImageList1.ImageStream = CType(resources.GetObject("ImageList1.ImageStream"), System.Windows.Forms.ImageListStreamer)
		Me.ImageList1.TransparentColor = System.Drawing.Color.Transparent
		Me.ImageList1.Images.SetKeyName(0, "a_general.ico")
		Me.ImageList1.Images.SetKeyName(1, "b_post_work.ico")
		Me.ImageList1.Images.SetKeyName(2, "c_special.ico")
		Me.ImageList1.Images.SetKeyName(3, "d_analysis_1.ico")
		Me.ImageList1.Images.SetKeyName(4, "bar_chart.ico")
		Me.ImageList1.Images.SetKeyName(5, "pie_chart.ico")
		Me.ImageList1.Images.SetKeyName(6, "assembly_box_32.png")
		'
		'GroupBox12
		'
		Me.GroupBox12.Controls.Add(Me.FlowLayoutPanel8)
		Me.GroupBox12.Location = New System.Drawing.Point(4, 655)
		Me.GroupBox12.Margin = New System.Windows.Forms.Padding(4, 0, 4, 4)
		Me.GroupBox12.Name = "GroupBox12"
		Me.GroupBox12.Padding = New System.Windows.Forms.Padding(4)
		Me.GroupBox12.Size = New System.Drawing.Size(546, 240)
		Me.GroupBox12.TabIndex = 2
		Me.GroupBox12.TabStop = False
		'
		'FlowLayoutPanel8
		'
		Me.FlowLayoutPanel8.Controls.Add(Me.FlowLayoutPanel9)
		Me.FlowLayoutPanel8.Dock = System.Windows.Forms.DockStyle.Fill
		Me.FlowLayoutPanel8.Location = New System.Drawing.Point(4, 22)
		Me.FlowLayoutPanel8.Margin = New System.Windows.Forms.Padding(4)
		Me.FlowLayoutPanel8.Name = "FlowLayoutPanel8"
		Me.FlowLayoutPanel8.Size = New System.Drawing.Size(538, 214)
		Me.FlowLayoutPanel8.TabIndex = 0
		'
		'FlowLayoutPanel9
		'
		Me.FlowLayoutPanel9.Controls.Add(Me.Label62)
		Me.FlowLayoutPanel9.Controls.Add(Me.Label66)
		Me.FlowLayoutPanel9.Controls.Add(Me.Label38)
		Me.FlowLayoutPanel9.Controls.Add(Me.D_GenValTextBox)
		Me.FlowLayoutPanel9.Controls.Add(Me.Label46)
		Me.FlowLayoutPanel9.Controls.Add(Me.D_ISO_1_TextBox)
		Me.FlowLayoutPanel9.Controls.Add(Me.Label67)
		Me.FlowLayoutPanel9.Controls.Add(Me.Label47)
		Me.FlowLayoutPanel9.Controls.Add(Me.D_PostProcValTextBox)
		Me.FlowLayoutPanel9.Controls.Add(Me.Label49)
		Me.FlowLayoutPanel9.Controls.Add(Me.D_ISO_2_TextBox)
		Me.FlowLayoutPanel9.Controls.Add(Me.Label68)
		Me.FlowLayoutPanel9.Controls.Add(Me.Label50)
		Me.FlowLayoutPanel9.Controls.Add(Me.D_SpecialComponentTextBox)
		Me.FlowLayoutPanel9.Controls.Add(Me.Label52)
		Me.FlowLayoutPanel9.Controls.Add(Me.D_ISO_3_TextBox)
		Me.FlowLayoutPanel9.Controls.Add(Me.Label69)
		Me.FlowLayoutPanel9.Controls.Add(Me.Label72)
		Me.FlowLayoutPanel9.Controls.Add(Me.D_WFN_4_TextBox)
		Me.FlowLayoutPanel9.Controls.Add(Me.Label75)
		Me.FlowLayoutPanel9.Controls.Add(Me.D_ISO_4_TextBox)
		Me.FlowLayoutPanel9.Controls.Add(Me.Label82)
		Me.FlowLayoutPanel9.Controls.Add(Me.Label53)
		Me.FlowLayoutPanel9.Controls.Add(Me.D_TotalWFValTextBox)
		Me.FlowLayoutPanel9.Controls.Add(Me.Label54)
		Me.FlowLayoutPanel9.Controls.Add(Me.D_ISO_Total_TextBox)
		Me.FlowLayoutPanel9.Controls.Add(Me.Label70)
		Me.FlowLayoutPanel9.Location = New System.Drawing.Point(4, 4)
		Me.FlowLayoutPanel9.Margin = New System.Windows.Forms.Padding(4)
		Me.FlowLayoutPanel9.Name = "FlowLayoutPanel9"
		Me.FlowLayoutPanel9.Size = New System.Drawing.Size(516, 202)
		Me.FlowLayoutPanel9.TabIndex = 0
		'
		'Label62
		'
		Me.Label62.Font = New System.Drawing.Font("PMingLiU", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
		Me.Label62.ForeColor = System.Drawing.Color.Purple
		Me.Label62.Location = New System.Drawing.Point(3, 0)
		Me.Label62.Name = "Label62"
		Me.Label62.Size = New System.Drawing.Size(220, 22)
		Me.Label62.TabIndex = 13
		Me.Label62.Text = "WFN"
		Me.Label62.TextAlign = System.Drawing.ContentAlignment.BottomRight
		'
		'Label66
		'
		Me.Label66.Font = New System.Drawing.Font("PMingLiU", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
		Me.Label66.ForeColor = System.Drawing.Color.Black
		Me.Label66.Location = New System.Drawing.Point(229, 0)
		Me.Label66.Name = "Label66"
		Me.Label66.Size = New System.Drawing.Size(208, 22)
		Me.Label66.TabIndex = 14
		Me.Label66.Text = "ISO 14046"
		Me.Label66.TextAlign = System.Drawing.ContentAlignment.BottomRight
		'
		'Label38
		'
		Me.Label38.AutoSize = True
		Me.Label38.Font = New System.Drawing.Font("PMingLiU", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
		Me.Label38.ForeColor = System.Drawing.Color.Blue
		Me.Label38.Location = New System.Drawing.Point(4, 32)
		Me.Label38.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label38.Name = "Label38"
		Me.Label38.Size = New System.Drawing.Size(109, 15)
		Me.Label38.TabIndex = 0
		Me.Label38.Text = "一般零件階段 : "
		'
		'D_GenValTextBox
		'
		Me.D_GenValTextBox.Location = New System.Drawing.Point(121, 26)
		Me.D_GenValTextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.D_GenValTextBox.Name = "D_GenValTextBox"
		Me.D_GenValTextBox.Size = New System.Drawing.Size(145, 25)
		Me.D_GenValTextBox.TabIndex = 2
		'
		'Label46
		'
		Me.Label46.AutoSize = True
		Me.Label46.Location = New System.Drawing.Point(274, 32)
		Me.Label46.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label46.Name = "Label46"
		Me.Label46.Size = New System.Drawing.Size(32, 15)
		Me.Label46.TabIndex = 3
		Me.Label46.Text = "m^3"
		'
		'D_ISO_1_TextBox
		'
		Me.D_ISO_1_TextBox.Location = New System.Drawing.Point(314, 26)
		Me.D_ISO_1_TextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.D_ISO_1_TextBox.Name = "D_ISO_1_TextBox"
		Me.D_ISO_1_TextBox.Size = New System.Drawing.Size(145, 25)
		Me.D_ISO_1_TextBox.TabIndex = 12
		'
		'Label67
		'
		Me.Label67.AutoSize = True
		Me.Label67.Location = New System.Drawing.Point(467, 32)
		Me.Label67.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label67.Name = "Label67"
		Me.Label67.Size = New System.Drawing.Size(32, 15)
		Me.Label67.TabIndex = 15
		Me.Label67.Text = "m^3"
		'
		'Label47
		'
		Me.Label47.AutoSize = True
		Me.Label47.Font = New System.Drawing.Font("PMingLiU", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
		Me.Label47.ForeColor = System.Drawing.Color.Orange
		Me.Label47.Location = New System.Drawing.Point(4, 65)
		Me.Label47.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label47.Name = "Label47"
		Me.Label47.Size = New System.Drawing.Size(109, 15)
		Me.Label47.TabIndex = 4
		Me.Label47.Text = "後製加工階段 : "
		'
		'D_PostProcValTextBox
		'
		Me.D_PostProcValTextBox.Location = New System.Drawing.Point(121, 59)
		Me.D_PostProcValTextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.D_PostProcValTextBox.Name = "D_PostProcValTextBox"
		Me.D_PostProcValTextBox.Size = New System.Drawing.Size(145, 25)
		Me.D_PostProcValTextBox.TabIndex = 6
		'
		'Label49
		'
		Me.Label49.AutoSize = True
		Me.Label49.Location = New System.Drawing.Point(274, 65)
		Me.Label49.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label49.Name = "Label49"
		Me.Label49.Size = New System.Drawing.Size(32, 15)
		Me.Label49.TabIndex = 7
		Me.Label49.Text = "m^3"
		'
		'D_ISO_2_TextBox
		'
		Me.D_ISO_2_TextBox.Location = New System.Drawing.Point(314, 59)
		Me.D_ISO_2_TextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.D_ISO_2_TextBox.Name = "D_ISO_2_TextBox"
		Me.D_ISO_2_TextBox.Size = New System.Drawing.Size(145, 25)
		Me.D_ISO_2_TextBox.TabIndex = 16
		'
		'Label68
		'
		Me.Label68.AutoSize = True
		Me.Label68.Location = New System.Drawing.Point(467, 65)
		Me.Label68.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label68.Name = "Label68"
		Me.Label68.Size = New System.Drawing.Size(32, 15)
		Me.Label68.TabIndex = 17
		Me.Label68.Text = "m^3"
		'
		'Label50
		'
		Me.Label50.AutoSize = True
		Me.Label50.Font = New System.Drawing.Font("PMingLiU", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
		Me.Label50.ForeColor = System.Drawing.Color.Red
		Me.Label50.Location = New System.Drawing.Point(4, 98)
		Me.Label50.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label50.Name = "Label50"
		Me.Label50.Size = New System.Drawing.Size(109, 15)
		Me.Label50.TabIndex = 8
		Me.Label50.Text = "特殊零件階段 : "
		'
		'D_SpecialComponentTextBox
		'
		Me.D_SpecialComponentTextBox.Location = New System.Drawing.Point(121, 92)
		Me.D_SpecialComponentTextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.D_SpecialComponentTextBox.Name = "D_SpecialComponentTextBox"
		Me.D_SpecialComponentTextBox.Size = New System.Drawing.Size(145, 25)
		Me.D_SpecialComponentTextBox.TabIndex = 10
		'
		'Label52
		'
		Me.Label52.AutoSize = True
		Me.Label52.Location = New System.Drawing.Point(274, 98)
		Me.Label52.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label52.Name = "Label52"
		Me.Label52.Size = New System.Drawing.Size(32, 15)
		Me.Label52.TabIndex = 11
		Me.Label52.Text = "m^3"
		'
		'D_ISO_3_TextBox
		'
		Me.D_ISO_3_TextBox.Location = New System.Drawing.Point(314, 92)
		Me.D_ISO_3_TextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.D_ISO_3_TextBox.Name = "D_ISO_3_TextBox"
		Me.D_ISO_3_TextBox.Size = New System.Drawing.Size(145, 25)
		Me.D_ISO_3_TextBox.TabIndex = 18
		'
		'Label69
		'
		Me.Label69.AutoSize = True
		Me.Label69.Location = New System.Drawing.Point(467, 98)
		Me.Label69.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label69.Name = "Label69"
		Me.Label69.Size = New System.Drawing.Size(32, 15)
		Me.Label69.TabIndex = 19
		Me.Label69.Text = "m^3"
		'
		'Label72
		'
		Me.Label72.AutoSize = True
		Me.Label72.Font = New System.Drawing.Font("PMingLiU", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
		Me.Label72.ForeColor = System.Drawing.Color.Green
		Me.Label72.Location = New System.Drawing.Point(4, 131)
		Me.Label72.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label72.Name = "Label72"
		Me.Label72.Size = New System.Drawing.Size(109, 15)
		Me.Label72.TabIndex = 22
		Me.Label72.Text = "產品組裝階段 : "
		'
		'D_WFN_4_TextBox
		'
		Me.D_WFN_4_TextBox.Location = New System.Drawing.Point(121, 125)
		Me.D_WFN_4_TextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.D_WFN_4_TextBox.Name = "D_WFN_4_TextBox"
		Me.D_WFN_4_TextBox.Size = New System.Drawing.Size(145, 25)
		Me.D_WFN_4_TextBox.TabIndex = 23
		'
		'Label75
		'
		Me.Label75.AutoSize = True
		Me.Label75.Location = New System.Drawing.Point(274, 131)
		Me.Label75.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label75.Name = "Label75"
		Me.Label75.Size = New System.Drawing.Size(32, 15)
		Me.Label75.TabIndex = 24
		Me.Label75.Text = "m^3"
		'
		'D_ISO_4_TextBox
		'
		Me.D_ISO_4_TextBox.Location = New System.Drawing.Point(314, 125)
		Me.D_ISO_4_TextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.D_ISO_4_TextBox.Name = "D_ISO_4_TextBox"
		Me.D_ISO_4_TextBox.Size = New System.Drawing.Size(145, 25)
		Me.D_ISO_4_TextBox.TabIndex = 25
		'
		'Label82
		'
		Me.Label82.AutoSize = True
		Me.Label82.Location = New System.Drawing.Point(467, 131)
		Me.Label82.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label82.Name = "Label82"
		Me.Label82.Size = New System.Drawing.Size(32, 15)
		Me.Label82.TabIndex = 26
		Me.Label82.Text = "m^3"
		'
		'Label53
		'
		Me.Label53.Font = New System.Drawing.Font("PMingLiU", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
		Me.Label53.Location = New System.Drawing.Point(4, 164)
		Me.Label53.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label53.Name = "Label53"
		Me.Label53.Size = New System.Drawing.Size(109, 15)
		Me.Label53.TabIndex = 0
		Me.Label53.Text = "總水足跡 :"
		'
		'D_TotalWFValTextBox
		'
		Me.D_TotalWFValTextBox.Location = New System.Drawing.Point(121, 158)
		Me.D_TotalWFValTextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.D_TotalWFValTextBox.Name = "D_TotalWFValTextBox"
		Me.D_TotalWFValTextBox.Size = New System.Drawing.Size(145, 25)
		Me.D_TotalWFValTextBox.TabIndex = 3
		'
		'Label54
		'
		Me.Label54.AutoSize = True
		Me.Label54.Location = New System.Drawing.Point(274, 164)
		Me.Label54.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label54.Name = "Label54"
		Me.Label54.Size = New System.Drawing.Size(32, 15)
		Me.Label54.TabIndex = 4
		Me.Label54.Text = "m^3"
		'
		'D_ISO_Total_TextBox
		'
		Me.D_ISO_Total_TextBox.Location = New System.Drawing.Point(314, 158)
		Me.D_ISO_Total_TextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.D_ISO_Total_TextBox.Name = "D_ISO_Total_TextBox"
		Me.D_ISO_Total_TextBox.Size = New System.Drawing.Size(145, 25)
		Me.D_ISO_Total_TextBox.TabIndex = 20
		'
		'Label70
		'
		Me.Label70.AutoSize = True
		Me.Label70.Location = New System.Drawing.Point(467, 164)
		Me.Label70.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label70.Name = "Label70"
		Me.Label70.Size = New System.Drawing.Size(32, 15)
		Me.Label70.TabIndex = 21
		Me.Label70.Text = "m^3"
		'
		'D_ChoiceProductButton
		'
		Me.D_ChoiceProductButton.Location = New System.Drawing.Point(8, 900)
		Me.D_ChoiceProductButton.Margin = New System.Windows.Forms.Padding(8, 1, 4, 4)
		Me.D_ChoiceProductButton.Name = "D_ChoiceProductButton"
		Me.D_ChoiceProductButton.Size = New System.Drawing.Size(535, 40)
		Me.D_ChoiceProductButton.TabIndex = 3
		Me.D_ChoiceProductButton.Text = "選擇專案"
		Me.D_ChoiceProductButton.UseVisualStyleBackColor = True
		'
		'MainForm
		'
		Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 15.0!)
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.ClientSize = New System.Drawing.Size(564, 1041)
		Me.Controls.Add(Me.WFETabControl)
		Me.Controls.Add(Me.StatusStrip1)
		Me.Controls.Add(Me.MenuStrip1)
		Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
		Me.MainMenuStrip = Me.MenuStrip1
		Me.Margin = New System.Windows.Forms.Padding(4)
		Me.MinimumSize = New System.Drawing.Size(542, 1078)
		Me.Name = "MainForm"
		Me.Text = "Water Footprint Evaluator"
		Me.TopMost = True
		Me.MenuStrip1.ResumeLayout(False)
		Me.MenuStrip1.PerformLayout()
		Me.StatusStrip1.ResumeLayout(False)
		Me.StatusStrip1.PerformLayout()
		Me.WFETabControl.ResumeLayout(False)
		Me.TabPage1.ResumeLayout(False)
		Me.FlowLayoutPanel1.ResumeLayout(False)
		Me.GroupBox1.ResumeLayout(False)
		Me.GroupBox1.PerformLayout()
		Me.GroupBox2.ResumeLayout(False)
		Me.GroupBox2.PerformLayout()
		Me.GroupBox3.ResumeLayout(False)
		Me.GroupBox3.PerformLayout()
		Me.GroupBox4.ResumeLayout(False)
		CType(Me.a_DataGridView, System.ComponentModel.ISupportInitialize).EndInit()
		Me.Panel1.ResumeLayout(False)
		Me.Panel1.PerformLayout()
		Me.TabPage2.ResumeLayout(False)
		Me.FlowLayoutPanel2.ResumeLayout(False)
		Me.GroupBox5.ResumeLayout(False)
		CType(Me.B_ChoiceComponent_DataGridView, System.ComponentModel.ISupportInitialize).EndInit()
		Me.Panel3.ResumeLayout(False)
		Me.Panel3.PerformLayout()
		Me.FlowLayoutPanel3.ResumeLayout(False)
		Me.FlowLayoutPanel3.PerformLayout()
		Me.GroupBox6.ResumeLayout(False)
		Me.GroupBox6.PerformLayout()
		Me.GroupBox7.ResumeLayout(False)
		Me.GroupBox7.PerformLayout()
		Me.GroupBox8.ResumeLayout(False)
		CType(Me.B_Component_Post_Process_DataGridView, System.ComponentModel.ISupportInitialize).EndInit()
		Me.Panel2.ResumeLayout(False)
		Me.Panel2.PerformLayout()
		Me.TabPage3.ResumeLayout(False)
		Me.FlowLayoutPanel4.ResumeLayout(False)
		Me.GroupBox9.ResumeLayout(False)
		Me.FlowLayoutPanel6.ResumeLayout(False)
		Me.FlowLayoutPanel6.PerformLayout()
		Me.C_Area_Panel.ResumeLayout(False)
		Me.C_Area_Panel.PerformLayout()
		Me.C_Volumn_Panel.ResumeLayout(False)
		Me.C_Volumn_Panel.PerformLayout()
		Me.C_Density_Panel.ResumeLayout(False)
		Me.C_Density_Panel.PerformLayout()
		Me.C_Mass_Panel.ResumeLayout(False)
		Me.C_Mass_Panel.PerformLayout()
		Me.D_ProcessWFE_Panel.ResumeLayout(False)
		Me.D_ProcessWFE_Panel.PerformLayout()
		Me.Panel5.ResumeLayout(False)
		Me.Panel5.PerformLayout()
		Me.FlowLayoutPanel5.ResumeLayout(False)
		Me.FlowLayoutPanel5.PerformLayout()
		Me.GroupBox10.ResumeLayout(False)
		CType(Me.c_Special_Components_DataGridView, System.ComponentModel.ISupportInitialize).EndInit()
		Me.Panel4.ResumeLayout(False)
		Me.Panel4.PerformLayout()
		Me.TabPage7.ResumeLayout(False)
		Me.product_assembly_tabpage_FlowLayoutPanel.ResumeLayout(False)
		Me.GroupBox13.ResumeLayout(False)
		Me.FlowLayoutPanel10.ResumeLayout(False)
		Me.FlowLayoutPanel10.PerformLayout()
		CType(Me.assemblyDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
		Me.Panel6.ResumeLayout(False)
		Me.Panel6.PerformLayout()
		Me.GroupBox14.ResumeLayout(False)
		Me.FlowLayoutPanel11.ResumeLayout(False)
		Me.Panel8.ResumeLayout(False)
		Me.Panel8.PerformLayout()
		Me.GroupBox15.ResumeLayout(False)
		CType(Me.assembly_record_DataGridView, System.ComponentModel.ISupportInitialize).EndInit()
		Me.Panel7.ResumeLayout(False)
		Me.Panel7.PerformLayout()
		Me.TabPage4.ResumeLayout(False)
		Me.FlowLayoutPanel7.ResumeLayout(False)
		Me.GroupBox11.ResumeLayout(False)
		Me.TabControl2.ResumeLayout(False)
		Me.TabPage5.ResumeLayout(False)
		CType(Me.BarChart, System.ComponentModel.ISupportInitialize).EndInit()
		Me.TabPage6.ResumeLayout(False)
		CType(Me.ISO_PieChart, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.PieChart, System.ComponentModel.ISupportInitialize).EndInit()
		Me.GroupBox12.ResumeLayout(False)
		Me.FlowLayoutPanel8.ResumeLayout(False)
		Me.FlowLayoutPanel9.ResumeLayout(False)
		Me.FlowLayoutPanel9.PerformLayout()
		Me.ResumeLayout(False)
		Me.PerformLayout()

	End Sub
	Friend WithEvents MenuStrip1 As MenuStrip
	Friend WithEvents FileToolStripMenuItem As ToolStripMenuItem
	Friend WithEvents NewToolStripMenuItem As ToolStripMenuItem
	Friend WithEvents OpenToolStripMenuItem As ToolStripMenuItem
	Friend WithEvents toolStripSeparator As ToolStripSeparator
	Friend WithEvents SaveToolStripMenuItem As ToolStripMenuItem
	Friend WithEvents SettiingsToolStripMenuItem As ToolStripMenuItem
	Friend WithEvents StatusStrip1 As StatusStrip
	Friend WithEvents ProductName_StatusLabel As ToolStripStatusLabel
	Friend WithEvents WFETabControl As TabControl
	Friend WithEvents TabPage1 As TabPage
	Friend WithEvents FlowLayoutPanel1 As FlowLayoutPanel
	Friend WithEvents GroupBox1 As GroupBox
	Friend WithEvents Label8 As Label
	Friend WithEvents Label7 As Label
	Friend WithEvents Label6 As Label
	Friend WithEvents a_Componet_Mass_TextBox As TextBox
	Friend WithEvents Label5 As Label
	Friend WithEvents a_Material_Density_TextBox As TextBox
	Friend WithEvents Label4 As Label
	Friend WithEvents a_VolumnTextBox As TextBox
	Friend WithEvents Label3 As Label
	Friend WithEvents A_ChoiceMaterialButton As Button
	Friend WithEvents a_Material_TextBox As TextBox
	Friend WithEvents Label2 As Label
	Friend WithEvents A_CaptureComponentButton As Button
	Friend WithEvents a_ComponnetNameTextBox As TextBox
	Friend WithEvents Label1 As Label
	Friend WithEvents GroupBox2 As GroupBox
	Friend WithEvents A_ChoiceTransportButton As Button
	Friend WithEvents GroupBox3 As GroupBox
	Friend WithEvents A_ChoiceMakeProcButton As Button
	Friend WithEvents GroupBox4 As GroupBox
	Friend WithEvents TabPage2 As TabPage
	Friend WithEvents TabPage3 As TabPage
	Friend WithEvents TabPage4 As TabPage
	Friend WithEvents Panel1 As Panel
	Friend WithEvents Label12 As Label
	Friend WithEvents a_TotalWFP_TextBox As TextBox
	Friend WithEvents Label11 As Label
	Friend WithEvents a_Transport_Sum_WFP_Label As Label
	Friend WithEvents Label22 As Label
	Friend WithEvents a_Process_Sum_WFP_Label As Label
	Friend WithEvents Label20 As Label
	Friend WithEvents FlowLayoutPanel2 As FlowLayoutPanel
	Friend WithEvents GroupBox5 As GroupBox
	Friend WithEvents FlowLayoutPanel3 As FlowLayoutPanel
	Friend WithEvents Label24 As Label
	Friend WithEvents B_Name_Filter_TextBox As TextBox
	Friend WithEvents B_CleanFilterButton As Button
	Friend WithEvents GroupBox6 As GroupBox
	Friend WithEvents GroupBox7 As GroupBox
	Friend WithEvents GroupBox8 As GroupBox
	Friend WithEvents Panel2 As Panel
	Friend WithEvents B_ChoiceTransportButton As Button
	Friend WithEvents B_ChoiceMakeProcessButton As Button
	Friend WithEvents Label35 As Label
	Friend WithEvents B_Component_Post_Process_WFP_TextBox As TextBox
	Friend WithEvents Label36 As Label
	Friend WithEvents FlowLayoutPanel4 As FlowLayoutPanel
	Friend WithEvents GroupBox9 As GroupBox
	Friend WithEvents A_CompletedButton As Button
	Friend WithEvents B_CompletedButton As Button
	Friend WithEvents FlowLayoutPanel5 As FlowLayoutPanel
	Friend WithEvents EBoardRadioButton As RadioButton
	Friend WithEvents EComopnentRadioButton As RadioButton
	Friend WithEvents LCDRadioButton As RadioButton
	Friend WithEvents FlowLayoutPanel6 As FlowLayoutPanel
	Friend WithEvents Label44 As Label
	Friend WithEvents C_Component_Name_TextBox As TextBox
	Friend WithEvents C_CaptureComponentButton As Button
	Friend WithEvents Label45 As Label
	Friend WithEvents C_Manufacture_Name_TextBox As TextBox
	Friend WithEvents C_ChoiceMakeProcessButton As Button
	Friend WithEvents C_AreaTitleLabel As Label
	Friend WithEvents C_AreaTextBox As TextBox
	Friend WithEvents C_AreaLabel As Label
	Friend WithEvents C_VolumnTitleLabel As Label
	Friend WithEvents C_VolumnTextBox As TextBox
	Friend WithEvents C_VolumnLabel As Label
	Friend WithEvents Label39 As Label
	Friend WithEvents C_Manufacture_Density_TextBox As TextBox
	Friend WithEvents Label40 As Label
	Friend WithEvents GroupBox10 As GroupBox
	Friend WithEvents Panel4 As Panel
	Friend WithEvents Label41 As Label
	Friend WithEvents C_Total_WFP_TextBox As TextBox
	Friend WithEvents Label42 As Label
	Friend WithEvents C_CompletedButton As Button
	Friend WithEvents EditToolStripMenuItem As ToolStripMenuItem
	Friend WithEvents DBToolStripMenuItem As ToolStripMenuItem
	Friend WithEvents C_DensityTitleLabel As Label
	Friend WithEvents C_DensityLabel As Label
	Friend WithEvents C_MassTitleLabel As Label
	Friend WithEvents C_MassTextBox As TextBox
	Friend WithEvents C_MassLabel As Label
	Friend WithEvents C_Manufacture_WFP_TextBox As TextBox
	Friend WithEvents C_Area_Panel As Panel
	Friend WithEvents C_Volumn_Panel As Panel
	Friend WithEvents C_Density_Panel As Panel
	Friend WithEvents C_Mass_Panel As Panel
	Friend WithEvents D_ProcessWFE_Panel As Panel
	Friend WithEvents TransportToolStripMenuItem As ToolStripMenuItem
	Friend WithEvents MaterialToolStripMenuItem As ToolStripMenuItem
	Friend WithEvents MakeProcessToolStripMenuItem As ToolStripMenuItem
	Friend WithEvents SPMakeProcessToolStripMenuItem As ToolStripMenuItem
	Friend WithEvents FlowLayoutPanel7 As FlowLayoutPanel
	Friend WithEvents Label37 As Label
	Friend WithEvents GroupBox11 As GroupBox
	Friend WithEvents TabControl2 As TabControl
	Friend WithEvents TabPage5 As TabPage
	Friend WithEvents BarChart As DataVisualization.Charting.Chart
	Friend WithEvents TabPage6 As TabPage
	Friend WithEvents PieChart As DataVisualization.Charting.Chart
	Friend WithEvents GroupBox12 As GroupBox
	Friend WithEvents FlowLayoutPanel8 As FlowLayoutPanel
	Friend WithEvents FlowLayoutPanel9 As FlowLayoutPanel
	Friend WithEvents Label38 As Label
	Friend WithEvents D_GenValTextBox As TextBox
	Friend WithEvents Label46 As Label
	Friend WithEvents Label47 As Label
	Friend WithEvents D_PostProcValTextBox As TextBox
	Friend WithEvents Label49 As Label
	Friend WithEvents Label50 As Label
	Friend WithEvents D_SpecialComponentTextBox As TextBox
	Friend WithEvents Label52 As Label
	Friend WithEvents Label54 As Label
	Friend WithEvents D_TotalWFValTextBox As TextBox
	Friend WithEvents Label53 As Label
	Friend WithEvents a_Component_WFP_Label As Label
	Friend WithEvents Label15 As Label
	Friend WithEvents Label9 As Label
	Friend WithEvents a_Material_WFP_TextBox As TextBox
	Friend WithEvents Label13 As Label
	Friend WithEvents b_post_transport_sum_wfp_Label As Label
	Friend WithEvents Label16 As Label
	Friend WithEvents b_post_process_sum_wfp_Label As Label
	Friend WithEvents Label18 As Label
	Friend WithEvents a_DataGridView As DataGridView
	Friend WithEvents a_delete_Button As Button
	Friend WithEvents B_Component_Post_Process_DataGridView As DataGridView
	Friend WithEvents B_Delete_Component_Post_Process_Button As Button
	Friend WithEvents Panel5 As Panel
	Friend WithEvents C_Component_WFP_Label As Label
	Friend WithEvents Label25 As Label
	Friend WithEvents B_ChoiceComponent_DataGridView As DataGridView
	Friend WithEvents Panel3 As Panel
	Friend WithEvents B_Choice_Component_TextBox As TextBox
	Friend WithEvents Label23 As Label
	Friend WithEvents c_Special_Components_DataGridView As DataGridView
	Friend WithEvents C_Delete_Button As Button
	Friend WithEvents a_Componet_Calculate_Button As Button
	Friend WithEvents c_add_component_Button As Button
	Friend WithEvents ImageList1 As ImageList
	Friend WithEvents ToolStripStatusLabel1 As ToolStripStatusLabel
	Friend WithEvents ProductCompareToolStripMenuItem As ToolStripMenuItem
	Friend WithEvents CalculateToolStripMenuItem As ToolStripMenuItem
	Friend WithEvents ProductComparisonToolStripMenuItem As ToolStripMenuItem
	Friend WithEvents b_AddComponent_Button As Button
	Friend WithEvents Label17 As Label
	Friend WithEvents Label10 As Label
	Friend WithEvents a_Material_ISO_TextBox As TextBox
	Friend WithEvents Label14 As Label
	Friend WithEvents a_Component_ISO_Label As Label
	Friend WithEvents Label21 As Label
	Friend WithEvents Label19 As Label
	Friend WithEvents Label28 As Label
	Friend WithEvents Label27 As Label
	Friend WithEvents a_Transport_Sum_ISO_Label As Label
	Friend WithEvents Label32 As Label
	Friend WithEvents Label31 As Label
	Friend WithEvents a_Process_Sum_ISO_Label As Label
	Friend WithEvents Label30 As Label
	Friend WithEvents a_Total_ISO_TextBox As TextBox
	Friend WithEvents Label29 As Label
	Friend WithEvents Label26 As Label
	Friend WithEvents b_post_transport_sum_iso_Label As Label
	Friend WithEvents Label34 As Label
	Friend WithEvents Label43 As Label
	Friend WithEvents b_post_process_sum_iso_Label As Label
	Friend WithEvents Label51 As Label
	Friend WithEvents Label55 As Label
	Friend WithEvents Label59 As Label
	Friend WithEvents B_Component_Post_Process_ISO_TextBox As TextBox
	Friend WithEvents Label57 As Label
	Friend WithEvents Label58 As Label
	Friend WithEvents Label56 As Label
	Friend WithEvents C_Manufacture_ISO_TextBox As TextBox
	Friend WithEvents Label33 As Label
	Friend WithEvents Label48 As Label
	Friend WithEvents C_Component_ISO_Label As Label
	Friend WithEvents Label60 As Label
	Friend WithEvents Label61 As Label
	Friend WithEvents Label65 As Label
	Friend WithEvents C_Total_ISO_TextBox As TextBox
	Friend WithEvents Label63 As Label
	Friend WithEvents Label64 As Label
	Friend WithEvents Label62 As Label
	Friend WithEvents Label66 As Label
	Friend WithEvents D_ISO_1_TextBox As TextBox
	Friend WithEvents Label67 As Label
	Friend WithEvents D_ISO_2_TextBox As TextBox
	Friend WithEvents Label68 As Label
	Friend WithEvents D_ISO_3_TextBox As TextBox
	Friend WithEvents Label69 As Label
	Friend WithEvents D_ISO_Total_TextBox As TextBox
	Friend WithEvents Label70 As Label
	Friend WithEvents ISO_PieChart As DataVisualization.Charting.Chart
	Friend WithEvents TabPage7 As TabPage
	Friend WithEvents product_assembly_tabpage_FlowLayoutPanel As FlowLayoutPanel
	Friend WithEvents GroupBox13 As GroupBox
	Friend WithEvents FlowLayoutPanel10 As FlowLayoutPanel
	Friend WithEvents assemblyDataGridView As DataGridView
	Friend WithEvents Label71 As Label
	Friend WithEvents assembly_choiceTextBox As TextBox
	Friend WithEvents assembly_iso_Label As Label
	Friend WithEvents Label73 As Label
	Friend WithEvents Label74 As Label
	Friend WithEvents assembly_wfn_Label As Label
	Friend WithEvents Label76 As Label
	Friend WithEvents assembly_choice_transport_Button As Button
	Friend WithEvents GroupBox14 As GroupBox
	Friend WithEvents FlowLayoutPanel11 As FlowLayoutPanel
	Friend WithEvents Panel6 As Panel
	Friend WithEvents add_assembly_Button As Button
	Friend WithEvents GroupBox15 As GroupBox
	Friend WithEvents assembly_record_DataGridView As DataGridView
	Friend WithEvents Panel7 As Panel
	Friend WithEvents Label77 As Label
	Friend WithEvents assembly_total_iso_TextBox As TextBox
	Friend WithEvents Label78 As Label
	Friend WithEvents Label79 As Label
	Friend WithEvents Label80 As Label
	Friend WithEvents assembly_total_wfn_TextBox As TextBox
	Friend WithEvents Label81 As Label
	Friend WithEvents assembly_stage_finish_Button As Button
	Friend WithEvents AssemblyMethodToolStripMenuItem As ToolStripMenuItem
	Friend WithEvents assembly_TreeView As TreeView
	Friend WithEvents assembly_tree_toggle_Button As Button
	Friend WithEvents function_ok_Button As Button
	Friend WithEvents enable_merge_CheckBox As CheckBox
	Friend WithEvents enable_disassembly_CheckBox As CheckBox
	Friend WithEvents Label72 As Label
	Friend WithEvents D_WFN_4_TextBox As TextBox
	Friend WithEvents Label75 As Label
	Friend WithEvents D_ISO_4_TextBox As TextBox
	Friend WithEvents Label82 As Label
	Friend WithEvents SaveAsToolStripMenuItem As ToolStripMenuItem
	Friend WithEvents Label84 As Label
	Friend WithEvents Label83 As Label
	Friend WithEvents Label86 As Label
	Friend WithEvents Label85 As Label
	Friend WithEvents Label88 As Label
	Friend WithEvents Label87 As Label
	Friend WithEvents Label90 As Label
	Friend WithEvents Label89 As Label
	Friend WithEvents Label92 As Label
	Friend WithEvents Label91 As Label
	Friend WithEvents Label94 As Label
	Friend WithEvents Label93 As Label
	Friend WithEvents Label96 As Label
	Friend WithEvents Label95 As Label
	Friend WithEvents HelpToolStripMenuItem As ToolStripMenuItem
	Friend WithEvents CopyrightInfoToolStripMenuItem As ToolStripMenuItem
	Friend WithEvents Panel8 As Panel
	Friend WithEvents Label97 As Label
	Friend WithEvents Label98 As Label
	Friend WithEvents asm_stage_iso_Label As Label
	Friend WithEvents Label100 As Label
	Friend WithEvents Label101 As Label
	Friend WithEvents asm_stage_wfn_Label As Label
	Friend WithEvents Label103 As Label
	Friend WithEvents D_ChoiceProductButton As Button
End Class
