﻿Imports WFE.Assistants.Controllers

Public Class ManufactureProcedureForm
	Private _dc As DataController
	Private _viewController As ManufactureProcedureFormController
	Public Sub New(dc As DataController)
		Me._dc = dc
		InitializeComponent()
	End Sub

	Private Sub SPMakeProcessForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
		Product_Info_GroupBox.Text = "專案:" + _dc.ProductDTO.ProductName
		Material_Name_Label.Text = _dc.StageCViewModel.Component_Name
		'====
		_viewController = New ManufactureProcedureFormController()
		_viewController.Setup(NameFilter_TextBox,
							  RecentUsed_ComboBox, Result_DataGridView,
							  MakeProcessName_TextBox,
							  MakeProcess_WFP_TextBox,
							  MakeProcess_ISO_TextBox,
							  Filter_Info_Label, _dc.StageCViewModel, Density_TextBox)
	End Sub

	Private Sub CompletedButton_Click(sender As Object, e As EventArgs) Handles CompletedButton.Click
		'完成
		If (_viewController.GetSelectedModel() IsNot Nothing) Then
			Dim success = _viewController.SaveChoiceItem()
			If success = False Then
				MsgBox(_viewController.GetErrMsg())
			Else
				'close
				_dc.SelectedManufactureProcedureModel = _viewController.GetSelectedModel()
				DialogResult = DialogResult.OK
				Close()
			End If
		Else
			MsgBox("請選擇材料")
		End If
	End Sub

	Private Sub NameFilter_TextBox_TextChanged(sender As Object, e As EventArgs) Handles NameFilter_TextBox.TextChanged
		_viewController.Filter(NameFilter_TextBox.Text)
	End Sub

	Private Sub Clean_Filter_Button_Click(sender As Object, e As EventArgs) Handles Clean_Filter_Button.Click
		NameFilter_TextBox.Text = ""
	End Sub
End Class