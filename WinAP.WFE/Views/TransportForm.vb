﻿Imports System
Imports WFE.Assistants.Controllers
Imports WFE.Assistants.Models
Imports WFE.Assistants.Repos

Public Class TransportForm
	Private _panelInfo As TransportFormInfoDTO
	Private _viewController As TransportFormController
	Public Sub UseModel(panelInfo As TransportFormInfoDTO)
		_panelInfo = panelInfo
		setup()
	End Sub
	Private Sub setup()
		'Setup controller
		_viewController = New TransportFormController(NameFilter_TextBox,
														RecentUsed_ComboBox,
													  Filter_Info_Label,
													  Transport_DataGridView,
													  Selected_Name_TextBox,
													  Transport_WFP_TextBox,
													  Transport_ISO_TextBox,
													  Transport_Distance_TextBox,
													  WFP_Label,
													  ISO_Label,
													  Transport_Records_DataGridView)

		_viewController.Initial(_panelInfo.Component)
	End Sub
	Private Sub TransportForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
		'Assign value
		Me.Text = _panelInfo.FormText
		CaptionName_Label.Text = _panelInfo.C_Name
		CaptionMass_Label.Text = _panelInfo.C_Mass

		'
		Product_Info_GroupBox.Text = "專案:" + _panelInfo.ProductName
		Component_Name_Label.Text = _panelInfo.Component.Component_Name
		Component_Mass_Label.Text = _panelInfo.Component.Mass

		'Focus
		NameFilter_TextBox.Focus()
	End Sub

	Private Sub Clean_Filter_Button_Click(sender As Object, e As EventArgs) Handles Clean_Filter_Button.Click
		NameFilter_TextBox.Text = ""
	End Sub

	Private Sub AddTransport_Button_Click(sender As Object, e As EventArgs) Handles AddTransport_Button.Click
		'save recent used
		Dim addRecentUsedSuccess = _viewController.SaveRecentUsedItem()
		If addRecentUsedSuccess = False Then
			Dim errMsg = _viewController.GetErrMsg()
			MsgBox("加入最近使用運輸工具失敗！ 原因: " + errMsg)
		End If

		'add Stage A Transport Records
		Dim addTransportRecordSuccess = _viewController.AddTransportRecord()
		If addTransportRecordSuccess = False Then
			MsgBox("加入記錄失敗！ 原因: " + _viewController.GetErrMsg())
		End If

	End Sub

	Private Sub DeleteRecord_Button_Click(sender As Object, e As EventArgs) Handles DeleteRecord_Button.Click
		'刪除運輸記錄
		_viewController.DeleteTransportRecord()
	End Sub

	Private Sub Completed_Button_Click(sender As Object, e As EventArgs) Handles Completed_Button.Click
		'
		Me.Close()
	End Sub
End Class