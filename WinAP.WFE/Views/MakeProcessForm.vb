﻿Imports WFE.Assistants.Controllers
Imports WFE.Assistants.Models

Public Class MakeProcessForm
	Private _panelInfo As MakeProcessFormInfoDTO
	Private _viewController As MakeProcessFormController

	Friend Sub UseModel(dataInfo As MakeProcessFormInfoDTO)
		_panelInfo = dataInfo
		setup()
	End Sub
	Private Sub setup()
		'Setup controller
		_viewController = New MakeProcessFormController(Name_Filter_TextBox,
														Recent_Used_ComboBox,
														Filter_Info_Label,
														Process_DataGridView,
														Process_Name_TextBox,
														Process_WFP_TextBox,
														COEEF_TextBox,
														Process_Power_Consumption_TextBox,
														WorkingHour_TextBox,
														WFP_Label,
														ISO_Label,
														Record_DataGridView,
														Mass_FlowLayoutPanel,
														Power_FlowLayoutPanel)

		_viewController.Initial(_panelInfo.Component)
	End Sub
	Private Sub MakeProcessForm_Load(sender As Object, e As System.EventArgs) Handles MyBase.Load
		'Assign
		Me.Text = _panelInfo.FormText
		PanelInfo_GroupBox.Text = _panelInfo.ProductText + ":" + _panelInfo.ProductName
		PanelInfo_Component_Label.Text = _panelInfo.C_Text
		Mass_Label.Text = _panelInfo.Mass_Text

		Component_Name_Label.Text = _panelInfo.Component.Component_Name
		Mass_Value_Label.Text = _panelInfo.Component.Mass

		CalcComponent_WFP_Label.Text = _panelInfo.CalcWFP_Text
	End Sub

	Private Sub Add_Button_Click(sender As Object, e As System.EventArgs) Handles Add_Button.Click
		'save recent used
		Dim addRecentUsedSuccess = _viewController.SaveRecentUsedItem()
		If addRecentUsedSuccess = False Then
			Dim errMsg = _viewController.GetErrMsg()
			MsgBox("加入最近使用項目失敗！ 原因: " + errMsg)
		End If

		'add Records
		Dim addRecordSuccess = _viewController.AddRecord()
		If addRecordSuccess = False Then
			MsgBox("加入記錄失敗！ 原因: " + _viewController.GetErrMsg())
		End If
	End Sub

	Private Sub Delete_Button_Click(sender As Object, e As System.EventArgs) Handles Delete_Button.Click
		_viewController.DeleteRecord()
	End Sub

	Private Sub Completed_Button_Click(sender As Object, e As System.EventArgs) Handles Completed_Button.Click
		Me.Close()
	End Sub

	Private Sub Clean_Filter_Button_Click(sender As Object, e As System.EventArgs) Handles Clean_Filter_Button.Click
		'Clean Filter
		Name_Filter_TextBox.Text = ""
	End Sub
End Class