﻿Imports WFE.Assistants.Repos
Public Class DBSettingsForm
	'===== private variable ===
	Private _repo As ConfigRepo

	Public Sub New(configValue As String)
		_repo = New ConfigRepo(configValue)
		InitializeComponent()
	End Sub

	Private Sub DBSettingsForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
		If _repo.IsValid Then
			DBSource_TextBox.Text = _repo.GetDBSource()
		End If
	End Sub

	Private Sub BrowserFile_Button_Click(sender As Object, e As EventArgs) Handles BrowserFile_Button.Click
		'設定開啟檔案瀏覽對話視窗
		Dim ofd = New OpenFileDialog()
		ofd.InitialDirectory = "c:\"
		ofd.Filter = "Microsoft Office Access files (*.mdb)|*.mdb"
		ofd.FilterIndex = 1
		ofd.RestoreDirectory = True

		'已選定檔案
		If ofd.ShowDialog() = System.Windows.Forms.DialogResult.OK Then
			'ofd.FileName
			DBSource_TextBox.Text = ofd.FileName
		End If
	End Sub

	Private Sub ConnectionTest_Button_Click(sender As Object, e As EventArgs) Handles ConnectionTest_Button.Click
		Dim result = _repo.DBConnectionTest(DBSource_TextBox.Text)
		If result = False Then
			MsgBox(_repo.GetErrMsg())
		Else
			MsgBox("連接成功")
		End If
	End Sub

	Private Sub SaveChange_Button_Click(sender As Object, e As EventArgs) Handles SaveChange_Button.Click
		Dim result = _repo.SetDBSource(DBSource_TextBox.Text)
		If result = False Then
			MsgBox(_repo.GetErrMsg())
		Else
			MsgBox("完成設定")
			Me.Close()
		End If
	End Sub


End Class