﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class MakeProcessForm
	Inherits System.Windows.Forms.Form

	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()>
	Protected Overrides Sub Dispose(ByVal disposing As Boolean)
		Try
			If disposing AndAlso components IsNot Nothing Then
				components.Dispose()
			End If
		Finally
			MyBase.Dispose(disposing)
		End Try
	End Sub

	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer

	'NOTE: The following procedure is required by the Windows Form Designer
	'It can be modified using the Windows Form Designer.  
	'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()>
	Private Sub InitializeComponent()
		Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(MakeProcessForm))
		Me.Completed_Button = New System.Windows.Forms.Button()
		Me.FlowLayoutPanel2 = New System.Windows.Forms.FlowLayoutPanel()
		Me.Label5 = New System.Windows.Forms.Label()
		Me.Name_Filter_TextBox = New System.Windows.Forms.TextBox()
		Me.Clean_Filter_Button = New System.Windows.Forms.Button()
		Me.Component_Name_Label = New System.Windows.Forms.Label()
		Me.PanelInfo_Component_Label = New System.Windows.Forms.Label()
		Me.PanelInfo_GroupBox = New System.Windows.Forms.GroupBox()
		Me.Mass_Value_Label = New System.Windows.Forms.Label()
		Me.Mass_Label = New System.Windows.Forms.Label()
		Me.GroupBox2 = New System.Windows.Forms.GroupBox()
		Me.Add_Button = New System.Windows.Forms.Button()
		Me.FlowLayoutPanel4 = New System.Windows.Forms.FlowLayoutPanel()
		Me.Mass_FlowLayoutPanel = New System.Windows.Forms.FlowLayoutPanel()
		Me.Label6 = New System.Windows.Forms.Label()
		Me.Process_Name_TextBox = New System.Windows.Forms.TextBox()
		Me.Label8 = New System.Windows.Forms.Label()
		Me.Process_WFP_TextBox = New System.Windows.Forms.TextBox()
		Me.Label9 = New System.Windows.Forms.Label()
		Me.Power_FlowLayoutPanel = New System.Windows.Forms.FlowLayoutPanel()
		Me.Label15 = New System.Windows.Forms.Label()
		Me.Process_Power_Consumption_TextBox = New System.Windows.Forms.TextBox()
		Me.Label16 = New System.Windows.Forms.Label()
		Me.Label11 = New System.Windows.Forms.Label()
		Me.COEEF_TextBox = New System.Windows.Forms.TextBox()
		Me.Label2 = New System.Windows.Forms.Label()
		Me.Label12 = New System.Windows.Forms.Label()
		Me.WorkingHour_TextBox = New System.Windows.Forms.TextBox()
		Me.Label3 = New System.Windows.Forms.Label()
		Me.CalcComponent_WFP_Label = New System.Windows.Forms.Label()
		Me.Label27 = New System.Windows.Forms.Label()
		Me.WFP_Label = New System.Windows.Forms.Label()
		Me.Label28 = New System.Windows.Forms.Label()
		Me.Label4 = New System.Windows.Forms.Label()
		Me.ISO_Label = New System.Windows.Forms.Label()
		Me.Process_DataGridView = New System.Windows.Forms.DataGridView()
		Me.FlowLayoutPanel3 = New System.Windows.Forms.FlowLayoutPanel()
		Me.Label10 = New System.Windows.Forms.Label()
		Me.Recent_Used_ComboBox = New System.Windows.Forms.ComboBox()
		Me.Label1 = New System.Windows.Forms.Label()
		Me.Filter_Info_Label = New System.Windows.Forms.Label()
		Me.GroupBox3 = New System.Windows.Forms.GroupBox()
		Me.Record_DataGridView = New System.Windows.Forms.DataGridView()
		Me.Delete_Button = New System.Windows.Forms.Button()
		Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
		Me.Label7 = New System.Windows.Forms.Label()
		Me.FlowLayoutPanel2.SuspendLayout()
		Me.PanelInfo_GroupBox.SuspendLayout()
		Me.GroupBox2.SuspendLayout()
		Me.FlowLayoutPanel4.SuspendLayout()
		Me.Mass_FlowLayoutPanel.SuspendLayout()
		Me.Power_FlowLayoutPanel.SuspendLayout()
		CType(Me.Process_DataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.FlowLayoutPanel3.SuspendLayout()
		Me.GroupBox3.SuspendLayout()
		CType(Me.Record_DataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.FlowLayoutPanel1.SuspendLayout()
		Me.SuspendLayout()
		'
		'Completed_Button
		'
		Me.Completed_Button.Location = New System.Drawing.Point(4, 936)
		Me.Completed_Button.Margin = New System.Windows.Forms.Padding(4)
		Me.Completed_Button.Name = "Completed_Button"
		Me.Completed_Button.Size = New System.Drawing.Size(665, 38)
		Me.Completed_Button.TabIndex = 3
		Me.Completed_Button.Text = "完成"
		Me.Completed_Button.UseVisualStyleBackColor = True
		'
		'FlowLayoutPanel2
		'
		Me.FlowLayoutPanel2.Controls.Add(Me.Label5)
		Me.FlowLayoutPanel2.Controls.Add(Me.Name_Filter_TextBox)
		Me.FlowLayoutPanel2.Controls.Add(Me.Clean_Filter_Button)
		Me.FlowLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Top
		Me.FlowLayoutPanel2.Location = New System.Drawing.Point(4, 22)
		Me.FlowLayoutPanel2.Margin = New System.Windows.Forms.Padding(4)
		Me.FlowLayoutPanel2.Name = "FlowLayoutPanel2"
		Me.FlowLayoutPanel2.Size = New System.Drawing.Size(657, 39)
		Me.FlowLayoutPanel2.TabIndex = 3
		'
		'Label5
		'
		Me.Label5.AutoSize = True
		Me.Label5.Location = New System.Drawing.Point(4, 10)
		Me.Label5.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label5.Name = "Label5"
		Me.Label5.Size = New System.Drawing.Size(127, 15)
		Me.Label5.TabIndex = 0
		Me.Label5.Text = "加工方式名稱篩選"
		'
		'Name_Filter_TextBox
		'
		Me.Name_Filter_TextBox.Location = New System.Drawing.Point(139, 4)
		Me.Name_Filter_TextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.Name_Filter_TextBox.Name = "Name_Filter_TextBox"
		Me.Name_Filter_TextBox.Size = New System.Drawing.Size(401, 25)
		Me.Name_Filter_TextBox.TabIndex = 1
		'
		'Clean_Filter_Button
		'
		Me.Clean_Filter_Button.Location = New System.Drawing.Point(548, 4)
		Me.Clean_Filter_Button.Margin = New System.Windows.Forms.Padding(4)
		Me.Clean_Filter_Button.Name = "Clean_Filter_Button"
		Me.Clean_Filter_Button.Size = New System.Drawing.Size(100, 29)
		Me.Clean_Filter_Button.TabIndex = 2
		Me.Clean_Filter_Button.Text = "清除篩選"
		Me.Clean_Filter_Button.UseVisualStyleBackColor = True
		'
		'Component_Name_Label
		'
		Me.Component_Name_Label.AutoSize = True
		Me.Component_Name_Label.ForeColor = System.Drawing.Color.Blue
		Me.Component_Name_Label.Location = New System.Drawing.Point(148, 22)
		Me.Component_Name_Label.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
		Me.Component_Name_Label.Name = "Component_Name_Label"
		Me.Component_Name_Label.Size = New System.Drawing.Size(31, 15)
		Me.Component_Name_Label.TabIndex = 1
		Me.Component_Name_Label.Text = "N/A"
		'
		'PanelInfo_Component_Label
		'
		Me.PanelInfo_Component_Label.AutoSize = True
		Me.PanelInfo_Component_Label.Location = New System.Drawing.Point(12, 22)
		Me.PanelInfo_Component_Label.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
		Me.PanelInfo_Component_Label.Name = "PanelInfo_Component_Label"
		Me.PanelInfo_Component_Label.Size = New System.Drawing.Size(105, 15)
		Me.PanelInfo_Component_Label.TabIndex = 0
		Me.PanelInfo_Component_Label.Text = "零件/ 原料名稱"
		'
		'PanelInfo_GroupBox
		'
		Me.PanelInfo_GroupBox.Controls.Add(Me.Mass_Value_Label)
		Me.PanelInfo_GroupBox.Controls.Add(Me.Mass_Label)
		Me.PanelInfo_GroupBox.Controls.Add(Me.Component_Name_Label)
		Me.PanelInfo_GroupBox.Controls.Add(Me.PanelInfo_Component_Label)
		Me.PanelInfo_GroupBox.Location = New System.Drawing.Point(4, 4)
		Me.PanelInfo_GroupBox.Margin = New System.Windows.Forms.Padding(4)
		Me.PanelInfo_GroupBox.Name = "PanelInfo_GroupBox"
		Me.PanelInfo_GroupBox.Padding = New System.Windows.Forms.Padding(4)
		Me.PanelInfo_GroupBox.Size = New System.Drawing.Size(665, 69)
		Me.PanelInfo_GroupBox.TabIndex = 0
		Me.PanelInfo_GroupBox.TabStop = False
		Me.PanelInfo_GroupBox.Text = "專案 : N/A"
		'
		'Mass_Value_Label
		'
		Me.Mass_Value_Label.AutoSize = True
		Me.Mass_Value_Label.ForeColor = System.Drawing.Color.Magenta
		Me.Mass_Value_Label.Location = New System.Drawing.Point(148, 48)
		Me.Mass_Value_Label.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
		Me.Mass_Value_Label.Name = "Mass_Value_Label"
		Me.Mass_Value_Label.Size = New System.Drawing.Size(31, 15)
		Me.Mass_Value_Label.TabIndex = 3
		Me.Mass_Value_Label.Text = "N/A"
		'
		'Mass_Label
		'
		Me.Mass_Label.AutoSize = True
		Me.Mass_Label.Location = New System.Drawing.Point(12, 48)
		Me.Mass_Label.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
		Me.Mass_Label.Name = "Mass_Label"
		Me.Mass_Label.Size = New System.Drawing.Size(105, 15)
		Me.Mass_Label.TabIndex = 2
		Me.Mass_Label.Text = "零件/ 原料質量"
		'
		'GroupBox2
		'
		Me.GroupBox2.Controls.Add(Me.Add_Button)
		Me.GroupBox2.Controls.Add(Me.FlowLayoutPanel4)
		Me.GroupBox2.Controls.Add(Me.Process_DataGridView)
		Me.GroupBox2.Controls.Add(Me.FlowLayoutPanel3)
		Me.GroupBox2.Controls.Add(Me.FlowLayoutPanel2)
		Me.GroupBox2.ForeColor = System.Drawing.Color.Crimson
		Me.GroupBox2.Location = New System.Drawing.Point(4, 81)
		Me.GroupBox2.Margin = New System.Windows.Forms.Padding(4)
		Me.GroupBox2.Name = "GroupBox2"
		Me.GroupBox2.Padding = New System.Windows.Forms.Padding(4)
		Me.GroupBox2.Size = New System.Drawing.Size(665, 603)
		Me.GroupBox2.TabIndex = 1
		Me.GroupBox2.TabStop = False
		Me.GroupBox2.Text = "選擇加工方式"
		'
		'Add_Button
		'
		Me.Add_Button.Dock = System.Windows.Forms.DockStyle.Bottom
		Me.Add_Button.Location = New System.Drawing.Point(4, 561)
		Me.Add_Button.Margin = New System.Windows.Forms.Padding(4)
		Me.Add_Button.Name = "Add_Button"
		Me.Add_Button.Size = New System.Drawing.Size(657, 38)
		Me.Add_Button.TabIndex = 9
		Me.Add_Button.Text = "確定/新增"
		Me.Add_Button.UseVisualStyleBackColor = True
		'
		'FlowLayoutPanel4
		'
		Me.FlowLayoutPanel4.BackColor = System.Drawing.Color.Transparent
		Me.FlowLayoutPanel4.Controls.Add(Me.Mass_FlowLayoutPanel)
		Me.FlowLayoutPanel4.Controls.Add(Me.Power_FlowLayoutPanel)
		Me.FlowLayoutPanel4.Controls.Add(Me.CalcComponent_WFP_Label)
		Me.FlowLayoutPanel4.Controls.Add(Me.Label27)
		Me.FlowLayoutPanel4.Controls.Add(Me.WFP_Label)
		Me.FlowLayoutPanel4.Controls.Add(Me.Label28)
		Me.FlowLayoutPanel4.Controls.Add(Me.Label4)
		Me.FlowLayoutPanel4.Controls.Add(Me.ISO_Label)
		Me.FlowLayoutPanel4.Controls.Add(Me.Label7)
		Me.FlowLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill
		Me.FlowLayoutPanel4.Location = New System.Drawing.Point(4, 312)
		Me.FlowLayoutPanel4.Margin = New System.Windows.Forms.Padding(4)
		Me.FlowLayoutPanel4.Name = "FlowLayoutPanel4"
		Me.FlowLayoutPanel4.Size = New System.Drawing.Size(657, 287)
		Me.FlowLayoutPanel4.TabIndex = 8
		'
		'Mass_FlowLayoutPanel
		'
		Me.Mass_FlowLayoutPanel.Controls.Add(Me.Label6)
		Me.Mass_FlowLayoutPanel.Controls.Add(Me.Process_Name_TextBox)
		Me.Mass_FlowLayoutPanel.Controls.Add(Me.Label8)
		Me.Mass_FlowLayoutPanel.Controls.Add(Me.Process_WFP_TextBox)
		Me.Mass_FlowLayoutPanel.Controls.Add(Me.Label9)
		Me.Mass_FlowLayoutPanel.Location = New System.Drawing.Point(0, 4)
		Me.Mass_FlowLayoutPanel.Margin = New System.Windows.Forms.Padding(0, 4, 4, 1)
		Me.Mass_FlowLayoutPanel.Name = "Mass_FlowLayoutPanel"
		Me.Mass_FlowLayoutPanel.Size = New System.Drawing.Size(645, 70)
		Me.Mass_FlowLayoutPanel.TabIndex = 4
		'
		'Label6
		'
		Me.Label6.AutoSize = True
		Me.Label6.Location = New System.Drawing.Point(4, 10)
		Me.Label6.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label6.Name = "Label6"
		Me.Label6.Size = New System.Drawing.Size(67, 15)
		Me.Label6.TabIndex = 0
		Me.Label6.Text = "加工方式"
		'
		'Process_Name_TextBox
		'
		Me.Process_Name_TextBox.Location = New System.Drawing.Point(79, 4)
		Me.Process_Name_TextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.Process_Name_TextBox.Name = "Process_Name_TextBox"
		Me.Process_Name_TextBox.ReadOnly = True
		Me.Process_Name_TextBox.Size = New System.Drawing.Size(461, 25)
		Me.Process_Name_TextBox.TabIndex = 1
		'
		'Label8
		'
		Me.Label8.AutoSize = True
		Me.Label8.Location = New System.Drawing.Point(4, 43)
		Me.Label8.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label8.Name = "Label8"
		Me.Label8.Size = New System.Drawing.Size(142, 15)
		Me.Label8.TabIndex = 3
		Me.Label8.Text = "加工方式單位水足跡"
		'
		'Process_WFP_TextBox
		'
		Me.Process_WFP_TextBox.Location = New System.Drawing.Point(154, 37)
		Me.Process_WFP_TextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.Process_WFP_TextBox.Name = "Process_WFP_TextBox"
		Me.Process_WFP_TextBox.ReadOnly = True
		Me.Process_WFP_TextBox.Size = New System.Drawing.Size(386, 25)
		Me.Process_WFP_TextBox.TabIndex = 4
		'
		'Label9
		'
		Me.Label9.AutoSize = True
		Me.Label9.Location = New System.Drawing.Point(548, 43)
		Me.Label9.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label9.Name = "Label9"
		Me.Label9.Size = New System.Drawing.Size(32, 15)
		Me.Label9.TabIndex = 5
		Me.Label9.Text = "m^3"
		'
		'Power_FlowLayoutPanel
		'
		Me.Power_FlowLayoutPanel.Controls.Add(Me.Label15)
		Me.Power_FlowLayoutPanel.Controls.Add(Me.Process_Power_Consumption_TextBox)
		Me.Power_FlowLayoutPanel.Controls.Add(Me.Label16)
		Me.Power_FlowLayoutPanel.Controls.Add(Me.Label11)
		Me.Power_FlowLayoutPanel.Controls.Add(Me.COEEF_TextBox)
		Me.Power_FlowLayoutPanel.Controls.Add(Me.Label2)
		Me.Power_FlowLayoutPanel.Controls.Add(Me.Label12)
		Me.Power_FlowLayoutPanel.Controls.Add(Me.WorkingHour_TextBox)
		Me.Power_FlowLayoutPanel.Controls.Add(Me.Label3)
		Me.Power_FlowLayoutPanel.Location = New System.Drawing.Point(0, 75)
		Me.Power_FlowLayoutPanel.Margin = New System.Windows.Forms.Padding(0, 0, 4, 1)
		Me.Power_FlowLayoutPanel.Name = "Power_FlowLayoutPanel"
		Me.Power_FlowLayoutPanel.Size = New System.Drawing.Size(645, 104)
		Me.Power_FlowLayoutPanel.TabIndex = 4
		'
		'Label15
		'
		Me.Label15.AutoSize = True
		Me.Label15.Location = New System.Drawing.Point(4, 10)
		Me.Label15.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label15.Name = "Label15"
		Me.Label15.Size = New System.Drawing.Size(82, 15)
		Me.Label15.TabIndex = 12
		Me.Label15.Text = "電力消耗量"
		'
		'Process_Power_Consumption_TextBox
		'
		Me.Process_Power_Consumption_TextBox.Location = New System.Drawing.Point(94, 4)
		Me.Process_Power_Consumption_TextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.Process_Power_Consumption_TextBox.Name = "Process_Power_Consumption_TextBox"
		Me.Process_Power_Consumption_TextBox.ReadOnly = True
		Me.Process_Power_Consumption_TextBox.Size = New System.Drawing.Size(446, 25)
		Me.Process_Power_Consumption_TextBox.TabIndex = 13
		'
		'Label16
		'
		Me.Label16.AutoSize = True
		Me.Label16.Location = New System.Drawing.Point(548, 10)
		Me.Label16.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label16.Name = "Label16"
		Me.Label16.Size = New System.Drawing.Size(27, 15)
		Me.Label16.TabIndex = 14
		Me.Label16.Text = "Kw"
		'
		'Label11
		'
		Me.Label11.AutoSize = True
		Me.Label11.Location = New System.Drawing.Point(4, 43)
		Me.Label11.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label11.Name = "Label11"
		Me.Label11.Size = New System.Drawing.Size(67, 15)
		Me.Label11.TabIndex = 6
		Me.Label11.Text = "電力系數"
		'
		'COEEF_TextBox
		'
		Me.COEEF_TextBox.Location = New System.Drawing.Point(79, 37)
		Me.COEEF_TextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.COEEF_TextBox.Name = "COEEF_TextBox"
		Me.COEEF_TextBox.ReadOnly = True
		Me.COEEF_TextBox.Size = New System.Drawing.Size(461, 25)
		Me.COEEF_TextBox.TabIndex = 7
		'
		'Label2
		'
		Me.Label2.AutoSize = True
		Me.Label2.Location = New System.Drawing.Point(548, 43)
		Me.Label2.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label2.Name = "Label2"
		Me.Label2.Size = New System.Drawing.Size(83, 15)
		Me.Label2.TabIndex = 19
		Me.Label2.Text = "m^3/Kw．hr"
		'
		'Label12
		'
		Me.Label12.AutoSize = True
		Me.Label12.Location = New System.Drawing.Point(4, 76)
		Me.Label12.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label12.Name = "Label12"
		Me.Label12.Size = New System.Drawing.Size(67, 15)
		Me.Label12.TabIndex = 8
		Me.Label12.Text = "加工時數"
		'
		'WorkingHour_TextBox
		'
		Me.WorkingHour_TextBox.Location = New System.Drawing.Point(79, 70)
		Me.WorkingHour_TextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.WorkingHour_TextBox.Name = "WorkingHour_TextBox"
		Me.WorkingHour_TextBox.Size = New System.Drawing.Size(461, 25)
		Me.WorkingHour_TextBox.TabIndex = 17
		'
		'Label3
		'
		Me.Label3.AutoSize = True
		Me.Label3.Location = New System.Drawing.Point(548, 76)
		Me.Label3.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label3.Name = "Label3"
		Me.Label3.Size = New System.Drawing.Size(19, 15)
		Me.Label3.TabIndex = 18
		Me.Label3.Text = "hr"
		'
		'CalcComponent_WFP_Label
		'
		Me.CalcComponent_WFP_Label.Location = New System.Drawing.Point(4, 190)
		Me.CalcComponent_WFP_Label.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.CalcComponent_WFP_Label.Name = "CalcComponent_WFP_Label"
		Me.CalcComponent_WFP_Label.Size = New System.Drawing.Size(127, 19)
		Me.CalcComponent_WFP_Label.TabIndex = 15
		Me.CalcComponent_WFP_Label.Text = "零件加工水足跡"
		'
		'Label27
		'
		Me.Label27.Font = New System.Drawing.Font("PMingLiU", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
		Me.Label27.ForeColor = System.Drawing.Color.Purple
		Me.Label27.Location = New System.Drawing.Point(138, 190)
		Me.Label27.Margin = New System.Windows.Forms.Padding(3, 10, 3, 0)
		Me.Label27.Name = "Label27"
		Me.Label27.Size = New System.Drawing.Size(95, 15)
		Me.Label27.TabIndex = 34
		Me.Label27.Text = "WFN"
		Me.Label27.TextAlign = System.Drawing.ContentAlignment.TopRight
		'
		'WFP_Label
		'
		Me.WFP_Label.ForeColor = System.Drawing.Color.CornflowerBlue
		Me.WFP_Label.Location = New System.Drawing.Point(240, 190)
		Me.WFP_Label.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.WFP_Label.Name = "WFP_Label"
		Me.WFP_Label.Size = New System.Drawing.Size(300, 19)
		Me.WFP_Label.TabIndex = 16
		Me.WFP_Label.Text = "N/A"
		'
		'Label28
		'
		Me.Label28.Font = New System.Drawing.Font("PMingLiU", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
		Me.Label28.ForeColor = System.Drawing.Color.Crimson
		Me.Label28.Location = New System.Drawing.Point(547, 190)
		Me.Label28.Margin = New System.Windows.Forms.Padding(3, 10, 3, 0)
		Me.Label28.Name = "Label28"
		Me.Label28.Size = New System.Drawing.Size(83, 26)
		Me.Label28.TabIndex = 35
		Me.Label28.Text = "m^3"
		'
		'Label4
		'
		Me.Label4.Font = New System.Drawing.Font("PMingLiU", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
		Me.Label4.ForeColor = System.Drawing.Color.Black
		Me.Label4.Location = New System.Drawing.Point(3, 216)
		Me.Label4.Name = "Label4"
		Me.Label4.Size = New System.Drawing.Size(230, 25)
		Me.Label4.TabIndex = 37
		Me.Label4.Text = "ISO 14046"
		Me.Label4.TextAlign = System.Drawing.ContentAlignment.BottomRight
		'
		'ISO_Label
		'
		Me.ISO_Label.ForeColor = System.Drawing.Color.CornflowerBlue
		Me.ISO_Label.Location = New System.Drawing.Point(240, 226)
		Me.ISO_Label.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.ISO_Label.Name = "ISO_Label"
		Me.ISO_Label.Size = New System.Drawing.Size(300, 19)
		Me.ISO_Label.TabIndex = 36
		Me.ISO_Label.Text = "N/A"
		'
		'Process_DataGridView
		'
		Me.Process_DataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
		Me.Process_DataGridView.Dock = System.Windows.Forms.DockStyle.Top
		Me.Process_DataGridView.Location = New System.Drawing.Point(4, 132)
		Me.Process_DataGridView.Margin = New System.Windows.Forms.Padding(4)
		Me.Process_DataGridView.Name = "Process_DataGridView"
		Me.Process_DataGridView.RowTemplate.Height = 24
		Me.Process_DataGridView.Size = New System.Drawing.Size(657, 180)
		Me.Process_DataGridView.TabIndex = 7
		'
		'FlowLayoutPanel3
		'
		Me.FlowLayoutPanel3.Controls.Add(Me.Label10)
		Me.FlowLayoutPanel3.Controls.Add(Me.Recent_Used_ComboBox)
		Me.FlowLayoutPanel3.Controls.Add(Me.Label1)
		Me.FlowLayoutPanel3.Controls.Add(Me.Filter_Info_Label)
		Me.FlowLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Top
		Me.FlowLayoutPanel3.Location = New System.Drawing.Point(4, 61)
		Me.FlowLayoutPanel3.Margin = New System.Windows.Forms.Padding(4)
		Me.FlowLayoutPanel3.Name = "FlowLayoutPanel3"
		Me.FlowLayoutPanel3.Size = New System.Drawing.Size(657, 71)
		Me.FlowLayoutPanel3.TabIndex = 6
		'
		'Label10
		'
		Me.Label10.AutoSize = True
		Me.Label10.Location = New System.Drawing.Point(4, 10)
		Me.Label10.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label10.Name = "Label10"
		Me.Label10.Size = New System.Drawing.Size(142, 15)
		Me.Label10.TabIndex = 0
		Me.Label10.Text = "最近曾使用加工方式"
		'
		'Recent_Used_ComboBox
		'
		Me.Recent_Used_ComboBox.Font = New System.Drawing.Font("PMingLiU", 10.0!)
		Me.Recent_Used_ComboBox.FormattingEnabled = True
		Me.Recent_Used_ComboBox.Location = New System.Drawing.Point(154, 4)
		Me.Recent_Used_ComboBox.Margin = New System.Windows.Forms.Padding(4)
		Me.Recent_Used_ComboBox.Name = "Recent_Used_ComboBox"
		Me.Recent_Used_ComboBox.Size = New System.Drawing.Size(386, 25)
		Me.Recent_Used_ComboBox.TabIndex = 1
		Me.Recent_Used_ComboBox.Text = "請選擇"
		'
		'Label1
		'
		Me.Label1.AutoSize = True
		Me.Label1.ForeColor = System.Drawing.Color.Black
		Me.Label1.Location = New System.Drawing.Point(4, 48)
		Me.Label1.Margin = New System.Windows.Forms.Padding(4, 15, 4, 0)
		Me.Label1.Name = "Label1"
		Me.Label1.Size = New System.Drawing.Size(136, 15)
		Me.Label1.TabIndex = 2
		Me.Label1.Text = "篩選筆數 / 總筆數 : "
		'
		'Filter_Info_Label
		'
		Me.Filter_Info_Label.AutoSize = True
		Me.Filter_Info_Label.Location = New System.Drawing.Point(148, 48)
		Me.Filter_Info_Label.Margin = New System.Windows.Forms.Padding(4, 15, 4, 0)
		Me.Filter_Info_Label.Name = "Filter_Info_Label"
		Me.Filter_Info_Label.Size = New System.Drawing.Size(31, 15)
		Me.Filter_Info_Label.TabIndex = 3
		Me.Filter_Info_Label.Text = "N/A"
		'
		'GroupBox3
		'
		Me.GroupBox3.Controls.Add(Me.Record_DataGridView)
		Me.GroupBox3.Controls.Add(Me.Delete_Button)
		Me.GroupBox3.Location = New System.Drawing.Point(4, 692)
		Me.GroupBox3.Margin = New System.Windows.Forms.Padding(4)
		Me.GroupBox3.Name = "GroupBox3"
		Me.GroupBox3.Padding = New System.Windows.Forms.Padding(4)
		Me.GroupBox3.Size = New System.Drawing.Size(665, 236)
		Me.GroupBox3.TabIndex = 2
		Me.GroupBox3.TabStop = False
		Me.GroupBox3.Text = "加工記錄(以下水足跡單位為 m^3)"
		'
		'Record_DataGridView
		'
		Me.Record_DataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
		Me.Record_DataGridView.Dock = System.Windows.Forms.DockStyle.Fill
		Me.Record_DataGridView.Location = New System.Drawing.Point(4, 22)
		Me.Record_DataGridView.Margin = New System.Windows.Forms.Padding(4)
		Me.Record_DataGridView.Name = "Record_DataGridView"
		Me.Record_DataGridView.RowTemplate.Height = 24
		Me.Record_DataGridView.Size = New System.Drawing.Size(657, 172)
		Me.Record_DataGridView.TabIndex = 1
		'
		'Delete_Button
		'
		Me.Delete_Button.Dock = System.Windows.Forms.DockStyle.Bottom
		Me.Delete_Button.Location = New System.Drawing.Point(4, 194)
		Me.Delete_Button.Margin = New System.Windows.Forms.Padding(4)
		Me.Delete_Button.Name = "Delete_Button"
		Me.Delete_Button.Size = New System.Drawing.Size(657, 38)
		Me.Delete_Button.TabIndex = 0
		Me.Delete_Button.Text = "刪除記錄"
		Me.Delete_Button.UseVisualStyleBackColor = True
		'
		'FlowLayoutPanel1
		'
		Me.FlowLayoutPanel1.Controls.Add(Me.PanelInfo_GroupBox)
		Me.FlowLayoutPanel1.Controls.Add(Me.GroupBox2)
		Me.FlowLayoutPanel1.Controls.Add(Me.GroupBox3)
		Me.FlowLayoutPanel1.Controls.Add(Me.Completed_Button)
		Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
		Me.FlowLayoutPanel1.Location = New System.Drawing.Point(0, 0)
		Me.FlowLayoutPanel1.Margin = New System.Windows.Forms.Padding(4)
		Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
		Me.FlowLayoutPanel1.Size = New System.Drawing.Size(682, 978)
		Me.FlowLayoutPanel1.TabIndex = 1
		'
		'Label7
		'
		Me.Label7.Font = New System.Drawing.Font("PMingLiU", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
		Me.Label7.ForeColor = System.Drawing.Color.Crimson
		Me.Label7.Location = New System.Drawing.Point(547, 226)
		Me.Label7.Margin = New System.Windows.Forms.Padding(3, 10, 3, 0)
		Me.Label7.Name = "Label7"
		Me.Label7.Size = New System.Drawing.Size(83, 26)
		Me.Label7.TabIndex = 38
		Me.Label7.Text = "m^3"
		'
		'MakeProcessForm
		'
		Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 15.0!)
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.ClientSize = New System.Drawing.Size(682, 978)
		Me.Controls.Add(Me.FlowLayoutPanel1)
		Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
		Me.Margin = New System.Windows.Forms.Padding(4)
		Me.Name = "MakeProcessForm"
		Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
		Me.Text = "選擇加工方式 (一般/後製)"
		Me.TopMost = True
		Me.FlowLayoutPanel2.ResumeLayout(False)
		Me.FlowLayoutPanel2.PerformLayout()
		Me.PanelInfo_GroupBox.ResumeLayout(False)
		Me.PanelInfo_GroupBox.PerformLayout()
		Me.GroupBox2.ResumeLayout(False)
		Me.FlowLayoutPanel4.ResumeLayout(False)
		Me.Mass_FlowLayoutPanel.ResumeLayout(False)
		Me.Mass_FlowLayoutPanel.PerformLayout()
		Me.Power_FlowLayoutPanel.ResumeLayout(False)
		Me.Power_FlowLayoutPanel.PerformLayout()
		CType(Me.Process_DataGridView, System.ComponentModel.ISupportInitialize).EndInit()
		Me.FlowLayoutPanel3.ResumeLayout(False)
		Me.FlowLayoutPanel3.PerformLayout()
		Me.GroupBox3.ResumeLayout(False)
		CType(Me.Record_DataGridView, System.ComponentModel.ISupportInitialize).EndInit()
		Me.FlowLayoutPanel1.ResumeLayout(False)
		Me.ResumeLayout(False)

	End Sub

	Friend WithEvents Completed_Button As Button
	Friend WithEvents FlowLayoutPanel2 As FlowLayoutPanel
	Friend WithEvents Label5 As Label
	Friend WithEvents Name_Filter_TextBox As TextBox
	Friend WithEvents Clean_Filter_Button As Button
	Friend WithEvents Component_Name_Label As Label
	Friend WithEvents PanelInfo_Component_Label As Label
	Friend WithEvents PanelInfo_GroupBox As GroupBox
	Friend WithEvents GroupBox2 As GroupBox
	Friend WithEvents GroupBox3 As GroupBox
	Friend WithEvents FlowLayoutPanel1 As FlowLayoutPanel
	Friend WithEvents FlowLayoutPanel3 As FlowLayoutPanel
	Friend WithEvents Label10 As Label
	Friend WithEvents Recent_Used_ComboBox As ComboBox
	Friend WithEvents Add_Button As Button
	Friend WithEvents FlowLayoutPanel4 As FlowLayoutPanel
	Friend WithEvents Label6 As Label
	Friend WithEvents Process_Name_TextBox As TextBox
	Friend WithEvents Label8 As Label
	Friend WithEvents Process_WFP_TextBox As TextBox
	Friend WithEvents Label9 As Label
	Friend WithEvents Label11 As Label
	Friend WithEvents COEEF_TextBox As TextBox
	Friend WithEvents Label12 As Label
	Friend WithEvents Label15 As Label
	Friend WithEvents Process_Power_Consumption_TextBox As TextBox
	Friend WithEvents Label16 As Label
	Friend WithEvents Process_DataGridView As DataGridView
	Friend WithEvents CalcComponent_WFP_Label As Label
	Friend WithEvents WFP_Label As Label
	Friend WithEvents Record_DataGridView As DataGridView
	Friend WithEvents Delete_Button As Button
	Friend WithEvents Mass_Value_Label As Label
	Friend WithEvents Mass_Label As Label
	Friend WithEvents Label1 As Label
	Friend WithEvents Filter_Info_Label As Label
	Friend WithEvents WorkingHour_TextBox As TextBox
	Friend WithEvents Label3 As Label
	Friend WithEvents Power_FlowLayoutPanel As FlowLayoutPanel
	Friend WithEvents Mass_FlowLayoutPanel As FlowLayoutPanel
	Friend WithEvents Label27 As Label
	Friend WithEvents Label28 As Label
	Friend WithEvents ISO_Label As Label
	Friend WithEvents Label2 As Label
	Friend WithEvents Label4 As Label
	Friend WithEvents Label7 As Label
End Class
