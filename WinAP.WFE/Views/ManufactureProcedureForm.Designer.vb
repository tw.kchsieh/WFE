﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ManufactureProcedureForm
	Inherits System.Windows.Forms.Form

	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> _
	Protected Overrides Sub Dispose(ByVal disposing As Boolean)
		Try
			If disposing AndAlso components IsNot Nothing Then
				components.Dispose()
			End If
		Finally
			MyBase.Dispose(disposing)
		End Try
	End Sub

	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer

	'NOTE: The following procedure is required by the Windows Form Designer
	'It can be modified using the Windows Form Designer.  
	'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> _
	Private Sub InitializeComponent()
		Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ManufactureProcedureForm))
		Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
		Me.Product_Info_GroupBox = New System.Windows.Forms.GroupBox()
		Me.Material_Name_Label = New System.Windows.Forms.Label()
		Me.Label1 = New System.Windows.Forms.Label()
		Me.GroupBox2 = New System.Windows.Forms.GroupBox()
		Me.Panel1 = New System.Windows.Forms.Panel()
		Me.Filter_Info_Label = New System.Windows.Forms.Label()
		Me.Label2 = New System.Windows.Forms.Label()
		Me.FlowLayoutPanel4 = New System.Windows.Forms.FlowLayoutPanel()
		Me.Label6 = New System.Windows.Forms.Label()
		Me.MakeProcessName_TextBox = New System.Windows.Forms.TextBox()
		Me.Label8 = New System.Windows.Forms.Label()
		Me.Label61 = New System.Windows.Forms.Label()
		Me.MakeProcess_WFP_TextBox = New System.Windows.Forms.TextBox()
		Me.Label9 = New System.Windows.Forms.Label()
		Me.Label60 = New System.Windows.Forms.Label()
		Me.MakeProcess_ISO_TextBox = New System.Windows.Forms.TextBox()
		Me.Label7 = New System.Windows.Forms.Label()
		Me.Label3 = New System.Windows.Forms.Label()
		Me.Density_TextBox = New System.Windows.Forms.TextBox()
		Me.Label4 = New System.Windows.Forms.Label()
		Me.Result_DataGridView = New System.Windows.Forms.DataGridView()
		Me.FlowLayoutPanel3 = New System.Windows.Forms.FlowLayoutPanel()
		Me.Label10 = New System.Windows.Forms.Label()
		Me.RecentUsed_ComboBox = New System.Windows.Forms.ComboBox()
		Me.FlowLayoutPanel2 = New System.Windows.Forms.FlowLayoutPanel()
		Me.Label5 = New System.Windows.Forms.Label()
		Me.NameFilter_TextBox = New System.Windows.Forms.TextBox()
		Me.Clean_Filter_Button = New System.Windows.Forms.Button()
		Me.CompletedButton = New System.Windows.Forms.Button()
		Me.FlowLayoutPanel1.SuspendLayout()
		Me.Product_Info_GroupBox.SuspendLayout()
		Me.GroupBox2.SuspendLayout()
		Me.Panel1.SuspendLayout()
		Me.FlowLayoutPanel4.SuspendLayout()
		CType(Me.Result_DataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.FlowLayoutPanel3.SuspendLayout()
		Me.FlowLayoutPanel2.SuspendLayout()
		Me.SuspendLayout()
		'
		'FlowLayoutPanel1
		'
		Me.FlowLayoutPanel1.Controls.Add(Me.Product_Info_GroupBox)
		Me.FlowLayoutPanel1.Controls.Add(Me.GroupBox2)
		Me.FlowLayoutPanel1.Controls.Add(Me.CompletedButton)
		Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
		Me.FlowLayoutPanel1.Location = New System.Drawing.Point(0, 0)
		Me.FlowLayoutPanel1.Margin = New System.Windows.Forms.Padding(4)
		Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
		Me.FlowLayoutPanel1.Size = New System.Drawing.Size(565, 825)
		Me.FlowLayoutPanel1.TabIndex = 2
		'
		'Product_Info_GroupBox
		'
		Me.Product_Info_GroupBox.Controls.Add(Me.Material_Name_Label)
		Me.Product_Info_GroupBox.Controls.Add(Me.Label1)
		Me.Product_Info_GroupBox.Location = New System.Drawing.Point(4, 4)
		Me.Product_Info_GroupBox.Margin = New System.Windows.Forms.Padding(4)
		Me.Product_Info_GroupBox.Name = "Product_Info_GroupBox"
		Me.Product_Info_GroupBox.Padding = New System.Windows.Forms.Padding(4)
		Me.Product_Info_GroupBox.Size = New System.Drawing.Size(556, 50)
		Me.Product_Info_GroupBox.TabIndex = 0
		Me.Product_Info_GroupBox.TabStop = False
		Me.Product_Info_GroupBox.Text = "製程 : N/A"
		'
		'Material_Name_Label
		'
		Me.Material_Name_Label.AutoSize = True
		Me.Material_Name_Label.ForeColor = System.Drawing.Color.Blue
		Me.Material_Name_Label.Location = New System.Drawing.Point(148, 22)
		Me.Material_Name_Label.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
		Me.Material_Name_Label.Name = "Material_Name_Label"
		Me.Material_Name_Label.Size = New System.Drawing.Size(31, 15)
		Me.Material_Name_Label.TabIndex = 1
		Me.Material_Name_Label.Text = "N/A"
		'
		'Label1
		'
		Me.Label1.AutoSize = True
		Me.Label1.Location = New System.Drawing.Point(12, 22)
		Me.Label1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
		Me.Label1.Name = "Label1"
		Me.Label1.Size = New System.Drawing.Size(67, 15)
		Me.Label1.TabIndex = 0
		Me.Label1.Text = "零件名稱"
		'
		'GroupBox2
		'
		Me.GroupBox2.Controls.Add(Me.Panel1)
		Me.GroupBox2.Controls.Add(Me.FlowLayoutPanel4)
		Me.GroupBox2.Controls.Add(Me.Result_DataGridView)
		Me.GroupBox2.Controls.Add(Me.FlowLayoutPanel3)
		Me.GroupBox2.Controls.Add(Me.FlowLayoutPanel2)
		Me.GroupBox2.ForeColor = System.Drawing.Color.Crimson
		Me.GroupBox2.Location = New System.Drawing.Point(4, 62)
		Me.GroupBox2.Margin = New System.Windows.Forms.Padding(4)
		Me.GroupBox2.Name = "GroupBox2"
		Me.GroupBox2.Padding = New System.Windows.Forms.Padding(4)
		Me.GroupBox2.Size = New System.Drawing.Size(556, 710)
		Me.GroupBox2.TabIndex = 1
		Me.GroupBox2.TabStop = False
		Me.GroupBox2.Text = "選擇製程"
		'
		'Panel1
		'
		Me.Panel1.Controls.Add(Me.Filter_Info_Label)
		Me.Panel1.Controls.Add(Me.Label2)
		Me.Panel1.Location = New System.Drawing.Point(4, 101)
		Me.Panel1.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
		Me.Panel1.Name = "Panel1"
		Me.Panel1.Size = New System.Drawing.Size(548, 30)
		Me.Panel1.TabIndex = 9
		'
		'Filter_Info_Label
		'
		Me.Filter_Info_Label.AutoSize = True
		Me.Filter_Info_Label.ForeColor = System.Drawing.Color.DarkCyan
		Me.Filter_Info_Label.Location = New System.Drawing.Point(159, 5)
		Me.Filter_Info_Label.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
		Me.Filter_Info_Label.Name = "Filter_Info_Label"
		Me.Filter_Info_Label.Size = New System.Drawing.Size(31, 15)
		Me.Filter_Info_Label.TabIndex = 3
		Me.Filter_Info_Label.Text = "N/A"
		'
		'Label2
		'
		Me.Label2.AutoSize = True
		Me.Label2.Location = New System.Drawing.Point(4, 5)
		Me.Label2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
		Me.Label2.Name = "Label2"
		Me.Label2.Size = New System.Drawing.Size(136, 15)
		Me.Label2.TabIndex = 2
		Me.Label2.Text = "篩選筆數 / 總筆數 : "
		'
		'FlowLayoutPanel4
		'
		Me.FlowLayoutPanel4.BackColor = System.Drawing.Color.Transparent
		Me.FlowLayoutPanel4.Controls.Add(Me.Label6)
		Me.FlowLayoutPanel4.Controls.Add(Me.MakeProcessName_TextBox)
		Me.FlowLayoutPanel4.Controls.Add(Me.Label8)
		Me.FlowLayoutPanel4.Controls.Add(Me.Label61)
		Me.FlowLayoutPanel4.Controls.Add(Me.MakeProcess_WFP_TextBox)
		Me.FlowLayoutPanel4.Controls.Add(Me.Label9)
		Me.FlowLayoutPanel4.Controls.Add(Me.Label60)
		Me.FlowLayoutPanel4.Controls.Add(Me.MakeProcess_ISO_TextBox)
		Me.FlowLayoutPanel4.Controls.Add(Me.Label7)
		Me.FlowLayoutPanel4.Controls.Add(Me.Label3)
		Me.FlowLayoutPanel4.Controls.Add(Me.Density_TextBox)
		Me.FlowLayoutPanel4.Controls.Add(Me.Label4)
		Me.FlowLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Bottom
		Me.FlowLayoutPanel4.Location = New System.Drawing.Point(4, 546)
		Me.FlowLayoutPanel4.Margin = New System.Windows.Forms.Padding(4)
		Me.FlowLayoutPanel4.Name = "FlowLayoutPanel4"
		Me.FlowLayoutPanel4.Size = New System.Drawing.Size(548, 160)
		Me.FlowLayoutPanel4.TabIndex = 8
		'
		'Label6
		'
		Me.Label6.Location = New System.Drawing.Point(4, 10)
		Me.Label6.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label6.Name = "Label6"
		Me.Label6.Size = New System.Drawing.Size(87, 15)
		Me.Label6.TabIndex = 0
		Me.Label6.Text = "製程名稱"
		'
		'MakeProcessName_TextBox
		'
		Me.MakeProcessName_TextBox.Location = New System.Drawing.Point(99, 4)
		Me.MakeProcessName_TextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.MakeProcessName_TextBox.Name = "MakeProcessName_TextBox"
		Me.MakeProcessName_TextBox.ReadOnly = True
		Me.MakeProcessName_TextBox.Size = New System.Drawing.Size(373, 25)
		Me.MakeProcessName_TextBox.TabIndex = 1
		'
		'Label8
		'
		Me.Label8.Location = New System.Drawing.Point(4, 43)
		Me.Label8.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label8.Name = "Label8"
		Me.Label8.Size = New System.Drawing.Size(87, 19)
		Me.Label8.TabIndex = 3
		Me.Label8.Text = "製程水足跡"
		'
		'Label61
		'
		Me.Label61.AutoSize = True
		Me.Label61.Font = New System.Drawing.Font("PMingLiU", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
		Me.Label61.ForeColor = System.Drawing.Color.Purple
		Me.Label61.Location = New System.Drawing.Point(98, 41)
		Me.Label61.Margin = New System.Windows.Forms.Padding(3, 8, 3, 0)
		Me.Label61.Name = "Label61"
		Me.Label61.Size = New System.Drawing.Size(41, 15)
		Me.Label61.TabIndex = 38
		Me.Label61.Text = "WFN"
		'
		'MakeProcess_WFP_TextBox
		'
		Me.MakeProcess_WFP_TextBox.Location = New System.Drawing.Point(146, 37)
		Me.MakeProcess_WFP_TextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.MakeProcess_WFP_TextBox.Name = "MakeProcess_WFP_TextBox"
		Me.MakeProcess_WFP_TextBox.ReadOnly = True
		Me.MakeProcess_WFP_TextBox.Size = New System.Drawing.Size(326, 25)
		Me.MakeProcess_WFP_TextBox.TabIndex = 4
		'
		'Label9
		'
		Me.Label9.AutoSize = True
		Me.Label9.Location = New System.Drawing.Point(480, 43)
		Me.Label9.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label9.Name = "Label9"
		Me.Label9.Size = New System.Drawing.Size(32, 15)
		Me.Label9.TabIndex = 5
		Me.Label9.Text = "m^3"
		'
		'Label60
		'
		Me.Label60.Font = New System.Drawing.Font("PMingLiU", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
		Me.Label60.ForeColor = System.Drawing.Color.Black
		Me.Label60.Location = New System.Drawing.Point(3, 74)
		Me.Label60.Margin = New System.Windows.Forms.Padding(3, 8, 3, 0)
		Me.Label60.Name = "Label60"
		Me.Label60.Size = New System.Drawing.Size(136, 17)
		Me.Label60.TabIndex = 39
		Me.Label60.Text = "ISO 14046"
		Me.Label60.TextAlign = System.Drawing.ContentAlignment.TopRight
		'
		'MakeProcess_ISO_TextBox
		'
		Me.MakeProcess_ISO_TextBox.Location = New System.Drawing.Point(146, 70)
		Me.MakeProcess_ISO_TextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.MakeProcess_ISO_TextBox.Name = "MakeProcess_ISO_TextBox"
		Me.MakeProcess_ISO_TextBox.ReadOnly = True
		Me.MakeProcess_ISO_TextBox.Size = New System.Drawing.Size(326, 25)
		Me.MakeProcess_ISO_TextBox.TabIndex = 40
		'
		'Label7
		'
		Me.Label7.AutoSize = True
		Me.Label7.Location = New System.Drawing.Point(480, 76)
		Me.Label7.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label7.Name = "Label7"
		Me.Label7.Size = New System.Drawing.Size(32, 15)
		Me.Label7.TabIndex = 41
		Me.Label7.Text = "m^3"
		'
		'Label3
		'
		Me.Label3.AutoSize = True
		Me.Label3.Location = New System.Drawing.Point(4, 109)
		Me.Label3.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label3.Name = "Label3"
		Me.Label3.Size = New System.Drawing.Size(37, 15)
		Me.Label3.TabIndex = 6
		Me.Label3.Text = "密度"
		'
		'Density_TextBox
		'
		Me.Density_TextBox.Location = New System.Drawing.Point(49, 103)
		Me.Density_TextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.Density_TextBox.Name = "Density_TextBox"
		Me.Density_TextBox.ReadOnly = True
		Me.Density_TextBox.Size = New System.Drawing.Size(423, 25)
		Me.Density_TextBox.TabIndex = 7
		'
		'Label4
		'
		Me.Label4.AutoSize = True
		Me.Label4.Location = New System.Drawing.Point(480, 109)
		Me.Label4.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label4.Name = "Label4"
		Me.Label4.Size = New System.Drawing.Size(53, 15)
		Me.Label4.TabIndex = 8
		Me.Label4.Text = "Kg/m^3"
		'
		'Result_DataGridView
		'
		Me.Result_DataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
		Me.Result_DataGridView.Location = New System.Drawing.Point(4, 138)
		Me.Result_DataGridView.Margin = New System.Windows.Forms.Padding(4)
		Me.Result_DataGridView.Name = "Result_DataGridView"
		Me.Result_DataGridView.RowTemplate.Height = 24
		Me.Result_DataGridView.Size = New System.Drawing.Size(548, 404)
		Me.Result_DataGridView.TabIndex = 7
		'
		'FlowLayoutPanel3
		'
		Me.FlowLayoutPanel3.Controls.Add(Me.Label10)
		Me.FlowLayoutPanel3.Controls.Add(Me.RecentUsed_ComboBox)
		Me.FlowLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Top
		Me.FlowLayoutPanel3.Location = New System.Drawing.Point(4, 61)
		Me.FlowLayoutPanel3.Margin = New System.Windows.Forms.Padding(4)
		Me.FlowLayoutPanel3.Name = "FlowLayoutPanel3"
		Me.FlowLayoutPanel3.Size = New System.Drawing.Size(548, 38)
		Me.FlowLayoutPanel3.TabIndex = 6
		'
		'Label10
		'
		Me.Label10.AutoSize = True
		Me.Label10.Location = New System.Drawing.Point(4, 10)
		Me.Label10.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label10.Name = "Label10"
		Me.Label10.Size = New System.Drawing.Size(112, 15)
		Me.Label10.TabIndex = 0
		Me.Label10.Text = "最近曾使用製程"
		'
		'RecentUsed_ComboBox
		'
		Me.RecentUsed_ComboBox.Font = New System.Drawing.Font("PMingLiU", 10.0!)
		Me.RecentUsed_ComboBox.FormattingEnabled = True
		Me.RecentUsed_ComboBox.Location = New System.Drawing.Point(124, 4)
		Me.RecentUsed_ComboBox.Margin = New System.Windows.Forms.Padding(4)
		Me.RecentUsed_ComboBox.Name = "RecentUsed_ComboBox"
		Me.RecentUsed_ComboBox.Size = New System.Drawing.Size(377, 25)
		Me.RecentUsed_ComboBox.TabIndex = 1
		Me.RecentUsed_ComboBox.Text = "請選擇"
		'
		'FlowLayoutPanel2
		'
		Me.FlowLayoutPanel2.Controls.Add(Me.Label5)
		Me.FlowLayoutPanel2.Controls.Add(Me.NameFilter_TextBox)
		Me.FlowLayoutPanel2.Controls.Add(Me.Clean_Filter_Button)
		Me.FlowLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Top
		Me.FlowLayoutPanel2.Location = New System.Drawing.Point(4, 22)
		Me.FlowLayoutPanel2.Margin = New System.Windows.Forms.Padding(4)
		Me.FlowLayoutPanel2.Name = "FlowLayoutPanel2"
		Me.FlowLayoutPanel2.Size = New System.Drawing.Size(548, 39)
		Me.FlowLayoutPanel2.TabIndex = 3
		'
		'Label5
		'
		Me.Label5.AutoSize = True
		Me.Label5.Location = New System.Drawing.Point(4, 10)
		Me.Label5.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label5.Name = "Label5"
		Me.Label5.Size = New System.Drawing.Size(97, 15)
		Me.Label5.TabIndex = 0
		Me.Label5.Text = "製程名稱篩選"
		'
		'NameFilter_TextBox
		'
		Me.NameFilter_TextBox.Location = New System.Drawing.Point(109, 4)
		Me.NameFilter_TextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.NameFilter_TextBox.Name = "NameFilter_TextBox"
		Me.NameFilter_TextBox.Size = New System.Drawing.Size(287, 25)
		Me.NameFilter_TextBox.TabIndex = 1
		'
		'Clean_Filter_Button
		'
		Me.Clean_Filter_Button.Location = New System.Drawing.Point(404, 4)
		Me.Clean_Filter_Button.Margin = New System.Windows.Forms.Padding(4)
		Me.Clean_Filter_Button.Name = "Clean_Filter_Button"
		Me.Clean_Filter_Button.Size = New System.Drawing.Size(97, 29)
		Me.Clean_Filter_Button.TabIndex = 2
		Me.Clean_Filter_Button.Text = "清除篩選"
		Me.Clean_Filter_Button.UseVisualStyleBackColor = True
		'
		'CompletedButton
		'
		Me.CompletedButton.Location = New System.Drawing.Point(4, 780)
		Me.CompletedButton.Margin = New System.Windows.Forms.Padding(4)
		Me.CompletedButton.Name = "CompletedButton"
		Me.CompletedButton.Size = New System.Drawing.Size(556, 38)
		Me.CompletedButton.TabIndex = 3
		Me.CompletedButton.Text = "完成"
		Me.CompletedButton.UseVisualStyleBackColor = True
		'
		'ManufactureProcedureForm
		'
		Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 15.0!)
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.ClientSize = New System.Drawing.Size(565, 825)
		Me.Controls.Add(Me.FlowLayoutPanel1)
		Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
		Me.Margin = New System.Windows.Forms.Padding(4)
		Me.Name = "ManufactureProcedureForm"
		Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
		Me.Text = "選擇特殊零件製程"
		Me.TopMost = True
		Me.FlowLayoutPanel1.ResumeLayout(False)
		Me.Product_Info_GroupBox.ResumeLayout(False)
		Me.Product_Info_GroupBox.PerformLayout()
		Me.GroupBox2.ResumeLayout(False)
		Me.Panel1.ResumeLayout(False)
		Me.Panel1.PerformLayout()
		Me.FlowLayoutPanel4.ResumeLayout(False)
		Me.FlowLayoutPanel4.PerformLayout()
		CType(Me.Result_DataGridView, System.ComponentModel.ISupportInitialize).EndInit()
		Me.FlowLayoutPanel3.ResumeLayout(False)
		Me.FlowLayoutPanel3.PerformLayout()
		Me.FlowLayoutPanel2.ResumeLayout(False)
		Me.FlowLayoutPanel2.PerformLayout()
		Me.ResumeLayout(False)

	End Sub

	Friend WithEvents FlowLayoutPanel1 As FlowLayoutPanel
	Friend WithEvents Product_Info_GroupBox As GroupBox
	Friend WithEvents Material_Name_Label As Label
	Friend WithEvents Label1 As Label
	Friend WithEvents GroupBox2 As GroupBox
	Friend WithEvents FlowLayoutPanel4 As FlowLayoutPanel
	Friend WithEvents Label6 As Label
	Friend WithEvents MakeProcessName_TextBox As TextBox
	Friend WithEvents Label8 As Label
	Friend WithEvents MakeProcess_WFP_TextBox As TextBox
	Friend WithEvents Label9 As Label
	Friend WithEvents Result_DataGridView As DataGridView
	Friend WithEvents FlowLayoutPanel3 As FlowLayoutPanel
	Friend WithEvents Label10 As Label
	Friend WithEvents RecentUsed_ComboBox As ComboBox
	Friend WithEvents FlowLayoutPanel2 As FlowLayoutPanel
	Friend WithEvents Label5 As Label
	Friend WithEvents NameFilter_TextBox As TextBox
	Friend WithEvents Clean_Filter_Button As Button
	Friend WithEvents CompletedButton As Button
	Friend WithEvents Panel1 As Panel
	Friend WithEvents Filter_Info_Label As Label
	Friend WithEvents Label2 As Label
	Friend WithEvents Label3 As Label
	Friend WithEvents Density_TextBox As TextBox
	Friend WithEvents Label4 As Label
	Friend WithEvents Label61 As Label
	Friend WithEvents Label60 As Label
	Friend WithEvents MakeProcess_ISO_TextBox As TextBox
	Friend WithEvents Label7 As Label
End Class
