﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class CopyrightForm
	Inherits System.Windows.Forms.Form

	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> _
	Protected Overrides Sub Dispose(ByVal disposing As Boolean)
		Try
			If disposing AndAlso components IsNot Nothing Then
				components.Dispose()
			End If
		Finally
			MyBase.Dispose(disposing)
		End Try
	End Sub

	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer

	'NOTE: The following procedure is required by the Windows Form Designer
	'It can be modified using the Windows Form Designer.  
	'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> _
	Private Sub InitializeComponent()
		Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(CopyrightForm))
		Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
		Me.PictureBox1 = New System.Windows.Forms.PictureBox()
		Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
		Me.Label1 = New System.Windows.Forms.Label()
		Me.version_year_Label = New System.Windows.Forms.Label()
		Me.FlowLayoutPanel2 = New System.Windows.Forms.FlowLayoutPanel()
		Me.Label5 = New System.Windows.Forms.Label()
		Me.Label4 = New System.Windows.Forms.Label()
		Me.Label6 = New System.Windows.Forms.Label()
		Me.Label7 = New System.Windows.Forms.Label()
		Me.Label8 = New System.Windows.Forms.Label()
		Me.Label9 = New System.Windows.Forms.Label()
		Me.Label10 = New System.Windows.Forms.Label()
		Me.Panel1 = New System.Windows.Forms.Panel()
		Me.closeButton = New System.Windows.Forms.Button()
		Me.TableLayoutPanel1.SuspendLayout()
		CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.FlowLayoutPanel1.SuspendLayout()
		Me.FlowLayoutPanel2.SuspendLayout()
		Me.Panel1.SuspendLayout()
		Me.SuspendLayout()
		'
		'TableLayoutPanel1
		'
		Me.TableLayoutPanel1.ColumnCount = 2
		Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 22.80967!))
		Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 77.19033!))
		Me.TableLayoutPanel1.Controls.Add(Me.PictureBox1, 0, 0)
		Me.TableLayoutPanel1.Controls.Add(Me.FlowLayoutPanel1, 1, 0)
		Me.TableLayoutPanel1.Controls.Add(Me.FlowLayoutPanel2, 1, 1)
		Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
		Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 0)
		Me.TableLayoutPanel1.Margin = New System.Windows.Forms.Padding(0)
		Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
		Me.TableLayoutPanel1.RowCount = 2
		Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 32.83582!))
		Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 67.16418!))
		Me.TableLayoutPanel1.Size = New System.Drawing.Size(658, 418)
		Me.TableLayoutPanel1.TabIndex = 0
		'
		'PictureBox1
		'
		Me.PictureBox1.Image = Global.WinAP.WFE.My.Resources.Resources.blue_water
		Me.PictureBox1.Location = New System.Drawing.Point(25, 25)
		Me.PictureBox1.Margin = New System.Windows.Forms.Padding(25, 25, 25, 0)
		Me.PictureBox1.Name = "PictureBox1"
		Me.PictureBox1.Padding = New System.Windows.Forms.Padding(5)
		Me.PictureBox1.Size = New System.Drawing.Size(96, 96)
		Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
		Me.PictureBox1.TabIndex = 0
		Me.PictureBox1.TabStop = False
		'
		'FlowLayoutPanel1
		'
		Me.FlowLayoutPanel1.Controls.Add(Me.Label1)
		Me.FlowLayoutPanel1.Controls.Add(Me.version_year_Label)
		Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
		Me.FlowLayoutPanel1.Location = New System.Drawing.Point(153, 3)
		Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
		Me.FlowLayoutPanel1.Size = New System.Drawing.Size(502, 131)
		Me.FlowLayoutPanel1.TabIndex = 1
		'
		'Label1
		'
		Me.Label1.Font = New System.Drawing.Font("Tahoma", 16.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
		Me.Label1.Location = New System.Drawing.Point(3, 40)
		Me.Label1.Margin = New System.Windows.Forms.Padding(3, 40, 3, 0)
		Me.Label1.Name = "Label1"
		Me.Label1.Size = New System.Drawing.Size(451, 43)
		Me.Label1.TabIndex = 2
		Me.Label1.Text = "Water Footprint Evaluator"
		'
		'version_year_Label
		'
		Me.version_year_Label.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
		Me.version_year_Label.Location = New System.Drawing.Point(8, 83)
		Me.version_year_Label.Margin = New System.Windows.Forms.Padding(8, 0, 3, 0)
		Me.version_year_Label.Name = "version_year_Label"
		Me.version_year_Label.Size = New System.Drawing.Size(436, 23)
		Me.version_year_Label.TabIndex = 3
		Me.version_year_Label.Text = "Ver. 1.0.0.21"
		'
		'FlowLayoutPanel2
		'
		Me.FlowLayoutPanel2.Controls.Add(Me.Label5)
		Me.FlowLayoutPanel2.Controls.Add(Me.Label4)
		Me.FlowLayoutPanel2.Controls.Add(Me.Label6)
		Me.FlowLayoutPanel2.Controls.Add(Me.Label7)
		Me.FlowLayoutPanel2.Controls.Add(Me.Label8)
		Me.FlowLayoutPanel2.Controls.Add(Me.Label9)
		Me.FlowLayoutPanel2.Controls.Add(Me.Label10)
		Me.FlowLayoutPanel2.Controls.Add(Me.Panel1)
		Me.FlowLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill
		Me.FlowLayoutPanel2.Location = New System.Drawing.Point(153, 140)
		Me.FlowLayoutPanel2.Name = "FlowLayoutPanel2"
		Me.FlowLayoutPanel2.Size = New System.Drawing.Size(502, 275)
		Me.FlowLayoutPanel2.TabIndex = 4
		'
		'Label5
		'
		Me.Label5.Font = New System.Drawing.Font("Microsoft JhengHei", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
		Me.Label5.Location = New System.Drawing.Point(8, 0)
		Me.Label5.Margin = New System.Windows.Forms.Padding(8, 0, 3, 7)
		Me.Label5.Name = "Label5"
		Me.Label5.Size = New System.Drawing.Size(442, 22)
		Me.Label5.TabIndex = 4
		Me.Label5.Text = "國立高雄科技大學"
		'
		'Label4
		'
		Me.Label4.Font = New System.Drawing.Font("Microsoft JhengHei", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
		Me.Label4.Location = New System.Drawing.Point(8, 29)
		Me.Label4.Margin = New System.Windows.Forms.Padding(8, 0, 3, 7)
		Me.Label4.Name = "Label4"
		Me.Label4.Size = New System.Drawing.Size(442, 22)
		Me.Label4.TabIndex = 5
		Me.Label4.Text = "創新設計工程系．工業設計碩士班"
		'
		'Label6
		'
		Me.Label6.Font = New System.Drawing.Font("Microsoft JhengHei", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
		Me.Label6.Location = New System.Drawing.Point(8, 58)
		Me.Label6.Margin = New System.Windows.Forms.Padding(8, 0, 3, 7)
		Me.Label6.Name = "Label6"
		Me.Label6.Size = New System.Drawing.Size(442, 22)
		Me.Label6.TabIndex = 6
		Me.Label6.Text = "創新產品與永續設計實驗室"
		'
		'Label7
		'
		Me.Label7.Font = New System.Drawing.Font("Microsoft JhengHei", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
		Me.Label7.Location = New System.Drawing.Point(8, 87)
		Me.Label7.Margin = New System.Windows.Forms.Padding(8, 0, 3, 7)
		Me.Label7.Name = "Label7"
		Me.Label7.Size = New System.Drawing.Size(442, 22)
		Me.Label7.TabIndex = 7
		Me.Label7.Text = "National Kaohsiung University of Science and Technology"
		'
		'Label8
		'
		Me.Label8.Font = New System.Drawing.Font("Microsoft JhengHei", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
		Me.Label8.Location = New System.Drawing.Point(8, 116)
		Me.Label8.Margin = New System.Windows.Forms.Padding(8, 0, 3, 7)
		Me.Label8.Name = "Label8"
		Me.Label8.Size = New System.Drawing.Size(442, 22)
		Me.Label8.TabIndex = 8
		Me.Label8.Text = "Department of Innovation Design Engineering"
		'
		'Label9
		'
		Me.Label9.Font = New System.Drawing.Font("Microsoft JhengHei", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
		Me.Label9.Location = New System.Drawing.Point(8, 145)
		Me.Label9.Margin = New System.Windows.Forms.Padding(8, 0, 3, 7)
		Me.Label9.Name = "Label9"
		Me.Label9.Size = New System.Drawing.Size(442, 22)
		Me.Label9.TabIndex = 9
		Me.Label9.Text = "Graduate Program of Industrial Design"
		'
		'Label10
		'
		Me.Label10.Font = New System.Drawing.Font("Microsoft JhengHei", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
		Me.Label10.Location = New System.Drawing.Point(8, 174)
		Me.Label10.Margin = New System.Windows.Forms.Padding(8, 0, 3, 7)
		Me.Label10.Name = "Label10"
		Me.Label10.Size = New System.Drawing.Size(442, 22)
		Me.Label10.TabIndex = 10
		Me.Label10.Text = "Innovative Product and Sustainable Design Lab"
		'
		'Panel1
		'
		Me.Panel1.Controls.Add(Me.closeButton)
		Me.Panel1.Dock = System.Windows.Forms.DockStyle.Bottom
		Me.Panel1.Location = New System.Drawing.Point(3, 206)
		Me.Panel1.Name = "Panel1"
		Me.Panel1.Size = New System.Drawing.Size(485, 55)
		Me.Panel1.TabIndex = 11
		'
		'closeButton
		'
		Me.closeButton.Location = New System.Drawing.Point(409, 26)
		Me.closeButton.Name = "closeButton"
		Me.closeButton.Size = New System.Drawing.Size(75, 28)
		Me.closeButton.TabIndex = 0
		Me.closeButton.Text = "確定"
		Me.closeButton.UseVisualStyleBackColor = True
		'
		'CopyrightForm
		'
		Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 15.0!)
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.ClientSize = New System.Drawing.Size(658, 418)
		Me.ControlBox = False
		Me.Controls.Add(Me.TableLayoutPanel1)
		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
		Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
		Me.MaximizeBox = False
		Me.MaximumSize = New System.Drawing.Size(660, 420)
		Me.MinimizeBox = False
		Me.MinimumSize = New System.Drawing.Size(660, 420)
		Me.Name = "CopyrightForm"
		Me.ShowInTaskbar = False
		Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
		Me.TopMost = True
		Me.TableLayoutPanel1.ResumeLayout(False)
		CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
		Me.FlowLayoutPanel1.ResumeLayout(False)
		Me.FlowLayoutPanel2.ResumeLayout(False)
		Me.Panel1.ResumeLayout(False)
		Me.ResumeLayout(False)

	End Sub

	Friend WithEvents TableLayoutPanel1 As TableLayoutPanel
	Friend WithEvents PictureBox1 As PictureBox
	Friend WithEvents FlowLayoutPanel1 As FlowLayoutPanel
	Friend WithEvents Label1 As Label
	Friend WithEvents version_year_Label As Label
	Friend WithEvents FlowLayoutPanel2 As FlowLayoutPanel
	Friend WithEvents Label5 As Label
	Friend WithEvents Label4 As Label
	Friend WithEvents Label6 As Label
	Friend WithEvents Label7 As Label
	Friend WithEvents Label8 As Label
	Friend WithEvents Label9 As Label
	Friend WithEvents Label10 As Label
	Friend WithEvents Panel1 As Panel
	Friend WithEvents closeButton As Button
End Class
