﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class MaterialForm
	Inherits System.Windows.Forms.Form

	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> _
	Protected Overrides Sub Dispose(ByVal disposing As Boolean)
		Try
			If disposing AndAlso components IsNot Nothing Then
				components.Dispose()
			End If
		Finally
			MyBase.Dispose(disposing)
		End Try
	End Sub

	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer

	'NOTE: The following procedure is required by the Windows Form Designer
	'It can be modified using the Windows Form Designer.  
	'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> _
	Private Sub InitializeComponent()
		Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(MaterialForm))
		Me.Completed_Button = New System.Windows.Forms.Button()
		Me.Label5 = New System.Windows.Forms.Label()
		Me.M_NameFilter_TextBox = New System.Windows.Forms.TextBox()
		Me.CleanFilter_Button = New System.Windows.Forms.Button()
		Me.FlowLayoutPanel2 = New System.Windows.Forms.FlowLayoutPanel()
		Me.Label10 = New System.Windows.Forms.Label()
		Me.M_RecentUsed_ComboBox = New System.Windows.Forms.ComboBox()
		Me.Label6 = New System.Windows.Forms.Label()
		Me.M_SelectedMaterial_TextBox = New System.Windows.Forms.TextBox()
		Me.Label8 = New System.Windows.Forms.Label()
		Me.M_DataGridView = New System.Windows.Forms.DataGridView()
		Me.FlowLayoutPanel3 = New System.Windows.Forms.FlowLayoutPanel()
		Me.M_SelectedMaterial_WFP_TextBox = New System.Windows.Forms.TextBox()
		Me.FlowLayoutPanel4 = New System.Windows.Forms.FlowLayoutPanel()
		Me.Panel1 = New System.Windows.Forms.Panel()
		Me.Filter_Info_Label = New System.Windows.Forms.Label()
		Me.Label2 = New System.Windows.Forms.Label()
		Me.Label27 = New System.Windows.Forms.Label()
		Me.Label9 = New System.Windows.Forms.Label()
		Me.Label28 = New System.Windows.Forms.Label()
		Me.M_SelectedMaterial_ISO_TextBox = New System.Windows.Forms.TextBox()
		Me.Label3 = New System.Windows.Forms.Label()
		Me.GroupBox2 = New System.Windows.Forms.GroupBox()
		Me.ProductName_Label = New System.Windows.Forms.Label()
		Me.Label4 = New System.Windows.Forms.Label()
		Me.CapturedName_Label = New System.Windows.Forms.Label()
		Me.Label1 = New System.Windows.Forms.Label()
		Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
		Me.GroupBox1 = New System.Windows.Forms.GroupBox()
		Me.FlowLayoutPanel2.SuspendLayout()
		CType(Me.M_DataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.FlowLayoutPanel3.SuspendLayout()
		Me.FlowLayoutPanel4.SuspendLayout()
		Me.Panel1.SuspendLayout()
		Me.GroupBox2.SuspendLayout()
		Me.FlowLayoutPanel1.SuspendLayout()
		Me.GroupBox1.SuspendLayout()
		Me.SuspendLayout()
		'
		'Completed_Button
		'
		Me.Completed_Button.Location = New System.Drawing.Point(4, 714)
		Me.Completed_Button.Margin = New System.Windows.Forms.Padding(4)
		Me.Completed_Button.Name = "Completed_Button"
		Me.Completed_Button.Size = New System.Drawing.Size(661, 40)
		Me.Completed_Button.TabIndex = 3
		Me.Completed_Button.Text = "完成"
		Me.Completed_Button.UseVisualStyleBackColor = True
		'
		'Label5
		'
		Me.Label5.AutoSize = True
		Me.Label5.Location = New System.Drawing.Point(4, 10)
		Me.Label5.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label5.Name = "Label5"
		Me.Label5.Size = New System.Drawing.Size(97, 15)
		Me.Label5.TabIndex = 0
		Me.Label5.Text = "材料名稱篩選"
		'
		'M_NameFilter_TextBox
		'
		Me.M_NameFilter_TextBox.Location = New System.Drawing.Point(109, 4)
		Me.M_NameFilter_TextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.M_NameFilter_TextBox.Name = "M_NameFilter_TextBox"
		Me.M_NameFilter_TextBox.Size = New System.Drawing.Size(432, 25)
		Me.M_NameFilter_TextBox.TabIndex = 1
		'
		'CleanFilter_Button
		'
		Me.CleanFilter_Button.Location = New System.Drawing.Point(549, 4)
		Me.CleanFilter_Button.Margin = New System.Windows.Forms.Padding(4)
		Me.CleanFilter_Button.Name = "CleanFilter_Button"
		Me.CleanFilter_Button.Size = New System.Drawing.Size(100, 29)
		Me.CleanFilter_Button.TabIndex = 2
		Me.CleanFilter_Button.Text = "清除篩選"
		Me.CleanFilter_Button.UseVisualStyleBackColor = True
		'
		'FlowLayoutPanel2
		'
		Me.FlowLayoutPanel2.Controls.Add(Me.Label5)
		Me.FlowLayoutPanel2.Controls.Add(Me.M_NameFilter_TextBox)
		Me.FlowLayoutPanel2.Controls.Add(Me.CleanFilter_Button)
		Me.FlowLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Top
		Me.FlowLayoutPanel2.Location = New System.Drawing.Point(4, 22)
		Me.FlowLayoutPanel2.Margin = New System.Windows.Forms.Padding(4)
		Me.FlowLayoutPanel2.Name = "FlowLayoutPanel2"
		Me.FlowLayoutPanel2.Size = New System.Drawing.Size(657, 39)
		Me.FlowLayoutPanel2.TabIndex = 3
		'
		'Label10
		'
		Me.Label10.AutoSize = True
		Me.Label10.Location = New System.Drawing.Point(4, 10)
		Me.Label10.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label10.Name = "Label10"
		Me.Label10.Size = New System.Drawing.Size(112, 15)
		Me.Label10.TabIndex = 0
		Me.Label10.Text = "最近曾使用材料"
		'
		'M_RecentUsed_ComboBox
		'
		Me.M_RecentUsed_ComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
		Me.M_RecentUsed_ComboBox.Font = New System.Drawing.Font("PMingLiU", 10.0!)
		Me.M_RecentUsed_ComboBox.FormattingEnabled = True
		Me.M_RecentUsed_ComboBox.Location = New System.Drawing.Point(124, 4)
		Me.M_RecentUsed_ComboBox.Margin = New System.Windows.Forms.Padding(4)
		Me.M_RecentUsed_ComboBox.Name = "M_RecentUsed_ComboBox"
		Me.M_RecentUsed_ComboBox.Size = New System.Drawing.Size(417, 25)
		Me.M_RecentUsed_ComboBox.TabIndex = 1
		'
		'Label6
		'
		Me.Label6.Location = New System.Drawing.Point(4, 418)
		Me.Label6.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label6.Name = "Label6"
		Me.Label6.Size = New System.Drawing.Size(87, 15)
		Me.Label6.TabIndex = 0
		Me.Label6.Text = "材料名稱"
		'
		'M_SelectedMaterial_TextBox
		'
		Me.M_SelectedMaterial_TextBox.Location = New System.Drawing.Point(99, 412)
		Me.M_SelectedMaterial_TextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.M_SelectedMaterial_TextBox.Name = "M_SelectedMaterial_TextBox"
		Me.M_SelectedMaterial_TextBox.ReadOnly = True
		Me.M_SelectedMaterial_TextBox.Size = New System.Drawing.Size(481, 25)
		Me.M_SelectedMaterial_TextBox.TabIndex = 1
		'
		'Label8
		'
		Me.Label8.Location = New System.Drawing.Point(4, 451)
		Me.Label8.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label8.Name = "Label8"
		Me.Label8.Size = New System.Drawing.Size(97, 19)
		Me.Label8.TabIndex = 3
		Me.Label8.Text = "材料水足跡"
		'
		'M_DataGridView
		'
		Me.M_DataGridView.AllowUserToAddRows = False
		Me.M_DataGridView.AllowUserToDeleteRows = False
		Me.M_DataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
		Me.M_DataGridView.Location = New System.Drawing.Point(4, 56)
		Me.M_DataGridView.Margin = New System.Windows.Forms.Padding(4)
		Me.M_DataGridView.Name = "M_DataGridView"
		Me.M_DataGridView.ReadOnly = True
		Me.M_DataGridView.RowTemplate.Height = 24
		Me.M_DataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
		Me.M_DataGridView.Size = New System.Drawing.Size(643, 348)
		Me.M_DataGridView.TabIndex = 7
		'
		'FlowLayoutPanel3
		'
		Me.FlowLayoutPanel3.Controls.Add(Me.Label10)
		Me.FlowLayoutPanel3.Controls.Add(Me.M_RecentUsed_ComboBox)
		Me.FlowLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Top
		Me.FlowLayoutPanel3.Location = New System.Drawing.Point(4, 61)
		Me.FlowLayoutPanel3.Margin = New System.Windows.Forms.Padding(4)
		Me.FlowLayoutPanel3.Name = "FlowLayoutPanel3"
		Me.FlowLayoutPanel3.Size = New System.Drawing.Size(657, 34)
		Me.FlowLayoutPanel3.TabIndex = 6
		'
		'M_SelectedMaterial_WFP_TextBox
		'
		Me.M_SelectedMaterial_WFP_TextBox.Location = New System.Drawing.Point(156, 445)
		Me.M_SelectedMaterial_WFP_TextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.M_SelectedMaterial_WFP_TextBox.Name = "M_SelectedMaterial_WFP_TextBox"
		Me.M_SelectedMaterial_WFP_TextBox.ReadOnly = True
		Me.M_SelectedMaterial_WFP_TextBox.Size = New System.Drawing.Size(424, 25)
		Me.M_SelectedMaterial_WFP_TextBox.TabIndex = 4
		'
		'FlowLayoutPanel4
		'
		Me.FlowLayoutPanel4.BackColor = System.Drawing.Color.Transparent
		Me.FlowLayoutPanel4.Controls.Add(Me.Panel1)
		Me.FlowLayoutPanel4.Controls.Add(Me.M_DataGridView)
		Me.FlowLayoutPanel4.Controls.Add(Me.Label6)
		Me.FlowLayoutPanel4.Controls.Add(Me.M_SelectedMaterial_TextBox)
		Me.FlowLayoutPanel4.Controls.Add(Me.Label8)
		Me.FlowLayoutPanel4.Controls.Add(Me.Label27)
		Me.FlowLayoutPanel4.Controls.Add(Me.M_SelectedMaterial_WFP_TextBox)
		Me.FlowLayoutPanel4.Controls.Add(Me.Label9)
		Me.FlowLayoutPanel4.Controls.Add(Me.Label28)
		Me.FlowLayoutPanel4.Controls.Add(Me.M_SelectedMaterial_ISO_TextBox)
		Me.FlowLayoutPanel4.Controls.Add(Me.Label3)
		Me.FlowLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill
		Me.FlowLayoutPanel4.Location = New System.Drawing.Point(4, 95)
		Me.FlowLayoutPanel4.Margin = New System.Windows.Forms.Padding(4)
		Me.FlowLayoutPanel4.Name = "FlowLayoutPanel4"
		Me.FlowLayoutPanel4.Size = New System.Drawing.Size(657, 513)
		Me.FlowLayoutPanel4.TabIndex = 8
		'
		'Panel1
		'
		Me.Panel1.Controls.Add(Me.Filter_Info_Label)
		Me.Panel1.Controls.Add(Me.Label2)
		Me.Panel1.Location = New System.Drawing.Point(4, 4)
		Me.Panel1.Margin = New System.Windows.Forms.Padding(4)
		Me.Panel1.Name = "Panel1"
		Me.Panel1.Size = New System.Drawing.Size(643, 44)
		Me.Panel1.TabIndex = 8
		'
		'Filter_Info_Label
		'
		Me.Filter_Info_Label.AutoSize = True
		Me.Filter_Info_Label.ForeColor = System.Drawing.Color.Magenta
		Me.Filter_Info_Label.Location = New System.Drawing.Point(155, 18)
		Me.Filter_Info_Label.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
		Me.Filter_Info_Label.Name = "Filter_Info_Label"
		Me.Filter_Info_Label.Size = New System.Drawing.Size(31, 15)
		Me.Filter_Info_Label.TabIndex = 1
		Me.Filter_Info_Label.Text = "N/A"
		'
		'Label2
		'
		Me.Label2.AutoSize = True
		Me.Label2.Location = New System.Drawing.Point(4, 18)
		Me.Label2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
		Me.Label2.Name = "Label2"
		Me.Label2.Size = New System.Drawing.Size(136, 15)
		Me.Label2.TabIndex = 0
		Me.Label2.Text = "篩選筆數 / 總筆數 : "
		'
		'Label27
		'
		Me.Label27.AutoSize = True
		Me.Label27.Font = New System.Drawing.Font("PMingLiU", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
		Me.Label27.ForeColor = System.Drawing.Color.Purple
		Me.Label27.Location = New System.Drawing.Point(108, 451)
		Me.Label27.Margin = New System.Windows.Forms.Padding(3, 10, 3, 0)
		Me.Label27.Name = "Label27"
		Me.Label27.Size = New System.Drawing.Size(41, 15)
		Me.Label27.TabIndex = 32
		Me.Label27.Text = "WFN"
		'
		'Label9
		'
		Me.Label9.AutoSize = True
		Me.Label9.Location = New System.Drawing.Point(588, 451)
		Me.Label9.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label9.Name = "Label9"
		Me.Label9.Size = New System.Drawing.Size(53, 15)
		Me.Label9.TabIndex = 5
		Me.Label9.Text = "m^3/Kg"
		'
		'Label28
		'
		Me.Label28.Font = New System.Drawing.Font("PMingLiU", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
		Me.Label28.ForeColor = System.Drawing.Color.Black
		Me.Label28.Location = New System.Drawing.Point(3, 484)
		Me.Label28.Margin = New System.Windows.Forms.Padding(3, 10, 3, 0)
		Me.Label28.Name = "Label28"
		Me.Label28.Size = New System.Drawing.Size(146, 19)
		Me.Label28.TabIndex = 33
		Me.Label28.Text = "ISO 14046"
		Me.Label28.TextAlign = System.Drawing.ContentAlignment.TopRight
		'
		'M_SelectedMaterial_ISO_TextBox
		'
		Me.M_SelectedMaterial_ISO_TextBox.Location = New System.Drawing.Point(156, 478)
		Me.M_SelectedMaterial_ISO_TextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.M_SelectedMaterial_ISO_TextBox.Name = "M_SelectedMaterial_ISO_TextBox"
		Me.M_SelectedMaterial_ISO_TextBox.ReadOnly = True
		Me.M_SelectedMaterial_ISO_TextBox.Size = New System.Drawing.Size(424, 25)
		Me.M_SelectedMaterial_ISO_TextBox.TabIndex = 34
		'
		'Label3
		'
		Me.Label3.AutoSize = True
		Me.Label3.Location = New System.Drawing.Point(588, 484)
		Me.Label3.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label3.Name = "Label3"
		Me.Label3.Size = New System.Drawing.Size(53, 15)
		Me.Label3.TabIndex = 35
		Me.Label3.Text = "m^3/Kg"
		'
		'GroupBox2
		'
		Me.GroupBox2.Controls.Add(Me.FlowLayoutPanel4)
		Me.GroupBox2.Controls.Add(Me.FlowLayoutPanel3)
		Me.GroupBox2.Controls.Add(Me.FlowLayoutPanel2)
		Me.GroupBox2.ForeColor = System.Drawing.Color.DarkGreen
		Me.GroupBox2.Location = New System.Drawing.Point(4, 94)
		Me.GroupBox2.Margin = New System.Windows.Forms.Padding(4)
		Me.GroupBox2.Name = "GroupBox2"
		Me.GroupBox2.Padding = New System.Windows.Forms.Padding(4)
		Me.GroupBox2.Size = New System.Drawing.Size(665, 612)
		Me.GroupBox2.TabIndex = 1
		Me.GroupBox2.TabStop = False
		Me.GroupBox2.Text = "選擇材料"
		'
		'ProductName_Label
		'
		Me.ProductName_Label.AutoSize = True
		Me.ProductName_Label.ForeColor = System.Drawing.Color.Red
		Me.ProductName_Label.Location = New System.Drawing.Point(148, 52)
		Me.ProductName_Label.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
		Me.ProductName_Label.Name = "ProductName_Label"
		Me.ProductName_Label.Size = New System.Drawing.Size(31, 15)
		Me.ProductName_Label.TabIndex = 3
		Me.ProductName_Label.Text = "N/A"
		'
		'Label4
		'
		Me.Label4.AutoSize = True
		Me.Label4.Location = New System.Drawing.Point(12, 52)
		Me.Label4.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
		Me.Label4.Name = "Label4"
		Me.Label4.Size = New System.Drawing.Size(97, 15)
		Me.Label4.TabIndex = 2
		Me.Label4.Text = "所屬專案名稱"
		'
		'CapturedName_Label
		'
		Me.CapturedName_Label.AutoSize = True
		Me.CapturedName_Label.ForeColor = System.Drawing.Color.Blue
		Me.CapturedName_Label.Location = New System.Drawing.Point(148, 22)
		Me.CapturedName_Label.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
		Me.CapturedName_Label.Name = "CapturedName_Label"
		Me.CapturedName_Label.Size = New System.Drawing.Size(31, 15)
		Me.CapturedName_Label.TabIndex = 1
		Me.CapturedName_Label.Text = "N/A"
		'
		'Label1
		'
		Me.Label1.AutoSize = True
		Me.Label1.Location = New System.Drawing.Point(12, 22)
		Me.Label1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
		Me.Label1.Name = "Label1"
		Me.Label1.Size = New System.Drawing.Size(67, 15)
		Me.Label1.TabIndex = 0
		Me.Label1.Text = "零件名稱"
		'
		'FlowLayoutPanel1
		'
		Me.FlowLayoutPanel1.Controls.Add(Me.GroupBox1)
		Me.FlowLayoutPanel1.Controls.Add(Me.GroupBox2)
		Me.FlowLayoutPanel1.Controls.Add(Me.Completed_Button)
		Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
		Me.FlowLayoutPanel1.Location = New System.Drawing.Point(0, 0)
		Me.FlowLayoutPanel1.Margin = New System.Windows.Forms.Padding(4)
		Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
		Me.FlowLayoutPanel1.Size = New System.Drawing.Size(677, 760)
		Me.FlowLayoutPanel1.TabIndex = 3
		'
		'GroupBox1
		'
		Me.GroupBox1.Controls.Add(Me.ProductName_Label)
		Me.GroupBox1.Controls.Add(Me.Label4)
		Me.GroupBox1.Controls.Add(Me.CapturedName_Label)
		Me.GroupBox1.Controls.Add(Me.Label1)
		Me.GroupBox1.Location = New System.Drawing.Point(4, 4)
		Me.GroupBox1.Margin = New System.Windows.Forms.Padding(4)
		Me.GroupBox1.Name = "GroupBox1"
		Me.GroupBox1.Padding = New System.Windows.Forms.Padding(4)
		Me.GroupBox1.Size = New System.Drawing.Size(665, 82)
		Me.GroupBox1.TabIndex = 0
		Me.GroupBox1.TabStop = False
		'
		'MaterialForm
		'
		Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 15.0!)
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.ClientSize = New System.Drawing.Size(677, 760)
		Me.Controls.Add(Me.FlowLayoutPanel1)
		Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
		Me.Margin = New System.Windows.Forms.Padding(4)
		Me.Name = "MaterialForm"
		Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
		Me.Text = "選擇材料"
		Me.TopMost = True
		Me.FlowLayoutPanel2.ResumeLayout(False)
		Me.FlowLayoutPanel2.PerformLayout()
		CType(Me.M_DataGridView, System.ComponentModel.ISupportInitialize).EndInit()
		Me.FlowLayoutPanel3.ResumeLayout(False)
		Me.FlowLayoutPanel3.PerformLayout()
		Me.FlowLayoutPanel4.ResumeLayout(False)
		Me.FlowLayoutPanel4.PerformLayout()
		Me.Panel1.ResumeLayout(False)
		Me.Panel1.PerformLayout()
		Me.GroupBox2.ResumeLayout(False)
		Me.FlowLayoutPanel1.ResumeLayout(False)
		Me.GroupBox1.ResumeLayout(False)
		Me.GroupBox1.PerformLayout()
		Me.ResumeLayout(False)

	End Sub

	Friend WithEvents Completed_Button As Button
	Friend WithEvents Label5 As Label
	Friend WithEvents M_NameFilter_TextBox As TextBox
	Friend WithEvents CleanFilter_Button As Button
	Friend WithEvents FlowLayoutPanel2 As FlowLayoutPanel
	Friend WithEvents Label10 As Label
	Friend WithEvents M_RecentUsed_ComboBox As ComboBox
	Friend WithEvents Label6 As Label
	Friend WithEvents M_SelectedMaterial_TextBox As TextBox
	Friend WithEvents Label8 As Label
	Friend WithEvents M_DataGridView As DataGridView
	Friend WithEvents FlowLayoutPanel3 As FlowLayoutPanel
	Friend WithEvents M_SelectedMaterial_WFP_TextBox As TextBox
	Friend WithEvents FlowLayoutPanel4 As FlowLayoutPanel
	Friend WithEvents Label9 As Label
	Friend WithEvents GroupBox2 As GroupBox
	Friend WithEvents ProductName_Label As Label
	Friend WithEvents Label4 As Label
	Friend WithEvents CapturedName_Label As Label
	Friend WithEvents Label1 As Label
	Friend WithEvents FlowLayoutPanel1 As FlowLayoutPanel
	Friend WithEvents GroupBox1 As GroupBox
	Friend WithEvents Panel1 As Panel
	Friend WithEvents Filter_Info_Label As Label
	Friend WithEvents Label2 As Label
	Friend WithEvents Label28 As Label
	Friend WithEvents Label27 As Label
	Friend WithEvents M_SelectedMaterial_ISO_TextBox As TextBox
	Friend WithEvents Label3 As Label
End Class
