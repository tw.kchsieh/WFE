﻿Imports WFE.Assistants.Controllers

Public Class ProductForm
	Private _dc As DataController
	Private _pController As ProductController
	Friend Sub UseController(dc As DataController)
		'assign
		_dc = dc
	End Sub
	Private Sub ProductForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
		EmptyLabel.Text = ""
		EmptyLabel2.Text = ""
		_pController = New ProductController()
		_pController.Setup(Name_Filter_TextBox,
						   RecentUsed_ComboBox,
						   FilterInfo_Label,
						   Product_DataGridView,
						   ProductName_TextBox,
						   StageA_WFP_TextBox,
						   StageB_WFP_TextBox,
						   StageC_WFP_TextBox,
						   Total_WFP_TextBox,
						   StageA_ISO_TextBox,
						   StageB_ISO_TextBox,
						   StageC_ISO_TextBox,
							Total_ISO_TextBox,
							Stage_Asm_WFP_TextBox,
							Stage_Asm_ISO_TextBox,
						   _dc)
	End Sub

	Private Sub Clear_Filter_Button_Click(sender As Object, e As EventArgs) Handles Clear_Filter_Button.Click
		'Clear
		Name_Filter_TextBox.Text = ""
	End Sub

	Private Sub LoadProduct_Button_Click(sender As Object, e As EventArgs) Handles LoadProduct_Button.Click
		'Load Product and save RecentUsedProduct
		Dim success As Boolean = _pController.LoadProduct()
		If success Then
			Me.DialogResult = DialogResult.OK
			Me.Close()
		End If
	End Sub

	Private Sub DeleteButton_Click(sender As Object, e As EventArgs) Handles DeleteButton.Click
		'Delete Product
		_pController.Delete()
	End Sub
End Class