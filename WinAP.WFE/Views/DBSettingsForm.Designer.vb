﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class DBSettingsForm
	Inherits System.Windows.Forms.Form

	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()>
	Protected Overrides Sub Dispose(ByVal disposing As Boolean)
		Try
			If disposing AndAlso components IsNot Nothing Then
				components.Dispose()
			End If
		Finally
			MyBase.Dispose(disposing)
		End Try
	End Sub

	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer

	'NOTE: The following procedure is required by the Windows Form Designer
	'It can be modified using the Windows Form Designer.  
	'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()>
	Private Sub InitializeComponent()
		Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(DBSettingsForm))
		Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
		Me.Label1 = New System.Windows.Forms.Label()
		Me.DBSource_TextBox = New System.Windows.Forms.TextBox()
		Me.BrowserFile_Button = New System.Windows.Forms.Button()
		Me.ConnectionTest_Button = New System.Windows.Forms.Button()
		Me.SaveChange_Button = New System.Windows.Forms.Button()
		Me.FlowLayoutPanel1.SuspendLayout()
		Me.SuspendLayout()
		'
		'FlowLayoutPanel1
		'
		Me.FlowLayoutPanel1.Controls.Add(Me.Label1)
		Me.FlowLayoutPanel1.Controls.Add(Me.DBSource_TextBox)
		Me.FlowLayoutPanel1.Controls.Add(Me.BrowserFile_Button)
		Me.FlowLayoutPanel1.Controls.Add(Me.ConnectionTest_Button)
		Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
		Me.FlowLayoutPanel1.Location = New System.Drawing.Point(0, 0)
		Me.FlowLayoutPanel1.Margin = New System.Windows.Forms.Padding(4)
		Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
		Me.FlowLayoutPanel1.Size = New System.Drawing.Size(559, 51)
		Me.FlowLayoutPanel1.TabIndex = 0
		'
		'Label1
		'
		Me.Label1.AutoSize = True
		Me.Label1.Location = New System.Drawing.Point(4, 10)
		Me.Label1.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label1.Name = "Label1"
		Me.Label1.Size = New System.Drawing.Size(82, 15)
		Me.Label1.TabIndex = 0
		Me.Label1.Text = "資料庫來源"
		'
		'DBSource_TextBox
		'
		Me.DBSource_TextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.DBSource_TextBox.Location = New System.Drawing.Point(94, 4)
		Me.DBSource_TextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.DBSource_TextBox.Multiline = True
		Me.DBSource_TextBox.Name = "DBSource_TextBox"
		Me.DBSource_TextBox.ReadOnly = True
		Me.DBSource_TextBox.Size = New System.Drawing.Size(229, 28)
		Me.DBSource_TextBox.TabIndex = 1
		'
		'BrowserFile_Button
		'
		Me.BrowserFile_Button.Location = New System.Drawing.Point(331, 4)
		Me.BrowserFile_Button.Margin = New System.Windows.Forms.Padding(4)
		Me.BrowserFile_Button.Name = "BrowserFile_Button"
		Me.BrowserFile_Button.Size = New System.Drawing.Size(100, 29)
		Me.BrowserFile_Button.TabIndex = 2
		Me.BrowserFile_Button.Text = "瀏覽"
		Me.BrowserFile_Button.UseVisualStyleBackColor = True
		'
		'ConnectionTest_Button
		'
		Me.ConnectionTest_Button.Location = New System.Drawing.Point(439, 4)
		Me.ConnectionTest_Button.Margin = New System.Windows.Forms.Padding(4)
		Me.ConnectionTest_Button.Name = "ConnectionTest_Button"
		Me.ConnectionTest_Button.Size = New System.Drawing.Size(100, 29)
		Me.ConnectionTest_Button.TabIndex = 3
		Me.ConnectionTest_Button.Text = "測試連線"
		Me.ConnectionTest_Button.UseVisualStyleBackColor = True
		'
		'SaveChange_Button
		'
		Me.SaveChange_Button.Dock = System.Windows.Forms.DockStyle.Bottom
		Me.SaveChange_Button.Location = New System.Drawing.Point(0, 51)
		Me.SaveChange_Button.Margin = New System.Windows.Forms.Padding(4)
		Me.SaveChange_Button.Name = "SaveChange_Button"
		Me.SaveChange_Button.Size = New System.Drawing.Size(559, 38)
		Me.SaveChange_Button.TabIndex = 4
		Me.SaveChange_Button.Text = "儲存設定"
		Me.SaveChange_Button.UseVisualStyleBackColor = True
		'
		'DBSettingsForm
		'
		Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 15.0!)
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.BackColor = System.Drawing.Color.WhiteSmoke
		Me.ClientSize = New System.Drawing.Size(559, 89)
		Me.Controls.Add(Me.FlowLayoutPanel1)
		Me.Controls.Add(Me.SaveChange_Button)
		Me.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
		Me.Margin = New System.Windows.Forms.Padding(4)
		Me.Name = "DBSettingsForm"
		Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
		Me.Text = "資料來源設定"
		Me.TopMost = True
		Me.FlowLayoutPanel1.ResumeLayout(False)
		Me.FlowLayoutPanel1.PerformLayout()
		Me.ResumeLayout(False)

	End Sub

	Friend WithEvents FlowLayoutPanel1 As FlowLayoutPanel
	Friend WithEvents Label1 As Label
	Friend WithEvents DBSource_TextBox As TextBox
	Friend WithEvents BrowserFile_Button As Button
	Friend WithEvents ConnectionTest_Button As Button
	Friend WithEvents SaveChange_Button As Button
End Class
