﻿Imports WFE.Assistants.Controllers
Public Class MaterialForm
	Private _dc As DataController
	Private _viewController As MaterialFormController
	Public Sub New(dc As DataController)
		Me._dc = dc
		InitializeComponent()
	End Sub

	Private Sub MaterialForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
		CapturedName_Label.Text = _dc.StageAViewModel.Component_Name
		ProductName_Label.Text = _dc.ProductDTO.ProductName
		'====
		_viewController = New MaterialFormController()
		_viewController.Setup(M_DataGridView,
							   M_NameFilter_TextBox,
							   M_RecentUsed_ComboBox,
							   M_SelectedMaterial_TextBox,
							   M_SelectedMaterial_WFP_TextBox,
							   M_SelectedMaterial_ISO_TextBox,
							   Filter_Info_Label)
	End Sub

	Private Sub Material_NameFilter_TextBox_TextChanged(sender As Object, e As EventArgs) Handles M_NameFilter_TextBox.TextChanged
		'篩選
		Dim filter = M_NameFilter_TextBox.Text.Trim()
		_viewController.Filter(filter)
	End Sub

	Private Sub CleanFilter_Button_Click(sender As Object, e As EventArgs) Handles CleanFilter_Button.Click
		'清除篩選
		M_NameFilter_TextBox.Text = ""
	End Sub

	Private Sub RecentUsed_ComboBox_SelectedIndexChanged(sender As Object, e As EventArgs) Handles M_RecentUsed_ComboBox.SelectedIndexChanged
		'最近使用的
	End Sub

	Private Sub Completed_Button_Click(sender As Object, e As EventArgs) Handles Completed_Button.Click
		'完成
		If (_viewController.GetSelectedMaterialModel() IsNot Nothing) Then
			Dim success = _viewController.SaveChoiceItem()
			If success = False Then
				MsgBox(_viewController.GetErrMsg())
			Else
				'close
				_dc.SelectedMaterialModel = _viewController.GetSelectedMaterialModel()
				DialogResult = DialogResult.OK
				Close()
			End If
		Else
			MsgBox("請選擇材料")
		End If

	End Sub


End Class