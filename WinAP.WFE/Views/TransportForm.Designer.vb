﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class TransportForm
	Inherits System.Windows.Forms.Form

	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()>
	Protected Overrides Sub Dispose(ByVal disposing As Boolean)
		Try
			If disposing AndAlso components IsNot Nothing Then
				components.Dispose()
			End If
		Finally
			MyBase.Dispose(disposing)
		End Try
	End Sub

	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer

	'NOTE: The following procedure is required by the Windows Form Designer
	'It can be modified using the Windows Form Designer.  
	'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()>
	Private Sub InitializeComponent()
		Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(TransportForm))
		Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
		Me.Product_Info_GroupBox = New System.Windows.Forms.GroupBox()
		Me.Component_Mass_Label = New System.Windows.Forms.Label()
		Me.CaptionMass_Label = New System.Windows.Forms.Label()
		Me.Component_Name_Label = New System.Windows.Forms.Label()
		Me.CaptionName_Label = New System.Windows.Forms.Label()
		Me.GroupBox2 = New System.Windows.Forms.GroupBox()
		Me.Panel1 = New System.Windows.Forms.Panel()
		Me.Filter_Info_Label = New System.Windows.Forms.Label()
		Me.Label2 = New System.Windows.Forms.Label()
		Me.AddTransport_Button = New System.Windows.Forms.Button()
		Me.FlowLayoutPanel4 = New System.Windows.Forms.FlowLayoutPanel()
		Me.Label1 = New System.Windows.Forms.Label()
		Me.Selected_Name_TextBox = New System.Windows.Forms.TextBox()
		Me.Label8 = New System.Windows.Forms.Label()
		Me.Label27 = New System.Windows.Forms.Label()
		Me.Transport_WFP_TextBox = New System.Windows.Forms.TextBox()
		Me.Label9 = New System.Windows.Forms.Label()
		Me.Label28 = New System.Windows.Forms.Label()
		Me.Transport_ISO_TextBox = New System.Windows.Forms.TextBox()
		Me.Label3 = New System.Windows.Forms.Label()
		Me.Label11 = New System.Windows.Forms.Label()
		Me.Transport_Distance_TextBox = New System.Windows.Forms.TextBox()
		Me.Label12 = New System.Windows.Forms.Label()
		Me.CaptionWFP_Label = New System.Windows.Forms.Label()
		Me.Label4 = New System.Windows.Forms.Label()
		Me.WFP_Label = New System.Windows.Forms.Label()
		Me.Label7 = New System.Windows.Forms.Label()
		Me.Label6 = New System.Windows.Forms.Label()
		Me.ISO_Label = New System.Windows.Forms.Label()
		Me.Label13 = New System.Windows.Forms.Label()
		Me.Transport_DataGridView = New System.Windows.Forms.DataGridView()
		Me.FlowLayoutPanel3 = New System.Windows.Forms.FlowLayoutPanel()
		Me.Label10 = New System.Windows.Forms.Label()
		Me.RecentUsed_ComboBox = New System.Windows.Forms.ComboBox()
		Me.FlowLayoutPanel2 = New System.Windows.Forms.FlowLayoutPanel()
		Me.Label5 = New System.Windows.Forms.Label()
		Me.NameFilter_TextBox = New System.Windows.Forms.TextBox()
		Me.Clean_Filter_Button = New System.Windows.Forms.Button()
		Me.GroupBox3 = New System.Windows.Forms.GroupBox()
		Me.DeleteRecord_Button = New System.Windows.Forms.Button()
		Me.Transport_Records_DataGridView = New System.Windows.Forms.DataGridView()
		Me.Completed_Button = New System.Windows.Forms.Button()
		Me.FlowLayoutPanel1.SuspendLayout()
		Me.Product_Info_GroupBox.SuspendLayout()
		Me.GroupBox2.SuspendLayout()
		Me.Panel1.SuspendLayout()
		Me.FlowLayoutPanel4.SuspendLayout()
		CType(Me.Transport_DataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.FlowLayoutPanel3.SuspendLayout()
		Me.FlowLayoutPanel2.SuspendLayout()
		Me.GroupBox3.SuspendLayout()
		CType(Me.Transport_Records_DataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.SuspendLayout()
		'
		'FlowLayoutPanel1
		'
		Me.FlowLayoutPanel1.Controls.Add(Me.Product_Info_GroupBox)
		Me.FlowLayoutPanel1.Controls.Add(Me.GroupBox2)
		Me.FlowLayoutPanel1.Controls.Add(Me.GroupBox3)
		Me.FlowLayoutPanel1.Controls.Add(Me.Completed_Button)
		Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
		Me.FlowLayoutPanel1.Location = New System.Drawing.Point(0, 0)
		Me.FlowLayoutPanel1.Margin = New System.Windows.Forms.Padding(4)
		Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
		Me.FlowLayoutPanel1.Size = New System.Drawing.Size(677, 1033)
		Me.FlowLayoutPanel1.TabIndex = 2
		'
		'Product_Info_GroupBox
		'
		Me.Product_Info_GroupBox.Controls.Add(Me.Component_Mass_Label)
		Me.Product_Info_GroupBox.Controls.Add(Me.CaptionMass_Label)
		Me.Product_Info_GroupBox.Controls.Add(Me.Component_Name_Label)
		Me.Product_Info_GroupBox.Controls.Add(Me.CaptionName_Label)
		Me.Product_Info_GroupBox.Location = New System.Drawing.Point(4, 4)
		Me.Product_Info_GroupBox.Margin = New System.Windows.Forms.Padding(4)
		Me.Product_Info_GroupBox.Name = "Product_Info_GroupBox"
		Me.Product_Info_GroupBox.Padding = New System.Windows.Forms.Padding(4)
		Me.Product_Info_GroupBox.Size = New System.Drawing.Size(660, 65)
		Me.Product_Info_GroupBox.TabIndex = 0
		Me.Product_Info_GroupBox.TabStop = False
		Me.Product_Info_GroupBox.Text = "專案 : N/A"
		'
		'Component_Mass_Label
		'
		Me.Component_Mass_Label.AutoSize = True
		Me.Component_Mass_Label.ForeColor = System.Drawing.Color.Magenta
		Me.Component_Mass_Label.Location = New System.Drawing.Point(148, 42)
		Me.Component_Mass_Label.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
		Me.Component_Mass_Label.Name = "Component_Mass_Label"
		Me.Component_Mass_Label.Size = New System.Drawing.Size(31, 15)
		Me.Component_Mass_Label.TabIndex = 5
		Me.Component_Mass_Label.Text = "N/A"
		'
		'CaptionMass_Label
		'
		Me.CaptionMass_Label.AutoSize = True
		Me.CaptionMass_Label.Location = New System.Drawing.Point(12, 42)
		Me.CaptionMass_Label.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
		Me.CaptionMass_Label.Name = "CaptionMass_Label"
		Me.CaptionMass_Label.Size = New System.Drawing.Size(109, 15)
		Me.CaptionMass_Label.TabIndex = 4
		Me.CaptionMass_Label.Text = "零件 / 原料質量"
		'
		'Component_Name_Label
		'
		Me.Component_Name_Label.AutoSize = True
		Me.Component_Name_Label.ForeColor = System.Drawing.Color.Blue
		Me.Component_Name_Label.Location = New System.Drawing.Point(148, 22)
		Me.Component_Name_Label.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
		Me.Component_Name_Label.Name = "Component_Name_Label"
		Me.Component_Name_Label.Size = New System.Drawing.Size(31, 15)
		Me.Component_Name_Label.TabIndex = 1
		Me.Component_Name_Label.Text = "N/A"
		'
		'CaptionName_Label
		'
		Me.CaptionName_Label.AutoSize = True
		Me.CaptionName_Label.Location = New System.Drawing.Point(12, 22)
		Me.CaptionName_Label.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
		Me.CaptionName_Label.Name = "CaptionName_Label"
		Me.CaptionName_Label.Size = New System.Drawing.Size(109, 15)
		Me.CaptionName_Label.TabIndex = 0
		Me.CaptionName_Label.Text = "零件 / 原料名稱"
		'
		'GroupBox2
		'
		Me.GroupBox2.Controls.Add(Me.Panel1)
		Me.GroupBox2.Controls.Add(Me.AddTransport_Button)
		Me.GroupBox2.Controls.Add(Me.FlowLayoutPanel4)
		Me.GroupBox2.Controls.Add(Me.Transport_DataGridView)
		Me.GroupBox2.Controls.Add(Me.FlowLayoutPanel3)
		Me.GroupBox2.Controls.Add(Me.FlowLayoutPanel2)
		Me.GroupBox2.ForeColor = System.Drawing.SystemColors.Highlight
		Me.GroupBox2.Location = New System.Drawing.Point(4, 77)
		Me.GroupBox2.Margin = New System.Windows.Forms.Padding(4)
		Me.GroupBox2.Name = "GroupBox2"
		Me.GroupBox2.Padding = New System.Windows.Forms.Padding(4)
		Me.GroupBox2.Size = New System.Drawing.Size(660, 595)
		Me.GroupBox2.TabIndex = 1
		Me.GroupBox2.TabStop = False
		Me.GroupBox2.Text = "選擇運輸工具"
		'
		'Panel1
		'
		Me.Panel1.Controls.Add(Me.Filter_Info_Label)
		Me.Panel1.Controls.Add(Me.Label2)
		Me.Panel1.Dock = System.Windows.Forms.DockStyle.Top
		Me.Panel1.Location = New System.Drawing.Point(4, 92)
		Me.Panel1.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
		Me.Panel1.Name = "Panel1"
		Me.Panel1.Size = New System.Drawing.Size(652, 28)
		Me.Panel1.TabIndex = 10
		'
		'Filter_Info_Label
		'
		Me.Filter_Info_Label.AutoSize = True
		Me.Filter_Info_Label.ForeColor = System.Drawing.Color.Crimson
		Me.Filter_Info_Label.Location = New System.Drawing.Point(144, 8)
		Me.Filter_Info_Label.Name = "Filter_Info_Label"
		Me.Filter_Info_Label.Size = New System.Drawing.Size(31, 15)
		Me.Filter_Info_Label.TabIndex = 12
		Me.Filter_Info_Label.Text = "N/A"
		'
		'Label2
		'
		Me.Label2.AutoSize = True
		Me.Label2.Location = New System.Drawing.Point(4, 8)
		Me.Label2.Name = "Label2"
		Me.Label2.Size = New System.Drawing.Size(136, 15)
		Me.Label2.TabIndex = 11
		Me.Label2.Text = "篩選筆數 / 總筆數 : "
		'
		'AddTransport_Button
		'
		Me.AddTransport_Button.Dock = System.Windows.Forms.DockStyle.Bottom
		Me.AddTransport_Button.Location = New System.Drawing.Point(4, 553)
		Me.AddTransport_Button.Margin = New System.Windows.Forms.Padding(4)
		Me.AddTransport_Button.Name = "AddTransport_Button"
		Me.AddTransport_Button.Size = New System.Drawing.Size(652, 38)
		Me.AddTransport_Button.TabIndex = 8
		Me.AddTransport_Button.Text = "確定/新增"
		Me.AddTransport_Button.UseVisualStyleBackColor = True
		'
		'FlowLayoutPanel4
		'
		Me.FlowLayoutPanel4.BackColor = System.Drawing.Color.Transparent
		Me.FlowLayoutPanel4.Controls.Add(Me.Label1)
		Me.FlowLayoutPanel4.Controls.Add(Me.Selected_Name_TextBox)
		Me.FlowLayoutPanel4.Controls.Add(Me.Label8)
		Me.FlowLayoutPanel4.Controls.Add(Me.Label27)
		Me.FlowLayoutPanel4.Controls.Add(Me.Transport_WFP_TextBox)
		Me.FlowLayoutPanel4.Controls.Add(Me.Label9)
		Me.FlowLayoutPanel4.Controls.Add(Me.Label28)
		Me.FlowLayoutPanel4.Controls.Add(Me.Transport_ISO_TextBox)
		Me.FlowLayoutPanel4.Controls.Add(Me.Label3)
		Me.FlowLayoutPanel4.Controls.Add(Me.Label11)
		Me.FlowLayoutPanel4.Controls.Add(Me.Transport_Distance_TextBox)
		Me.FlowLayoutPanel4.Controls.Add(Me.Label12)
		Me.FlowLayoutPanel4.Controls.Add(Me.CaptionWFP_Label)
		Me.FlowLayoutPanel4.Controls.Add(Me.Label4)
		Me.FlowLayoutPanel4.Controls.Add(Me.WFP_Label)
		Me.FlowLayoutPanel4.Controls.Add(Me.Label7)
		Me.FlowLayoutPanel4.Controls.Add(Me.Label6)
		Me.FlowLayoutPanel4.Controls.Add(Me.ISO_Label)
		Me.FlowLayoutPanel4.Controls.Add(Me.Label13)
		Me.FlowLayoutPanel4.Location = New System.Drawing.Point(4, 338)
		Me.FlowLayoutPanel4.Margin = New System.Windows.Forms.Padding(4)
		Me.FlowLayoutPanel4.Name = "FlowLayoutPanel4"
		Me.FlowLayoutPanel4.Size = New System.Drawing.Size(648, 207)
		Me.FlowLayoutPanel4.TabIndex = 8
		'
		'Label1
		'
		Me.Label1.Location = New System.Drawing.Point(4, 10)
		Me.Label1.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label1.Name = "Label1"
		Me.Label1.Size = New System.Drawing.Size(112, 15)
		Me.Label1.TabIndex = 11
		Me.Label1.Text = "運輸工具名稱"
		'
		'Selected_Name_TextBox
		'
		Me.Selected_Name_TextBox.Location = New System.Drawing.Point(124, 4)
		Me.Selected_Name_TextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.Selected_Name_TextBox.Name = "Selected_Name_TextBox"
		Me.Selected_Name_TextBox.ReadOnly = True
		Me.Selected_Name_TextBox.Size = New System.Drawing.Size(408, 25)
		Me.Selected_Name_TextBox.TabIndex = 12
		'
		'Label8
		'
		Me.Label8.Location = New System.Drawing.Point(4, 43)
		Me.Label8.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label8.Name = "Label8"
		Me.Label8.Size = New System.Drawing.Size(112, 15)
		Me.Label8.TabIndex = 3
		Me.Label8.Text = "運輸工具水足跡"
		'
		'Label27
		'
		Me.Label27.Font = New System.Drawing.Font("PMingLiU", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
		Me.Label27.ForeColor = System.Drawing.Color.Purple
		Me.Label27.Location = New System.Drawing.Point(123, 43)
		Me.Label27.Margin = New System.Windows.Forms.Padding(3, 10, 3, 0)
		Me.Label27.Name = "Label27"
		Me.Label27.Size = New System.Drawing.Size(72, 19)
		Me.Label27.TabIndex = 33
		Me.Label27.Text = "WFN"
		Me.Label27.TextAlign = System.Drawing.ContentAlignment.TopRight
		'
		'Transport_WFP_TextBox
		'
		Me.Transport_WFP_TextBox.Location = New System.Drawing.Point(202, 37)
		Me.Transport_WFP_TextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.Transport_WFP_TextBox.Name = "Transport_WFP_TextBox"
		Me.Transport_WFP_TextBox.ReadOnly = True
		Me.Transport_WFP_TextBox.Size = New System.Drawing.Size(330, 25)
		Me.Transport_WFP_TextBox.TabIndex = 4
		'
		'Label9
		'
		Me.Label9.AutoSize = True
		Me.Label9.Location = New System.Drawing.Point(540, 43)
		Me.Label9.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label9.Name = "Label9"
		Me.Label9.Size = New System.Drawing.Size(89, 15)
		Me.Label9.TabIndex = 5
		Me.Label9.Text = "m^3/Kg．Km"
		'
		'Label28
		'
		Me.Label28.Font = New System.Drawing.Font("PMingLiU", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
		Me.Label28.ForeColor = System.Drawing.Color.Black
		Me.Label28.Location = New System.Drawing.Point(3, 76)
		Me.Label28.Margin = New System.Windows.Forms.Padding(3, 10, 3, 0)
		Me.Label28.Name = "Label28"
		Me.Label28.Size = New System.Drawing.Size(192, 19)
		Me.Label28.TabIndex = 34
		Me.Label28.Text = "ISO 14046"
		Me.Label28.TextAlign = System.Drawing.ContentAlignment.TopRight
		'
		'Transport_ISO_TextBox
		'
		Me.Transport_ISO_TextBox.Location = New System.Drawing.Point(202, 70)
		Me.Transport_ISO_TextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.Transport_ISO_TextBox.Name = "Transport_ISO_TextBox"
		Me.Transport_ISO_TextBox.ReadOnly = True
		Me.Transport_ISO_TextBox.Size = New System.Drawing.Size(330, 25)
		Me.Transport_ISO_TextBox.TabIndex = 35
		'
		'Label3
		'
		Me.Label3.AutoSize = True
		Me.Label3.Location = New System.Drawing.Point(540, 76)
		Me.Label3.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label3.Name = "Label3"
		Me.Label3.Size = New System.Drawing.Size(89, 15)
		Me.Label3.TabIndex = 36
		Me.Label3.Text = "m^3/Kg．Km"
		'
		'Label11
		'
		Me.Label11.Location = New System.Drawing.Point(4, 109)
		Me.Label11.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label11.Name = "Label11"
		Me.Label11.Size = New System.Drawing.Size(112, 15)
		Me.Label11.TabIndex = 6
		Me.Label11.Text = "運輸距離"
		'
		'Transport_Distance_TextBox
		'
		Me.Transport_Distance_TextBox.Location = New System.Drawing.Point(124, 103)
		Me.Transport_Distance_TextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.Transport_Distance_TextBox.Name = "Transport_Distance_TextBox"
		Me.Transport_Distance_TextBox.Size = New System.Drawing.Size(408, 25)
		Me.Transport_Distance_TextBox.TabIndex = 7
		'
		'Label12
		'
		Me.Label12.AutoSize = True
		Me.Label12.Location = New System.Drawing.Point(540, 109)
		Me.Label12.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label12.Name = "Label12"
		Me.Label12.Size = New System.Drawing.Size(28, 15)
		Me.Label12.TabIndex = 8
		Me.Label12.Text = "Km"
		'
		'CaptionWFP_Label
		'
		Me.CaptionWFP_Label.Location = New System.Drawing.Point(4, 142)
		Me.CaptionWFP_Label.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.CaptionWFP_Label.Name = "CaptionWFP_Label"
		Me.CaptionWFP_Label.Size = New System.Drawing.Size(112, 15)
		Me.CaptionWFP_Label.TabIndex = 9
		Me.CaptionWFP_Label.Text = "本零件水足跡"
		'
		'Label4
		'
		Me.Label4.Font = New System.Drawing.Font("PMingLiU", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
		Me.Label4.ForeColor = System.Drawing.Color.Purple
		Me.Label4.Location = New System.Drawing.Point(123, 142)
		Me.Label4.Margin = New System.Windows.Forms.Padding(3, 10, 3, 0)
		Me.Label4.Name = "Label4"
		Me.Label4.Size = New System.Drawing.Size(72, 19)
		Me.Label4.TabIndex = 37
		Me.Label4.Text = "WFN"
		Me.Label4.TextAlign = System.Drawing.ContentAlignment.TopRight
		'
		'WFP_Label
		'
		Me.WFP_Label.ForeColor = System.Drawing.Color.Black
		Me.WFP_Label.Location = New System.Drawing.Point(202, 142)
		Me.WFP_Label.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.WFP_Label.Name = "WFP_Label"
		Me.WFP_Label.Size = New System.Drawing.Size(330, 19)
		Me.WFP_Label.TabIndex = 10
		Me.WFP_Label.Text = "N/A"
		'
		'Label7
		'
		Me.Label7.AutoSize = True
		Me.Label7.Location = New System.Drawing.Point(539, 142)
		Me.Label7.Margin = New System.Windows.Forms.Padding(3, 10, 3, 0)
		Me.Label7.Name = "Label7"
		Me.Label7.Size = New System.Drawing.Size(32, 15)
		Me.Label7.TabIndex = 40
		Me.Label7.Text = "m^3"
		'
		'Label6
		'
		Me.Label6.Font = New System.Drawing.Font("PMingLiU", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
		Me.Label6.ForeColor = System.Drawing.Color.Black
		Me.Label6.Location = New System.Drawing.Point(3, 171)
		Me.Label6.Margin = New System.Windows.Forms.Padding(3, 10, 3, 0)
		Me.Label6.Name = "Label6"
		Me.Label6.Size = New System.Drawing.Size(192, 15)
		Me.Label6.TabIndex = 38
		Me.Label6.Text = "ISO 14046"
		Me.Label6.TextAlign = System.Drawing.ContentAlignment.TopRight
		'
		'ISO_Label
		'
		Me.ISO_Label.ForeColor = System.Drawing.Color.Black
		Me.ISO_Label.Location = New System.Drawing.Point(202, 171)
		Me.ISO_Label.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.ISO_Label.Name = "ISO_Label"
		Me.ISO_Label.Size = New System.Drawing.Size(330, 19)
		Me.ISO_Label.TabIndex = 39
		Me.ISO_Label.Text = "N/A"
		'
		'Label13
		'
		Me.Label13.AutoSize = True
		Me.Label13.Location = New System.Drawing.Point(539, 171)
		Me.Label13.Margin = New System.Windows.Forms.Padding(3, 10, 3, 0)
		Me.Label13.Name = "Label13"
		Me.Label13.Size = New System.Drawing.Size(32, 15)
		Me.Label13.TabIndex = 41
		Me.Label13.Text = "m^3"
		'
		'Transport_DataGridView
		'
		Me.Transport_DataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
		Me.Transport_DataGridView.Location = New System.Drawing.Point(4, 122)
		Me.Transport_DataGridView.Margin = New System.Windows.Forms.Padding(4)
		Me.Transport_DataGridView.Name = "Transport_DataGridView"
		Me.Transport_DataGridView.RowTemplate.Height = 24
		Me.Transport_DataGridView.Size = New System.Drawing.Size(648, 212)
		Me.Transport_DataGridView.TabIndex = 7
		'
		'FlowLayoutPanel3
		'
		Me.FlowLayoutPanel3.Controls.Add(Me.Label10)
		Me.FlowLayoutPanel3.Controls.Add(Me.RecentUsed_ComboBox)
		Me.FlowLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Top
		Me.FlowLayoutPanel3.Location = New System.Drawing.Point(4, 57)
		Me.FlowLayoutPanel3.Margin = New System.Windows.Forms.Padding(4)
		Me.FlowLayoutPanel3.Name = "FlowLayoutPanel3"
		Me.FlowLayoutPanel3.Size = New System.Drawing.Size(652, 35)
		Me.FlowLayoutPanel3.TabIndex = 6
		'
		'Label10
		'
		Me.Label10.AutoSize = True
		Me.Label10.Location = New System.Drawing.Point(4, 10)
		Me.Label10.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label10.Name = "Label10"
		Me.Label10.Size = New System.Drawing.Size(142, 15)
		Me.Label10.TabIndex = 0
		Me.Label10.Text = "最近曾使用運輸工具"
		'
		'RecentUsed_ComboBox
		'
		Me.RecentUsed_ComboBox.Font = New System.Drawing.Font("PMingLiU", 10.0!)
		Me.RecentUsed_ComboBox.FormattingEnabled = True
		Me.RecentUsed_ComboBox.Location = New System.Drawing.Point(154, 4)
		Me.RecentUsed_ComboBox.Margin = New System.Windows.Forms.Padding(4)
		Me.RecentUsed_ComboBox.Name = "RecentUsed_ComboBox"
		Me.RecentUsed_ComboBox.Size = New System.Drawing.Size(378, 25)
		Me.RecentUsed_ComboBox.TabIndex = 1
		Me.RecentUsed_ComboBox.Text = "請選擇"
		'
		'FlowLayoutPanel2
		'
		Me.FlowLayoutPanel2.Controls.Add(Me.Label5)
		Me.FlowLayoutPanel2.Controls.Add(Me.NameFilter_TextBox)
		Me.FlowLayoutPanel2.Controls.Add(Me.Clean_Filter_Button)
		Me.FlowLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Top
		Me.FlowLayoutPanel2.Location = New System.Drawing.Point(4, 22)
		Me.FlowLayoutPanel2.Margin = New System.Windows.Forms.Padding(4)
		Me.FlowLayoutPanel2.Name = "FlowLayoutPanel2"
		Me.FlowLayoutPanel2.Size = New System.Drawing.Size(652, 35)
		Me.FlowLayoutPanel2.TabIndex = 3
		'
		'Label5
		'
		Me.Label5.AutoSize = True
		Me.Label5.Location = New System.Drawing.Point(4, 10)
		Me.Label5.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label5.Name = "Label5"
		Me.Label5.Size = New System.Drawing.Size(127, 15)
		Me.Label5.TabIndex = 0
		Me.Label5.Text = "運輸工具名稱篩選"
		'
		'NameFilter_TextBox
		'
		Me.NameFilter_TextBox.Location = New System.Drawing.Point(139, 4)
		Me.NameFilter_TextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.NameFilter_TextBox.Name = "NameFilter_TextBox"
		Me.NameFilter_TextBox.Size = New System.Drawing.Size(393, 25)
		Me.NameFilter_TextBox.TabIndex = 1
		'
		'Clean_Filter_Button
		'
		Me.Clean_Filter_Button.Location = New System.Drawing.Point(540, 4)
		Me.Clean_Filter_Button.Margin = New System.Windows.Forms.Padding(4)
		Me.Clean_Filter_Button.Name = "Clean_Filter_Button"
		Me.Clean_Filter_Button.Size = New System.Drawing.Size(100, 29)
		Me.Clean_Filter_Button.TabIndex = 2
		Me.Clean_Filter_Button.Text = "清除篩選"
		Me.Clean_Filter_Button.UseVisualStyleBackColor = True
		'
		'GroupBox3
		'
		Me.GroupBox3.Controls.Add(Me.DeleteRecord_Button)
		Me.GroupBox3.Controls.Add(Me.Transport_Records_DataGridView)
		Me.GroupBox3.Location = New System.Drawing.Point(4, 680)
		Me.GroupBox3.Margin = New System.Windows.Forms.Padding(4)
		Me.GroupBox3.Name = "GroupBox3"
		Me.GroupBox3.Padding = New System.Windows.Forms.Padding(4)
		Me.GroupBox3.Size = New System.Drawing.Size(656, 303)
		Me.GroupBox3.TabIndex = 2
		Me.GroupBox3.TabStop = False
		Me.GroupBox3.Text = "本零件水足跡歷程(以下水足跡單位為 m^3)"
		'
		'DeleteRecord_Button
		'
		Me.DeleteRecord_Button.Dock = System.Windows.Forms.DockStyle.Bottom
		Me.DeleteRecord_Button.Location = New System.Drawing.Point(4, 261)
		Me.DeleteRecord_Button.Margin = New System.Windows.Forms.Padding(4)
		Me.DeleteRecord_Button.Name = "DeleteRecord_Button"
		Me.DeleteRecord_Button.Size = New System.Drawing.Size(648, 38)
		Me.DeleteRecord_Button.TabIndex = 1
		Me.DeleteRecord_Button.Text = "刪除"
		Me.DeleteRecord_Button.UseVisualStyleBackColor = True
		'
		'Transport_Records_DataGridView
		'
		Me.Transport_Records_DataGridView.AllowUserToAddRows = False
		Me.Transport_Records_DataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
		Me.Transport_Records_DataGridView.Dock = System.Windows.Forms.DockStyle.Top
		Me.Transport_Records_DataGridView.Location = New System.Drawing.Point(4, 22)
		Me.Transport_Records_DataGridView.Margin = New System.Windows.Forms.Padding(4)
		Me.Transport_Records_DataGridView.Name = "Transport_Records_DataGridView"
		Me.Transport_Records_DataGridView.RowTemplate.Height = 24
		Me.Transport_Records_DataGridView.Size = New System.Drawing.Size(648, 212)
		Me.Transport_Records_DataGridView.TabIndex = 0
		'
		'Completed_Button
		'
		Me.Completed_Button.Location = New System.Drawing.Point(3, 989)
		Me.Completed_Button.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
		Me.Completed_Button.Name = "Completed_Button"
		Me.Completed_Button.Size = New System.Drawing.Size(653, 38)
		Me.Completed_Button.TabIndex = 3
		Me.Completed_Button.Text = "完成"
		Me.Completed_Button.UseVisualStyleBackColor = True
		'
		'TransportForm
		'
		Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 15.0!)
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.ClientSize = New System.Drawing.Size(677, 1033)
		Me.Controls.Add(Me.FlowLayoutPanel1)
		Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
		Me.Margin = New System.Windows.Forms.Padding(4)
		Me.Name = "TransportForm"
		Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
		Me.Text = "選擇運輸工具 (原料/後製)"
		Me.TopMost = True
		Me.FlowLayoutPanel1.ResumeLayout(False)
		Me.Product_Info_GroupBox.ResumeLayout(False)
		Me.Product_Info_GroupBox.PerformLayout()
		Me.GroupBox2.ResumeLayout(False)
		Me.Panel1.ResumeLayout(False)
		Me.Panel1.PerformLayout()
		Me.FlowLayoutPanel4.ResumeLayout(False)
		Me.FlowLayoutPanel4.PerformLayout()
		CType(Me.Transport_DataGridView, System.ComponentModel.ISupportInitialize).EndInit()
		Me.FlowLayoutPanel3.ResumeLayout(False)
		Me.FlowLayoutPanel3.PerformLayout()
		Me.FlowLayoutPanel2.ResumeLayout(False)
		Me.FlowLayoutPanel2.PerformLayout()
		Me.GroupBox3.ResumeLayout(False)
		CType(Me.Transport_Records_DataGridView, System.ComponentModel.ISupportInitialize).EndInit()
		Me.ResumeLayout(False)

	End Sub

	Friend WithEvents FlowLayoutPanel1 As FlowLayoutPanel
	Friend WithEvents Product_Info_GroupBox As GroupBox
	Friend WithEvents Component_Name_Label As Label
	Friend WithEvents CaptionName_Label As Label
	Friend WithEvents GroupBox2 As GroupBox
	Friend WithEvents AddTransport_Button As Button
	Friend WithEvents FlowLayoutPanel4 As FlowLayoutPanel
	Friend WithEvents Label8 As Label
	Friend WithEvents Transport_WFP_TextBox As TextBox
	Friend WithEvents Label9 As Label
	Friend WithEvents Label11 As Label
	Friend WithEvents Transport_Distance_TextBox As TextBox
	Friend WithEvents Label12 As Label
	Friend WithEvents Transport_DataGridView As DataGridView
	Friend WithEvents FlowLayoutPanel3 As FlowLayoutPanel
	Friend WithEvents Label10 As Label
	Friend WithEvents RecentUsed_ComboBox As ComboBox
	Friend WithEvents FlowLayoutPanel2 As FlowLayoutPanel
	Friend WithEvents Label5 As Label
	Friend WithEvents NameFilter_TextBox As TextBox
	Friend WithEvents Clean_Filter_Button As Button
	Friend WithEvents GroupBox3 As GroupBox
	Friend WithEvents DeleteRecord_Button As Button
	Friend WithEvents Transport_Records_DataGridView As DataGridView
	Friend WithEvents Completed_Button As Button
	Friend WithEvents Component_Mass_Label As Label
	Friend WithEvents CaptionMass_Label As Label
	Friend WithEvents CaptionWFP_Label As Label
	Friend WithEvents WFP_Label As Label
	Friend WithEvents Filter_Info_Label As Label
	Friend WithEvents Label2 As Label
	Friend WithEvents Panel1 As Panel
	Friend WithEvents Label1 As Label
	Friend WithEvents Selected_Name_TextBox As TextBox
	Friend WithEvents Label27 As Label
	Friend WithEvents Label28 As Label
	Friend WithEvents Transport_ISO_TextBox As TextBox
	Friend WithEvents Label3 As Label
	Friend WithEvents Label4 As Label
	Friend WithEvents Label6 As Label
	Friend WithEvents ISO_Label As Label
	Friend WithEvents Label7 As Label
	Friend WithEvents Label13 As Label
End Class
