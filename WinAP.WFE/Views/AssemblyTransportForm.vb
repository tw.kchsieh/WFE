﻿Imports WFE.Assistants.Controllers
Imports WFE.Assistants.Models.AssemblyPageViewModels
Imports WFE.Assistants.Models.ORDs

Public Class AssemblyTransportForm
	Private _viewController As AssemblyTrasnportFormController
	Private _dataInfo As AssemblyTransportFormInfoDTO
	Private _selectedComponent As ComponentTransportToAssemblyDTO
	Private _assemblyTransports As List(Of ComponentTransport)
	Friend Sub UseModel(dataInfo As AssemblyTransportFormInfoDTO, selectedComponent As ComponentTransportToAssemblyDTO, assemblyTransports As List(Of ComponentTransport))
		_dataInfo = dataInfo
		_selectedComponent = selectedComponent
		_assemblyTransports = assemblyTransports

		'Setup
		setup()
	End Sub
	Private Sub setup()
		'Setup controller
		_viewController = New AssemblyTrasnportFormController(NameFilter_TextBox,
														RecentUsed_ComboBox,
													  Filter_Info_Label,
													  Transport_DataGridView,
													  Selected_Name_TextBox,
													  Transport_WFP_TextBox,
													  Transport_ISO_TextBox,
													  Transport_Distance_TextBox,
													  WFP_Label,
													  ISO_Label,
													  Transport_Records_DataGridView,
															  sum_wfn_Label,
															  sum_iso_Label)

		_viewController.Initial(_assemblyTransports, _selectedComponent)
	End Sub
	Private Sub AssemblyTransportForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
		'Assign value
		Me.Text = _dataInfo.FormText
		CaptionName_Label.Text = _dataInfo.C_Name
		'
		Product_Info_GroupBox.Text = "專案:" + _dataInfo.ProductName
		Component_Name_Label.Text = _selectedComponent.ComponentName
		Component_Source_Label.Text = _selectedComponent.ComponentType
		Component_Mass_Label.Text = _selectedComponent.ComponentMass

		'Focus
		NameFilter_TextBox.Focus()
	End Sub

	Private Sub Clean_Filter_Button_Click(sender As Object, e As EventArgs) Handles Clean_Filter_Button.Click
		NameFilter_TextBox.Text = ""
	End Sub

	Private Sub AddTransport_Button_Click(sender As Object, e As EventArgs) Handles AddTransport_Button.Click
		'save recent used
		Dim addRecentUsedSuccess = _viewController.SaveRecentUsedItem()
		If addRecentUsedSuccess = False Then
			Dim errMsg = _viewController.GetErrMsg()
			MsgBox("加入最近使用運輸工具失敗！ 原因: " + errMsg)
		End If

		'add Stage A Transport Records
		Dim addTransportRecordSuccess = _viewController.AddTransportRecord()
		If addTransportRecordSuccess = False Then
			MsgBox("加入記錄失敗！ 原因: " + _viewController.GetErrMsg())
		End If
	End Sub

	Private Sub DeleteRecord_Button_Click(sender As Object, e As EventArgs) Handles DeleteRecord_Button.Click
		'刪除運輸記錄
		_viewController.DeleteTransportRecord()
	End Sub

	Private Sub Completed_Button_Click(sender As Object, e As EventArgs) Handles Completed_Button.Click
		'
		_viewController.CompletedTransportRecord()
		Me.Close()
	End Sub
End Class