﻿Imports WFE.Assistants.Controllers
Imports WFE.Assistants.Models

Public Class SaveProductDialogForm
	Private _product As ProductViewModel
	Private _dc As DataController
	Public Property ProjectSaveAs As Boolean = False

	Private Sub SaveProductDialogForm_Load(sender As Object, e As System.EventArgs) Handles MyBase.Load

	End Sub
	Friend Sub UseController(dc As DataController)
		_dc = dc
		_product = _dc.ProductDTO
		ProductName_TextBox.Text = _product.ProductName
		Product_Desc_RichTextBox.Text = _product.ProductDesc
		If ProjectSaveAs = True And _product.SN > 0 Then
			Save_Button.Text = "另存"
			ProductName_TextBox.SelectAll()
			ProductName_TextBox.Focus()
		ElseIf _product.SN > 0 Then
			Save_Button.Text = "更新"
		End If
	End Sub

	Private Sub Save_Button_Click(sender As Object, e As System.EventArgs) Handles Save_Button.Click
		If _product.ProductName <> "" Then
			Dim success = _dc.SaveProduct(ProjectSaveAs)
			If (success) Then
				MsgBox("儲存成功！")
				Me.Close()
			Else
				MsgBox("儲存發生錯誤！:" + _dc.GetErrorMsg())
			End If
		Else
			MsgBox("請輸入「產品名稱」！")
		End If
	End Sub

	Private Sub ProductName_TextBox_TextChanged(sender As Object, e As System.EventArgs) Handles ProductName_TextBox.TextChanged
		_product.ProductName = ProductName_TextBox.Text.Trim()
	End Sub

	Private Sub Product_Desc_RichTextBox_TextChanged(sender As Object, e As System.EventArgs) Handles Product_Desc_RichTextBox.TextChanged
		_product.ProductDesc = Product_Desc_RichTextBox.Text.Trim()
	End Sub

	Private Sub Cancel_Button_Click(sender As Object, e As System.EventArgs) Handles Cancel_Button.Click
		Me.Close()
	End Sub


End Class