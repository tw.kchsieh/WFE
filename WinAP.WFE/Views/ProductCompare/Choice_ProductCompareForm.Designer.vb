﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Choice_ProductCompareForm
	Inherits System.Windows.Forms.Form

	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()>
	Protected Overrides Sub Dispose(ByVal disposing As Boolean)
		Try
			If disposing AndAlso components IsNot Nothing Then
				components.Dispose()
			End If
		Finally
			MyBase.Dispose(disposing)
		End Try
	End Sub

	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer

	'NOTE: The following procedure is required by the Windows Form Designer
	'It can be modified using the Windows Form Designer.  
	'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()>
	Private Sub InitializeComponent()
		Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Choice_ProductCompareForm))
		Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
		Me.Label1 = New System.Windows.Forms.Label()
		Me.name_Filter_TextBox = New System.Windows.Forms.TextBox()
		Me.clean_filter_Button = New System.Windows.Forms.Button()
		Me.FlowLayoutPanel2 = New System.Windows.Forms.FlowLayoutPanel()
		Me.Label2 = New System.Windows.Forms.Label()
		Me.filterInfo_Label = New System.Windows.Forms.Label()
		Me.FlowLayoutPanel3 = New System.Windows.Forms.FlowLayoutPanel()
		Me.choice_all_Button = New System.Windows.Forms.Button()
		Me.clean_choice_Button = New System.Windows.Forms.Button()
		Me.ok_Button = New System.Windows.Forms.Button()
		Me.record_DataGridView = New System.Windows.Forms.DataGridView()
		Me.FlowLayoutPanel1.SuspendLayout()
		Me.FlowLayoutPanel2.SuspendLayout()
		Me.FlowLayoutPanel3.SuspendLayout()
		CType(Me.record_DataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.SuspendLayout()
		'
		'FlowLayoutPanel1
		'
		Me.FlowLayoutPanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.FlowLayoutPanel1.Controls.Add(Me.Label1)
		Me.FlowLayoutPanel1.Controls.Add(Me.name_Filter_TextBox)
		Me.FlowLayoutPanel1.Controls.Add(Me.clean_filter_Button)
		Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top
		Me.FlowLayoutPanel1.Location = New System.Drawing.Point(0, 0)
		Me.FlowLayoutPanel1.Margin = New System.Windows.Forms.Padding(4)
		Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
		Me.FlowLayoutPanel1.Size = New System.Drawing.Size(1282, 41)
		Me.FlowLayoutPanel1.TabIndex = 2
		'
		'Label1
		'
		Me.Label1.AutoSize = True
		Me.Label1.Location = New System.Drawing.Point(4, 10)
		Me.Label1.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label1.Name = "Label1"
		Me.Label1.Size = New System.Drawing.Size(97, 15)
		Me.Label1.TabIndex = 3
		Me.Label1.Text = "產品名稱篩選"
		'
		'name_Filter_TextBox
		'
		Me.name_Filter_TextBox.Location = New System.Drawing.Point(109, 4)
		Me.name_Filter_TextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.name_Filter_TextBox.Name = "name_Filter_TextBox"
		Me.name_Filter_TextBox.Size = New System.Drawing.Size(716, 25)
		Me.name_Filter_TextBox.TabIndex = 4
		'
		'clean_filter_Button
		'
		Me.clean_filter_Button.Location = New System.Drawing.Point(833, 4)
		Me.clean_filter_Button.Margin = New System.Windows.Forms.Padding(4)
		Me.clean_filter_Button.Name = "clean_filter_Button"
		Me.clean_filter_Button.Size = New System.Drawing.Size(100, 29)
		Me.clean_filter_Button.TabIndex = 5
		Me.clean_filter_Button.Text = "清除篩選"
		Me.clean_filter_Button.UseVisualStyleBackColor = True
		'
		'FlowLayoutPanel2
		'
		Me.FlowLayoutPanel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.FlowLayoutPanel2.Controls.Add(Me.Label2)
		Me.FlowLayoutPanel2.Controls.Add(Me.filterInfo_Label)
		Me.FlowLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Top
		Me.FlowLayoutPanel2.Location = New System.Drawing.Point(0, 41)
		Me.FlowLayoutPanel2.Margin = New System.Windows.Forms.Padding(4)
		Me.FlowLayoutPanel2.Name = "FlowLayoutPanel2"
		Me.FlowLayoutPanel2.Size = New System.Drawing.Size(1282, 33)
		Me.FlowLayoutPanel2.TabIndex = 3
		'
		'Label2
		'
		Me.Label2.AutoSize = True
		Me.Label2.ForeColor = System.Drawing.Color.Magenta
		Me.Label2.Location = New System.Drawing.Point(4, 10)
		Me.Label2.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label2.Name = "Label2"
		Me.Label2.Size = New System.Drawing.Size(132, 15)
		Me.Label2.TabIndex = 0
		Me.Label2.Text = "篩選筆數 / 總筆數 :"
		Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
		'
		'filterInfo_Label
		'
		Me.filterInfo_Label.AutoSize = True
		Me.filterInfo_Label.ForeColor = System.Drawing.Color.Coral
		Me.filterInfo_Label.Location = New System.Drawing.Point(144, 10)
		Me.filterInfo_Label.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.filterInfo_Label.Name = "filterInfo_Label"
		Me.filterInfo_Label.Size = New System.Drawing.Size(47, 15)
		Me.filterInfo_Label.TabIndex = 1
		Me.filterInfo_Label.Text = "10 / 50"
		Me.filterInfo_Label.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
		'
		'FlowLayoutPanel3
		'
		Me.FlowLayoutPanel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.FlowLayoutPanel3.Controls.Add(Me.choice_all_Button)
		Me.FlowLayoutPanel3.Controls.Add(Me.clean_choice_Button)
		Me.FlowLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Top
		Me.FlowLayoutPanel3.Location = New System.Drawing.Point(0, 74)
		Me.FlowLayoutPanel3.Margin = New System.Windows.Forms.Padding(4)
		Me.FlowLayoutPanel3.Name = "FlowLayoutPanel3"
		Me.FlowLayoutPanel3.Size = New System.Drawing.Size(1282, 40)
		Me.FlowLayoutPanel3.TabIndex = 4
		'
		'choice_all_Button
		'
		Me.choice_all_Button.ForeColor = System.Drawing.Color.Blue
		Me.choice_all_Button.Location = New System.Drawing.Point(4, 4)
		Me.choice_all_Button.Margin = New System.Windows.Forms.Padding(4)
		Me.choice_all_Button.Name = "choice_all_Button"
		Me.choice_all_Button.Size = New System.Drawing.Size(100, 29)
		Me.choice_all_Button.TabIndex = 5
		Me.choice_all_Button.Text = "全選"
		Me.choice_all_Button.UseVisualStyleBackColor = True
		'
		'clean_choice_Button
		'
		Me.clean_choice_Button.Location = New System.Drawing.Point(112, 4)
		Me.clean_choice_Button.Margin = New System.Windows.Forms.Padding(4)
		Me.clean_choice_Button.Name = "clean_choice_Button"
		Me.clean_choice_Button.Size = New System.Drawing.Size(100, 29)
		Me.clean_choice_Button.TabIndex = 6
		Me.clean_choice_Button.Text = "清除全選"
		Me.clean_choice_Button.UseVisualStyleBackColor = True
		'
		'ok_Button
		'
		Me.ok_Button.Dock = System.Windows.Forms.DockStyle.Bottom
		Me.ok_Button.ForeColor = System.Drawing.Color.Green
		Me.ok_Button.Location = New System.Drawing.Point(0, 610)
		Me.ok_Button.Margin = New System.Windows.Forms.Padding(4)
		Me.ok_Button.Name = "ok_Button"
		Me.ok_Button.Size = New System.Drawing.Size(1282, 51)
		Me.ok_Button.TabIndex = 5
		Me.ok_Button.Text = "確定"
		Me.ok_Button.UseVisualStyleBackColor = True
		'
		'record_DataGridView
		'
		Me.record_DataGridView.AllowUserToAddRows = False
		Me.record_DataGridView.AllowUserToDeleteRows = False
		Me.record_DataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
		Me.record_DataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
		Me.record_DataGridView.Dock = System.Windows.Forms.DockStyle.Fill
		Me.record_DataGridView.Location = New System.Drawing.Point(0, 114)
		Me.record_DataGridView.Margin = New System.Windows.Forms.Padding(4)
		Me.record_DataGridView.Name = "record_DataGridView"
		Me.record_DataGridView.ReadOnly = True
		Me.record_DataGridView.RowTemplate.Height = 24
		Me.record_DataGridView.Size = New System.Drawing.Size(1282, 496)
		Me.record_DataGridView.TabIndex = 6
		'
		'Choice_ProductCompareForm
		'
		Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 15.0!)
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.ClientSize = New System.Drawing.Size(1282, 661)
		Me.Controls.Add(Me.record_DataGridView)
		Me.Controls.Add(Me.ok_Button)
		Me.Controls.Add(Me.FlowLayoutPanel3)
		Me.Controls.Add(Me.FlowLayoutPanel2)
		Me.Controls.Add(Me.FlowLayoutPanel1)
		Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
		Me.Margin = New System.Windows.Forms.Padding(4)
		Me.Name = "Choice_ProductCompareForm"
		Me.Text = "選擇比較產品"
		Me.TopMost = True
		Me.FlowLayoutPanel1.ResumeLayout(False)
		Me.FlowLayoutPanel1.PerformLayout()
		Me.FlowLayoutPanel2.ResumeLayout(False)
		Me.FlowLayoutPanel2.PerformLayout()
		Me.FlowLayoutPanel3.ResumeLayout(False)
		CType(Me.record_DataGridView, System.ComponentModel.ISupportInitialize).EndInit()
		Me.ResumeLayout(False)

	End Sub

	Friend WithEvents FlowLayoutPanel1 As FlowLayoutPanel
	Friend WithEvents Label1 As Label
	Friend WithEvents name_Filter_TextBox As TextBox
	Friend WithEvents clean_filter_Button As Button
	Friend WithEvents FlowLayoutPanel2 As FlowLayoutPanel
	Friend WithEvents Label2 As Label
	Friend WithEvents filterInfo_Label As Label
	Friend WithEvents FlowLayoutPanel3 As FlowLayoutPanel
	Friend WithEvents choice_all_Button As Button
	Friend WithEvents clean_choice_Button As Button
	Friend WithEvents ok_Button As Button
	Friend WithEvents record_DataGridView As DataGridView
End Class
