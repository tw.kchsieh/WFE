﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Load_ProductCompareForm
	Inherits System.Windows.Forms.Form

	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()>
	Protected Overrides Sub Dispose(ByVal disposing As Boolean)
		Try
			If disposing AndAlso components IsNot Nothing Then
				components.Dispose()
			End If
		Finally
			MyBase.Dispose(disposing)
		End Try
	End Sub

	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer

	'NOTE: The following procedure is required by the Windows Form Designer
	'It can be modified using the Windows Form Designer.  
	'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()>
	Private Sub InitializeComponent()
		Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Load_ProductCompareForm))
		Me.Label2 = New System.Windows.Forms.Label()
		Me.filterInfo_Label = New System.Windows.Forms.Label()
		Me.FlowLayoutPanel2 = New System.Windows.Forms.FlowLayoutPanel()
		Me.Label1 = New System.Windows.Forms.Label()
		Me.name_Filter_TextBox = New System.Windows.Forms.TextBox()
		Me.clean_filter_Button = New System.Windows.Forms.Button()
		Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
		Me.FlowLayoutPanel3 = New System.Windows.Forms.FlowLayoutPanel()
		Me.DeleteButton = New System.Windows.Forms.Button()
		Me.LoadButton = New System.Windows.Forms.Button()
		Me.Panel1 = New System.Windows.Forms.Panel()
		Me.DataGridView1 = New System.Windows.Forms.DataGridView()
		Me.FlowLayoutPanel4 = New System.Windows.Forms.FlowLayoutPanel()
		Me.Label3 = New System.Windows.Forms.Label()
		Me.ComparisonName_TextBox = New System.Windows.Forms.TextBox()
		Me.Label4 = New System.Windows.Forms.Label()
		Me.ComparisonDesc_RichTextBox = New System.Windows.Forms.RichTextBox()
		Me.FlowLayoutPanel2.SuspendLayout()
		Me.FlowLayoutPanel1.SuspendLayout()
		Me.FlowLayoutPanel3.SuspendLayout()
		Me.Panel1.SuspendLayout()
		CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.FlowLayoutPanel4.SuspendLayout()
		Me.SuspendLayout()
		'
		'Label2
		'
		Me.Label2.AutoSize = True
		Me.Label2.ForeColor = System.Drawing.Color.Magenta
		Me.Label2.Location = New System.Drawing.Point(4, 10)
		Me.Label2.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label2.Name = "Label2"
		Me.Label2.Size = New System.Drawing.Size(132, 15)
		Me.Label2.TabIndex = 0
		Me.Label2.Text = "篩選筆數 / 總筆數 :"
		Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
		'
		'filterInfo_Label
		'
		Me.filterInfo_Label.AutoSize = True
		Me.filterInfo_Label.ForeColor = System.Drawing.Color.Coral
		Me.filterInfo_Label.Location = New System.Drawing.Point(144, 10)
		Me.filterInfo_Label.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.filterInfo_Label.Name = "filterInfo_Label"
		Me.filterInfo_Label.Size = New System.Drawing.Size(47, 15)
		Me.filterInfo_Label.TabIndex = 1
		Me.filterInfo_Label.Text = "10 / 50"
		Me.filterInfo_Label.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
		'
		'FlowLayoutPanel2
		'
		Me.FlowLayoutPanel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.FlowLayoutPanel2.Controls.Add(Me.Label2)
		Me.FlowLayoutPanel2.Controls.Add(Me.filterInfo_Label)
		Me.FlowLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Top
		Me.FlowLayoutPanel2.Location = New System.Drawing.Point(0, 41)
		Me.FlowLayoutPanel2.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
		Me.FlowLayoutPanel2.Name = "FlowLayoutPanel2"
		Me.FlowLayoutPanel2.Size = New System.Drawing.Size(1084, 33)
		Me.FlowLayoutPanel2.TabIndex = 8
		'
		'Label1
		'
		Me.Label1.AutoSize = True
		Me.Label1.Location = New System.Drawing.Point(4, 10)
		Me.Label1.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label1.Name = "Label1"
		Me.Label1.Size = New System.Drawing.Size(217, 15)
		Me.Label1.TabIndex = 3
		Me.Label1.Text = "「產品比較分析評估」名稱篩選"
		'
		'name_Filter_TextBox
		'
		Me.name_Filter_TextBox.Location = New System.Drawing.Point(229, 4)
		Me.name_Filter_TextBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
		Me.name_Filter_TextBox.Name = "name_Filter_TextBox"
		Me.name_Filter_TextBox.Size = New System.Drawing.Size(716, 25)
		Me.name_Filter_TextBox.TabIndex = 4
		'
		'clean_filter_Button
		'
		Me.clean_filter_Button.Location = New System.Drawing.Point(953, 4)
		Me.clean_filter_Button.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
		Me.clean_filter_Button.Name = "clean_filter_Button"
		Me.clean_filter_Button.Size = New System.Drawing.Size(100, 29)
		Me.clean_filter_Button.TabIndex = 5
		Me.clean_filter_Button.Text = "清除篩選"
		Me.clean_filter_Button.UseVisualStyleBackColor = True
		'
		'FlowLayoutPanel1
		'
		Me.FlowLayoutPanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.FlowLayoutPanel1.Controls.Add(Me.Label1)
		Me.FlowLayoutPanel1.Controls.Add(Me.name_Filter_TextBox)
		Me.FlowLayoutPanel1.Controls.Add(Me.clean_filter_Button)
		Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top
		Me.FlowLayoutPanel1.Location = New System.Drawing.Point(0, 0)
		Me.FlowLayoutPanel1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
		Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
		Me.FlowLayoutPanel1.Size = New System.Drawing.Size(1084, 41)
		Me.FlowLayoutPanel1.TabIndex = 7
		'
		'FlowLayoutPanel3
		'
		Me.FlowLayoutPanel3.BackColor = System.Drawing.SystemColors.Control
		Me.FlowLayoutPanel3.Controls.Add(Me.DeleteButton)
		Me.FlowLayoutPanel3.Controls.Add(Me.LoadButton)
		Me.FlowLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Bottom
		Me.FlowLayoutPanel3.Location = New System.Drawing.Point(0, 738)
		Me.FlowLayoutPanel3.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
		Me.FlowLayoutPanel3.Name = "FlowLayoutPanel3"
		Me.FlowLayoutPanel3.Size = New System.Drawing.Size(1084, 56)
		Me.FlowLayoutPanel3.TabIndex = 9
		'
		'DeleteButton
		'
		Me.DeleteButton.ForeColor = System.Drawing.Color.Red
		Me.DeleteButton.Location = New System.Drawing.Point(4, 4)
		Me.DeleteButton.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
		Me.DeleteButton.Name = "DeleteButton"
		Me.DeleteButton.Size = New System.Drawing.Size(533, 44)
		Me.DeleteButton.TabIndex = 0
		Me.DeleteButton.Text = "刪除"
		Me.DeleteButton.UseVisualStyleBackColor = True
		'
		'LoadButton
		'
		Me.LoadButton.ForeColor = System.Drawing.Color.Blue
		Me.LoadButton.Location = New System.Drawing.Point(545, 4)
		Me.LoadButton.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
		Me.LoadButton.Name = "LoadButton"
		Me.LoadButton.Size = New System.Drawing.Size(532, 44)
		Me.LoadButton.TabIndex = 1
		Me.LoadButton.Text = "載入"
		Me.LoadButton.UseVisualStyleBackColor = True
		'
		'Panel1
		'
		Me.Panel1.Controls.Add(Me.DataGridView1)
		Me.Panel1.Controls.Add(Me.FlowLayoutPanel4)
		Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
		Me.Panel1.Location = New System.Drawing.Point(0, 74)
		Me.Panel1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
		Me.Panel1.Name = "Panel1"
		Me.Panel1.Size = New System.Drawing.Size(1084, 664)
		Me.Panel1.TabIndex = 10
		'
		'DataGridView1
		'
		Me.DataGridView1.AllowUserToAddRows = False
		Me.DataGridView1.AllowUserToDeleteRows = False
		Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
		Me.DataGridView1.Dock = System.Windows.Forms.DockStyle.Fill
		Me.DataGridView1.Location = New System.Drawing.Point(0, 0)
		Me.DataGridView1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
		Me.DataGridView1.Name = "DataGridView1"
		Me.DataGridView1.ReadOnly = True
		Me.DataGridView1.RowTemplate.Height = 24
		Me.DataGridView1.Size = New System.Drawing.Size(1084, 492)
		Me.DataGridView1.TabIndex = 1
		'
		'FlowLayoutPanel4
		'
		Me.FlowLayoutPanel4.Controls.Add(Me.Label3)
		Me.FlowLayoutPanel4.Controls.Add(Me.ComparisonName_TextBox)
		Me.FlowLayoutPanel4.Controls.Add(Me.Label4)
		Me.FlowLayoutPanel4.Controls.Add(Me.ComparisonDesc_RichTextBox)
		Me.FlowLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Bottom
		Me.FlowLayoutPanel4.Location = New System.Drawing.Point(0, 492)
		Me.FlowLayoutPanel4.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
		Me.FlowLayoutPanel4.Name = "FlowLayoutPanel4"
		Me.FlowLayoutPanel4.Size = New System.Drawing.Size(1084, 172)
		Me.FlowLayoutPanel4.TabIndex = 0
		'
		'Label3
		'
		Me.Label3.AutoSize = True
		Me.Label3.Location = New System.Drawing.Point(4, 10)
		Me.Label3.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label3.Name = "Label3"
		Me.Label3.Size = New System.Drawing.Size(127, 15)
		Me.Label3.TabIndex = 0
		Me.Label3.Text = "產品比較分析名稱"
		'
		'ComparisonName_TextBox
		'
		Me.ComparisonName_TextBox.Location = New System.Drawing.Point(139, 4)
		Me.ComparisonName_TextBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
		Me.ComparisonName_TextBox.Name = "ComparisonName_TextBox"
		Me.ComparisonName_TextBox.Size = New System.Drawing.Size(920, 25)
		Me.ComparisonName_TextBox.TabIndex = 1
		'
		'Label4
		'
		Me.Label4.AutoSize = True
		Me.Label4.Location = New System.Drawing.Point(4, 43)
		Me.Label4.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label4.Name = "Label4"
		Me.Label4.Size = New System.Drawing.Size(127, 15)
		Me.Label4.TabIndex = 2
		Me.Label4.Text = "產品比較分析描述"
		'
		'ComparisonDesc_RichTextBox
		'
		Me.ComparisonDesc_RichTextBox.Location = New System.Drawing.Point(139, 37)
		Me.ComparisonDesc_RichTextBox.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
		Me.ComparisonDesc_RichTextBox.Name = "ComparisonDesc_RichTextBox"
		Me.ComparisonDesc_RichTextBox.Size = New System.Drawing.Size(919, 119)
		Me.ComparisonDesc_RichTextBox.TabIndex = 3
		Me.ComparisonDesc_RichTextBox.Text = ""
		'
		'Load_ProductCompareForm
		'
		Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 15.0!)
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.ClientSize = New System.Drawing.Size(1084, 794)
		Me.Controls.Add(Me.Panel1)
		Me.Controls.Add(Me.FlowLayoutPanel3)
		Me.Controls.Add(Me.FlowLayoutPanel2)
		Me.Controls.Add(Me.FlowLayoutPanel1)
		Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
		Me.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
		Me.Name = "Load_ProductCompareForm"
		Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
		Me.Text = "載入「產品比較分析評估」"
		Me.TopMost = True
		Me.FlowLayoutPanel2.ResumeLayout(False)
		Me.FlowLayoutPanel2.PerformLayout()
		Me.FlowLayoutPanel1.ResumeLayout(False)
		Me.FlowLayoutPanel1.PerformLayout()
		Me.FlowLayoutPanel3.ResumeLayout(False)
		Me.Panel1.ResumeLayout(False)
		CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
		Me.FlowLayoutPanel4.ResumeLayout(False)
		Me.FlowLayoutPanel4.PerformLayout()
		Me.ResumeLayout(False)

	End Sub
	Friend WithEvents Label2 As Label
	Friend WithEvents filterInfo_Label As Label
	Friend WithEvents FlowLayoutPanel2 As FlowLayoutPanel
	Friend WithEvents Label1 As Label
	Friend WithEvents name_Filter_TextBox As TextBox
	Friend WithEvents clean_filter_Button As Button
	Friend WithEvents FlowLayoutPanel1 As FlowLayoutPanel
	Friend WithEvents FlowLayoutPanel3 As FlowLayoutPanel
	Friend WithEvents DeleteButton As Button
	Friend WithEvents LoadButton As Button
	Friend WithEvents Panel1 As Panel
	Friend WithEvents DataGridView1 As DataGridView
	Friend WithEvents FlowLayoutPanel4 As FlowLayoutPanel
	Friend WithEvents Label3 As Label
	Friend WithEvents ComparisonName_TextBox As TextBox
	Friend WithEvents Label4 As Label
	Friend WithEvents ComparisonDesc_RichTextBox As RichTextBox
End Class
