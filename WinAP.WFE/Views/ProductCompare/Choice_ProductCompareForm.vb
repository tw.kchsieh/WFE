﻿Imports WFE.Assistants.Controllers.ProductCompare

Public Class Choice_ProductCompareForm
	Dim _controller As ChoiceProductController
	Dim _chooseProductSNs() As Integer
	Private Sub Choice_ProductCompareForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
		_controller = New ChoiceProductController()
		Dim funcButtons = {clean_filter_Button, choice_all_Button, clean_choice_Button}
		_controller.Setup(record_DataGridView, name_Filter_TextBox, filterInfo_Label, funcButtons, _chooseProductSNs)
	End Sub

	Friend Sub SetChooseProduct(chooseProductSNs() As Integer)
		_chooseProductSNs = chooseProductSNs
	End Sub

	Private Sub ok_Button_Click(sender As Object, e As EventArgs) Handles ok_Button.Click
		DialogResult = DialogResult.OK
		Me.Close()
	End Sub

	Friend Function GetModel() As String
		Return _controller.GetModel()
	End Function
End Class