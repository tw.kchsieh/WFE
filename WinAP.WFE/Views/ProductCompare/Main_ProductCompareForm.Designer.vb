﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Main_ProductCompareForm
	Inherits System.Windows.Forms.Form

	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> _
	Protected Overrides Sub Dispose(ByVal disposing As Boolean)
		Try
			If disposing AndAlso components IsNot Nothing Then
				components.Dispose()
			End If
		Finally
			MyBase.Dispose(disposing)
		End Try
	End Sub

	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer

	'NOTE: The following procedure is required by the Windows Form Designer
	'It can be modified using the Windows Form Designer.  
	'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> _
	Private Sub InitializeComponent()
		Dim ChartArea1 As System.Windows.Forms.DataVisualization.Charting.ChartArea = New System.Windows.Forms.DataVisualization.Charting.ChartArea()
		Dim Legend1 As System.Windows.Forms.DataVisualization.Charting.Legend = New System.Windows.Forms.DataVisualization.Charting.Legend()
		Dim Series1 As System.Windows.Forms.DataVisualization.Charting.Series = New System.Windows.Forms.DataVisualization.Charting.Series()
		Dim DataPoint1 As System.Windows.Forms.DataVisualization.Charting.DataPoint = New System.Windows.Forms.DataVisualization.Charting.DataPoint(1.0R, 20.0R)
		Dim DataPoint2 As System.Windows.Forms.DataVisualization.Charting.DataPoint = New System.Windows.Forms.DataVisualization.Charting.DataPoint(2.0R, 33.0R)
		Dim DataPoint3 As System.Windows.Forms.DataVisualization.Charting.DataPoint = New System.Windows.Forms.DataVisualization.Charting.DataPoint(3.0R, 33.0R)
		Dim Series2 As System.Windows.Forms.DataVisualization.Charting.Series = New System.Windows.Forms.DataVisualization.Charting.Series()
		Dim DataPoint4 As System.Windows.Forms.DataVisualization.Charting.DataPoint = New System.Windows.Forms.DataVisualization.Charting.DataPoint(1.0R, 12.0R)
		Dim DataPoint5 As System.Windows.Forms.DataVisualization.Charting.DataPoint = New System.Windows.Forms.DataVisualization.Charting.DataPoint(2.0R, 24.0R)
		Dim DataPoint6 As System.Windows.Forms.DataVisualization.Charting.DataPoint = New System.Windows.Forms.DataVisualization.Charting.DataPoint(3.0R, 33.0R)
		Dim Series3 As System.Windows.Forms.DataVisualization.Charting.Series = New System.Windows.Forms.DataVisualization.Charting.Series()
		Dim DataPoint7 As System.Windows.Forms.DataVisualization.Charting.DataPoint = New System.Windows.Forms.DataVisualization.Charting.DataPoint(1.0R, 70.0R)
		Dim DataPoint8 As System.Windows.Forms.DataVisualization.Charting.DataPoint = New System.Windows.Forms.DataVisualization.Charting.DataPoint(2.0R, 5.0R)
		Dim DataPoint9 As System.Windows.Forms.DataVisualization.Charting.DataPoint = New System.Windows.Forms.DataVisualization.Charting.DataPoint(3.0R, 33.0R)
		Dim Title1 As System.Windows.Forms.DataVisualization.Charting.Title = New System.Windows.Forms.DataVisualization.Charting.Title()
		Dim ChartArea2 As System.Windows.Forms.DataVisualization.Charting.ChartArea = New System.Windows.Forms.DataVisualization.Charting.ChartArea()
		Dim Legend2 As System.Windows.Forms.DataVisualization.Charting.Legend = New System.Windows.Forms.DataVisualization.Charting.Legend()
		Dim Series4 As System.Windows.Forms.DataVisualization.Charting.Series = New System.Windows.Forms.DataVisualization.Charting.Series()
		Dim DataPoint10 As System.Windows.Forms.DataVisualization.Charting.DataPoint = New System.Windows.Forms.DataVisualization.Charting.DataPoint(1.0R, 20.0R)
		Dim DataPoint11 As System.Windows.Forms.DataVisualization.Charting.DataPoint = New System.Windows.Forms.DataVisualization.Charting.DataPoint(2.0R, 33.0R)
		Dim DataPoint12 As System.Windows.Forms.DataVisualization.Charting.DataPoint = New System.Windows.Forms.DataVisualization.Charting.DataPoint(3.0R, 33.0R)
		Dim Series5 As System.Windows.Forms.DataVisualization.Charting.Series = New System.Windows.Forms.DataVisualization.Charting.Series()
		Dim DataPoint13 As System.Windows.Forms.DataVisualization.Charting.DataPoint = New System.Windows.Forms.DataVisualization.Charting.DataPoint(1.0R, 12.0R)
		Dim DataPoint14 As System.Windows.Forms.DataVisualization.Charting.DataPoint = New System.Windows.Forms.DataVisualization.Charting.DataPoint(2.0R, 24.0R)
		Dim DataPoint15 As System.Windows.Forms.DataVisualization.Charting.DataPoint = New System.Windows.Forms.DataVisualization.Charting.DataPoint(3.0R, 33.0R)
		Dim Series6 As System.Windows.Forms.DataVisualization.Charting.Series = New System.Windows.Forms.DataVisualization.Charting.Series()
		Dim DataPoint16 As System.Windows.Forms.DataVisualization.Charting.DataPoint = New System.Windows.Forms.DataVisualization.Charting.DataPoint(1.0R, 70.0R)
		Dim DataPoint17 As System.Windows.Forms.DataVisualization.Charting.DataPoint = New System.Windows.Forms.DataVisualization.Charting.DataPoint(2.0R, 5.0R)
		Dim DataPoint18 As System.Windows.Forms.DataVisualization.Charting.DataPoint = New System.Windows.Forms.DataVisualization.Charting.DataPoint(3.0R, 33.0R)
		Dim Title2 As System.Windows.Forms.DataVisualization.Charting.Title = New System.Windows.Forms.DataVisualization.Charting.Title()
		Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Main_ProductCompareForm))
		Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
		Me.add_product_compare_ToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
		Me.save_product_compare_ToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
		Me.load_product_compare_ToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
		Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
		Me.ProductComparisonCaption_Label = New System.Windows.Forms.ToolStripStatusLabel()
		Me.ComparisonName_Label = New System.Windows.Forms.ToolStripStatusLabel()
		Me.TabControl1 = New System.Windows.Forms.TabControl()
		Me.TabPage1 = New System.Windows.Forms.TabPage()
		Me.Page1_Main_FlowLayoutPanel = New System.Windows.Forms.FlowLayoutPanel()
		Me.choice_product_compare_GroupBox = New System.Windows.Forms.GroupBox()
		Me.choice_product_compare_DataGridView = New System.Windows.Forms.DataGridView()
		Me.FlowLayoutPanel2 = New System.Windows.Forms.FlowLayoutPanel()
		Me.add_Button = New System.Windows.Forms.Button()
		Me.clear_Button = New System.Windows.Forms.Button()
		Me.delete_Button = New System.Windows.Forms.Button()
		Me.product_compare_condition_GroupBox = New System.Windows.Forms.GroupBox()
		Me.product_compare_condition_group_main_Panel = New System.Windows.Forms.FlowLayoutPanel()
		Me.GroupBox6 = New System.Windows.Forms.GroupBox()
		Me.FlowLayoutPanel5 = New System.Windows.Forms.FlowLayoutPanel()
		Me.Label5 = New System.Windows.Forms.Label()
		Me.productName_TextBox = New System.Windows.Forms.TextBox()
		Me.Label6 = New System.Windows.Forms.Label()
		Me.WFP_TextBox = New System.Windows.Forms.TextBox()
		Me.Label1 = New System.Windows.Forms.Label()
		Me.ISO_TextBox = New System.Windows.Forms.TextBox()
		Me.GroupBox4 = New System.Windows.Forms.GroupBox()
		Me.FlowLayoutPanel11 = New System.Windows.Forms.FlowLayoutPanel()
		Me.condition_2_camera_Panel = New System.Windows.Forms.FlowLayoutPanel()
		Me.GroupBox7 = New System.Windows.Forms.GroupBox()
		Me.FlowLayoutPanel7 = New System.Windows.Forms.FlowLayoutPanel()
		Me.p1_camera_radio1 = New System.Windows.Forms.RadioButton()
		Me.p1_camera_radio2 = New System.Windows.Forms.RadioButton()
		Me.p1_camera_radio3 = New System.Windows.Forms.RadioButton()
		Me.p1_camera_radio4 = New System.Windows.Forms.RadioButton()
		Me.p1_camera_radio5 = New System.Windows.Forms.RadioButton()
		Me.GroupBox8 = New System.Windows.Forms.GroupBox()
		Me.FlowLayoutPanel8 = New System.Windows.Forms.FlowLayoutPanel()
		Me.p2_camera_radio1 = New System.Windows.Forms.RadioButton()
		Me.p2_camera_radio2 = New System.Windows.Forms.RadioButton()
		Me.p2_camera_radio3 = New System.Windows.Forms.RadioButton()
		Me.p2_camera_radio4 = New System.Windows.Forms.RadioButton()
		Me.p2_camera_radio5 = New System.Windows.Forms.RadioButton()
		Me.GroupBox9 = New System.Windows.Forms.GroupBox()
		Me.FlowLayoutPanel9 = New System.Windows.Forms.FlowLayoutPanel()
		Me.p3_camera_radio1 = New System.Windows.Forms.RadioButton()
		Me.p3_camera_radio2 = New System.Windows.Forms.RadioButton()
		Me.p3_camera_radio3 = New System.Windows.Forms.RadioButton()
		Me.p3_camera_radio4 = New System.Windows.Forms.RadioButton()
		Me.p3_camera_radio5 = New System.Windows.Forms.RadioButton()
		Me.condition_2_LCD_Panel = New System.Windows.Forms.FlowLayoutPanel()
		Me.GroupBox10 = New System.Windows.Forms.GroupBox()
		Me.FlowLayoutPanel4 = New System.Windows.Forms.FlowLayoutPanel()
		Me.p1_lcd_radio1 = New System.Windows.Forms.RadioButton()
		Me.p1_lcd_radio2 = New System.Windows.Forms.RadioButton()
		Me.p1_lcd_radio3 = New System.Windows.Forms.RadioButton()
		Me.p1_lcd_radio4 = New System.Windows.Forms.RadioButton()
		Me.GroupBox11 = New System.Windows.Forms.GroupBox()
		Me.FlowLayoutPanel12 = New System.Windows.Forms.FlowLayoutPanel()
		Me.p2_lcd_radio1 = New System.Windows.Forms.RadioButton()
		Me.p2_lcd_radio2 = New System.Windows.Forms.RadioButton()
		Me.p2_lcd_radio3 = New System.Windows.Forms.RadioButton()
		Me.p2_lcd_radio4 = New System.Windows.Forms.RadioButton()
		Me.GroupBox12 = New System.Windows.Forms.GroupBox()
		Me.FlowLayoutPanel13 = New System.Windows.Forms.FlowLayoutPanel()
		Me.p3_lcd_radio1 = New System.Windows.Forms.RadioButton()
		Me.p3_lcd_radio2 = New System.Windows.Forms.RadioButton()
		Me.p3_lcd_radio3 = New System.Windows.Forms.RadioButton()
		Me.p3_lcd_radio4 = New System.Windows.Forms.RadioButton()
		Me.FlowLayoutPanel10 = New System.Windows.Forms.FlowLayoutPanel()
		Me.condition_2_camera_RadioButton = New System.Windows.Forms.RadioButton()
		Me.condition_2_LCD_RadioButton = New System.Windows.Forms.RadioButton()
		Me.GroupBox5 = New System.Windows.Forms.GroupBox()
		Me.Importance_FlowLayoutPanel = New System.Windows.Forms.FlowLayoutPanel()
		Me.Label7 = New System.Windows.Forms.Label()
		Me.importance_radio1 = New System.Windows.Forms.RadioButton()
		Me.importance_radio2 = New System.Windows.Forms.RadioButton()
		Me.importance_radio3 = New System.Windows.Forms.RadioButton()
		Me.importance_radio4 = New System.Windows.Forms.RadioButton()
		Me.importance_radio5 = New System.Windows.Forms.RadioButton()
		Me.GroupBox3 = New System.Windows.Forms.GroupBox()
		Me.FlowLayoutPanel3 = New System.Windows.Forms.FlowLayoutPanel()
		Me.Label2 = New System.Windows.Forms.Label()
		Me.w1_TrackBar = New System.Windows.Forms.TrackBar()
		Me.w1_TextBox = New System.Windows.Forms.TextBox()
		Me.Label3 = New System.Windows.Forms.Label()
		Me.w2_TrackBar = New System.Windows.Forms.TrackBar()
		Me.w2_TextBox = New System.Windows.Forms.TextBox()
		Me.Label4 = New System.Windows.Forms.Label()
		Me.w3_TrackBar = New System.Windows.Forms.TrackBar()
		Me.w3_TextBox = New System.Windows.Forms.TextBox()
		Me.TabPage3 = New System.Windows.Forms.TabPage()
		Me.MainFlowLayoutPanel4 = New System.Windows.Forms.FlowLayoutPanel()
		Me.GroupBox13 = New System.Windows.Forms.GroupBox()
		Me.WFN_DataGridView = New System.Windows.Forms.DataGridView()
		Me.GroupBox14 = New System.Windows.Forms.GroupBox()
		Me.WFN_Chart = New System.Windows.Forms.DataVisualization.Charting.Chart()
		Me.TabPage4 = New System.Windows.Forms.TabPage()
		Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
		Me.GroupBox15 = New System.Windows.Forms.GroupBox()
		Me.ISO_DataGridView = New System.Windows.Forms.DataGridView()
		Me.GroupBox16 = New System.Windows.Forms.GroupBox()
		Me.ISO_Chart = New System.Windows.Forms.DataVisualization.Charting.Chart()
		Me.MenuStrip1.SuspendLayout()
		Me.StatusStrip1.SuspendLayout()
		Me.TabControl1.SuspendLayout()
		Me.TabPage1.SuspendLayout()
		Me.Page1_Main_FlowLayoutPanel.SuspendLayout()
		Me.choice_product_compare_GroupBox.SuspendLayout()
		CType(Me.choice_product_compare_DataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.FlowLayoutPanel2.SuspendLayout()
		Me.product_compare_condition_GroupBox.SuspendLayout()
		Me.product_compare_condition_group_main_Panel.SuspendLayout()
		Me.GroupBox6.SuspendLayout()
		Me.FlowLayoutPanel5.SuspendLayout()
		Me.GroupBox4.SuspendLayout()
		Me.FlowLayoutPanel11.SuspendLayout()
		Me.condition_2_camera_Panel.SuspendLayout()
		Me.GroupBox7.SuspendLayout()
		Me.FlowLayoutPanel7.SuspendLayout()
		Me.GroupBox8.SuspendLayout()
		Me.FlowLayoutPanel8.SuspendLayout()
		Me.GroupBox9.SuspendLayout()
		Me.FlowLayoutPanel9.SuspendLayout()
		Me.condition_2_LCD_Panel.SuspendLayout()
		Me.GroupBox10.SuspendLayout()
		Me.FlowLayoutPanel4.SuspendLayout()
		Me.GroupBox11.SuspendLayout()
		Me.FlowLayoutPanel12.SuspendLayout()
		Me.GroupBox12.SuspendLayout()
		Me.FlowLayoutPanel13.SuspendLayout()
		Me.FlowLayoutPanel10.SuspendLayout()
		Me.GroupBox5.SuspendLayout()
		Me.Importance_FlowLayoutPanel.SuspendLayout()
		Me.GroupBox3.SuspendLayout()
		Me.FlowLayoutPanel3.SuspendLayout()
		CType(Me.w1_TrackBar, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.w2_TrackBar, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.w3_TrackBar, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.TabPage3.SuspendLayout()
		Me.MainFlowLayoutPanel4.SuspendLayout()
		Me.GroupBox13.SuspendLayout()
		CType(Me.WFN_DataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.GroupBox14.SuspendLayout()
		CType(Me.WFN_Chart, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.TabPage4.SuspendLayout()
		Me.FlowLayoutPanel1.SuspendLayout()
		Me.GroupBox15.SuspendLayout()
		CType(Me.ISO_DataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.GroupBox16.SuspendLayout()
		CType(Me.ISO_Chart, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.SuspendLayout()
		'
		'MenuStrip1
		'
		Me.MenuStrip1.ImageScalingSize = New System.Drawing.Size(20, 20)
		Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.add_product_compare_ToolStripMenuItem, Me.save_product_compare_ToolStripMenuItem, Me.load_product_compare_ToolStripMenuItem})
		Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
		Me.MenuStrip1.Name = "MenuStrip1"
		Me.MenuStrip1.Padding = New System.Windows.Forms.Padding(8, 2, 0, 2)
		Me.MenuStrip1.Size = New System.Drawing.Size(1352, 28)
		Me.MenuStrip1.TabIndex = 0
		Me.MenuStrip1.Text = "MenuStrip1"
		'
		'add_product_compare_ToolStripMenuItem
		'
		Me.add_product_compare_ToolStripMenuItem.Image = Global.WinAP.WFE.My.Resources.Resources.product_compare_add
		Me.add_product_compare_ToolStripMenuItem.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.add_product_compare_ToolStripMenuItem.Name = "add_product_compare_ToolStripMenuItem"
		Me.add_product_compare_ToolStripMenuItem.Size = New System.Drawing.Size(201, 24)
		Me.add_product_compare_ToolStripMenuItem.Text = "新增「產品比較分析」"
		'
		'save_product_compare_ToolStripMenuItem
		'
		Me.save_product_compare_ToolStripMenuItem.Image = Global.WinAP.WFE.My.Resources.Resources.product_compare_save
		Me.save_product_compare_ToolStripMenuItem.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.save_product_compare_ToolStripMenuItem.Name = "save_product_compare_ToolStripMenuItem"
		Me.save_product_compare_ToolStripMenuItem.Size = New System.Drawing.Size(201, 24)
		Me.save_product_compare_ToolStripMenuItem.Text = "儲存「產品比較分析」"
		'
		'load_product_compare_ToolStripMenuItem
		'
		Me.load_product_compare_ToolStripMenuItem.Image = Global.WinAP.WFE.My.Resources.Resources.product_compare_load
		Me.load_product_compare_ToolStripMenuItem.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.load_product_compare_ToolStripMenuItem.Name = "load_product_compare_ToolStripMenuItem"
		Me.load_product_compare_ToolStripMenuItem.Size = New System.Drawing.Size(201, 24)
		Me.load_product_compare_ToolStripMenuItem.Text = "載入「產品比較分析」"
		'
		'StatusStrip1
		'
		Me.StatusStrip1.ImageScalingSize = New System.Drawing.Size(20, 20)
		Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ProductComparisonCaption_Label, Me.ComparisonName_Label})
		Me.StatusStrip1.Location = New System.Drawing.Point(0, 1055)
		Me.StatusStrip1.Name = "StatusStrip1"
		Me.StatusStrip1.Padding = New System.Windows.Forms.Padding(1, 0, 19, 0)
		Me.StatusStrip1.Size = New System.Drawing.Size(1352, 24)
		Me.StatusStrip1.TabIndex = 1
		Me.StatusStrip1.Text = "StatusStrip1"
		'
		'ProductComparisonCaption_Label
		'
		Me.ProductComparisonCaption_Label.Font = New System.Drawing.Font("Microsoft JhengHei UI", 9.0!, System.Drawing.FontStyle.Bold)
		Me.ProductComparisonCaption_Label.Name = "ProductComparisonCaption_Label"
		Me.ProductComparisonCaption_Label.Size = New System.Drawing.Size(171, 19)
		Me.ProductComparisonCaption_Label.Text = "產品比較分析專案名稱 : "
		'
		'ComparisonName_Label
		'
		Me.ComparisonName_Label.AutoToolTip = True
		Me.ComparisonName_Label.Font = New System.Drawing.Font("Microsoft JhengHei UI", 9.0!, System.Drawing.FontStyle.Bold)
		Me.ComparisonName_Label.ForeColor = System.Drawing.Color.Blue
		Me.ComparisonName_Label.Name = "ComparisonName_Label"
		Me.ComparisonName_Label.Size = New System.Drawing.Size(39, 19)
		Me.ComparisonName_Label.Text = "N/A"
		'
		'TabControl1
		'
		Me.TabControl1.Controls.Add(Me.TabPage1)
		Me.TabControl1.Controls.Add(Me.TabPage3)
		Me.TabControl1.Controls.Add(Me.TabPage4)
		Me.TabControl1.Dock = System.Windows.Forms.DockStyle.Fill
		Me.TabControl1.Location = New System.Drawing.Point(0, 28)
		Me.TabControl1.Margin = New System.Windows.Forms.Padding(4)
		Me.TabControl1.Name = "TabControl1"
		Me.TabControl1.Padding = New System.Drawing.Point(10, 10)
		Me.TabControl1.SelectedIndex = 0
		Me.TabControl1.Size = New System.Drawing.Size(1352, 1027)
		Me.TabControl1.TabIndex = 2
		'
		'TabPage1
		'
		Me.TabPage1.Controls.Add(Me.Page1_Main_FlowLayoutPanel)
		Me.TabPage1.Location = New System.Drawing.Point(4, 39)
		Me.TabPage1.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
		Me.TabPage1.Name = "TabPage1"
		Me.TabPage1.Size = New System.Drawing.Size(1344, 984)
		Me.TabPage1.TabIndex = 0
		Me.TabPage1.Text = "輸入產品條件"
		Me.TabPage1.UseVisualStyleBackColor = True
		'
		'Page1_Main_FlowLayoutPanel
		'
		Me.Page1_Main_FlowLayoutPanel.Controls.Add(Me.choice_product_compare_GroupBox)
		Me.Page1_Main_FlowLayoutPanel.Controls.Add(Me.product_compare_condition_GroupBox)
		Me.Page1_Main_FlowLayoutPanel.Controls.Add(Me.GroupBox3)
		Me.Page1_Main_FlowLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill
		Me.Page1_Main_FlowLayoutPanel.Location = New System.Drawing.Point(0, 0)
		Me.Page1_Main_FlowLayoutPanel.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
		Me.Page1_Main_FlowLayoutPanel.Name = "Page1_Main_FlowLayoutPanel"
		Me.Page1_Main_FlowLayoutPanel.Size = New System.Drawing.Size(1344, 984)
		Me.Page1_Main_FlowLayoutPanel.TabIndex = 6
		'
		'choice_product_compare_GroupBox
		'
		Me.choice_product_compare_GroupBox.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
		Me.choice_product_compare_GroupBox.Controls.Add(Me.choice_product_compare_DataGridView)
		Me.choice_product_compare_GroupBox.Controls.Add(Me.FlowLayoutPanel2)
		Me.choice_product_compare_GroupBox.Location = New System.Drawing.Point(4, 4)
		Me.choice_product_compare_GroupBox.Margin = New System.Windows.Forms.Padding(4)
		Me.choice_product_compare_GroupBox.Name = "choice_product_compare_GroupBox"
		Me.choice_product_compare_GroupBox.Padding = New System.Windows.Forms.Padding(8)
		Me.choice_product_compare_GroupBox.Size = New System.Drawing.Size(1333, 238)
		Me.choice_product_compare_GroupBox.TabIndex = 3
		Me.choice_product_compare_GroupBox.TabStop = False
		Me.choice_product_compare_GroupBox.Text = "選擇比較產品"
		'
		'choice_product_compare_DataGridView
		'
		Me.choice_product_compare_DataGridView.AllowUserToAddRows = False
		Me.choice_product_compare_DataGridView.AllowUserToDeleteRows = False
		Me.choice_product_compare_DataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
		Me.choice_product_compare_DataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
		Me.choice_product_compare_DataGridView.Dock = System.Windows.Forms.DockStyle.Fill
		Me.choice_product_compare_DataGridView.Location = New System.Drawing.Point(8, 70)
		Me.choice_product_compare_DataGridView.Margin = New System.Windows.Forms.Padding(4)
		Me.choice_product_compare_DataGridView.Name = "choice_product_compare_DataGridView"
		Me.choice_product_compare_DataGridView.ReadOnly = True
		Me.choice_product_compare_DataGridView.RowTemplate.Height = 24
		Me.choice_product_compare_DataGridView.Size = New System.Drawing.Size(1317, 160)
		Me.choice_product_compare_DataGridView.TabIndex = 4
		'
		'FlowLayoutPanel2
		'
		Me.FlowLayoutPanel2.Controls.Add(Me.add_Button)
		Me.FlowLayoutPanel2.Controls.Add(Me.clear_Button)
		Me.FlowLayoutPanel2.Controls.Add(Me.delete_Button)
		Me.FlowLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Top
		Me.FlowLayoutPanel2.Location = New System.Drawing.Point(8, 26)
		Me.FlowLayoutPanel2.Margin = New System.Windows.Forms.Padding(4)
		Me.FlowLayoutPanel2.Name = "FlowLayoutPanel2"
		Me.FlowLayoutPanel2.Size = New System.Drawing.Size(1317, 44)
		Me.FlowLayoutPanel2.TabIndex = 3
		'
		'add_Button
		'
		Me.add_Button.ForeColor = System.Drawing.Color.Blue
		Me.add_Button.Location = New System.Drawing.Point(4, 4)
		Me.add_Button.Margin = New System.Windows.Forms.Padding(4)
		Me.add_Button.Name = "add_Button"
		Me.add_Button.Size = New System.Drawing.Size(429, 38)
		Me.add_Button.TabIndex = 0
		Me.add_Button.Text = "新增"
		Me.add_Button.UseVisualStyleBackColor = True
		'
		'clear_Button
		'
		Me.clear_Button.ForeColor = System.Drawing.Color.Magenta
		Me.clear_Button.Location = New System.Drawing.Point(441, 4)
		Me.clear_Button.Margin = New System.Windows.Forms.Padding(4)
		Me.clear_Button.Name = "clear_Button"
		Me.clear_Button.Size = New System.Drawing.Size(429, 38)
		Me.clear_Button.TabIndex = 1
		Me.clear_Button.Text = "全部刪除"
		Me.clear_Button.UseVisualStyleBackColor = True
		'
		'delete_Button
		'
		Me.delete_Button.ForeColor = System.Drawing.Color.Red
		Me.delete_Button.Location = New System.Drawing.Point(878, 4)
		Me.delete_Button.Margin = New System.Windows.Forms.Padding(4)
		Me.delete_Button.Name = "delete_Button"
		Me.delete_Button.Size = New System.Drawing.Size(432, 38)
		Me.delete_Button.TabIndex = 2
		Me.delete_Button.Text = "刪除"
		Me.delete_Button.UseVisualStyleBackColor = True
		'
		'product_compare_condition_GroupBox
		'
		Me.product_compare_condition_GroupBox.Controls.Add(Me.product_compare_condition_group_main_Panel)
		Me.product_compare_condition_GroupBox.Location = New System.Drawing.Point(4, 250)
		Me.product_compare_condition_GroupBox.Margin = New System.Windows.Forms.Padding(4)
		Me.product_compare_condition_GroupBox.Name = "product_compare_condition_GroupBox"
		Me.product_compare_condition_GroupBox.Padding = New System.Windows.Forms.Padding(8)
		Me.product_compare_condition_GroupBox.Size = New System.Drawing.Size(1333, 481)
		Me.product_compare_condition_GroupBox.TabIndex = 4
		Me.product_compare_condition_GroupBox.TabStop = False
		Me.product_compare_condition_GroupBox.Text = "產品比較分析評估"
		'
		'product_compare_condition_group_main_Panel
		'
		Me.product_compare_condition_group_main_Panel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.product_compare_condition_group_main_Panel.Controls.Add(Me.GroupBox6)
		Me.product_compare_condition_group_main_Panel.Controls.Add(Me.GroupBox4)
		Me.product_compare_condition_group_main_Panel.Controls.Add(Me.GroupBox5)
		Me.product_compare_condition_group_main_Panel.Dock = System.Windows.Forms.DockStyle.Fill
		Me.product_compare_condition_group_main_Panel.Location = New System.Drawing.Point(8, 26)
		Me.product_compare_condition_group_main_Panel.Margin = New System.Windows.Forms.Padding(4)
		Me.product_compare_condition_group_main_Panel.Name = "product_compare_condition_group_main_Panel"
		Me.product_compare_condition_group_main_Panel.Size = New System.Drawing.Size(1317, 447)
		Me.product_compare_condition_group_main_Panel.TabIndex = 0
		'
		'GroupBox6
		'
		Me.GroupBox6.Controls.Add(Me.FlowLayoutPanel5)
		Me.GroupBox6.ForeColor = System.Drawing.Color.Blue
		Me.GroupBox6.Location = New System.Drawing.Point(4, 4)
		Me.GroupBox6.Margin = New System.Windows.Forms.Padding(4)
		Me.GroupBox6.Name = "GroupBox6"
		Me.GroupBox6.Padding = New System.Windows.Forms.Padding(4)
		Me.GroupBox6.Size = New System.Drawing.Size(1311, 62)
		Me.GroupBox6.TabIndex = 6
		Me.GroupBox6.TabStop = False
		Me.GroupBox6.Text = "條件一 : 產品總水足跡"
		'
		'FlowLayoutPanel5
		'
		Me.FlowLayoutPanel5.Controls.Add(Me.Label5)
		Me.FlowLayoutPanel5.Controls.Add(Me.productName_TextBox)
		Me.FlowLayoutPanel5.Controls.Add(Me.Label6)
		Me.FlowLayoutPanel5.Controls.Add(Me.WFP_TextBox)
		Me.FlowLayoutPanel5.Controls.Add(Me.Label1)
		Me.FlowLayoutPanel5.Controls.Add(Me.ISO_TextBox)
		Me.FlowLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill
		Me.FlowLayoutPanel5.Location = New System.Drawing.Point(4, 22)
		Me.FlowLayoutPanel5.Margin = New System.Windows.Forms.Padding(4)
		Me.FlowLayoutPanel5.Name = "FlowLayoutPanel5"
		Me.FlowLayoutPanel5.Size = New System.Drawing.Size(1303, 36)
		Me.FlowLayoutPanel5.TabIndex = 3
		'
		'Label5
		'
		Me.Label5.AutoSize = True
		Me.Label5.Location = New System.Drawing.Point(4, 10)
		Me.Label5.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label5.Name = "Label5"
		Me.Label5.Size = New System.Drawing.Size(67, 15)
		Me.Label5.TabIndex = 0
		Me.Label5.Text = "產品名稱"
		'
		'productName_TextBox
		'
		Me.productName_TextBox.Location = New System.Drawing.Point(79, 4)
		Me.productName_TextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.productName_TextBox.Name = "productName_TextBox"
		Me.productName_TextBox.Size = New System.Drawing.Size(487, 25)
		Me.productName_TextBox.TabIndex = 1
		'
		'Label6
		'
		Me.Label6.AutoSize = True
		Me.Label6.Location = New System.Drawing.Point(574, 10)
		Me.Label6.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label6.Name = "Label6"
		Me.Label6.Size = New System.Drawing.Size(112, 15)
		Me.Label6.TabIndex = 2
		Me.Label6.Text = "總水足跡 (WFN)"
		'
		'WFP_TextBox
		'
		Me.WFP_TextBox.Location = New System.Drawing.Point(694, 4)
		Me.WFP_TextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.WFP_TextBox.Name = "WFP_TextBox"
		Me.WFP_TextBox.Size = New System.Drawing.Size(205, 25)
		Me.WFP_TextBox.TabIndex = 3
		'
		'Label1
		'
		Me.Label1.AutoSize = True
		Me.Label1.Location = New System.Drawing.Point(907, 10)
		Me.Label1.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label1.Name = "Label1"
		Me.Label1.Size = New System.Drawing.Size(143, 15)
		Me.Label1.TabIndex = 4
		Me.Label1.Text = "總水足跡 (ISO 14046)"
		'
		'ISO_TextBox
		'
		Me.ISO_TextBox.Location = New System.Drawing.Point(1058, 4)
		Me.ISO_TextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.ISO_TextBox.Name = "ISO_TextBox"
		Me.ISO_TextBox.Size = New System.Drawing.Size(205, 25)
		Me.ISO_TextBox.TabIndex = 5
		'
		'GroupBox4
		'
		Me.GroupBox4.Controls.Add(Me.FlowLayoutPanel11)
		Me.GroupBox4.Controls.Add(Me.FlowLayoutPanel10)
		Me.GroupBox4.ForeColor = System.Drawing.Color.Green
		Me.GroupBox4.Location = New System.Drawing.Point(4, 74)
		Me.GroupBox4.Margin = New System.Windows.Forms.Padding(4)
		Me.GroupBox4.Name = "GroupBox4"
		Me.GroupBox4.Padding = New System.Windows.Forms.Padding(8)
		Me.GroupBox4.Size = New System.Drawing.Size(1316, 256)
		Me.GroupBox4.TabIndex = 4
		Me.GroupBox4.TabStop = False
		Me.GroupBox4.Text = "條件二 : 產品性能表現"
		'
		'FlowLayoutPanel11
		'
		Me.FlowLayoutPanel11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.FlowLayoutPanel11.Controls.Add(Me.condition_2_camera_Panel)
		Me.FlowLayoutPanel11.Controls.Add(Me.condition_2_LCD_Panel)
		Me.FlowLayoutPanel11.Dock = System.Windows.Forms.DockStyle.Fill
		Me.FlowLayoutPanel11.Location = New System.Drawing.Point(8, 55)
		Me.FlowLayoutPanel11.Margin = New System.Windows.Forms.Padding(4)
		Me.FlowLayoutPanel11.Name = "FlowLayoutPanel11"
		Me.FlowLayoutPanel11.Size = New System.Drawing.Size(1300, 193)
		Me.FlowLayoutPanel11.TabIndex = 2
		'
		'condition_2_camera_Panel
		'
		Me.condition_2_camera_Panel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.condition_2_camera_Panel.Controls.Add(Me.GroupBox7)
		Me.condition_2_camera_Panel.Controls.Add(Me.GroupBox8)
		Me.condition_2_camera_Panel.Controls.Add(Me.GroupBox9)
		Me.condition_2_camera_Panel.Location = New System.Drawing.Point(4, 4)
		Me.condition_2_camera_Panel.Margin = New System.Windows.Forms.Padding(4)
		Me.condition_2_camera_Panel.Name = "condition_2_camera_Panel"
		Me.condition_2_camera_Panel.Size = New System.Drawing.Size(514, 181)
		Me.condition_2_camera_Panel.TabIndex = 4
		Me.condition_2_camera_Panel.Tag = "Camera"
		'
		'GroupBox7
		'
		Me.GroupBox7.Controls.Add(Me.FlowLayoutPanel7)
		Me.GroupBox7.Location = New System.Drawing.Point(4, 4)
		Me.GroupBox7.Margin = New System.Windows.Forms.Padding(4)
		Me.GroupBox7.Name = "GroupBox7"
		Me.GroupBox7.Padding = New System.Windows.Forms.Padding(4)
		Me.GroupBox7.Size = New System.Drawing.Size(160, 169)
		Me.GroupBox7.TabIndex = 0
		Me.GroupBox7.TabStop = False
		Me.GroupBox7.Tag = "P1"
		Me.GroupBox7.Text = "傳感器大小"
		'
		'FlowLayoutPanel7
		'
		Me.FlowLayoutPanel7.Controls.Add(Me.p1_camera_radio1)
		Me.FlowLayoutPanel7.Controls.Add(Me.p1_camera_radio2)
		Me.FlowLayoutPanel7.Controls.Add(Me.p1_camera_radio3)
		Me.FlowLayoutPanel7.Controls.Add(Me.p1_camera_radio4)
		Me.FlowLayoutPanel7.Controls.Add(Me.p1_camera_radio5)
		Me.FlowLayoutPanel7.Dock = System.Windows.Forms.DockStyle.Fill
		Me.FlowLayoutPanel7.Location = New System.Drawing.Point(4, 22)
		Me.FlowLayoutPanel7.Margin = New System.Windows.Forms.Padding(4)
		Me.FlowLayoutPanel7.Name = "FlowLayoutPanel7"
		Me.FlowLayoutPanel7.Size = New System.Drawing.Size(152, 143)
		Me.FlowLayoutPanel7.TabIndex = 0
		Me.FlowLayoutPanel7.Tag = "P1"
		'
		'p1_camera_radio1
		'
		Me.p1_camera_radio1.AutoSize = True
		Me.p1_camera_radio1.Checked = True
		Me.p1_camera_radio1.Location = New System.Drawing.Point(4, 4)
		Me.p1_camera_radio1.Margin = New System.Windows.Forms.Padding(4)
		Me.p1_camera_radio1.Name = "p1_camera_radio1"
		Me.p1_camera_radio1.Size = New System.Drawing.Size(88, 19)
		Me.p1_camera_radio1.TabIndex = 0
		Me.p1_camera_radio1.TabStop = True
		Me.p1_camera_radio1.Tag = "11"
		Me.p1_camera_radio1.Text = "1 / 3 英吋"
		Me.p1_camera_radio1.UseVisualStyleBackColor = True
		'
		'p1_camera_radio2
		'
		Me.p1_camera_radio2.AutoSize = True
		Me.p1_camera_radio2.Location = New System.Drawing.Point(4, 31)
		Me.p1_camera_radio2.Margin = New System.Windows.Forms.Padding(4)
		Me.p1_camera_radio2.Name = "p1_camera_radio2"
		Me.p1_camera_radio2.Size = New System.Drawing.Size(99, 19)
		Me.p1_camera_radio2.TabIndex = 1
		Me.p1_camera_radio2.Tag = "7"
		Me.p1_camera_radio2.Text = "1 / 2.6 英吋"
		Me.p1_camera_radio2.UseVisualStyleBackColor = True
		'
		'p1_camera_radio3
		'
		Me.p1_camera_radio3.AutoSize = True
		Me.p1_camera_radio3.Location = New System.Drawing.Point(4, 58)
		Me.p1_camera_radio3.Margin = New System.Windows.Forms.Padding(4)
		Me.p1_camera_radio3.Name = "p1_camera_radio3"
		Me.p1_camera_radio3.Size = New System.Drawing.Size(99, 19)
		Me.p1_camera_radio3.TabIndex = 2
		Me.p1_camera_radio3.Tag = "4"
		Me.p1_camera_radio3.Text = "1 / 2.3 英吋"
		Me.p1_camera_radio3.UseVisualStyleBackColor = True
		'
		'p1_camera_radio4
		'
		Me.p1_camera_radio4.AutoSize = True
		Me.p1_camera_radio4.Location = New System.Drawing.Point(4, 85)
		Me.p1_camera_radio4.Margin = New System.Windows.Forms.Padding(4)
		Me.p1_camera_radio4.Name = "p1_camera_radio4"
		Me.p1_camera_radio4.Size = New System.Drawing.Size(99, 19)
		Me.p1_camera_radio4.TabIndex = 3
		Me.p1_camera_radio4.Tag = "2"
		Me.p1_camera_radio4.Text = "1 / 1.5 英吋"
		Me.p1_camera_radio4.UseVisualStyleBackColor = True
		'
		'p1_camera_radio5
		'
		Me.p1_camera_radio5.AutoSize = True
		Me.p1_camera_radio5.Location = New System.Drawing.Point(4, 112)
		Me.p1_camera_radio5.Margin = New System.Windows.Forms.Padding(4)
		Me.p1_camera_radio5.Name = "p1_camera_radio5"
		Me.p1_camera_radio5.Size = New System.Drawing.Size(99, 19)
		Me.p1_camera_radio5.TabIndex = 4
		Me.p1_camera_radio5.Tag = "1"
		Me.p1_camera_radio5.Text = "1 / 1.2 英吋"
		Me.p1_camera_radio5.UseVisualStyleBackColor = True
		'
		'GroupBox8
		'
		Me.GroupBox8.Controls.Add(Me.FlowLayoutPanel8)
		Me.GroupBox8.Location = New System.Drawing.Point(172, 4)
		Me.GroupBox8.Margin = New System.Windows.Forms.Padding(4)
		Me.GroupBox8.Name = "GroupBox8"
		Me.GroupBox8.Padding = New System.Windows.Forms.Padding(4)
		Me.GroupBox8.Size = New System.Drawing.Size(160, 169)
		Me.GroupBox8.TabIndex = 1
		Me.GroupBox8.TabStop = False
		Me.GroupBox8.Tag = "P2"
		Me.GroupBox8.Text = "像素大小"
		'
		'FlowLayoutPanel8
		'
		Me.FlowLayoutPanel8.Controls.Add(Me.p2_camera_radio1)
		Me.FlowLayoutPanel8.Controls.Add(Me.p2_camera_radio2)
		Me.FlowLayoutPanel8.Controls.Add(Me.p2_camera_radio3)
		Me.FlowLayoutPanel8.Controls.Add(Me.p2_camera_radio4)
		Me.FlowLayoutPanel8.Controls.Add(Me.p2_camera_radio5)
		Me.FlowLayoutPanel8.Dock = System.Windows.Forms.DockStyle.Fill
		Me.FlowLayoutPanel8.Location = New System.Drawing.Point(4, 22)
		Me.FlowLayoutPanel8.Margin = New System.Windows.Forms.Padding(4)
		Me.FlowLayoutPanel8.Name = "FlowLayoutPanel8"
		Me.FlowLayoutPanel8.Size = New System.Drawing.Size(152, 143)
		Me.FlowLayoutPanel8.TabIndex = 0
		Me.FlowLayoutPanel8.Tag = "P2"
		'
		'p2_camera_radio1
		'
		Me.p2_camera_radio1.AutoSize = True
		Me.p2_camera_radio1.Checked = True
		Me.p2_camera_radio1.Location = New System.Drawing.Point(4, 4)
		Me.p2_camera_radio1.Margin = New System.Windows.Forms.Padding(4)
		Me.p2_camera_radio1.Name = "p2_camera_radio1"
		Me.p2_camera_radio1.Size = New System.Drawing.Size(68, 19)
		Me.p2_camera_radio1.TabIndex = 0
		Me.p2_camera_radio1.TabStop = True
		Me.p2_camera_radio1.Tag = "11"
		Me.p2_camera_radio1.Text = "500 萬"
		Me.p2_camera_radio1.UseVisualStyleBackColor = True
		'
		'p2_camera_radio2
		'
		Me.p2_camera_radio2.AutoSize = True
		Me.p2_camera_radio2.Location = New System.Drawing.Point(4, 31)
		Me.p2_camera_radio2.Margin = New System.Windows.Forms.Padding(4)
		Me.p2_camera_radio2.Name = "p2_camera_radio2"
		Me.p2_camera_radio2.Size = New System.Drawing.Size(72, 19)
		Me.p2_camera_radio2.TabIndex = 1
		Me.p2_camera_radio2.Tag = "7"
		Me.p2_camera_radio2.Text = "800  萬"
		Me.p2_camera_radio2.UseVisualStyleBackColor = True
		'
		'p2_camera_radio3
		'
		Me.p2_camera_radio3.AutoSize = True
		Me.p2_camera_radio3.Location = New System.Drawing.Point(4, 58)
		Me.p2_camera_radio3.Margin = New System.Windows.Forms.Padding(4)
		Me.p2_camera_radio3.Name = "p2_camera_radio3"
		Me.p2_camera_radio3.Size = New System.Drawing.Size(79, 19)
		Me.p2_camera_radio3.TabIndex = 2
		Me.p2_camera_radio3.Tag = "4"
		Me.p2_camera_radio3.Text = "1200  萬"
		Me.p2_camera_radio3.UseVisualStyleBackColor = True
		'
		'p2_camera_radio4
		'
		Me.p2_camera_radio4.AutoSize = True
		Me.p2_camera_radio4.Location = New System.Drawing.Point(4, 85)
		Me.p2_camera_radio4.Margin = New System.Windows.Forms.Padding(4)
		Me.p2_camera_radio4.Name = "p2_camera_radio4"
		Me.p2_camera_radio4.Size = New System.Drawing.Size(79, 19)
		Me.p2_camera_radio4.TabIndex = 3
		Me.p2_camera_radio4.Tag = "2"
		Me.p2_camera_radio4.Text = "1300  萬"
		Me.p2_camera_radio4.UseVisualStyleBackColor = True
		'
		'p2_camera_radio5
		'
		Me.p2_camera_radio5.AutoSize = True
		Me.p2_camera_radio5.Location = New System.Drawing.Point(4, 112)
		Me.p2_camera_radio5.Margin = New System.Windows.Forms.Padding(4)
		Me.p2_camera_radio5.Name = "p2_camera_radio5"
		Me.p2_camera_radio5.Size = New System.Drawing.Size(79, 19)
		Me.p2_camera_radio5.TabIndex = 4
		Me.p2_camera_radio5.Tag = "1"
		Me.p2_camera_radio5.Text = "1600  萬"
		Me.p2_camera_radio5.UseVisualStyleBackColor = True
		'
		'GroupBox9
		'
		Me.GroupBox9.Controls.Add(Me.FlowLayoutPanel9)
		Me.GroupBox9.Location = New System.Drawing.Point(340, 4)
		Me.GroupBox9.Margin = New System.Windows.Forms.Padding(4)
		Me.GroupBox9.Name = "GroupBox9"
		Me.GroupBox9.Padding = New System.Windows.Forms.Padding(4)
		Me.GroupBox9.Size = New System.Drawing.Size(155, 169)
		Me.GroupBox9.TabIndex = 2
		Me.GroupBox9.TabStop = False
		Me.GroupBox9.Tag = "P3"
		Me.GroupBox9.Text = "光圈大小"
		'
		'FlowLayoutPanel9
		'
		Me.FlowLayoutPanel9.Controls.Add(Me.p3_camera_radio1)
		Me.FlowLayoutPanel9.Controls.Add(Me.p3_camera_radio2)
		Me.FlowLayoutPanel9.Controls.Add(Me.p3_camera_radio3)
		Me.FlowLayoutPanel9.Controls.Add(Me.p3_camera_radio4)
		Me.FlowLayoutPanel9.Controls.Add(Me.p3_camera_radio5)
		Me.FlowLayoutPanel9.Dock = System.Windows.Forms.DockStyle.Fill
		Me.FlowLayoutPanel9.Location = New System.Drawing.Point(4, 22)
		Me.FlowLayoutPanel9.Margin = New System.Windows.Forms.Padding(4)
		Me.FlowLayoutPanel9.Name = "FlowLayoutPanel9"
		Me.FlowLayoutPanel9.Size = New System.Drawing.Size(147, 143)
		Me.FlowLayoutPanel9.TabIndex = 0
		Me.FlowLayoutPanel9.Tag = "P3"
		'
		'p3_camera_radio1
		'
		Me.p3_camera_radio1.AutoSize = True
		Me.p3_camera_radio1.Checked = True
		Me.p3_camera_radio1.Location = New System.Drawing.Point(4, 4)
		Me.p3_camera_radio1.Margin = New System.Windows.Forms.Padding(4)
		Me.p3_camera_radio1.Name = "p3_camera_radio1"
		Me.p3_camera_radio1.Size = New System.Drawing.Size(66, 19)
		Me.p3_camera_radio1.TabIndex = 0
		Me.p3_camera_radio1.TabStop = True
		Me.p3_camera_radio1.Tag = "11"
		Me.p3_camera_radio1.Text = "F / 2.2"
		Me.p3_camera_radio1.UseVisualStyleBackColor = True
		'
		'p3_camera_radio2
		'
		Me.p3_camera_radio2.AutoSize = True
		Me.p3_camera_radio2.Location = New System.Drawing.Point(4, 31)
		Me.p3_camera_radio2.Margin = New System.Windows.Forms.Padding(4)
		Me.p3_camera_radio2.Name = "p3_camera_radio2"
		Me.p3_camera_radio2.Size = New System.Drawing.Size(66, 19)
		Me.p3_camera_radio2.TabIndex = 1
		Me.p3_camera_radio2.Tag = "7"
		Me.p3_camera_radio2.Text = "F / 2.0"
		Me.p3_camera_radio2.UseVisualStyleBackColor = True
		'
		'p3_camera_radio3
		'
		Me.p3_camera_radio3.AutoSize = True
		Me.p3_camera_radio3.Location = New System.Drawing.Point(4, 58)
		Me.p3_camera_radio3.Margin = New System.Windows.Forms.Padding(4)
		Me.p3_camera_radio3.Name = "p3_camera_radio3"
		Me.p3_camera_radio3.Size = New System.Drawing.Size(66, 19)
		Me.p3_camera_radio3.TabIndex = 2
		Me.p3_camera_radio3.Tag = "4"
		Me.p3_camera_radio3.Text = "F / 1.9"
		Me.p3_camera_radio3.UseVisualStyleBackColor = True
		'
		'p3_camera_radio4
		'
		Me.p3_camera_radio4.AutoSize = True
		Me.p3_camera_radio4.Location = New System.Drawing.Point(4, 85)
		Me.p3_camera_radio4.Margin = New System.Windows.Forms.Padding(4)
		Me.p3_camera_radio4.Name = "p3_camera_radio4"
		Me.p3_camera_radio4.Size = New System.Drawing.Size(66, 19)
		Me.p3_camera_radio4.TabIndex = 3
		Me.p3_camera_radio4.Tag = "2"
		Me.p3_camera_radio4.Text = "F / 1.8"
		Me.p3_camera_radio4.UseVisualStyleBackColor = True
		'
		'p3_camera_radio5
		'
		Me.p3_camera_radio5.AutoSize = True
		Me.p3_camera_radio5.Location = New System.Drawing.Point(4, 112)
		Me.p3_camera_radio5.Margin = New System.Windows.Forms.Padding(4)
		Me.p3_camera_radio5.Name = "p3_camera_radio5"
		Me.p3_camera_radio5.Size = New System.Drawing.Size(66, 19)
		Me.p3_camera_radio5.TabIndex = 4
		Me.p3_camera_radio5.Tag = "1"
		Me.p3_camera_radio5.Text = "F / 1.7"
		Me.p3_camera_radio5.UseVisualStyleBackColor = True
		'
		'condition_2_LCD_Panel
		'
		Me.condition_2_LCD_Panel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.condition_2_LCD_Panel.Controls.Add(Me.GroupBox10)
		Me.condition_2_LCD_Panel.Controls.Add(Me.GroupBox11)
		Me.condition_2_LCD_Panel.Controls.Add(Me.GroupBox12)
		Me.condition_2_LCD_Panel.Location = New System.Drawing.Point(526, 4)
		Me.condition_2_LCD_Panel.Margin = New System.Windows.Forms.Padding(4)
		Me.condition_2_LCD_Panel.Name = "condition_2_LCD_Panel"
		Me.condition_2_LCD_Panel.Size = New System.Drawing.Size(602, 181)
		Me.condition_2_LCD_Panel.TabIndex = 5
		Me.condition_2_LCD_Panel.Tag = "LCD"
		'
		'GroupBox10
		'
		Me.GroupBox10.Controls.Add(Me.FlowLayoutPanel4)
		Me.GroupBox10.Location = New System.Drawing.Point(4, 4)
		Me.GroupBox10.Margin = New System.Windows.Forms.Padding(4)
		Me.GroupBox10.Name = "GroupBox10"
		Me.GroupBox10.Padding = New System.Windows.Forms.Padding(4)
		Me.GroupBox10.Size = New System.Drawing.Size(152, 169)
		Me.GroupBox10.TabIndex = 3
		Me.GroupBox10.TabStop = False
		Me.GroupBox10.Tag = "P1"
		Me.GroupBox10.Text = "像素密度"
		'
		'FlowLayoutPanel4
		'
		Me.FlowLayoutPanel4.Controls.Add(Me.p1_lcd_radio1)
		Me.FlowLayoutPanel4.Controls.Add(Me.p1_lcd_radio2)
		Me.FlowLayoutPanel4.Controls.Add(Me.p1_lcd_radio3)
		Me.FlowLayoutPanel4.Controls.Add(Me.p1_lcd_radio4)
		Me.FlowLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill
		Me.FlowLayoutPanel4.Location = New System.Drawing.Point(4, 22)
		Me.FlowLayoutPanel4.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
		Me.FlowLayoutPanel4.Name = "FlowLayoutPanel4"
		Me.FlowLayoutPanel4.Size = New System.Drawing.Size(144, 143)
		Me.FlowLayoutPanel4.TabIndex = 9
		Me.FlowLayoutPanel4.Tag = "P1"
		'
		'p1_lcd_radio1
		'
		Me.p1_lcd_radio1.AutoSize = True
		Me.p1_lcd_radio1.Checked = True
		Me.p1_lcd_radio1.Location = New System.Drawing.Point(4, 4)
		Me.p1_lcd_radio1.Margin = New System.Windows.Forms.Padding(4)
		Me.p1_lcd_radio1.Name = "p1_lcd_radio1"
		Me.p1_lcd_radio1.Size = New System.Drawing.Size(86, 19)
		Me.p1_lcd_radio1.TabIndex = 5
		Me.p1_lcd_radio1.TabStop = True
		Me.p1_lcd_radio1.Tag = "7"
		Me.p1_lcd_radio1.Text = "234 ~ 342"
		Me.p1_lcd_radio1.UseVisualStyleBackColor = True
		'
		'p1_lcd_radio2
		'
		Me.p1_lcd_radio2.AutoSize = True
		Me.p1_lcd_radio2.Location = New System.Drawing.Point(4, 31)
		Me.p1_lcd_radio2.Margin = New System.Windows.Forms.Padding(4)
		Me.p1_lcd_radio2.Name = "p1_lcd_radio2"
		Me.p1_lcd_radio2.Size = New System.Drawing.Size(86, 19)
		Me.p1_lcd_radio2.TabIndex = 6
		Me.p1_lcd_radio2.Tag = "4"
		Me.p1_lcd_radio2.Text = "343 ~ 468"
		Me.p1_lcd_radio2.UseVisualStyleBackColor = True
		'
		'p1_lcd_radio3
		'
		Me.p1_lcd_radio3.AutoSize = True
		Me.p1_lcd_radio3.Location = New System.Drawing.Point(4, 58)
		Me.p1_lcd_radio3.Margin = New System.Windows.Forms.Padding(4)
		Me.p1_lcd_radio3.Name = "p1_lcd_radio3"
		Me.p1_lcd_radio3.Size = New System.Drawing.Size(86, 19)
		Me.p1_lcd_radio3.TabIndex = 7
		Me.p1_lcd_radio3.Tag = "2"
		Me.p1_lcd_radio3.Text = "490 ~ 577"
		Me.p1_lcd_radio3.UseVisualStyleBackColor = True
		'
		'p1_lcd_radio4
		'
		Me.p1_lcd_radio4.AutoSize = True
		Me.p1_lcd_radio4.Location = New System.Drawing.Point(4, 85)
		Me.p1_lcd_radio4.Margin = New System.Windows.Forms.Padding(4)
		Me.p1_lcd_radio4.Name = "p1_lcd_radio4"
		Me.p1_lcd_radio4.Size = New System.Drawing.Size(86, 19)
		Me.p1_lcd_radio4.TabIndex = 8
		Me.p1_lcd_radio4.Tag = "1"
		Me.p1_lcd_radio4.Text = "801 ~ 808"
		Me.p1_lcd_radio4.UseVisualStyleBackColor = True
		'
		'GroupBox11
		'
		Me.GroupBox11.Controls.Add(Me.FlowLayoutPanel12)
		Me.GroupBox11.Location = New System.Drawing.Point(164, 4)
		Me.GroupBox11.Margin = New System.Windows.Forms.Padding(4)
		Me.GroupBox11.Name = "GroupBox11"
		Me.GroupBox11.Padding = New System.Windows.Forms.Padding(4)
		Me.GroupBox11.Size = New System.Drawing.Size(236, 169)
		Me.GroupBox11.TabIndex = 4
		Me.GroupBox11.TabStop = False
		Me.GroupBox11.Tag = "P2"
		Me.GroupBox11.Text = "像素總數"
		'
		'FlowLayoutPanel12
		'
		Me.FlowLayoutPanel12.Controls.Add(Me.p2_lcd_radio1)
		Me.FlowLayoutPanel12.Controls.Add(Me.p2_lcd_radio2)
		Me.FlowLayoutPanel12.Controls.Add(Me.p2_lcd_radio3)
		Me.FlowLayoutPanel12.Controls.Add(Me.p2_lcd_radio4)
		Me.FlowLayoutPanel12.Dock = System.Windows.Forms.DockStyle.Fill
		Me.FlowLayoutPanel12.Location = New System.Drawing.Point(4, 22)
		Me.FlowLayoutPanel12.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
		Me.FlowLayoutPanel12.Name = "FlowLayoutPanel12"
		Me.FlowLayoutPanel12.Size = New System.Drawing.Size(228, 143)
		Me.FlowLayoutPanel12.TabIndex = 9
		Me.FlowLayoutPanel12.Tag = "P2"
		'
		'p2_lcd_radio1
		'
		Me.p2_lcd_radio1.AutoSize = True
		Me.p2_lcd_radio1.Checked = True
		Me.p2_lcd_radio1.Location = New System.Drawing.Point(4, 4)
		Me.p2_lcd_radio1.Margin = New System.Windows.Forms.Padding(4)
		Me.p2_lcd_radio1.Name = "p2_lcd_radio1"
		Me.p2_lcd_radio1.Size = New System.Drawing.Size(130, 19)
		Me.p2_lcd_radio1.TabIndex = 5
		Me.p2_lcd_radio1.TabStop = True
		Me.p2_lcd_radio1.Tag = "7"
		Me.p2_lcd_radio1.Text = "HD ( 1280 * 720)"
		Me.p2_lcd_radio1.UseVisualStyleBackColor = True
		'
		'p2_lcd_radio2
		'
		Me.p2_lcd_radio2.AutoSize = True
		Me.p2_lcd_radio2.Location = New System.Drawing.Point(4, 31)
		Me.p2_lcd_radio2.Margin = New System.Windows.Forms.Padding(4)
		Me.p2_lcd_radio2.Name = "p2_lcd_radio2"
		Me.p2_lcd_radio2.Size = New System.Drawing.Size(158, 19)
		Me.p2_lcd_radio2.TabIndex = 6
		Me.p2_lcd_radio2.Tag = "4"
		Me.p2_lcd_radio2.Text = "FHD 1K (1920 *1080)"
		Me.p2_lcd_radio2.UseVisualStyleBackColor = True
		'
		'p2_lcd_radio3
		'
		Me.p2_lcd_radio3.AutoSize = True
		Me.p2_lcd_radio3.Location = New System.Drawing.Point(4, 58)
		Me.p2_lcd_radio3.Margin = New System.Windows.Forms.Padding(4)
		Me.p2_lcd_radio3.Name = "p2_lcd_radio3"
		Me.p2_lcd_radio3.Size = New System.Drawing.Size(164, 19)
		Me.p2_lcd_radio3.TabIndex = 7
		Me.p2_lcd_radio3.Tag = "2"
		Me.p2_lcd_radio3.Text = "QHD 2K (2560 * 1440)"
		Me.p2_lcd_radio3.UseVisualStyleBackColor = True
		'
		'p2_lcd_radio4
		'
		Me.p2_lcd_radio4.AutoSize = True
		Me.p2_lcd_radio4.Location = New System.Drawing.Point(4, 85)
		Me.p2_lcd_radio4.Margin = New System.Windows.Forms.Padding(4)
		Me.p2_lcd_radio4.Name = "p2_lcd_radio4"
		Me.p2_lcd_radio4.Size = New System.Drawing.Size(164, 19)
		Me.p2_lcd_radio4.TabIndex = 8
		Me.p2_lcd_radio4.Tag = "1"
		Me.p2_lcd_radio4.Text = "UHD 4K (2840 * 2160)"
		Me.p2_lcd_radio4.UseVisualStyleBackColor = True
		'
		'GroupBox12
		'
		Me.GroupBox12.Controls.Add(Me.FlowLayoutPanel13)
		Me.GroupBox12.Location = New System.Drawing.Point(408, 4)
		Me.GroupBox12.Margin = New System.Windows.Forms.Padding(4)
		Me.GroupBox12.Name = "GroupBox12"
		Me.GroupBox12.Padding = New System.Windows.Forms.Padding(4)
		Me.GroupBox12.Size = New System.Drawing.Size(177, 169)
		Me.GroupBox12.TabIndex = 5
		Me.GroupBox12.TabStop = False
		Me.GroupBox12.Tag = "P3"
		Me.GroupBox12.Text = "螢幕大小"
		'
		'FlowLayoutPanel13
		'
		Me.FlowLayoutPanel13.Controls.Add(Me.p3_lcd_radio1)
		Me.FlowLayoutPanel13.Controls.Add(Me.p3_lcd_radio2)
		Me.FlowLayoutPanel13.Controls.Add(Me.p3_lcd_radio3)
		Me.FlowLayoutPanel13.Controls.Add(Me.p3_lcd_radio4)
		Me.FlowLayoutPanel13.Dock = System.Windows.Forms.DockStyle.Fill
		Me.FlowLayoutPanel13.Location = New System.Drawing.Point(4, 22)
		Me.FlowLayoutPanel13.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
		Me.FlowLayoutPanel13.Name = "FlowLayoutPanel13"
		Me.FlowLayoutPanel13.Size = New System.Drawing.Size(169, 143)
		Me.FlowLayoutPanel13.TabIndex = 9
		Me.FlowLayoutPanel13.Tag = "P3"
		'
		'p3_lcd_radio1
		'
		Me.p3_lcd_radio1.AutoSize = True
		Me.p3_lcd_radio1.Checked = True
		Me.p3_lcd_radio1.Location = New System.Drawing.Point(4, 4)
		Me.p3_lcd_radio1.Margin = New System.Windows.Forms.Padding(4)
		Me.p3_lcd_radio1.Name = "p3_lcd_radio1"
		Me.p3_lcd_radio1.Size = New System.Drawing.Size(114, 19)
		Me.p3_lcd_radio1.TabIndex = 5
		Me.p3_lcd_radio1.TabStop = True
		Me.p3_lcd_radio1.Tag = "7"
		Me.p3_lcd_radio1.Text = "4.2 ~ 4.6 英吋"
		Me.p3_lcd_radio1.UseVisualStyleBackColor = True
		'
		'p3_lcd_radio2
		'
		Me.p3_lcd_radio2.AutoSize = True
		Me.p3_lcd_radio2.Location = New System.Drawing.Point(4, 31)
		Me.p3_lcd_radio2.Margin = New System.Windows.Forms.Padding(4)
		Me.p3_lcd_radio2.Name = "p3_lcd_radio2"
		Me.p3_lcd_radio2.Size = New System.Drawing.Size(114, 19)
		Me.p3_lcd_radio2.TabIndex = 6
		Me.p3_lcd_radio2.Tag = "4"
		Me.p3_lcd_radio2.Text = "4.7 ~ 5.1 英吋"
		Me.p3_lcd_radio2.UseVisualStyleBackColor = True
		'
		'p3_lcd_radio3
		'
		Me.p3_lcd_radio3.AutoSize = True
		Me.p3_lcd_radio3.Location = New System.Drawing.Point(4, 58)
		Me.p3_lcd_radio3.Margin = New System.Windows.Forms.Padding(4)
		Me.p3_lcd_radio3.Name = "p3_lcd_radio3"
		Me.p3_lcd_radio3.Size = New System.Drawing.Size(118, 19)
		Me.p3_lcd_radio3.TabIndex = 7
		Me.p3_lcd_radio3.Tag = "2"
		Me.p3_lcd_radio3.Text = "5.2 ~ 5.6  英吋"
		Me.p3_lcd_radio3.UseVisualStyleBackColor = True
		'
		'p3_lcd_radio4
		'
		Me.p3_lcd_radio4.AutoSize = True
		Me.p3_lcd_radio4.Location = New System.Drawing.Point(4, 85)
		Me.p3_lcd_radio4.Margin = New System.Windows.Forms.Padding(4)
		Me.p3_lcd_radio4.Name = "p3_lcd_radio4"
		Me.p3_lcd_radio4.Size = New System.Drawing.Size(114, 19)
		Me.p3_lcd_radio4.TabIndex = 8
		Me.p3_lcd_radio4.Tag = "1"
		Me.p3_lcd_radio4.Text = "5.7 ~ 6.1 英吋"
		Me.p3_lcd_radio4.UseVisualStyleBackColor = True
		'
		'FlowLayoutPanel10
		'
		Me.FlowLayoutPanel10.Controls.Add(Me.condition_2_camera_RadioButton)
		Me.FlowLayoutPanel10.Controls.Add(Me.condition_2_LCD_RadioButton)
		Me.FlowLayoutPanel10.Dock = System.Windows.Forms.DockStyle.Top
		Me.FlowLayoutPanel10.Location = New System.Drawing.Point(8, 26)
		Me.FlowLayoutPanel10.Margin = New System.Windows.Forms.Padding(4)
		Me.FlowLayoutPanel10.Name = "FlowLayoutPanel10"
		Me.FlowLayoutPanel10.Size = New System.Drawing.Size(1300, 29)
		Me.FlowLayoutPanel10.TabIndex = 1
		'
		'condition_2_camera_RadioButton
		'
		Me.condition_2_camera_RadioButton.AutoSize = True
		Me.condition_2_camera_RadioButton.Checked = True
		Me.condition_2_camera_RadioButton.Location = New System.Drawing.Point(4, 4)
		Me.condition_2_camera_RadioButton.Margin = New System.Windows.Forms.Padding(4)
		Me.condition_2_camera_RadioButton.Name = "condition_2_camera_RadioButton"
		Me.condition_2_camera_RadioButton.Size = New System.Drawing.Size(88, 19)
		Me.condition_2_camera_RadioButton.TabIndex = 0
		Me.condition_2_camera_RadioButton.TabStop = True
		Me.condition_2_camera_RadioButton.Tag = "Camera"
		Me.condition_2_camera_RadioButton.Text = "攝像模組"
		Me.condition_2_camera_RadioButton.UseVisualStyleBackColor = True
		'
		'condition_2_LCD_RadioButton
		'
		Me.condition_2_LCD_RadioButton.AutoSize = True
		Me.condition_2_LCD_RadioButton.Location = New System.Drawing.Point(100, 4)
		Me.condition_2_LCD_RadioButton.Margin = New System.Windows.Forms.Padding(4)
		Me.condition_2_LCD_RadioButton.Name = "condition_2_LCD_RadioButton"
		Me.condition_2_LCD_RadioButton.Size = New System.Drawing.Size(118, 19)
		Me.condition_2_LCD_RadioButton.TabIndex = 1
		Me.condition_2_LCD_RadioButton.Tag = "LCD"
		Me.condition_2_LCD_RadioButton.Text = "液晶螢幕總成"
		Me.condition_2_LCD_RadioButton.UseVisualStyleBackColor = True
		'
		'GroupBox5
		'
		Me.GroupBox5.Controls.Add(Me.Importance_FlowLayoutPanel)
		Me.GroupBox5.ForeColor = System.Drawing.Color.OrangeRed
		Me.GroupBox5.Location = New System.Drawing.Point(4, 338)
		Me.GroupBox5.Margin = New System.Windows.Forms.Padding(4)
		Me.GroupBox5.Name = "GroupBox5"
		Me.GroupBox5.Padding = New System.Windows.Forms.Padding(8)
		Me.GroupBox5.Size = New System.Drawing.Size(1316, 106)
		Me.GroupBox5.TabIndex = 5
		Me.GroupBox5.TabStop = False
		Me.GroupBox5.Text = "條件三 : 產品重要性"
		'
		'Importance_FlowLayoutPanel
		'
		Me.Importance_FlowLayoutPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.Importance_FlowLayoutPanel.Controls.Add(Me.Label7)
		Me.Importance_FlowLayoutPanel.Controls.Add(Me.importance_radio1)
		Me.Importance_FlowLayoutPanel.Controls.Add(Me.importance_radio2)
		Me.Importance_FlowLayoutPanel.Controls.Add(Me.importance_radio3)
		Me.Importance_FlowLayoutPanel.Controls.Add(Me.importance_radio4)
		Me.Importance_FlowLayoutPanel.Controls.Add(Me.importance_radio5)
		Me.Importance_FlowLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill
		Me.Importance_FlowLayoutPanel.Location = New System.Drawing.Point(8, 26)
		Me.Importance_FlowLayoutPanel.Margin = New System.Windows.Forms.Padding(4)
		Me.Importance_FlowLayoutPanel.Name = "Importance_FlowLayoutPanel"
		Me.Importance_FlowLayoutPanel.Size = New System.Drawing.Size(1300, 72)
		Me.Importance_FlowLayoutPanel.TabIndex = 0
		'
		'Label7
		'
		Me.Label7.Location = New System.Drawing.Point(4, 0)
		Me.Label7.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
		Me.Label7.Name = "Label7"
		Me.Label7.Size = New System.Drawing.Size(1269, 29)
		Me.Label7.TabIndex = 0
		Me.Label7.Text = "自訂重要程度"
		Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
		'
		'importance_radio1
		'
		Me.importance_radio1.Location = New System.Drawing.Point(4, 33)
		Me.importance_radio1.Margin = New System.Windows.Forms.Padding(4)
		Me.importance_radio1.Name = "importance_radio1"
		Me.importance_radio1.Size = New System.Drawing.Size(133, 20)
		Me.importance_radio1.TabIndex = 1
		Me.importance_radio1.Tag = "11"
		Me.importance_radio1.Text = "非常不重要"
		Me.importance_radio1.UseVisualStyleBackColor = True
		'
		'importance_radio2
		'
		Me.importance_radio2.Location = New System.Drawing.Point(145, 33)
		Me.importance_radio2.Margin = New System.Windows.Forms.Padding(4)
		Me.importance_radio2.Name = "importance_radio2"
		Me.importance_radio2.Size = New System.Drawing.Size(133, 20)
		Me.importance_radio2.TabIndex = 2
		Me.importance_radio2.Tag = "7"
		Me.importance_radio2.Text = "不重要"
		Me.importance_radio2.UseVisualStyleBackColor = True
		'
		'importance_radio3
		'
		Me.importance_radio3.Checked = True
		Me.importance_radio3.Location = New System.Drawing.Point(286, 33)
		Me.importance_radio3.Margin = New System.Windows.Forms.Padding(4)
		Me.importance_radio3.Name = "importance_radio3"
		Me.importance_radio3.Size = New System.Drawing.Size(133, 20)
		Me.importance_radio3.TabIndex = 3
		Me.importance_radio3.TabStop = True
		Me.importance_radio3.Tag = "4"
		Me.importance_radio3.Text = " 普通"
		Me.importance_radio3.UseVisualStyleBackColor = True
		'
		'importance_radio4
		'
		Me.importance_radio4.Location = New System.Drawing.Point(427, 33)
		Me.importance_radio4.Margin = New System.Windows.Forms.Padding(4)
		Me.importance_radio4.Name = "importance_radio4"
		Me.importance_radio4.Size = New System.Drawing.Size(133, 20)
		Me.importance_radio4.TabIndex = 4
		Me.importance_radio4.Tag = "2"
		Me.importance_radio4.Text = "重要"
		Me.importance_radio4.UseVisualStyleBackColor = True
		'
		'importance_radio5
		'
		Me.importance_radio5.Location = New System.Drawing.Point(568, 33)
		Me.importance_radio5.Margin = New System.Windows.Forms.Padding(4)
		Me.importance_radio5.Name = "importance_radio5"
		Me.importance_radio5.Size = New System.Drawing.Size(133, 20)
		Me.importance_radio5.TabIndex = 5
		Me.importance_radio5.Tag = "1"
		Me.importance_radio5.Text = "非常重要"
		Me.importance_radio5.UseVisualStyleBackColor = True
		'
		'GroupBox3
		'
		Me.GroupBox3.Controls.Add(Me.FlowLayoutPanel3)
		Me.GroupBox3.Location = New System.Drawing.Point(4, 739)
		Me.GroupBox3.Margin = New System.Windows.Forms.Padding(4)
		Me.GroupBox3.Name = "GroupBox3"
		Me.GroupBox3.Padding = New System.Windows.Forms.Padding(8)
		Me.GroupBox3.Size = New System.Drawing.Size(1333, 225)
		Me.GroupBox3.TabIndex = 5
		Me.GroupBox3.TabStop = False
		Me.GroupBox3.Text = "權重設定"
		'
		'FlowLayoutPanel3
		'
		Me.FlowLayoutPanel3.Controls.Add(Me.Label2)
		Me.FlowLayoutPanel3.Controls.Add(Me.w1_TrackBar)
		Me.FlowLayoutPanel3.Controls.Add(Me.w1_TextBox)
		Me.FlowLayoutPanel3.Controls.Add(Me.Label3)
		Me.FlowLayoutPanel3.Controls.Add(Me.w2_TrackBar)
		Me.FlowLayoutPanel3.Controls.Add(Me.w2_TextBox)
		Me.FlowLayoutPanel3.Controls.Add(Me.Label4)
		Me.FlowLayoutPanel3.Controls.Add(Me.w3_TrackBar)
		Me.FlowLayoutPanel3.Controls.Add(Me.w3_TextBox)
		Me.FlowLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill
		Me.FlowLayoutPanel3.Location = New System.Drawing.Point(8, 26)
		Me.FlowLayoutPanel3.Margin = New System.Windows.Forms.Padding(4)
		Me.FlowLayoutPanel3.Name = "FlowLayoutPanel3"
		Me.FlowLayoutPanel3.Size = New System.Drawing.Size(1317, 191)
		Me.FlowLayoutPanel3.TabIndex = 0
		'
		'Label2
		'
		Me.Label2.AutoSize = True
		Me.Label2.Location = New System.Drawing.Point(4, 10)
		Me.Label2.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label2.Name = "Label2"
		Me.Label2.Size = New System.Drawing.Size(27, 15)
		Me.Label2.TabIndex = 3
		Me.Label2.Text = "W1"
		'
		'w1_TrackBar
		'
		Me.w1_TrackBar.Location = New System.Drawing.Point(39, 4)
		Me.w1_TrackBar.Margin = New System.Windows.Forms.Padding(4)
		Me.w1_TrackBar.Maximum = 500
		Me.w1_TrackBar.Minimum = 100
		Me.w1_TrackBar.Name = "w1_TrackBar"
		Me.w1_TrackBar.Size = New System.Drawing.Size(1117, 56)
		Me.w1_TrackBar.TabIndex = 4
		Me.w1_TrackBar.Tag = "W1"
		Me.w1_TrackBar.TickStyle = System.Windows.Forms.TickStyle.TopLeft
		Me.w1_TrackBar.Value = 100
		'
		'w1_TextBox
		'
		Me.w1_TextBox.Location = New System.Drawing.Point(1164, 19)
		Me.w1_TextBox.Margin = New System.Windows.Forms.Padding(4, 19, 4, 4)
		Me.w1_TextBox.Name = "w1_TextBox"
		Me.w1_TextBox.Size = New System.Drawing.Size(132, 25)
		Me.w1_TextBox.TabIndex = 5
		Me.w1_TextBox.Tag = "W1"
		'
		'Label3
		'
		Me.Label3.AutoSize = True
		Me.Label3.Location = New System.Drawing.Point(4, 74)
		Me.Label3.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label3.Name = "Label3"
		Me.Label3.Size = New System.Drawing.Size(27, 15)
		Me.Label3.TabIndex = 6
		Me.Label3.Text = "W2"
		'
		'w2_TrackBar
		'
		Me.w2_TrackBar.Location = New System.Drawing.Point(39, 68)
		Me.w2_TrackBar.Margin = New System.Windows.Forms.Padding(4)
		Me.w2_TrackBar.Maximum = 500
		Me.w2_TrackBar.Minimum = 100
		Me.w2_TrackBar.Name = "w2_TrackBar"
		Me.w2_TrackBar.Size = New System.Drawing.Size(1117, 56)
		Me.w2_TrackBar.TabIndex = 7
		Me.w2_TrackBar.Tag = "W2"
		Me.w2_TrackBar.TickStyle = System.Windows.Forms.TickStyle.TopLeft
		Me.w2_TrackBar.Value = 100
		'
		'w2_TextBox
		'
		Me.w2_TextBox.Location = New System.Drawing.Point(1164, 83)
		Me.w2_TextBox.Margin = New System.Windows.Forms.Padding(4, 19, 4, 4)
		Me.w2_TextBox.Name = "w2_TextBox"
		Me.w2_TextBox.Size = New System.Drawing.Size(132, 25)
		Me.w2_TextBox.TabIndex = 8
		Me.w2_TextBox.Tag = "W2"
		'
		'Label4
		'
		Me.Label4.AutoSize = True
		Me.Label4.Location = New System.Drawing.Point(4, 138)
		Me.Label4.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label4.Name = "Label4"
		Me.Label4.Size = New System.Drawing.Size(27, 15)
		Me.Label4.TabIndex = 9
		Me.Label4.Text = "W3"
		'
		'w3_TrackBar
		'
		Me.w3_TrackBar.Location = New System.Drawing.Point(39, 132)
		Me.w3_TrackBar.Margin = New System.Windows.Forms.Padding(4)
		Me.w3_TrackBar.Maximum = 500
		Me.w3_TrackBar.Minimum = 100
		Me.w3_TrackBar.Name = "w3_TrackBar"
		Me.w3_TrackBar.Size = New System.Drawing.Size(1117, 56)
		Me.w3_TrackBar.TabIndex = 10
		Me.w3_TrackBar.Tag = "W3"
		Me.w3_TrackBar.TickStyle = System.Windows.Forms.TickStyle.TopLeft
		Me.w3_TrackBar.Value = 100
		'
		'w3_TextBox
		'
		Me.w3_TextBox.Location = New System.Drawing.Point(1164, 147)
		Me.w3_TextBox.Margin = New System.Windows.Forms.Padding(4, 19, 4, 4)
		Me.w3_TextBox.Name = "w3_TextBox"
		Me.w3_TextBox.Size = New System.Drawing.Size(132, 25)
		Me.w3_TextBox.TabIndex = 11
		Me.w3_TextBox.Tag = "W3"
		'
		'TabPage3
		'
		Me.TabPage3.Controls.Add(Me.MainFlowLayoutPanel4)
		Me.TabPage3.Location = New System.Drawing.Point(4, 39)
		Me.TabPage3.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
		Me.TabPage3.Name = "TabPage3"
		Me.TabPage3.Size = New System.Drawing.Size(1344, 984)
		Me.TabPage3.TabIndex = 2
		Me.TabPage3.Text = "評估結果 - 總水足跡 (WFN)"
		Me.TabPage3.UseVisualStyleBackColor = True
		'
		'MainFlowLayoutPanel4
		'
		Me.MainFlowLayoutPanel4.BackColor = System.Drawing.Color.Cornsilk
		Me.MainFlowLayoutPanel4.Controls.Add(Me.GroupBox13)
		Me.MainFlowLayoutPanel4.Controls.Add(Me.GroupBox14)
		Me.MainFlowLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill
		Me.MainFlowLayoutPanel4.Location = New System.Drawing.Point(0, 0)
		Me.MainFlowLayoutPanel4.Margin = New System.Windows.Forms.Padding(4)
		Me.MainFlowLayoutPanel4.Name = "MainFlowLayoutPanel4"
		Me.MainFlowLayoutPanel4.Size = New System.Drawing.Size(1344, 984)
		Me.MainFlowLayoutPanel4.TabIndex = 0
		'
		'GroupBox13
		'
		Me.GroupBox13.Controls.Add(Me.WFN_DataGridView)
		Me.GroupBox13.Location = New System.Drawing.Point(4, 4)
		Me.GroupBox13.Margin = New System.Windows.Forms.Padding(4)
		Me.GroupBox13.Name = "GroupBox13"
		Me.GroupBox13.Padding = New System.Windows.Forms.Padding(4)
		Me.GroupBox13.Size = New System.Drawing.Size(1321, 388)
		Me.GroupBox13.TabIndex = 2
		Me.GroupBox13.TabStop = False
		Me.GroupBox13.Text = "產品比較分析評估值"
		'
		'WFN_DataGridView
		'
		Me.WFN_DataGridView.AllowUserToAddRows = False
		Me.WFN_DataGridView.AllowUserToDeleteRows = False
		Me.WFN_DataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
		Me.WFN_DataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
		Me.WFN_DataGridView.Dock = System.Windows.Forms.DockStyle.Fill
		Me.WFN_DataGridView.Location = New System.Drawing.Point(4, 22)
		Me.WFN_DataGridView.Margin = New System.Windows.Forms.Padding(4)
		Me.WFN_DataGridView.Name = "WFN_DataGridView"
		Me.WFN_DataGridView.ReadOnly = True
		Me.WFN_DataGridView.RowTemplate.Height = 24
		Me.WFN_DataGridView.Size = New System.Drawing.Size(1313, 362)
		Me.WFN_DataGridView.TabIndex = 0
		'
		'GroupBox14
		'
		Me.GroupBox14.Controls.Add(Me.WFN_Chart)
		Me.GroupBox14.Location = New System.Drawing.Point(4, 400)
		Me.GroupBox14.Margin = New System.Windows.Forms.Padding(4)
		Me.GroupBox14.Name = "GroupBox14"
		Me.GroupBox14.Padding = New System.Windows.Forms.Padding(4)
		Me.GroupBox14.Size = New System.Drawing.Size(1317, 560)
		Me.GroupBox14.TabIndex = 3
		Me.GroupBox14.TabStop = False
		Me.GroupBox14.Text = "分析圖表"
		'
		'WFN_Chart
		'
		ChartArea1.AxisY.TextOrientation = System.Windows.Forms.DataVisualization.Charting.TextOrientation.Horizontal
		ChartArea1.AxisY.Title = "K"
		ChartArea1.AxisY.TitleAlignment = System.Drawing.StringAlignment.Far
		ChartArea1.AxisY.TitleFont = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold)
		ChartArea1.Name = "ChartArea1"
		Me.WFN_Chart.ChartAreas.Add(ChartArea1)
		Me.WFN_Chart.Dock = System.Windows.Forms.DockStyle.Fill
		Legend1.Name = "Legend1"
		Me.WFN_Chart.Legends.Add(Legend1)
		Me.WFN_Chart.Location = New System.Drawing.Point(4, 22)
		Me.WFN_Chart.Margin = New System.Windows.Forms.Padding(4)
		Me.WFN_Chart.Name = "WFN_Chart"
		Series1.ChartArea = "ChartArea1"
		Series1.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.StackedColumn
		Series1.Legend = "Legend1"
		Series1.LegendText = "條件一 : W1*K_WFP_i"
		Series1.Name = "Series1"
		DataPoint1.AxisLabel = "產品A"
		DataPoint1.Label = "20"
		DataPoint2.AxisLabel = "產品B"
		DataPoint2.Label = "33"
		DataPoint3.AxisLabel = "產品 C"
		DataPoint3.Label = "33"
		Series1.Points.Add(DataPoint1)
		Series1.Points.Add(DataPoint2)
		Series1.Points.Add(DataPoint3)
		Series2.ChartArea = "ChartArea1"
		Series2.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.StackedColumn
		Series2.Legend = "Legend1"
		Series2.LegendText = "條件二 : W2*K_Performance_i"
		Series2.Name = "Series2"
		DataPoint4.Label = "12"
		DataPoint5.Label = "24"
		DataPoint6.Label = "33"
		Series2.Points.Add(DataPoint4)
		Series2.Points.Add(DataPoint5)
		Series2.Points.Add(DataPoint6)
		Series3.ChartArea = "ChartArea1"
		Series3.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.StackedColumn
		Series3.Legend = "Legend1"
		Series3.LegendText = "條件三 : W3*K_Importance_i"
		Series3.Name = "Series3"
		DataPoint7.Label = "70"
		DataPoint8.Label = "5"
		DataPoint9.Label = "33"
		Series3.Points.Add(DataPoint7)
		Series3.Points.Add(DataPoint8)
		Series3.Points.Add(DataPoint9)
		Me.WFN_Chart.Series.Add(Series1)
		Me.WFN_Chart.Series.Add(Series2)
		Me.WFN_Chart.Series.Add(Series3)
		Me.WFN_Chart.Size = New System.Drawing.Size(1309, 534)
		Me.WFN_Chart.TabIndex = 1
		Me.WFN_Chart.Text = "Chart2"
		Title1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold)
		Title1.Name = "Title1"
		Title1.Text = "總水足跡 (WFN)"
		Me.WFN_Chart.Titles.Add(Title1)
		'
		'TabPage4
		'
		Me.TabPage4.Controls.Add(Me.FlowLayoutPanel1)
		Me.TabPage4.Location = New System.Drawing.Point(4, 39)
		Me.TabPage4.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
		Me.TabPage4.Name = "TabPage4"
		Me.TabPage4.Size = New System.Drawing.Size(1344, 984)
		Me.TabPage4.TabIndex = 3
		Me.TabPage4.Text = "評估結果 - 總水足跡 (ISO 14046)"
		Me.TabPage4.UseVisualStyleBackColor = True
		'
		'FlowLayoutPanel1
		'
		Me.FlowLayoutPanel1.BackColor = System.Drawing.Color.Violet
		Me.FlowLayoutPanel1.Controls.Add(Me.GroupBox15)
		Me.FlowLayoutPanel1.Controls.Add(Me.GroupBox16)
		Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
		Me.FlowLayoutPanel1.Location = New System.Drawing.Point(0, 0)
		Me.FlowLayoutPanel1.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
		Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
		Me.FlowLayoutPanel1.Size = New System.Drawing.Size(1344, 984)
		Me.FlowLayoutPanel1.TabIndex = 0
		'
		'GroupBox15
		'
		Me.GroupBox15.Controls.Add(Me.ISO_DataGridView)
		Me.GroupBox15.Location = New System.Drawing.Point(4, 4)
		Me.GroupBox15.Margin = New System.Windows.Forms.Padding(4)
		Me.GroupBox15.Name = "GroupBox15"
		Me.GroupBox15.Padding = New System.Windows.Forms.Padding(4)
		Me.GroupBox15.Size = New System.Drawing.Size(1321, 388)
		Me.GroupBox15.TabIndex = 4
		Me.GroupBox15.TabStop = False
		Me.GroupBox15.Text = "產品比較分析評估值"
		'
		'ISO_DataGridView
		'
		Me.ISO_DataGridView.AllowUserToAddRows = False
		Me.ISO_DataGridView.AllowUserToDeleteRows = False
		Me.ISO_DataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
		Me.ISO_DataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
		Me.ISO_DataGridView.Dock = System.Windows.Forms.DockStyle.Fill
		Me.ISO_DataGridView.Location = New System.Drawing.Point(4, 22)
		Me.ISO_DataGridView.Margin = New System.Windows.Forms.Padding(4)
		Me.ISO_DataGridView.Name = "ISO_DataGridView"
		Me.ISO_DataGridView.ReadOnly = True
		Me.ISO_DataGridView.RowTemplate.Height = 24
		Me.ISO_DataGridView.Size = New System.Drawing.Size(1313, 362)
		Me.ISO_DataGridView.TabIndex = 0
		'
		'GroupBox16
		'
		Me.GroupBox16.Controls.Add(Me.ISO_Chart)
		Me.GroupBox16.Location = New System.Drawing.Point(4, 400)
		Me.GroupBox16.Margin = New System.Windows.Forms.Padding(4)
		Me.GroupBox16.Name = "GroupBox16"
		Me.GroupBox16.Padding = New System.Windows.Forms.Padding(4)
		Me.GroupBox16.Size = New System.Drawing.Size(1317, 560)
		Me.GroupBox16.TabIndex = 5
		Me.GroupBox16.TabStop = False
		Me.GroupBox16.Text = "分析圖表"
		'
		'ISO_Chart
		'
		ChartArea2.AxisY.TextOrientation = System.Windows.Forms.DataVisualization.Charting.TextOrientation.Horizontal
		ChartArea2.AxisY.Title = "K"
		ChartArea2.AxisY.TitleAlignment = System.Drawing.StringAlignment.Far
		ChartArea2.AxisY.TitleFont = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold)
		ChartArea2.Name = "ChartArea1"
		Me.ISO_Chart.ChartAreas.Add(ChartArea2)
		Me.ISO_Chart.Dock = System.Windows.Forms.DockStyle.Fill
		Legend2.Name = "Legend1"
		Me.ISO_Chart.Legends.Add(Legend2)
		Me.ISO_Chart.Location = New System.Drawing.Point(4, 22)
		Me.ISO_Chart.Margin = New System.Windows.Forms.Padding(4)
		Me.ISO_Chart.Name = "ISO_Chart"
		Series4.ChartArea = "ChartArea1"
		Series4.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.StackedColumn
		Series4.Legend = "Legend1"
		Series4.LegendText = "條件一 : W1*K_WFP_i"
		Series4.Name = "Series1"
		DataPoint10.AxisLabel = "產品A"
		DataPoint10.Label = "20"
		DataPoint11.AxisLabel = "產品B"
		DataPoint11.Label = "33"
		DataPoint12.AxisLabel = "產品 C"
		DataPoint12.Label = "33"
		Series4.Points.Add(DataPoint10)
		Series4.Points.Add(DataPoint11)
		Series4.Points.Add(DataPoint12)
		Series5.ChartArea = "ChartArea1"
		Series5.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.StackedColumn
		Series5.Legend = "Legend1"
		Series5.LegendText = "條件二 : W2*K_Performance_i"
		Series5.Name = "Series2"
		DataPoint13.Label = "12"
		DataPoint14.Label = "24"
		DataPoint15.Label = "33"
		Series5.Points.Add(DataPoint13)
		Series5.Points.Add(DataPoint14)
		Series5.Points.Add(DataPoint15)
		Series6.ChartArea = "ChartArea1"
		Series6.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.StackedColumn
		Series6.Legend = "Legend1"
		Series6.LegendText = "條件三 : W3*K_Importance_i"
		Series6.Name = "Series3"
		DataPoint16.Label = "70"
		DataPoint17.Label = "5"
		DataPoint18.Label = "33"
		Series6.Points.Add(DataPoint16)
		Series6.Points.Add(DataPoint17)
		Series6.Points.Add(DataPoint18)
		Me.ISO_Chart.Series.Add(Series4)
		Me.ISO_Chart.Series.Add(Series5)
		Me.ISO_Chart.Series.Add(Series6)
		Me.ISO_Chart.Size = New System.Drawing.Size(1309, 534)
		Me.ISO_Chart.TabIndex = 1
		Me.ISO_Chart.Text = "Chart3"
		Title2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold)
		Title2.Name = "Title1"
		Title2.Text = "總水足跡 (ISO 14046)"
		Me.ISO_Chart.Titles.Add(Title2)
		'
		'Main_ProductCompareForm
		'
		Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 15.0!)
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.ClientSize = New System.Drawing.Size(1352, 1079)
		Me.Controls.Add(Me.TabControl1)
		Me.Controls.Add(Me.StatusStrip1)
		Me.Controls.Add(Me.MenuStrip1)
		Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
		Me.MainMenuStrip = Me.MenuStrip1
		Me.Margin = New System.Windows.Forms.Padding(4)
		Me.Name = "Main_ProductCompareForm"
		Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
		Me.Text = "產品比較分析評估"
		Me.TopMost = True
		Me.MenuStrip1.ResumeLayout(False)
		Me.MenuStrip1.PerformLayout()
		Me.StatusStrip1.ResumeLayout(False)
		Me.StatusStrip1.PerformLayout()
		Me.TabControl1.ResumeLayout(False)
		Me.TabPage1.ResumeLayout(False)
		Me.Page1_Main_FlowLayoutPanel.ResumeLayout(False)
		Me.choice_product_compare_GroupBox.ResumeLayout(False)
		CType(Me.choice_product_compare_DataGridView, System.ComponentModel.ISupportInitialize).EndInit()
		Me.FlowLayoutPanel2.ResumeLayout(False)
		Me.product_compare_condition_GroupBox.ResumeLayout(False)
		Me.product_compare_condition_group_main_Panel.ResumeLayout(False)
		Me.GroupBox6.ResumeLayout(False)
		Me.FlowLayoutPanel5.ResumeLayout(False)
		Me.FlowLayoutPanel5.PerformLayout()
		Me.GroupBox4.ResumeLayout(False)
		Me.FlowLayoutPanel11.ResumeLayout(False)
		Me.condition_2_camera_Panel.ResumeLayout(False)
		Me.GroupBox7.ResumeLayout(False)
		Me.FlowLayoutPanel7.ResumeLayout(False)
		Me.FlowLayoutPanel7.PerformLayout()
		Me.GroupBox8.ResumeLayout(False)
		Me.FlowLayoutPanel8.ResumeLayout(False)
		Me.FlowLayoutPanel8.PerformLayout()
		Me.GroupBox9.ResumeLayout(False)
		Me.FlowLayoutPanel9.ResumeLayout(False)
		Me.FlowLayoutPanel9.PerformLayout()
		Me.condition_2_LCD_Panel.ResumeLayout(False)
		Me.GroupBox10.ResumeLayout(False)
		Me.FlowLayoutPanel4.ResumeLayout(False)
		Me.FlowLayoutPanel4.PerformLayout()
		Me.GroupBox11.ResumeLayout(False)
		Me.FlowLayoutPanel12.ResumeLayout(False)
		Me.FlowLayoutPanel12.PerformLayout()
		Me.GroupBox12.ResumeLayout(False)
		Me.FlowLayoutPanel13.ResumeLayout(False)
		Me.FlowLayoutPanel13.PerformLayout()
		Me.FlowLayoutPanel10.ResumeLayout(False)
		Me.FlowLayoutPanel10.PerformLayout()
		Me.GroupBox5.ResumeLayout(False)
		Me.Importance_FlowLayoutPanel.ResumeLayout(False)
		Me.GroupBox3.ResumeLayout(False)
		Me.FlowLayoutPanel3.ResumeLayout(False)
		Me.FlowLayoutPanel3.PerformLayout()
		CType(Me.w1_TrackBar, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.w2_TrackBar, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.w3_TrackBar, System.ComponentModel.ISupportInitialize).EndInit()
		Me.TabPage3.ResumeLayout(False)
		Me.MainFlowLayoutPanel4.ResumeLayout(False)
		Me.GroupBox13.ResumeLayout(False)
		CType(Me.WFN_DataGridView, System.ComponentModel.ISupportInitialize).EndInit()
		Me.GroupBox14.ResumeLayout(False)
		CType(Me.WFN_Chart, System.ComponentModel.ISupportInitialize).EndInit()
		Me.TabPage4.ResumeLayout(False)
		Me.FlowLayoutPanel1.ResumeLayout(False)
		Me.GroupBox15.ResumeLayout(False)
		CType(Me.ISO_DataGridView, System.ComponentModel.ISupportInitialize).EndInit()
		Me.GroupBox16.ResumeLayout(False)
		CType(Me.ISO_Chart, System.ComponentModel.ISupportInitialize).EndInit()
		Me.ResumeLayout(False)
		Me.PerformLayout()

	End Sub

	Friend WithEvents MenuStrip1 As MenuStrip
	Friend WithEvents StatusStrip1 As StatusStrip
	Friend WithEvents ProductComparisonCaption_Label As ToolStripStatusLabel
	Friend WithEvents add_product_compare_ToolStripMenuItem As ToolStripMenuItem
	Friend WithEvents save_product_compare_ToolStripMenuItem As ToolStripMenuItem
	Friend WithEvents load_product_compare_ToolStripMenuItem As ToolStripMenuItem
	Friend WithEvents TabControl1 As TabControl
	Friend WithEvents TabPage1 As TabPage
	Friend WithEvents choice_product_compare_GroupBox As GroupBox

	Friend WithEvents FlowLayoutPanel2 As FlowLayoutPanel
	Friend WithEvents add_Button As Button
	Friend WithEvents clear_Button As Button
	Friend WithEvents delete_Button As Button
	Friend WithEvents product_compare_condition_GroupBox As GroupBox
	Friend WithEvents GroupBox3 As GroupBox
	Friend WithEvents FlowLayoutPanel3 As FlowLayoutPanel
	Friend WithEvents Label2 As Label
	Friend WithEvents w1_TrackBar As TrackBar
	Friend WithEvents w1_TextBox As TextBox
	Friend WithEvents Label3 As Label
	Friend WithEvents w2_TrackBar As TrackBar
	Friend WithEvents w2_TextBox As TextBox
	Friend WithEvents Label4 As Label
	Friend WithEvents w3_TrackBar As TrackBar
	Friend WithEvents w3_TextBox As TextBox
	Friend WithEvents product_compare_condition_group_main_Panel As FlowLayoutPanel
	Friend WithEvents FlowLayoutPanel5 As FlowLayoutPanel
	Friend WithEvents Label5 As Label
	Friend WithEvents productName_TextBox As TextBox
	Friend WithEvents Label6 As Label
	Friend WithEvents WFP_TextBox As TextBox
	Friend WithEvents GroupBox4 As GroupBox
	Friend WithEvents GroupBox5 As GroupBox
	Friend WithEvents GroupBox6 As GroupBox
	Friend WithEvents Importance_FlowLayoutPanel As FlowLayoutPanel
	Friend WithEvents Label7 As Label
	Friend WithEvents importance_radio1 As RadioButton
	Friend WithEvents importance_radio2 As RadioButton
	Friend WithEvents importance_radio3 As RadioButton
	Friend WithEvents importance_radio4 As RadioButton
	Friend WithEvents importance_radio5 As RadioButton
	Friend WithEvents GroupBox9 As GroupBox
	Friend WithEvents FlowLayoutPanel9 As FlowLayoutPanel
	Friend WithEvents p3_camera_radio1 As RadioButton
	Friend WithEvents p3_camera_radio2 As RadioButton
	Friend WithEvents p3_camera_radio3 As RadioButton
	Friend WithEvents p3_camera_radio4 As RadioButton
	Friend WithEvents p3_camera_radio5 As RadioButton
	Friend WithEvents GroupBox8 As GroupBox
	Friend WithEvents FlowLayoutPanel8 As FlowLayoutPanel
	Friend WithEvents p2_camera_radio1 As RadioButton
	Friend WithEvents p2_camera_radio2 As RadioButton
	Friend WithEvents p2_camera_radio3 As RadioButton
	Friend WithEvents p2_camera_radio4 As RadioButton
	Friend WithEvents p2_camera_radio5 As RadioButton
	Friend WithEvents GroupBox7 As GroupBox
	Friend WithEvents FlowLayoutPanel7 As FlowLayoutPanel
	Friend WithEvents p1_camera_radio1 As RadioButton
	Friend WithEvents p1_camera_radio2 As RadioButton
	Friend WithEvents p1_camera_radio3 As RadioButton
	Friend WithEvents p1_camera_radio4 As RadioButton
	Friend WithEvents p1_camera_radio5 As RadioButton
	Friend WithEvents FlowLayoutPanel11 As FlowLayoutPanel
	Friend WithEvents FlowLayoutPanel10 As FlowLayoutPanel
	Friend WithEvents condition_2_camera_RadioButton As RadioButton
	Friend WithEvents condition_2_LCD_RadioButton As RadioButton
	Friend WithEvents condition_2_camera_Panel As FlowLayoutPanel
	Friend WithEvents condition_2_LCD_Panel As FlowLayoutPanel
	Friend WithEvents GroupBox10 As GroupBox
	Friend WithEvents p1_lcd_radio1 As RadioButton
	Friend WithEvents p1_lcd_radio2 As RadioButton
	Friend WithEvents p1_lcd_radio3 As RadioButton
	Friend WithEvents p1_lcd_radio4 As RadioButton
	Friend WithEvents GroupBox11 As GroupBox
	Friend WithEvents p2_lcd_radio1 As RadioButton
	Friend WithEvents p2_lcd_radio2 As RadioButton
	Friend WithEvents p2_lcd_radio3 As RadioButton
	Friend WithEvents p2_lcd_radio4 As RadioButton
	Friend WithEvents GroupBox12 As GroupBox
	Friend WithEvents p3_lcd_radio1 As RadioButton
	Friend WithEvents p3_lcd_radio2 As RadioButton
	Friend WithEvents p3_lcd_radio3 As RadioButton
	Friend WithEvents p3_lcd_radio4 As RadioButton
	Friend WithEvents choice_product_compare_DataGridView As DataGridView
	Friend WithEvents Label1 As Label
	Friend WithEvents ISO_TextBox As TextBox
	Friend WithEvents Page1_Main_FlowLayoutPanel As FlowLayoutPanel
	Friend WithEvents TabPage3 As TabPage
	Friend WithEvents MainFlowLayoutPanel4 As FlowLayoutPanel
	Friend WithEvents GroupBox13 As GroupBox
	Friend WithEvents WFN_DataGridView As DataGridView
	Friend WithEvents GroupBox14 As GroupBox
	Friend WithEvents WFN_Chart As DataVisualization.Charting.Chart
	Friend WithEvents TabPage4 As TabPage
	Friend WithEvents FlowLayoutPanel1 As FlowLayoutPanel
	Friend WithEvents GroupBox15 As GroupBox
	Friend WithEvents ISO_DataGridView As DataGridView
	Friend WithEvents GroupBox16 As GroupBox
	Friend WithEvents ISO_Chart As DataVisualization.Charting.Chart
	Friend WithEvents FlowLayoutPanel4 As FlowLayoutPanel
	Friend WithEvents FlowLayoutPanel12 As FlowLayoutPanel
	Friend WithEvents FlowLayoutPanel13 As FlowLayoutPanel
	Friend WithEvents ComparisonName_Label As ToolStripStatusLabel
End Class
