﻿Imports WFE.Assistants.Controllers.ProductCompare
Imports WFE.Assistants.Helpers
Imports WFE.Assistants.Models.ProductCompare

Public Class Main_ProductCompareForm
	'private
	Dim _mainController As MainProdcuctComparisonController
	Dim _WFNController As ProductComparisonEvaluateController
	Dim _ISOController As ProductComparisonEvaluateController
	Dim _ProductComparisonSN As Integer
	Private Sub ProductCompareMainForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
		'執行時不顯示 Border
		condition_2_LCD_Panel.BorderStyle = BorderStyle.None
		condition_2_camera_Panel.BorderStyle = BorderStyle.None
		product_compare_condition_group_main_Panel.BorderStyle = BorderStyle.None

		'評估結果 Panel 
		'FlowLayoutPanel1.BackColor = Color.Transparent
		'選擇比較產品
		_mainController = New MainProdcuctComparisonController()
		Dim pInfoTextArr = {productName_TextBox, WFP_TextBox, ISO_TextBox}
		Dim c2Arr = {condition_2_camera_RadioButton, condition_2_LCD_RadioButton}
		Dim c2PanelArr = {condition_2_camera_Panel, condition_2_LCD_Panel}
		Dim c3TrackBarArr = {w1_TrackBar, w2_TrackBar, w3_TrackBar}
		Dim c3TextArr = {w1_TextBox, w2_TextBox, w3_TextBox}
		_mainController.Setup(choice_product_compare_DataGridView _
			, pInfoTextArr, c2Arr, c2PanelArr, Importance_FlowLayoutPanel, c3TrackBarArr, c3TextArr)
		'====
		_WFNController = New ProductComparisonEvaluateController()
		_WFNController.Setup(Of WFNEvaluateRecord)(WFN_DataGridView, WFN_Chart, _mainController)
		'==
		_ISOController = New ProductComparisonEvaluateController()
		_ISOController.Setup(Of ISOEvaluateRecord)(ISO_DataGridView, ISO_Chart, _mainController)
		'==
		_ProductComparisonSN = 0
	End Sub
#Region "TabControl 選擇產品"
	Private Sub TabControl1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles TabControl1.SelectedIndexChanged
		'Tabpage Change
		Dim idx = TabControl1.SelectedIndex
		If (idx = 1) Then
			_WFNController.UpdateData()
		ElseIf (idx = 2) Then
			_ISOController.UpdateData()
		End If

	End Sub
	Private Sub add_Button_Click(sender As Object, e As EventArgs) Handles add_Button.Click
		Dim win = New Choice_ProductCompareForm()
		win.StartPosition = FormStartPosition.CenterParent
		Dim chooseProductSNs = _mainController.GetCurrentProductSNs()
		win.SetChooseProduct(chooseProductSNs)
		If win.ShowDialog() = DialogResult.OK Then
			Dim new_productSNs As String = win.GetModel()
			If (new_productSNs = "") Then
				MessageBox.Show("No Product Selected")
			Else
				_mainController.AddProducts(new_productSNs)
			End If
		End If
	End Sub
	Private Sub clear_Button_Click(sender As Object, e As EventArgs) Handles clear_Button.Click
		Dim confrimMsg = "確定刪除所有資料？"
		Dim result As Integer = MessageBox.Show(confrimMsg, "警告！！", MessageBoxButtons.YesNo)
		If result = DialogResult.Yes Then
			Dim success = _mainController.RemoveAllProduct()
		End If
	End Sub

	Private Sub delete_Button_Click(sender As Object, e As EventArgs) Handles delete_Button.Click
		Dim model = _mainController.GetSelectedProduct()
		If model IsNot Nothing Then
			Dim confrimMsg = String.Format("確定刪除「{0}」", model.ProductName)
			Dim result As Integer = MessageBox.Show(confrimMsg, "警告！！", MessageBoxButtons.YesNo)
			If result = DialogResult.Yes Then
				_mainController.RemoveProduct()
			End If
		End If
	End Sub
#End Region
#Region "功能列"
	Private Sub add_product_compare_ToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles add_product_compare_ToolStripMenuItem.Click
		'新增「產品比較分析評估」
		Dim confrimMsg = "確定新增「產品比較分析評估」？" + vbCrLf + "未儲存的資料不會保留"
		Dim result As Integer = MessageBox.Show(confrimMsg, "警告！！", MessageBoxButtons.YesNo)
		If result = DialogResult.Yes Then
			Dim success As Boolean = _mainController.CreateEmpty()
			If (success = True) Then
				_ProductComparisonSN = 0
				ComparisonName_Label.Text = "N/A"
				ComparisonName_Label.ToolTipText = ""
				MessageBox.Show("已建立新的「產品比較分析評估」")
			End If
		End If
	End Sub
	Private Sub load_product_compare_ToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles load_product_compare_ToolStripMenuItem.Click
		'載入「產品比較分析評估」
		Dim win = New Load_ProductCompareForm()
		win.TopMost = True
		win.Setup(_mainController)
		Dim result = win.ShowDialog()
		If result = DialogResult.Yes Then
			_ProductComparisonSN = win.GetCurrentSN()
			updateUI()
		End If
	End Sub
	Private Sub save_product_compare_ToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles save_product_compare_ToolStripMenuItem.Click
		'儲存「產品比較分析評估」
		Dim win = New Save_ProductCompareForm()
		win.TopMost = True
		win.Setup(_mainController, _ProductComparisonSN)
		Dim result = win.ShowDialog()
		If result = DialogResult.Yes Then
			_ProductComparisonSN = win.GetCurrentSN()
			updateUI()
		End If
	End Sub
	Private Sub updateUI()
		Dim model = _mainController.GetProductComparison(_ProductComparisonSN)
		ComparisonName_Label.Text = model.ComparisonName
		ComparisonName_Label.ToolTipText = model.ComparisonDesc
	End Sub
#End Region
End Class