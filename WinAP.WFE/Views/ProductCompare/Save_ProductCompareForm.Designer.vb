﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Save_ProductCompareForm
	Inherits System.Windows.Forms.Form

	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> _
	Protected Overrides Sub Dispose(ByVal disposing As Boolean)
		Try
			If disposing AndAlso components IsNot Nothing Then
				components.Dispose()
			End If
		Finally
			MyBase.Dispose(disposing)
		End Try
	End Sub

	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer

	'NOTE: The following procedure is required by the Windows Form Designer
	'It can be modified using the Windows Form Designer.  
	'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> _
	Private Sub InitializeComponent()
		Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Save_ProductCompareForm))
		Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
		Me.Label1 = New System.Windows.Forms.Label()
		Me.ComparisonName_TextBox = New System.Windows.Forms.TextBox()
		Me.Label3 = New System.Windows.Forms.Label()
		Me.Label2 = New System.Windows.Forms.Label()
		Me.ComparisonDesc_RichTextBox = New System.Windows.Forms.RichTextBox()
		Me.FlowLayoutPanel2 = New System.Windows.Forms.FlowLayoutPanel()
		Me.CancelButton = New System.Windows.Forms.Button()
		Me.SaveButton = New System.Windows.Forms.Button()
		Me.FlowLayoutPanel1.SuspendLayout()
		Me.FlowLayoutPanel2.SuspendLayout()
		Me.SuspendLayout()
		'
		'FlowLayoutPanel1
		'
		Me.FlowLayoutPanel1.Controls.Add(Me.Label1)
		Me.FlowLayoutPanel1.Controls.Add(Me.ComparisonName_TextBox)
		Me.FlowLayoutPanel1.Controls.Add(Me.Label3)
		Me.FlowLayoutPanel1.Controls.Add(Me.Label2)
		Me.FlowLayoutPanel1.Controls.Add(Me.ComparisonDesc_RichTextBox)
		Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top
		Me.FlowLayoutPanel1.Location = New System.Drawing.Point(0, 0)
		Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
		Me.FlowLayoutPanel1.Size = New System.Drawing.Size(647, 199)
		Me.FlowLayoutPanel1.TabIndex = 0
		'
		'Label1
		'
		Me.Label1.AutoSize = True
		Me.Label1.Location = New System.Drawing.Point(3, 8)
		Me.Label1.Margin = New System.Windows.Forms.Padding(3, 8, 3, 0)
		Me.Label1.Name = "Label1"
		Me.Label1.Size = New System.Drawing.Size(134, 12)
		Me.Label1.TabIndex = 0
		Me.Label1.Text = "產品比較分析評估名稱 : "
		'
		'ComparisonName_TextBox
		'
		Me.ComparisonName_TextBox.Location = New System.Drawing.Point(143, 3)
		Me.ComparisonName_TextBox.Name = "ComparisonName_TextBox"
		Me.ComparisonName_TextBox.Size = New System.Drawing.Size(448, 22)
		Me.ComparisonName_TextBox.TabIndex = 1
		'
		'Label3
		'
		Me.Label3.AutoSize = True
		Me.Label3.ForeColor = System.Drawing.Color.Red
		Me.Label3.Location = New System.Drawing.Point(597, 8)
		Me.Label3.Margin = New System.Windows.Forms.Padding(3, 8, 3, 0)
		Me.Label3.Name = "Label3"
		Me.Label3.Size = New System.Drawing.Size(37, 12)
		Me.Label3.TabIndex = 4
		Me.Label3.Text = "(必填)"
		'
		'Label2
		'
		Me.Label2.Location = New System.Drawing.Point(3, 33)
		Me.Label2.Margin = New System.Windows.Forms.Padding(3, 5, 3, 0)
		Me.Label2.Name = "Label2"
		Me.Label2.Size = New System.Drawing.Size(134, 23)
		Me.Label2.TabIndex = 2
		Me.Label2.Text = "描述 : "
		Me.Label2.TextAlign = System.Drawing.ContentAlignment.TopRight
		'
		'ComparisonDesc_RichTextBox
		'
		Me.ComparisonDesc_RichTextBox.Location = New System.Drawing.Point(143, 31)
		Me.ComparisonDesc_RichTextBox.Name = "ComparisonDesc_RichTextBox"
		Me.ComparisonDesc_RichTextBox.Size = New System.Drawing.Size(448, 151)
		Me.ComparisonDesc_RichTextBox.TabIndex = 3
		Me.ComparisonDesc_RichTextBox.Text = ""
		'
		'FlowLayoutPanel2
		'
		Me.FlowLayoutPanel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
		Me.FlowLayoutPanel2.Controls.Add(Me.CancelButton)
		Me.FlowLayoutPanel2.Controls.Add(Me.SaveButton)
		Me.FlowLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill
		Me.FlowLayoutPanel2.Location = New System.Drawing.Point(0, 199)
		Me.FlowLayoutPanel2.Name = "FlowLayoutPanel2"
		Me.FlowLayoutPanel2.Size = New System.Drawing.Size(647, 49)
		Me.FlowLayoutPanel2.TabIndex = 1
		'
		'CancelButton
		'
		Me.CancelButton.ForeColor = System.Drawing.Color.Red
		Me.CancelButton.Location = New System.Drawing.Point(3, 3)
		Me.CancelButton.Name = "CancelButton"
		Me.CancelButton.Size = New System.Drawing.Size(315, 35)
		Me.CancelButton.TabIndex = 0
		Me.CancelButton.Text = "取消"
		Me.CancelButton.UseVisualStyleBackColor = True
		'
		'SaveButton
		'
		Me.SaveButton.ForeColor = System.Drawing.Color.Green
		Me.SaveButton.Location = New System.Drawing.Point(324, 3)
		Me.SaveButton.Name = "SaveButton"
		Me.SaveButton.Size = New System.Drawing.Size(315, 35)
		Me.SaveButton.TabIndex = 1
		Me.SaveButton.Text = "儲存"
		Me.SaveButton.UseVisualStyleBackColor = True
		'
		'Save_ProductCompareForm
		'
		Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.ClientSize = New System.Drawing.Size(647, 248)
		Me.Controls.Add(Me.FlowLayoutPanel2)
		Me.Controls.Add(Me.FlowLayoutPanel1)
		Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
		Me.Name = "Save_ProductCompareForm"
		Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
		Me.Text = "儲存「產品比較分析評估」"
		Me.FlowLayoutPanel1.ResumeLayout(False)
		Me.FlowLayoutPanel1.PerformLayout()
		Me.FlowLayoutPanel2.ResumeLayout(False)
		Me.ResumeLayout(False)

	End Sub

	Friend WithEvents FlowLayoutPanel1 As FlowLayoutPanel
	Friend WithEvents Label1 As Label
	Friend WithEvents ComparisonName_TextBox As TextBox
	Friend WithEvents Label2 As Label
	Friend WithEvents ComparisonDesc_RichTextBox As RichTextBox
	Friend WithEvents FlowLayoutPanel2 As FlowLayoutPanel
	Friend WithEvents CancelButton As Button
	Friend WithEvents SaveButton As Button
	Friend WithEvents Label3 As Label
End Class
