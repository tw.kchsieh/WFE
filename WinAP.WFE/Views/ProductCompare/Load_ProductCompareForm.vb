﻿Imports WFE.Assistants.Controllers.ProductCompare

Public Class Load_ProductCompareForm
	Dim _controller As LoadProductComparisonController
	Dim _mainController As MainProdcuctComparisonController
	Dim _loadSN As Integer
	Private Sub Load_ProductCompareForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
		_controller = New LoadProductComparisonController()
		_controller.Setup(DataGridView1, name_Filter_TextBox, filterInfo_Label, clean_filter_Button, ComparisonName_TextBox, ComparisonDesc_RichTextBox)
	End Sub
	Friend Sub Setup(mainController As MainProdcuctComparisonController)
		_mainController = mainController
	End Sub
	Private Sub LoadButton_Click(sender As Object, e As EventArgs) Handles LoadButton.Click
		'載入按鈕
		Dim success As Boolean = _controller.Load(_mainController, _loadSN)
		If (success = True) Then
			DialogResult = DialogResult.Yes
			Me.Close()
		End If
	End Sub

	Private Sub DeleteButton_Click(sender As Object, e As EventArgs) Handles DeleteButton.Click
		'刪除按鈕
		Dim success = _controller.Delete()
		If (success = True) Then
			MessageBox.Show("刪除成功")
		End If
	End Sub

	Friend Function GetCurrentSN() As Integer
		Return _loadSN
	End Function
End Class