﻿Imports WFE.Assistants.Controllers.ProductCompare
Imports WFE.Assistants.Models.ProductCompare

Public Class Save_ProductCompareForm
	Dim _mainController As MainProdcuctComparisonController
	Dim _saveController As SaveComparisonController
	Dim _comparisonSN As Integer
	Dim _actionText As String
	Private Sub Save_ProductCompareForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
		_saveController = New SaveComparisonController()
		_saveController.Setup(ComparisonName_TextBox, ComparisonDesc_RichTextBox, _comparisonSN)
		If (_comparisonSN > 0) Then
			_actionText = "更新"
		Else
			_actionText = "儲存"
		End If
		SaveButton.Text = _actionText
	End Sub
	Friend Sub Setup(mainController As MainProdcuctComparisonController, comparisonSN As Integer)
		_mainController = mainController
		_comparisonSN = comparisonSN
	End Sub
	Private Sub CancelButton_Click(sender As Object, e As EventArgs) Handles CancelButton.Click
		DialogResult = DialogResult.Cancel
		Me.Close()
	End Sub

	Private Sub SaveButton_Click(sender As Object, e As EventArgs) Handles SaveButton.Click
		'Save 
		If (ComparisonName_TextBox.Text.Trim() = "") Then
			MessageBox.Show("請輸入名稱！！")
			ComparisonName_TextBox.Focus()
			Return
		End If

		Dim confrimMsg = String.Format("確定 {0}「產品比較分析評估」？", _actionText)
		Dim result As Integer = MessageBox.Show(confrimMsg, "警告！！", MessageBoxButtons.YesNo)
		If result = DialogResult.Yes Then
			Dim msg As String = ""
			Dim success As Boolean
			Dim model = New ProductComparison()
			model.ComparisonName = ComparisonName_TextBox.Text.Trim()
			model.ComparisonDesc = ComparisonDesc_RichTextBox.Text.Trim()
			model.SN = _comparisonSN
			_comparisonSN = _mainController.SaveProductComparison(model, msg, success)
			DialogResult = DialogResult.Yes
			If (success = False) Then
				MessageBox.Show("儲存失敗 : " + msg)
			Else
				Me.Close()
			End If
		End If
	End Sub

	Friend Function GetCurrentSN() As Integer
		Return _comparisonSN
	End Function
End Class