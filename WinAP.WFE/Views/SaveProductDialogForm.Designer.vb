﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class SaveProductDialogForm
	Inherits System.Windows.Forms.Form

	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> _
	Protected Overrides Sub Dispose(ByVal disposing As Boolean)
		Try
			If disposing AndAlso components IsNot Nothing Then
				components.Dispose()
			End If
		Finally
			MyBase.Dispose(disposing)
		End Try
	End Sub

	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer

	'NOTE: The following procedure is required by the Windows Form Designer
	'It can be modified using the Windows Form Designer.  
	'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> _
	Private Sub InitializeComponent()
		Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(SaveProductDialogForm))
		Me.Label1 = New System.Windows.Forms.Label()
		Me.ProductName_TextBox = New System.Windows.Forms.TextBox()
		Me.Cancel_Button = New System.Windows.Forms.Button()
		Me.Save_Button = New System.Windows.Forms.Button()
		Me.Label2 = New System.Windows.Forms.Label()
		Me.Product_Desc_RichTextBox = New System.Windows.Forms.RichTextBox()
		Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
		Me.FlowLayoutPanel2 = New System.Windows.Forms.FlowLayoutPanel()
		Me.FlowLayoutPanel1.SuspendLayout()
		Me.FlowLayoutPanel2.SuspendLayout()
		Me.SuspendLayout()
		'
		'Label1
		'
		Me.Label1.AutoSize = True
		Me.Label1.Font = New System.Drawing.Font("PMingLiU", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
		Me.Label1.Location = New System.Drawing.Point(4, 8)
		Me.Label1.Margin = New System.Windows.Forms.Padding(4, 8, 4, 0)
		Me.Label1.Name = "Label1"
		Me.Label1.Size = New System.Drawing.Size(71, 15)
		Me.Label1.TabIndex = 0
		Me.Label1.Text = "產品名稱"
		'
		'ProductName_TextBox
		'
		Me.ProductName_TextBox.Location = New System.Drawing.Point(83, 4)
		Me.ProductName_TextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.ProductName_TextBox.Name = "ProductName_TextBox"
		Me.ProductName_TextBox.Size = New System.Drawing.Size(493, 25)
		Me.ProductName_TextBox.TabIndex = 1
		'
		'Cancel_Button
		'
		Me.Cancel_Button.BackColor = System.Drawing.Color.DarkOrange
		Me.Cancel_Button.ForeColor = System.Drawing.Color.Black
		Me.Cancel_Button.Location = New System.Drawing.Point(380, 4)
		Me.Cancel_Button.Margin = New System.Windows.Forms.Padding(4)
		Me.Cancel_Button.Name = "Cancel_Button"
		Me.Cancel_Button.Size = New System.Drawing.Size(100, 38)
		Me.Cancel_Button.TabIndex = 4
		Me.Cancel_Button.Text = "取消"
		Me.Cancel_Button.UseVisualStyleBackColor = False
		'
		'Save_Button
		'
		Me.Save_Button.BackColor = System.Drawing.Color.DarkGreen
		Me.Save_Button.ForeColor = System.Drawing.Color.White
		Me.Save_Button.Location = New System.Drawing.Point(488, 4)
		Me.Save_Button.Margin = New System.Windows.Forms.Padding(4)
		Me.Save_Button.Name = "Save_Button"
		Me.Save_Button.Size = New System.Drawing.Size(100, 38)
		Me.Save_Button.TabIndex = 3
		Me.Save_Button.Text = "儲存"
		Me.Save_Button.UseVisualStyleBackColor = False
		'
		'Label2
		'
		Me.Label2.AutoSize = True
		Me.Label2.Font = New System.Drawing.Font("PMingLiU", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
		Me.Label2.Location = New System.Drawing.Point(4, 41)
		Me.Label2.Margin = New System.Windows.Forms.Padding(4, 8, 4, 0)
		Me.Label2.Name = "Label2"
		Me.Label2.Size = New System.Drawing.Size(71, 15)
		Me.Label2.TabIndex = 4
		Me.Label2.Text = "產品描述"
		'
		'Product_Desc_RichTextBox
		'
		Me.Product_Desc_RichTextBox.Location = New System.Drawing.Point(83, 37)
		Me.Product_Desc_RichTextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.Product_Desc_RichTextBox.Name = "Product_Desc_RichTextBox"
		Me.Product_Desc_RichTextBox.Size = New System.Drawing.Size(492, 119)
		Me.Product_Desc_RichTextBox.TabIndex = 2
		Me.Product_Desc_RichTextBox.Text = ""
		'
		'FlowLayoutPanel1
		'
		Me.FlowLayoutPanel1.Controls.Add(Me.Label1)
		Me.FlowLayoutPanel1.Controls.Add(Me.ProductName_TextBox)
		Me.FlowLayoutPanel1.Controls.Add(Me.Label2)
		Me.FlowLayoutPanel1.Controls.Add(Me.Product_Desc_RichTextBox)
		Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
		Me.FlowLayoutPanel1.Location = New System.Drawing.Point(0, 0)
		Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
		Me.FlowLayoutPanel1.Size = New System.Drawing.Size(592, 215)
		Me.FlowLayoutPanel1.TabIndex = 5
		'
		'FlowLayoutPanel2
		'
		Me.FlowLayoutPanel2.Controls.Add(Me.Save_Button)
		Me.FlowLayoutPanel2.Controls.Add(Me.Cancel_Button)
		Me.FlowLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Bottom
		Me.FlowLayoutPanel2.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft
		Me.FlowLayoutPanel2.Location = New System.Drawing.Point(0, 168)
		Me.FlowLayoutPanel2.Name = "FlowLayoutPanel2"
		Me.FlowLayoutPanel2.Size = New System.Drawing.Size(592, 47)
		Me.FlowLayoutPanel2.TabIndex = 6
		'
		'SaveProductDialogForm
		'
		Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 15.0!)
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.ClientSize = New System.Drawing.Size(592, 215)
		Me.Controls.Add(Me.FlowLayoutPanel2)
		Me.Controls.Add(Me.FlowLayoutPanel1)
		Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
		Me.Margin = New System.Windows.Forms.Padding(4)
		Me.Name = "SaveProductDialogForm"
		Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
		Me.Text = "儲存產品"
		Me.TopMost = True
		Me.FlowLayoutPanel1.ResumeLayout(False)
		Me.FlowLayoutPanel1.PerformLayout()
		Me.FlowLayoutPanel2.ResumeLayout(False)
		Me.ResumeLayout(False)

	End Sub

	Friend WithEvents Label1 As Label
	Friend WithEvents ProductName_TextBox As TextBox
	Friend WithEvents Cancel_Button As Button
	Friend WithEvents Save_Button As Button
	Friend WithEvents Label2 As Label
	Friend WithEvents Product_Desc_RichTextBox As RichTextBox
	Friend WithEvents FlowLayoutPanel1 As FlowLayoutPanel
	Friend WithEvents FlowLayoutPanel2 As FlowLayoutPanel
End Class
