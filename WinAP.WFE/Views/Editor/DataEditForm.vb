﻿Imports WFE.Assistants.Controllers.DataEditor
Imports WFE.Assistants.Models.DataEditor
Imports WFE.Assistants.Models.EventArgs

Public Class DataEditForm
	Friend Sub UseTabPage(v As String)
		Select Case v
			Case "material"
				TabControl1.SelectedIndex = 0
			Case "transport"
				TabControl1.SelectedIndex = 1
			Case "makeProcess"
				TabControl1.SelectedIndex = 2
			Case "electronic"
				TabControl1.SelectedIndex = 3
			Case "board"
				TabControl1.SelectedIndex = 4
			Case "lcd"
				TabControl1.SelectedIndex = 5
			Case "assembly"
				TabControl1.SelectedIndex = 6
		End Select
	End Sub
	Private Sub b_soft_RadioButton_CheckedChanged(sender As Object, e As EventArgs) Handles b_soft_RadioButton.CheckedChanged
		b_ProcessTypeCode_TextBox.Text = "M-Soft"
	End Sub

	Private Sub b_hard_RadioButton_CheckedChanged(sender As Object, e As EventArgs) Handles b_hard_RadioButton.CheckedChanged
		b_ProcessTypeCode_TextBox.Text = "M-Hard"
	End Sub

	Private Sub DataEditForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
		Initial_Material_TabPage()
		Initial_Transport_TabPage()
		Initial_MakeProcess_TabPage()
		Initial_Electronic_MakeProcess_TabPage()
		Initial_Board_MakeProcess_TabPage()
		Initial_LCD_MakeProcess_TabPage()
		Initial_AssemblyMethod_TabPage()
	End Sub
#Region "Material - 材料"
	Private Sub Initial_Material_TabPage()
		Dim _materialController = New BaseEditorController()

		Dim m_inputs As TextBox() = {
			m_MaterialName_TextBox,
			m_Density_TextBox,
			m_WFP_TextBox,
			m_ISO_TextBox,
			m_BOD_TextBox,
			m_BlueWater_TextBox,
			m_GreenWater_TextBox,
			m_GrayWater_TextBox,
			m_DataSource_TextBox
		}
		Dim m_buttons As Button() = {
			m_Insert_Button,
			m_Update_Button,
			m_Delete_Button
		}
		Dim m_required As String() = {
			"MaterialName", "Density", "WFP"
		}
		_materialController.Setup(m_inputs, m_buttons, m_Clean_Button, m_FilterInfo_Label, m_NameFilter_TextBox, "Material", "SN", "MaterialName")
		_materialController.SetupDataGridView(Of Material)(m_DataGridView)
		_materialController.Setup_RequiredColumnNames(m_required)
	End Sub
#End Region

#Region "Transport - 運輸工具"
	Private Sub Initial_Transport_TabPage()

		'assign
		Dim transportController = New BaseEditorController()
		Dim tableName = "Transport"
		Dim filterColumnName = "TransportName"

		Dim t_inputs As TextBox() = {
			t_TransportName_TextBox,
			t_CalculateUnit_TextBox,
			t_MaxLoad_TextBox,
			t_WFP_TextBox,
			t_ISO_TextBox,
			t_BOD_TextBox,
			t_BlueWater_TextBox,
			t_GreenWater_TextBox,
			t_GrayWater_TextBox,
			t_Unit_TextBox,
			t_EnergyPerUnit_TextBox,
			t_DataSource_TextBox
		}
		Dim t_buttons As Button() = {
			t_Insert_Button,
			t_Update_Button,
			t_Delete_Button
		}
		Dim t_required As String() = {
			"TransportName", "WFP"
		}

		'setup
		transportController.Setup(t_inputs, t_buttons, t_Clean_Button, t_FilterInfo_Label, t_NameFilter_TextBox, tableName, "SN", filterColumnName)
		transportController.SetupDataGridView(Of Transport)(t_DataGridView)
		transportController.Setup_RequiredColumnNames(t_required)
	End Sub
#End Region

#Region "MakeProcess - 加工方式"
	Private Sub Initial_MakeProcess_TabPage()

		'assign
		Dim makeProcessController = New BaseEditorController()
		Dim tableName = "MakeProcess"
		Dim filterColumnName = tableName + "Name"

		Dim p_inputs As TextBox() = {
			p_MakeProcessName_TextBox,
			p_CalculateCode_TextBox,
			p_CalculateUnit_TextBox,
			p_WFP_TextBox,
			p_ISO_TextBox,
			p_BOD_TextBox,
			p_BlueWater_TextBox,
			p_GreenWater_TextBox,
			p_GrayWater_TextBox,
			p_MachineName_TextBox,
			p_EnergyPerUnit_TextBox,
			p_PowerConsumption_TextBox,
			p_PowerCoefficient_TextBox,
			p_DataSource_TextBox
		}
		Dim p_buttons As Button() = {
			p_Insert_Button,
			p_Update_Button,
			p_Delete_Button
		}
		Dim p_required As String() = {
			filterColumnName, "CalculateCode"
		}

		'setup
		makeProcessController.Setup(p_inputs, p_buttons,
									p_Clean_Button, p_FilterInfo_Label, p_NameFilter_TextBox,
									tableName, "SN", filterColumnName)
		makeProcessController.SetupDataGridView(Of MakeProcess)(p_DataGridView)
		makeProcessController.Setup_RequiredColumnNames(p_required)

		'addEvent
		AddHandler makeProcessController.RefreshInputsCompleted, AddressOf Refresh_MakeProcess_Inputs_Completed

	End Sub
	Private Sub Refresh_MakeProcess_Inputs_Completed(sender As Object, e As RefreshInputEventArgs)

		If (e IsNot Nothing) Then
			Dim dict = e.RequiredInputDict
			Dim value = dict("p_CalculateCode_TextBox")
			If value.Contains("Power") Then
				p_CalculateCode_ComboBox.SelectedIndex = 0
			Else
				p_CalculateCode_ComboBox.SelectedIndex = 1
			End If
		End If
	End Sub

	Private Sub p_CalculateCode_ComboBox_SelectedIndexChanged(sender As Object, e As EventArgs) Handles p_CalculateCode_ComboBox.SelectedIndexChanged
		Dim text = p_CalculateCode_ComboBox.SelectedItem.ToString()
		If (text.Contains("Power")) Then
			p_CalculateCode_TextBox.Text = "Power"
		Else
			p_CalculateCode_TextBox.Text = "WFP"
		End If
	End Sub
#End Region

#Region "電子元件製程"
	Private Sub Initial_Electronic_MakeProcess_TabPage()

		'assign
		Dim eProcessController = New BaseEditorController()
		Dim tableName = "ManufactureProcedure"
		Dim filterColumnName = "ProcessName"

		Dim e_inputs As TextBox() = {
			e_ProcessName_TextBox,
			e_Density_TextBox,
			e_UnitCode_TextBox,
			e_WFP_TextBox,
			e_ISO_TextBox,
			e_BOD_TextBox,
			e_BlueWater_TextBox,
			e_GreenWater_TextBox,
			e_GrayWater_TextBox,
			e_DataSource_TextBox
		}
		Dim e_buttons As Button() = {
			e_Insert_Button,
			e_Update_Button,
			e_Delete_Button
		}
		Dim e_required As String() = {
			filterColumnName
		}

		'setup
		eProcessController.Setup(e_inputs, e_buttons,
									e_Clean_Button, e_FilterInfo_Label, e_NameFilter_TextBox,
									tableName, "SN", filterColumnName)
		eProcessController.Setup_Data_Condition("ProcessTypeCode", "E")
		eProcessController.SetupDataGridView(Of ManufactureProcedure)(e_DataGridView)
		eProcessController.Setup_RequiredColumnNames(e_required)
	End Sub
#End Region

#Region "電路版製程"
	Private Sub Initial_Board_MakeProcess_TabPage()

		'assign
		Dim bProcessController = New BaseEditorController()
		Dim tableName = "ManufactureProcedure"
		Dim filterColumnName = "ProcessName"

		Dim b_inputs As TextBox() = {
			b_ProcessName_TextBox,
			b_ProcessTypeCode_TextBox,
			b_Density_TextBox,
			b_UnitCode_TextBox,
			b_WFP_TextBox,
			b_ISO_TextBox,
			b_BOD_TextBox,
			b_BlueWater_TextBox,
			b_GreenWater_TextBox,
			b_GrayWater_TextBox,
			b_DataSource_TextBox
		}
		Dim b_buttons As Button() = {
			b_Insert_Button,
			b_Update_Button,
			b_Delete_Button
		}
		Dim b_required As String() = {
			filterColumnName, "ProcessTypeCode"
		}

		'setup
		bProcessController.Setup(b_inputs, b_buttons,
									b_Clean_Button, b_FilterInfo_Label, b_NameFilter_TextBox,
									tableName, "SN", filterColumnName)
		bProcessController.Setup_Data_Condition("ProcessTypeCode", "M")
		bProcessController.SetupDataGridView(Of ManufactureProcedure)(b_DataGridView)
		bProcessController.Setup_RequiredColumnNames(b_required)
		AddHandler bProcessController.RefreshInputsCompleted, AddressOf RefreshBoardInputs_Completed

	End Sub
	Private Sub RefreshBoardInputs_Completed(sender As Object, e As RefreshInputEventArgs)
		'更新 Radio Button select
		If (e IsNot Nothing) Then
			Dim dict = e.RequiredInputDict
			Dim value = dict("b_ProcessTypeCode_TextBox")
			'select Radio button
			If value = "M-Soft" Then
				b_soft_RadioButton.Checked = True
			Else
				b_hard_RadioButton.Checked = True
			End If
		End If
	End Sub
#End Region

#Region "LCD 製程"
	Private Sub Initial_LCD_MakeProcess_TabPage()

		'assign
		Dim dProcessController = New BaseEditorController()
		Dim tableName = "ManufactureProcedure"
		Dim filterColumnName = "ProcessName"

		Dim d_inputs As TextBox() = {
			d_ProcessName_TextBox,
			d_Density_TextBox,
			d_UnitCode_TextBox,
			d_WFP_TextBox,
			d_ISO_TextBox,
			d_BOD_TextBox,
			d_BlueWater_TextBox,
			d_GreenWater_TextBox,
			d_GrayWater_TextBox,
			d_DataSource_TextBox
		}
		Dim d_buttons As Button() = {
			d_Insert_Button,
			d_Update_Button,
			d_Delete_Button
		}
		Dim d_required As String() = {
			filterColumnName
		}

		'setup
		dProcessController.Setup(d_inputs, d_buttons,
									d_Clean_Button, d_FilterInfo_Label, d_NameFilter_TextBox,
									tableName, "SN", filterColumnName)
		dProcessController.Setup_Data_Condition("ProcessTypeCode", "LCD")
		dProcessController.SetupDataGridView(Of ManufactureProcedure)(d_DataGridView)
		dProcessController.Setup_RequiredColumnNames(d_required)

	End Sub
#End Region

#Region "AssemblyMethod - 組裝方式"
	Private Sub Initial_AssemblyMethod_TabPage()

		'assign
		Dim controller = New BaseEditorController()
		Dim tableName = "AssemblyMethod"
		Dim filterColumnName = tableName + "Name"

		Dim asm_inputs As TextBox() = {
			asm_AssemblyMethodName_TextBox,
			asm_CalculateUnit_TextBox,
			asm_PowerConsumption_TextBox,
			asm_PowerCoefficient_TextBox,
			asm_DataSource_TextBox,
			asm_Remark_TextBox
		}
		Dim asm_buttons As Button() = {
			asm_Insert_Button,
			asm_Update_Button,
			asm_Delete_Button
		}
		Dim asm_required As String() = {
			filterColumnName
		}

		'setup
		controller.Setup(asm_inputs, asm_buttons,
									asm_Clean_Button, asm_FilterInfo_Label, asm_NameFilter_TextBox,
									tableName, "SN", filterColumnName)
		controller.SetupDataGridView(Of AssemblyMethod)(asm_DataGridView)
		controller.Setup_RequiredColumnNames(asm_required)
	End Sub



#End Region

End Class