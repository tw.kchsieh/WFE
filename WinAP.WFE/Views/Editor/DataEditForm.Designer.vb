﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class DataEditForm
	Inherits System.Windows.Forms.Form

	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> _
	Protected Overrides Sub Dispose(ByVal disposing As Boolean)
		Try
			If disposing AndAlso components IsNot Nothing Then
				components.Dispose()
			End If
		Finally
			MyBase.Dispose(disposing)
		End Try
	End Sub

	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer

	'NOTE: The following procedure is required by the Windows Form Designer
	'It can be modified using the Windows Form Designer.  
	'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> _
	Private Sub InitializeComponent()
		Me.components = New System.ComponentModel.Container()
		Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(DataEditForm))
		Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
		Me.ToolStripStatusLabel1 = New System.Windows.Forms.ToolStripStatusLabel()
		Me.TabControl1 = New System.Windows.Forms.TabControl()
		Me.TabPage1 = New System.Windows.Forms.TabPage()
		Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
		Me.Label1 = New System.Windows.Forms.Label()
		Me.m_MaterialName_TextBox = New System.Windows.Forms.TextBox()
		Me.Label2 = New System.Windows.Forms.Label()
		Me.m_Density_TextBox = New System.Windows.Forms.TextBox()
		Me.Label4 = New System.Windows.Forms.Label()
		Me.m_WFP_TextBox = New System.Windows.Forms.TextBox()
		Me.Label59 = New System.Windows.Forms.Label()
		Me.m_ISO_TextBox = New System.Windows.Forms.TextBox()
		Me.Label65 = New System.Windows.Forms.Label()
		Me.m_BOD_TextBox = New System.Windows.Forms.TextBox()
		Me.Label6 = New System.Windows.Forms.Label()
		Me.m_BlueWater_TextBox = New System.Windows.Forms.TextBox()
		Me.Label8 = New System.Windows.Forms.Label()
		Me.m_GreenWater_TextBox = New System.Windows.Forms.TextBox()
		Me.Label10 = New System.Windows.Forms.Label()
		Me.m_GrayWater_TextBox = New System.Windows.Forms.TextBox()
		Me.Label12 = New System.Windows.Forms.Label()
		Me.m_DataSource_TextBox = New System.Windows.Forms.TextBox()
		Me.FlowLayoutPanel2 = New System.Windows.Forms.FlowLayoutPanel()
		Me.m_Insert_Button = New System.Windows.Forms.Button()
		Me.m_Update_Button = New System.Windows.Forms.Button()
		Me.m_Delete_Button = New System.Windows.Forms.Button()
		Me.Panel1 = New System.Windows.Forms.Panel()
		Me.m_DataGridView = New System.Windows.Forms.DataGridView()
		Me.FlowLayoutPanel3 = New System.Windows.Forms.FlowLayoutPanel()
		Me.Label14 = New System.Windows.Forms.Label()
		Me.m_NameFilter_TextBox = New System.Windows.Forms.TextBox()
		Me.m_Clean_Button = New System.Windows.Forms.Button()
		Me.Label57 = New System.Windows.Forms.Label()
		Me.m_FilterInfo_Label = New System.Windows.Forms.Label()
		Me.TabPage2 = New System.Windows.Forms.TabPage()
		Me.FlowLayoutPanel5 = New System.Windows.Forms.FlowLayoutPanel()
		Me.Label15 = New System.Windows.Forms.Label()
		Me.t_TransportName_TextBox = New System.Windows.Forms.TextBox()
		Me.Label16 = New System.Windows.Forms.Label()
		Me.t_CalculateUnit_TextBox = New System.Windows.Forms.TextBox()
		Me.Label29 = New System.Windows.Forms.Label()
		Me.t_MaxLoad_TextBox = New System.Windows.Forms.TextBox()
		Me.Label18 = New System.Windows.Forms.Label()
		Me.t_WFP_TextBox = New System.Windows.Forms.TextBox()
		Me.Label69 = New System.Windows.Forms.Label()
		Me.t_ISO_TextBox = New System.Windows.Forms.TextBox()
		Me.Label70 = New System.Windows.Forms.Label()
		Me.t_BOD_TextBox = New System.Windows.Forms.TextBox()
		Me.Label20 = New System.Windows.Forms.Label()
		Me.t_BlueWater_TextBox = New System.Windows.Forms.TextBox()
		Me.Label22 = New System.Windows.Forms.Label()
		Me.t_GreenWater_TextBox = New System.Windows.Forms.TextBox()
		Me.Label24 = New System.Windows.Forms.Label()
		Me.t_GrayWater_TextBox = New System.Windows.Forms.TextBox()
		Me.Label17 = New System.Windows.Forms.Label()
		Me.t_Unit_TextBox = New System.Windows.Forms.TextBox()
		Me.Label31 = New System.Windows.Forms.Label()
		Me.t_EnergyPerUnit_TextBox = New System.Windows.Forms.TextBox()
		Me.Label26 = New System.Windows.Forms.Label()
		Me.t_DataSource_TextBox = New System.Windows.Forms.TextBox()
		Me.FlowLayoutPanel6 = New System.Windows.Forms.FlowLayoutPanel()
		Me.t_Insert_Button = New System.Windows.Forms.Button()
		Me.t_Update_Button = New System.Windows.Forms.Button()
		Me.t_Delete_Button = New System.Windows.Forms.Button()
		Me.Panel2 = New System.Windows.Forms.Panel()
		Me.t_DataGridView = New System.Windows.Forms.DataGridView()
		Me.FlowLayoutPanel8 = New System.Windows.Forms.FlowLayoutPanel()
		Me.Label28 = New System.Windows.Forms.Label()
		Me.t_NameFilter_TextBox = New System.Windows.Forms.TextBox()
		Me.t_Clean_Button = New System.Windows.Forms.Button()
		Me.Label56 = New System.Windows.Forms.Label()
		Me.t_FilterInfo_Label = New System.Windows.Forms.Label()
		Me.TabPage3 = New System.Windows.Forms.TabPage()
		Me.FlowLayoutPanel9 = New System.Windows.Forms.FlowLayoutPanel()
		Me.Label27 = New System.Windows.Forms.Label()
		Me.p_MakeProcessName_TextBox = New System.Windows.Forms.TextBox()
		Me.Label32 = New System.Windows.Forms.Label()
		Me.p_CalculateCode_TextBox = New System.Windows.Forms.TextBox()
		Me.p_CalculateCode_ComboBox = New System.Windows.Forms.ComboBox()
		Me.Label66 = New System.Windows.Forms.Label()
		Me.p_CalculateUnit_TextBox = New System.Windows.Forms.TextBox()
		Me.Label33 = New System.Windows.Forms.Label()
		Me.p_MachineName_TextBox = New System.Windows.Forms.TextBox()
		Me.Label35 = New System.Windows.Forms.Label()
		Me.p_WFP_TextBox = New System.Windows.Forms.TextBox()
		Me.Label71 = New System.Windows.Forms.Label()
		Me.p_ISO_TextBox = New System.Windows.Forms.TextBox()
		Me.Label72 = New System.Windows.Forms.Label()
		Me.p_BOD_TextBox = New System.Windows.Forms.TextBox()
		Me.p_BlueWater_TextBox = New System.Windows.Forms.TextBox()
		Me.p_GreenWater_TextBox = New System.Windows.Forms.TextBox()
		Me.p_GrayWater_TextBox = New System.Windows.Forms.TextBox()
		Me.Label44 = New System.Windows.Forms.Label()
		Me.p_EnergyPerUnit_TextBox = New System.Windows.Forms.TextBox()
		Me.Label43 = New System.Windows.Forms.Label()
		Me.p_PowerConsumption_TextBox = New System.Windows.Forms.TextBox()
		Me.Label45 = New System.Windows.Forms.Label()
		Me.p_PowerCoefficient_TextBox = New System.Windows.Forms.TextBox()
		Me.Label34 = New System.Windows.Forms.Label()
		Me.p_DataSource_TextBox = New System.Windows.Forms.TextBox()
		Me.FlowLayoutPanel10 = New System.Windows.Forms.FlowLayoutPanel()
		Me.p_Insert_Button = New System.Windows.Forms.Button()
		Me.p_Update_Button = New System.Windows.Forms.Button()
		Me.p_Delete_Button = New System.Windows.Forms.Button()
		Me.Panel3 = New System.Windows.Forms.Panel()
		Me.p_DataGridView = New System.Windows.Forms.DataGridView()
		Me.FlowLayoutPanel12 = New System.Windows.Forms.FlowLayoutPanel()
		Me.Label46 = New System.Windows.Forms.Label()
		Me.p_NameFilter_TextBox = New System.Windows.Forms.TextBox()
		Me.p_Clean_Button = New System.Windows.Forms.Button()
		Me.Label58 = New System.Windows.Forms.Label()
		Me.p_FilterInfo_Label = New System.Windows.Forms.Label()
		Me.TabPage4 = New System.Windows.Forms.TabPage()
		Me.FlowLayoutPanel13 = New System.Windows.Forms.FlowLayoutPanel()
		Me.Label3 = New System.Windows.Forms.Label()
		Me.e_ProcessName_TextBox = New System.Windows.Forms.TextBox()
		Me.Label67 = New System.Windows.Forms.Label()
		Me.e_ProcessTypeCode_TextBox = New System.Windows.Forms.TextBox()
		Me.Label5 = New System.Windows.Forms.Label()
		Me.e_Density_TextBox = New System.Windows.Forms.TextBox()
		Me.Label68 = New System.Windows.Forms.Label()
		Me.e_UnitCode_TextBox = New System.Windows.Forms.TextBox()
		Me.Label9 = New System.Windows.Forms.Label()
		Me.e_WFP_TextBox = New System.Windows.Forms.TextBox()
		Me.Label73 = New System.Windows.Forms.Label()
		Me.e_ISO_TextBox = New System.Windows.Forms.TextBox()
		Me.Label74 = New System.Windows.Forms.Label()
		Me.e_BOD_TextBox = New System.Windows.Forms.TextBox()
		Me.Label11 = New System.Windows.Forms.Label()
		Me.e_BlueWater_TextBox = New System.Windows.Forms.TextBox()
		Me.Label13 = New System.Windows.Forms.Label()
		Me.e_GreenWater_TextBox = New System.Windows.Forms.TextBox()
		Me.Label19 = New System.Windows.Forms.Label()
		Me.e_GrayWater_TextBox = New System.Windows.Forms.TextBox()
		Me.Label30 = New System.Windows.Forms.Label()
		Me.e_DataSource_TextBox = New System.Windows.Forms.TextBox()
		Me.FlowLayoutPanel14 = New System.Windows.Forms.FlowLayoutPanel()
		Me.e_Insert_Button = New System.Windows.Forms.Button()
		Me.e_Update_Button = New System.Windows.Forms.Button()
		Me.e_Delete_Button = New System.Windows.Forms.Button()
		Me.Panel4 = New System.Windows.Forms.Panel()
		Me.e_DataGridView = New System.Windows.Forms.DataGridView()
		Me.FlowLayoutPanel16 = New System.Windows.Forms.FlowLayoutPanel()
		Me.Label36 = New System.Windows.Forms.Label()
		Me.e_NameFilter_TextBox = New System.Windows.Forms.TextBox()
		Me.e_Clean_Button = New System.Windows.Forms.Button()
		Me.Label60 = New System.Windows.Forms.Label()
		Me.e_FilterInfo_Label = New System.Windows.Forms.Label()
		Me.TabPage5 = New System.Windows.Forms.TabPage()
		Me.FlowLayoutPanel17 = New System.Windows.Forms.FlowLayoutPanel()
		Me.Label7 = New System.Windows.Forms.Label()
		Me.b_ProcessName_TextBox = New System.Windows.Forms.TextBox()
		Me.Label61 = New System.Windows.Forms.Label()
		Me.b_soft_RadioButton = New System.Windows.Forms.RadioButton()
		Me.b_hard_RadioButton = New System.Windows.Forms.RadioButton()
		Me.b_ProcessTypeCode_TextBox = New System.Windows.Forms.TextBox()
		Me.Label85 = New System.Windows.Forms.Label()
		Me.b_Density_TextBox = New System.Windows.Forms.TextBox()
		Me.Label21 = New System.Windows.Forms.Label()
		Me.b_UnitCode_TextBox = New System.Windows.Forms.TextBox()
		Me.Label23 = New System.Windows.Forms.Label()
		Me.b_WFP_TextBox = New System.Windows.Forms.TextBox()
		Me.Label75 = New System.Windows.Forms.Label()
		Me.b_ISO_TextBox = New System.Windows.Forms.TextBox()
		Me.Label76 = New System.Windows.Forms.Label()
		Me.b_BOD_TextBox = New System.Windows.Forms.TextBox()
		Me.Label25 = New System.Windows.Forms.Label()
		Me.b_BlueWater_TextBox = New System.Windows.Forms.TextBox()
		Me.Label38 = New System.Windows.Forms.Label()
		Me.b_GreenWater_TextBox = New System.Windows.Forms.TextBox()
		Me.Label40 = New System.Windows.Forms.Label()
		Me.b_GrayWater_TextBox = New System.Windows.Forms.TextBox()
		Me.Label42 = New System.Windows.Forms.Label()
		Me.b_DataSource_TextBox = New System.Windows.Forms.TextBox()
		Me.FlowLayoutPanel18 = New System.Windows.Forms.FlowLayoutPanel()
		Me.b_Insert_Button = New System.Windows.Forms.Button()
		Me.b_Update_Button = New System.Windows.Forms.Button()
		Me.b_Delete_Button = New System.Windows.Forms.Button()
		Me.Panel5 = New System.Windows.Forms.Panel()
		Me.b_DataGridView = New System.Windows.Forms.DataGridView()
		Me.FlowLayoutPanel20 = New System.Windows.Forms.FlowLayoutPanel()
		Me.Label47 = New System.Windows.Forms.Label()
		Me.b_NameFilter_TextBox = New System.Windows.Forms.TextBox()
		Me.b_Clean_Button = New System.Windows.Forms.Button()
		Me.Label62 = New System.Windows.Forms.Label()
		Me.b_FilterInfo_Label = New System.Windows.Forms.Label()
		Me.TabPage6 = New System.Windows.Forms.TabPage()
		Me.FlowLayoutPanel21 = New System.Windows.Forms.FlowLayoutPanel()
		Me.Label48 = New System.Windows.Forms.Label()
		Me.d_ProcessName_TextBox = New System.Windows.Forms.TextBox()
		Me.Label63 = New System.Windows.Forms.Label()
		Me.d_ProcessTypeCode_TextBox = New System.Windows.Forms.TextBox()
		Me.Label86 = New System.Windows.Forms.Label()
		Me.d_Density_TextBox = New System.Windows.Forms.TextBox()
		Me.Label49 = New System.Windows.Forms.Label()
		Me.d_UnitCode_TextBox = New System.Windows.Forms.TextBox()
		Me.Label50 = New System.Windows.Forms.Label()
		Me.d_WFP_TextBox = New System.Windows.Forms.TextBox()
		Me.Label77 = New System.Windows.Forms.Label()
		Me.d_ISO_TextBox = New System.Windows.Forms.TextBox()
		Me.Label78 = New System.Windows.Forms.Label()
		Me.d_BOD_TextBox = New System.Windows.Forms.TextBox()
		Me.Label51 = New System.Windows.Forms.Label()
		Me.d_BlueWater_TextBox = New System.Windows.Forms.TextBox()
		Me.Label52 = New System.Windows.Forms.Label()
		Me.d_GreenWater_TextBox = New System.Windows.Forms.TextBox()
		Me.Label53 = New System.Windows.Forms.Label()
		Me.d_GrayWater_TextBox = New System.Windows.Forms.TextBox()
		Me.Label54 = New System.Windows.Forms.Label()
		Me.d_DataSource_TextBox = New System.Windows.Forms.TextBox()
		Me.FlowLayoutPanel22 = New System.Windows.Forms.FlowLayoutPanel()
		Me.d_Insert_Button = New System.Windows.Forms.Button()
		Me.d_Update_Button = New System.Windows.Forms.Button()
		Me.d_Delete_Button = New System.Windows.Forms.Button()
		Me.Panel6 = New System.Windows.Forms.Panel()
		Me.d_DataGridView = New System.Windows.Forms.DataGridView()
		Me.FlowLayoutPanel24 = New System.Windows.Forms.FlowLayoutPanel()
		Me.Label55 = New System.Windows.Forms.Label()
		Me.d_NameFilter_TextBox = New System.Windows.Forms.TextBox()
		Me.d_Clean_Button = New System.Windows.Forms.Button()
		Me.Label64 = New System.Windows.Forms.Label()
		Me.d_FilterInfo_Label = New System.Windows.Forms.Label()
		Me.TabPage7 = New System.Windows.Forms.TabPage()
		Me.FlowLayoutPanel4 = New System.Windows.Forms.FlowLayoutPanel()
		Me.Label79 = New System.Windows.Forms.Label()
		Me.asm_AssemblyMethodName_TextBox = New System.Windows.Forms.TextBox()
		Me.Label81 = New System.Windows.Forms.Label()
		Me.asm_CalculateUnit_TextBox = New System.Windows.Forms.TextBox()
		Me.Label82 = New System.Windows.Forms.Label()
		Me.asm_PowerConsumption_TextBox = New System.Windows.Forms.TextBox()
		Me.Label83 = New System.Windows.Forms.Label()
		Me.asm_PowerCoefficient_TextBox = New System.Windows.Forms.TextBox()
		Me.Label88 = New System.Windows.Forms.Label()
		Me.asm_DataSource_TextBox = New System.Windows.Forms.TextBox()
		Me.Label80 = New System.Windows.Forms.Label()
		Me.asm_Remark_TextBox = New System.Windows.Forms.TextBox()
		Me.FlowLayoutPanel7 = New System.Windows.Forms.FlowLayoutPanel()
		Me.asm_Insert_Button = New System.Windows.Forms.Button()
		Me.asm_Update_Button = New System.Windows.Forms.Button()
		Me.asm_Delete_Button = New System.Windows.Forms.Button()
		Me.Panel7 = New System.Windows.Forms.Panel()
		Me.asm_DataGridView = New System.Windows.Forms.DataGridView()
		Me.FlowLayoutPanel11 = New System.Windows.Forms.FlowLayoutPanel()
		Me.Label89 = New System.Windows.Forms.Label()
		Me.asm_NameFilter_TextBox = New System.Windows.Forms.TextBox()
		Me.asm_Clean_Button = New System.Windows.Forms.Button()
		Me.Label90 = New System.Windows.Forms.Label()
		Me.asm_FilterInfo_Label = New System.Windows.Forms.Label()
		Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
		Me.Label37 = New System.Windows.Forms.Label()
		Me.Label39 = New System.Windows.Forms.Label()
		Me.Label41 = New System.Windows.Forms.Label()
		Me.StatusStrip1.SuspendLayout()
		Me.TabControl1.SuspendLayout()
		Me.TabPage1.SuspendLayout()
		Me.FlowLayoutPanel1.SuspendLayout()
		Me.FlowLayoutPanel2.SuspendLayout()
		Me.Panel1.SuspendLayout()
		CType(Me.m_DataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.FlowLayoutPanel3.SuspendLayout()
		Me.TabPage2.SuspendLayout()
		Me.FlowLayoutPanel5.SuspendLayout()
		Me.FlowLayoutPanel6.SuspendLayout()
		Me.Panel2.SuspendLayout()
		CType(Me.t_DataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.FlowLayoutPanel8.SuspendLayout()
		Me.TabPage3.SuspendLayout()
		Me.FlowLayoutPanel9.SuspendLayout()
		Me.FlowLayoutPanel10.SuspendLayout()
		Me.Panel3.SuspendLayout()
		CType(Me.p_DataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.FlowLayoutPanel12.SuspendLayout()
		Me.TabPage4.SuspendLayout()
		Me.FlowLayoutPanel13.SuspendLayout()
		Me.FlowLayoutPanel14.SuspendLayout()
		Me.Panel4.SuspendLayout()
		CType(Me.e_DataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.FlowLayoutPanel16.SuspendLayout()
		Me.TabPage5.SuspendLayout()
		Me.FlowLayoutPanel17.SuspendLayout()
		Me.FlowLayoutPanel18.SuspendLayout()
		Me.Panel5.SuspendLayout()
		CType(Me.b_DataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.FlowLayoutPanel20.SuspendLayout()
		Me.TabPage6.SuspendLayout()
		Me.FlowLayoutPanel21.SuspendLayout()
		Me.FlowLayoutPanel22.SuspendLayout()
		Me.Panel6.SuspendLayout()
		CType(Me.d_DataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.FlowLayoutPanel24.SuspendLayout()
		Me.TabPage7.SuspendLayout()
		Me.FlowLayoutPanel4.SuspendLayout()
		Me.FlowLayoutPanel7.SuspendLayout()
		Me.Panel7.SuspendLayout()
		CType(Me.asm_DataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.FlowLayoutPanel11.SuspendLayout()
		Me.SuspendLayout()
		'
		'StatusStrip1
		'
		Me.StatusStrip1.ImageScalingSize = New System.Drawing.Size(20, 20)
		Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripStatusLabel1})
		Me.StatusStrip1.Location = New System.Drawing.Point(0, 953)
		Me.StatusStrip1.Name = "StatusStrip1"
		Me.StatusStrip1.Padding = New System.Windows.Forms.Padding(1, 0, 19, 0)
		Me.StatusStrip1.Size = New System.Drawing.Size(1192, 25)
		Me.StatusStrip1.TabIndex = 0
		Me.StatusStrip1.Text = "StatusStrip1"
		'
		'ToolStripStatusLabel1
		'
		Me.ToolStripStatusLabel1.Name = "ToolStripStatusLabel1"
		Me.ToolStripStatusLabel1.Size = New System.Drawing.Size(41, 20)
		Me.ToolStripStatusLabel1.Text = "狀態"
		'
		'TabControl1
		'
		Me.TabControl1.Controls.Add(Me.TabPage1)
		Me.TabControl1.Controls.Add(Me.TabPage2)
		Me.TabControl1.Controls.Add(Me.TabPage3)
		Me.TabControl1.Controls.Add(Me.TabPage4)
		Me.TabControl1.Controls.Add(Me.TabPage5)
		Me.TabControl1.Controls.Add(Me.TabPage6)
		Me.TabControl1.Controls.Add(Me.TabPage7)
		Me.TabControl1.Dock = System.Windows.Forms.DockStyle.Fill
		Me.TabControl1.ImageList = Me.ImageList1
		Me.TabControl1.Location = New System.Drawing.Point(0, 0)
		Me.TabControl1.Margin = New System.Windows.Forms.Padding(2, 4, 4, 4)
		Me.TabControl1.Name = "TabControl1"
		Me.TabControl1.SelectedIndex = 0
		Me.TabControl1.Size = New System.Drawing.Size(1192, 953)
		Me.TabControl1.TabIndex = 1
		'
		'TabPage1
		'
		Me.TabPage1.Controls.Add(Me.FlowLayoutPanel1)
		Me.TabPage1.ImageIndex = 0
		Me.TabPage1.Location = New System.Drawing.Point(4, 25)
		Me.TabPage1.Margin = New System.Windows.Forms.Padding(4)
		Me.TabPage1.Name = "TabPage1"
		Me.TabPage1.Padding = New System.Windows.Forms.Padding(4)
		Me.TabPage1.Size = New System.Drawing.Size(1184, 924)
		Me.TabPage1.TabIndex = 0
		Me.TabPage1.Text = "材料"
		Me.TabPage1.UseVisualStyleBackColor = True
		'
		'FlowLayoutPanel1
		'
		Me.FlowLayoutPanel1.Controls.Add(Me.Label1)
		Me.FlowLayoutPanel1.Controls.Add(Me.m_MaterialName_TextBox)
		Me.FlowLayoutPanel1.Controls.Add(Me.Label2)
		Me.FlowLayoutPanel1.Controls.Add(Me.m_Density_TextBox)
		Me.FlowLayoutPanel1.Controls.Add(Me.Label4)
		Me.FlowLayoutPanel1.Controls.Add(Me.m_WFP_TextBox)
		Me.FlowLayoutPanel1.Controls.Add(Me.Label59)
		Me.FlowLayoutPanel1.Controls.Add(Me.m_ISO_TextBox)
		Me.FlowLayoutPanel1.Controls.Add(Me.Label65)
		Me.FlowLayoutPanel1.Controls.Add(Me.m_BOD_TextBox)
		Me.FlowLayoutPanel1.Controls.Add(Me.Label6)
		Me.FlowLayoutPanel1.Controls.Add(Me.m_BlueWater_TextBox)
		Me.FlowLayoutPanel1.Controls.Add(Me.Label8)
		Me.FlowLayoutPanel1.Controls.Add(Me.m_GreenWater_TextBox)
		Me.FlowLayoutPanel1.Controls.Add(Me.Label10)
		Me.FlowLayoutPanel1.Controls.Add(Me.m_GrayWater_TextBox)
		Me.FlowLayoutPanel1.Controls.Add(Me.Label12)
		Me.FlowLayoutPanel1.Controls.Add(Me.m_DataSource_TextBox)
		Me.FlowLayoutPanel1.Controls.Add(Me.FlowLayoutPanel2)
		Me.FlowLayoutPanel1.Controls.Add(Me.Panel1)
		Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
		Me.FlowLayoutPanel1.Location = New System.Drawing.Point(4, 4)
		Me.FlowLayoutPanel1.Margin = New System.Windows.Forms.Padding(4)
		Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
		Me.FlowLayoutPanel1.Size = New System.Drawing.Size(1176, 916)
		Me.FlowLayoutPanel1.TabIndex = 0
		'
		'Label1
		'
		Me.Label1.Location = New System.Drawing.Point(4, 10)
		Me.Label1.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label1.Name = "Label1"
		Me.Label1.Size = New System.Drawing.Size(160, 20)
		Me.Label1.TabIndex = 0
		Me.Label1.Text = "材料名稱"
		'
		'm_MaterialName_TextBox
		'
		Me.m_MaterialName_TextBox.Location = New System.Drawing.Point(172, 4)
		Me.m_MaterialName_TextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.m_MaterialName_TextBox.Name = "m_MaterialName_TextBox"
		Me.m_MaterialName_TextBox.Size = New System.Drawing.Size(940, 25)
		Me.m_MaterialName_TextBox.TabIndex = 1
		Me.m_MaterialName_TextBox.Tag = "MaterialName"
		'
		'Label2
		'
		Me.Label2.Location = New System.Drawing.Point(4, 43)
		Me.Label2.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label2.Name = "Label2"
		Me.Label2.Size = New System.Drawing.Size(160, 20)
		Me.Label2.TabIndex = 2
		Me.Label2.Text = "材料密度 kg / m^3"
		'
		'm_Density_TextBox
		'
		Me.m_Density_TextBox.Location = New System.Drawing.Point(172, 37)
		Me.m_Density_TextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.m_Density_TextBox.Name = "m_Density_TextBox"
		Me.m_Density_TextBox.Size = New System.Drawing.Size(940, 25)
		Me.m_Density_TextBox.TabIndex = 2
		Me.m_Density_TextBox.Tag = "Density"
		'
		'Label4
		'
		Me.Label4.Location = New System.Drawing.Point(4, 76)
		Me.Label4.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label4.Name = "Label4"
		Me.Label4.Size = New System.Drawing.Size(160, 20)
		Me.Label4.TabIndex = 5
		Me.Label4.Text = "水足跡 m^3/Kg (WFN)"
		'
		'm_WFP_TextBox
		'
		Me.m_WFP_TextBox.Location = New System.Drawing.Point(172, 70)
		Me.m_WFP_TextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.m_WFP_TextBox.Name = "m_WFP_TextBox"
		Me.m_WFP_TextBox.Size = New System.Drawing.Size(940, 25)
		Me.m_WFP_TextBox.TabIndex = 3
		Me.m_WFP_TextBox.Tag = "WFP"
		'
		'Label59
		'
		Me.Label59.Location = New System.Drawing.Point(4, 109)
		Me.Label59.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label59.Name = "Label59"
		Me.Label59.Size = New System.Drawing.Size(160, 20)
		Me.Label59.TabIndex = 25
		Me.Label59.Text = "水足跡 m^3/Kg (ISO)"
		'
		'm_ISO_TextBox
		'
		Me.m_ISO_TextBox.Location = New System.Drawing.Point(172, 103)
		Me.m_ISO_TextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.m_ISO_TextBox.Name = "m_ISO_TextBox"
		Me.m_ISO_TextBox.Size = New System.Drawing.Size(940, 25)
		Me.m_ISO_TextBox.TabIndex = 4
		Me.m_ISO_TextBox.Tag = "ISO"
		'
		'Label65
		'
		Me.Label65.Location = New System.Drawing.Point(4, 142)
		Me.Label65.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label65.Name = "Label65"
		Me.Label65.Size = New System.Drawing.Size(160, 20)
		Me.Label65.TabIndex = 28
		Me.Label65.Text = "BOD mg/Kg"
		'
		'm_BOD_TextBox
		'
		Me.m_BOD_TextBox.Location = New System.Drawing.Point(172, 136)
		Me.m_BOD_TextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.m_BOD_TextBox.Name = "m_BOD_TextBox"
		Me.m_BOD_TextBox.Size = New System.Drawing.Size(940, 25)
		Me.m_BOD_TextBox.TabIndex = 5
		Me.m_BOD_TextBox.Tag = "BOD"
		'
		'Label6
		'
		Me.Label6.Image = Global.WinAP.WFE.My.Resources.Resources.blue_water_16
		Me.Label6.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.Label6.Location = New System.Drawing.Point(4, 175)
		Me.Label6.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label6.Name = "Label6"
		Me.Label6.Size = New System.Drawing.Size(160, 20)
		Me.Label6.TabIndex = 8
		Me.Label6.Text = "藍水 m^3/Kg"
		Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		'
		'm_BlueWater_TextBox
		'
		Me.m_BlueWater_TextBox.Location = New System.Drawing.Point(172, 169)
		Me.m_BlueWater_TextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.m_BlueWater_TextBox.Name = "m_BlueWater_TextBox"
		Me.m_BlueWater_TextBox.Size = New System.Drawing.Size(940, 25)
		Me.m_BlueWater_TextBox.TabIndex = 6
		Me.m_BlueWater_TextBox.Tag = "BlueWater"
		'
		'Label8
		'
		Me.Label8.Image = Global.WinAP.WFE.My.Resources.Resources.green_water_16
		Me.Label8.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.Label8.Location = New System.Drawing.Point(4, 208)
		Me.Label8.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label8.Name = "Label8"
		Me.Label8.Size = New System.Drawing.Size(160, 20)
		Me.Label8.TabIndex = 11
		Me.Label8.Text = "綠水 m^3/Kg"
		Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		'
		'm_GreenWater_TextBox
		'
		Me.m_GreenWater_TextBox.Location = New System.Drawing.Point(172, 202)
		Me.m_GreenWater_TextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.m_GreenWater_TextBox.Name = "m_GreenWater_TextBox"
		Me.m_GreenWater_TextBox.Size = New System.Drawing.Size(940, 25)
		Me.m_GreenWater_TextBox.TabIndex = 7
		Me.m_GreenWater_TextBox.Tag = "GreenWater"
		'
		'Label10
		'
		Me.Label10.Image = Global.WinAP.WFE.My.Resources.Resources.gray_water_16
		Me.Label10.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.Label10.Location = New System.Drawing.Point(4, 241)
		Me.Label10.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label10.Name = "Label10"
		Me.Label10.Size = New System.Drawing.Size(160, 20)
		Me.Label10.TabIndex = 14
		Me.Label10.Text = "灰水 m^3/Kg"
		Me.Label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		'
		'm_GrayWater_TextBox
		'
		Me.m_GrayWater_TextBox.Location = New System.Drawing.Point(172, 235)
		Me.m_GrayWater_TextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.m_GrayWater_TextBox.Name = "m_GrayWater_TextBox"
		Me.m_GrayWater_TextBox.Size = New System.Drawing.Size(940, 25)
		Me.m_GrayWater_TextBox.TabIndex = 8
		Me.m_GrayWater_TextBox.Tag = "GrayWater"
		'
		'Label12
		'
		Me.Label12.Location = New System.Drawing.Point(4, 274)
		Me.Label12.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label12.Name = "Label12"
		Me.Label12.Size = New System.Drawing.Size(160, 20)
		Me.Label12.TabIndex = 17
		Me.Label12.Text = "資料來源"
		'
		'm_DataSource_TextBox
		'
		Me.m_DataSource_TextBox.Location = New System.Drawing.Point(172, 268)
		Me.m_DataSource_TextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.m_DataSource_TextBox.Name = "m_DataSource_TextBox"
		Me.m_DataSource_TextBox.Size = New System.Drawing.Size(940, 25)
		Me.m_DataSource_TextBox.TabIndex = 9
		Me.m_DataSource_TextBox.Tag = "DataSource"
		'
		'FlowLayoutPanel2
		'
		Me.FlowLayoutPanel2.Controls.Add(Me.m_Insert_Button)
		Me.FlowLayoutPanel2.Controls.Add(Me.m_Update_Button)
		Me.FlowLayoutPanel2.Controls.Add(Me.m_Delete_Button)
		Me.FlowLayoutPanel2.Location = New System.Drawing.Point(4, 301)
		Me.FlowLayoutPanel2.Margin = New System.Windows.Forms.Padding(4)
		Me.FlowLayoutPanel2.Name = "FlowLayoutPanel2"
		Me.FlowLayoutPanel2.Size = New System.Drawing.Size(1165, 45)
		Me.FlowLayoutPanel2.TabIndex = 20
		'
		'm_Insert_Button
		'
		Me.m_Insert_Button.ForeColor = System.Drawing.Color.Blue
		Me.m_Insert_Button.Location = New System.Drawing.Point(2, 4)
		Me.m_Insert_Button.Margin = New System.Windows.Forms.Padding(2, 4, 4, 4)
		Me.m_Insert_Button.Name = "m_Insert_Button"
		Me.m_Insert_Button.Size = New System.Drawing.Size(380, 38)
		Me.m_Insert_Button.TabIndex = 21
		Me.m_Insert_Button.Tag = "Insert"
		Me.m_Insert_Button.Text = "新增"
		Me.m_Insert_Button.UseVisualStyleBackColor = True
		'
		'm_Update_Button
		'
		Me.m_Update_Button.ForeColor = System.Drawing.Color.DarkGreen
		Me.m_Update_Button.Location = New System.Drawing.Point(390, 4)
		Me.m_Update_Button.Margin = New System.Windows.Forms.Padding(4)
		Me.m_Update_Button.Name = "m_Update_Button"
		Me.m_Update_Button.Size = New System.Drawing.Size(375, 38)
		Me.m_Update_Button.TabIndex = 22
		Me.m_Update_Button.Tag = "Update"
		Me.m_Update_Button.Text = "更新"
		Me.m_Update_Button.UseVisualStyleBackColor = True
		'
		'm_Delete_Button
		'
		Me.m_Delete_Button.ForeColor = System.Drawing.Color.Red
		Me.m_Delete_Button.Location = New System.Drawing.Point(773, 4)
		Me.m_Delete_Button.Margin = New System.Windows.Forms.Padding(4)
		Me.m_Delete_Button.Name = "m_Delete_Button"
		Me.m_Delete_Button.Size = New System.Drawing.Size(335, 38)
		Me.m_Delete_Button.TabIndex = 23
		Me.m_Delete_Button.Tag = "Delete"
		Me.m_Delete_Button.Text = "刪除"
		Me.m_Delete_Button.UseVisualStyleBackColor = True
		'
		'Panel1
		'
		Me.Panel1.Controls.Add(Me.m_DataGridView)
		Me.Panel1.Controls.Add(Me.FlowLayoutPanel3)
		Me.Panel1.Location = New System.Drawing.Point(4, 354)
		Me.Panel1.Margin = New System.Windows.Forms.Padding(4)
		Me.Panel1.Name = "Panel1"
		Me.Panel1.Size = New System.Drawing.Size(1165, 555)
		Me.Panel1.TabIndex = 21
		'
		'm_DataGridView
		'
		Me.m_DataGridView.AllowUserToAddRows = False
		Me.m_DataGridView.AllowUserToDeleteRows = False
		Me.m_DataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
		Me.m_DataGridView.Dock = System.Windows.Forms.DockStyle.Fill
		Me.m_DataGridView.Location = New System.Drawing.Point(0, 69)
		Me.m_DataGridView.Margin = New System.Windows.Forms.Padding(4)
		Me.m_DataGridView.Name = "m_DataGridView"
		Me.m_DataGridView.ReadOnly = True
		Me.m_DataGridView.RowTemplate.Height = 24
		Me.m_DataGridView.Size = New System.Drawing.Size(1165, 486)
		Me.m_DataGridView.TabIndex = 41
		'
		'FlowLayoutPanel3
		'
		Me.FlowLayoutPanel3.Controls.Add(Me.Label14)
		Me.FlowLayoutPanel3.Controls.Add(Me.m_NameFilter_TextBox)
		Me.FlowLayoutPanel3.Controls.Add(Me.m_Clean_Button)
		Me.FlowLayoutPanel3.Controls.Add(Me.Label57)
		Me.FlowLayoutPanel3.Controls.Add(Me.m_FilterInfo_Label)
		Me.FlowLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Top
		Me.FlowLayoutPanel3.Location = New System.Drawing.Point(0, 0)
		Me.FlowLayoutPanel3.Margin = New System.Windows.Forms.Padding(4)
		Me.FlowLayoutPanel3.Name = "FlowLayoutPanel3"
		Me.FlowLayoutPanel3.Size = New System.Drawing.Size(1165, 69)
		Me.FlowLayoutPanel3.TabIndex = 4
		'
		'Label14
		'
		Me.Label14.Location = New System.Drawing.Point(2, 10)
		Me.Label14.Margin = New System.Windows.Forms.Padding(2, 10, 4, 0)
		Me.Label14.Name = "Label14"
		Me.Label14.Size = New System.Drawing.Size(160, 20)
		Me.Label14.TabIndex = 0
		Me.Label14.Text = "材料名稱篩選"
		'
		'm_NameFilter_TextBox
		'
		Me.m_NameFilter_TextBox.Location = New System.Drawing.Point(170, 4)
		Me.m_NameFilter_TextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.m_NameFilter_TextBox.Name = "m_NameFilter_TextBox"
		Me.m_NameFilter_TextBox.Size = New System.Drawing.Size(745, 25)
		Me.m_NameFilter_TextBox.TabIndex = 31
		Me.m_NameFilter_TextBox.Tag = "NameFilter"
		'
		'm_Clean_Button
		'
		Me.m_Clean_Button.Location = New System.Drawing.Point(923, 4)
		Me.m_Clean_Button.Margin = New System.Windows.Forms.Padding(4)
		Me.m_Clean_Button.Name = "m_Clean_Button"
		Me.m_Clean_Button.Size = New System.Drawing.Size(185, 29)
		Me.m_Clean_Button.TabIndex = 32
		Me.m_Clean_Button.Tag = "Clean"
		Me.m_Clean_Button.Text = "清除篩選"
		Me.m_Clean_Button.UseVisualStyleBackColor = True
		'
		'Label57
		'
		Me.Label57.AutoSize = True
		Me.Label57.ForeColor = System.Drawing.SystemColors.Highlight
		Me.Label57.Location = New System.Drawing.Point(2, 47)
		Me.Label57.Margin = New System.Windows.Forms.Padding(2, 10, 4, 0)
		Me.Label57.Name = "Label57"
		Me.Label57.Size = New System.Drawing.Size(124, 15)
		Me.Label57.TabIndex = 5
		Me.Label57.Text = "篩選筆數 / 總筆數"
		'
		'm_FilterInfo_Label
		'
		Me.m_FilterInfo_Label.AutoSize = True
		Me.m_FilterInfo_Label.ForeColor = System.Drawing.Color.Red
		Me.m_FilterInfo_Label.Location = New System.Drawing.Point(134, 47)
		Me.m_FilterInfo_Label.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.m_FilterInfo_Label.Name = "m_FilterInfo_Label"
		Me.m_FilterInfo_Label.Size = New System.Drawing.Size(31, 15)
		Me.m_FilterInfo_Label.TabIndex = 6
		Me.m_FilterInfo_Label.Text = "N/A"
		'
		'TabPage2
		'
		Me.TabPage2.Controls.Add(Me.FlowLayoutPanel5)
		Me.TabPage2.ImageIndex = 1
		Me.TabPage2.Location = New System.Drawing.Point(4, 25)
		Me.TabPage2.Margin = New System.Windows.Forms.Padding(4)
		Me.TabPage2.Name = "TabPage2"
		Me.TabPage2.Padding = New System.Windows.Forms.Padding(4)
		Me.TabPage2.Size = New System.Drawing.Size(1184, 924)
		Me.TabPage2.TabIndex = 1
		Me.TabPage2.Text = "運輸工具"
		Me.TabPage2.UseVisualStyleBackColor = True
		'
		'FlowLayoutPanel5
		'
		Me.FlowLayoutPanel5.Controls.Add(Me.Label15)
		Me.FlowLayoutPanel5.Controls.Add(Me.t_TransportName_TextBox)
		Me.FlowLayoutPanel5.Controls.Add(Me.Label16)
		Me.FlowLayoutPanel5.Controls.Add(Me.t_CalculateUnit_TextBox)
		Me.FlowLayoutPanel5.Controls.Add(Me.Label29)
		Me.FlowLayoutPanel5.Controls.Add(Me.t_MaxLoad_TextBox)
		Me.FlowLayoutPanel5.Controls.Add(Me.Label18)
		Me.FlowLayoutPanel5.Controls.Add(Me.t_WFP_TextBox)
		Me.FlowLayoutPanel5.Controls.Add(Me.Label69)
		Me.FlowLayoutPanel5.Controls.Add(Me.t_ISO_TextBox)
		Me.FlowLayoutPanel5.Controls.Add(Me.Label70)
		Me.FlowLayoutPanel5.Controls.Add(Me.t_BOD_TextBox)
		Me.FlowLayoutPanel5.Controls.Add(Me.Label20)
		Me.FlowLayoutPanel5.Controls.Add(Me.t_BlueWater_TextBox)
		Me.FlowLayoutPanel5.Controls.Add(Me.Label22)
		Me.FlowLayoutPanel5.Controls.Add(Me.t_GreenWater_TextBox)
		Me.FlowLayoutPanel5.Controls.Add(Me.Label24)
		Me.FlowLayoutPanel5.Controls.Add(Me.t_GrayWater_TextBox)
		Me.FlowLayoutPanel5.Controls.Add(Me.Label17)
		Me.FlowLayoutPanel5.Controls.Add(Me.t_Unit_TextBox)
		Me.FlowLayoutPanel5.Controls.Add(Me.Label31)
		Me.FlowLayoutPanel5.Controls.Add(Me.t_EnergyPerUnit_TextBox)
		Me.FlowLayoutPanel5.Controls.Add(Me.Label26)
		Me.FlowLayoutPanel5.Controls.Add(Me.t_DataSource_TextBox)
		Me.FlowLayoutPanel5.Controls.Add(Me.FlowLayoutPanel6)
		Me.FlowLayoutPanel5.Controls.Add(Me.Panel2)
		Me.FlowLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill
		Me.FlowLayoutPanel5.Location = New System.Drawing.Point(4, 4)
		Me.FlowLayoutPanel5.Margin = New System.Windows.Forms.Padding(4)
		Me.FlowLayoutPanel5.Name = "FlowLayoutPanel5"
		Me.FlowLayoutPanel5.Size = New System.Drawing.Size(1176, 916)
		Me.FlowLayoutPanel5.TabIndex = 1
		'
		'Label15
		'
		Me.Label15.Location = New System.Drawing.Point(4, 10)
		Me.Label15.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label15.Name = "Label15"
		Me.Label15.Size = New System.Drawing.Size(160, 20)
		Me.Label15.TabIndex = 0
		Me.Label15.Text = "運輸工具名稱"
		'
		't_TransportName_TextBox
		'
		Me.t_TransportName_TextBox.Location = New System.Drawing.Point(172, 4)
		Me.t_TransportName_TextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.t_TransportName_TextBox.Name = "t_TransportName_TextBox"
		Me.t_TransportName_TextBox.Size = New System.Drawing.Size(940, 25)
		Me.t_TransportName_TextBox.TabIndex = 1
		'
		'Label16
		'
		Me.Label16.Location = New System.Drawing.Point(4, 43)
		Me.Label16.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label16.Name = "Label16"
		Me.Label16.Size = New System.Drawing.Size(160, 20)
		Me.Label16.TabIndex = 2
		Me.Label16.Text = "計算單位"
		'
		't_CalculateUnit_TextBox
		'
		Me.t_CalculateUnit_TextBox.Location = New System.Drawing.Point(172, 37)
		Me.t_CalculateUnit_TextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.t_CalculateUnit_TextBox.Name = "t_CalculateUnit_TextBox"
		Me.t_CalculateUnit_TextBox.Size = New System.Drawing.Size(940, 25)
		Me.t_CalculateUnit_TextBox.TabIndex = 2
		'
		'Label29
		'
		Me.Label29.Location = New System.Drawing.Point(4, 76)
		Me.Label29.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label29.Name = "Label29"
		Me.Label29.Size = New System.Drawing.Size(160, 20)
		Me.Label29.TabIndex = 22
		Me.Label29.Text = "最大載貨重量 kg"
		'
		't_MaxLoad_TextBox
		'
		Me.t_MaxLoad_TextBox.Location = New System.Drawing.Point(172, 70)
		Me.t_MaxLoad_TextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.t_MaxLoad_TextBox.Name = "t_MaxLoad_TextBox"
		Me.t_MaxLoad_TextBox.Size = New System.Drawing.Size(940, 25)
		Me.t_MaxLoad_TextBox.TabIndex = 3
		'
		'Label18
		'
		Me.Label18.Location = New System.Drawing.Point(4, 109)
		Me.Label18.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label18.Name = "Label18"
		Me.Label18.Size = New System.Drawing.Size(160, 20)
		Me.Label18.TabIndex = 5
		Me.Label18.Text = "水足跡 m^3/Kg (WFN)"
		'
		't_WFP_TextBox
		'
		Me.t_WFP_TextBox.Location = New System.Drawing.Point(172, 103)
		Me.t_WFP_TextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.t_WFP_TextBox.Name = "t_WFP_TextBox"
		Me.t_WFP_TextBox.Size = New System.Drawing.Size(940, 25)
		Me.t_WFP_TextBox.TabIndex = 4
		'
		'Label69
		'
		Me.Label69.Location = New System.Drawing.Point(4, 142)
		Me.Label69.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label69.Name = "Label69"
		Me.Label69.Size = New System.Drawing.Size(160, 20)
		Me.Label69.TabIndex = 33
		Me.Label69.Text = "水足跡 m^3/Kg (ISO)"
		'
		't_ISO_TextBox
		'
		Me.t_ISO_TextBox.Location = New System.Drawing.Point(172, 136)
		Me.t_ISO_TextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.t_ISO_TextBox.Name = "t_ISO_TextBox"
		Me.t_ISO_TextBox.Size = New System.Drawing.Size(940, 25)
		Me.t_ISO_TextBox.TabIndex = 5
		'
		'Label70
		'
		Me.Label70.Location = New System.Drawing.Point(4, 175)
		Me.Label70.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label70.Name = "Label70"
		Me.Label70.Size = New System.Drawing.Size(160, 20)
		Me.Label70.TabIndex = 34
		Me.Label70.Text = "BOD mg/Kg"
		'
		't_BOD_TextBox
		'
		Me.t_BOD_TextBox.Location = New System.Drawing.Point(172, 169)
		Me.t_BOD_TextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.t_BOD_TextBox.Name = "t_BOD_TextBox"
		Me.t_BOD_TextBox.Size = New System.Drawing.Size(940, 25)
		Me.t_BOD_TextBox.TabIndex = 6
		'
		'Label20
		'
		Me.Label20.Image = Global.WinAP.WFE.My.Resources.Resources.blue_water_16
		Me.Label20.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.Label20.Location = New System.Drawing.Point(4, 208)
		Me.Label20.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label20.Name = "Label20"
		Me.Label20.Size = New System.Drawing.Size(160, 20)
		Me.Label20.TabIndex = 8
		Me.Label20.Text = "藍水  m^3/Kg"
		Me.Label20.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		'
		't_BlueWater_TextBox
		'
		Me.t_BlueWater_TextBox.Location = New System.Drawing.Point(172, 202)
		Me.t_BlueWater_TextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.t_BlueWater_TextBox.Name = "t_BlueWater_TextBox"
		Me.t_BlueWater_TextBox.Size = New System.Drawing.Size(940, 25)
		Me.t_BlueWater_TextBox.TabIndex = 7
		'
		'Label22
		'
		Me.Label22.Image = Global.WinAP.WFE.My.Resources.Resources.green_water_16
		Me.Label22.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.Label22.Location = New System.Drawing.Point(4, 241)
		Me.Label22.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label22.Name = "Label22"
		Me.Label22.Size = New System.Drawing.Size(160, 20)
		Me.Label22.TabIndex = 11
		Me.Label22.Text = "綠水  m^3/Kg"
		Me.Label22.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		'
		't_GreenWater_TextBox
		'
		Me.t_GreenWater_TextBox.Location = New System.Drawing.Point(172, 235)
		Me.t_GreenWater_TextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.t_GreenWater_TextBox.Name = "t_GreenWater_TextBox"
		Me.t_GreenWater_TextBox.Size = New System.Drawing.Size(940, 25)
		Me.t_GreenWater_TextBox.TabIndex = 8
		'
		'Label24
		'
		Me.Label24.Image = Global.WinAP.WFE.My.Resources.Resources.gray_water_16
		Me.Label24.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.Label24.Location = New System.Drawing.Point(4, 274)
		Me.Label24.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label24.Name = "Label24"
		Me.Label24.Size = New System.Drawing.Size(160, 20)
		Me.Label24.TabIndex = 14
		Me.Label24.Text = "灰水  m^3/Kg"
		Me.Label24.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		'
		't_GrayWater_TextBox
		'
		Me.t_GrayWater_TextBox.Location = New System.Drawing.Point(172, 268)
		Me.t_GrayWater_TextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.t_GrayWater_TextBox.Name = "t_GrayWater_TextBox"
		Me.t_GrayWater_TextBox.Size = New System.Drawing.Size(940, 25)
		Me.t_GrayWater_TextBox.TabIndex = 9
		'
		'Label17
		'
		Me.Label17.Location = New System.Drawing.Point(4, 307)
		Me.Label17.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label17.Name = "Label17"
		Me.Label17.Size = New System.Drawing.Size(160, 20)
		Me.Label17.TabIndex = 25
		Me.Label17.Text = "單位"
		'
		't_Unit_TextBox
		'
		Me.t_Unit_TextBox.Location = New System.Drawing.Point(172, 301)
		Me.t_Unit_TextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.t_Unit_TextBox.Name = "t_Unit_TextBox"
		Me.t_Unit_TextBox.Size = New System.Drawing.Size(940, 25)
		Me.t_Unit_TextBox.TabIndex = 10
		'
		'Label31
		'
		Me.Label31.Location = New System.Drawing.Point(4, 340)
		Me.Label31.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label31.Name = "Label31"
		Me.Label31.Size = New System.Drawing.Size(160, 20)
		Me.Label31.TabIndex = 27
		Me.Label31.Text = "能源 / 單位"
		'
		't_EnergyPerUnit_TextBox
		'
		Me.t_EnergyPerUnit_TextBox.Location = New System.Drawing.Point(172, 334)
		Me.t_EnergyPerUnit_TextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.t_EnergyPerUnit_TextBox.Name = "t_EnergyPerUnit_TextBox"
		Me.t_EnergyPerUnit_TextBox.Size = New System.Drawing.Size(940, 25)
		Me.t_EnergyPerUnit_TextBox.TabIndex = 11
		'
		'Label26
		'
		Me.Label26.Location = New System.Drawing.Point(4, 373)
		Me.Label26.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label26.Name = "Label26"
		Me.Label26.Size = New System.Drawing.Size(160, 20)
		Me.Label26.TabIndex = 17
		Me.Label26.Text = "資料來源"
		'
		't_DataSource_TextBox
		'
		Me.t_DataSource_TextBox.Location = New System.Drawing.Point(172, 367)
		Me.t_DataSource_TextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.t_DataSource_TextBox.Name = "t_DataSource_TextBox"
		Me.t_DataSource_TextBox.Size = New System.Drawing.Size(940, 25)
		Me.t_DataSource_TextBox.TabIndex = 12
		'
		'FlowLayoutPanel6
		'
		Me.FlowLayoutPanel6.Controls.Add(Me.t_Insert_Button)
		Me.FlowLayoutPanel6.Controls.Add(Me.t_Update_Button)
		Me.FlowLayoutPanel6.Controls.Add(Me.t_Delete_Button)
		Me.FlowLayoutPanel6.Location = New System.Drawing.Point(4, 400)
		Me.FlowLayoutPanel6.Margin = New System.Windows.Forms.Padding(4)
		Me.FlowLayoutPanel6.Name = "FlowLayoutPanel6"
		Me.FlowLayoutPanel6.Size = New System.Drawing.Size(1165, 45)
		Me.FlowLayoutPanel6.TabIndex = 20
		'
		't_Insert_Button
		'
		Me.t_Insert_Button.ForeColor = System.Drawing.Color.Blue
		Me.t_Insert_Button.Location = New System.Drawing.Point(2, 4)
		Me.t_Insert_Button.Margin = New System.Windows.Forms.Padding(2, 4, 4, 4)
		Me.t_Insert_Button.Name = "t_Insert_Button"
		Me.t_Insert_Button.Size = New System.Drawing.Size(380, 38)
		Me.t_Insert_Button.TabIndex = 21
		Me.t_Insert_Button.Tag = "Insert"
		Me.t_Insert_Button.Text = "新增"
		Me.t_Insert_Button.UseVisualStyleBackColor = True
		'
		't_Update_Button
		'
		Me.t_Update_Button.ForeColor = System.Drawing.Color.DarkGreen
		Me.t_Update_Button.Location = New System.Drawing.Point(390, 4)
		Me.t_Update_Button.Margin = New System.Windows.Forms.Padding(4)
		Me.t_Update_Button.Name = "t_Update_Button"
		Me.t_Update_Button.Size = New System.Drawing.Size(375, 38)
		Me.t_Update_Button.TabIndex = 22
		Me.t_Update_Button.Tag = "Update"
		Me.t_Update_Button.Text = "更新"
		Me.t_Update_Button.UseVisualStyleBackColor = True
		'
		't_Delete_Button
		'
		Me.t_Delete_Button.ForeColor = System.Drawing.Color.Red
		Me.t_Delete_Button.Location = New System.Drawing.Point(773, 4)
		Me.t_Delete_Button.Margin = New System.Windows.Forms.Padding(4)
		Me.t_Delete_Button.Name = "t_Delete_Button"
		Me.t_Delete_Button.Size = New System.Drawing.Size(335, 38)
		Me.t_Delete_Button.TabIndex = 23
		Me.t_Delete_Button.Tag = "Delete"
		Me.t_Delete_Button.Text = "刪除"
		Me.t_Delete_Button.UseVisualStyleBackColor = True
		'
		'Panel2
		'
		Me.Panel2.Controls.Add(Me.t_DataGridView)
		Me.Panel2.Controls.Add(Me.FlowLayoutPanel8)
		Me.Panel2.Location = New System.Drawing.Point(4, 453)
		Me.Panel2.Margin = New System.Windows.Forms.Padding(4)
		Me.Panel2.Name = "Panel2"
		Me.Panel2.Size = New System.Drawing.Size(1165, 455)
		Me.Panel2.TabIndex = 21
		'
		't_DataGridView
		'
		Me.t_DataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
		Me.t_DataGridView.Dock = System.Windows.Forms.DockStyle.Fill
		Me.t_DataGridView.Location = New System.Drawing.Point(0, 69)
		Me.t_DataGridView.Margin = New System.Windows.Forms.Padding(4)
		Me.t_DataGridView.Name = "t_DataGridView"
		Me.t_DataGridView.RowTemplate.Height = 24
		Me.t_DataGridView.Size = New System.Drawing.Size(1165, 386)
		Me.t_DataGridView.TabIndex = 41
		'
		'FlowLayoutPanel8
		'
		Me.FlowLayoutPanel8.Controls.Add(Me.Label28)
		Me.FlowLayoutPanel8.Controls.Add(Me.t_NameFilter_TextBox)
		Me.FlowLayoutPanel8.Controls.Add(Me.t_Clean_Button)
		Me.FlowLayoutPanel8.Controls.Add(Me.Label56)
		Me.FlowLayoutPanel8.Controls.Add(Me.t_FilterInfo_Label)
		Me.FlowLayoutPanel8.Dock = System.Windows.Forms.DockStyle.Top
		Me.FlowLayoutPanel8.Location = New System.Drawing.Point(0, 0)
		Me.FlowLayoutPanel8.Margin = New System.Windows.Forms.Padding(4)
		Me.FlowLayoutPanel8.Name = "FlowLayoutPanel8"
		Me.FlowLayoutPanel8.Size = New System.Drawing.Size(1165, 69)
		Me.FlowLayoutPanel8.TabIndex = 4
		'
		'Label28
		'
		Me.Label28.Location = New System.Drawing.Point(2, 10)
		Me.Label28.Margin = New System.Windows.Forms.Padding(2, 10, 4, 0)
		Me.Label28.Name = "Label28"
		Me.Label28.Size = New System.Drawing.Size(160, 20)
		Me.Label28.TabIndex = 0
		Me.Label28.Text = "運輸工具名稱篩選"
		'
		't_NameFilter_TextBox
		'
		Me.t_NameFilter_TextBox.Location = New System.Drawing.Point(170, 4)
		Me.t_NameFilter_TextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.t_NameFilter_TextBox.Name = "t_NameFilter_TextBox"
		Me.t_NameFilter_TextBox.Size = New System.Drawing.Size(745, 25)
		Me.t_NameFilter_TextBox.TabIndex = 31
		'
		't_Clean_Button
		'
		Me.t_Clean_Button.Location = New System.Drawing.Point(923, 4)
		Me.t_Clean_Button.Margin = New System.Windows.Forms.Padding(4)
		Me.t_Clean_Button.Name = "t_Clean_Button"
		Me.t_Clean_Button.Size = New System.Drawing.Size(185, 29)
		Me.t_Clean_Button.TabIndex = 32
		Me.t_Clean_Button.Text = "清除篩選"
		Me.t_Clean_Button.UseVisualStyleBackColor = True
		'
		'Label56
		'
		Me.Label56.AutoSize = True
		Me.Label56.ForeColor = System.Drawing.SystemColors.Highlight
		Me.Label56.Location = New System.Drawing.Point(2, 47)
		Me.Label56.Margin = New System.Windows.Forms.Padding(2, 10, 4, 0)
		Me.Label56.Name = "Label56"
		Me.Label56.Size = New System.Drawing.Size(124, 15)
		Me.Label56.TabIndex = 3
		Me.Label56.Text = "篩選筆數 / 總筆數"
		'
		't_FilterInfo_Label
		'
		Me.t_FilterInfo_Label.AutoSize = True
		Me.t_FilterInfo_Label.ForeColor = System.Drawing.Color.Red
		Me.t_FilterInfo_Label.Location = New System.Drawing.Point(134, 47)
		Me.t_FilterInfo_Label.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.t_FilterInfo_Label.Name = "t_FilterInfo_Label"
		Me.t_FilterInfo_Label.Size = New System.Drawing.Size(31, 15)
		Me.t_FilterInfo_Label.TabIndex = 4
		Me.t_FilterInfo_Label.Text = "N/A"
		'
		'TabPage3
		'
		Me.TabPage3.Controls.Add(Me.FlowLayoutPanel9)
		Me.TabPage3.ImageIndex = 2
		Me.TabPage3.Location = New System.Drawing.Point(4, 25)
		Me.TabPage3.Margin = New System.Windows.Forms.Padding(4)
		Me.TabPage3.Name = "TabPage3"
		Me.TabPage3.Size = New System.Drawing.Size(1184, 924)
		Me.TabPage3.TabIndex = 2
		Me.TabPage3.Text = "加工方式"
		Me.TabPage3.UseVisualStyleBackColor = True
		'
		'FlowLayoutPanel9
		'
		Me.FlowLayoutPanel9.Controls.Add(Me.Label27)
		Me.FlowLayoutPanel9.Controls.Add(Me.p_MakeProcessName_TextBox)
		Me.FlowLayoutPanel9.Controls.Add(Me.Label32)
		Me.FlowLayoutPanel9.Controls.Add(Me.p_CalculateCode_TextBox)
		Me.FlowLayoutPanel9.Controls.Add(Me.p_CalculateCode_ComboBox)
		Me.FlowLayoutPanel9.Controls.Add(Me.Label66)
		Me.FlowLayoutPanel9.Controls.Add(Me.p_CalculateUnit_TextBox)
		Me.FlowLayoutPanel9.Controls.Add(Me.Label33)
		Me.FlowLayoutPanel9.Controls.Add(Me.p_MachineName_TextBox)
		Me.FlowLayoutPanel9.Controls.Add(Me.Label35)
		Me.FlowLayoutPanel9.Controls.Add(Me.p_WFP_TextBox)
		Me.FlowLayoutPanel9.Controls.Add(Me.Label71)
		Me.FlowLayoutPanel9.Controls.Add(Me.p_ISO_TextBox)
		Me.FlowLayoutPanel9.Controls.Add(Me.Label72)
		Me.FlowLayoutPanel9.Controls.Add(Me.p_BOD_TextBox)
		Me.FlowLayoutPanel9.Controls.Add(Me.Label37)
		Me.FlowLayoutPanel9.Controls.Add(Me.p_BlueWater_TextBox)
		Me.FlowLayoutPanel9.Controls.Add(Me.Label39)
		Me.FlowLayoutPanel9.Controls.Add(Me.p_GreenWater_TextBox)
		Me.FlowLayoutPanel9.Controls.Add(Me.Label41)
		Me.FlowLayoutPanel9.Controls.Add(Me.p_GrayWater_TextBox)
		Me.FlowLayoutPanel9.Controls.Add(Me.Label44)
		Me.FlowLayoutPanel9.Controls.Add(Me.p_EnergyPerUnit_TextBox)
		Me.FlowLayoutPanel9.Controls.Add(Me.Label43)
		Me.FlowLayoutPanel9.Controls.Add(Me.p_PowerConsumption_TextBox)
		Me.FlowLayoutPanel9.Controls.Add(Me.Label45)
		Me.FlowLayoutPanel9.Controls.Add(Me.p_PowerCoefficient_TextBox)
		Me.FlowLayoutPanel9.Controls.Add(Me.Label34)
		Me.FlowLayoutPanel9.Controls.Add(Me.p_DataSource_TextBox)
		Me.FlowLayoutPanel9.Controls.Add(Me.FlowLayoutPanel10)
		Me.FlowLayoutPanel9.Controls.Add(Me.Panel3)
		Me.FlowLayoutPanel9.Dock = System.Windows.Forms.DockStyle.Fill
		Me.FlowLayoutPanel9.Location = New System.Drawing.Point(0, 0)
		Me.FlowLayoutPanel9.Margin = New System.Windows.Forms.Padding(4)
		Me.FlowLayoutPanel9.Name = "FlowLayoutPanel9"
		Me.FlowLayoutPanel9.Size = New System.Drawing.Size(1184, 924)
		Me.FlowLayoutPanel9.TabIndex = 2
		'
		'Label27
		'
		Me.Label27.Font = New System.Drawing.Font("PMingLiU", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label27.Location = New System.Drawing.Point(4, 10)
		Me.Label27.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label27.Name = "Label27"
		Me.Label27.Size = New System.Drawing.Size(160, 20)
		Me.Label27.TabIndex = 0
		Me.Label27.Text = "加工方式名稱"
		'
		'p_MakeProcessName_TextBox
		'
		Me.p_MakeProcessName_TextBox.Location = New System.Drawing.Point(172, 4)
		Me.p_MakeProcessName_TextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.p_MakeProcessName_TextBox.Name = "p_MakeProcessName_TextBox"
		Me.p_MakeProcessName_TextBox.Size = New System.Drawing.Size(940, 25)
		Me.p_MakeProcessName_TextBox.TabIndex = 1
		'
		'Label32
		'
		Me.Label32.Location = New System.Drawing.Point(4, 43)
		Me.Label32.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label32.Name = "Label32"
		Me.Label32.Size = New System.Drawing.Size(160, 20)
		Me.Label32.TabIndex = 2
		Me.Label32.Text = "計算方式分類"
		'
		'p_CalculateCode_TextBox
		'
		Me.p_CalculateCode_TextBox.Location = New System.Drawing.Point(172, 37)
		Me.p_CalculateCode_TextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.p_CalculateCode_TextBox.Name = "p_CalculateCode_TextBox"
		Me.p_CalculateCode_TextBox.ReadOnly = True
		Me.p_CalculateCode_TextBox.Size = New System.Drawing.Size(518, 25)
		Me.p_CalculateCode_TextBox.TabIndex = 38
		'
		'p_CalculateCode_ComboBox
		'
		Me.p_CalculateCode_ComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
		Me.p_CalculateCode_ComboBox.FormattingEnabled = True
		Me.p_CalculateCode_ComboBox.Items.AddRange(New Object() {"電力消耗 (Power)", "水足跡(WFP)"})
		Me.p_CalculateCode_ComboBox.Location = New System.Drawing.Point(698, 37)
		Me.p_CalculateCode_ComboBox.Margin = New System.Windows.Forms.Padding(4)
		Me.p_CalculateCode_ComboBox.Name = "p_CalculateCode_ComboBox"
		Me.p_CalculateCode_ComboBox.Size = New System.Drawing.Size(414, 23)
		Me.p_CalculateCode_ComboBox.TabIndex = 2
		'
		'Label66
		'
		Me.Label66.Location = New System.Drawing.Point(4, 76)
		Me.Label66.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label66.Name = "Label66"
		Me.Label66.Size = New System.Drawing.Size(160, 20)
		Me.Label66.TabIndex = 35
		Me.Label66.Text = "計算單位"
		'
		'p_CalculateUnit_TextBox
		'
		Me.p_CalculateUnit_TextBox.Location = New System.Drawing.Point(172, 70)
		Me.p_CalculateUnit_TextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.p_CalculateUnit_TextBox.Name = "p_CalculateUnit_TextBox"
		Me.p_CalculateUnit_TextBox.Size = New System.Drawing.Size(940, 25)
		Me.p_CalculateUnit_TextBox.TabIndex = 3
		'
		'Label33
		'
		Me.Label33.Location = New System.Drawing.Point(4, 109)
		Me.Label33.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label33.Name = "Label33"
		Me.Label33.Size = New System.Drawing.Size(160, 20)
		Me.Label33.TabIndex = 22
		Me.Label33.Text = "機具名稱"
		'
		'p_MachineName_TextBox
		'
		Me.p_MachineName_TextBox.Location = New System.Drawing.Point(172, 103)
		Me.p_MachineName_TextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.p_MachineName_TextBox.Name = "p_MachineName_TextBox"
		Me.p_MachineName_TextBox.Size = New System.Drawing.Size(940, 25)
		Me.p_MachineName_TextBox.TabIndex = 4
		'
		'Label35
		'
		Me.Label35.Location = New System.Drawing.Point(4, 142)
		Me.Label35.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label35.Name = "Label35"
		Me.Label35.Size = New System.Drawing.Size(160, 20)
		Me.Label35.TabIndex = 5
		Me.Label35.Text = "水足跡 (WFN) m^3/Kg"
		'
		'p_WFP_TextBox
		'
		Me.p_WFP_TextBox.Location = New System.Drawing.Point(172, 136)
		Me.p_WFP_TextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.p_WFP_TextBox.Name = "p_WFP_TextBox"
		Me.p_WFP_TextBox.Size = New System.Drawing.Size(940, 25)
		Me.p_WFP_TextBox.TabIndex = 5
		'
		'Label71
		'
		Me.Label71.Location = New System.Drawing.Point(4, 175)
		Me.Label71.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label71.Name = "Label71"
		Me.Label71.Size = New System.Drawing.Size(160, 20)
		Me.Label71.TabIndex = 39
		Me.Label71.Text = "水足跡 (ISO) m^3/Kg"
		'
		'p_ISO_TextBox
		'
		Me.p_ISO_TextBox.Location = New System.Drawing.Point(172, 169)
		Me.p_ISO_TextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.p_ISO_TextBox.Name = "p_ISO_TextBox"
		Me.p_ISO_TextBox.Size = New System.Drawing.Size(940, 25)
		Me.p_ISO_TextBox.TabIndex = 6
		'
		'Label72
		'
		Me.Label72.Location = New System.Drawing.Point(4, 208)
		Me.Label72.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label72.Name = "Label72"
		Me.Label72.Size = New System.Drawing.Size(160, 20)
		Me.Label72.TabIndex = 41
		Me.Label72.Text = "BOD mg/Kg"
		'
		'p_BOD_TextBox
		'
		Me.p_BOD_TextBox.Location = New System.Drawing.Point(172, 202)
		Me.p_BOD_TextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.p_BOD_TextBox.Name = "p_BOD_TextBox"
		Me.p_BOD_TextBox.Size = New System.Drawing.Size(940, 25)
		Me.p_BOD_TextBox.TabIndex = 7
		'
		'p_BlueWater_TextBox
		'
		Me.p_BlueWater_TextBox.Location = New System.Drawing.Point(172, 235)
		Me.p_BlueWater_TextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.p_BlueWater_TextBox.Name = "p_BlueWater_TextBox"
		Me.p_BlueWater_TextBox.Size = New System.Drawing.Size(940, 25)
		Me.p_BlueWater_TextBox.TabIndex = 8
		'
		'p_GreenWater_TextBox
		'
		Me.p_GreenWater_TextBox.Location = New System.Drawing.Point(172, 268)
		Me.p_GreenWater_TextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.p_GreenWater_TextBox.Name = "p_GreenWater_TextBox"
		Me.p_GreenWater_TextBox.Size = New System.Drawing.Size(940, 25)
		Me.p_GreenWater_TextBox.TabIndex = 9
		'
		'p_GrayWater_TextBox
		'
		Me.p_GrayWater_TextBox.Location = New System.Drawing.Point(172, 301)
		Me.p_GrayWater_TextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.p_GrayWater_TextBox.Name = "p_GrayWater_TextBox"
		Me.p_GrayWater_TextBox.Size = New System.Drawing.Size(940, 25)
		Me.p_GrayWater_TextBox.TabIndex = 10
		'
		'Label44
		'
		Me.Label44.Location = New System.Drawing.Point(4, 340)
		Me.Label44.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label44.Name = "Label44"
		Me.Label44.Size = New System.Drawing.Size(160, 20)
		Me.Label44.TabIndex = 27
		Me.Label44.Text = "能源 / 單位"
		'
		'p_EnergyPerUnit_TextBox
		'
		Me.p_EnergyPerUnit_TextBox.Location = New System.Drawing.Point(172, 334)
		Me.p_EnergyPerUnit_TextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.p_EnergyPerUnit_TextBox.Name = "p_EnergyPerUnit_TextBox"
		Me.p_EnergyPerUnit_TextBox.Size = New System.Drawing.Size(940, 25)
		Me.p_EnergyPerUnit_TextBox.TabIndex = 11
		'
		'Label43
		'
		Me.Label43.Location = New System.Drawing.Point(4, 373)
		Me.Label43.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label43.Name = "Label43"
		Me.Label43.Size = New System.Drawing.Size(160, 20)
		Me.Label43.TabIndex = 25
		Me.Label43.Text = "電力消耗量 Kw"
		'
		'p_PowerConsumption_TextBox
		'
		Me.p_PowerConsumption_TextBox.Location = New System.Drawing.Point(172, 367)
		Me.p_PowerConsumption_TextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.p_PowerConsumption_TextBox.Name = "p_PowerConsumption_TextBox"
		Me.p_PowerConsumption_TextBox.Size = New System.Drawing.Size(940, 25)
		Me.p_PowerConsumption_TextBox.TabIndex = 12
		'
		'Label45
		'
		Me.Label45.Location = New System.Drawing.Point(4, 406)
		Me.Label45.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label45.Name = "Label45"
		Me.Label45.Size = New System.Drawing.Size(160, 20)
		Me.Label45.TabIndex = 17
		Me.Label45.Text = "電力系數 m^3/Kw．hr"
		'
		'p_PowerCoefficient_TextBox
		'
		Me.p_PowerCoefficient_TextBox.Location = New System.Drawing.Point(172, 400)
		Me.p_PowerCoefficient_TextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.p_PowerCoefficient_TextBox.Name = "p_PowerCoefficient_TextBox"
		Me.p_PowerCoefficient_TextBox.Size = New System.Drawing.Size(940, 25)
		Me.p_PowerCoefficient_TextBox.TabIndex = 13
		'
		'Label34
		'
		Me.Label34.Location = New System.Drawing.Point(4, 439)
		Me.Label34.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label34.Name = "Label34"
		Me.Label34.Size = New System.Drawing.Size(160, 20)
		Me.Label34.TabIndex = 30
		Me.Label34.Text = "資料來源"
		'
		'p_DataSource_TextBox
		'
		Me.p_DataSource_TextBox.Location = New System.Drawing.Point(172, 433)
		Me.p_DataSource_TextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.p_DataSource_TextBox.Name = "p_DataSource_TextBox"
		Me.p_DataSource_TextBox.Size = New System.Drawing.Size(940, 25)
		Me.p_DataSource_TextBox.TabIndex = 14
		'
		'FlowLayoutPanel10
		'
		Me.FlowLayoutPanel10.Controls.Add(Me.p_Insert_Button)
		Me.FlowLayoutPanel10.Controls.Add(Me.p_Update_Button)
		Me.FlowLayoutPanel10.Controls.Add(Me.p_Delete_Button)
		Me.FlowLayoutPanel10.Location = New System.Drawing.Point(4, 466)
		Me.FlowLayoutPanel10.Margin = New System.Windows.Forms.Padding(4)
		Me.FlowLayoutPanel10.Name = "FlowLayoutPanel10"
		Me.FlowLayoutPanel10.Size = New System.Drawing.Size(1165, 45)
		Me.FlowLayoutPanel10.TabIndex = 20
		'
		'p_Insert_Button
		'
		Me.p_Insert_Button.ForeColor = System.Drawing.Color.Blue
		Me.p_Insert_Button.Location = New System.Drawing.Point(2, 4)
		Me.p_Insert_Button.Margin = New System.Windows.Forms.Padding(2, 4, 4, 4)
		Me.p_Insert_Button.Name = "p_Insert_Button"
		Me.p_Insert_Button.Size = New System.Drawing.Size(380, 38)
		Me.p_Insert_Button.TabIndex = 20
		Me.p_Insert_Button.Tag = "Insert"
		Me.p_Insert_Button.Text = "新增"
		Me.p_Insert_Button.UseVisualStyleBackColor = True
		'
		'p_Update_Button
		'
		Me.p_Update_Button.ForeColor = System.Drawing.Color.DarkGreen
		Me.p_Update_Button.Location = New System.Drawing.Point(390, 4)
		Me.p_Update_Button.Margin = New System.Windows.Forms.Padding(4)
		Me.p_Update_Button.Name = "p_Update_Button"
		Me.p_Update_Button.Size = New System.Drawing.Size(375, 38)
		Me.p_Update_Button.TabIndex = 21
		Me.p_Update_Button.Tag = "Update"
		Me.p_Update_Button.Text = "更新"
		Me.p_Update_Button.UseVisualStyleBackColor = True
		'
		'p_Delete_Button
		'
		Me.p_Delete_Button.ForeColor = System.Drawing.Color.Red
		Me.p_Delete_Button.Location = New System.Drawing.Point(773, 4)
		Me.p_Delete_Button.Margin = New System.Windows.Forms.Padding(4)
		Me.p_Delete_Button.Name = "p_Delete_Button"
		Me.p_Delete_Button.Size = New System.Drawing.Size(335, 38)
		Me.p_Delete_Button.TabIndex = 22
		Me.p_Delete_Button.Tag = "Delete"
		Me.p_Delete_Button.Text = "刪除"
		Me.p_Delete_Button.UseVisualStyleBackColor = True
		'
		'Panel3
		'
		Me.Panel3.Controls.Add(Me.p_DataGridView)
		Me.Panel3.Controls.Add(Me.FlowLayoutPanel12)
		Me.Panel3.Location = New System.Drawing.Point(4, 519)
		Me.Panel3.Margin = New System.Windows.Forms.Padding(4)
		Me.Panel3.Name = "Panel3"
		Me.Panel3.Size = New System.Drawing.Size(1165, 395)
		Me.Panel3.TabIndex = 21
		'
		'p_DataGridView
		'
		Me.p_DataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
		Me.p_DataGridView.Dock = System.Windows.Forms.DockStyle.Fill
		Me.p_DataGridView.Location = New System.Drawing.Point(0, 69)
		Me.p_DataGridView.Margin = New System.Windows.Forms.Padding(4)
		Me.p_DataGridView.Name = "p_DataGridView"
		Me.p_DataGridView.RowTemplate.Height = 24
		Me.p_DataGridView.Size = New System.Drawing.Size(1165, 326)
		Me.p_DataGridView.TabIndex = 41
		'
		'FlowLayoutPanel12
		'
		Me.FlowLayoutPanel12.Controls.Add(Me.Label46)
		Me.FlowLayoutPanel12.Controls.Add(Me.p_NameFilter_TextBox)
		Me.FlowLayoutPanel12.Controls.Add(Me.p_Clean_Button)
		Me.FlowLayoutPanel12.Controls.Add(Me.Label58)
		Me.FlowLayoutPanel12.Controls.Add(Me.p_FilterInfo_Label)
		Me.FlowLayoutPanel12.Dock = System.Windows.Forms.DockStyle.Top
		Me.FlowLayoutPanel12.Location = New System.Drawing.Point(0, 0)
		Me.FlowLayoutPanel12.Margin = New System.Windows.Forms.Padding(4)
		Me.FlowLayoutPanel12.Name = "FlowLayoutPanel12"
		Me.FlowLayoutPanel12.Size = New System.Drawing.Size(1165, 69)
		Me.FlowLayoutPanel12.TabIndex = 4
		'
		'Label46
		'
		Me.Label46.Location = New System.Drawing.Point(2, 10)
		Me.Label46.Margin = New System.Windows.Forms.Padding(2, 10, 4, 0)
		Me.Label46.Name = "Label46"
		Me.Label46.Size = New System.Drawing.Size(160, 20)
		Me.Label46.TabIndex = 0
		Me.Label46.Text = "加工名稱篩選"
		'
		'p_NameFilter_TextBox
		'
		Me.p_NameFilter_TextBox.Location = New System.Drawing.Point(170, 4)
		Me.p_NameFilter_TextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.p_NameFilter_TextBox.Name = "p_NameFilter_TextBox"
		Me.p_NameFilter_TextBox.Size = New System.Drawing.Size(745, 25)
		Me.p_NameFilter_TextBox.TabIndex = 31
		'
		'p_Clean_Button
		'
		Me.p_Clean_Button.Location = New System.Drawing.Point(923, 4)
		Me.p_Clean_Button.Margin = New System.Windows.Forms.Padding(4)
		Me.p_Clean_Button.Name = "p_Clean_Button"
		Me.p_Clean_Button.Size = New System.Drawing.Size(185, 29)
		Me.p_Clean_Button.TabIndex = 32
		Me.p_Clean_Button.Text = "清除篩選"
		Me.p_Clean_Button.UseVisualStyleBackColor = True
		'
		'Label58
		'
		Me.Label58.AutoSize = True
		Me.Label58.ForeColor = System.Drawing.SystemColors.Highlight
		Me.Label58.Location = New System.Drawing.Point(2, 47)
		Me.Label58.Margin = New System.Windows.Forms.Padding(2, 10, 4, 0)
		Me.Label58.Name = "Label58"
		Me.Label58.Size = New System.Drawing.Size(124, 15)
		Me.Label58.TabIndex = 5
		Me.Label58.Text = "篩選筆數 / 總筆數"
		'
		'p_FilterInfo_Label
		'
		Me.p_FilterInfo_Label.AutoSize = True
		Me.p_FilterInfo_Label.ForeColor = System.Drawing.Color.Red
		Me.p_FilterInfo_Label.Location = New System.Drawing.Point(134, 47)
		Me.p_FilterInfo_Label.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.p_FilterInfo_Label.Name = "p_FilterInfo_Label"
		Me.p_FilterInfo_Label.Size = New System.Drawing.Size(31, 15)
		Me.p_FilterInfo_Label.TabIndex = 6
		Me.p_FilterInfo_Label.Text = "N/A"
		'
		'TabPage4
		'
		Me.TabPage4.Controls.Add(Me.FlowLayoutPanel13)
		Me.TabPage4.ImageIndex = 3
		Me.TabPage4.Location = New System.Drawing.Point(4, 25)
		Me.TabPage4.Margin = New System.Windows.Forms.Padding(4)
		Me.TabPage4.Name = "TabPage4"
		Me.TabPage4.Size = New System.Drawing.Size(1184, 924)
		Me.TabPage4.TabIndex = 3
		Me.TabPage4.Text = "電子元件製程"
		Me.TabPage4.UseVisualStyleBackColor = True
		'
		'FlowLayoutPanel13
		'
		Me.FlowLayoutPanel13.Controls.Add(Me.Label3)
		Me.FlowLayoutPanel13.Controls.Add(Me.e_ProcessName_TextBox)
		Me.FlowLayoutPanel13.Controls.Add(Me.Label67)
		Me.FlowLayoutPanel13.Controls.Add(Me.e_ProcessTypeCode_TextBox)
		Me.FlowLayoutPanel13.Controls.Add(Me.Label5)
		Me.FlowLayoutPanel13.Controls.Add(Me.e_Density_TextBox)
		Me.FlowLayoutPanel13.Controls.Add(Me.Label68)
		Me.FlowLayoutPanel13.Controls.Add(Me.e_UnitCode_TextBox)
		Me.FlowLayoutPanel13.Controls.Add(Me.Label9)
		Me.FlowLayoutPanel13.Controls.Add(Me.e_WFP_TextBox)
		Me.FlowLayoutPanel13.Controls.Add(Me.Label73)
		Me.FlowLayoutPanel13.Controls.Add(Me.e_ISO_TextBox)
		Me.FlowLayoutPanel13.Controls.Add(Me.Label74)
		Me.FlowLayoutPanel13.Controls.Add(Me.e_BOD_TextBox)
		Me.FlowLayoutPanel13.Controls.Add(Me.Label11)
		Me.FlowLayoutPanel13.Controls.Add(Me.e_BlueWater_TextBox)
		Me.FlowLayoutPanel13.Controls.Add(Me.Label13)
		Me.FlowLayoutPanel13.Controls.Add(Me.e_GreenWater_TextBox)
		Me.FlowLayoutPanel13.Controls.Add(Me.Label19)
		Me.FlowLayoutPanel13.Controls.Add(Me.e_GrayWater_TextBox)
		Me.FlowLayoutPanel13.Controls.Add(Me.Label30)
		Me.FlowLayoutPanel13.Controls.Add(Me.e_DataSource_TextBox)
		Me.FlowLayoutPanel13.Controls.Add(Me.FlowLayoutPanel14)
		Me.FlowLayoutPanel13.Controls.Add(Me.Panel4)
		Me.FlowLayoutPanel13.Dock = System.Windows.Forms.DockStyle.Fill
		Me.FlowLayoutPanel13.Location = New System.Drawing.Point(0, 0)
		Me.FlowLayoutPanel13.Margin = New System.Windows.Forms.Padding(4)
		Me.FlowLayoutPanel13.Name = "FlowLayoutPanel13"
		Me.FlowLayoutPanel13.Size = New System.Drawing.Size(1184, 924)
		Me.FlowLayoutPanel13.TabIndex = 3
		'
		'Label3
		'
		Me.Label3.Location = New System.Drawing.Point(4, 10)
		Me.Label3.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label3.Name = "Label3"
		Me.Label3.Size = New System.Drawing.Size(160, 20)
		Me.Label3.TabIndex = 0
		Me.Label3.Text = "製程名稱"
		'
		'e_ProcessName_TextBox
		'
		Me.e_ProcessName_TextBox.Location = New System.Drawing.Point(172, 4)
		Me.e_ProcessName_TextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.e_ProcessName_TextBox.Name = "e_ProcessName_TextBox"
		Me.e_ProcessName_TextBox.Size = New System.Drawing.Size(940, 25)
		Me.e_ProcessName_TextBox.TabIndex = 1
		'
		'Label67
		'
		Me.Label67.Location = New System.Drawing.Point(4, 43)
		Me.Label67.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label67.Name = "Label67"
		Me.Label67.Size = New System.Drawing.Size(160, 20)
		Me.Label67.TabIndex = 35
		Me.Label67.Text = "製程類別代碼"
		'
		'e_ProcessTypeCode_TextBox
		'
		Me.e_ProcessTypeCode_TextBox.Location = New System.Drawing.Point(172, 37)
		Me.e_ProcessTypeCode_TextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.e_ProcessTypeCode_TextBox.Name = "e_ProcessTypeCode_TextBox"
		Me.e_ProcessTypeCode_TextBox.ReadOnly = True
		Me.e_ProcessTypeCode_TextBox.Size = New System.Drawing.Size(940, 25)
		Me.e_ProcessTypeCode_TextBox.TabIndex = 2
		Me.e_ProcessTypeCode_TextBox.Text = "E"
		'
		'Label5
		'
		Me.Label5.Location = New System.Drawing.Point(4, 76)
		Me.Label5.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label5.Name = "Label5"
		Me.Label5.Size = New System.Drawing.Size(160, 20)
		Me.Label5.TabIndex = 2
		Me.Label5.Text = "密度 Kg / m^3"
		'
		'e_Density_TextBox
		'
		Me.e_Density_TextBox.Location = New System.Drawing.Point(172, 70)
		Me.e_Density_TextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.e_Density_TextBox.Name = "e_Density_TextBox"
		Me.e_Density_TextBox.Size = New System.Drawing.Size(940, 25)
		Me.e_Density_TextBox.TabIndex = 2
		'
		'Label68
		'
		Me.Label68.Location = New System.Drawing.Point(4, 109)
		Me.Label68.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label68.Name = "Label68"
		Me.Label68.Size = New System.Drawing.Size(160, 20)
		Me.Label68.TabIndex = 37
		Me.Label68.Text = "單位"
		'
		'e_UnitCode_TextBox
		'
		Me.e_UnitCode_TextBox.Location = New System.Drawing.Point(172, 103)
		Me.e_UnitCode_TextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.e_UnitCode_TextBox.Name = "e_UnitCode_TextBox"
		Me.e_UnitCode_TextBox.Size = New System.Drawing.Size(940, 25)
		Me.e_UnitCode_TextBox.TabIndex = 3
		'
		'Label9
		'
		Me.Label9.Location = New System.Drawing.Point(4, 142)
		Me.Label9.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label9.Name = "Label9"
		Me.Label9.Size = New System.Drawing.Size(160, 20)
		Me.Label9.TabIndex = 5
		Me.Label9.Text = "水足跡  m^3/Kg (WFN)"
		'
		'e_WFP_TextBox
		'
		Me.e_WFP_TextBox.Location = New System.Drawing.Point(172, 136)
		Me.e_WFP_TextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.e_WFP_TextBox.Name = "e_WFP_TextBox"
		Me.e_WFP_TextBox.Size = New System.Drawing.Size(940, 25)
		Me.e_WFP_TextBox.TabIndex = 4
		'
		'Label73
		'
		Me.Label73.Location = New System.Drawing.Point(4, 175)
		Me.Label73.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label73.Name = "Label73"
		Me.Label73.Size = New System.Drawing.Size(160, 20)
		Me.Label73.TabIndex = 39
		Me.Label73.Text = "水足跡  m^3/Kg (ISO)"
		'
		'e_ISO_TextBox
		'
		Me.e_ISO_TextBox.Location = New System.Drawing.Point(172, 169)
		Me.e_ISO_TextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.e_ISO_TextBox.Name = "e_ISO_TextBox"
		Me.e_ISO_TextBox.Size = New System.Drawing.Size(940, 25)
		Me.e_ISO_TextBox.TabIndex = 5
		'
		'Label74
		'
		Me.Label74.Location = New System.Drawing.Point(4, 208)
		Me.Label74.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label74.Name = "Label74"
		Me.Label74.Size = New System.Drawing.Size(160, 20)
		Me.Label74.TabIndex = 41
		Me.Label74.Text = "BOD mg/Kg"
		'
		'e_BOD_TextBox
		'
		Me.e_BOD_TextBox.Location = New System.Drawing.Point(172, 202)
		Me.e_BOD_TextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.e_BOD_TextBox.Name = "e_BOD_TextBox"
		Me.e_BOD_TextBox.Size = New System.Drawing.Size(940, 25)
		Me.e_BOD_TextBox.TabIndex = 6
		'
		'Label11
		'
		Me.Label11.Image = Global.WinAP.WFE.My.Resources.Resources.blue_water_16
		Me.Label11.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.Label11.Location = New System.Drawing.Point(4, 241)
		Me.Label11.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label11.Name = "Label11"
		Me.Label11.Size = New System.Drawing.Size(160, 20)
		Me.Label11.TabIndex = 8
		Me.Label11.Text = "藍水 m^3/Kg"
		Me.Label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		'
		'e_BlueWater_TextBox
		'
		Me.e_BlueWater_TextBox.Location = New System.Drawing.Point(172, 235)
		Me.e_BlueWater_TextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.e_BlueWater_TextBox.Name = "e_BlueWater_TextBox"
		Me.e_BlueWater_TextBox.Size = New System.Drawing.Size(940, 25)
		Me.e_BlueWater_TextBox.TabIndex = 7
		'
		'Label13
		'
		Me.Label13.Image = Global.WinAP.WFE.My.Resources.Resources.green_water_16
		Me.Label13.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.Label13.Location = New System.Drawing.Point(4, 274)
		Me.Label13.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label13.Name = "Label13"
		Me.Label13.Size = New System.Drawing.Size(160, 20)
		Me.Label13.TabIndex = 11
		Me.Label13.Text = "綠水 m^3/Kg"
		Me.Label13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		'
		'e_GreenWater_TextBox
		'
		Me.e_GreenWater_TextBox.Location = New System.Drawing.Point(172, 268)
		Me.e_GreenWater_TextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.e_GreenWater_TextBox.Name = "e_GreenWater_TextBox"
		Me.e_GreenWater_TextBox.Size = New System.Drawing.Size(940, 25)
		Me.e_GreenWater_TextBox.TabIndex = 8
		'
		'Label19
		'
		Me.Label19.Image = Global.WinAP.WFE.My.Resources.Resources.gray_water_16
		Me.Label19.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.Label19.Location = New System.Drawing.Point(4, 307)
		Me.Label19.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label19.Name = "Label19"
		Me.Label19.Size = New System.Drawing.Size(160, 20)
		Me.Label19.TabIndex = 14
		Me.Label19.Text = "灰水 m^3/Kg"
		Me.Label19.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		'
		'e_GrayWater_TextBox
		'
		Me.e_GrayWater_TextBox.Location = New System.Drawing.Point(172, 301)
		Me.e_GrayWater_TextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.e_GrayWater_TextBox.Name = "e_GrayWater_TextBox"
		Me.e_GrayWater_TextBox.Size = New System.Drawing.Size(940, 25)
		Me.e_GrayWater_TextBox.TabIndex = 9
		'
		'Label30
		'
		Me.Label30.Location = New System.Drawing.Point(4, 340)
		Me.Label30.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label30.Name = "Label30"
		Me.Label30.Size = New System.Drawing.Size(160, 20)
		Me.Label30.TabIndex = 30
		Me.Label30.Text = "資料來源"
		'
		'e_DataSource_TextBox
		'
		Me.e_DataSource_TextBox.Location = New System.Drawing.Point(172, 334)
		Me.e_DataSource_TextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.e_DataSource_TextBox.Name = "e_DataSource_TextBox"
		Me.e_DataSource_TextBox.Size = New System.Drawing.Size(940, 25)
		Me.e_DataSource_TextBox.TabIndex = 10
		'
		'FlowLayoutPanel14
		'
		Me.FlowLayoutPanel14.Controls.Add(Me.e_Insert_Button)
		Me.FlowLayoutPanel14.Controls.Add(Me.e_Update_Button)
		Me.FlowLayoutPanel14.Controls.Add(Me.e_Delete_Button)
		Me.FlowLayoutPanel14.Location = New System.Drawing.Point(4, 367)
		Me.FlowLayoutPanel14.Margin = New System.Windows.Forms.Padding(4)
		Me.FlowLayoutPanel14.Name = "FlowLayoutPanel14"
		Me.FlowLayoutPanel14.Size = New System.Drawing.Size(1165, 45)
		Me.FlowLayoutPanel14.TabIndex = 20
		'
		'e_Insert_Button
		'
		Me.e_Insert_Button.ForeColor = System.Drawing.Color.Blue
		Me.e_Insert_Button.Location = New System.Drawing.Point(2, 4)
		Me.e_Insert_Button.Margin = New System.Windows.Forms.Padding(2, 4, 4, 4)
		Me.e_Insert_Button.Name = "e_Insert_Button"
		Me.e_Insert_Button.Size = New System.Drawing.Size(380, 38)
		Me.e_Insert_Button.TabIndex = 21
		Me.e_Insert_Button.Tag = "Insert"
		Me.e_Insert_Button.Text = "新增"
		Me.e_Insert_Button.UseVisualStyleBackColor = True
		'
		'e_Update_Button
		'
		Me.e_Update_Button.ForeColor = System.Drawing.Color.DarkGreen
		Me.e_Update_Button.Location = New System.Drawing.Point(390, 4)
		Me.e_Update_Button.Margin = New System.Windows.Forms.Padding(4)
		Me.e_Update_Button.Name = "e_Update_Button"
		Me.e_Update_Button.Size = New System.Drawing.Size(375, 38)
		Me.e_Update_Button.TabIndex = 22
		Me.e_Update_Button.Tag = "Update"
		Me.e_Update_Button.Text = "更新"
		Me.e_Update_Button.UseVisualStyleBackColor = True
		'
		'e_Delete_Button
		'
		Me.e_Delete_Button.ForeColor = System.Drawing.Color.Red
		Me.e_Delete_Button.Location = New System.Drawing.Point(773, 4)
		Me.e_Delete_Button.Margin = New System.Windows.Forms.Padding(4)
		Me.e_Delete_Button.Name = "e_Delete_Button"
		Me.e_Delete_Button.Size = New System.Drawing.Size(335, 38)
		Me.e_Delete_Button.TabIndex = 23
		Me.e_Delete_Button.Tag = "Delete"
		Me.e_Delete_Button.Text = "刪除"
		Me.e_Delete_Button.UseVisualStyleBackColor = True
		'
		'Panel4
		'
		Me.Panel4.Controls.Add(Me.e_DataGridView)
		Me.Panel4.Controls.Add(Me.FlowLayoutPanel16)
		Me.Panel4.Location = New System.Drawing.Point(4, 420)
		Me.Panel4.Margin = New System.Windows.Forms.Padding(4)
		Me.Panel4.Name = "Panel4"
		Me.Panel4.Size = New System.Drawing.Size(1165, 495)
		Me.Panel4.TabIndex = 21
		'
		'e_DataGridView
		'
		Me.e_DataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
		Me.e_DataGridView.Dock = System.Windows.Forms.DockStyle.Fill
		Me.e_DataGridView.Location = New System.Drawing.Point(0, 69)
		Me.e_DataGridView.Margin = New System.Windows.Forms.Padding(4)
		Me.e_DataGridView.Name = "e_DataGridView"
		Me.e_DataGridView.RowTemplate.Height = 24
		Me.e_DataGridView.Size = New System.Drawing.Size(1165, 426)
		Me.e_DataGridView.TabIndex = 41
		'
		'FlowLayoutPanel16
		'
		Me.FlowLayoutPanel16.Controls.Add(Me.Label36)
		Me.FlowLayoutPanel16.Controls.Add(Me.e_NameFilter_TextBox)
		Me.FlowLayoutPanel16.Controls.Add(Me.e_Clean_Button)
		Me.FlowLayoutPanel16.Controls.Add(Me.Label60)
		Me.FlowLayoutPanel16.Controls.Add(Me.e_FilterInfo_Label)
		Me.FlowLayoutPanel16.Dock = System.Windows.Forms.DockStyle.Top
		Me.FlowLayoutPanel16.Location = New System.Drawing.Point(0, 0)
		Me.FlowLayoutPanel16.Margin = New System.Windows.Forms.Padding(4)
		Me.FlowLayoutPanel16.Name = "FlowLayoutPanel16"
		Me.FlowLayoutPanel16.Size = New System.Drawing.Size(1165, 69)
		Me.FlowLayoutPanel16.TabIndex = 4
		'
		'Label36
		'
		Me.Label36.Location = New System.Drawing.Point(2, 10)
		Me.Label36.Margin = New System.Windows.Forms.Padding(2, 10, 4, 0)
		Me.Label36.Name = "Label36"
		Me.Label36.Size = New System.Drawing.Size(160, 20)
		Me.Label36.TabIndex = 0
		Me.Label36.Text = "材料名稱篩選"
		'
		'e_NameFilter_TextBox
		'
		Me.e_NameFilter_TextBox.Location = New System.Drawing.Point(170, 4)
		Me.e_NameFilter_TextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.e_NameFilter_TextBox.Name = "e_NameFilter_TextBox"
		Me.e_NameFilter_TextBox.Size = New System.Drawing.Size(745, 25)
		Me.e_NameFilter_TextBox.TabIndex = 31
		'
		'e_Clean_Button
		'
		Me.e_Clean_Button.Location = New System.Drawing.Point(923, 4)
		Me.e_Clean_Button.Margin = New System.Windows.Forms.Padding(4)
		Me.e_Clean_Button.Name = "e_Clean_Button"
		Me.e_Clean_Button.Size = New System.Drawing.Size(185, 29)
		Me.e_Clean_Button.TabIndex = 2
		Me.e_Clean_Button.Text = "清除篩選"
		Me.e_Clean_Button.UseVisualStyleBackColor = True
		'
		'Label60
		'
		Me.Label60.AutoSize = True
		Me.Label60.ForeColor = System.Drawing.SystemColors.Highlight
		Me.Label60.Location = New System.Drawing.Point(2, 47)
		Me.Label60.Margin = New System.Windows.Forms.Padding(2, 10, 4, 0)
		Me.Label60.Name = "Label60"
		Me.Label60.Size = New System.Drawing.Size(124, 15)
		Me.Label60.TabIndex = 5
		Me.Label60.Text = "篩選筆數 / 總筆數"
		'
		'e_FilterInfo_Label
		'
		Me.e_FilterInfo_Label.AutoSize = True
		Me.e_FilterInfo_Label.ForeColor = System.Drawing.Color.Red
		Me.e_FilterInfo_Label.Location = New System.Drawing.Point(134, 47)
		Me.e_FilterInfo_Label.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.e_FilterInfo_Label.Name = "e_FilterInfo_Label"
		Me.e_FilterInfo_Label.Size = New System.Drawing.Size(31, 15)
		Me.e_FilterInfo_Label.TabIndex = 6
		Me.e_FilterInfo_Label.Text = "N/A"
		'
		'TabPage5
		'
		Me.TabPage5.Controls.Add(Me.FlowLayoutPanel17)
		Me.TabPage5.ImageIndex = 4
		Me.TabPage5.Location = New System.Drawing.Point(4, 25)
		Me.TabPage5.Margin = New System.Windows.Forms.Padding(4)
		Me.TabPage5.Name = "TabPage5"
		Me.TabPage5.Size = New System.Drawing.Size(1184, 924)
		Me.TabPage5.TabIndex = 4
		Me.TabPage5.Text = "電路板製程"
		Me.TabPage5.UseVisualStyleBackColor = True
		'
		'FlowLayoutPanel17
		'
		Me.FlowLayoutPanel17.Controls.Add(Me.Label7)
		Me.FlowLayoutPanel17.Controls.Add(Me.b_ProcessName_TextBox)
		Me.FlowLayoutPanel17.Controls.Add(Me.Label61)
		Me.FlowLayoutPanel17.Controls.Add(Me.b_soft_RadioButton)
		Me.FlowLayoutPanel17.Controls.Add(Me.b_hard_RadioButton)
		Me.FlowLayoutPanel17.Controls.Add(Me.b_ProcessTypeCode_TextBox)
		Me.FlowLayoutPanel17.Controls.Add(Me.Label85)
		Me.FlowLayoutPanel17.Controls.Add(Me.b_Density_TextBox)
		Me.FlowLayoutPanel17.Controls.Add(Me.Label21)
		Me.FlowLayoutPanel17.Controls.Add(Me.b_UnitCode_TextBox)
		Me.FlowLayoutPanel17.Controls.Add(Me.Label23)
		Me.FlowLayoutPanel17.Controls.Add(Me.b_WFP_TextBox)
		Me.FlowLayoutPanel17.Controls.Add(Me.Label75)
		Me.FlowLayoutPanel17.Controls.Add(Me.b_ISO_TextBox)
		Me.FlowLayoutPanel17.Controls.Add(Me.Label76)
		Me.FlowLayoutPanel17.Controls.Add(Me.b_BOD_TextBox)
		Me.FlowLayoutPanel17.Controls.Add(Me.Label25)
		Me.FlowLayoutPanel17.Controls.Add(Me.b_BlueWater_TextBox)
		Me.FlowLayoutPanel17.Controls.Add(Me.Label38)
		Me.FlowLayoutPanel17.Controls.Add(Me.b_GreenWater_TextBox)
		Me.FlowLayoutPanel17.Controls.Add(Me.Label40)
		Me.FlowLayoutPanel17.Controls.Add(Me.b_GrayWater_TextBox)
		Me.FlowLayoutPanel17.Controls.Add(Me.Label42)
		Me.FlowLayoutPanel17.Controls.Add(Me.b_DataSource_TextBox)
		Me.FlowLayoutPanel17.Controls.Add(Me.FlowLayoutPanel18)
		Me.FlowLayoutPanel17.Controls.Add(Me.Panel5)
		Me.FlowLayoutPanel17.Dock = System.Windows.Forms.DockStyle.Fill
		Me.FlowLayoutPanel17.Location = New System.Drawing.Point(0, 0)
		Me.FlowLayoutPanel17.Margin = New System.Windows.Forms.Padding(4)
		Me.FlowLayoutPanel17.Name = "FlowLayoutPanel17"
		Me.FlowLayoutPanel17.Size = New System.Drawing.Size(1184, 924)
		Me.FlowLayoutPanel17.TabIndex = 4
		'
		'Label7
		'
		Me.Label7.Location = New System.Drawing.Point(4, 10)
		Me.Label7.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label7.Name = "Label7"
		Me.Label7.Size = New System.Drawing.Size(160, 20)
		Me.Label7.TabIndex = 0
		Me.Label7.Text = "製程名稱"
		'
		'b_ProcessName_TextBox
		'
		Me.b_ProcessName_TextBox.Location = New System.Drawing.Point(172, 4)
		Me.b_ProcessName_TextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.b_ProcessName_TextBox.Name = "b_ProcessName_TextBox"
		Me.b_ProcessName_TextBox.Size = New System.Drawing.Size(940, 25)
		Me.b_ProcessName_TextBox.TabIndex = 1
		'
		'Label61
		'
		Me.Label61.Location = New System.Drawing.Point(4, 43)
		Me.Label61.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label61.Name = "Label61"
		Me.Label61.Size = New System.Drawing.Size(160, 20)
		Me.Label61.TabIndex = 37
		Me.Label61.Text = "製程類別代碼"
		'
		'b_soft_RadioButton
		'
		Me.b_soft_RadioButton.AutoSize = True
		Me.b_soft_RadioButton.Checked = True
		Me.b_soft_RadioButton.ForeColor = System.Drawing.Color.DarkGreen
		Me.b_soft_RadioButton.Location = New System.Drawing.Point(172, 37)
		Me.b_soft_RadioButton.Margin = New System.Windows.Forms.Padding(4)
		Me.b_soft_RadioButton.Name = "b_soft_RadioButton"
		Me.b_soft_RadioButton.Size = New System.Drawing.Size(58, 19)
		Me.b_soft_RadioButton.TabIndex = 2
		Me.b_soft_RadioButton.TabStop = True
		Me.b_soft_RadioButton.Text = "軟版"
		Me.b_soft_RadioButton.UseVisualStyleBackColor = True
		'
		'b_hard_RadioButton
		'
		Me.b_hard_RadioButton.AutoSize = True
		Me.b_hard_RadioButton.ForeColor = System.Drawing.Color.Red
		Me.b_hard_RadioButton.Location = New System.Drawing.Point(238, 37)
		Me.b_hard_RadioButton.Margin = New System.Windows.Forms.Padding(4)
		Me.b_hard_RadioButton.Name = "b_hard_RadioButton"
		Me.b_hard_RadioButton.Size = New System.Drawing.Size(58, 19)
		Me.b_hard_RadioButton.TabIndex = 3
		Me.b_hard_RadioButton.Text = "硬版"
		Me.b_hard_RadioButton.UseVisualStyleBackColor = True
		'
		'b_ProcessTypeCode_TextBox
		'
		Me.b_ProcessTypeCode_TextBox.Location = New System.Drawing.Point(304, 37)
		Me.b_ProcessTypeCode_TextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.b_ProcessTypeCode_TextBox.Name = "b_ProcessTypeCode_TextBox"
		Me.b_ProcessTypeCode_TextBox.ReadOnly = True
		Me.b_ProcessTypeCode_TextBox.Size = New System.Drawing.Size(808, 25)
		Me.b_ProcessTypeCode_TextBox.TabIndex = 4
		Me.b_ProcessTypeCode_TextBox.Text = "M-Soft"
		'
		'Label85
		'
		Me.Label85.Location = New System.Drawing.Point(4, 76)
		Me.Label85.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label85.Name = "Label85"
		Me.Label85.Size = New System.Drawing.Size(160, 20)
		Me.Label85.TabIndex = 46
		Me.Label85.Text = "密度 Kg / m^3"
		'
		'b_Density_TextBox
		'
		Me.b_Density_TextBox.Location = New System.Drawing.Point(172, 70)
		Me.b_Density_TextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.b_Density_TextBox.Name = "b_Density_TextBox"
		Me.b_Density_TextBox.Size = New System.Drawing.Size(940, 25)
		Me.b_Density_TextBox.TabIndex = 5
		'
		'Label21
		'
		Me.Label21.Location = New System.Drawing.Point(4, 109)
		Me.Label21.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label21.Name = "Label21"
		Me.Label21.Size = New System.Drawing.Size(160, 20)
		Me.Label21.TabIndex = 2
		Me.Label21.Text = "單位"
		'
		'b_UnitCode_TextBox
		'
		Me.b_UnitCode_TextBox.Location = New System.Drawing.Point(172, 103)
		Me.b_UnitCode_TextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.b_UnitCode_TextBox.Name = "b_UnitCode_TextBox"
		Me.b_UnitCode_TextBox.Size = New System.Drawing.Size(940, 25)
		Me.b_UnitCode_TextBox.TabIndex = 6
		'
		'Label23
		'
		Me.Label23.Location = New System.Drawing.Point(4, 142)
		Me.Label23.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label23.Name = "Label23"
		Me.Label23.Size = New System.Drawing.Size(160, 20)
		Me.Label23.TabIndex = 5
		Me.Label23.Text = "水足跡 m^3/Kg (WFN)"
		'
		'b_WFP_TextBox
		'
		Me.b_WFP_TextBox.Location = New System.Drawing.Point(172, 136)
		Me.b_WFP_TextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.b_WFP_TextBox.Name = "b_WFP_TextBox"
		Me.b_WFP_TextBox.Size = New System.Drawing.Size(940, 25)
		Me.b_WFP_TextBox.TabIndex = 7
		'
		'Label75
		'
		Me.Label75.Location = New System.Drawing.Point(4, 175)
		Me.Label75.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label75.Name = "Label75"
		Me.Label75.Size = New System.Drawing.Size(160, 20)
		Me.Label75.TabIndex = 43
		Me.Label75.Text = "水足跡 m^3/Kg (ISO)"
		'
		'b_ISO_TextBox
		'
		Me.b_ISO_TextBox.Location = New System.Drawing.Point(172, 169)
		Me.b_ISO_TextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.b_ISO_TextBox.Name = "b_ISO_TextBox"
		Me.b_ISO_TextBox.Size = New System.Drawing.Size(940, 25)
		Me.b_ISO_TextBox.TabIndex = 8
		'
		'Label76
		'
		Me.Label76.Location = New System.Drawing.Point(4, 208)
		Me.Label76.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label76.Name = "Label76"
		Me.Label76.Size = New System.Drawing.Size(160, 20)
		Me.Label76.TabIndex = 45
		Me.Label76.Text = "BOD mg/Kg"
		'
		'b_BOD_TextBox
		'
		Me.b_BOD_TextBox.Location = New System.Drawing.Point(172, 202)
		Me.b_BOD_TextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.b_BOD_TextBox.Name = "b_BOD_TextBox"
		Me.b_BOD_TextBox.Size = New System.Drawing.Size(940, 25)
		Me.b_BOD_TextBox.TabIndex = 9
		'
		'Label25
		'
		Me.Label25.Image = Global.WinAP.WFE.My.Resources.Resources.blue_water_16
		Me.Label25.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.Label25.Location = New System.Drawing.Point(4, 241)
		Me.Label25.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label25.Name = "Label25"
		Me.Label25.Size = New System.Drawing.Size(160, 20)
		Me.Label25.TabIndex = 8
		Me.Label25.Text = "藍水 m^3/Kg"
		Me.Label25.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		'
		'b_BlueWater_TextBox
		'
		Me.b_BlueWater_TextBox.Location = New System.Drawing.Point(172, 235)
		Me.b_BlueWater_TextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.b_BlueWater_TextBox.Name = "b_BlueWater_TextBox"
		Me.b_BlueWater_TextBox.Size = New System.Drawing.Size(940, 25)
		Me.b_BlueWater_TextBox.TabIndex = 10
		'
		'Label38
		'
		Me.Label38.Image = Global.WinAP.WFE.My.Resources.Resources.green_water_16
		Me.Label38.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.Label38.Location = New System.Drawing.Point(4, 274)
		Me.Label38.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label38.Name = "Label38"
		Me.Label38.Size = New System.Drawing.Size(160, 20)
		Me.Label38.TabIndex = 11
		Me.Label38.Text = "綠水 m^3/Kg"
		Me.Label38.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		'
		'b_GreenWater_TextBox
		'
		Me.b_GreenWater_TextBox.Location = New System.Drawing.Point(172, 268)
		Me.b_GreenWater_TextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.b_GreenWater_TextBox.Name = "b_GreenWater_TextBox"
		Me.b_GreenWater_TextBox.Size = New System.Drawing.Size(940, 25)
		Me.b_GreenWater_TextBox.TabIndex = 11
		'
		'Label40
		'
		Me.Label40.Image = Global.WinAP.WFE.My.Resources.Resources.gray_water_16
		Me.Label40.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.Label40.Location = New System.Drawing.Point(4, 307)
		Me.Label40.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label40.Name = "Label40"
		Me.Label40.Size = New System.Drawing.Size(160, 20)
		Me.Label40.TabIndex = 14
		Me.Label40.Text = "灰水 m^3/Kg"
		Me.Label40.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		'
		'b_GrayWater_TextBox
		'
		Me.b_GrayWater_TextBox.Location = New System.Drawing.Point(172, 301)
		Me.b_GrayWater_TextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.b_GrayWater_TextBox.Name = "b_GrayWater_TextBox"
		Me.b_GrayWater_TextBox.Size = New System.Drawing.Size(940, 25)
		Me.b_GrayWater_TextBox.TabIndex = 12
		'
		'Label42
		'
		Me.Label42.Location = New System.Drawing.Point(4, 340)
		Me.Label42.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label42.Name = "Label42"
		Me.Label42.Size = New System.Drawing.Size(160, 20)
		Me.Label42.TabIndex = 30
		Me.Label42.Text = "資料來源"
		'
		'b_DataSource_TextBox
		'
		Me.b_DataSource_TextBox.Location = New System.Drawing.Point(172, 334)
		Me.b_DataSource_TextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.b_DataSource_TextBox.Name = "b_DataSource_TextBox"
		Me.b_DataSource_TextBox.Size = New System.Drawing.Size(940, 25)
		Me.b_DataSource_TextBox.TabIndex = 13
		'
		'FlowLayoutPanel18
		'
		Me.FlowLayoutPanel18.Controls.Add(Me.b_Insert_Button)
		Me.FlowLayoutPanel18.Controls.Add(Me.b_Update_Button)
		Me.FlowLayoutPanel18.Controls.Add(Me.b_Delete_Button)
		Me.FlowLayoutPanel18.Location = New System.Drawing.Point(4, 367)
		Me.FlowLayoutPanel18.Margin = New System.Windows.Forms.Padding(4)
		Me.FlowLayoutPanel18.Name = "FlowLayoutPanel18"
		Me.FlowLayoutPanel18.Size = New System.Drawing.Size(1165, 45)
		Me.FlowLayoutPanel18.TabIndex = 20
		'
		'b_Insert_Button
		'
		Me.b_Insert_Button.ForeColor = System.Drawing.Color.Blue
		Me.b_Insert_Button.Location = New System.Drawing.Point(2, 4)
		Me.b_Insert_Button.Margin = New System.Windows.Forms.Padding(2, 4, 4, 4)
		Me.b_Insert_Button.Name = "b_Insert_Button"
		Me.b_Insert_Button.Size = New System.Drawing.Size(380, 38)
		Me.b_Insert_Button.TabIndex = 21
		Me.b_Insert_Button.Tag = "Insert"
		Me.b_Insert_Button.Text = "新增"
		Me.b_Insert_Button.UseVisualStyleBackColor = True
		'
		'b_Update_Button
		'
		Me.b_Update_Button.ForeColor = System.Drawing.Color.DarkGreen
		Me.b_Update_Button.Location = New System.Drawing.Point(390, 4)
		Me.b_Update_Button.Margin = New System.Windows.Forms.Padding(4)
		Me.b_Update_Button.Name = "b_Update_Button"
		Me.b_Update_Button.Size = New System.Drawing.Size(375, 38)
		Me.b_Update_Button.TabIndex = 22
		Me.b_Update_Button.Tag = "Update"
		Me.b_Update_Button.Text = "更新"
		Me.b_Update_Button.UseVisualStyleBackColor = True
		'
		'b_Delete_Button
		'
		Me.b_Delete_Button.ForeColor = System.Drawing.Color.Red
		Me.b_Delete_Button.Location = New System.Drawing.Point(773, 4)
		Me.b_Delete_Button.Margin = New System.Windows.Forms.Padding(4)
		Me.b_Delete_Button.Name = "b_Delete_Button"
		Me.b_Delete_Button.Size = New System.Drawing.Size(335, 38)
		Me.b_Delete_Button.TabIndex = 23
		Me.b_Delete_Button.Tag = "Delete"
		Me.b_Delete_Button.Text = "刪除"
		Me.b_Delete_Button.UseVisualStyleBackColor = True
		'
		'Panel5
		'
		Me.Panel5.Controls.Add(Me.b_DataGridView)
		Me.Panel5.Controls.Add(Me.FlowLayoutPanel20)
		Me.Panel5.Location = New System.Drawing.Point(4, 420)
		Me.Panel5.Margin = New System.Windows.Forms.Padding(4)
		Me.Panel5.Name = "Panel5"
		Me.Panel5.Size = New System.Drawing.Size(1165, 485)
		Me.Panel5.TabIndex = 21
		'
		'b_DataGridView
		'
		Me.b_DataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
		Me.b_DataGridView.Dock = System.Windows.Forms.DockStyle.Fill
		Me.b_DataGridView.Location = New System.Drawing.Point(0, 69)
		Me.b_DataGridView.Margin = New System.Windows.Forms.Padding(4)
		Me.b_DataGridView.Name = "b_DataGridView"
		Me.b_DataGridView.RowTemplate.Height = 24
		Me.b_DataGridView.Size = New System.Drawing.Size(1165, 416)
		Me.b_DataGridView.TabIndex = 41
		'
		'FlowLayoutPanel20
		'
		Me.FlowLayoutPanel20.Controls.Add(Me.Label47)
		Me.FlowLayoutPanel20.Controls.Add(Me.b_NameFilter_TextBox)
		Me.FlowLayoutPanel20.Controls.Add(Me.b_Clean_Button)
		Me.FlowLayoutPanel20.Controls.Add(Me.Label62)
		Me.FlowLayoutPanel20.Controls.Add(Me.b_FilterInfo_Label)
		Me.FlowLayoutPanel20.Dock = System.Windows.Forms.DockStyle.Top
		Me.FlowLayoutPanel20.Location = New System.Drawing.Point(0, 0)
		Me.FlowLayoutPanel20.Margin = New System.Windows.Forms.Padding(4)
		Me.FlowLayoutPanel20.Name = "FlowLayoutPanel20"
		Me.FlowLayoutPanel20.Size = New System.Drawing.Size(1165, 69)
		Me.FlowLayoutPanel20.TabIndex = 4
		'
		'Label47
		'
		Me.Label47.Location = New System.Drawing.Point(2, 10)
		Me.Label47.Margin = New System.Windows.Forms.Padding(2, 10, 4, 0)
		Me.Label47.Name = "Label47"
		Me.Label47.Size = New System.Drawing.Size(160, 20)
		Me.Label47.TabIndex = 0
		Me.Label47.Text = "製程名稱篩選"
		'
		'b_NameFilter_TextBox
		'
		Me.b_NameFilter_TextBox.Location = New System.Drawing.Point(170, 4)
		Me.b_NameFilter_TextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.b_NameFilter_TextBox.Name = "b_NameFilter_TextBox"
		Me.b_NameFilter_TextBox.Size = New System.Drawing.Size(745, 25)
		Me.b_NameFilter_TextBox.TabIndex = 31
		'
		'b_Clean_Button
		'
		Me.b_Clean_Button.Location = New System.Drawing.Point(923, 4)
		Me.b_Clean_Button.Margin = New System.Windows.Forms.Padding(4)
		Me.b_Clean_Button.Name = "b_Clean_Button"
		Me.b_Clean_Button.Size = New System.Drawing.Size(185, 29)
		Me.b_Clean_Button.TabIndex = 32
		Me.b_Clean_Button.Text = "清除篩選"
		Me.b_Clean_Button.UseVisualStyleBackColor = True
		'
		'Label62
		'
		Me.Label62.AutoSize = True
		Me.Label62.ForeColor = System.Drawing.SystemColors.Highlight
		Me.Label62.Location = New System.Drawing.Point(2, 47)
		Me.Label62.Margin = New System.Windows.Forms.Padding(2, 10, 4, 0)
		Me.Label62.Name = "Label62"
		Me.Label62.Size = New System.Drawing.Size(124, 15)
		Me.Label62.TabIndex = 5
		Me.Label62.Text = "篩選筆數 / 總筆數"
		'
		'b_FilterInfo_Label
		'
		Me.b_FilterInfo_Label.AutoSize = True
		Me.b_FilterInfo_Label.ForeColor = System.Drawing.Color.Red
		Me.b_FilterInfo_Label.Location = New System.Drawing.Point(134, 47)
		Me.b_FilterInfo_Label.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.b_FilterInfo_Label.Name = "b_FilterInfo_Label"
		Me.b_FilterInfo_Label.Size = New System.Drawing.Size(31, 15)
		Me.b_FilterInfo_Label.TabIndex = 6
		Me.b_FilterInfo_Label.Text = "N/A"
		'
		'TabPage6
		'
		Me.TabPage6.Controls.Add(Me.FlowLayoutPanel21)
		Me.TabPage6.ImageIndex = 5
		Me.TabPage6.Location = New System.Drawing.Point(4, 25)
		Me.TabPage6.Margin = New System.Windows.Forms.Padding(4)
		Me.TabPage6.Name = "TabPage6"
		Me.TabPage6.Size = New System.Drawing.Size(1184, 924)
		Me.TabPage6.TabIndex = 5
		Me.TabPage6.Text = "液晶顯示器"
		Me.TabPage6.UseVisualStyleBackColor = True
		'
		'FlowLayoutPanel21
		'
		Me.FlowLayoutPanel21.Controls.Add(Me.Label48)
		Me.FlowLayoutPanel21.Controls.Add(Me.d_ProcessName_TextBox)
		Me.FlowLayoutPanel21.Controls.Add(Me.Label63)
		Me.FlowLayoutPanel21.Controls.Add(Me.d_ProcessTypeCode_TextBox)
		Me.FlowLayoutPanel21.Controls.Add(Me.Label86)
		Me.FlowLayoutPanel21.Controls.Add(Me.d_Density_TextBox)
		Me.FlowLayoutPanel21.Controls.Add(Me.Label49)
		Me.FlowLayoutPanel21.Controls.Add(Me.d_UnitCode_TextBox)
		Me.FlowLayoutPanel21.Controls.Add(Me.Label50)
		Me.FlowLayoutPanel21.Controls.Add(Me.d_WFP_TextBox)
		Me.FlowLayoutPanel21.Controls.Add(Me.Label77)
		Me.FlowLayoutPanel21.Controls.Add(Me.d_ISO_TextBox)
		Me.FlowLayoutPanel21.Controls.Add(Me.Label78)
		Me.FlowLayoutPanel21.Controls.Add(Me.d_BOD_TextBox)
		Me.FlowLayoutPanel21.Controls.Add(Me.Label51)
		Me.FlowLayoutPanel21.Controls.Add(Me.d_BlueWater_TextBox)
		Me.FlowLayoutPanel21.Controls.Add(Me.Label52)
		Me.FlowLayoutPanel21.Controls.Add(Me.d_GreenWater_TextBox)
		Me.FlowLayoutPanel21.Controls.Add(Me.Label53)
		Me.FlowLayoutPanel21.Controls.Add(Me.d_GrayWater_TextBox)
		Me.FlowLayoutPanel21.Controls.Add(Me.Label54)
		Me.FlowLayoutPanel21.Controls.Add(Me.d_DataSource_TextBox)
		Me.FlowLayoutPanel21.Controls.Add(Me.FlowLayoutPanel22)
		Me.FlowLayoutPanel21.Controls.Add(Me.Panel6)
		Me.FlowLayoutPanel21.Dock = System.Windows.Forms.DockStyle.Fill
		Me.FlowLayoutPanel21.Location = New System.Drawing.Point(0, 0)
		Me.FlowLayoutPanel21.Margin = New System.Windows.Forms.Padding(4)
		Me.FlowLayoutPanel21.Name = "FlowLayoutPanel21"
		Me.FlowLayoutPanel21.Size = New System.Drawing.Size(1184, 924)
		Me.FlowLayoutPanel21.TabIndex = 5
		'
		'Label48
		'
		Me.Label48.Location = New System.Drawing.Point(4, 10)
		Me.Label48.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label48.Name = "Label48"
		Me.Label48.Size = New System.Drawing.Size(160, 20)
		Me.Label48.TabIndex = 0
		Me.Label48.Text = "製程名稱"
		'
		'd_ProcessName_TextBox
		'
		Me.d_ProcessName_TextBox.Location = New System.Drawing.Point(172, 4)
		Me.d_ProcessName_TextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.d_ProcessName_TextBox.Name = "d_ProcessName_TextBox"
		Me.d_ProcessName_TextBox.Size = New System.Drawing.Size(940, 25)
		Me.d_ProcessName_TextBox.TabIndex = 1
		'
		'Label63
		'
		Me.Label63.Location = New System.Drawing.Point(4, 43)
		Me.Label63.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label63.Name = "Label63"
		Me.Label63.Size = New System.Drawing.Size(160, 20)
		Me.Label63.TabIndex = 37
		Me.Label63.Text = "製程類別代碼"
		'
		'd_ProcessTypeCode_TextBox
		'
		Me.d_ProcessTypeCode_TextBox.Location = New System.Drawing.Point(172, 37)
		Me.d_ProcessTypeCode_TextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.d_ProcessTypeCode_TextBox.Name = "d_ProcessTypeCode_TextBox"
		Me.d_ProcessTypeCode_TextBox.ReadOnly = True
		Me.d_ProcessTypeCode_TextBox.Size = New System.Drawing.Size(940, 25)
		Me.d_ProcessTypeCode_TextBox.TabIndex = 2
		Me.d_ProcessTypeCode_TextBox.Text = "LCD"
		'
		'Label86
		'
		Me.Label86.Location = New System.Drawing.Point(4, 76)
		Me.Label86.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label86.Name = "Label86"
		Me.Label86.Size = New System.Drawing.Size(160, 20)
		Me.Label86.TabIndex = 46
		Me.Label86.Text = "密度 Kg / m^3"
		'
		'd_Density_TextBox
		'
		Me.d_Density_TextBox.Location = New System.Drawing.Point(172, 70)
		Me.d_Density_TextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.d_Density_TextBox.Name = "d_Density_TextBox"
		Me.d_Density_TextBox.Size = New System.Drawing.Size(940, 25)
		Me.d_Density_TextBox.TabIndex = 3
		'
		'Label49
		'
		Me.Label49.Location = New System.Drawing.Point(4, 109)
		Me.Label49.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label49.Name = "Label49"
		Me.Label49.Size = New System.Drawing.Size(160, 20)
		Me.Label49.TabIndex = 2
		Me.Label49.Text = "單位"
		'
		'd_UnitCode_TextBox
		'
		Me.d_UnitCode_TextBox.Location = New System.Drawing.Point(172, 103)
		Me.d_UnitCode_TextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.d_UnitCode_TextBox.Name = "d_UnitCode_TextBox"
		Me.d_UnitCode_TextBox.Size = New System.Drawing.Size(940, 25)
		Me.d_UnitCode_TextBox.TabIndex = 4
		'
		'Label50
		'
		Me.Label50.Location = New System.Drawing.Point(4, 142)
		Me.Label50.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label50.Name = "Label50"
		Me.Label50.Size = New System.Drawing.Size(160, 20)
		Me.Label50.TabIndex = 5
		Me.Label50.Text = "水足跡 m^3/Kg (WFN)"
		'
		'd_WFP_TextBox
		'
		Me.d_WFP_TextBox.Location = New System.Drawing.Point(172, 136)
		Me.d_WFP_TextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.d_WFP_TextBox.Name = "d_WFP_TextBox"
		Me.d_WFP_TextBox.Size = New System.Drawing.Size(940, 25)
		Me.d_WFP_TextBox.TabIndex = 5
		'
		'Label77
		'
		Me.Label77.Location = New System.Drawing.Point(4, 175)
		Me.Label77.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label77.Name = "Label77"
		Me.Label77.Size = New System.Drawing.Size(160, 20)
		Me.Label77.TabIndex = 43
		Me.Label77.Text = "水足跡 m^3/Kg (ISO)"
		'
		'd_ISO_TextBox
		'
		Me.d_ISO_TextBox.Location = New System.Drawing.Point(172, 169)
		Me.d_ISO_TextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.d_ISO_TextBox.Name = "d_ISO_TextBox"
		Me.d_ISO_TextBox.Size = New System.Drawing.Size(940, 25)
		Me.d_ISO_TextBox.TabIndex = 6
		'
		'Label78
		'
		Me.Label78.Location = New System.Drawing.Point(4, 208)
		Me.Label78.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label78.Name = "Label78"
		Me.Label78.Size = New System.Drawing.Size(160, 20)
		Me.Label78.TabIndex = 45
		Me.Label78.Text = "BOD mg/Kg"
		'
		'd_BOD_TextBox
		'
		Me.d_BOD_TextBox.Location = New System.Drawing.Point(172, 202)
		Me.d_BOD_TextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.d_BOD_TextBox.Name = "d_BOD_TextBox"
		Me.d_BOD_TextBox.Size = New System.Drawing.Size(940, 25)
		Me.d_BOD_TextBox.TabIndex = 7
		'
		'Label51
		'
		Me.Label51.Image = Global.WinAP.WFE.My.Resources.Resources.blue_water_16
		Me.Label51.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.Label51.Location = New System.Drawing.Point(4, 241)
		Me.Label51.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label51.Name = "Label51"
		Me.Label51.Size = New System.Drawing.Size(160, 20)
		Me.Label51.TabIndex = 8
		Me.Label51.Text = "藍水 m^3/Kg"
		Me.Label51.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		'
		'd_BlueWater_TextBox
		'
		Me.d_BlueWater_TextBox.Location = New System.Drawing.Point(172, 235)
		Me.d_BlueWater_TextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.d_BlueWater_TextBox.Name = "d_BlueWater_TextBox"
		Me.d_BlueWater_TextBox.Size = New System.Drawing.Size(940, 25)
		Me.d_BlueWater_TextBox.TabIndex = 8
		'
		'Label52
		'
		Me.Label52.Image = Global.WinAP.WFE.My.Resources.Resources.green_water_16
		Me.Label52.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.Label52.Location = New System.Drawing.Point(4, 274)
		Me.Label52.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label52.Name = "Label52"
		Me.Label52.Size = New System.Drawing.Size(160, 20)
		Me.Label52.TabIndex = 11
		Me.Label52.Text = "綠水 m^3/Kg"
		Me.Label52.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		'
		'd_GreenWater_TextBox
		'
		Me.d_GreenWater_TextBox.Location = New System.Drawing.Point(172, 268)
		Me.d_GreenWater_TextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.d_GreenWater_TextBox.Name = "d_GreenWater_TextBox"
		Me.d_GreenWater_TextBox.Size = New System.Drawing.Size(940, 25)
		Me.d_GreenWater_TextBox.TabIndex = 9
		'
		'Label53
		'
		Me.Label53.Image = Global.WinAP.WFE.My.Resources.Resources.gray_water_16
		Me.Label53.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.Label53.Location = New System.Drawing.Point(4, 307)
		Me.Label53.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label53.Name = "Label53"
		Me.Label53.Size = New System.Drawing.Size(160, 20)
		Me.Label53.TabIndex = 14
		Me.Label53.Text = "灰水 m^3/Kg"
		Me.Label53.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		'
		'd_GrayWater_TextBox
		'
		Me.d_GrayWater_TextBox.Location = New System.Drawing.Point(172, 301)
		Me.d_GrayWater_TextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.d_GrayWater_TextBox.Name = "d_GrayWater_TextBox"
		Me.d_GrayWater_TextBox.Size = New System.Drawing.Size(940, 25)
		Me.d_GrayWater_TextBox.TabIndex = 10
		'
		'Label54
		'
		Me.Label54.Location = New System.Drawing.Point(4, 340)
		Me.Label54.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label54.Name = "Label54"
		Me.Label54.Size = New System.Drawing.Size(160, 20)
		Me.Label54.TabIndex = 30
		Me.Label54.Text = "資料來源"
		'
		'd_DataSource_TextBox
		'
		Me.d_DataSource_TextBox.Location = New System.Drawing.Point(172, 334)
		Me.d_DataSource_TextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.d_DataSource_TextBox.Name = "d_DataSource_TextBox"
		Me.d_DataSource_TextBox.Size = New System.Drawing.Size(940, 25)
		Me.d_DataSource_TextBox.TabIndex = 11
		'
		'FlowLayoutPanel22
		'
		Me.FlowLayoutPanel22.Controls.Add(Me.d_Insert_Button)
		Me.FlowLayoutPanel22.Controls.Add(Me.d_Update_Button)
		Me.FlowLayoutPanel22.Controls.Add(Me.d_Delete_Button)
		Me.FlowLayoutPanel22.Location = New System.Drawing.Point(4, 367)
		Me.FlowLayoutPanel22.Margin = New System.Windows.Forms.Padding(4)
		Me.FlowLayoutPanel22.Name = "FlowLayoutPanel22"
		Me.FlowLayoutPanel22.Size = New System.Drawing.Size(1165, 45)
		Me.FlowLayoutPanel22.TabIndex = 20
		'
		'd_Insert_Button
		'
		Me.d_Insert_Button.ForeColor = System.Drawing.Color.Blue
		Me.d_Insert_Button.Location = New System.Drawing.Point(2, 4)
		Me.d_Insert_Button.Margin = New System.Windows.Forms.Padding(2, 4, 4, 4)
		Me.d_Insert_Button.Name = "d_Insert_Button"
		Me.d_Insert_Button.Size = New System.Drawing.Size(380, 38)
		Me.d_Insert_Button.TabIndex = 21
		Me.d_Insert_Button.Tag = "Insert"
		Me.d_Insert_Button.Text = "新增"
		Me.d_Insert_Button.UseVisualStyleBackColor = True
		'
		'd_Update_Button
		'
		Me.d_Update_Button.ForeColor = System.Drawing.Color.DarkGreen
		Me.d_Update_Button.Location = New System.Drawing.Point(390, 4)
		Me.d_Update_Button.Margin = New System.Windows.Forms.Padding(4)
		Me.d_Update_Button.Name = "d_Update_Button"
		Me.d_Update_Button.Size = New System.Drawing.Size(375, 38)
		Me.d_Update_Button.TabIndex = 22
		Me.d_Update_Button.Tag = "Update"
		Me.d_Update_Button.Text = "更新"
		Me.d_Update_Button.UseVisualStyleBackColor = True
		'
		'd_Delete_Button
		'
		Me.d_Delete_Button.ForeColor = System.Drawing.Color.Red
		Me.d_Delete_Button.Location = New System.Drawing.Point(773, 4)
		Me.d_Delete_Button.Margin = New System.Windows.Forms.Padding(4)
		Me.d_Delete_Button.Name = "d_Delete_Button"
		Me.d_Delete_Button.Size = New System.Drawing.Size(335, 38)
		Me.d_Delete_Button.TabIndex = 23
		Me.d_Delete_Button.Tag = "Delete"
		Me.d_Delete_Button.Text = "刪除"
		Me.d_Delete_Button.UseVisualStyleBackColor = True
		'
		'Panel6
		'
		Me.Panel6.Controls.Add(Me.d_DataGridView)
		Me.Panel6.Controls.Add(Me.FlowLayoutPanel24)
		Me.Panel6.Location = New System.Drawing.Point(4, 420)
		Me.Panel6.Margin = New System.Windows.Forms.Padding(4)
		Me.Panel6.Name = "Panel6"
		Me.Panel6.Size = New System.Drawing.Size(1165, 485)
		Me.Panel6.TabIndex = 21
		'
		'd_DataGridView
		'
		Me.d_DataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
		Me.d_DataGridView.Dock = System.Windows.Forms.DockStyle.Fill
		Me.d_DataGridView.Location = New System.Drawing.Point(0, 69)
		Me.d_DataGridView.Margin = New System.Windows.Forms.Padding(4)
		Me.d_DataGridView.Name = "d_DataGridView"
		Me.d_DataGridView.RowTemplate.Height = 24
		Me.d_DataGridView.Size = New System.Drawing.Size(1165, 416)
		Me.d_DataGridView.TabIndex = 41
		'
		'FlowLayoutPanel24
		'
		Me.FlowLayoutPanel24.Controls.Add(Me.Label55)
		Me.FlowLayoutPanel24.Controls.Add(Me.d_NameFilter_TextBox)
		Me.FlowLayoutPanel24.Controls.Add(Me.d_Clean_Button)
		Me.FlowLayoutPanel24.Controls.Add(Me.Label64)
		Me.FlowLayoutPanel24.Controls.Add(Me.d_FilterInfo_Label)
		Me.FlowLayoutPanel24.Dock = System.Windows.Forms.DockStyle.Top
		Me.FlowLayoutPanel24.Location = New System.Drawing.Point(0, 0)
		Me.FlowLayoutPanel24.Margin = New System.Windows.Forms.Padding(4)
		Me.FlowLayoutPanel24.Name = "FlowLayoutPanel24"
		Me.FlowLayoutPanel24.Size = New System.Drawing.Size(1165, 69)
		Me.FlowLayoutPanel24.TabIndex = 4
		'
		'Label55
		'
		Me.Label55.Location = New System.Drawing.Point(2, 10)
		Me.Label55.Margin = New System.Windows.Forms.Padding(2, 10, 4, 0)
		Me.Label55.Name = "Label55"
		Me.Label55.Size = New System.Drawing.Size(160, 20)
		Me.Label55.TabIndex = 0
		Me.Label55.Text = "製程名稱篩選"
		'
		'd_NameFilter_TextBox
		'
		Me.d_NameFilter_TextBox.Location = New System.Drawing.Point(170, 4)
		Me.d_NameFilter_TextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.d_NameFilter_TextBox.Name = "d_NameFilter_TextBox"
		Me.d_NameFilter_TextBox.Size = New System.Drawing.Size(745, 25)
		Me.d_NameFilter_TextBox.TabIndex = 31
		'
		'd_Clean_Button
		'
		Me.d_Clean_Button.Location = New System.Drawing.Point(923, 4)
		Me.d_Clean_Button.Margin = New System.Windows.Forms.Padding(4)
		Me.d_Clean_Button.Name = "d_Clean_Button"
		Me.d_Clean_Button.Size = New System.Drawing.Size(185, 29)
		Me.d_Clean_Button.TabIndex = 32
		Me.d_Clean_Button.Text = "清除篩選"
		Me.d_Clean_Button.UseVisualStyleBackColor = True
		'
		'Label64
		'
		Me.Label64.AutoSize = True
		Me.Label64.ForeColor = System.Drawing.SystemColors.Highlight
		Me.Label64.Location = New System.Drawing.Point(2, 47)
		Me.Label64.Margin = New System.Windows.Forms.Padding(2, 10, 4, 0)
		Me.Label64.Name = "Label64"
		Me.Label64.Size = New System.Drawing.Size(124, 15)
		Me.Label64.TabIndex = 5
		Me.Label64.Text = "篩選筆數 / 總筆數"
		'
		'd_FilterInfo_Label
		'
		Me.d_FilterInfo_Label.AutoSize = True
		Me.d_FilterInfo_Label.ForeColor = System.Drawing.Color.Red
		Me.d_FilterInfo_Label.Location = New System.Drawing.Point(134, 47)
		Me.d_FilterInfo_Label.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.d_FilterInfo_Label.Name = "d_FilterInfo_Label"
		Me.d_FilterInfo_Label.Size = New System.Drawing.Size(31, 15)
		Me.d_FilterInfo_Label.TabIndex = 6
		Me.d_FilterInfo_Label.Text = "N/A"
		'
		'TabPage7
		'
		Me.TabPage7.Controls.Add(Me.FlowLayoutPanel4)
		Me.TabPage7.ImageIndex = 6
		Me.TabPage7.Location = New System.Drawing.Point(4, 25)
		Me.TabPage7.Name = "TabPage7"
		Me.TabPage7.Padding = New System.Windows.Forms.Padding(3)
		Me.TabPage7.Size = New System.Drawing.Size(1184, 924)
		Me.TabPage7.TabIndex = 6
		Me.TabPage7.Text = "組裝方式"
		Me.TabPage7.ToolTipText = "組裝方式"
		Me.TabPage7.UseVisualStyleBackColor = True
		'
		'FlowLayoutPanel4
		'
		Me.FlowLayoutPanel4.Controls.Add(Me.Label79)
		Me.FlowLayoutPanel4.Controls.Add(Me.asm_AssemblyMethodName_TextBox)
		Me.FlowLayoutPanel4.Controls.Add(Me.Label81)
		Me.FlowLayoutPanel4.Controls.Add(Me.asm_CalculateUnit_TextBox)
		Me.FlowLayoutPanel4.Controls.Add(Me.Label82)
		Me.FlowLayoutPanel4.Controls.Add(Me.asm_PowerConsumption_TextBox)
		Me.FlowLayoutPanel4.Controls.Add(Me.Label83)
		Me.FlowLayoutPanel4.Controls.Add(Me.asm_PowerCoefficient_TextBox)
		Me.FlowLayoutPanel4.Controls.Add(Me.Label88)
		Me.FlowLayoutPanel4.Controls.Add(Me.asm_DataSource_TextBox)
		Me.FlowLayoutPanel4.Controls.Add(Me.Label80)
		Me.FlowLayoutPanel4.Controls.Add(Me.asm_Remark_TextBox)
		Me.FlowLayoutPanel4.Controls.Add(Me.FlowLayoutPanel7)
		Me.FlowLayoutPanel4.Controls.Add(Me.Panel7)
		Me.FlowLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill
		Me.FlowLayoutPanel4.Location = New System.Drawing.Point(3, 3)
		Me.FlowLayoutPanel4.Margin = New System.Windows.Forms.Padding(4)
		Me.FlowLayoutPanel4.Name = "FlowLayoutPanel4"
		Me.FlowLayoutPanel4.Size = New System.Drawing.Size(1178, 918)
		Me.FlowLayoutPanel4.TabIndex = 6
		'
		'Label79
		'
		Me.Label79.Location = New System.Drawing.Point(4, 10)
		Me.Label79.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label79.Name = "Label79"
		Me.Label79.Size = New System.Drawing.Size(160, 20)
		Me.Label79.TabIndex = 0
		Me.Label79.Text = "組裝方式名稱"
		'
		'asm_AssemblyMethodName_TextBox
		'
		Me.asm_AssemblyMethodName_TextBox.Location = New System.Drawing.Point(172, 4)
		Me.asm_AssemblyMethodName_TextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.asm_AssemblyMethodName_TextBox.Name = "asm_AssemblyMethodName_TextBox"
		Me.asm_AssemblyMethodName_TextBox.Size = New System.Drawing.Size(940, 25)
		Me.asm_AssemblyMethodName_TextBox.TabIndex = 1
		'
		'Label81
		'
		Me.Label81.Location = New System.Drawing.Point(4, 43)
		Me.Label81.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label81.Name = "Label81"
		Me.Label81.Size = New System.Drawing.Size(160, 20)
		Me.Label81.TabIndex = 2
		Me.Label81.Text = "單位"
		'
		'asm_CalculateUnit_TextBox
		'
		Me.asm_CalculateUnit_TextBox.Location = New System.Drawing.Point(172, 37)
		Me.asm_CalculateUnit_TextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.asm_CalculateUnit_TextBox.Name = "asm_CalculateUnit_TextBox"
		Me.asm_CalculateUnit_TextBox.Size = New System.Drawing.Size(940, 25)
		Me.asm_CalculateUnit_TextBox.TabIndex = 2
		'
		'Label82
		'
		Me.Label82.Location = New System.Drawing.Point(4, 76)
		Me.Label82.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label82.Name = "Label82"
		Me.Label82.Size = New System.Drawing.Size(160, 20)
		Me.Label82.TabIndex = 5
		Me.Label82.Text = "電力消耗量 Kw"
		'
		'asm_PowerConsumption_TextBox
		'
		Me.asm_PowerConsumption_TextBox.Location = New System.Drawing.Point(172, 70)
		Me.asm_PowerConsumption_TextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.asm_PowerConsumption_TextBox.Name = "asm_PowerConsumption_TextBox"
		Me.asm_PowerConsumption_TextBox.Size = New System.Drawing.Size(940, 25)
		Me.asm_PowerConsumption_TextBox.TabIndex = 3
		'
		'Label83
		'
		Me.Label83.Location = New System.Drawing.Point(4, 109)
		Me.Label83.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label83.Name = "Label83"
		Me.Label83.Size = New System.Drawing.Size(160, 20)
		Me.Label83.TabIndex = 43
		Me.Label83.Text = "電力係數 m^3/Kw ．hr"
		'
		'asm_PowerCoefficient_TextBox
		'
		Me.asm_PowerCoefficient_TextBox.Location = New System.Drawing.Point(172, 103)
		Me.asm_PowerCoefficient_TextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.asm_PowerCoefficient_TextBox.Name = "asm_PowerCoefficient_TextBox"
		Me.asm_PowerCoefficient_TextBox.Size = New System.Drawing.Size(940, 25)
		Me.asm_PowerCoefficient_TextBox.TabIndex = 4
		'
		'Label88
		'
		Me.Label88.Location = New System.Drawing.Point(4, 142)
		Me.Label88.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label88.Name = "Label88"
		Me.Label88.Size = New System.Drawing.Size(160, 20)
		Me.Label88.TabIndex = 30
		Me.Label88.Text = "資料來源"
		'
		'asm_DataSource_TextBox
		'
		Me.asm_DataSource_TextBox.Location = New System.Drawing.Point(172, 136)
		Me.asm_DataSource_TextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.asm_DataSource_TextBox.Name = "asm_DataSource_TextBox"
		Me.asm_DataSource_TextBox.Size = New System.Drawing.Size(940, 25)
		Me.asm_DataSource_TextBox.TabIndex = 5
		'
		'Label80
		'
		Me.Label80.Location = New System.Drawing.Point(4, 175)
		Me.Label80.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label80.Name = "Label80"
		Me.Label80.Size = New System.Drawing.Size(160, 20)
		Me.Label80.TabIndex = 47
		Me.Label80.Text = "備註"
		'
		'asm_Remark_TextBox
		'
		Me.asm_Remark_TextBox.Location = New System.Drawing.Point(172, 169)
		Me.asm_Remark_TextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.asm_Remark_TextBox.Name = "asm_Remark_TextBox"
		Me.asm_Remark_TextBox.Size = New System.Drawing.Size(940, 25)
		Me.asm_Remark_TextBox.TabIndex = 6
		'
		'FlowLayoutPanel7
		'
		Me.FlowLayoutPanel7.Controls.Add(Me.asm_Insert_Button)
		Me.FlowLayoutPanel7.Controls.Add(Me.asm_Update_Button)
		Me.FlowLayoutPanel7.Controls.Add(Me.asm_Delete_Button)
		Me.FlowLayoutPanel7.Location = New System.Drawing.Point(4, 202)
		Me.FlowLayoutPanel7.Margin = New System.Windows.Forms.Padding(4)
		Me.FlowLayoutPanel7.Name = "FlowLayoutPanel7"
		Me.FlowLayoutPanel7.Size = New System.Drawing.Size(1165, 45)
		Me.FlowLayoutPanel7.TabIndex = 20
		'
		'asm_Insert_Button
		'
		Me.asm_Insert_Button.ForeColor = System.Drawing.Color.Blue
		Me.asm_Insert_Button.Location = New System.Drawing.Point(2, 4)
		Me.asm_Insert_Button.Margin = New System.Windows.Forms.Padding(2, 4, 4, 4)
		Me.asm_Insert_Button.Name = "asm_Insert_Button"
		Me.asm_Insert_Button.Size = New System.Drawing.Size(380, 38)
		Me.asm_Insert_Button.TabIndex = 21
		Me.asm_Insert_Button.Tag = "Insert"
		Me.asm_Insert_Button.Text = "新增"
		Me.asm_Insert_Button.UseVisualStyleBackColor = True
		'
		'asm_Update_Button
		'
		Me.asm_Update_Button.ForeColor = System.Drawing.Color.DarkGreen
		Me.asm_Update_Button.Location = New System.Drawing.Point(390, 4)
		Me.asm_Update_Button.Margin = New System.Windows.Forms.Padding(4)
		Me.asm_Update_Button.Name = "asm_Update_Button"
		Me.asm_Update_Button.Size = New System.Drawing.Size(375, 38)
		Me.asm_Update_Button.TabIndex = 22
		Me.asm_Update_Button.Tag = "Update"
		Me.asm_Update_Button.Text = "更新"
		Me.asm_Update_Button.UseVisualStyleBackColor = True
		'
		'asm_Delete_Button
		'
		Me.asm_Delete_Button.ForeColor = System.Drawing.Color.Red
		Me.asm_Delete_Button.Location = New System.Drawing.Point(773, 4)
		Me.asm_Delete_Button.Margin = New System.Windows.Forms.Padding(4)
		Me.asm_Delete_Button.Name = "asm_Delete_Button"
		Me.asm_Delete_Button.Size = New System.Drawing.Size(335, 38)
		Me.asm_Delete_Button.TabIndex = 23
		Me.asm_Delete_Button.Tag = "Delete"
		Me.asm_Delete_Button.Text = "刪除"
		Me.asm_Delete_Button.UseVisualStyleBackColor = True
		'
		'Panel7
		'
		Me.Panel7.Controls.Add(Me.asm_DataGridView)
		Me.Panel7.Controls.Add(Me.FlowLayoutPanel11)
		Me.Panel7.Location = New System.Drawing.Point(4, 255)
		Me.Panel7.Margin = New System.Windows.Forms.Padding(4)
		Me.Panel7.Name = "Panel7"
		Me.Panel7.Size = New System.Drawing.Size(1165, 627)
		Me.Panel7.TabIndex = 21
		'
		'asm_DataGridView
		'
		Me.asm_DataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
		Me.asm_DataGridView.Dock = System.Windows.Forms.DockStyle.Fill
		Me.asm_DataGridView.Location = New System.Drawing.Point(0, 72)
		Me.asm_DataGridView.Margin = New System.Windows.Forms.Padding(4)
		Me.asm_DataGridView.Name = "asm_DataGridView"
		Me.asm_DataGridView.RowTemplate.Height = 24
		Me.asm_DataGridView.Size = New System.Drawing.Size(1165, 555)
		Me.asm_DataGridView.TabIndex = 41
		'
		'FlowLayoutPanel11
		'
		Me.FlowLayoutPanel11.Controls.Add(Me.Label89)
		Me.FlowLayoutPanel11.Controls.Add(Me.asm_NameFilter_TextBox)
		Me.FlowLayoutPanel11.Controls.Add(Me.asm_Clean_Button)
		Me.FlowLayoutPanel11.Controls.Add(Me.Label90)
		Me.FlowLayoutPanel11.Controls.Add(Me.asm_FilterInfo_Label)
		Me.FlowLayoutPanel11.Dock = System.Windows.Forms.DockStyle.Top
		Me.FlowLayoutPanel11.Location = New System.Drawing.Point(0, 0)
		Me.FlowLayoutPanel11.Margin = New System.Windows.Forms.Padding(4)
		Me.FlowLayoutPanel11.Name = "FlowLayoutPanel11"
		Me.FlowLayoutPanel11.Size = New System.Drawing.Size(1165, 72)
		Me.FlowLayoutPanel11.TabIndex = 4
		'
		'Label89
		'
		Me.Label89.Location = New System.Drawing.Point(2, 10)
		Me.Label89.Margin = New System.Windows.Forms.Padding(2, 10, 4, 0)
		Me.Label89.Name = "Label89"
		Me.Label89.Size = New System.Drawing.Size(160, 20)
		Me.Label89.TabIndex = 0
		Me.Label89.Text = "組裝方式名稱篩選"
		'
		'asm_NameFilter_TextBox
		'
		Me.asm_NameFilter_TextBox.Location = New System.Drawing.Point(170, 4)
		Me.asm_NameFilter_TextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.asm_NameFilter_TextBox.Name = "asm_NameFilter_TextBox"
		Me.asm_NameFilter_TextBox.Size = New System.Drawing.Size(745, 25)
		Me.asm_NameFilter_TextBox.TabIndex = 31
		'
		'asm_Clean_Button
		'
		Me.asm_Clean_Button.Location = New System.Drawing.Point(923, 4)
		Me.asm_Clean_Button.Margin = New System.Windows.Forms.Padding(4)
		Me.asm_Clean_Button.Name = "asm_Clean_Button"
		Me.asm_Clean_Button.Size = New System.Drawing.Size(185, 29)
		Me.asm_Clean_Button.TabIndex = 32
		Me.asm_Clean_Button.Text = "清除篩選"
		Me.asm_Clean_Button.UseVisualStyleBackColor = True
		'
		'Label90
		'
		Me.Label90.AutoSize = True
		Me.Label90.ForeColor = System.Drawing.SystemColors.Highlight
		Me.Label90.Location = New System.Drawing.Point(2, 47)
		Me.Label90.Margin = New System.Windows.Forms.Padding(2, 10, 4, 0)
		Me.Label90.Name = "Label90"
		Me.Label90.Size = New System.Drawing.Size(124, 15)
		Me.Label90.TabIndex = 5
		Me.Label90.Text = "篩選筆數 / 總筆數"
		'
		'asm_FilterInfo_Label
		'
		Me.asm_FilterInfo_Label.AutoSize = True
		Me.asm_FilterInfo_Label.ForeColor = System.Drawing.Color.Red
		Me.asm_FilterInfo_Label.Location = New System.Drawing.Point(134, 47)
		Me.asm_FilterInfo_Label.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.asm_FilterInfo_Label.Name = "asm_FilterInfo_Label"
		Me.asm_FilterInfo_Label.Size = New System.Drawing.Size(31, 15)
		Me.asm_FilterInfo_Label.TabIndex = 6
		Me.asm_FilterInfo_Label.Text = "N/A"
		'
		'ImageList1
		'
		Me.ImageList1.ImageStream = CType(resources.GetObject("ImageList1.ImageStream"), System.Windows.Forms.ImageListStreamer)
		Me.ImageList1.TransparentColor = System.Drawing.Color.Transparent
		Me.ImageList1.Images.SetKeyName(0, "material_16.ico")
		Me.ImageList1.Images.SetKeyName(1, "transport_2.ico")
		Me.ImageList1.Images.SetKeyName(2, "make_process.ico")
		Me.ImageList1.Images.SetKeyName(3, "electronic_component.ico")
		Me.ImageList1.Images.SetKeyName(4, "electronic_board.ico")
		Me.ImageList1.Images.SetKeyName(5, "lcd.ico")
		Me.ImageList1.Images.SetKeyName(6, "assembly.ico")
		'
		'Label37
		'
		Me.Label37.Image = Global.WinAP.WFE.My.Resources.Resources.blue_water_16
		Me.Label37.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.Label37.Location = New System.Drawing.Point(4, 241)
		Me.Label37.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label37.Name = "Label37"
		Me.Label37.Size = New System.Drawing.Size(160, 20)
		Me.Label37.TabIndex = 8
		Me.Label37.Text = "藍水 m^3/Kg"
		Me.Label37.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		'
		'Label39
		'
		Me.Label39.Image = Global.WinAP.WFE.My.Resources.Resources.green_water_16
		Me.Label39.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.Label39.Location = New System.Drawing.Point(4, 274)
		Me.Label39.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label39.Name = "Label39"
		Me.Label39.Size = New System.Drawing.Size(160, 20)
		Me.Label39.TabIndex = 11
		Me.Label39.Text = "綠水 m^3/Kg"
		Me.Label39.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		'
		'Label41
		'
		Me.Label41.Image = Global.WinAP.WFE.My.Resources.Resources.gray_water_16
		Me.Label41.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me.Label41.Location = New System.Drawing.Point(4, 307)
		Me.Label41.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label41.Name = "Label41"
		Me.Label41.Size = New System.Drawing.Size(160, 20)
		Me.Label41.TabIndex = 14
		Me.Label41.Text = "灰水 m^3/Kg"
		Me.Label41.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		'
		'DataEditForm
		'
		Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 15.0!)
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.ClientSize = New System.Drawing.Size(1192, 978)
		Me.Controls.Add(Me.TabControl1)
		Me.Controls.Add(Me.StatusStrip1)
		Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
		Me.Margin = New System.Windows.Forms.Padding(4)
		Me.MaximizeBox = False
		Me.MinimumSize = New System.Drawing.Size(781, 956)
		Me.Name = "DataEditForm"
		Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
		Me.Text = "資料新增/編輯"
		Me.TopMost = True
		Me.StatusStrip1.ResumeLayout(False)
		Me.StatusStrip1.PerformLayout()
		Me.TabControl1.ResumeLayout(False)
		Me.TabPage1.ResumeLayout(False)
		Me.FlowLayoutPanel1.ResumeLayout(False)
		Me.FlowLayoutPanel1.PerformLayout()
		Me.FlowLayoutPanel2.ResumeLayout(False)
		Me.Panel1.ResumeLayout(False)
		CType(Me.m_DataGridView, System.ComponentModel.ISupportInitialize).EndInit()
		Me.FlowLayoutPanel3.ResumeLayout(False)
		Me.FlowLayoutPanel3.PerformLayout()
		Me.TabPage2.ResumeLayout(False)
		Me.FlowLayoutPanel5.ResumeLayout(False)
		Me.FlowLayoutPanel5.PerformLayout()
		Me.FlowLayoutPanel6.ResumeLayout(False)
		Me.Panel2.ResumeLayout(False)
		CType(Me.t_DataGridView, System.ComponentModel.ISupportInitialize).EndInit()
		Me.FlowLayoutPanel8.ResumeLayout(False)
		Me.FlowLayoutPanel8.PerformLayout()
		Me.TabPage3.ResumeLayout(False)
		Me.FlowLayoutPanel9.ResumeLayout(False)
		Me.FlowLayoutPanel9.PerformLayout()
		Me.FlowLayoutPanel10.ResumeLayout(False)
		Me.Panel3.ResumeLayout(False)
		CType(Me.p_DataGridView, System.ComponentModel.ISupportInitialize).EndInit()
		Me.FlowLayoutPanel12.ResumeLayout(False)
		Me.FlowLayoutPanel12.PerformLayout()
		Me.TabPage4.ResumeLayout(False)
		Me.FlowLayoutPanel13.ResumeLayout(False)
		Me.FlowLayoutPanel13.PerformLayout()
		Me.FlowLayoutPanel14.ResumeLayout(False)
		Me.Panel4.ResumeLayout(False)
		CType(Me.e_DataGridView, System.ComponentModel.ISupportInitialize).EndInit()
		Me.FlowLayoutPanel16.ResumeLayout(False)
		Me.FlowLayoutPanel16.PerformLayout()
		Me.TabPage5.ResumeLayout(False)
		Me.FlowLayoutPanel17.ResumeLayout(False)
		Me.FlowLayoutPanel17.PerformLayout()
		Me.FlowLayoutPanel18.ResumeLayout(False)
		Me.Panel5.ResumeLayout(False)
		CType(Me.b_DataGridView, System.ComponentModel.ISupportInitialize).EndInit()
		Me.FlowLayoutPanel20.ResumeLayout(False)
		Me.FlowLayoutPanel20.PerformLayout()
		Me.TabPage6.ResumeLayout(False)
		Me.FlowLayoutPanel21.ResumeLayout(False)
		Me.FlowLayoutPanel21.PerformLayout()
		Me.FlowLayoutPanel22.ResumeLayout(False)
		Me.Panel6.ResumeLayout(False)
		CType(Me.d_DataGridView, System.ComponentModel.ISupportInitialize).EndInit()
		Me.FlowLayoutPanel24.ResumeLayout(False)
		Me.FlowLayoutPanel24.PerformLayout()
		Me.TabPage7.ResumeLayout(False)
		Me.FlowLayoutPanel4.ResumeLayout(False)
		Me.FlowLayoutPanel4.PerformLayout()
		Me.FlowLayoutPanel7.ResumeLayout(False)
		Me.Panel7.ResumeLayout(False)
		CType(Me.asm_DataGridView, System.ComponentModel.ISupportInitialize).EndInit()
		Me.FlowLayoutPanel11.ResumeLayout(False)
		Me.FlowLayoutPanel11.PerformLayout()
		Me.ResumeLayout(False)
		Me.PerformLayout()

	End Sub

	Friend WithEvents StatusStrip1 As StatusStrip
	Friend WithEvents ToolStripStatusLabel1 As ToolStripStatusLabel
	Friend WithEvents TabControl1 As TabControl
	Friend WithEvents TabPage1 As TabPage
	Friend WithEvents TabPage2 As TabPage
	Friend WithEvents FlowLayoutPanel1 As FlowLayoutPanel
	Friend WithEvents Label1 As Label
	Friend WithEvents m_MaterialName_TextBox As TextBox
	Friend WithEvents Label2 As Label
	Friend WithEvents m_Density_TextBox As TextBox
	Friend WithEvents Label4 As Label
	Friend WithEvents m_WFP_TextBox As TextBox
	Friend WithEvents Label6 As Label
	Friend WithEvents m_BlueWater_TextBox As TextBox
	Friend WithEvents Label8 As Label
	Friend WithEvents m_GreenWater_TextBox As TextBox
	Friend WithEvents Label10 As Label
	Friend WithEvents m_GrayWater_TextBox As TextBox
	Friend WithEvents Label12 As Label
	Friend WithEvents m_DataSource_TextBox As TextBox
	Friend WithEvents TabPage3 As TabPage
	Friend WithEvents TabPage4 As TabPage
	Friend WithEvents FlowLayoutPanel2 As FlowLayoutPanel
	Friend WithEvents m_Insert_Button As Button
	Friend WithEvents Panel1 As Panel
	Friend WithEvents m_Delete_Button As Button
	Friend WithEvents FlowLayoutPanel3 As FlowLayoutPanel
	Friend WithEvents Label14 As Label
	Friend WithEvents m_NameFilter_TextBox As TextBox
	Friend WithEvents m_Clean_Button As Button
	Friend WithEvents FlowLayoutPanel5 As FlowLayoutPanel
	Friend WithEvents Label15 As Label
	Friend WithEvents t_TransportName_TextBox As TextBox
	Friend WithEvents Label16 As Label
	Friend WithEvents t_CalculateUnit_TextBox As TextBox
	Friend WithEvents Label29 As Label
	Friend WithEvents t_MaxLoad_TextBox As TextBox
	Friend WithEvents Label18 As Label
	Friend WithEvents t_WFP_TextBox As TextBox
	Friend WithEvents Label20 As Label
	Friend WithEvents t_BlueWater_TextBox As TextBox
	Friend WithEvents Label22 As Label
	Friend WithEvents t_GreenWater_TextBox As TextBox
	Friend WithEvents Label24 As Label
	Friend WithEvents t_GrayWater_TextBox As TextBox
	Friend WithEvents Label17 As Label
	Friend WithEvents t_Unit_TextBox As TextBox
	Friend WithEvents Label31 As Label
	Friend WithEvents t_EnergyPerUnit_TextBox As TextBox
	Friend WithEvents Label26 As Label
	Friend WithEvents t_DataSource_TextBox As TextBox
	Friend WithEvents FlowLayoutPanel6 As FlowLayoutPanel
	Friend WithEvents Panel2 As Panel
	Friend WithEvents t_DataGridView As DataGridView
	Friend WithEvents FlowLayoutPanel8 As FlowLayoutPanel
	Friend WithEvents Label28 As Label
	Friend WithEvents t_NameFilter_TextBox As TextBox
	Friend WithEvents t_Clean_Button As Button
	Friend WithEvents FlowLayoutPanel9 As FlowLayoutPanel
	Friend WithEvents Label27 As Label
	Friend WithEvents p_MakeProcessName_TextBox As TextBox
	Friend WithEvents Label32 As Label
	Friend WithEvents Label33 As Label
	Friend WithEvents p_MachineName_TextBox As TextBox
	Friend WithEvents Label35 As Label
	Friend WithEvents p_WFP_TextBox As TextBox
	Friend WithEvents Label37 As Label
	Friend WithEvents p_BlueWater_TextBox As TextBox
	Friend WithEvents Label39 As Label
	Friend WithEvents p_GreenWater_TextBox As TextBox
	Friend WithEvents Label41 As Label
	Friend WithEvents p_GrayWater_TextBox As TextBox
	Friend WithEvents Label43 As Label
	Friend WithEvents p_CalculateUnit_TextBox As TextBox
	Friend WithEvents Label44 As Label
	Friend WithEvents p_PowerConsumption_TextBox As TextBox
	Friend WithEvents Label45 As Label
	Friend WithEvents p_PowerCoefficient_TextBox As TextBox
	Friend WithEvents FlowLayoutPanel10 As FlowLayoutPanel
	Friend WithEvents Panel3 As Panel
	Friend WithEvents p_DataGridView As DataGridView
	Friend WithEvents FlowLayoutPanel12 As FlowLayoutPanel
	Friend WithEvents Label46 As Label
	Friend WithEvents p_NameFilter_TextBox As TextBox
	Friend WithEvents p_Clean_Button As Button
	Friend WithEvents Label34 As Label
	Friend WithEvents p_DataSource_TextBox As TextBox
	Friend WithEvents FlowLayoutPanel13 As FlowLayoutPanel
	Friend WithEvents Label3 As Label
	Friend WithEvents e_ProcessName_TextBox As TextBox
	Friend WithEvents Label5 As Label
	Friend WithEvents e_Density_TextBox As TextBox
	Friend WithEvents Label9 As Label
	Friend WithEvents e_WFP_TextBox As TextBox
	Friend WithEvents Label11 As Label
	Friend WithEvents e_BlueWater_TextBox As TextBox
	Friend WithEvents Label13 As Label
	Friend WithEvents e_GreenWater_TextBox As TextBox
	Friend WithEvents Label19 As Label
	Friend WithEvents e_GrayWater_TextBox As TextBox
	Friend WithEvents Label30 As Label
	Friend WithEvents e_DataSource_TextBox As TextBox
	Friend WithEvents FlowLayoutPanel14 As FlowLayoutPanel
	Friend WithEvents Panel4 As Panel
	Friend WithEvents e_DataGridView As DataGridView
	Friend WithEvents FlowLayoutPanel16 As FlowLayoutPanel
	Friend WithEvents Label36 As Label
	Friend WithEvents e_NameFilter_TextBox As TextBox
	Friend WithEvents e_Clean_Button As Button
	Friend WithEvents TabPage5 As TabPage
	Friend WithEvents TabPage6 As TabPage
	Friend WithEvents FlowLayoutPanel17 As FlowLayoutPanel
	Friend WithEvents Label7 As Label
	Friend WithEvents b_ProcessName_TextBox As TextBox
	Friend WithEvents Label21 As Label
	Friend WithEvents b_UnitCode_TextBox As TextBox
	Friend WithEvents Label23 As Label
	Friend WithEvents b_WFP_TextBox As TextBox
	Friend WithEvents Label25 As Label
	Friend WithEvents b_BlueWater_TextBox As TextBox
	Friend WithEvents Label38 As Label
	Friend WithEvents b_GreenWater_TextBox As TextBox
	Friend WithEvents Label40 As Label
	Friend WithEvents b_GrayWater_TextBox As TextBox
	Friend WithEvents Label42 As Label
	Friend WithEvents b_DataSource_TextBox As TextBox
	Friend WithEvents FlowLayoutPanel18 As FlowLayoutPanel
	Friend WithEvents Panel5 As Panel
	Friend WithEvents b_DataGridView As DataGridView
	Friend WithEvents FlowLayoutPanel20 As FlowLayoutPanel
	Friend WithEvents Label47 As Label
	Friend WithEvents b_NameFilter_TextBox As TextBox
	Friend WithEvents b_Clean_Button As Button
	Friend WithEvents FlowLayoutPanel21 As FlowLayoutPanel
	Friend WithEvents Label48 As Label
	Friend WithEvents d_ProcessName_TextBox As TextBox
	Friend WithEvents Label49 As Label
	Friend WithEvents d_UnitCode_TextBox As TextBox
	Friend WithEvents Label50 As Label
	Friend WithEvents d_WFP_TextBox As TextBox
	Friend WithEvents Label51 As Label
	Friend WithEvents d_BlueWater_TextBox As TextBox
	Friend WithEvents Label52 As Label
	Friend WithEvents d_GreenWater_TextBox As TextBox
	Friend WithEvents Label53 As Label
	Friend WithEvents d_GrayWater_TextBox As TextBox
	Friend WithEvents Label54 As Label
	Friend WithEvents d_DataSource_TextBox As TextBox
	Friend WithEvents FlowLayoutPanel22 As FlowLayoutPanel
	Friend WithEvents Panel6 As Panel
	Friend WithEvents d_DataGridView As DataGridView
	Friend WithEvents FlowLayoutPanel24 As FlowLayoutPanel
	Friend WithEvents Label55 As Label
	Friend WithEvents d_NameFilter_TextBox As TextBox
	Friend WithEvents d_Clean_Button As Button
	Friend WithEvents ImageList1 As ImageList
	Friend WithEvents Label56 As Label
	Friend WithEvents t_FilterInfo_Label As Label
	Friend WithEvents Label57 As Label
	Friend WithEvents m_FilterInfo_Label As Label
	Friend WithEvents Label58 As Label
	Friend WithEvents p_FilterInfo_Label As Label
	Friend WithEvents Label60 As Label
	Friend WithEvents e_FilterInfo_Label As Label
	Friend WithEvents Label62 As Label
	Friend WithEvents b_FilterInfo_Label As Label
	Friend WithEvents Label64 As Label
	Friend WithEvents d_FilterInfo_Label As Label
	Friend WithEvents m_Update_Button As Button
	Friend WithEvents m_DataGridView As DataGridView
	Friend WithEvents t_Insert_Button As Button
	Friend WithEvents t_Update_Button As Button
	Friend WithEvents t_Delete_Button As Button
	Friend WithEvents p_Insert_Button As Button
	Friend WithEvents p_Update_Button As Button
	Friend WithEvents p_Delete_Button As Button
	Friend WithEvents e_Insert_Button As Button
	Friend WithEvents e_Update_Button As Button
	Friend WithEvents e_Delete_Button As Button
	Friend WithEvents b_Insert_Button As Button
	Friend WithEvents b_Update_Button As Button
	Friend WithEvents b_Delete_Button As Button
	Friend WithEvents d_Insert_Button As Button
	Friend WithEvents d_Update_Button As Button
	Friend WithEvents d_Delete_Button As Button
	Friend WithEvents p_CalculateCode_ComboBox As ComboBox
	Friend WithEvents Label66 As Label
	Friend WithEvents p_EnergyPerUnit_TextBox As TextBox
	Friend WithEvents Label67 As Label
	Friend WithEvents e_ProcessTypeCode_TextBox As TextBox
	Friend WithEvents Label68 As Label
	Friend WithEvents e_UnitCode_TextBox As TextBox
	Friend WithEvents Label61 As Label
	Friend WithEvents b_soft_RadioButton As RadioButton
	Friend WithEvents b_hard_RadioButton As RadioButton
	Friend WithEvents b_ProcessTypeCode_TextBox As TextBox
	Friend WithEvents Label63 As Label
	Friend WithEvents d_ProcessTypeCode_TextBox As TextBox
	Friend WithEvents p_CalculateCode_TextBox As TextBox
	Friend WithEvents Label59 As Label
	Friend WithEvents m_ISO_TextBox As TextBox
	Friend WithEvents Label65 As Label
	Friend WithEvents m_BOD_TextBox As TextBox
	Friend WithEvents Label69 As Label
	Friend WithEvents Label70 As Label
	Friend WithEvents t_ISO_TextBox As TextBox
	Friend WithEvents t_BOD_TextBox As TextBox
	Friend WithEvents Label71 As Label
	Friend WithEvents p_ISO_TextBox As TextBox
	Friend WithEvents Label72 As Label
	Friend WithEvents p_BOD_TextBox As TextBox
	Friend WithEvents Label73 As Label
	Friend WithEvents e_ISO_TextBox As TextBox
	Friend WithEvents Label74 As Label
	Friend WithEvents e_BOD_TextBox As TextBox
	Friend WithEvents Label75 As Label
	Friend WithEvents b_ISO_TextBox As TextBox
	Friend WithEvents Label76 As Label
	Friend WithEvents b_BOD_TextBox As TextBox
	Friend WithEvents Label77 As Label
	Friend WithEvents d_ISO_TextBox As TextBox
	Friend WithEvents Label78 As Label
	Friend WithEvents d_BOD_TextBox As TextBox
	Friend WithEvents TabPage7 As TabPage
	Friend WithEvents FlowLayoutPanel4 As FlowLayoutPanel
	Friend WithEvents Label79 As Label
	Friend WithEvents asm_AssemblyMethodName_TextBox As TextBox
	Friend WithEvents Label81 As Label
	Friend WithEvents asm_CalculateUnit_TextBox As TextBox
	Friend WithEvents Label82 As Label
	Friend WithEvents asm_PowerConsumption_TextBox As TextBox
	Friend WithEvents Label83 As Label
	Friend WithEvents asm_PowerCoefficient_TextBox As TextBox
	Friend WithEvents Label88 As Label
	Friend WithEvents asm_DataSource_TextBox As TextBox
	Friend WithEvents Label80 As Label
	Friend WithEvents asm_Remark_TextBox As TextBox
	Friend WithEvents FlowLayoutPanel7 As FlowLayoutPanel
	Friend WithEvents asm_Insert_Button As Button
	Friend WithEvents asm_Update_Button As Button
	Friend WithEvents asm_Delete_Button As Button
	Friend WithEvents Panel7 As Panel
	Friend WithEvents asm_DataGridView As DataGridView
	Friend WithEvents FlowLayoutPanel11 As FlowLayoutPanel
	Friend WithEvents Label89 As Label
	Friend WithEvents asm_NameFilter_TextBox As TextBox
	Friend WithEvents asm_Clean_Button As Button
	Friend WithEvents Label90 As Label
	Friend WithEvents asm_FilterInfo_Label As Label
	Friend WithEvents Label85 As Label
	Friend WithEvents b_Density_TextBox As TextBox
	Friend WithEvents Label86 As Label
	Friend WithEvents d_Density_TextBox As TextBox
End Class
