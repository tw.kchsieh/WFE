﻿Imports System.Configuration
Imports WFE.Assistants.Helpers
Imports WFE.Assistants.Repos

Public Class CalculateSettingForm
	Private _precision As Integer
	Private _displayCode As String
	Private _configValue As String
	Private _repo As ConfigRepo
	Private Sub Button1_Click(sender As Object, e As EventArgs) Handles SaveButton.Click
		'儲存
		_precision = NumericUpDown1.Value
		Dim v1 = _repo.SavePrecisionNumber(_precision)
		Dim v2 = _repo.SaveNumberDisplayType(_displayCode)
		If v1 And v2 Then
			'儲存成功
			MsgBox("儲存成功！！")
			UIHelper.SetPrceisionNumber(_repo.PrecisionNumber)
			UIHelper.SetNumberDisplayType(_repo.NumberDisplayType)
			UIHelper.SetNumberTextFormatPattern(_repo.GetNumberFormatPattern())
			Me.Close()
		Else
			MsgBox("儲存失敗！！")
		End If
	End Sub

	Private Sub ComboBox1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ComboBox1.SelectedIndexChanged
		If ComboBox1.SelectedIndex = 0 Then
			'Fixed Point
			_displayCode = "F"
		Else
			'Scientific
			_displayCode = "E"
		End If
	End Sub

	Private Sub CalculateSettingForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
		'Load Config
		'取得設定檔位置
		Dim appSettings = ConfigurationManager.AppSettings
		_configValue = appSettings("settings")
		If IsNothing(_configValue) Then
			_configValue = ""
		End If

		_repo = New ConfigRepo(_configValue)
		If _repo.IsValid Then
			_precision = _repo.PrecisionNumber
			_displayCode = _repo.NumberDisplayType

			'SetUI
			If _displayCode = "E" Then
				ComboBox1.SelectedIndex = 1
			Else
				ComboBox1.SelectedIndex = 0
			End If

			NumericUpDown1.Value = _precision
		End If
	End Sub
End Class