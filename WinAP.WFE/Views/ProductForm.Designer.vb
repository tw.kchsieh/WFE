﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ProductForm
	Inherits System.Windows.Forms.Form

	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> _
	Protected Overrides Sub Dispose(ByVal disposing As Boolean)
		Try
			If disposing AndAlso components IsNot Nothing Then
				components.Dispose()
			End If
		Finally
			MyBase.Dispose(disposing)
		End Try
	End Sub

	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer

	'NOTE: The following procedure is required by the Windows Form Designer
	'It can be modified using the Windows Form Designer.  
	'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> _
	Private Sub InitializeComponent()
		Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ProductForm))
		Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
		Me.GroupBox2 = New System.Windows.Forms.GroupBox()
		Me.FlowLayoutPanel4 = New System.Windows.Forms.FlowLayoutPanel()
		Me.Label6 = New System.Windows.Forms.Label()
		Me.ProductName_TextBox = New System.Windows.Forms.TextBox()
		Me.EmptyLabel = New System.Windows.Forms.Label()
		Me.CenterWFN_Label = New System.Windows.Forms.Label()
		Me.EmptyLabel2 = New System.Windows.Forms.Label()
		Me.CenterISO_Label = New System.Windows.Forms.Label()
		Me.Label11 = New System.Windows.Forms.Label()
		Me.StageA_WFP_TextBox = New System.Windows.Forms.TextBox()
		Me.Label9 = New System.Windows.Forms.Label()
		Me.StageA_ISO_TextBox = New System.Windows.Forms.TextBox()
		Me.Label18 = New System.Windows.Forms.Label()
		Me.Label2 = New System.Windows.Forms.Label()
		Me.StageB_WFP_TextBox = New System.Windows.Forms.TextBox()
		Me.Label3 = New System.Windows.Forms.Label()
		Me.StageB_ISO_TextBox = New System.Windows.Forms.TextBox()
		Me.Label19 = New System.Windows.Forms.Label()
		Me.Label7 = New System.Windows.Forms.Label()
		Me.StageC_WFP_TextBox = New System.Windows.Forms.TextBox()
		Me.Label12 = New System.Windows.Forms.Label()
		Me.StageC_ISO_TextBox = New System.Windows.Forms.TextBox()
		Me.Label20 = New System.Windows.Forms.Label()
		Me.Label72 = New System.Windows.Forms.Label()
		Me.Stage_Asm_WFP_TextBox = New System.Windows.Forms.TextBox()
		Me.Label1 = New System.Windows.Forms.Label()
		Me.Stage_Asm_ISO_TextBox = New System.Windows.Forms.TextBox()
		Me.Label4 = New System.Windows.Forms.Label()
		Me.Label14 = New System.Windows.Forms.Label()
		Me.Total_WFP_TextBox = New System.Windows.Forms.TextBox()
		Me.Label13 = New System.Windows.Forms.Label()
		Me.Total_ISO_TextBox = New System.Windows.Forms.TextBox()
		Me.Label21 = New System.Windows.Forms.Label()
		Me.Product_DataGridView = New System.Windows.Forms.DataGridView()
		Me.FlowLayoutPanel3 = New System.Windows.Forms.FlowLayoutPanel()
		Me.Label10 = New System.Windows.Forms.Label()
		Me.RecentUsed_ComboBox = New System.Windows.Forms.ComboBox()
		Me.Label15 = New System.Windows.Forms.Label()
		Me.FilterInfo_Label = New System.Windows.Forms.Label()
		Me.FlowLayoutPanel2 = New System.Windows.Forms.FlowLayoutPanel()
		Me.Label5 = New System.Windows.Forms.Label()
		Me.Name_Filter_TextBox = New System.Windows.Forms.TextBox()
		Me.Clear_Filter_Button = New System.Windows.Forms.Button()
		Me.LoadProduct_Button = New System.Windows.Forms.Button()
		Me.DeleteButton = New System.Windows.Forms.Button()
		Me.FlowLayoutPanel1.SuspendLayout()
		Me.GroupBox2.SuspendLayout()
		Me.FlowLayoutPanel4.SuspendLayout()
		CType(Me.Product_DataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.FlowLayoutPanel3.SuspendLayout()
		Me.FlowLayoutPanel2.SuspendLayout()
		Me.SuspendLayout()
		'
		'FlowLayoutPanel1
		'
		Me.FlowLayoutPanel1.Controls.Add(Me.GroupBox2)
		Me.FlowLayoutPanel1.Controls.Add(Me.LoadProduct_Button)
		Me.FlowLayoutPanel1.Controls.Add(Me.DeleteButton)
		Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
		Me.FlowLayoutPanel1.Location = New System.Drawing.Point(0, 0)
		Me.FlowLayoutPanel1.Margin = New System.Windows.Forms.Padding(4)
		Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
		Me.FlowLayoutPanel1.Size = New System.Drawing.Size(577, 952)
		Me.FlowLayoutPanel1.TabIndex = 2
		'
		'GroupBox2
		'
		Me.GroupBox2.Controls.Add(Me.FlowLayoutPanel4)
		Me.GroupBox2.Controls.Add(Me.Product_DataGridView)
		Me.GroupBox2.Controls.Add(Me.FlowLayoutPanel3)
		Me.GroupBox2.Controls.Add(Me.FlowLayoutPanel2)
		Me.GroupBox2.ForeColor = System.Drawing.Color.Black
		Me.GroupBox2.Location = New System.Drawing.Point(4, 4)
		Me.GroupBox2.Margin = New System.Windows.Forms.Padding(4)
		Me.GroupBox2.Name = "GroupBox2"
		Me.GroupBox2.Padding = New System.Windows.Forms.Padding(4)
		Me.GroupBox2.Size = New System.Drawing.Size(568, 839)
		Me.GroupBox2.TabIndex = 1
		Me.GroupBox2.TabStop = False
		Me.GroupBox2.Text = "選擇專案"
		'
		'FlowLayoutPanel4
		'
		Me.FlowLayoutPanel4.BackColor = System.Drawing.Color.Transparent
		Me.FlowLayoutPanel4.Controls.Add(Me.Label6)
		Me.FlowLayoutPanel4.Controls.Add(Me.ProductName_TextBox)
		Me.FlowLayoutPanel4.Controls.Add(Me.EmptyLabel)
		Me.FlowLayoutPanel4.Controls.Add(Me.CenterWFN_Label)
		Me.FlowLayoutPanel4.Controls.Add(Me.EmptyLabel2)
		Me.FlowLayoutPanel4.Controls.Add(Me.CenterISO_Label)
		Me.FlowLayoutPanel4.Controls.Add(Me.Label11)
		Me.FlowLayoutPanel4.Controls.Add(Me.StageA_WFP_TextBox)
		Me.FlowLayoutPanel4.Controls.Add(Me.Label9)
		Me.FlowLayoutPanel4.Controls.Add(Me.StageA_ISO_TextBox)
		Me.FlowLayoutPanel4.Controls.Add(Me.Label18)
		Me.FlowLayoutPanel4.Controls.Add(Me.Label2)
		Me.FlowLayoutPanel4.Controls.Add(Me.StageB_WFP_TextBox)
		Me.FlowLayoutPanel4.Controls.Add(Me.Label3)
		Me.FlowLayoutPanel4.Controls.Add(Me.StageB_ISO_TextBox)
		Me.FlowLayoutPanel4.Controls.Add(Me.Label19)
		Me.FlowLayoutPanel4.Controls.Add(Me.Label7)
		Me.FlowLayoutPanel4.Controls.Add(Me.StageC_WFP_TextBox)
		Me.FlowLayoutPanel4.Controls.Add(Me.Label12)
		Me.FlowLayoutPanel4.Controls.Add(Me.StageC_ISO_TextBox)
		Me.FlowLayoutPanel4.Controls.Add(Me.Label20)
		Me.FlowLayoutPanel4.Controls.Add(Me.Label72)
		Me.FlowLayoutPanel4.Controls.Add(Me.Stage_Asm_WFP_TextBox)
		Me.FlowLayoutPanel4.Controls.Add(Me.Label1)
		Me.FlowLayoutPanel4.Controls.Add(Me.Stage_Asm_ISO_TextBox)
		Me.FlowLayoutPanel4.Controls.Add(Me.Label4)
		Me.FlowLayoutPanel4.Controls.Add(Me.Label14)
		Me.FlowLayoutPanel4.Controls.Add(Me.Total_WFP_TextBox)
		Me.FlowLayoutPanel4.Controls.Add(Me.Label13)
		Me.FlowLayoutPanel4.Controls.Add(Me.Total_ISO_TextBox)
		Me.FlowLayoutPanel4.Controls.Add(Me.Label21)
		Me.FlowLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill
		Me.FlowLayoutPanel4.Location = New System.Drawing.Point(4, 574)
		Me.FlowLayoutPanel4.Margin = New System.Windows.Forms.Padding(4)
		Me.FlowLayoutPanel4.Name = "FlowLayoutPanel4"
		Me.FlowLayoutPanel4.Size = New System.Drawing.Size(560, 261)
		Me.FlowLayoutPanel4.TabIndex = 8
		'
		'Label6
		'
		Me.Label6.Location = New System.Drawing.Point(4, 10)
		Me.Label6.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label6.Name = "Label6"
		Me.Label6.Size = New System.Drawing.Size(77, 15)
		Me.Label6.TabIndex = 0
		Me.Label6.Text = "專案名稱"
		'
		'ProductName_TextBox
		'
		Me.ProductName_TextBox.Location = New System.Drawing.Point(89, 4)
		Me.ProductName_TextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.ProductName_TextBox.Name = "ProductName_TextBox"
		Me.ProductName_TextBox.ReadOnly = True
		Me.ProductName_TextBox.Size = New System.Drawing.Size(465, 25)
		Me.ProductName_TextBox.TabIndex = 1
		'
		'EmptyLabel
		'
		Me.EmptyLabel.Font = New System.Drawing.Font("PMingLiU", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
		Me.EmptyLabel.Location = New System.Drawing.Point(4, 41)
		Me.EmptyLabel.Margin = New System.Windows.Forms.Padding(4, 8, 4, 0)
		Me.EmptyLabel.Name = "EmptyLabel"
		Me.EmptyLabel.Size = New System.Drawing.Size(113, 25)
		Me.EmptyLabel.TabIndex = 47
		Me.EmptyLabel.Text = "e1"
		Me.EmptyLabel.TextAlign = System.Drawing.ContentAlignment.BottomLeft
		'
		'CenterWFN_Label
		'
		Me.CenterWFN_Label.Font = New System.Drawing.Font("PMingLiU", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
		Me.CenterWFN_Label.ForeColor = System.Drawing.Color.Purple
		Me.CenterWFN_Label.Location = New System.Drawing.Point(125, 41)
		Me.CenterWFN_Label.Margin = New System.Windows.Forms.Padding(4, 8, 4, 0)
		Me.CenterWFN_Label.Name = "CenterWFN_Label"
		Me.CenterWFN_Label.Size = New System.Drawing.Size(168, 25)
		Me.CenterWFN_Label.TabIndex = 30
		Me.CenterWFN_Label.Text = "WFN"
		Me.CenterWFN_Label.TextAlign = System.Drawing.ContentAlignment.BottomCenter
		'
		'EmptyLabel2
		'
		Me.EmptyLabel2.Font = New System.Drawing.Font("PMingLiU", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
		Me.EmptyLabel2.Location = New System.Drawing.Point(301, 41)
		Me.EmptyLabel2.Margin = New System.Windows.Forms.Padding(4, 8, 4, 0)
		Me.EmptyLabel2.Name = "EmptyLabel2"
		Me.EmptyLabel2.Size = New System.Drawing.Size(36, 25)
		Me.EmptyLabel2.TabIndex = 48
		Me.EmptyLabel2.Text = "e2"
		Me.EmptyLabel2.TextAlign = System.Drawing.ContentAlignment.BottomLeft
		'
		'CenterISO_Label
		'
		Me.CenterISO_Label.Font = New System.Drawing.Font("PMingLiU", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
		Me.CenterISO_Label.Location = New System.Drawing.Point(345, 41)
		Me.CenterISO_Label.Margin = New System.Windows.Forms.Padding(4, 8, 4, 0)
		Me.CenterISO_Label.Name = "CenterISO_Label"
		Me.CenterISO_Label.Size = New System.Drawing.Size(168, 25)
		Me.CenterISO_Label.TabIndex = 29
		Me.CenterISO_Label.Text = "ISO 14046"
		Me.CenterISO_Label.TextAlign = System.Drawing.ContentAlignment.BottomCenter
		'
		'Label11
		'
		Me.Label11.ForeColor = System.Drawing.Color.Blue
		Me.Label11.Location = New System.Drawing.Point(4, 76)
		Me.Label11.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label11.Name = "Label11"
		Me.Label11.Size = New System.Drawing.Size(113, 15)
		Me.Label11.TabIndex = 6
		Me.Label11.Text = "一般零件階段 :"
		Me.Label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight
		'
		'StageA_WFP_TextBox
		'
		Me.StageA_WFP_TextBox.Location = New System.Drawing.Point(125, 70)
		Me.StageA_WFP_TextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.StageA_WFP_TextBox.Name = "StageA_WFP_TextBox"
		Me.StageA_WFP_TextBox.ReadOnly = True
		Me.StageA_WFP_TextBox.Size = New System.Drawing.Size(168, 25)
		Me.StageA_WFP_TextBox.TabIndex = 4
		'
		'Label9
		'
		Me.Label9.AutoSize = True
		Me.Label9.Location = New System.Drawing.Point(301, 76)
		Me.Label9.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label9.Name = "Label9"
		Me.Label9.Size = New System.Drawing.Size(32, 15)
		Me.Label9.TabIndex = 5
		Me.Label9.Text = "m^3"
		'
		'StageA_ISO_TextBox
		'
		Me.StageA_ISO_TextBox.Location = New System.Drawing.Point(341, 70)
		Me.StageA_ISO_TextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.StageA_ISO_TextBox.Name = "StageA_ISO_TextBox"
		Me.StageA_ISO_TextBox.ReadOnly = True
		Me.StageA_ISO_TextBox.Size = New System.Drawing.Size(168, 25)
		Me.StageA_ISO_TextBox.TabIndex = 31
		'
		'Label18
		'
		Me.Label18.AutoSize = True
		Me.Label18.Location = New System.Drawing.Point(517, 76)
		Me.Label18.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label18.Name = "Label18"
		Me.Label18.Size = New System.Drawing.Size(32, 15)
		Me.Label18.TabIndex = 32
		Me.Label18.Text = "m^3"
		'
		'Label2
		'
		Me.Label2.ForeColor = System.Drawing.Color.Goldenrod
		Me.Label2.Location = New System.Drawing.Point(4, 109)
		Me.Label2.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label2.Name = "Label2"
		Me.Label2.Size = New System.Drawing.Size(113, 15)
		Me.Label2.TabIndex = 10
		Me.Label2.Text = "後製加工階段 :"
		Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
		'
		'StageB_WFP_TextBox
		'
		Me.StageB_WFP_TextBox.Location = New System.Drawing.Point(125, 103)
		Me.StageB_WFP_TextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.StageB_WFP_TextBox.Name = "StageB_WFP_TextBox"
		Me.StageB_WFP_TextBox.ReadOnly = True
		Me.StageB_WFP_TextBox.Size = New System.Drawing.Size(168, 25)
		Me.StageB_WFP_TextBox.TabIndex = 8
		'
		'Label3
		'
		Me.Label3.AutoSize = True
		Me.Label3.Location = New System.Drawing.Point(301, 109)
		Me.Label3.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label3.Name = "Label3"
		Me.Label3.Size = New System.Drawing.Size(32, 15)
		Me.Label3.TabIndex = 9
		Me.Label3.Text = "m^3"
		'
		'StageB_ISO_TextBox
		'
		Me.StageB_ISO_TextBox.Location = New System.Drawing.Point(341, 103)
		Me.StageB_ISO_TextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.StageB_ISO_TextBox.Name = "StageB_ISO_TextBox"
		Me.StageB_ISO_TextBox.ReadOnly = True
		Me.StageB_ISO_TextBox.Size = New System.Drawing.Size(168, 25)
		Me.StageB_ISO_TextBox.TabIndex = 33
		'
		'Label19
		'
		Me.Label19.AutoSize = True
		Me.Label19.Location = New System.Drawing.Point(517, 109)
		Me.Label19.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label19.Name = "Label19"
		Me.Label19.Size = New System.Drawing.Size(32, 15)
		Me.Label19.TabIndex = 35
		Me.Label19.Text = "m^3"
		'
		'Label7
		'
		Me.Label7.ForeColor = System.Drawing.Color.Red
		Me.Label7.Location = New System.Drawing.Point(4, 142)
		Me.Label7.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label7.Name = "Label7"
		Me.Label7.Size = New System.Drawing.Size(113, 15)
		Me.Label7.TabIndex = 14
		Me.Label7.Text = "特殊零件階段 :"
		Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight
		'
		'StageC_WFP_TextBox
		'
		Me.StageC_WFP_TextBox.Location = New System.Drawing.Point(125, 136)
		Me.StageC_WFP_TextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.StageC_WFP_TextBox.Name = "StageC_WFP_TextBox"
		Me.StageC_WFP_TextBox.ReadOnly = True
		Me.StageC_WFP_TextBox.Size = New System.Drawing.Size(168, 25)
		Me.StageC_WFP_TextBox.TabIndex = 12
		'
		'Label12
		'
		Me.Label12.AutoSize = True
		Me.Label12.Location = New System.Drawing.Point(301, 142)
		Me.Label12.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label12.Name = "Label12"
		Me.Label12.Size = New System.Drawing.Size(32, 15)
		Me.Label12.TabIndex = 13
		Me.Label12.Text = "m^3"
		'
		'StageC_ISO_TextBox
		'
		Me.StageC_ISO_TextBox.Location = New System.Drawing.Point(341, 136)
		Me.StageC_ISO_TextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.StageC_ISO_TextBox.Name = "StageC_ISO_TextBox"
		Me.StageC_ISO_TextBox.ReadOnly = True
		Me.StageC_ISO_TextBox.Size = New System.Drawing.Size(168, 25)
		Me.StageC_ISO_TextBox.TabIndex = 34
		'
		'Label20
		'
		Me.Label20.AutoSize = True
		Me.Label20.Location = New System.Drawing.Point(517, 142)
		Me.Label20.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label20.Name = "Label20"
		Me.Label20.Size = New System.Drawing.Size(32, 15)
		Me.Label20.TabIndex = 36
		Me.Label20.Text = "m^3"
		'
		'Label72
		'
		Me.Label72.Font = New System.Drawing.Font("PMingLiU", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
		Me.Label72.ForeColor = System.Drawing.Color.DarkGreen
		Me.Label72.Location = New System.Drawing.Point(4, 175)
		Me.Label72.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label72.Name = "Label72"
		Me.Label72.Size = New System.Drawing.Size(113, 15)
		Me.Label72.TabIndex = 42
		Me.Label72.Text = "產品組裝階段 :"
		Me.Label72.TextAlign = System.Drawing.ContentAlignment.MiddleRight
		'
		'Stage_Asm_WFP_TextBox
		'
		Me.Stage_Asm_WFP_TextBox.Location = New System.Drawing.Point(125, 169)
		Me.Stage_Asm_WFP_TextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.Stage_Asm_WFP_TextBox.Name = "Stage_Asm_WFP_TextBox"
		Me.Stage_Asm_WFP_TextBox.ReadOnly = True
		Me.Stage_Asm_WFP_TextBox.Size = New System.Drawing.Size(168, 25)
		Me.Stage_Asm_WFP_TextBox.TabIndex = 43
		'
		'Label1
		'
		Me.Label1.AutoSize = True
		Me.Label1.Location = New System.Drawing.Point(301, 175)
		Me.Label1.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label1.Name = "Label1"
		Me.Label1.Size = New System.Drawing.Size(32, 15)
		Me.Label1.TabIndex = 44
		Me.Label1.Text = "m^3"
		'
		'Stage_Asm_ISO_TextBox
		'
		Me.Stage_Asm_ISO_TextBox.Location = New System.Drawing.Point(341, 169)
		Me.Stage_Asm_ISO_TextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.Stage_Asm_ISO_TextBox.Name = "Stage_Asm_ISO_TextBox"
		Me.Stage_Asm_ISO_TextBox.ReadOnly = True
		Me.Stage_Asm_ISO_TextBox.Size = New System.Drawing.Size(168, 25)
		Me.Stage_Asm_ISO_TextBox.TabIndex = 45
		'
		'Label4
		'
		Me.Label4.AutoSize = True
		Me.Label4.Location = New System.Drawing.Point(517, 175)
		Me.Label4.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label4.Name = "Label4"
		Me.Label4.Size = New System.Drawing.Size(32, 15)
		Me.Label4.TabIndex = 46
		Me.Label4.Text = "m^3"
		'
		'Label14
		'
		Me.Label14.Font = New System.Drawing.Font("PMingLiU", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
		Me.Label14.Location = New System.Drawing.Point(4, 208)
		Me.Label14.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label14.Name = "Label14"
		Me.Label14.Size = New System.Drawing.Size(113, 15)
		Me.Label14.TabIndex = 39
		Me.Label14.Text = "總水足跡 :"
		Me.Label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight
		'
		'Total_WFP_TextBox
		'
		Me.Total_WFP_TextBox.Location = New System.Drawing.Point(125, 202)
		Me.Total_WFP_TextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.Total_WFP_TextBox.Name = "Total_WFP_TextBox"
		Me.Total_WFP_TextBox.ReadOnly = True
		Me.Total_WFP_TextBox.Size = New System.Drawing.Size(168, 25)
		Me.Total_WFP_TextBox.TabIndex = 38
		'
		'Label13
		'
		Me.Label13.AutoSize = True
		Me.Label13.Location = New System.Drawing.Point(301, 208)
		Me.Label13.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label13.Name = "Label13"
		Me.Label13.Size = New System.Drawing.Size(32, 15)
		Me.Label13.TabIndex = 37
		Me.Label13.Text = "m^3"
		'
		'Total_ISO_TextBox
		'
		Me.Total_ISO_TextBox.Location = New System.Drawing.Point(341, 202)
		Me.Total_ISO_TextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.Total_ISO_TextBox.Name = "Total_ISO_TextBox"
		Me.Total_ISO_TextBox.ReadOnly = True
		Me.Total_ISO_TextBox.Size = New System.Drawing.Size(168, 25)
		Me.Total_ISO_TextBox.TabIndex = 40
		'
		'Label21
		'
		Me.Label21.AutoSize = True
		Me.Label21.Location = New System.Drawing.Point(517, 208)
		Me.Label21.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label21.Name = "Label21"
		Me.Label21.Size = New System.Drawing.Size(32, 15)
		Me.Label21.TabIndex = 41
		Me.Label21.Text = "m^3"
		'
		'Product_DataGridView
		'
		Me.Product_DataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
		Me.Product_DataGridView.Dock = System.Windows.Forms.DockStyle.Top
		Me.Product_DataGridView.Location = New System.Drawing.Point(4, 132)
		Me.Product_DataGridView.Margin = New System.Windows.Forms.Padding(4)
		Me.Product_DataGridView.Name = "Product_DataGridView"
		Me.Product_DataGridView.RowTemplate.Height = 24
		Me.Product_DataGridView.Size = New System.Drawing.Size(560, 442)
		Me.Product_DataGridView.TabIndex = 7
		'
		'FlowLayoutPanel3
		'
		Me.FlowLayoutPanel3.Controls.Add(Me.Label10)
		Me.FlowLayoutPanel3.Controls.Add(Me.RecentUsed_ComboBox)
		Me.FlowLayoutPanel3.Controls.Add(Me.Label15)
		Me.FlowLayoutPanel3.Controls.Add(Me.FilterInfo_Label)
		Me.FlowLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Top
		Me.FlowLayoutPanel3.Location = New System.Drawing.Point(4, 61)
		Me.FlowLayoutPanel3.Margin = New System.Windows.Forms.Padding(4)
		Me.FlowLayoutPanel3.Name = "FlowLayoutPanel3"
		Me.FlowLayoutPanel3.Size = New System.Drawing.Size(560, 71)
		Me.FlowLayoutPanel3.TabIndex = 6
		'
		'Label10
		'
		Me.Label10.Location = New System.Drawing.Point(4, 10)
		Me.Label10.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label10.Name = "Label10"
		Me.Label10.Size = New System.Drawing.Size(112, 15)
		Me.Label10.TabIndex = 0
		Me.Label10.Text = "最近曾使用產品"
		'
		'RecentUsed_ComboBox
		'
		Me.RecentUsed_ComboBox.Font = New System.Drawing.Font("PMingLiU", 10.0!)
		Me.RecentUsed_ComboBox.FormattingEnabled = True
		Me.RecentUsed_ComboBox.Location = New System.Drawing.Point(124, 4)
		Me.RecentUsed_ComboBox.Margin = New System.Windows.Forms.Padding(4)
		Me.RecentUsed_ComboBox.Name = "RecentUsed_ComboBox"
		Me.RecentUsed_ComboBox.Size = New System.Drawing.Size(430, 25)
		Me.RecentUsed_ComboBox.TabIndex = 1
		Me.RecentUsed_ComboBox.Text = "請選擇"
		'
		'Label15
		'
		Me.Label15.AutoSize = True
		Me.Label15.ForeColor = System.Drawing.Color.Blue
		Me.Label15.Location = New System.Drawing.Point(4, 45)
		Me.Label15.Margin = New System.Windows.Forms.Padding(4, 12, 4, 0)
		Me.Label15.Name = "Label15"
		Me.Label15.Size = New System.Drawing.Size(124, 15)
		Me.Label15.TabIndex = 2
		Me.Label15.Text = "篩選筆數 / 總筆數"
		'
		'FilterInfo_Label
		'
		Me.FilterInfo_Label.AutoSize = True
		Me.FilterInfo_Label.ForeColor = System.Drawing.Color.Crimson
		Me.FilterInfo_Label.Location = New System.Drawing.Point(136, 45)
		Me.FilterInfo_Label.Margin = New System.Windows.Forms.Padding(4, 12, 4, 0)
		Me.FilterInfo_Label.Name = "FilterInfo_Label"
		Me.FilterInfo_Label.Size = New System.Drawing.Size(31, 15)
		Me.FilterInfo_Label.TabIndex = 3
		Me.FilterInfo_Label.Text = "N/A"
		'
		'FlowLayoutPanel2
		'
		Me.FlowLayoutPanel2.Controls.Add(Me.Label5)
		Me.FlowLayoutPanel2.Controls.Add(Me.Name_Filter_TextBox)
		Me.FlowLayoutPanel2.Controls.Add(Me.Clear_Filter_Button)
		Me.FlowLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Top
		Me.FlowLayoutPanel2.Location = New System.Drawing.Point(4, 22)
		Me.FlowLayoutPanel2.Margin = New System.Windows.Forms.Padding(4)
		Me.FlowLayoutPanel2.Name = "FlowLayoutPanel2"
		Me.FlowLayoutPanel2.Size = New System.Drawing.Size(560, 39)
		Me.FlowLayoutPanel2.TabIndex = 3
		'
		'Label5
		'
		Me.Label5.Location = New System.Drawing.Point(4, 10)
		Me.Label5.Margin = New System.Windows.Forms.Padding(4, 10, 4, 0)
		Me.Label5.Name = "Label5"
		Me.Label5.Size = New System.Drawing.Size(112, 15)
		Me.Label5.TabIndex = 0
		Me.Label5.Text = "專案名稱篩選"
		'
		'Name_Filter_TextBox
		'
		Me.Name_Filter_TextBox.Location = New System.Drawing.Point(124, 4)
		Me.Name_Filter_TextBox.Margin = New System.Windows.Forms.Padding(4)
		Me.Name_Filter_TextBox.Name = "Name_Filter_TextBox"
		Me.Name_Filter_TextBox.Size = New System.Drawing.Size(323, 25)
		Me.Name_Filter_TextBox.TabIndex = 1
		'
		'Clear_Filter_Button
		'
		Me.Clear_Filter_Button.Location = New System.Drawing.Point(455, 4)
		Me.Clear_Filter_Button.Margin = New System.Windows.Forms.Padding(4)
		Me.Clear_Filter_Button.Name = "Clear_Filter_Button"
		Me.Clear_Filter_Button.Size = New System.Drawing.Size(100, 29)
		Me.Clear_Filter_Button.TabIndex = 2
		Me.Clear_Filter_Button.Text = "清除篩選"
		Me.Clear_Filter_Button.UseVisualStyleBackColor = True
		'
		'LoadProduct_Button
		'
		Me.LoadProduct_Button.ForeColor = System.Drawing.Color.DarkGreen
		Me.LoadProduct_Button.Location = New System.Drawing.Point(4, 851)
		Me.LoadProduct_Button.Margin = New System.Windows.Forms.Padding(4)
		Me.LoadProduct_Button.Name = "LoadProduct_Button"
		Me.LoadProduct_Button.Size = New System.Drawing.Size(568, 38)
		Me.LoadProduct_Button.TabIndex = 3
		Me.LoadProduct_Button.Text = "載入"
		Me.LoadProduct_Button.UseVisualStyleBackColor = True
		'
		'DeleteButton
		'
		Me.DeleteButton.ForeColor = System.Drawing.Color.Red
		Me.DeleteButton.Location = New System.Drawing.Point(4, 897)
		Me.DeleteButton.Margin = New System.Windows.Forms.Padding(4)
		Me.DeleteButton.Name = "DeleteButton"
		Me.DeleteButton.Size = New System.Drawing.Size(568, 38)
		Me.DeleteButton.TabIndex = 10
		Me.DeleteButton.Text = "刪除"
		Me.DeleteButton.UseVisualStyleBackColor = True
		'
		'ProductForm
		'
		Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 15.0!)
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.ClientSize = New System.Drawing.Size(577, 952)
		Me.Controls.Add(Me.FlowLayoutPanel1)
		Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
		Me.Margin = New System.Windows.Forms.Padding(4)
		Me.Name = "ProductForm"
		Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
		Me.Text = "專案"
		Me.TopMost = True
		Me.FlowLayoutPanel1.ResumeLayout(False)
		Me.GroupBox2.ResumeLayout(False)
		Me.FlowLayoutPanel4.ResumeLayout(False)
		Me.FlowLayoutPanel4.PerformLayout()
		CType(Me.Product_DataGridView, System.ComponentModel.ISupportInitialize).EndInit()
		Me.FlowLayoutPanel3.ResumeLayout(False)
		Me.FlowLayoutPanel3.PerformLayout()
		Me.FlowLayoutPanel2.ResumeLayout(False)
		Me.FlowLayoutPanel2.PerformLayout()
		Me.ResumeLayout(False)

	End Sub

	Friend WithEvents FlowLayoutPanel1 As FlowLayoutPanel
	Friend WithEvents GroupBox2 As GroupBox
	Friend WithEvents FlowLayoutPanel4 As FlowLayoutPanel
	Friend WithEvents Label6 As Label
	Friend WithEvents ProductName_TextBox As TextBox
	Friend WithEvents Label11 As Label
	Friend WithEvents StageA_WFP_TextBox As TextBox
	Friend WithEvents Label9 As Label
	Friend WithEvents Label2 As Label
	Friend WithEvents StageB_WFP_TextBox As TextBox
	Friend WithEvents Label3 As Label
	Friend WithEvents Label7 As Label
	Friend WithEvents StageC_WFP_TextBox As TextBox
	Friend WithEvents Label12 As Label
	Friend WithEvents Product_DataGridView As DataGridView
	Friend WithEvents FlowLayoutPanel3 As FlowLayoutPanel
	Friend WithEvents Label10 As Label
	Friend WithEvents RecentUsed_ComboBox As ComboBox
	Friend WithEvents FlowLayoutPanel2 As FlowLayoutPanel
	Friend WithEvents Label5 As Label
	Friend WithEvents Name_Filter_TextBox As TextBox
	Friend WithEvents Clear_Filter_Button As Button
	Friend WithEvents LoadProduct_Button As Button
	Friend WithEvents DeleteButton As Button
	Friend WithEvents Label15 As Label
	Friend WithEvents FilterInfo_Label As Label
	Friend WithEvents CenterWFN_Label As Label
	Friend WithEvents CenterISO_Label As Label
	Friend WithEvents StageA_ISO_TextBox As TextBox
	Friend WithEvents Label18 As Label
	Friend WithEvents StageB_ISO_TextBox As TextBox
	Friend WithEvents Label19 As Label
	Friend WithEvents StageC_ISO_TextBox As TextBox
	Friend WithEvents Label20 As Label
	Friend WithEvents Label14 As Label
	Friend WithEvents Total_WFP_TextBox As TextBox
	Friend WithEvents Label13 As Label
	Friend WithEvents Total_ISO_TextBox As TextBox
	Friend WithEvents Label21 As Label
	Friend WithEvents Label72 As Label
	Friend WithEvents Stage_Asm_WFP_TextBox As TextBox
	Friend WithEvents Label1 As Label
	Friend WithEvents Stage_Asm_ISO_TextBox As TextBox
	Friend WithEvents Label4 As Label
	Friend WithEvents EmptyLabel As Label
	Friend WithEvents EmptyLabel2 As Label
End Class
