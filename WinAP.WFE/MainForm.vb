﻿Imports System
Imports System.Configuration
Imports SWs.Helper ' Solidworks API Helper
Imports WFE.Assistants.Controllers
Imports WFE.Assistants.Helpers
Imports WFE.Assistants.Models
Imports WFE.Assistants.Models.AssemblyPageViewModels
Imports WFE.Assistants.Models.EventArgs
Imports WFE.Assistants.Models.ORDs
Imports WFE.Assistants.Repos

Public Class MainForm
	'============ variable ================
	Private _dc As DataController
	Private _configValue As String
	Private _configRepo As ConfigRepo

	Private Sub MainForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
		'起始位置 => 視窗右上角
		Dim screenWidth = Screen.PrimaryScreen.Bounds.Width
		Dim screenHeight = Screen.PrimaryScreen.Bounds.Height
		Me.StartPosition = FormStartPosition.Manual
		Dim loc_x = screenWidth - Me.Width
		Dim loc_y = 5
		Me.Location = New Point(loc_x, loc_y)

		'version
		Me.Text += " ver." + VersionHelper.Version

		'初始化預設值
		Initialize_Default_Settings()

		'檢查資料庫連線
		DBConnectionCheck()

		'產品比較
		ProductCompareToolStripMenuItem.Visible = True
	End Sub

	Private Sub Initialize_Default_Settings()
		'config
		InitConfigValue()

		'Data Controller
		_dc = New DataController()
		_dc.Init(_configValue)

		_dc.SetupTabPageA(a_DataGridView)
		AddHandler _dc.TabPageA.ProductComponetClick, AddressOf ProductComponent_Click
		AddHandler _dc.TabPageA.ProductComponetDeleted, AddressOf ProductComponent_Deleted

		_dc.SetupTabPageB(B_ChoiceComponent_DataGridView,
						  B_Component_Post_Process_DataGridView,
						  B_Name_Filter_TextBox, B_Choice_Component_TextBox)
		Dim handler As New Update_CapturedSpecialComponentInfo_Delegate(AddressOf c_Update_Captured_Component)
		_dc.SetupTabPageC(c_Special_Components_DataGridView, handler)

		_dc.SetupTabPageD(D_GenValTextBox, D_PostProcValTextBox,
						  D_SpecialComponentTextBox, D_TotalWFValTextBox,
						  BarChart, PieChart,
						  D_ISO_1_TextBox, D_ISO_2_TextBox, D_ISO_3_TextBox, D_ISO_Total_TextBox, ISO_PieChart,
						  D_WFN_4_TextBox, D_ISO_4_TextBox
		) ' TabPage D 設定

		'Tab Assembly 設定
		_dc.SetupAssemblyPage(assemblyDataGridView, assembly_choiceTextBox,
							  assembly_choice_transport_Button, add_assembly_Button,
							  assembly_wfn_Label, assembly_iso_Label,
							  assembly_TreeView, assembly_record_DataGridView,
							  assembly_total_wfn_TextBox, assembly_total_iso_TextBox,
							  function_ok_Button,
							  asm_stage_wfn_Label, asm_stage_iso_Label)

		'TabControl Tab C.特殊零件
		EBoardRadioButton.Checked = True

#If DEBUG Then
		'可編輯零件名稱
		a_ComponnetNameTextBox.ReadOnly = False
		AddHandler a_ComponnetNameTextBox.TextChanged, AddressOf a_ComponnetNameTextBox_TextChange

		C_Component_Name_TextBox.ReadOnly = False
		AddHandler C_Component_Name_TextBox.TextChanged, AddressOf C_Component_Name_TextBox_TextChange
#End If
	End Sub
	Private Sub a_ComponnetNameTextBox_TextChange(sender As Object, e As EventArgs)
		_dc.StageAViewModel.Component_Name = a_ComponnetNameTextBox.Text
	End Sub
	Private Sub C_Component_Name_TextBox_TextChange(sender As Object, e As EventArgs)
		_dc.StageCViewModel.Component_Name = C_Component_Name_TextBox.Text
	End Sub

	Private Sub InitConfigValue()
		'取得設定檔位置
		Dim appSettings = ConfigurationManager.AppSettings
		_configValue = appSettings("settings")
		If IsNothing(_configValue) Then
			_configValue = ""
		End If
		_configRepo = New ConfigRepo(_configValue)
		If _configRepo.IsValid Then
			UIHelper.SetPrceisionNumber(_configRepo.PrecisionNumber)
			UIHelper.SetNumberDisplayType(_configRepo.NumberDisplayType)
			UIHelper.SetNumberTextFormatPattern(_configRepo.GetNumberFormatPattern())
		End If
	End Sub

	Private Sub DBConnectionCheck()
		Dim repo = New ConfigRepo(_configValue)
		Dim success = repo.CurrentDBConnectionCheck()
		If Not success Then
			Dim dialog = New WarningFormDialog()
			dialog.ShowDialog()
		End If
	End Sub

	Private Sub WFETabControl_SelectedIndexChanged(sender As Object, e As EventArgs) Handles WFETabControl.SelectedIndexChanged
		If (WFETabControl.SelectedIndex = 4) Then
			'綜合分析
			_dc.UpdateChart()
		End If
		If (WFETabControl.SelectedIndex = 3) Then
			'產品組裝
			_dc.AssemblyPageUpdate()
		End If
	End Sub

#Region "TabControl A 一般零件"
	Private Sub A_CaptureComponentButton_Click(sender As Object, e As EventArgs) Handles A_CaptureComponentButton.Click
		'獲取零件
		Dim dispModel = _dc.StageAViewModel

#If DEBUG Then
		'模擬
		Dim simRepo = New SimRepo()

		dispModel.Component_Name = simRepo.GetRandomName()
		dispModel.Component_Volumn = UnitHelper.VolumnNormalize(simRepo.GetRandomVolumn())
		a_Update_Captured_Component()
		Return
#End If

		Dim swsHelper = New SWsAssistant()
		Dim model As SWsViewModel = swsHelper.GetComponentInfo()


		If (model.ErrMsg.Length > 0) Then
			MsgBox("無法取得 SolidWorks 執行物件，詳細錯誤內容請參閱系統記錄檔", 0 + 48, "警告")
			'TODO: ErrMsg			
			Return
		End If

		If (model.IsElementCatched) Then
			'Set value
			dispModel.Component_Name = model.Name
			dispModel.Component_Volumn = UnitHelper.VolumnNormalize(model.Volumn)

			'
			a_Update_Captured_Component()
		Else
			MsgBox("請選擇一組合件零件", 0 + 48, "警告") '如未選擇零件將出現"請選擇一組合件零件"提示，0 + 48會出現"黃色三角形加!"的圖示
		End If
	End Sub

	Private Sub A_ChoiceMaterialButton_Click(sender As Object, e As EventArgs) Handles A_ChoiceMaterialButton.Click
		'選取材料
		Dim winForm = New MaterialForm(_dc)
		If winForm.ShowDialog() = DialogResult.OK Then
			'設定回傳值
			Dim model = _dc.SelectedMaterialModel
			Dim dispModel = _dc.StageAViewModel

			dispModel.Material_SN = model.SN
			dispModel.Material_Name = model.Name
			dispModel.Material_Density = model.Density
			dispModel.Material_WFP = model.WFP
			dispModel.Material_ISO = model.ISO

			a_Update_Captured_Component()
		End If
	End Sub

	Private Sub a_Componet_Calculate_Button_Click(sender As Object, e As EventArgs) Handles a_Componet_Calculate_Button.Click
		'新增獲取零件到「已選零件」列表		
		Dim dispModel = _dc.StageAViewModel
		Dim exists As Boolean = _dc.ProductDTO.ComponentExists(dispModel.Component_Name)
		Dim v1 = _dc.StageAViewModel.Component_WFP <> "N/A" And _dc.StageAViewModel.Component_WFP <> ""
		Dim v2 = _dc.StageAViewModel.Component_ISO <> "N/A" And _dc.StageAViewModel.Component_ISO <> ""
		Dim valid = v1 And v2

		If exists Then
			MsgBox("零件已經存在")
		ElseIf valid Then
			Dim component = dispModel.To_ProductComponent()
			_dc.TabPageA.AddComponent(component)
			update_TabPageA_View()
		Else
			MsgBox("零件水足跡不正確！請確認是否已獲取正確的資料")
		End If
	End Sub
	Private Sub a_delete_Button_Click(sender As Object, e As EventArgs) Handles a_delete_Button.Click
		'刪除已選零件列表
		_dc.DeleteComponent()
		update_TabPageA_View()
	End Sub
	Private Sub update_TabPageA_View()
		a_DataGridView.Refresh()
		a_TotalWFP_TextBox.Text = _dc.ProductDTO.StageA_WFP
		a_Total_ISO_TextBox.Text = _dc.ProductDTO.StageA_ISO

		a_Transport_Sum_WFP_Label.Text = _dc.ProductDTO.GetTotalComponentTransportWFP()
		a_Transport_Sum_ISO_Label.Text = _dc.ProductDTO.GetTotalComponentTransportISO()

		a_Process_Sum_WFP_Label.Text = _dc.ProductDTO.GetTotalComponentProcessWFP()
		a_Process_Sum_ISO_Label.Text = _dc.ProductDTO.GetTotalComponentProcessISO()

	End Sub

	Private Sub A_ChoiceTransportButton_Click(sender As Object, e As EventArgs) Handles A_ChoiceTransportButton.Click
		'選取運輸工具
		If _dc.IsStageViewModelValid() = True Then
			Dim curComponent As ProductComponent = _dc.ProductDTO.GetProductComponet(_dc.StageAViewModel.Component_Name)
			Dim dataInfo = New TransportFormInfoDTO("stagea")
			dataInfo.UseModel(_dc.ProductDTO.ProductName, curComponent)
			Dim winForm = New TransportForm()
			winForm.UseModel(dataInfo)
			winForm.ShowDialog()

			'update View
			update_TabPageA_View()
		Else
			MsgBox("請先選擇零件")
		End If

	End Sub

	Private Sub A_ChoiceMakeProcButton_Click(sender As Object, e As EventArgs) Handles A_ChoiceMakeProcButton.Click
		'選取加工方式
		If _dc.IsStageViewModelValid() = True Then
			Dim curComponent As ProductComponent = _dc.ProductDTO.GetProductComponet(_dc.StageAViewModel.Component_Name)
			Dim dataInfo = New MakeProcessFormInfoDTO("stagea")
			dataInfo.UseModel(_dc.ProductDTO.ProductName, curComponent)
			Dim winForm = New MakeProcessForm()
			winForm.UseModel(dataInfo)
			winForm.ShowDialog()
			'update_view
			update_TabPageA_View()
		Else
			MsgBox("請先選擇零件")
		End If

	End Sub

	Private Sub A_CompletedButton_Click(sender As Object, e As EventArgs) Handles A_CompletedButton.Click
		'切換至「後製加工頁籤」
		WFETabControl.SelectedIndex = 1
	End Sub
	''' <summary>
	''' 已選擇零件 DataGridView Event
	''' <para>替換 TabPage A 已選零件資訊</para>
	''' </summary>
	''' <param name="sender"></param>
	''' <param name="e"></param>
	Private Sub ProductComponent_Click(sender As Object, e As ProductComponentEventArgs)
		_dc.StageAViewModel = e.Data.To_StageA_ViewModel()
		a_Update_Captured_Component()
	End Sub
	Private Sub ProductComponent_Deleted(sender As Object, e As ProductComponentEventArgs)
		'刪除已選零件
		_dc.TabPageB.DeletePostComponentByName(e.Data.Component_Name)
		update_TabPageB_View()
	End Sub

	''' <summary>
	''' 更新 Stage A 畫面資訊
	''' </summary>
	Private Sub a_Update_Captured_Component()
		Dim dispModel = _dc.StageAViewModel

		a_ComponnetNameTextBox.Text = dispModel.Component_Name
		a_VolumnTextBox.Text = dispModel.Component_Volumn

		a_Material_TextBox.Text = dispModel.Material_Name
		a_Material_Density_TextBox.Text = dispModel.Material_Density
		a_Material_WFP_TextBox.Text = dispModel.Material_WFP
		a_Material_ISO_TextBox.Text = dispModel.Material_ISO

		a_Componet_Mass_TextBox.Text = dispModel.Component_Mass
		a_Component_WFP_Label.Text = dispModel.Component_WFP
		a_Component_ISO_Label.Text = dispModel.Component_ISO
	End Sub
#End Region

#Region "Tab Page 後製加工"

	Private Sub B_CleanFilterButton_Click(sender As Object, e As EventArgs) Handles B_CleanFilterButton.Click
		B_Name_Filter_TextBox.Text = ""
	End Sub

	Private Sub b_AddComponent_Button_Click(sender As Object, e As EventArgs) Handles b_AddComponent_Button.Click
		'新增獲取零件到「已選零件」列表
		Dim isSelected As Boolean = _dc.TabPageB.IsSelectComponent()
		If (isSelected) Then
			Dim selectedModel = _dc.TabPageB.GetSelectedProductComponent()
			Dim exists As Boolean = _dc.PostComponentExists(selectedModel.Component_Name)

			If exists Then
				MsgBox("零件已經存在")
			Else
				_dc.TabPageB.AddPostComponent(selectedModel)
				update_TabPageB_View()
			End If
		Else
			MsgBox("請先選擇零件！")
		End If

	End Sub

	Private Sub B_ChoiceTransportButton_Click(sender As Object, e As EventArgs) Handles B_ChoiceTransportButton.Click
		'後製加工 - 選取運輸工具
		Dim postComponent As ProductComponent = _dc.TabPageB.GetSelectedPostProductComponent()
		If postComponent IsNot Nothing Then
			Dim dataInfo = New TransportFormInfoDTO("stageb")
			dataInfo.UseModel(_dc.ProductDTO.ProductName, postComponent)
			Dim winForm = New TransportForm()
			winForm.UseModel(dataInfo)
			winForm.ShowDialog()

			update_TabPageB_View()
		Else
			MsgBox("請先選擇「後製加工零件」！")
		End If

	End Sub
	Private Sub update_TabPageB_View()
		'後製加工更新 View
		B_Component_Post_Process_DataGridView.Refresh()
		b_post_transport_sum_wfp_Label.Text = _dc.GetTotalPostComponentTrasnportWFP()
		b_post_transport_sum_iso_Label.Text = _dc.GetTotalPostComponentTrasnportISO()

		b_post_process_sum_wfp_Label.Text = _dc.GetTotalPostComponentProcessWFP()
		b_post_process_sum_iso_Label.Text = _dc.GetTotalPostComponentProcessISO()

		B_Component_Post_Process_WFP_TextBox.Text = _dc.GetTotalPostComponentWFP()
		B_Component_Post_Process_ISO_TextBox.Text = _dc.GetTotalPostComponentISO()
	End Sub
	Private Sub B_ChoiceMakeProcessButton_Click(sender As Object, e As EventArgs) Handles B_ChoiceMakeProcessButton.Click
		'後製加工 - 選取加工方式
		Dim postComponent As ProductComponent = _dc.TabPageB.GetSelectedPostProductComponent()
		If postComponent IsNot Nothing Then
			Dim dataInfo = New MakeProcessFormInfoDTO("stageb")
			dataInfo.UseModel(_dc.ProductDTO.ProductName, postComponent)
			Dim winForm = New MakeProcessForm()
			winForm.UseModel(dataInfo)
			winForm.ShowDialog()
			'update_view
			update_TabPageB_View()
		Else
			MsgBox("請先選擇「後製加工零件」！")
		End If

	End Sub

	Private Sub B_Delete_Component_Post_Process_Button_Click(sender As Object, e As EventArgs) Handles B_Delete_Component_Post_Process_Button.Click
		'後製加工 - 刪除零件
		_dc.TabPageB.DeletePostComponent()
		update_TabPageB_View()
	End Sub

	Private Sub B_CompletedButton_Click(sender As Object, e As EventArgs) Handles B_CompletedButton.Click
		'切換至 C. 特殊零件頁籤
		WFETabControl.SelectedIndex = 2
	End Sub
#End Region

#Region "Tab Page 特殊零件"
	Private Sub C_CaptureComponentButton_Click(sender As Object, e As EventArgs) Handles C_CaptureComponentButton.Click
		'特殊零件 - 獲取零件
		'取得類型

		Dim dispModel = _dc.StageCViewModel

#If DEBUG Then
		'模擬
		Dim simRepo = New SimRepo()

		dispModel.Component_Name = simRepo.GetRandomName()
		dispModel.Component_Volumn = simRepo.GetRandomVolumn()
		dispModel.Component_Area = simRepo.GetRandomArea()
		c_Update_Captured_Component()
		Return
#End If

		Dim swsHelper = New SWsAssistant()
		Dim model As SWsViewModel = swsHelper.GetComponentInfo()
		If (model.ErrMsg.Length > 0) Then
			MsgBox("無法取得 SolidWorks 執行物件，詳細錯誤內容請參閱系統記錄檔", 0 + 48, "警告")
			'TODO: ErrMsg			
			Return
		End If

		If (model.IsElementCatched) Then
			'Set value
			dispModel.Component_Name = model.Name
			dispModel.Component_Volumn = model.Volumn
			dispModel.Component_Area = model.SurfaceArea / 2.0 'Component_Area  = SurfaceArea / 2.0
			'Update UI
			c_Update_Captured_Component()
		Else
			MsgBox("請選擇一組合件零件", 0 + 48, "警告") '如未選擇零件將出現"請選擇一組合件零件"提示，0 + 48會出現"黃色三角形加!"的圖示
		End If
	End Sub

	Public Sub c_Update_Captured_Component()
		Dim model = _dc.StageCViewModel
		'獲取元件資訊
		C_Component_Name_TextBox.Text = model.Component_Name
		C_VolumnTextBox.Text = model.Component_Volumn
		C_AreaTextBox.Text = model.Component_Area

		'選取製程資訊
		C_Manufacture_Name_TextBox.Text = model.Manufacture_Name
		C_Manufacture_Density_TextBox.Text = model.Manufacture_Density
		C_Manufacture_WFP_TextBox.Text = model.Manufacture_WFP
		C_Manufacture_ISO_TextBox.Text = model.Manufacture_ISO

		'計算資訊
		C_MassTextBox.Text = model.Component_Mass
		C_Component_WFP_Label.Text = model.WFP
		C_Component_ISO_Label.Text = model.ISO

	End Sub
	Private Sub cleanManufactureProcedureChoice()
		'清除已選擇的製程
		Dim model = _dc.StageCViewModel
		model.ClearManufactureProcedureInfo()
	End Sub
	Private Sub EBoardRadioButton_CheckedChanged(sender As Object, e As EventArgs) Handles EBoardRadioButton.CheckedChanged
		'電路版		
		Display_C_AreaPanel(EBoardRadioButton.Checked)
		_dc.SetSpecialComponentType("EBoard")
		cleanManufactureProcedureChoice()
		c_Update_Captured_Component()
	End Sub
	Private Sub EComopnentRadioButton_CheckedChanged(sender As Object, e As EventArgs) Handles EComopnentRadioButton.CheckedChanged
		'電子元件
		Display_C_AreaPanel(Not EComopnentRadioButton.Checked)
		_dc.SetSpecialComponentType("EComponent")
		cleanManufactureProcedureChoice()
		c_Update_Captured_Component()
	End Sub

	Private Sub LCDRadioButton_CheckedChanged(sender As Object, e As EventArgs) Handles LCDRadioButton.CheckedChanged
		'LCD
		Display_C_AreaPanel(LCDRadioButton.Checked)
		_dc.SetSpecialComponentType("LCD")
		cleanManufactureProcedureChoice()
		c_Update_Captured_Component()
	End Sub

	Private Sub C_ChoiceMakeProcessButton_Click(sender As Object, e As EventArgs) Handles C_ChoiceMakeProcessButton.Click
		'開啟製程 Dialog
		Dim winForm = New ManufactureProcedureForm(_dc)
		If winForm.ShowDialog() = DialogResult.OK Then
			Dim result = _dc.SelectedManufactureProcedureModel
			Dim dispModel = _dc.StageCViewModel
			dispModel.Manufacture_SN = result.SN
			dispModel.Manufacture_Name = result.ProcessName
			dispModel.Manufacture_ProcessTypeCode = result.ProcessTypeCode
			dispModel.Manufacture_Density = result.Density
			dispModel.Manufacture_WFP = result.WFP
			dispModel.Manufacture_ISO = result.ISO

			c_Update_Captured_Component()
		End If

	End Sub

	Private Sub c_add_component_Button_Click(sender As Object, e As EventArgs) Handles c_add_component_Button.Click
		'新增特殊製資料至記錄
		Dim dispModel = _dc.StageCViewModel
		Dim exists As Boolean = _dc.ProductDTO.SpecialComponentExists(dispModel.Component_Name)
		If exists Then
			MsgBox("零件已經存在")
		Else
			Dim component = dispModel.ToProductSpecialComponent()
			_dc.TabPageC.AddComponent(component)
			update_TabPageC_View()
		End If
	End Sub
	Private Sub C_DeleteButton_Click(sender As Object, e As EventArgs) Handles C_Delete_Button.Click
		'刪除特殊零件
		_dc.TabPageC.DeleteComponent()
	End Sub

	Private Sub C_CompletedButton_Click(sender As Object, e As EventArgs) Handles C_CompletedButton.Click
		'切換至產品組裝頁籤
		WFETabControl.SelectedIndex = 3
	End Sub

	''' <summary>
	''' 控制顯示 TabControl Tab C.特殊零件 C_Area_Panel / C_Volumn_Panel, C_Density_Panel, C_Mass_Panel
	''' </summary>
	''' <param name="enableDisplay">是否顯示</param>
	Private Sub Display_C_AreaPanel(enableDisplay As Boolean)
		'C_Area_Panel.Visible = enableDisplay
		'C_Volumn_Panel.Visible = Not enableDisplay
		'C_Density_Panel.Visible = Not enableDisplay
		'C_Mass_Panel.Visible = Not enableDisplay
	End Sub
	Private Sub update_TabPageC_View()
		'
		c_Special_Components_DataGridView.Refresh()
		C_Total_WFP_TextBox.Text = _dc.GetTotalSpecialComponetWFP()
		C_Total_ISO_TextBox.Text = _dc.GetTotalSpecialComponetISO()
	End Sub

#End Region

#Region "Tab Page 綜合分析"
	Private Sub D_ChoiceProductButton_Click(sender As Object, e As EventArgs) Handles D_ChoiceProductButton.Click
		'開啟Product Form Dialog
		Dim winForm = New ProductForm()
		winForm.UseController(_dc)
		If winForm.ShowDialog() = DialogResult.OK Then
			updatProductInfo()
		End If
	End Sub
#End Region

#Region "Menu 產品/新增/儲存"
	Private Sub OpenToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles OpenToolStripMenuItem.Click
		'載入產品
		Dim winForm = New ProductForm()
		winForm.UseController(_dc)
		If winForm.ShowDialog() = DialogResult.OK Then
			updatProductInfo()
		End If
	End Sub

	Private Sub SaveToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles SaveToolStripMenuItem.Click
		'儲存產品
		Dim isEmpty As Boolean = _dc.IsProductEmpty()
		If isEmpty Then
			MsgBox("產品零件為空！請先新增零件")
		Else
			Dim winForm = New SaveProductDialogForm()
			winForm.UseController(_dc)
			winForm.ShowDialog()
			updatProductInfo()
		End If

	End Sub

	Private Sub NewToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles NewToolStripMenuItem.Click
		'新增產品
		Dim result As Integer = MessageBox.Show("目前已編輯的資料將會清除！確定新增產品? ", "警告！！", MessageBoxButtons.YesNo)
		If result = DialogResult.Yes Then
			Dim success = _dc.ReNewProduct()
			If success Then
				MsgBox("已建立新產品")
			Else
				MsgBox("建立新產品失敗")
			End If
			updatProductInfo()
		End If
	End Sub
	Private Sub updatProductInfo()
		ProductName_StatusLabel.Text = _dc.ProductDTO.ProductName
		ProductName_StatusLabel.ToolTipText = String.Format("{0}, {1}", _dc.ProductDTO.ProductName, _dc.ProductDTO.ProductDesc)

		a_Update_Captured_Component()
		update_TabPageA_View()

		B_Choice_Component_TextBox.Text = ""
		update_TabPageB_View()

		c_Update_Captured_Component()
		update_TabPageC_View()

		_dc.AssemblyPage.Update()

		_dc.UpdateChart()
	End Sub
#End Region

#Region "Menu - 資料新增/編輯"
	Private Sub MaterialToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles MaterialToolStripMenuItem.Click
		'材料新增/編輯
		Dim winForm = New DataEditForm()
		winForm.UseTabPage("material")
		winForm.ShowDialog()
	End Sub

	Private Sub SPMakeProcessToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles SPMakeProcessToolStripMenuItem.Click
		'製程新增/編輯
		Dim winForm = New DataEditForm()
		winForm.UseTabPage("electronic")
		winForm.ShowDialog()
	End Sub

	Private Sub TransportToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles TransportToolStripMenuItem.Click
		'運輸工具新增/編輯
		Dim winForm = New DataEditForm()
		winForm.UseTabPage("transport")
		winForm.ShowDialog()
	End Sub

	Private Sub MakeProcessToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles MakeProcessToolStripMenuItem.Click
		'加工方式新增/編輯
		Dim winForm = New DataEditForm()
		winForm.UseTabPage("makeProcess")
		winForm.ShowDialog()
	End Sub
	Private Sub AssemblyMethodToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles AssemblyMethodToolStripMenuItem.Click
		'組裝方式新增/編輯
		Dim winForm = New DataEditForm()
		winForm.UseTabPage("assembly")
		winForm.ShowDialog()
	End Sub
#End Region

#Region "Menu - 設定"
	Private Sub 資料庫設定ToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles DBToolStripMenuItem.Click

		'開啟資料庫設定 Dialog
		Dim winForm = New DBSettingsForm(_configValue)
		winForm.ShowDialog()
	End Sub
#End Region
	Private Sub CalculateToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles CalculateToolStripMenuItem.Click
		'計算設定
		Dim win = New CalculateSettingForm()
		win.ShowDialog()
	End Sub

	Private Sub ProductComparisonToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ProductComparisonToolStripMenuItem.Click
		'開啟產品比較
		Dim win = New Main_ProductCompareForm()
		win.StartPosition = FormStartPosition.CenterScreen
		win.Show()
	End Sub

	Private Sub assembly_choice_transport_Button_Click(sender As Object, e As EventArgs) Handles assembly_choice_transport_Button.Click
		'Assembly Page, 選擇運輸工具
		Dim page = _dc.AssemblyPage
		If page.IsCTA_Item_Selected() = True Then
			'assign
			Dim selectedComponent = page.GetSelectedItem()
			Dim dataInfo = New AssemblyTransportFormInfoDTO(_dc.ProductDTO.ProductName)
			'open dialog
			Dim winForm = New AssemblyTransportForm()
			winForm.UseModel(dataInfo, selectedComponent, _dc.ProductDTO.AssemblyTransports)
			winForm.ShowDialog()

			'update view
			page.UpdateTransportWFP()
		Else
			MsgBox("請先選擇零件")
		End If

	End Sub

	Private Sub add_assembly_Button_Click(sender As Object, e As EventArgs) Handles add_assembly_Button.Click
		'加入組裝
		Dim page = _dc.AssemblyPage
		If page.IsCTA_Item_Selected() = False Then
			MsgBox("請先選擇零件")
		Else
			If page.IsCTA_Item_Added() = True Then
				MsgBox("此零件已加入組裝")
			Else
				'Add to assembly list
				page.Add_CTA_Item_To_Assembly()
			End If
		End If
	End Sub

	Private Sub assembly_tree_toggle_Button_Click(sender As Object, e As EventArgs) Handles assembly_tree_toggle_Button.Click
		' Toggle Tree Collapse
		Dim page = _dc.AssemblyPage
		Dim text = page.TreeToggle()
		assembly_tree_toggle_Button.Text = text + "節點"
	End Sub

	Private Sub enable_merge_CheckBox_CheckedChanged(sender As Object, e As EventArgs) Handles enable_merge_CheckBox.CheckedChanged
		'組裝零件
		Dim checked = enable_merge_CheckBox.Checked
		If (enable_disassembly_CheckBox.Checked = True) Then
			enable_disassembly_CheckBox.Checked = Not checked
		End If

		Dim page = _dc.AssemblyPage
		page.EnableMerge(sender)
	End Sub

	Private Sub enable_disassembly_CheckBox_CheckedChanged(sender As Object, e As EventArgs) Handles enable_disassembly_CheckBox.CheckedChanged
		'分解 / 移除
		Dim checked = enable_disassembly_CheckBox.Checked
		If (enable_merge_CheckBox.Checked = True) Then
			enable_merge_CheckBox.Checked = Not checked
		End If

		Dim page = _dc.AssemblyPage
		page.EnableDisassembly(sender)
	End Sub

	Private Sub assembly_stage_finish_Button_Click(sender As Object, e As EventArgs) Handles assembly_stage_finish_Button.Click
		'切換至綜合分析頁籤
		WFETabControl.SelectedIndex = 4
	End Sub

	Private Sub SaveAsToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles SaveAsToolStripMenuItem.Click
		'另存專案		
		Dim isEmpty As Boolean = _dc.IsProductEmpty()
		If isEmpty Then
			MsgBox("產品零件為空！請先新增零件")
		Else
			Dim winForm = New SaveProductDialogForm()
			winForm.ProjectSaveAs = True
			winForm.UseController(_dc)
			winForm.ShowDialog()
			updatProductInfo()
		End If
	End Sub

	Private Sub CopyrightInfoToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles CopyrightInfoToolStripMenuItem.Click
		Dim winForm = New CopyrightForm()
		winForm.ShowDialog()
	End Sub
End Class
